<?php
header('Content-Type: text/html; charset=utf-8');
include('../../conf/config_web.php');
include('../conf/config_web.php');

$table_customer='customer';
$preferences='preferences';
$table_customer_preferences='customer_preferences';
$debug_pagina=0;

$sexo = (!empty($_REQUEST['genero'])) ? $_REQUEST['genero'] : '';
$nombre = (!empty($_REQUEST['nombre'])) ? trim($_REQUEST['nombre']) : '';
$apellidos = (!empty($_REQUEST['apellidos'])) ? trim($_REQUEST['apellidos']) : '';
$email = !empty($_REQUEST['em']) ? trim($_REQUEST['em']) : @$_REQUEST['em'];
$cp = (int)$_REQUEST['cp'];
$birth_date = (!empty($_REQUEST['birth_date'])) ? trim($_REQUEST['birth_date']) : '';
$legal =  (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';
$legal = ($legal == 1 || $legal == 0) ? $legal : 1;
//$permanencia = (!empty($_REQUEST['perm'])) ? trim($_REQUEST['perm']) : '';
//$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;
//$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';
//$id_client	= (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? trim($_REQUEST['idclient']) : '';
//$sourcetype = (!empty($_REQUEST['sourcetype'])) ? trim($_REQUEST['sourcetype']) : '';
//$campaign = (!empty($_REQUEST['campaign'])) ? trim($_REQUEST['campaign']) : '';
//$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';
//$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? trim($_REQUEST['prioridad']) : '';
//$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? trim($_REQUEST['telefono']) : '';

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

if($email != '')
{
	$conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name,$debug_pagina);

	if(!$conexion ->connectDB())
	{
	  $msg_err = 'No se ha podido conectar a la BBDD. Error 102';
	}
	else
	{
	  $cur_conn_id = $conexion->get_id_conexion();
	  $nombre = str_replace('  ',' ',ucwords(strtolower($nombre)));
	  $nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre),$cur_conn_id),'%_');
	  $apellidosIns = addcslashes(mysql_real_escape_string(checkSqlInjection($apellidos),$cur_conn_id),'%_');
	  $cpIns = addcslashes(mysql_real_escape_string(checkSqlInjection($cp),$cur_conn_id),'%_');
	  $sexIns = addcslashes(mysql_real_escape_string(checkSqlInjection($sexo),$cur_conn_id),'%_');
	  $emailIns = addcslashes(mysql_real_escape_string(checkSqlInjection($email),$cur_conn_id),'%_');
	  $birth_dateIns = addcslashes(mysql_real_escape_string(checkSqlInjection($birth_date),$cur_conn_id),'%_');

	  $sql='SELECT id_preference, field_form_name FROM %s WHERE enabled=1';
	  $sql=sprintf($sql,$preferences,$emailIns);
	  $conexion->getResultSelectArray($sql);
	  $datosRespuesta=$conexion->tResultadoQuery;
	  $arrBBDDFields=array();
	  $arrBBDDIdPref=array();

	  //   SE CARGAN LOS ARRAYS CON LOS VALORES DE LOS CAMPOS DE LA TABLA DE "preferences"
	  foreach($datosRespuesta as $key=>$value)
	  {
		$arrBBDDFields[$value['field_form_name']]=$value['id_preference'];
		$arrBBDDIdPref[$value['id_preference']]=$value['field_form_name'];
	  }

	  // SE VERIFICA LA EVENTUAL ANTERIOR PARTICIAPCION DEL USUARIO PARA EVITAR DUPLICADOS
	  $sql='SELECT email FROM %s WHERE email="%s" LIMIT 1';
	  $sql=sprintf($sql,$table_customer,$emailIns);
	  $conexion->getResultSelectArray($sql);
	  $datosRespuesta=$conexion->tResultadoQuery;
	  $existEmail=count($datosRespuesta[0]) ? true : false;

	  if($existEmail)
	  {
		$msg_err = 'Parece que ya ha contestado e esta encuesta anteriormente, solo puede hacerlo una vez!';
	  }
	  else
	  {
		// SE INSERTA UN NUEVO USUARIO
		$sql='INSERT INTO %s (name, surname, cp, sex, email, birth_date,legal_basis) VALUES ("%s","%s","%d","%s","%s","%s","%d");';
		$sql=sprintf($sql,$table_customer,$nombreIns,$apellidosIns,$cpIns,$sexIns,$emailIns,$birth_dateIns,$legal);
		$conexion->ejecuta_query($sql);
		$idInserted = mysql_insert_id();
		$error = $idInserted ? 0 : 1; // SE EVALUA LA CORRECTA INSERCION DE LOS DATOS DEL NAVEGAMTE

		if($error)
		{
		  $msg_err = 'Se ha producido un error al introducir los datos, si lo desea, puede intentarlo más tarde.';
		}
		else
		{
		  //SE RECORREN TODAS LOS ELEMENTOS DE LAS PREFERENCIAS MOSTRADAS EN EL FORMULARIO
		  foreach($_REQUEST['data_preferences'] as $key=>$value)
		  {
			$sql='INSERT INTO %s (id_customer_inserted, id_preference_selected) VALUES ("%d","%d");';
			$sql=sprintf($sql,$table_customer_preferences,$idInserted,$arrBBDDFields[$key]);

			// SE ALMACENAN LAS PREFERENCIAS SELECCIONADAS EN EL FORMULARIO
			if((in_array($key,$arrBBDDIdPref) && $value))
			  $res=$conexion->ejecuta_query($sql);
		  }
		  $error=0;
		}
	  }
	}
}
else
{
  $msg_err = 'Falta rellenar el campo del correo electrónico. Error 106';
}

//echo $msg_err.'<pre> id=';print_r($datosRespuesta[0]);die();
$sOutput = array(
  'error' => intval($error),
  'msg' => $msg_err,
  'id' => $idInserted
);

$r = ejecutasegunversion('JSON',$sOutput);
echo $r;			  
?>