// JavaScript Document
//Tipos de ClickToCall. CTC==máxima prioridad. Lead==formulario grande
var array_typeofctc = ["CTC", "LEAD"];
//var cookiesEnabled = 0;

var whitespace = " \t\n\r";
var reWhitespace = /^\s+$/;

function MM_openBrWindow(theURL, winName, features) { //v2.0
    checkCookie();
    window.open(theURL, winName, features);
}

//Función que pone pixel por javascript
function ponerPixelJS(pix,idProcess){
    var s = document.createElement("script");
    s.type = "text/javascript";
	pix=pix.replace("[XXfeaturedparamXX]",idProcess);
    s.src = pix;
    // Use any selector
    $("head").append(s);
}


function __replaceall(msg, needle, reemp) {
    return msg.split(needle).join(reemp);
}

function _showModal(typ, cab, msg) {
    $.facebox.close();
    var alerta = (typeof is_movil !== undefined) && is_movil == 1;
    if (!alerta) {
        showModal({title: cab
                    , message: msg
                    , type: typ});//1 aler	
    } else { //para moviles
        msg = __replaceall(msg, '<br />', "\n");
        msg = __replaceall(msg, '<br>', "\n");
        msg = __replaceall(msg, '<strong>', '');
        msg = __replaceall(msg, '</strong>', '');
        msg = __replaceall(msg, '<p>&nbsp;</p>', '\n');

        msg = __replaceall(msg, '<p>', '');
        msg = __replaceall(msg, '</p>', '\n');
        alert(msg);
    }
}

function clearForm(){
	$.each($("input[type=hidden]"),function(nnn,tt){
	  $("#"+this.id).val("");
	  $("._"+$(this).attr("name").substr(5)).removeClass("elemSelected");
	  $("#"+$(this).attr("name").substr(5)).children("h3").removeClass("txtVisible");
//console.log($("#"+$(this).attr("name").substr(5)).children("h3").attr("id"))
	});

    $('#nombre').val('');
    $('#email').val('');
    $('#telefono').val('');
    $('#apellidos').val('');
    $('select option:selected').removeAttr('selected');
    $('#cp').val('');
    $('#dia_fech').val('');
    $('#mes_fech').val('');
    $('#ano_fech').val('');
	$("input[name=tratamiento]").attr("checked",false);
    $("#cblegales").attr("checked", false);
    if(is_ctc == '1')
        $("#telefono_ctc").val('');
}

function ComprobarInsercion(dat, ind) {
    var data = eval("(" + dat + ")");
    var msg = "";
    var b_is_movil = (typeof is_movil !== undefined) && is_movil == 1;
    var v_tduid = (typeof tduid !== undefined) ? tduid : '';
    var pix = "";
    if (data.error == 0) { //no ha habido error. Le estamos llamando.
        clearForm();
        //Ponemos el pixel dependiendo del click realizado
        //if (vardlfj)
        if(cookiesEnabled == 1) {

            if (arr_pixel_JS[0] != "") {
                ponerPixelJS(arr_pixel_JS[0],data.id);
            }
            if (arr_pixel_JS[1] != "") {
                ponerPixelJS(arr_pixel_JS[1],data.id);
            }
//            pix = '<img src=\"http://tbl.tradedoubler.com/report?organization=' + trdbl_organization + '&event=' + trbdl_event;

			// ESPECIFICO PARA AVENIDA-VIP
            pix = '<img src=\"http://tbl.tradedoubler.com/report?organization=1907742&event=307762';
            if (b_is_movil) { //añadimos el parámetro tduid a la url que salta
//                pix += '&tduid=' + v_tduid;
            }
            pix += '&leadNumber=' + data.id + '\"/>';
            $('body').append(pix);
            //PIxel estático de retargeting
            // pix = "http://wrap.tradedoubler.com/wrap?id=8498";
            //ponerPixelJS(pix);
            //Modificado by LFP 2013.11.19 for Mario
//            $('body').append('<img src="https://secure.adnxs.com/seg?add=1177391&t=2" width="1" height="1" />');
//            $('body').append('<img src="https://ad.yieldmanager.com/pixel?id=2441369&t=2" width="1" height="1" />');
//            $('body').append('<img src="https://reachandrich.antevenio.com/track/compginc.asp?exptime=90&keyword=SanitasDentalCliente=visita">');
        }
        var txt = "<p>Registrazione effettuata con <strong>successo</strong> in Corso vip.</p><p>Se non avete lasciato le vostre preferenze, fàtelo.</p><p>Quindi vi potremmo inviare vantaggi che siano di <strong>vostro interesse</strong>.</p><br /><p>Grazie per aver scelto CORSO VIP</p>";
        _showModal('success', 'I dati sono stati raccolti correttamente', txt);
    } else {
//console.log("data="+data.mensaje)
        msg = data.msg ? data.msg : "Si è verificato un errore durante l'elaborazione dei dati.<br />Per favore riprova più tardi.";
        _showModal('error', "Errore durante l'elaborazione dei dati", '<p>' + msg + '</p>');
    }
}



function isEmpty(s){
  return((s == null) || (s.length == 0));
}

function isWhitespace (s){
  return(isEmpty(s) || reWhitespace.test(s));
}

//función que validará el formulario grande
function validateForm(ind) {
    $.facebox.loading();
//	$.facebox.close();

//$(".selectetable").removeClass("elemSelected");
//	return;
    var ccrea = '';
    var nomb = '';
    var apell = '';
    var email = '';
    var genero = '';
//    var telf = '';
    var cp = '';
//    var prov = "";
    var error = 0;
    var msg = '';
//    var prov = $('#sel_prov').val();
    //modificado by LFP 2013.11.19 Mario
//    var prov_text = $('#sel_prov').find('option:selected').text();
    var legales = ($("#cblegales").is(':checked')) ? '1' : '0';
//console.log(nomb_promo)
    if (typeof nomb_promo === 'undefined') {
        ccrea = 'SANITAS_1';
    } else {
        ccrea = nomb_promo;
    }
    var source = '';
    if (typeof id_source === 'undefined') {
        source = '';
    } else {
        source = id_source;
    }
    nomb = $('#nombre').val();
//    apellidos = $('#apellidos').val();
    email = $('#email').val();
    genero = $("input[name=tratamiento]:checked").val();
    var apell = $('#apellidos').val();
//    telf = $('#telefono').val();
    cp = $('#cp').val();
//    if (ind == 0 || ind == 2) {
//        telf = $('#telefono_ctc').val();
//    }
//    prov = $('#sel_prov').val();

    //Validamos ahora dependiendo del parámetro ind de entrada.
    if(ind == 1) { //formulario largo
//if(nomb)alert("["+nomb+"]")
		if(!$('input[name=tratamiento]').is(':checked')){
//        if(nomb.length < 1 || nomb != 'Nombre y apellidos') {
            error = 1;
            msg += "<p>* È necessario indicare il vostro genere</p>";
            $('#nombre').focus();
        }
		if(nomb.length < 1 || isWhitespace(nomb) || nomb == 'Nombre y apellidos'){
//        if(nomb.length < 1 || nomb != 'Nombre y apellidos') {
            error = 1;
            msg += "<p>* È necessario compilare il campo Nome</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* Il campo Nome contiene caratteri non validi. Sono consentite solo lettere e spazi</p>";
            $('#nombre').focus();
        }
		if(apell.length < 1 || isWhitespace(apellidos) || apell == 'Apellidos'){
//        if(nomb.length < 1 || nomb != 'Nombre y apellidos') {
            error = 1;
            msg += "<p>* È necessario compilare il campo Nome</p>";
            $('#apellidos').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, apell))) {
            error = 1;
            msg += "<p>* Il campo Cognome contiene caratteri non validi. Sono consentite solo lettere e spazi</p>";
            $('#apellidos').focus();
        }
        /*if (apell.length<1) {error = 1;msg += "<p>* Debes rellenar el campo Apellidos</p>";$('#apell').focus();}
         else if (!(comprobarExprRegular(regexOnlyLetters,apell))) {error = 1;msg += "<p>* El campo Apellidos contiene caracters no válidos. Solo se permiten letras y espacios</p>";$('#apell').focus();}	
         */
        if (cp.length < 1) {
            error = 1;
            msg += "<p>* È necessario compilare il campo di codice postale</p>";
            $('#telefono').focus();
        }
        else if (cp.length > 5) {
            error = 1;
            msg += "<p>* Il codice postale può avere solo 5 cifre</p>";
            $('#telefono').focus();
        }
//        if (telf.length < 1) {
//            error = 1;
//            msg += "<p>* Debes rellenar el campo Teléfono</p>";
//            $('#telefono').focus();
//        }
//        else if (telf.length > 9) {
//            error = 1;
//            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
//            $('#telefono').focus();
//        }
//        else if (!(comprobarExprRegular(regExTelf, telf))) {
//            error = 1;
//            msg += "<p>* El número de teléfono solo admite 9 números sin espacio (p.ej 912345678 ó 612345678)</p>";
//            $('#telefono').focus();
//        }
//        if ((typeof is_prov !== 'undefined') && prov <= 1 && prov.length < 1) {
//        if (prov <= 0 || prov.length < 1) {
//            error = 1;
//            msg += "<p>* Debes seleccionar una Provincia</p>";
//            $('#sel_prov').focus();
//        }

        if (email.length < 1 || email == "Email"){
            error = 1;
            msg += "<p>* È necessario compilare il campo e-mail</p>";
            $('#email').focus();
        }
        else if(!(comprobarExprRegular(regExEmail, email))) {
            error = 1;
            msg += "<p>* Formato e-mail errata.</p>";
            $('#email').focus();
        }

        if (cp.length<1) {error = 1;msg += "<p>* È necessario compilare il campo CAP</p>";$('#cp').focus();}
         else if (cp.length>5) {error = 1;msg += "<p>* Il codice del CAP può che essere solamente di 5 cifre</p>";$('#cp').focus();}
         else if (!(comprobarExprRegular(regexCP,cp))) {error = 1;msg += "<p>* Il codice CAP consente solo un massimo di 5 numeri senza spazi (ad esempio 01000 o 38000)</p>";$('#cp').focus();}

        if (legales != '1') {
            error = 1;
            msg += "<p>*È necessario accettare l'informativa sulla privacy</p>";
        }
    }
//	else if (ind == 0) { //Formulario CTC
//        if (telf.length < 1) {
//            error = 1;
//            msg += "<p>* Debes rellenar el campo Teléfono</p>";
//            $('#telefono_ctc').focus();
//        }
//        else if (telf.length > 9) {
//            error = 1;
//            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
//            $('#telefono_ctc').focus();
//        }
//    }
	else if (ind == 2) { //Formulario especial con teléfono, nombre y Legales

        if (nomb.length < 1) {
            error = 1;
            msg += "<p>* È necessario compilare il campo Nome</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* Il campo Nome contiene caratteri non validi. Sono consentite solo lettere e spazi</p>";
            $('#nombre').focus();
        }
//        if (telf.length < 1) {
//            error = 1;
//            msg += "<p>* Debes rellenar el campo Teléfono</p>";
//            $('#telefono_ctc').focus();
//        }
//        else if (telf.length > 9) {
//            error = 1;
//            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
//            $('#telefono_ctc').focus();
//        }
        if (legales != '1') {
            error = 1;
            msg += "<p>*È necessario accettare l'informativa sulla privacy</p>";
        }
    } else if (ind == 3) { //Formulario especial con teléfono, nombre 
        //alert(ind);   
        if (nomb.length < 1) {
            error = 1;
            msg += "<p>* È necessario compilare il campo Nome</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* Il campo Nome contiene caratteri non validi. Sono consentite solo lettere e spazi</p>";
            $('#nombre').focus();
        }
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* È necessario compilare il campo del telefono</p>";
            $('#telefono').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* Il numero di telefono può essere composto solo di 9 cifre</p>";
            $('#telefono').focus();
        }
        //if (legales != '1') {error = 1; msg += "<p>*Debes aceptar la Política de Privacidad</p>";}
    }
    if (error != 0) {

        _showModal('error', 'Errore nei dati di input', msg);
        //$.facebox.close();
        //showModal({title:'Error en los Datos Introducidos',message:msg,type:'success'});
    } else {//Mandamos.
        //modificado por LFP 2013.11.26.
        //Se requiere mandar como si fuese un LEAD (ind==1) y no como un CTC (ind==0)
        if (ind==3) {ind = 1;} 
        if (ind >= 2) {
            ind = 0;
        }
        var valortype = array_typeofctc[ind];
        var campana = arr_campanas[ind];
        //$.post(url_ws+file_ws,{idclient:id_client,sourcetype:valortype,campaign:campana,fuente:source,nombre:nomb,em:email,telefono:telf,provincia:prov,cblegales:legales,crea:ccrea},function(data){ComprobarInsercion(data,ind);},'');	


        $.post(root_path_local + "ajaxs/procesar_registro_ctc.php",{
//        $.post("../"+root_path_local+"ajaxs/procesar_registro_ctc.php",{
		  idclient:id_client,
		  sourcetype:valortype,
		  campaign:campana,
		  fuente:source,
		  genero:genero,
		  em:email,
		  nombre:nomb,
		  apellidos:apell,
		  cp:cp,
		  cblegales:legales,
		  birth_date:$("#ano_fech").val()+"-"+$("#mes_fech").val()+"-"+$("#dia_fech").val(),
		  crea:ccrea,
		  data_preferences:{
			salu:$("#data_salu").val(),
			moda:$("#data_moda").val(),
			viaj:$("#data_viaj").val(),
			form:$("#data_form").val(),
			banc:$("#data_banc").val(),
			moto:$("#data_moto").val(),
			tele:$("#data_tele").val(),
			segu:$("#data_segu").val(),
			ener:$("#data_ener").val()
		  }
		}, function(data){
            ComprobarInsercion(data, ind);
        }, '');	//,em:email
    }

}
/**
 * Función que comprobará o gestionará la cookie dentro del formulario
 */
function checkCookie() {
    //alert("hey");
    if (cookiesManaged == 0) {
        timer.stop();
        manageCookies(1);
    }
}

$(document).ready(function() {
    $("body").append("<div id='opaque' style='display: none;'></div>");
    $('#telefono').keypress(function(e) {
        return SoloCompatibleTlf(e);
    });
    //Política de cookies
    $(':input').focusin(function() {
        if($(this).attr('id') == "cookie_rejection" || $(this).attr('id') == "cookie_acept")
		  return false;
        checkCookie();
    });
    $(':checkbox').click(function(e) {
        checkCookie();
    });
    if (is_ctc == '1') {
        $("#telefono_ctc").keypress(function(e) {
            return SoloCompatibleTlf(e);
        });
    }
    $('#btnProcesar').click(function(e) {
        e.preventDefault();
        validateForm(1);

    }); //btnprocesar
    $('#btnProcesar_ctc').click(function(e) {
        e.preventDefault();
        validateForm(0);

    }); //btnProcesar_ctc
    /**
     * Nombre y teléfono
     */
    $('#btnProcesar_ctc_movil').click(function(e) {
        e.preventDefault();
        validateForm(3);

    });
    /*$('#btnProcesar_ctc_resp').click(function(e){
     e.preventDefault();
     validateForm(2);				 
     
     }); //btnProcesar_ctc
     */

});

var regExTelf = /([9|6|7|8])+[0-9]{8}/;// /^[6-7]{1}\d{8}$/
var regexOnlyLetters = /^[ñÑáÁéÉíÍóÓúÚçÇa-zA-Z\ \']+$/;
var regexCP = /^[0-9]{4,5}/;
var regExEmail = /^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/;	