<?php
?>
<div class="mainContent" style="display:block;position:relative;padding-bottom:<?php echo @$padd; ?>px;">
<div id="contenedor_centrado">
<section class="contenido">
	<section class="grupo">
        <header>

            <figure class="cabecera1"><img src="<?=$path_raiz_aplicacion_local?>img/cabecera.png"/></figure>
			<figure class="cabecera2"><img src="<?=$path_raiz_aplicacion_local?>img/cabecera2.png"/></figure>

        </header>
    	<div class="texto">Selecciona tus preferencias y regístrate</div>
        <div class="fotos">
          <a id="salu" href="#0"><img class="selectetable _salu" src="<?=$path_raiz_aplicacion_local?>img/preferences/salu.jpg"/><h3>Salud y Belleza</h3></a>
          <a id="moda" href="#0"><img class="selectetable _moda" src="<?=$path_raiz_aplicacion_local?>img/preferences/moda.jpg"/><h3>Moda</h3></a>
          <a id="viaj" href="#0"><img class="selectetable _viaj" src="<?=$path_raiz_aplicacion_local?>img/preferences/viaj.jpg"/><h3>Viajes</h3></a>
          <a id="form" href="#0"><img class="selectetable _form" src="<?=$path_raiz_aplicacion_local?>img/preferences/form.jpg"/><h3>Formación</h3></a>
          <a id="banc" href="#0"><img class="selectetable _banc" src="<?=$path_raiz_aplicacion_local?>img/preferences/banc.jpg"/><h3>Banca</h3></a>
          <a id="moto" href="#0"><img class="selectetable _moto" src="<?=$path_raiz_aplicacion_local?>img/preferences/moto.jpg"/><h3>Motor</h3></a>
          <a id="tele" href="#0"><img class="selectetable _tele" src="<?=$path_raiz_aplicacion_local?>img/preferences/tele.jpg"/><h3>Telecomunicaciones</h3></a>
          <a id="segu" href="#0"><img class="selectetable _segu" src="<?=$path_raiz_aplicacion_local?>img/preferences/segu.jpg"/><h3>Seguros</h3></a>
          <a id="ener" href="#0"><img class="selectetable _ener" src="<?=$path_raiz_aplicacion_local?>img/preferences/ener.jpg"/><h3>Energía</h3></a>
        </div>

        <div class="formulario">
        <h1>Introduce tus datos</h1>

        <form action="" method="get">
		  <input type="hidden" name="data_salu" id="data_salu" value="0" />
		  <input type="hidden" name="data_moda" id="data_moda" value="0" />
		  <input type="hidden" name="data_viaj" id="data_viaj" value="0" />
		  <input type="hidden" name="data_form" id="data_form" value="0" />
		  <input type="hidden" name="data_banc" id="data_banc" value="0" />
		  <input type="hidden" name="data_moto" id="data_moto" value="0" />
		  <input type="hidden" name="data_tele" id="data_tele" value="0" />
		  <input type="hidden" name="data_segu" id="data_segu" value="0" />
		  <input type="hidden" name="data_ener" id="data_ener" value="0" />
                    <div  class="fila">
                        <input name="tratamiento" type="radio" value="H" class="cookiesEnable">Sr.<input name="tratamiento" type="radio" value="M" class="cookiesEnable">Sra.</div>
                    <div class="fila">
                        <div class="fleft">Nombre<input type="text" class="celda" id="nombre" name="nombre" maxlength="499" value="<?=$nombre?>"></div>
                    </div>
                    <div class="fila">
                        <div class="fleft">Apellido<input type="text" class="celda" id="apellidos" name="apellidos" maxlength="499" value="<?=$apellidos?>"></div>
                    </div>
                    <div class="fila">
                        <div class="fleft">E-mail<input type="email" class="celda" name="em" id="email" value="Email" onBlur="this.value=(this.value==&#39;&#39;)?(&#39;Email&#39;):(this.value)" onClick="this.value=(this.value==&#39;Email&#39;)?(&#39;&#39;):(this.value)"></div>
                    </div>
                    <div class="fecha_nacimiento">
                    	<div id="fecha_nombre">Fecha de Nacimiento</div>
                        <div id="capa_fecha">
	                        <select name="dia_fech" id="dia_fech" class="f_dia cookiesEnable">
<?php
$outDay='';
for($day=1;$day < 32;$day++)
{
  $outDay.='<option value="'.$day.'"';
  $outDay.='';
  $outDay.='>'.str_pad($day,2,'0',STR_PAD_LEFT).'</option>';
}
echo $outDay;
?>
							</select>
	                        <select name="mes_fech" id="mes_fech" class="f_mes cookiesEnable">
	                      		<option value="01">Enero</option><option value="02" selected="selected">Febrero</option><option value="03">Marzo</option><option value="04">Abril</option><option value="05">Mayo</option><option value="06">Junio</option><option value="07">Julio</option><option value="08">Agosto</option><option value="09">Septiembre</option><option value="10">Octubre</option><option value="11">Noviembre</option><option value="12">Diciembre</option>	                  		</select>
	                        <select name="ano_fech" id="ano_fech" class="f_ano cookiesEnable">
<?php
$outYear='';
$startYear=date('Y')-18;
for($yearX=$startYear;$yearX > 1899;$yearX--)
{
  $outYear.='<option value="'.$yearX.'"';
  $outYear.='';
  $outYear.='>'.$yearX.'</option>';
}
echo $outYear;
?>
							</select>
                   		</div>
                     </div> 
                     <div class="fila">
                        <div class="fleft">Código Postal<input type="number" class="celda" id="cp" name="cp" maxlength="5" value=""></div>
                    </div>
                   
                 <div class="legal">
                    <input type="checkbox" id="cblegales" value="1" name="cblegales">  He leído y acepto la <a class="enlace_pp" href="<?=$path_raiz_aplicacion_local?>privacy_policy.php" target="_blank">política de privacidad</a>
                 </div>
			  <input name="btnProcesar" id="btnProcesar" value="REGISTRATE" class="green" type="button" />
            </form>

        </div>
	</section>
</section>

<footer class="footerLegalPolicy">
AVISO LEGAL<br />En cumplimiento de lo establecido en la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico, se informa de los datos de información general de este sitio web, www.avenidavip.com. Responsable por una parte, el proveedor de los bienes o servicios contratados por el usuario es NETSALES FACTORY SLU., <br/>a través de la marca AvenidaVip, con domicilio social en Madrid, palacio de la prensa , plaza de callo nº 4, 6ª planta , 28013 Madrid, inscrita en el Registro Mercantil de Madrid, tomo 28116, sección 8ª, folio 132, con C.I.F B86049400 y con teléfono de atención al cliente 91 110 79 10, y dirección de correo electrónico info@netsalesfactory.com. 
</footer>

 </div>
 </div>

