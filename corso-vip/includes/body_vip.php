<?php
?>

<div class="mainContent" style="display:block;position:relative;padding-bottom:<?php echo @$padd; ?>px;">
<div id="contenedor_centrado">
<section class="contenido">
	<section class="grupo">
        <header>

            <figure class="cabecera1"><img src="<?=$path_raiz_aplicacion_local?>img/cabecera.png"/></figure>
			<figure class="cabecera2"><img  src="<?=$path_raiz_aplicacion_local?>img/cabecera2.png"/></figure>

        </header>
    	<div class="texto">Seleziona le tue preferenze e registrati</div>
        <div class="fotos cookiesEnable">
          <a id="salu" href="#0"><img class="selectetable _salu" src="<?=$path_raiz_aplicacion_local?>img/preferences/salu.jpg" border="0" /><h3>Salute e bellezza</h3></a>
          <a id="moda" href="#0"><img class="selectetable _moda" src="<?=$path_raiz_aplicacion_local?>img/preferences/moda.jpg" border="0" /><h3>Moda</h3></a>
          <a id="viaj" href="#0"><img class="selectetable _viaj" src="<?=$path_raiz_aplicacion_local?>img/preferences/viaj.jpg" border="0" /><h3>Viaggi</h3></a>
          <a id="form" href="#0"><img class="selectetable _form" src="<?=$path_raiz_aplicacion_local?>img/preferences/form.jpg" border="0" /><h3>Formazione</h3></a>
          <a id="banc" href="#0"><img class="selectetable _banc" src="<?=$path_raiz_aplicacion_local?>img/preferences/banc.jpg" border="0" /><h3>Finanza</h3></a>
          <a id="moto" href="#0"><img class="selectetable _moto" src="<?=$path_raiz_aplicacion_local?>img/preferences/moto.jpg" border="0" /><h3>Motori</h3></a>
          <a id="tele" href="#0"><img class="selectetable _tele" src="<?=$path_raiz_aplicacion_local?>img/preferences/tele.jpg" border="0" /><h3>Telecomunicazioni</h3></a>
          <a id="segu" href="#0"><img class="selectetable _segu" src="<?=$path_raiz_aplicacion_local?>img/preferences/segu.jpg" border="0" /><h3>Assicurazioni</h3></a>
          <a id="ener" href="#0"><img class="selectetable _ener" src="<?=$path_raiz_aplicacion_local?>img/preferences/ener.jpg" border="0" /><h3>Energia</h3></a>
        </div>

        <div class="formulario">
        <h1>Introduci i tuoi dati</h1>

        <form action="" method="get">
		  <input type="hidden" name="data_salu" id="data_salu" value="0" />
		  <input type="hidden" name="data_moda" id="data_moda" value="0" />
		  <input type="hidden" name="data_viaj" id="data_viaj" value="0" />
		  <input type="hidden" name="data_form" id="data_form" value="0" />
		  <input type="hidden" name="data_banc" id="data_banc" value="0" />
		  <input type="hidden" name="data_moto" id="data_moto" value="0" />
		  <input type="hidden" name="data_tele" id="data_tele" value="0" />
		  <input type="hidden" name="data_segu" id="data_segu" value="0" />
		  <input type="hidden" name="data_ener" id="data_ener" value="0" />
				  <div  class="fila">
					  <input name="tratamiento" type="radio" value="H" class="cookiesEnable">Sig.<input name="tratamiento" type="radio" value="M" class="cookiesEnable">Sig.ra
				  </div>
				  <div class="fila">
					  <div class="fleft">Nome<input type="text" class="celda" id="nombre" name="nombre" maxlength="499" value="<?=$nombre?>"></div>
				  </div>
				  <div class="fila">
					  <div class="fleft">Cognome<input type="text" class="celda" id="apellidos" name="apellidos" maxlength="499" value="<?=$apellidos?>"></div>
				  </div>
				  <div class="fila">
					  <div class="fleft">E-mail<input type="text" class="celda" name="em" id="email" value="<?=$email?>" onBlur="this.value=(this.value==&#39;&#39;)?(&#39;Email&#39;):(this.value)" onClick="this.value=(this.value==&#39;Email&#39;)?(&#39;&#39;):(this.value)"></div>
				  </div>
				  <div class="fecha_nacimiento">
					  <div id="fecha_nombre">Data di nascita</div>
					  <div id="capa_fecha">
						  <select name="dia_fech" id="dia_fech" class="f_dia cookiesEnable">
<?php
$outDay='';
for($day=1;$day < 32;$day++)
{
$outDay.='<option value="'.str_pad($day,2,'0',STR_PAD_LEFT).'"';
$outDay.='';
$outDay.='>'.str_pad($day,2,'0',STR_PAD_LEFT).'</option>';
}
echo $outDay;
?>
						  </select>
						  <select name="mes_fech" id="mes_fech" class="f_mes cookiesEnable">
								<option value="01">Gennaio</option><option value="02" selected="selected">Febbraio</option><option value="03">Marzo</option><option value="04">Aprile</option><option value="05">Maggio</option><option value="06">Giugno</option><option value="07">Luglio</option><option value="08">Agosto</option><option value="09">Settembre</option><option value="10">Ottobre</option><option value="11">Novembre</option><option value="12">Dicembre</option>
							</select>
						  <select name="ano_fech" id="ano_fech" class="f_ano cookiesEnable">
<?php
$outYear='';
$startYear=date('Y')-18;
for($yearX=$startYear;$yearX > 1899;$yearX--)
{
$outYear.='<option value="'.$yearX.'"';
$outYear.='';
$outYear.='>'.$yearX.'</option>';
}
echo $outYear;
?>
						  </select>
					  </div>
				   </div> 
				   <div class="fila">
					  <div class="fleft">Codice postale<input type="text" class="celda" id="cp" name="cp" maxlength="5" value=""></div>
				  </div>
                   
                 <div class="legal">
                    <input type="checkbox" id="cblegales" value="1" name="cblegales">  Ho letto e accetto <a class="modalMain" href="#0">la politica delle leggi sulla privacy</a>
					<!--<input type="checkbox" id="cblegales" value="1" name="cblegales">  He leído y acepto la <a class="enlace_pp" href="politica.html" target="_blank">política de privacidad</a>-->
                 </div>
			  <input name="btnProcesar" id="btnProcesar" value="Registrati" class="green" type="button" />
            </form>

        </div>
	</section>
</section>
 </div>
 </div>

<footer>
</footer>
