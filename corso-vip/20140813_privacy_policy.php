<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Privacy Policy</title>
<?php
if($_REQUEST['deviceType'] == 'mob_'){
?>
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=2.0; user-scalable=yes;">
<meta http-equiv="Pragma" content="no-cache">
<?php
}
?>
<div style="display:none;"><?=$_REQUEST['deviceType']?></div>
<style>
<?php
if($_REQUEST['deviceType'] == 'mob_')
{
  echo '
.privPolData{
font-size:.7em;
background-color:#fff!important;
font-family:Arial, Helvetica, sans-serif, Gadget, sans-serif!important;
color:#000!important;
}

';
}
else
{
  echo '
p{font-size:.8em;}
.privPolData{
background-color:#fff!important;
font-family:Arial, Helvetica, sans-serif, Gadget, sans-serif!important;
color:#000000;
}
';
}
?>

</style>
</head>

<body class="privPolData">

<h5>POLÍTICA DE PRIVACIDAD Y PROTECCIÓN DE DATOS DE NET SALES</h5>
<?php
if($_REQUEST['deviceType'] != 'mob_'){
?>
<div><img src="http://www.comon-line.es/redaccion/images/politica-privacidad/netsales_logo.jpg" width="120" height="76"  border="0"/></div>
<?php
}
?>

<h5>1. PRESENTACIÓN Y DERECHO DE INFORMACIÓN</h5>

<p><br />
NETSALES FACTORY S.L.U. (en adelante, NETSALES FACTORY) se toma muy en serio la protección de su privacidad y de sus datos personales. Por lo tanto, su información personal se conserva de forma segura y se trata con el máximo cuidado. La presente política de protección de datos regula el acceso y el uso del servicio del de NETSALES FACTORY, con domicilio en el Palacio de la Prensa, Plaza de Callao 4, 6º, 28013 Madrid y con CIF número B-86049400, pone a disposición de los Usuarios (en adelante, los “Usuarios” o el “Usuario”) de Internet interesados en los servicios (en adelante, los “Servicios”) y contenidos (en adelante, los “Contenidos”) alojados en el Sitio Web. Se entiende que el Usuario acepta expresamente la presente Política de Privacidad y Protección de datos una vez que cumplimente el formulario de registro del Sitio Web haciendo click en la palabra “CONTINUAR” o “ENVIAR”.

<h5>2. RECOMENDACIONES</h5>

<p>
  Por favor lea detenidamente y siga las siguientes recomendaciones: <br />

 - Utilice contraseñas con una extensión mínima de 8 caracteres, alfanuméricos y con uso de mayúsculas y minúsculas. <br />

 - El Sitio Web no está dirigido a menores de edad. Por favor active el control parental para prevenir y controlar el acceso de menores de edad a Internet e informar a los menores de edad sobre aspectos relativos a la seguridad. <br />

 - Mantenga en su equipo un software antivirus instalado y debidamente actualizado, que garantice que su equipo se encuentra libre de software maligno, así como de aplicaciones spyware que pongan en riesgo su navegación en Internet, y en peligro la información alojada en el equipo. <br />

 - Revise y lea las condiciones generales de uso y la política de privacidad que la plataforma pone a su disposición en el Sitio Web. <br />

</p>
 <h5>3. PROCESAMIENTO DE DATOS PERSONALES, FINALIDAD DEL TRATAMIENTO Y CONSENTIMIENTO PARA LA CESIÓN DE DATOS</h5>
 
  <p>Los datos de los Usuarios que se recaban a través de los formularios de registro online disponibles en el Sitio Web, son recabados por NETSALES FACTORY con la finalidad de poder prestarles los Servicios ofrecidos a través del Sitio Web. Asimismo, le informamos de que sus datos personales de contacto (nombre, apellidos, teléfono móvil, dirección, empresa, cargo, sitio web, dirección de correo electrónico, etc.) serán incorporados a un fichero automatizado y utilizados para remitirle Newsletters y comunicaciones comerciales y promocionales relacionadas con los servicios de NETSALES FACTORY por carta, teléfono, correo electrónico, SMS/ MMS, o por otros medios de comunicación electrónica equivalentes y ello al amparo de lo establecido en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (en adelante, LOPD), en la Ley 34/2002 de 11 de Julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico y en la Ley 32/ 2003 de 3 de Noviembre, General de Telecomunicaciones.
El usuario, una vez cumplimente el formulario de registro del Sitio Web, consiente expresamente que sus datos (nombre, apellidos, dirección postal y datos de geolocalización) puedan ser utilizados por empresas pertenecientes al Grupo empresarial de NETSALES FACTORY (y en particular a las sociedades pertenecientes al grupo FAST TRACK NETWORK SL, tales como: FENIX DATA S.L.U.; MAXIDAT S.L.U.; DUJOK ICONSULTING S.L.; FINTEL TELEMARKETING S.L.U.; KARPE DEAL S.L., PICARISIMO S.L.) o cedidos (incluso para depurar datos personales) a otras entidades españolas o de la Unión Europea para enviarle, por carta, teléfono, correo electrónico, SMS/MMS, o por otros medios de comunicación electrónica equivalentes, a través nuestro o de otras entidades, publicidad y ofertas comerciales y de servicios relacionados con los sectores de:<br /><BR/>
    
    
- Marketing o pertenecientes a la Federación Española de Comercio Electrónico y Marketing Directo (FECEMD)   <br />
- Telecomunicaciones: Productos y Servicios de telecomunicaciones y tecnología.   <br />
- Financiero: Prestados por entidades financieras, Aseguradoras y de Previsión social.   <br />
- Ocio: Editorial, Turismo, Deportes, Coleccionismo, Fotografía, Pasatiempos, Juguetería, Transporte, Jardinería, Hobbies, Loterías, peñas de loterías, Comunicación y entretenimiento.   <br />
- Gran consumo: Electrónica, Informática, Textil, Imagen y Sonido, Complementos, Hogar, Bazar, Cuidado personal (Cosmética, Perfumería, Parafarmacia, especialidades Farmacéuticas publicitarias) Mobiliario, Inmobiliario, Alimentación y Bebidas, salud y belleza, Material de oficina. Moda y decoración.   <br />
- Automoción: Productos y Servicios relacionados con el Automóvil, Motocicletas y Camiones.   <br />
- Energía y agua: Productos relacionados con la Electricidad, Hidrocarburos, Gas y Agua.   <br />
- ONG: Productos y Servicios relacionados con ONG.   <br />

  </p>
 
  <p>
  El usuario acepta y consiente que NETSALES FACTORY trate sus Datos Personales y consulte ficheros de terceras entidades con la finalidad de determinar su perfil y ofrecerle productos y servicios adecuados, propios o de terceras empresas pertenecientes a los sectores anteriormente relacionados. El Usuario acepta expresamente la presente Política de Privacidad y otorga su consentimiento expreso al tratamiento automatizado de los datos personales facilitados. No obstante, el Usuario podrá revocar el consentimiento, en cada comunicado comercial o publicitario que se le haga llegar, y en cualquier momento, mediante notificación en la siguiente dirección de correo electrónico info@netsalesfactory.com o mediante carta dirigida a NETSALES FACTORY domiciliada en el Palacio de la Prensa, Plaza de Callao 4, 6º, 28013 Madrid. <br />
  </p>
  <h5>4. CARÁCTER OBLIGATORIO O FACULTATIVO DE LA INFORMACIÓN FACILITADA POR EL USUARIO Y VERACIDAD DE LOS DATOS</h5>
  
  <p>El Usuario garantiza que los datos personales facilitados son veraces y se hace responsable de comunicar a NETSALES FACTORY cualquier modificación de los mismos. El Usuario responderá, en cualquier caso, de la veracidad de los datos facilitados, NETSALES FACTORY el derecho a excluir de los servicios registrados a todo Usuario que haya facilitado datos falsos, sin perjuicio de la demás acciones que procedan en Derecho. Se recomienda tener la máxima diligencia en materia de Protección de Datos mediante la utilización de herramientas de seguridad, no pudiéndose responsabilizar a NETSALES FACTORY de sustracciones, modificaciones o pérdidas de datos ilícitas.
 </p>
  
  
   <h5>5. DERECHOS DE ACCESO, RECTIFICACIÓN, CANCELACIÓN Y OPOSICIÓN</h5>
  
  <p>
 Le recordamos que, en el momento que lo desee, pueda ejercitar sus derechos de acceso, rectificación, cancelación y oposición o de oposición al tratamiento general o con fines promocionales de sus datos personales enviándonos una comunicación gratuita a la dirección de correo electrónico info@netsalesfactory.com indicando la referencia:“NETSALES-Ejercicio derechos LOPD/LSSI]) que deberá contener: su nombre y apellidos, fotocopia de su DNI (pasaporte u otro documento válido que lo identifique), petición en que se concreta la solicitud, dirección a efectos de notificaciones, fecha, firma y documentos acreditativos de la petición que formula, en su caso. <br />
  </p>
  
  
    <h5>6. USO DE COOKIES</h5>
  
  <p>
   De conformidad con el Real Decreto-Ley 13/2012, de 30 de marzo, que entró en vigor el 1 de abril de 2012, NETSALES FACTORY ha contratado un servicio que posibilita al usuario que accede al Sitio Web de NETSALES FACTORY aceptar o rechazar que NETSALES FACTORY utilice cookies en su navegador.<br/>
Mediante la aceptación de la presente Política de Privacidad, el Usuario acepta los servicios de retargeting que se podrán llevar a cabo mediante el envío de comunicaciones comerciales. El Usuario consiente que dichas comunicaciones comerciales contengan dispositivos de almacenamiento o cookies de publicidad que se instalarán en su navegador.<br/>
Estos servicios de retargeting tienen como finalidad proporcionar más información sobre productos o servicios que puedan interesar al Usuario mediante:<br/>
-La instalación de dispositivos de almacenamiento y recuperación de datos o cookies en equipos terminales en algunos de los correos electrónicos enviados a los usuarios. <br/>
-Envío de correos electrónicos con comunicaciones comerciales a los que se haya instalado las Cookies mediante la visita de una página web o mediante la instalación a través de correo electrónico. Además, en cada comunicación comercial que contenga cookies, se informará al usuario de qué tipo de cookies contiene y cómo desactivarlas.<br/>
En todo caso, el usuario, mediante la aceptación de la política de protección de datos y de privacidad del Sitio Web, salvo mención expresa en contrario por el usuario, acepta expresamente que NETSALES FACTORY pueda utilizar las cookies. Aun así, el usuario tiene la posibilidad de configurar su navegador para ser avisado de la recepción de cookies y para impedir su instalación en su equipo. Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta información. Puede obtener más información acerca del funcionamiento de las cookies en http://www.youronlinechoices.com/es/ <br/>
Las cookies son ficheros enviados al navegador por medio de un servidor web con la finalidad de registrar las actividades del usuario durante su tiempo de navegación. Las cookies utilizadas por el sitio web se asocian únicamente con un usuario anónimo y su ordenador, y no proporcionan por sí mismas los datos personales del usuario. Mediante el uso de las cookies resulta posible que el servidor donde se encuentra la web, reconozca el navegador web utilizado por el usuario con la finalidad de que la navegación sea más sencilla, permitiendo, por ejemplo, el acceso a los usuarios que se hayan registrado previamente, acceder a las áreas, servicios, promociones o concursos reservados exclusivamente a ellos sin tener que registrarse en cada visita. Se utilizan también para medir la audiencia y parámetros del tráfico, controlar el progreso y número de entradas.<br/>
Para utilizar el sitio web, no resulta necesario que el usuario permita la instalación de las cookies enviadas por el sitio web, o el tercero que actúe en su nombre, sin perjuicio de que sea necesario que el usuario inicie una sesión como tal en cada uno de los servicios cuya prestación requiera el previo registro o “login”.<br/>


  </p>
  
  
   <h5>7.- MEDIDAS DE SEGURIDAD</h5>
  
  <p>
  NETSALES FACTORY mantiene los niveles de seguridad de protección de datos personales conforme a la LOPD y al Real Decreto 1720/2007, de 21 de diciembre, por el que se aprueba el Reglamento de desarrollo de la LOPD que contengan datos de carácter personal y ha establecido todos los medios técnicos a su alcance para evitar la pérdida, mal uso, alteración, acceso no autorizado y robo de los datos que el usuario facilite a través del Sitio Web, sin perjuicio de informarle de que las medidas de seguridad en Internet no son inexpugnables. NETSALES FACTORY se compromete a cumplir con el deber de secreto y confidencialidad respecto de los datos personales contenidos en el fichero automatizado de acuerdo con la legislación aplicable, así como a conferirles un tratamiento seguro en las cesiones que, en su caso, puedan producirse. 



 
<br />
  </p>
  
  <h5>8.- LINKS A PÁGINAS WEB</h5>
  
  <p>
  El Sitio Web de NETSALES FACTORY podría contener links a páginas web de compañías y entidades de terceros. NETSALES FACTORY no puede hacerse responsable de la forma en la que estas compañías tratan la protección de la privacidad y de los datos personales, por lo que le aconsejamos que lea detenidamente las declaraciones de política de privacidad de éstas páginas web que no son propiedad de NETSALES FACTORY con relación al uso, procesamiento y protección de datos personales. Las condiciones que ofrecen éstas páginas web pueden no ser las mismas que las que ofrece NETSALES FACTORY 

<br />
  </p>
  
  
   
   <h5>9. PREGUNTAS
</h5>
  
  <p>Si tiene alguna pregunta sobre esta Política de Privacidad, rogamos que se ponga en contacto con nosotros enviando un email a info@netsalesfactory.com. <br />
  </p>
  
  
   <h5>10. CAMBIOS
</h5>
  
  <p>NETSALES FACTORY se reserva el derecho de revisar su Política de Privacidad en el momento que lo considere oportuno. Por esta razón, le rogamos que compruebe de forma regular esta declaración de privacidad para leer la versión más reciente de la política de privacidad de NETSALES FACTORY.<br />
  </p>
  
  
   <h5>11. CONDICIONES PARTICULARES </h5>
  
  <p>El acceso a determinados Contenidos ofrecidos a través del Sitio Web puede encontrarse sometido a ciertas condiciones particulares propias que, según los casos, sustituyen, completan las modifican las condiciones generales. Por tanto, con anterioridad al acceso y/o utilización de dichos Contenidos, el Usuario ha de leer atentamente también las correspondientes condiciones particulares. <br />
  </p>
  
</body>
</html>
