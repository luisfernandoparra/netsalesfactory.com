<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Privacy Policy</title>
<?php
if($_REQUEST['deviceType'] == 'mob_'){
?>
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=2.0; user-scalable=yes;">
<meta http-equiv="Pragma" content="no-cache">
<?php
}
?>
<div style="display:none;"><?=$_REQUEST['deviceType']?></div>
<style>
<?php
if($_REQUEST['deviceType'] == 'mob_')
{
  echo '
.privPolData{
font-size:.7em;
background-color:#fff!important;
font-family:Arial, Helvetica, sans-serif, Gadget, sans-serif!important;
color:#000!important;
}

';
}
else
{
  echo '
p{font-size:.8em;}
.privPolData{
background-color:#fff!important;
font-family:Arial, Helvetica, sans-serif, Gadget, sans-serif!important;
color:#000000;
}
';
}
?>

</style>
</head>

<body class="privPolData">

<h1 align="center">PRIVACY POLICY </h1>
<?php
if($_REQUEST['deviceType'] != 'mob_'){
?>
<div></div>
<?php
}
?>


<h2>1. INFORMAZIONE </h2>

<p>NETSALES FACTORY S.L.U. (in appresso, NETSALES FACTORY) valuta con estrema attenzione la protezione della privacy Pertanto la sua informazione personale si conserva di forma sicura e si tratta con la massima attenzione. La presente privacy policy stabilisce i termini d'accesso sul sito web http://www.corsovip.com/ (in appresso, il Sito Web) e l'uso del servizidegli usi dei servizi (in appresso, i "Servizi") disponibili attraverso questo sito web titolarità della società NETSALES FACTORY, con sede in "Palacio de la Prensa", Piazza di Callao 4, 6º, 28013 Madrid (Spagna) e con CIF numero B-86049400.</p>

<p>Resta inteso che l'utente (in appresso, gli "Utenti" o l' "Utente") accetta espressamente la Privacy Policy facendo click nella parola “CONTINUARE" o "INVIARE" completando il modulo di registrazione del Sito Web. </p>

<h2>2. PRIVACY E PROTEZIONE DEI DATI PERSONALI</h2>

<p>Si prega di notare che NETSALES tratta tutti i dati personali ai sensi della Legge 15/1999, di 13 dicembre, sulla protezione dei dati personali (LOPD) e il Real Decreto 1720/2007, di 21 dicembre, che approva il Regolamento di attuazione della Legge 15/1999 del 13 dicembre sulla protezione dei dati personali (RLOPD), e così stesso di conformità con le disposizione della Legge 34/2002, di 11 luglio, di Servizi della Società dell’Informazione (LSSI) e della Legge 9/2014, di 9 maggio, Generale di Telecomunicazione (LGT). I dati personali sono trattati di conformità con questa normativa, adottando le misure tecniche, organizzative e di sicurezza necessarie per garantire la confidenzialità dei dati e per evitare la sua alterazione, perdita, e trattamento e acceso non autorizzato.</p>

<p>I dati personali da Lei forniti a noi attraverso i nostri moduli saranno registrati e inclusi inun file titolarità di NETSALES, che sarà correttamente iscritto nel Registro Generale dell'Agenzia Spagnola per la Protezione dei Dati, che si potrà consultare tramite il Sito  Web della Agenzia spagnola per la protezione dei dati www.agpd.es.</p>

<h2>3. RACCOMANDAZIONI</h2>

<p>Si prega di leggere attentamente e seguire le seguenti raccomandazioni:</p>
<p> - Utilizzare password con una lunghezza minima di 8 caratteri alfanumerici, facendo attenzione all'uso dei caratteri maiuscoli e minuscoli.  </p>
<p>- Il Sito non è destinato ai minori. Si prega di esercitare il controllo parentale per prevenire e controllare l'accesso dei minori a Internet ed informare i minori riguardo gli aspetti di sicurezza.</p>
<p> - Tenere il computer con un software antivirus installato e correttamente aggiornato, per garantire cosí che lo stesso computer sia libero di malware e applicazioni spyware che potrebbero compromettere la vostra navigazione in Internet, e mettere in pericolo le informazioni ospitate sul computer.  </p>
<p>- Controllare e leggere le condizioni generali di utilizzo e la privacy policy offerte disponibile sul sito web. </p>

<h2>4. TRATTAMENTO DEI DATI PERSONALI, FINALITÀ INFORMATICA E CONSENSO AL TRASFERIMENTO DEI DATI</h2>

<h3>4.1 Descrizione del trattamento dei dati</h3>

<p>L'utente, una volta completato il modulo di registrazione sullo Sito Web, acconsente espressamente che i suoi dati (nome, indirizzo e dati di geolocalizzazione) possano essere utilizzati per NETSALES con i seguenti obiettivi:</p>

<p>(i) la prestazione di Servizi di pubblicità per la remissione al Utente attraverso qualsiasi medio, anche attraverso telefono, e-mail, SMS / MMS o altri mezzi elettronici, pubblicità ed offerte commerciali e di servizi correlati nei settori di:

- Telecomunicazioni: Prodotti e servizi di telecomunicazioni e la tecnologia.
- Finanziaria: In prestito da istituti finanziari, assicurativi e previdenziali.º
- Intrattenimento: Publishing, Turismo, Sport, Collezionismo, Fotografia, Hobby, Giochi, Trasporti, Giardinaggio, Lotterie, Club della Lotteria, Comunicazione e Spettacolo.
- Grande Consumo: Elettronica, informatica, tessile, Immagine e Suono, Complementi, Casa, Bazar, Cura personale (Cosmetici, Profumeria, Prodotti Farmaceutici, Specialità Farmaceutiche Pubblicitarie) Mobili, Patrimonio Immobiliare, Alimentari e Bevande, Salute e Bellezza, Forniture per ufficio. Moda e Decorazione.
- Automotive: Prodotti e relativi Automobile, Motociclette e Autocarri.
- Energia e Acqua: Prodotti relativi con l'Elettricità, Idrocarburi, Gas e Acqua.
- ONG: Prodotti e servizi connessi con le ONG.

(ii) lo studio e analitica dell’utilizzo dei servizi per l’utente;
(iii) verificazione, conservazione e sviluppo di sistemi di analitica e di statistica
(iv) gestire la partecipazione dell’Utente nei diversi concorsi a premi in cui sia registrato.
(v) ricevere telefonate e SMS per motivi commerciali, pubblicitari o relazionati con servizi contratati .
(vi) remissione di sondaggi che l’Utente non sarà costretto a rispondere.
</p>

<h3>4.2 Cessione di dati personali</h3>

<p>(i) Cessione di dati a aziende del gruppo FAST TRACK NETWORKS, S.L.
I dati personali potranno essere ceduti alle società appartenente al  gruppo FAST TRACK NETWORK SL per la prestazione di servizi di marketing online e e-mail marketing, (FENIX DATA SLU, MAXIDAT SLU; DUJOK ICONSULTING SL; FINTEL TELEMARKETING SLU; KARPE DEAL SL; PICARISIMO SL e TELNET MARKETING AG) per l'invio tramite lettera, telefono, e-mail, SMS / MMS o altri mezzi di comunicazione elettronica. </p>
<p>(ii) Cessione ad altre aziende
  I dati personali potranno essere ceduti ad altre aziende per la prestazione di servizi di pubblicità e la remissione di comunicazioni commerciali correlate con i servizi specificate nel punto 4.1 (i), tramite lettera, telefono, e-mail, SMS / MMS o altri mezzi di comunicazione elettronica. </p>
  
<h3>4.3 Trattamento dei dati e obiettivi statistici</h3>

<p>L'utente accetta e acconsente che NETSALES FACTORY possa trattare i dati personali, al fine di determinare, profilo per offrirle prodotti e servizi adeguati alle sue preferenze, proprie o di altre società appartenenti ai settori dei servizi specificati nel punto 4.1 (i) </p>

<p>L'utente accetta espressamente la presente Privacy Policy ed esplicitamente acconsente al trattamento automatizzato dei dati personali facilitati in precedenza. Tuttavia, l'utente può revocare il consenso in ogni comunicazione commerciale o pubblicitaria che possa ricevere, e in qualsiasi momento, attraverso una notificazione alla seguente email info@netsalesfactory.com oppure scrivendo una lettera intestata a NETSALES FACTORY, con sede in "Palacio de la Prensa", Plaza de Callao 4, 6, 28013 Madrid.</p>

<h2>5. INFORMAZIONI FORNITE DALL'UTENTE E VERIDICITÀ DEI DATI</h2>

<p>L'Utente garantisce che i dati personali forniti siano veritieri ed è responsabile della comunicazione di eventuali modifiche a NETSALES FACTORY. L'utente, in ogni caso, sará responsabile della veridicità dei dati forniti, NETSALES FACTORY si riserva il diritto di negare il servizio a qualsiasi utente che abbia fornito dati falsi, fatte salve le altre azioni previste dalla legge. Si raccomanda la massima diligenza sulla protezione dei dati mediante l'uso di strumenti di sicurezza non potendo essere ritenuta responsabile NETSALES FACTORY di rimozioni, modifiche o perdita di dati illeciti.</p>

<h2>6. DIRITTI DI ACCESSO, MODIFICA, CANCELLAZIONE E OPPOSIZIONE</h2>

<p>Si ricorda che in qualsiasi momento é possibile esercitare i diritti di accesso, rettifica, cancellazione ed opposizione al trattamento generale o con fini promozionali dei dati personali inviandoci una comunicazione libera attraverso l'e-mail info@netsalesfactory.com che dovrà contenere: il vostro nome, la fotocopia della  carta d'identità (passaporto o altro documento di identità valido), testo della petizione nella cui si concretizza la richiesta, indirizzo di contatto a effetti di notifiche, data, firma e documenti a sostegno della richiesta fatta, se fosse il caso.</p>

<h2>7. USO DELLE COOKIE</h2>

<p>Di conformità con l’articolo 22.2 della LSSI, informiamo che NETSALES utilizza occasionalmente dei cookie sullo Sito Web www.corsovip.com I cookie registrano informazioni navigazione Internet del Utente.</p>

<p>Il cookie permette di ottimizzare la connessione e di personalizzare l'uso dello Sito Web. I rifiuto dei cookie può impedire l'accesso a determinate funzioni dello Sito Web. Per difetto, il tuo browser accetta automaticamente i cookie. Tuttavia, è possibile modificare le opzioni del browser per scegliere di non accettare i cookie.</p>

<p>Con riferimento alla gestione dei cookie e delle vostre scelte, la configurazione di ciascun browser è diversa. Essa è descritta nel menù del vostro browser e vi consentirà di conoscere come modificare le vostre scelte in materia di cookie.

</p>
<p>Per Internet Explorer : http://windows.microsoft.com/it-it/windows7/block-enable-or-allow-cookies
  
Per Safari : http://www.apple.com/it/privacy/use-of-cookies/</p>
<p> Per Chrome : https://support.google.com/chrome/answer/95647?hl=it&hlrm=en
  
</p>
<p>Per Firefox : http://support.mozilla.org/it/kb/Attivare%20e%20disattivare%20i%20cookie  </p>
<p>Per Opera : http://help.opera.com/Windows/10.20/it/cookies.html</p>
<p> Per accedere alla Cookie Policy deve cliccare sul link abilitato nello Sito Web.</p>
<p> Attraverso l’accettazione della presente Privacy Policy, l’Utente accetta anche espressamente la prestazione di servizi di e-mail retargeting con l’obiettivo di ricevere comunicazione commerciale. L’Utente consente che queste comunicazioni possono includere cookies di tipo pubblicitario che saranno scaricati nel dispositivo dell’Utente.</p>
<p> L’obiettivo di questi servizi di e-mail retargeting sarà l’acceso all’Utente all’informazione su prodotti e servizi che possano essere considerate del suo interesse attraverso:</p>
<p> (i) L’installazione di cookie nei suoi terminali mediante e-mails inviati agli Utenti.</p>
<p> (ii) Remissione di e-mails nei cui siano stati inseriti cookies dopo la visita di un sito web oppure dopo l’installazione di cookies attraverso e-mails. Le comunicazione commerciale che includono cookies informano all’Utente sul tipo di cookies inseriti e il procedimento per cancellare i cookies. </p>
<p>Tuttavia, l&rsquo;Utente può consultare l&rsquo;istruzione e i manuali del suo browser  per ampliare questa informazione. L&rsquo;Utente può anche ottenere più informazione  per quanto riguarda a questi dispositivi sul sito web <a href="http://www.youronlinechices.com">www.youronlinechices.com</a>  </p>

<h2>8. RICHIESTE</h2>

<p>Se avete domande sulla Privacy&nbsp;Policy, non esitate a contattarci  inviando una e-mail a <a href="mailto:info@netsalesfactory.com">info@netsalesfactory.com</a>. </p>

<h2>9. MODIFICHE</h2>

<p>NETSALES FACTORY si riserva il diritto di modificare termini della Privacy Policy in qualsiasi momento Per questo motivo, si prega di consultare regolarmente questa informativa sulla Privacy Policy per accedere alla versione più recente della Privacy Policy di NETSALES FACTORY.</p>  
</body>
</html>
