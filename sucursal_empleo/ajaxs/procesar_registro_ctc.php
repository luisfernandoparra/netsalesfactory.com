<?php
/**
 * Modificación LFP 2015.03.23
 * Añadimos variable para procesar los datos que vienen de acción mobile emailing Gmail
 * Para eso añadimos variable $emailingSource que será true si viene de gmail
 * Si esta variable es true, no hacemos el die del final y tenemos los datos en un array
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');
/**
 * Config loaded viene del fichero bridge. Si está definida y es true, es que ya ha cargado los configs
 */
if (empty($configLoaded)) {
    include('../../conf/config_web.php');
    include('../conf/config_web.php');
}
$resCurl = array();
///$sOutput = array(); // ARRAY PARA LOS DATOS DE SALIDA JSON
$raw = '';
$resCurl['error'] = 1;
//$sOutput['mensaje'] = 'ha habido un error en el proceso';
$sOutput['id'] = -1;
//$cli = isset($_REQUEST['cli']) ? $_REQUEST['cli'] : '';
$cli = empty($_REQUEST['cli']) ? '' : $_REQUEST['cli'];

$emailingSource = (!empty($_REQUEST['emailingsource']) && strtoupper(trim($_REQUEST['emailingsource'])) == 'GMAIL');

$nombre = (!empty($_REQUEST['s_name'])) ? trim($_REQUEST['s_name']) : '';
$surname= (!empty($_REQUEST['s_surname'])) ? trim($_REQUEST['s_surname']) : '';
$email = (!empty($_REQUEST['em'])) ? trim($_REQUEST['em']) : '';
$zip_code=(!empty($_REQUEST['i_cp'])) ? trim($_REQUEST['i_cp']) : '';
$gender=(!empty($_REQUEST['gender'])) ? trim($_REQUEST['gender']) : '';
$provI = (isset($_REQUEST['provincia_text'])) ? $_REQUEST['provincia_text'] : @$_REQUEST['provincia'];
$birth_date=(isset($_REQUEST['birth_date'])) ? $_REQUEST['birth_date'] : '';

$cbpromo=(!empty($_REQUEST['cbpromo'])) ? ($_REQUEST['cbpromo'] ? 1 : 0) : '';
$legal = (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : '1';
$permanencia = (!empty($_REQUEST['perm'])) ? trim($_REQUEST['perm']) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;
$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';
$id_crea = isset($_REQUEST['id_crea']) ? (int)$_REQUEST['id_crea'] : 0;

$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? trim($_REQUEST['idclient']) : '';
$sourcetype = (!empty($_REQUEST['sourcetype'])) ? trim($_REQUEST['sourcetype']) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? trim($_REQUEST['campaign']) : '';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? trim($_REQUEST['prioridad']) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? trim($_REQUEST['telefono']) : '';
$use_priority = (!empty($_REQUEST['use_priority']) && is_numeric($_REQUEST['use_priority']) && $_REQUEST['use_priority'] == 1 && $prioridad != '') ? 1 : 0;

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

$campos = array('idclient' => $id_client, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad,
    'nombre' => $nombre, 'apellidos' => $surname, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI,
    'vcc' => 'FM', 'pwd' => $secret_pwd, 'usr' => $secret_usr,
    'use_priority' => $use_priority);

//,"serverIp"=>"84.127.240.42"

if (!empty($_SESSION['ws_ip_id']) && is_numeric($_SESSION['ws_ip_id'])) {
    $campos['idIp'] = $_SESSION['ws_ip_id'];
}

$campos['cli']=$cli;	// ADDED 2016.04.13 M.F.
$campos['urlOrigen']=$_SERVER['HTTP_REFERER'];	// ADDED 2016.03.30 M.F.

$qs = http_build_query($campos);
$url = $url_ws . $file_ws . '?' . $qs;

if($telefono != '')
{
	$conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
	if (!$conexion->connectDB()) {
			$msg_err = 'No se ha podido conectar a la BBDD. Error 102';
	}
	else
	{
		$jsonOtherData=array();
		$jsonOtherData['authorizeReceiptCommunications']=$cbpromo;

		$jsonOtherData=json_encode($jsonOtherData);

		$cur_conn_id = $conexion->get_id_conexion();
		$nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
		$surname= str_replace('  ', ' ', ucwords(strtolower($surname)));

		if($sitesPruebas)
		{
			$nombreIns = addcslashes(mysqli_real_escape_string($cur_conn_id, checkSqlInjection($nombre)), '%_');
			$surnameIns = addcslashes(mysqli_real_escape_string($cur_conn_id, checkSqlInjection($surname)), '%_');
			$telefonoIns = addcslashes(mysqli_real_escape_string($cur_conn_id, checkSqlInjection($telefono)), '%_');
		}
		else
		{
			$nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');
			$surnameIns = addcslashes(mysqli_real_escape_string(checkSqlInjection($surname), $cur_conn_id), '%_');
			$telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
		}
		$sql='INSERT INTO %s (id_cliente,nombre,apellido2,apellidos,telefono,id_provincia,b_legales,email,cp,ip,sex,birth_date,id_crea,response_extended,id_source) VALUES (\'%d\',\'%s\',\'%s\',\'%s\',\'%s\',%d, %d,\'%s\',%d,\'%s\',\'%s\',\'%s\', %d,\'%s\',\'%s\');';
		$sql=sprintf($sql, $table_registros, $id_client, $nombreIns, $surnameIns, $crea, $telefonoIns, (int)@$_REQUEST['provincia'], $legal, $email, $zip_code, $_SERVER[REMOTE_ADDR], $gender,$birth_date, (int)$id_crea,$jsonOtherData,$fuente);

		$conexion->ejecuta_query($sql);
//echo $nombreIns."\n".$surnameIns."\n".'$error='.$error.'<pre>';print_r($_REQUEST);print_r($campos);print_r($_SERVER[REMOTE_ADDR]);die();
		if($sitesPruebas)
			$id=mysqli_insert_id($cur_conn_id);
		else
			$id=mysql_insert_id();
		$error=$id ? 0 : $id;

		$resCurl['error']=$id ? 0 : $id;
		$resCurl['idLeadLanding']=$id ? $id: 0;

	}
} else {
	if($telefono == ''){
		$error=1;
		$msg_err = 'Falta el campo Teléfono. Error 104';
	}
}

if($error)
{
	$query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
	$query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
	$conexion->getResultSelectArray($query);
	$resQuery = $conexion->tResultadoQuery;
	$idUpdate = $resQuery[0]['id'];
//	$query = 'UPDATE %s SET http_referer=\'ERROR CTC: %s\' WHERE id=\'%d\'';
//	$query = sprintf($query, $table_front_actions, $resCurl->mensaje, (int) $idUpdate);
//	$conexion->ejecuta_query($query);
}

if(!$emailingSource)
{
	$res=json_encode($resCurl);
//echo $sql."\n".'$error='.$error.'<pre>';print_r($_REQUEST);print_r($campos);print_r($res);die();
	die($res);
}
?>