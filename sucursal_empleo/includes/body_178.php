<?php 
@session_start();
$idLandingFormatted = str_pad($objSiteData->landingId, 3, '0', STR_PAD_LEFT);
$rutaImgs = $path_raiz_aplicacion_local . 'img/' . $idLandingFormatted . '/';
$defaultLocalPhoneCall = '';
$inbound_phone = ($inbound_phone) ? $inbound_phone : ''; // SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);

$h2PlainText='¡Consigue el empleo de tus sueños!';

?>

<link href="<?=$path_raiz_aplicacion_local?>css/responsive-nav.min.css" rel="stylesheet" type="text/css" />
<!--<link href="<?=$path_raiz_aplicacion_local?>css/spanize_letters<?=$minifiedFile?>.css" rel="stylesheet" type="text/css" />-->
<script type="text/javascript" src="<?=$path_raiz_aplicacion_local?>js/responsive-nav.min.js" defer="defer"></script>

 <!-- VoxReflex JS Function Starts -->
<!--<script type="text/javascript" src="<?=$path_raiz_aplicacion_local?>js/voxreflex.min.js" defer="defer"></script>-->
<!-- VoxReflex JS Function Ends -->

<style>
.claim_title h2{
display:none;
height:10px;
}

article.error2{ display : none; border:2px solid #D81E05; }
.error2{color:#D81E05; background:#FCF1F0;}
article.error2 ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
article.error2 ul li{margin:0 0 10 0;list-style-type:none;}
article.error2 ul li label{font-weight:normal}
article.error2, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error2{border:2px solid #D81E05;background-color:#FCF1F0;}

.appbutton {
background-color:#009fe4;
background-image:url("img/f-boton-color.png");
background-position:-10px 0;
background-repeat:no-repeat;
border:medium none #00335b;
border-radius:3px;
box-shadow:0 1px 2px #424242;
color:white !important;
cursor:pointer;
display:inline-block;
font:14px Arial,Helvetica,sans-serif;
margin:5px 5px 0 0;
padding:8px 13px;
text-align:center;
text-decoration:none !important;
text-shadow:0 1px #00335b;
width:auto;
}
.innerModal{
box-sizing:border-box;
margin:0;
padding:0;
padding-top:6%;
position:relative;
line-height:22px!important;
font:12px Arial,Helvetica,sans-serif;
}
</style>

<script type="text/javascript">
var errCheckTel = 1;
var iconselected = 1;
var css_selected = 'circulo_azul ico_selected';
var css_not_selected = 'circulo_azul';
var isCtcPass=0;

$(document).ready(function(){

	$("#ctcForm").validate({
			rules:{
				telefono_ctc:{
					required:true
					,remote:{
						url:root_path_local +"includes/phone_check.php",
						contentType:"application/json; charset=utf-8",  
						data:{cr:<?=(int)$id_crea?>},
						dataFilter:function(response){
							var jsonResponse="";
							isCtcPass=1;
							jsonResponse = JSON.parse(response);
							if(jsonResponse.success == false)
							{
								isCtcPass=0;
								return "\"" + jsonResponse.errorMessage + "\"";
							}else{
								return true;
							}
							return false;
						}
					}
				}
			},
			errorLabelContainer: $("#ctcForm article.error2")
	});

	$('input[name="cookie_rejection"]').click(function(){
		setTimeout('$("#pie_galletero_left").css("width","95%").css("text-align","left");',350)
	});


	$("#boton_ctc").click(function(){
		var phoneBumberCtc=$.trim($('#telefono_ctc').val());
		res=validator2.element("#telefono_ctc");

		if(!isCtcPass)
		{
			res=validator2.element("#telefono_ctc");
			setTimeout('$("#boton_ctc").click();',300);
			return false;
		}

		if(!res && !isCtcPass)
			return false;

		if($.isNumeric(phoneBumberCtc) && phoneBumberCtc.length >= 9)
		{
				var valortype = array_typeofctc[0];
				var campana = arr_campanas[0] ? arr_campanas[0] : 0;
				$.facebox.loading();
				$.ajax({
					url: root_path_local + "includes/phone_check.php",
					method: "post",
					dataType: "json",
					data: {cr:<?= (int) $id_crea ?>, telefono_ctc: phoneBumberCtc},
					cache: false,
					async: false,
					success: function(response)
					{
						if (!response.error)
						{
							$.ajax({
								url: root_path_local + "ajaxs/procesar_registro_ctc.php",
								method: "post",
								dataType: "json",
								data: {
									telefono: phoneBumberCtc,
									sourcetype: valortype,
									campaign: campana,
									fuente: id_source,
									idclient: id_client,
									crea: nomb_promo
								},
								cache: false,
								async: false,
								success: function(response)
								{
									if(!response.error)
									{
										ComprobarInsercion(response, 0);
									}
									else
									{
										ComprobarInsercion(response, 0);
									}
								},
								error:function(response){
										console.log("err2");
										return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
								}
							});
						}
						else
						{
								console.log("err code 2C");
						}
					},
					error: function(response) {
							console.log("err2");
							return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
					}
			});
		}
		return false;
	});

	validator2 = $("#ctcForm").validate({
			errorContainer: $('article.error2'),
			errorLabelContainer: $('article.error2 ul'),
			wrapper: 'li'
	});

//	START ANIMATED TEXT
//	$(".skeleton h2").append('<main style="display:inline-block;height:20px;max-width:100%;" class="mainAnimated"><section class="mast"><p class="mast__text js-spanize"><?=$h2PlainText?></section></main>');

  var spl, spanizeLetters={
    settings:{
      letters:$('.js-spanize')
    },
    init: function() {
      spl=this.settings;
      this.bindEvents();
    },
    bindEvents: function(){
      spl.letters.html(function(i, el){
        var spanizer=$.trim(el).split("");
        return '<span>'+spanizer.join('</span><span>')+'</span>';
      });
    },
  };

	$(".mainAnimated").show(1500,function(){
//		spanizeLetters.init();
	});
//	END ANIMATED TEXT

}); //document.ready


</script>
<?php
/** START EXCEPTION FOR DRAW GRATIS TOP PHONE ON TOP PAGE**/
/**
 * $arrGratisPhones = array que relaciona la fuente con el numero de teléfono a dibujar
 */
//echo'<pre>';print_r($objSiteData->phoneNumbersPerSources);echo'</pre>';

$arrGratisPhones[$objSiteData->phoneNumbersPerSources['id_surce']]=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($objSiteData->phoneNumbersPerSources['phone_number'])), 2);
$gratisPhoneCall=''; $sourceField='';
$sepParam=$sitesPruebas ? '&' : '/?' ;
$sourceField=$sepParam.'fuente='.$id_source;	// SIEMPRE SE ENVIA EL PARAMETRO DE LA FUENTE
$sourceField = $id_source ? $sourceField : '';

if($objSiteData->phoneNumbersPerSources['id_surce'] === $id_source && $objSiteData->phoneNumbersPerSources['id_surce'])
{
	$gratisPhoneCall='<section class="llamanos">	<article>		<span style="color:#1caae3; font-size:1.2em;">Llámanos GRATIS <font style="color:#036dbe;">'.$arrGratisPhones[$objSiteData->phoneNumbersPerSources['id_surce']].'</font></span>		<img src="img/tel.png" alt="Adeslas">		<span style="font-size:.9em;">Si ya eres cliente: 902.242.242</span>	</article></section>';
}
/** END EXCEPTION FOR DRAW GRATIS TOP PHONE ON TOP PAGE**/

?>
<header>
	<section class="">
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
{
	$resOutput=$objSiteData->headerTopContent;
	$resOutput=str_replace('[[gratisPhoneCall]]',$gratisPhoneCall,$resOutput);	// EXCEPTION FOR DRAW GRATIS TOP PHONE ON TOP PAGE
	echo $resOutput;
}
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
	</section>
</header>

<section class="basic-content">
	<section class="small-content">
		<section class="skeleton">

<?php
if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	$resOutput=$objSiteData->boxTopLeft;	// EXCEPTION FOR DRAW GRATIS TOP PHONE ON TOP PAGE
	echo $resOutput;
}

?>
		</section>

		<section class="form-fill">
			<form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
				<input type="hidden" name="destino" id="destino" value="">
<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
	$formScriptName='form_script_'.$idLandingFormatted.'.php';
}

$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
	echo '<span class="error3">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO '.$formScriptName.'</span>';
else
	include($formScriptName);
?>
				</form>

		    	</section><!--CIERRE FORM -->
        
    </section><!--CIERRE SMALL_CONTENT-->
    
</section><!--CIERRE BASIC_CONTENT -->
<section class="text-content">
<?php
if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<article class="boxBottom"></article>
';
}
?>
</section>


<footer>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<article class="mini"></article>
';
}

?>
</footer>
