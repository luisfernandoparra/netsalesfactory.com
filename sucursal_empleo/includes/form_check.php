<?php
/**
 * WEBSERVICES AUXILIARES
 *
 * M.F. (2017.07.17)
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

if(isset($_REQUEST['hashPipe']))
{
	include('../../conf/config_web.php');
	include('../conf/config_web.php');
}

$_REQUEST['valor_filtro']=isset($_REQUEST['s_name']) ? trim($_REQUEST['s_name']) : '';
$_REQUEST['valor_filtro']=isset($_REQUEST['s_surname']) ? trim($_REQUEST['s_surname']) : $_REQUEST['valor_filtro'];

$resCurl=array();
$raw='';
$timeout=30; // set to zero for no timeout

$resCurl['success']=false;
$resCurl['errorMessage']='Parece que el valor introducido para <b>\''.$_REQUEST['fieldName'].'\'</b> no sea correcto';


if(!is_numeric($_REQUEST['valor_filtro']))	// ADMITIDOS SOLO VALORES NO NUMERICOS
{
	$url_ws=$sitesPruebas ? 'https://dev.netsales.es/ganatuspremios.5.5.com/ajax/ajax_comprobar_filtros.php' : 'http://85.159.213.228/ajax/ajax_comprobar_filtros.php';
	$url_ws='http://85.159.213.228/ajax/ajax_comprobar_filtros.php';
	$wsFields=isset($_REQUEST['hashPipe']) ? $_REQUEST : array('ERROR'=>'expected data/fields');
	$qs=http_build_query($wsFields);
	$url=$url_ws.'?'.$qs;

	$fields_string='';

	foreach($_REQUEST as $key => $value)
		$fields_string .= $key.'='.$value.'&';

	//rtrim($fields_string,'&');
	$ch=curl_init();

	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1');
	curl_setopt($ch,CURLOPT_POST, count($_REQUEST));
	curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
	curl_setopt($ch, CURLOPT_URL, $url_ws);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$raw=curl_exec($ch);
	curl_close($ch);
	$raw=$raw ? $raw : true;

	$responseWs=json_decode($raw);
	$resCurl['success']=$responseWs->aaData ? $resCurl['success'] : true;
}

//echo $url."\n\n==>";print_r($responseWs);
//echo "\n----- devuelve: ";
$res=json_encode($resCurl);
die($res);
