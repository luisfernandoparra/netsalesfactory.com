<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
$privacyPolicy=strip_tags($privacyPolicy);
$isHttp=preg_match('/http/',$privacyPolicy);

if($isHttp)
	$resHttp=strrpos(strip_tags($privacyPolicy),'https');

if($resHttp)
	$privacyPolicy=substr($privacyPolicy,$resHttp);

?>
						<article class="error"><ul></ul></article>
						<!--<p class="form-title">Reg&iacute;strate gratis</p>-->

						<input type="text" name="s_name" id="s_name" value="<?=$s_name?>" class="celda" maxlength="100" required="required" data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" data-msg-nombre="El campo &lt;strong&gt;Nombre&lt;/strong&gt; no es válido" placeholder="Nombre" aria-required="true" minlength="3" title="Por lo menos 3 carácteres para el &lt;b&gt;Nombre&lt;/b&gt;">

						<input type="text" name="s_surname" id="s_surname" maxlength="100" class="celda" required="required" data-msg-required="El campo &lt;strong&gt; Apellido&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt; Apellido&lt;/strong&gt; no es válido" value="<?=$s_surname?>" placeholder="Apellido" aria-required="true" minlength="3" title="Por lo menos 3 carácteres para el &lt;b&gt;1º Apellido&lt;/b&gt;">

						<input type="email" name="email" id="email" maxlength="100" class="celda" required="required" data-msg-required="El campo &lt;strong&gt;Correo electrónico&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Correo electrónico&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="Correo electrónico" aria-required="true">

						<input type="tel" name="telefono" id="telefono" maxlength="10" class="celda" required="required" data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; no es válido" value="<?=$telefono?>" placeholder="Teléfono" aria-required="true">

						<input type="text" name="i_cp" id="i_cp" maxlength="5" class="celda" required="required" data-msg-required="El campo <strong> Código Postal</strong> es obligatorio" data-rule-digits="true" data-rule-minlength="5" data-msg-digits="El campo <strong>Código Postal</strong> sólo acepta números" data-msg-minlength="El campo <strong>Código Postal</strong> debe contener al menos 5 dígitos" placeholder="Código Postal" aria-required="true">

						<article class="date">
							<p>Fecha de nacimiento:</p>
							<select name="birth_day" id="birth_day" class="celda-date" required="required" data-msg-required="El campo &lt;strong&gt;Día&lt;/strong&gt; es obligatorio">
								<option value="">D&iacute;a</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option>
							</select>

							<select name="birth_month" id="birth_month" class="celda-date" required="required" data-msg-required="El campo &lt;strong&gt;Mes&lt;/strong&gt; es obligatorio">
								<option value="">Mes</option><option value="01">Enero</option><option value="02">Febrero</option><option value="03">Marzo</option><option value="04">Abril</option><option value="05">Mayo</option><option value="06">Junio</option><option value="07">Julio</option><option value="08">Agosto</option><option value="09">Septiembre</option><option value="10">Octubre</option><option value="11">Noviembre</option><option value="12">Diciembre</option>
							</select>

<?php
$ageRange=99;
$yearEnd=date('Y')-18;
$yearRange=($yearEnd-$ageRange);
$htmlOption='<option value="">A&ntilde;o</option>';

for($n=$yearEnd; $n >= $yearRange;$n--)
	$htmlOption.='<option value="'.$n.'">'.$n.'</option>';
?>
							<select name="birth_year" id="birth_year" class="celda-date" required="required" data-msg-required="El campo &lt;strong&gt;Año&lt;/strong&gt; es obligatorio">
								<?=$htmlOption;?>
							</select>
						</article>

						<article class="gender">
							<p>Sexo</p>
							<select name="gender" id="gender" class="celda" required="required" data-msg-required="El campo &lt;strong&gt;Sexo&lt;/strong&gt; es obligatorio">
							<option value="">Elige tu sexo</option><option value="H">Hombre</option><option value="M">Mujer</option>
							</select>
						</article>

						<article class="legal">
							<input required="required" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" aria-required="true" />
							&nbsp;&nbsp;He le&iacute;do y acepto la <a class="enlace_condiciones" href="<?=strip_tags($privacyPolicy) != '' ? strip_tags($privacyPolicy) : 'privacy_policies.php';?>" data-ref="" target="_blank">Pol&iacute;tica  de Privacidad</a> de Netsales Factory, S.L.U.
						</article>

						<article class="legal">
							<input required="required" data-msg-required="Se debe marcar &lt;strong&gt;la cesión de mis datos&lt;/strong&gt; a los Patrocinadores" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cbpromo" id="cbpromo" value="1" aria-required="true">&nbsp;&nbsp;Consiento la cesión de mis datos a los <a href="<?=$url_sponsors?>" target="_blank">Patrocinadores</a> de la Plataforma para el env&iacute;o de comunicaciones comerciales electr&oacute;nicas de los sectores especificados en la <a class="enlace_condiciones" href="<?=strip_tags($privacyPolicy) != '' ? strip_tags($privacyPolicy) : 'privacy_policies.php';?>" data-ref="" target="_blank">Pol&iacute;tica  de Privacidad</a> de Netsales.
						</article>


						<button class="button green sendData" id="btnProcesar" name="btnProcesar">Reg&iacute;strate</button>

						<article class="clearfix"></article>
