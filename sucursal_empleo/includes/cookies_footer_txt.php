<?php
@session_start();
// DEFINE COOKIES TEXT VARS 
$cookieCompanyNameFull='FINTEL MARKETING SLU';
$cookieCompanyNameShort='LA AGENCIA';
/*
 * LISTA DE LAS COOKIES UTILIZADAS EN ESTA PAGINA
 */
$arrThirdCookies=array('_ga'=>'Es necesaria para el funcionamiento de Google Analytics y tiene un período de caducidad de 2 años');

?>
<b>Cómo utiliza <?=$cookieCompanyNameFull?> las cookies:</b><br /><br />
<p><?=$cookieCompanyNameFull?>, en adelante <?=$cookieCompanyNameShort?>, utiliza cookies u otros dispositivos de almacenamiento y recuperaci&oacute;n de informaci&oacute;n para realizar un seguimiento de las interacciones de los usuarios con los productos de de <?=$cookieCompanyNameShort?>.<br /><br />
	Las <i>cookies</i> permiten reconocer el navegador de un usuario, así como el tipo de dispositivo desde el que se accede al sitio web, y se utilizan para facilitar la próxima visita del usuario y hacer que el sitio web o las aplicaciones resulten más útiles.<br /><br />
<b>Tipos de cookies utilizadas por <?=$cookieCompanyNameShort?>:</b></p>

<p>El sitio web titularidad de <?=$cookieCompanyNameShort?> utiliza los siguientes tipos de cookies:</p>
<ul class="ulCookies">
  <li><p><i>Cookies de preferencias o personalización:</i> Permiten al usuario acceder al servicio con algunas características de carácter general predefinidas en función de una serie de criterios en el terminal del usuario (idioma, tipo de navegador, configuración regional, etc.).</p></li>
  <li><p><i>Cookies técnicas:</i> Son aquéllas que permiten al usuario la navegación a través de una página web, plataforma, o aplicación y la utilización de las diferentes opciones o servicios que en ella existan como, por ejemplo, controlar el tráfico de la comunicación de datos, identificar la sesión, acceder a partes de acceso restringido, realizar el proceso de compra de un pedido, realizar la solicitud de inscripción o participación en un evento, utilizar elementos de seguridad durante la navegación o compartir contenidos a través de redes sociales.</p></li>
  <li><p><i>Cookies de Sesión:</i> Permiten al sitio web reconocer la información almacenada durante la sesión del usuario para evitar que el sitio web realice solicitudes de información ya facilitadas, de forma que no se pedirá la información dada anteriormente.</p></li>
  <li><p><i>Cookies analíticas:</i> Cuando el usuario se introduce en el sitio web de <?=$cookieCompanyNameShort?>, a través de esta herramienta se recopila información anónima de manera estándar sobre la navegación del usuario y sus patrones de comportamiento.</p></li>
</ul><br />

<p>Google Analytics habilita en el dominio del sitio web las cookies denominadas:</p>

<ul class="ulCookies">
<?php
/*
 * 	PAINT THE COOKIES LIST FROM "$arrThirdCookies"
 */
foreach($arrThirdCookies as $cookieName=>$cokkieDesc)
{
  echo '<li><p>"'.$cookieName.'": '.$cokkieDesc.'.</p></li>';
}
?>

</ul>

<br />
<p>Mediante la aceptaci&oacute;n de la presente, el Usuario acepta los servicios de retargeting que se podr&aacute;n llevar a cabo mediante el env&iacute;o de comunicaciones comerciales. El Usuario consiente que dichas comunicaciones comerciales contengan dispositivos de almacenamiento o cookies de publicidad que se instalar&aacute;n en su navegador.</p>

<p>Estos servicios de retargeting tienen como finalidad proporcionar m&aacute;s informaci&oacute;n sobre productos o servicios que puedan interesar al Usuario mediante:</p>
<ul class="ulCookies">
<li><p>La instalaci&oacute;n de dispositivos de almacenamiento y recuperaci&oacute;n de datos o cookies en equipos terminales en algunos de los correos electr&oacute;nicos enviados a los usuarios.</p></li>

<li><p>Env&iacute;o de correos electr&oacute;nicos con comunicaciones comerciales a los que se haya instalado las Cookies mediante la visita de una p&aacute;gina web o mediante la instalaci&oacute;n a trav&eacute;s de correo electr&oacute;nico. Adem&aacute;s, en cada comunicaci&oacute;n comercial que contenga cookies, se informar&aacute; al usuario de qu&eacute; tipo de cookies contiene y c&oacute;mo desactivarlas.</p></li>
</ul>
<br /><p>En todo caso, el usuario, mediante la aceptaci&oacute;n de la pol&iacute;tica de protecci&oacute;n de datos y de privacidad del Sitio Web, salvo menci&oacute;n expresa en contrario por el usuario, acepta expresamente que <?=$cookieCompanyNameShort?> pueda utilizar las cookies. Aun as&iacute;, el usuario tiene la posibilidad de configurar su navegador para ser avisado de la recepci&oacute;n de cookies y para impedir su instalaci&oacute;n en su equipo. Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta informaci&oacute;n. Puede obtener m&aacute;s informaci&oacute;n acerca del funcionamiento de las cookies en <a target="_blank" href="http://www.youronlinechoices.com/es/">youronlinechoices.com/es/</a></p>

<p></p>

<p>Para obtener más información acerca de Google Analytics puede dirigirse al siguiente enlace: <a href="http://goo.gl/KxpaZ" target="_blank">goo.gl/KxpaZ</a>.</p>

<b>Cómo desactivar la configuración de las cookies:</b>

<p>Todos los navegadores permiten hacer cambios para desactivar la configuraci&oacute;n de las cookies. Este es el motivo por el que la mayor&iacute;a de navegadores ofrecen la posibilidad de administrar las cookies, para obtener un control m&aacute;s preciso sobre la privacidad.</p>

<p>Estos ajustes se encuentran ubicados en las &quot;opciones&quot; o &quot;preferencias&quot; del men&uacute; de su navegador.</p>

<p>A continuaci&oacute;n podr&aacute; encontrar los links de cada navegador para deshabilitar las cookies siguiendo las instrucciones:</p>
<ul class="ulCookies">
<li>Internet Explorer (<a href="http://goo.gl/ksN5y" target="_blank">goo.gl/ksN5y</a>)</li>
<p>En el men&uacute; de herramientas, seleccione <I>&ldquo;Opciones de Internet&rdquo;</I>. Haga clic en la pesta&ntilde;a de privacidad. Ver&aacute; un cursor de desplazamiento para configurar la privacidad que tiene seis posiciones que le permite controlar la cantidad de cookies que se instalar&aacute;n: Bloquear todas las cookies, Alta, Media Alto, Media (nivel por defecto), Baja, y Aceptar todas las cookies.</p>

<li>Mozilla Firefox (<a href="http://goo.gl/F5pHX" target="_blank">goo.gl/F5pHX</a>)</li>
<p>En el men&uacute; de herramientas, seleccione <I>&ldquo;opciones&rdquo;</I>. Seleccione la etiqueta de privacidad en el recuadro de opciones. Del men&uacute; desplegable elija &ldquo;usar configuraci&oacute;n personalizada para el historial&rdquo;. Esto mostrar&aacute; las opciones de cookies y podr&aacute; optar por activarlas o desactivarlas marcando la casilla correspondiente.</p>

<li>Google Chrome (<a href="http://goo.gl/8cAo" target="_blank">goo.gl/8cAo</a>)</li>
<p>En el men&uacute; de configuraci&oacute;n, seleccione <I>&ldquo;mostrar configuraci&oacute;n avanzada&rdquo;</I> en la parte inferior de la p&aacute;gina. Seleccione la tecla de &ldquo;configuraci&oacute;n de contenido&rdquo; en la secci&oacute;n de privacidad.</p>
<p>La secci&oacute;n de la parte superior de la p&aacute;gina que aparece le da informaci&oacute;n sobre las cookies y le permite fijar las cookies que prefiera. Tambi&eacute;n le permite borrar cualquier cookie que tenga almacenada en ese momento.</p>

<li>Safari (<a href="http://goo.gl/KFBFh" target="_blank">goo.gl/KFBFh</a>)</li>
<p>En el men&uacute; de configuraci&oacute;n, seleccione la opci&oacute;n de <I>&ldquo;preferencias&rdquo;</I>. Abra la pesta&ntilde;a de privacidad. Seleccione la opci&oacute;n que quiera de la secci&oacute;n de <I>&ldquo;bloquear cookies&rdquo;</I>. Recuerde que ciertas funciones y la plena funcionalidad del Sitio pueden no estar disponibles despu&eacute;s de deshabilitar los cookies.</p>
</ul>

<p><br />Si desea no ser rastreado por las cookies, Google ha desarrollado un complemento para instalar en su navegador al que puede acceder en el siguiente enlace: <a href="http://goo.gl/up4ND" target="_blank">goo.gl/up4ND</a>.<br />
A partir de la opci&oacute;n que tome acerca del uso de cookies en los sitios web, se le enviar&aacute; una cookie adicional para salvaguardar su elecci&oacute;n y que no tenga que aceptar el uso de cookies cada vez que acceda al Sitio Web titularidad de <?=$cookieCompanyNameShort?>.</p>

<p><b>Cookies en los dispositivos móviles:</b></p>

<p><?=$cookieCompanyNameShort?> tambi&eacute;n usa cookies u otros dispositivos de almacenamiento en dispositivos m&oacute;viles.</p>

<p>Al igual que sucede en los navegadores de ordenadores, lo navegadores de los dispositivos m&oacute;viles permiten realizar cambios en las opciones o ajustes de privacidad para desactivar o eliminar las cookies.

Si desea modificar las opciones de privacidad siga las instrucciones especificadas por el desarrollador de su navegador para dispositivo m&oacute;vil.

A continuaci&oacute;n podr&aacute; encontrar algunos ejemplos de los links que le guiar&aacute;n para modificar las opciones de privacidad en su dispositivo m&oacute;vil:</p>

<ul class="ulCookies">
	<li><p>IOS: <a href="http://goo.gl/pRkla" target="_blank">goo.gl/pRkla</a></p></li>
	<li><p>Windows Phone: <a href="http://goo.gl/Rx8QQ" target="_blank">goo.gl/Rx8QQ</a></p></li>
	<li><p>Chrome Mobile: <a href="http://goo.gl/Rx8QQ" target="_blank">goo.gl/Rx8QQ</a></p></li>
	<li><p>Opera Mobile: <a href="http://goo.gl/pRkla" target="_blank">goo.gl/XvmTG</a></p></li>
</ul>


