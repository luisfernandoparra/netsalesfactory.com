<?php
/*
 * NOTA ACERCA DE INBOUND:
 * EL ARRAY CON LOS TELEFONOS INBOUND SE ENCUENTRAN EN EL
 * `index.php` AS SER NECESARIO RECIBIR `$id_source`
 * QUE SE PROCESA EN `includes/initiate.php` Y SE
 * CARGA DESPUES DE ESTE SCRIPT DE CONFIGURACION
 * 
 */
//***************************************
// START CONFIG DINAMICA (M.F. 28.10.2015)
$sitesPruebas=($_SERVER['SERVER_ADDR'] == '139.162.246.12' || $_SERVER['SERVER_ADDR'] == '127.0.0.1' || substr($_SERVER['SERVER_ADDR'], 0, 9) == '192.168.2'); // SITIOS PARA PRUEBAS
$paintErrors=false;
$minTimeCookiesStart=30;  // TIEMPO (EN SEGUNDOS) PARA ACTIVAR AUTOMATICAMENTE LAS COOKIES
$subFolderLanding='sucursal_empleo';

// START GLOBAL CHECK PHONE VARS (16.06.2014)
$landingMainName='SucursalEmpleo';
// END GLOBAL CHECK PHONE VARS

//$domainFinalName='www.ganatucafetera.com';	// SOLO PARA PRUEBAS, HASTA QUE NO ESTÈ DISPONIBLE LA REAL AQUI ABAJO:
//$domainFinalName='https://survey.sucursalempleo.com/';
$domainFinalName='https://survey.sucursalempleo.com/';

//$url_sponsors=$domainFinalName.'?id_sorteo=37&mi_seccion=3&from_origin=suemp';
$url_sponsors=$domainFinalName.'patrocinadores';
$url_raffle=$domainFinalName.'finalizar-registro/';
$url_local = 'http://www.';

if($sitesPruebas){
	$subFolderLanding='sucursal_empleo';
  $url_local_condiciones = $fromServer.'/netsalesfactory.com/promociones/';	//ORIGINAL PARA MARAVILLAO / netsalesfactory.com
	$url_local = $fromServer.'/'.$rootMaravillaoFolder.@$developersLocalFolder[$developerIp].'promociones';
	$dir_raiz_aplicacion_local = $rootMaravillaoFolder.@$developersLocalFolder[$developerIp].'promociones/' . $subFolderLanding.'/';
//	$path_raiz_aplicacion_local = $fromServer.'/'.$dir_raiz_aplicacion_local . '';
	$path_raiz_aplicacion_local = '/'.$dir_raiz_aplicacion_local . '';
	$path_raiz_includes_local = $path_raiz_includes . 'sucursal_empleo/';
	$url_condiciones = $url_local .'/'.$subFolderLanding .'/condiciones.php';
	$paintErrors=true;
	$minTimeCookiesStart=3000;
  $url_thankyouPage = 'thankyoupage.php';
  $url_raffle='http://concurso.netsales.es/ganatuspremios.5.5.com/plantilla/?id_sorteo=37';
  $url_sponsors='http://concurso.netsales.es/ganatuspremios.5.5.com/plantilla/?id_sorteo=37&mi_seccion=3&from_origin=suemp';
	//$url_ws = 'http://pre.w3.racc.es/ws/wsserver_ca_contactos.php?wsdl';
} else {
	$url_local_condiciones = 'http://www.';
	$dir_raiz_aplicacion_local = $dir_raiz_aplicacion . $subFolderLanding.'/';
	$path_raiz_aplicacion_local = '/' . $dir_raiz_aplicacion_local;
	$path_raiz_includes_local = $path_raiz_includes . $subFolderLanding.'/';
	$url_condiciones = $url_local .'/'.$subFolderLanding . '/condiciones.html';
  $url_thankyouPage = 'thankyoupage.php';
//	$url_ws = 'http://w3.racc.es/ws/wsserver_ca_contactos.php?wsdl';
}
// END CONFIG DINAMICA (M.F. 19.11.2013)

//Variable para la tabla REGISTROS, para diferenciar qué cliente es
$id_client = (!empty($_REQUEST['idClient'])&& is_numeric($_REQUEST['idClient'])) ? (int)$_REQUEST['idClient'] : 87;


$arr_client_campaign=array(
	87 => array(
//		'prefFichExtraccion' => 'raccBase',
//		'refInconcert' => array('100000016', '100000016')
	)
);

$prefFichExtraccion = $arr_client_campaign[$id_client]['prefFichExtraccion'];
$arr_campanas = $arr_client_campaign[$id_client]['refInconcert'];

$file_ws = 'router.php';

//echo'<pre style=text-align:left;>';print_r($arr_client_campaign);
/* $campana_lead = "PruebasClickToCall";
  $campana_ctc = "PruebasClickToCall";
 */

//Array con todas las creas existentes. 
/*
 * Notas 2014:
 * 1. El parámetro `manageDevice` condiciona la detección del dispositivo que carga la promo
 * 2. `new_template` gestiona el tipo de modelo a seguir con la composición de la promo,
 * - 0: modo antiguo con archivos distintos para contenidos según PC o MOBILE
 * - 1: modo nuevo un archivo único para todos los dispositivos
 */

$arrSemGoogle=array();	//PARAMETROS PARA GOOGLE SEM

$arr_creas = array();


//Parámetros del pixel de TradeDoubler
$organization = '1963177';
$event = '325638';
$trbdl_program='228617';
$secret_pwd_conf='p3Rd€r_N@da';
$secret_usr=date('Y-m-d H:i:s');
$secret_pwd = md5($secret_usr.$secret_pwd_conf);

$nombCliente = 'SucursalEmpleo';

//Pixel expecíficos
$isSpecificPixel = 1;

//Array de display
$arr_isSpecificPixel = array(
// START PIXEL DE PRUEBAS (M.F. 19.11.2013)
	'9999999' => '<script language="JavaScript1.1" src="http://wrap.tradedoubler.com/wrap?id=8498"></script>'
// END PIXEL DE PRUEBAS (M.F. 19.11.2013)
	);

// START FIXED PIXELS (ADDED 30.01.2014)
$mi_referer=strtolower(@$_SERVER['HTTP_REFERER']);
$arr_email_domain = array('orangemail.es', 'latinmail.com', 'webmail', 'mail');
$pos = false;

if(!empty($mi_referer)){
  foreach($arr_email_domain as $mi_valor)
	{
		$pos = strpos($mi_referer,$mi_valor);
		if ($pos === true)
			break;
	}
}

if($pos === true || empty($_SERVER['HTTP_REFERER'])){
  $conditionalPixelStatic='https://secure.adnxs.com/seg?add=1432957&t=1';
}else {
  $conditionalPixelStatic='https://secure.adnxs.com/seg?add=1432958&t=1';
}

$conditionalPixelStatic='';
// END FIXED PIXELS 


/*
 * Para el cliente XXXXX, lanzaremos 2... deber·n ser seguidos.
 * El parámetro `isImageObject` permite crear una imagen si está habilitado, en caso contrario se construirá un JS.
 */
$arr_isSpecificPixelJS = array(
	'XXXXXXX'=>array(0=> 'http://',1 => '','isImageObject'=>0)
);


/*
 * SE SOBREESCRIBEN EL ARRAY `$arr_creas` CON LOS DATOS ALMACENADOS EN LA BBDD
 * CORRESPONDIENTES A LA PROMOCION ACTUAL SI ESTA EXISTE
 * NECESARIO TAMBIEN PARA MANTENER COMPATIBILIDAD CON LAS PROMOS ANTERIORES
 * 
 * (M.F. mayo 2014)
 * 
 */
if(@$isDataDb && @$objSiteData->landingId)
{
	$arr_creas[$objSiteData->landingId]['title']=$objSiteData->webTitle;
	$arr_creas[$objSiteData->landingId]['body']=$objSiteData->bodyScript;
	$arr_creas[$objSiteData->landingId]['nombpromo']=$objSiteData->landingName;
	$arr_creas[$objSiteData->landingId]['is_mobile']=0;
	$arr_creas[$objSiteData->landingId]['is_compro_prov']=1;
	$arr_creas[$objSiteData->landingId]['condiciones']=$objSiteData->landingTermsFile;
	$arr_creas[$objSiteData->landingId]['protecciondatos']=$objSiteData->landingPrivacyPolicyFile;
	$arr_creas[$objSiteData->landingId]['cookie_css']=$objSiteData->footerCookiesCssFile;
	$arr_creas[$objSiteData->landingId]['mobile_auto_modal_on_open']=$objSiteData->mobileAutoModal;
	$arr_creas[$objSiteData->landingId]['manageDevice']=1;
	$arr_creas[$objSiteData->landingId]['new_template']=1;
	$arr_creas[$objSiteData->landingId]['js']='';
	$arr_creas[$objSiteData->landingId]['skip_check_phone']=$objSiteData->skipCheckPhone;	//	EVITA EL CONTROL DEL NUMERO TELEFONICO (CURL)
}

if($debugModeNew)
{
//	$debugDujok=true;
//echo 'local-conf:<pre>';print_r($objSiteData->semGoogleParams);echo '</pre>';
}
/**
 * Parámetros de conexión a WS Ip SEarch Engine
 */
$ws_ip_usr = 'XXXXX';
$ws_ip_pwd = '';
$ws_ip_arr_parameters['usr'] = $ws_ip_usr;//array('usr'=>$ws_ip_usr,'pwd'=>$ws_ip_pwd,'ip'=>'');
$ws_ip_arr_parameters['pwd'] = $ws_ip_pwd;
$ws_ip_check = true;

