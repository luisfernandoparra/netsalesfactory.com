<?php
/**
 * SCRIPT DE PUENTE DESDE LANDING SUCURSAL EMPLEO HACIA GTP 5.5
 *
 * M.F. ABRIL 2017
 *
 */
header('Content-Type: text/html; charset=utf-8');
@session_start();
include('../conf/config_web.php');
ini_set('display_errors',0);
@include('conf/config_web.php');
include($path_raiz_includes . 'includes/initiate_thankyoupage.php');
include($path_raiz_includes . 'class/class.pixels.management.php');

if(count($_POST))
  $arraExtVars=$_POST;
else
  $arraExtVars=$_REQUEST;

if(!isset($_REQUEST['url_raffle']) || !$_REQUEST['url_raffle'])
{
	$arraExtVars['url_raffle']=$_SESSION['init_access']['url_raffle'] ? $_SESSION['init_access']['url_raffle'] : 'https://newsletter.sucursalempleo.com/';

	if(!$arraExtVars['url_raffle'])
		die('ERROR, SIN URL DESTINO');
}

$secret_pwd='p3Rd€r_N@da';
$token=md5(date('YmdHisu').rand());
$pwd=md5($token.$secret_pwd);
$cleanDomain=stristr($arraExtVars['url_raffle'], 'http://') ? 7 : 8;
$cleanDomain=substr($arraExtVars['url_raffle'],$cleanDomain);
$posFirstSlash=strpos($cleanDomain,'/');
$posFirstSlash=$posFirstSlash ? $posFirstSlash : strpos($cleanDomain,'?');
$cleanDomain=$posFirstSlash ? substr($cleanDomain,0,$posFirstSlash) : 1;
$ipTarget=gethostbyname($cleanDomain);
$isSameServer=($ipTarget == $_SERVER['SERVER_ADDR']);
$strUrlTargetRaffle=null;

/**
	[scheme] => http
	[host] => concurso.netsales.es
	[path] => /ganatuspremios.5.5.com/plantilla/
	[query] => id_sorteo=17
 */
$stringIdSorteo=''; $paramIdSorteo='';
if(strpos($arraExtVars['url_raffle'],'?'))	// DESCOMPOSICION DE LA URL DE DESTINO PARA COMPONER CORRECTAMENTE LOS ELEMENTOS A ENVIAR
{
	$tmpArr=parse_url($arraExtVars['url_raffle']);
	$tmpParamArr=explode('=', $tmpArr['query']);
	$getIdSorteo=false;

	foreach($tmpParamArr as $key=>$value)
	{
		if($getIdSorteo)
		{
			$idSorteoValue=$value;
			break;
		}

		if($value == 'id_sorteo')
		{
			$idSorteoName=$value;
			$getIdSorteo=true;
		}
	}

	$stringIdSorteo='<input type="hidden" name="'.$idSorteoName.'" value="'.(int)$idSorteoValue.'" />';
	$paramIdSorteo='&'.$idSorteoName.'='.(int)$idSorteoValue;
}


?>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Welcome</title>
<style>
body{
margin:0;
padding:0;
}

h1{
color:#323232;
text-align:center;
font-size:3.5em;
font-family:Arial,verdana;
margin-top:0.9em;
}

p{
color:#e6a93f;
font-family:Arial,verdana;
text-align:center;
font-size:2.5em;
}

@media(max-width: 725px){
	h1{font-size:2.5em;}
	p{font-size:2em;}
}

@media(max-width: 526px){
	h1{font-size:1.9em;}
	p{font-size:1.8em;}
}

@media(max-width: 390px){
	p{margin-top:-0.1em;}
}

</style>

</head>
<body>

<h1>¡Gracias!</h1>
<p>Accediendo...</p>
<!--<br /><p style="color:#000;">Verifica tu correo electrónico,<br /><br />revisa tu carpeta spam</p>-->
<br /><br />

<form id="gtp6Data" name="gtp6Data" method="post" action="<?=$arraExtVars['url_raffle']?>" style="display:inline-block;width:100%;height:auto;float:left;overflow:hidden;font-family:arial;" >
	<input name="token" value="<?=$token?>" type="hidden" />
	<input name="pwd" value="<?=$pwd?>" type="hidden" />
	<input name="mi_seccion" value="10" type="hidden" />
	<input name="from_origin" value="suemp" type="hidden" />
	<input name="utm_campaign" value="<?=isset($_REQUEST['utm_campaign']) ? $_REQUEST['utm_campaign'] : ''?>" type="hidden" />
	<input name="ip_address" value="<?=$_SERVER['SERVER_ADDR']?>" type="hidden" />
	<input name="legal_bases" value="1" type="hidden" />
	<input name="b_email_valid" value="1" type="hidden" />

<?php
echo $stringIdSorteo;
//die();
foreach($arraExtVars as $key=>$value)
{
	if($key == 'gender' && is_array($value))	// CONVERSION POR TIPO DE DATO ARRAY, NECESARIO PARA UN RADIO TAG
		$value=$value[0];

	// AQUI LOS CAMPOS QUE NO DEBEN DE SER ENVIADOS A GTP 5.5.....
	if($key == 'url_target' || $key == 'action' || !$value)		continue;

	// MAS CAMPOS QUE DEBEN SER OMITIDOS
	if($key == '_rating_lead_value' || $key == 'rateContent' || $key == 'cbpromo' || $key == 'rateTitle' || substr($key,0,6) == 'data__')	continue;

	switch($key)	// COMPATIBILIZACION A NOMBRES DE CAMPOS DE v.GTP 5.5
	{
		case 'idLeadLanding':
			$fieldNameGtp='id_lead';
			break;
		case 'cblegales':
			$fieldNameGtp='privacy_policy';
			break;
		case 'telefono':
			$fieldNameGtp='phone';
			break;
		case 'i_cp':
			$fieldNameGtp='zip_code';
			break;
		case 's_surname':
			$fieldNameGtp='last_name1';
			break;
		case 's_name':
			$fieldNameGtp='name';
			break;
		default:
			$fieldNameGtp=$key;
	}

//echo'DEBUG: &nbsp;&nbsp;-&nbsp;&nbsp;NOMBRE CAMPO: <i>'.$fieldNameGtp.'</i>&nbsp;&nbsp;=&nbsp;&nbsp;<b>'.$value.'</b><br>';

//	if($isSameServer)
		echo '	<input name="'.$fieldNameGtp.'" type="hidden" value="'.$value.'" />
';
}

//echo '<hr>url_raffle = '.$arraExtVars['url_raffle'].'<br>';
//echo '<hr>'.$strUrlTargetRaffle.'<hr>'.$ipTarget.'] <pre>';print_r($arraExtVars);print_r($_SERVER);print_r($_SESSION);echo'</pre>';

//echo'<pre>';print_r($arraExtVars);print_r($_SESSION);echo'</pre>';die();

// HABILITAR LOS UNSETs CUANDO ESTE LISTA Y COMPLETA LA RECEPCION EN GTP 5.5
// SE DESTRUYE COMPLETAMENTE LA SESIÓN PARA PODER REINICIAR CORRECTAMENTE
unset($response['errorTitle']);
unset($response['errorDebug']);
unset($response);
unset($arraExtVars);
//unset($_SESSION);
unset($_REQUEST);
unset($_GLOBALS);
//session_destroy();

?>
</form>
<?php
if($strUrlTargetRaffle)	// ES UN SERVIDOR DISTINTO A GTP5.5
{
//echo'<HR>1.SE EJECUTARÁ: setTimeout(\'document.location.href="'.$strUrlTargetRaffle.'";\',20);';
//	echo '<script>setTimeout(\'document.location.href="'.$strUrlTargetRaffle.'";\',20);</script>';
}
else	// LA VAZLORACION PROVIENE DEL MISMO SERVIDOR DE GTP5.5
{
//echo'<HR>2.SE EJECUTARÁ: setTimeout(\'document.forms["gtp6Data"].submit();\',20);';
	echo '<script>setTimeout(\'document.forms["gtp6Data"].submit();\',10);</script>';
}
?>

</body>
</html>