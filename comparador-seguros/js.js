/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 $(document).ready(function () {
        var loader = false;
       $( "input[type=radio]" ).on( "click", function(){
           var rel = $(this).attr('rel');
           $('#question-box'+rel).fadeOut();
           if (rel<3) {
               rel = parseInt(rel) + 1;
               $('#question-box'+rel).fadeIn();
           } else {
               loader=true;
                $('#loader-box').fadeIn();
                setTimeout(function(){ window.location = '<?php echo $urlJumpTo;?>'; }, <?php echo $delay;?>);
           }
       });
    });
