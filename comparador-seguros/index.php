<?php
/**
 * @author Netsales
 * @date 2016.10.04
 * @version 1.0
 * Chorri Prelanding de fake-comparador de seguros
 */
$delay = rand(1000, 3000);

/**
 * Cogemos la IP para coger bien la carpeta de carga
 */
$ip = $_SERVER['SERVER_ADDR'];
//echo $ip;
$folder = ($ip == '178.79.159.246') ? 'comparador-seguros/' : (($ip=='85.159.213.228') ? 'prelander/comparador-seguros/' : '');

$partner = (!empty($_REQUEST['partner'])) ? trim($_REQUEST['partner']) : '41866';
$fuente = (!empty($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '49009';
$banner = (!empty($_REQUEST['banner'])) ? trim($_REQUEST['banner']) : '';
$cli = (!empty($_REQUEST['cli'])) ? trim($_REQUEST['cli']) : '';

?>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EL SUPERCOMPARADOR</title>
        <link href="<?php echo $folder;?>css/estilos.css" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="icon" href="<?php echo $folder;?>img/xxx/favicon.png" type="image/x-icon">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var loader = false;
                var inputs = 'ANSWERS';
                var partner = "<?php echo $partner;?>";
                var fuente = "<?php echo $fuente;?>";
                var banner = "<?php echo $banner;?>";
                $("input[type=radio]").on("click", function () {
                    var rel = $(this).attr('rel');
                    inputs = inputs + ',' + $(this).val();
                    $('#question-box' + rel).fadeOut();
                    if (rel < 3) {
                        rel = parseInt(rel) + 1;
                        $('#question-box' + rel).fadeIn();
                    } else {
                        loader = true;
                        //alert(inputs);
                        $('#loader-box').fadeIn();
                        $.ajax({
                            method: "POST",
                            url: "<?php echo $folder;?>ajax/process_data.php",
                            dataType: "json",
                            data: {data: inputs}
                        })
                                .done(function (urljumpto) {
                                    //console.log(urljumpto);
                                    //alert(urljumpto);
                                    url = urljumpto[0];
                                    
                                    
                                    url = url.replace("[%%PARTNER%%]",partner);
                                    url = url.replace("[%%FUENTE%%]",fuente);
                                    url = url.replace("[%%BANNER%%]",banner);
                                    //url = "http://marketing.net.netsales.es/ts/i4614207/tsc?amc=aff.netsales.41866.49009.62430.19641";
                                    setTimeout(function () {
                                        //window.open(url,"_self");
                                        //$('#frm').attr('action', url).submit();
//                                        $('#jumpto').val(url);
//                                        $('#frm').submit();
                                       window.location.href = url;
                                       
                                    }, <?php echo $delay; ?>);
                                    return false;
                                });


                    }
                });
            });
        </script>
    </head>

    <body>
        <form  action="jump" method="POST" name="frm" id="frm">
            <input type="hidden" id="cli" name="cli" value="<?php echo $cli;?>"/>
            <input type="hidden" id="amc" name="amc" value="aff.netsales.41866.49009.62430.19641" />
            <input type="hidden" id="jumpto" name="jumpto" value="" />
        </form>

        <header>
            <section class="cabecera">
                <img src="<?php echo $folder;?>img/xxx/logo.jpg" width="150" height="100" alt=""/>
                <h1>EL SUPERCOMPARADOR</h1>
            </section>
        </header>

        <section class="small-content">

            <section class="question-content">
                <img src="<?php echo $folder;?>img/xxx/background.png" width="742" height="340" alt=""/>
                <h1>¡Nosotros te ayudamos a decidirte!</h1>
                <h2>Contesta las siguientes preguntas y te diremos que seguro es el mejor para ti:</h2>

                <!--Pregunta Nº1-->

                <article class="question-box1" id="question-box1">
                    <p class="quest1">¿Eres?
                    </p>

                    <article class="answer1">
                        <div class="option1">
                            <label>
                                <input type="radio" id="" name="have_insurance[]" rel="1" value="GEND-M" required="" data-msg-required="Por favor, &lt;b&gt;¿Tienes 	                hijos/as?&lt;/b&gt;" aria-required="true"><font class="checks-quest1">Mujer</font>
                            </label>
                        </div>
                        <div class="option1" >
                            <label>
                                <input type="radio" id="" name="have_insurance[]" rel="1" value="GEND-H"  required="" aria-required="true"><font class="checks-quest1">	                Hombre</font>
                            </label>
                        </div>

                    </article>        

                </article>

                <!--Pregunta Nº2-->

                <article class="question-box2" style="display:none;"  id="question-box2" >
                    <p class="quest1">Edad:
                    </p>

                    <article class="answer2">
                        <div class="option1">
                            <label>
                                <input type="radio" id="" name="have_insurance[]" rel="2" value="AGE-30" required="" data-msg-required="Por favor, &lt;b&gt;¿Tienes 	                hijos/as?&lt;/b&gt;" aria-required="true"><font class="checks-quest2">18-30</font>
                            </label>
                        </div>
                        <div class="option2">
                            <label>
                                <input type="radio" id="" name="have_insurance[]" rel="2" value="AGE-50" required="" aria-required="true"><font class="checks-quest2">	                31-50</font>
                            </label>
                        </div>

                        <div class="option2">
                            <label>
                                <input type="radio" id="" name="have_insurance[]" rel="2" value="AGE-90" required="" aria-required="true"><font class="checks-quest2">	                +50</font>
                            </label>
                        </div>

                    </article>        

                </article>

                <!--Pregunta Nº3-->

                <article class="question-box3" style="display:none;"  id="question-box3">
                    <p class="quest1">¿Cuantos hijos tienes?
                    </p>

                    <article class="answer3">
                        <div class="option1">
                            <label>
                                <input type="radio" id="" name="have_insurance[]" rel="3"  value="HIJ-0" required="" data-msg-required="Por favor, &lt;b&gt;¿Tienes 	                hijos/as?&lt;/b&gt;" aria-required="true"><font class="checks-quest3">0</font>
                            </label>
                        </div>
                        <div class="option3">
                            <label>
                                <input type="radio" id="" name="have_insurance[]" rel="3" value="HIJ-2" required="" aria-required="true"><font class="checks-quest3">	                1-2</font>
                            </label>
                        </div>

                        <div class="option3">
                            <label>
                                <input type="radio" id="" name="have_insurance[]" rel="3" value="HIJ-3" required="" aria-required="true"><font class="checks-quest3">	                +3</font>
                            </label>
                        </div>

                    </article>        

                </article>

                <!--Loader-->

                <article class="loader-box" style="display:none;" id="loader-box">
                    <h1>¡Estamos seleccionando el mejor seguro para ti!</h1>
                    <img class="loader" src="<?php echo $folder;?>img/xxx/loader.gif" width="66" height="66" alt=""/>
                    <img class="hero" src="<?php echo $folder;?>img/xxx/loader-hero.png" width="200" height="120" alt=""/>
                </article>

            </section>

            <section class="logo-box">
                <h1>Trabajamos con las mejores compañias del mecado</h1>
                <img class="logo-ok" src="<?php echo $folder;?>img/xxx/logo-mapfre.png" width="190" height="67" alt=""/>
                <img class="logo-ok" src="<?php echo $folder;?>img/xxx/logo-adeslas.png" width="190" height="67" alt=""/> 
                <img class="logo-ok" src="<?php echo $folder;?>img/xxx/logo-asisa.png" width="190" height="67" alt=""/>
                <img class="logo-ok" src="<?php echo $folder;?>img/xxx/logo-dkw.png" width="190" height="67" alt=""/>
                <img class="logo-ok" src="<?php echo $folder;?>img/xxx/logo-sanitas.png" width="190" height="128" alt=""/>
                <img class="logo-ok" src="<?php echo $folder;?>img/xxx/logo-axa.png" width="190" height="108" alt=""/>
            </section>

        </section>

        <footer></footer>
    </body>
</html>
