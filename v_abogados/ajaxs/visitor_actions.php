<?php
/*
 * SCRIPT INVOCADO DESDE "includes/header_tmplt.php"
 * LOG ACCIONES LEADS, INICICALMENTE X LOPD
 *
 * M.F. (14.03.2018)
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');

$_REQUEST['skipLogAction']=true;	// OBLIGA A OMITIR EL LOG COMUN DEL config_web PARA EVITAR DATOS INNECESARIOS

include('../../conf/config_web.php');
ini_set('display_errors',$paintErrors);
include('../conf/config_web.php');
$response=array();
$response['error']=false;
//session_destroy(); //ESTO SOLO x TESTS !!!

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

if(!isset($_SESSION['namePromo']))	// SE HA PERDIDO LA SESION VALIDA PRA PROSEGUIR
{
	$msg_err='Debe recargar la p&aacute;gina';
	$response['error']=2;
	$response['msg']=$msg_err;
	$res=json_encode($response);
	die($res);
}

if(!$conexion->connectDB())
{
	$msg_err='<center><div class=errorModalGeneric>Ha ocurrido un error en la conex&oacute;n, <b>por favor, recargue esta p&aacute;gina</b>.<br /><br />Disculpe las molestias<br /><br /><span style=font-size:2.6em;color:red;>&#x2639;<span><div></center>';
	$response['msg']=$msg_err;
	$response['error']=true;
	$res=json_encode($response);
	die($res);
}

$pagToLog=$_SERVER['PHP_SELF'];
$pagToLog=substr($pagToLog,strrpos($pagToLog,'/')+1);
$pagToLog=substr($pagToLog,0,strpos($pagToLog,'.'));


$laAccion=isset($arraExtVars['detAction']) && $arraExtVars['detAction'] ? $arraExtVars['detAction'] : null;
$laAccion=normalizeInputData('STRINGS,SQLINJECTION', ucwords(strtolower(trim($laAccion))));

if(!isset($arraExtVars['action']) || $arraExtVars['action'] != 'setVisitorAction')
	$laAccion='!!! accion no permitida';

$query='INSERT INTO %s (session_id, fecha, ip_usuario, script, accion, id_crea, action_number) VALUES (\'%s\', NOW(), \'%s\', \'%s\', \'%s\', %d, %d)';
$query=sprintf($query, $table_front_actions, $_SESSION['namePromo'], $_SERVER['REMOTE_ADDR'], $pagToLog, $laAccion, $idLandingCr, $_SESSION['sessionCountPromo']);
$response['res']=$conexion->ejecuta_query($query);
$response['sql']=$_SERVER['REMOTE_ADDR'] === '192.168.2.102' ? $query : '';
$response['act']=$_SERVER['REMOTE_ADDR'] === '192.168.2.102' ? $laAccion : '-';
//echo $query."\r";print_r($_REQUEST);print_r($arraExtVars);die("\r\rFIN SCRIPT");
$_SESSION['sessionCountPromo']++;
$res=json_encode($response);
die($res);

