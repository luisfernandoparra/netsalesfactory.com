<?php
/**
 * Modificación LFP 2015.03.23
 * Añadimos variable para procesar los datos que vienen de acción mobile emailing Gmail
 * Para eso añadimos variable $emailingSource que será true si viene de gmail
 * Si esta variable es true, no hacemos el die del final y tenemos los datos en un array
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');
/**
 * Config loaded viene del fichero bridge. Si está definida y es true, es que ya ha cargado los configs
 */
if(empty($configLoaded)){
	include('../../conf/config_web.php');
	include('../conf/config_web.php');
}

$resCurl = array();
$raw = '';
$tokenError = '';
$resCurl['error'] = 1;
$arrPrefixSubjects=array();

for($n=0;$n < 400; $n++)	// SE RELLENAN TODOS CON LO MISMO
	$arrPrefixSubjects[$n]='Datos registro landing V abogados';

//	START EXCEPCIONES SEGUN ID CREATIVIDAD
$arrPrefixSubjects[167]='Afectados Cláusulas suelo';
$arrPrefixSubjects[171]='Afectados Banco Popular';
$arrPrefixSubjects[176]='Afectados Multidivisas';
$arrPrefixSubjects[182]='Datos registro landing V abogados SEPLA';
//	END EXCEPCIONES SEGUN ID CREATIVIDAD

$emailingSource = (!empty($_REQUEST['emailingsource']) && strtoupper(trim($_REQUEST['emailingsource'])) == 'GMAIL');

$nombre=(!empty($_REQUEST['nombre'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', ucwords(strtolower(trim($_REQUEST['nombre'])))) : '';

$apellidos = (!empty($_REQUEST['apellidos'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', ucwords(strtolower(trim($_REQUEST['apellidos'])))) : '';
$email = (!empty($_REQUEST['em'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,EMAIL', strtolower(trim($_REQUEST['em']))) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', $_REQUEST['provincia_text']) : (normalizeInputData('INPUT,STRINGS,SQLINJECTION', @$_REQUEST['provincia']) ? $_REQUEST['provincia'] : 0);

$legal = (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';

// START LOPD NEW VARS
$lopdPrivacyPolicy= ($legal || $legal === 0) ? $legal : 0;
$lopdLegalBasis=!empty($_REQUEST['legalBasis']) ? trim($_REQUEST['legalBasis']) : '';
$lopdLegalBasis=($lopdLegalBasis || $lopdLegalBasis === 0) ? (int)$lopdLegalBasis : 0;
$lopdLegalDataSent=!empty($_REQUEST['legalDataSent']) ? $_REQUEST['legalDataSent'] : '';
$lopdLegalDataSent=($lopdLegalDataSent || $lopdLegalDataSent === 0) ? (int)$lopdLegalDataSent : 0;
// END LOPD NEW VARS

$legal = ($legal === 1 || $legal === 0) ? $legal : 1;
$permanencia = (!empty($_REQUEST['perm'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['perm'])) : 0;
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;

$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';
$id_crea=(!empty($_REQUEST['id_crea'])) ? trim($_REQUEST['id_crea']) : 0;

//echo 'IdClient: '.$_REQUEST['idclient']. ' Crea:'.$crea;
$idSource = (!empty($_REQUEST['id_source']) && is_numeric($_REQUEST['id_source'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['id_source'])) : 1;
$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['idclient'])) : '';

$sourcetype = (!empty($_REQUEST['sourcetype'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['sourcetype'])) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['campaign'])) : '0';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['fuente'])) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['prioridad'])) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['telefono'])) : '';
$use_priority = (!empty($_REQUEST['use_priority']) && is_numeric($_REQUEST['use_priority']) && $_REQUEST['use_priority'] == 1 && $prioridad != '') ? 1 : 0;

$sec_tok = !empty($_REQUEST['sec_tok']) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', $_REQUEST['sec_tok']) : 0;
$spain_city= !empty($_REQUEST['spain_city']) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', $_REQUEST['spain_city']) : null;
$addParam=isset($_REQUEST['addparam']) && count($_REQUEST['addparam']) ? $_REQUEST['addparam'] : null;
$sqlAddParam=$addParam ? json_encode($addParam) : null;
$sqlAddParam=$sqlAddParam ? mysql_escape_string($sqlAddParam) : null;
$sqlAddParam=$sqlAddParam ? strip_tags($sqlAddParam) : null;


$check_sec_tok = '0'.$id_client . '_' . 'DJK' . date('dmd') . '' . $crea;
$check_sec_tok = md5($check_sec_tok);

if ($sec_tok != $check_sec_tok) {
    $telefono = '';
    $tokenError = ', Error de TOKEN de suguridad';
    $resCurl['msg'] = 'Error Token';
}

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

/**
 * POR DEFECTO SE ENVIAN LAS DIRECCIONES DE "$transactionEmailDoctorSender"
 *
 * EN CASO DE QUE EXISTA "$transactionEmailDoctorSenderSpecivicCrea[$id_crea]" SE UTILIZAN LOS EMAILS AHI CONTENIDOS
 */
$defaultReceiver=(isset($transactionEmailDoctorSenderSpecivicCrea[$id_crea])) ? $transactionEmailDoctorSenderSpecivicCrea[$id_crea] : $transactionEmailDoctorSender;

$campos=array('idclient' => $id_client,'id_source' => $idSource, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad,
    'nombre' => $nombre, 'apellidos' => $apellidos, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI,
    'vcc' => 'fintel', 'pwd' => $secret_pwd, 'usr' => $secret_usr,
    'use_priority' => $use_priority);
//echo $sql."\r";print_r($_REQUEST);print_r($campos);die("\r!!!!!!\rprovincia = ".$provI);
if (!empty($_SESSION['ws_ip_id']) && is_numeric($_SESSION['ws_ip_id'])) {
    $campos['idIp'] = $_SESSION['ws_ip_id'];
}

if (!$conexion->connectDB()){
	$msg_err = 'No se ha podido conectar a la BBDD. Error 102';
	$resCurl['msg'] = $msg_err;
}


if($telefono != '')
{
	$cur_conn_id = $conexion->get_id_conexion();
	$nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
	$nombreIns = addcslashes(checkSqlInjection($nombre), '%_').' '.addcslashes(checkSqlInjection($apellidos), '%_');

	$telefonoIns = addcslashes(mysql_escape_string($telefono), '%_');
	$sql = 'INSERT INTO %s (id_cliente,id_source,nombre,apellidos,telefono,id_provincia,b_legales,email, response_extended) VALUES (\'%d\',\'%d\',\'%s\',\'%s\',\'%s\',\'%d\', %d,\'%s\',\'%s\');';
	$sql = sprintf($sql, $table_registros, $id_client, $idSource, $nombreIns, $crea, $telefonoIns, (int) $_REQUEST['provincia'], $legal, $email, $sqlAddParam);
//echo $sql."\r";print_r($_REQUEST);print_r($campos);die("\r!!!!!!");
	$conexion->ejecuta_query($sql);
//	$id = $conexion->mysql_insert_id();
	$id = $conexion->Id;
	$resCurl['id']=$id;
	$idRegitro=$conexion->Id;

	switch((int)$id_client)
	{
		case 74:
			$fileCsv = '../v_abogadosfiles/bonospopular-'.date('Ymd').'.csv';
			break;
		case 75:
			$fileCsv = '../v_abogadosfiles/abogadosirph-'.date('Ymd').'.csv';
			break;
		case 79:
			$fileCsv = '../v_abogadosfiles/hipotecasmultidivisas-'.date('Ymd').'.csv';
			break;
		case 80:
			$fileCsv = '../v_abogadosfiles/afectadoscooperativa-'.date('Ymd').'.csv';
			break;
		case 83:
			$fileCsv = '../v_abogadosfiles/afectadosclausulasuelo-'.date('Ymd').'.csv';
			break;
		default:
			$fileCsv = '../v_abogadosfiles/vabogados'.($id_crea==182 ? '_sepla-' : '-').date('Ymd').'.csv';
	}

	$headerFile='';
	$headerCity=$spain_city ? 'ciudad;' : '';
	$addheaderCsv=''; $addDataCsv=''; $addEmailData='';

	if(!is_null($addParam) && count($addParam))
	{
		foreach($addParam as $fieldName=>$fieldValue)
		{
			$addheaderCsv.=';'.$fieldName;
			$addDataCsv.=';'.$fieldValue;
			$addEmailData.=$fieldName.': <b>'.$fieldValue.'</b><br />';
		}

	}

	if(!file_exists($fileCsv))
		$headerFile='Nombre; E-mail; Teléfono; ciudad; crea; ID source'.$addheaderCsv."\r\n";;

	$fp=@fopen($fileCsv,'a') or die('Unable to open file!');
	$line=$headerFile.$nombre.';'.$email.';'.$telefono.';'.($spain_city ? $spain_city.';' : ';').$crea.';'.$idSource.$addDataCsv."\r\n";
	$raw=$error;
	$error=fwrite($fp, $line);
	fclose($fp);
	$error=(int)$error > 0 ? '' : 1;


	$subjectSuffix=$arrPrefixSubjects[(int)$id_crea];
	$htmlDataCity=$spain_city ? 'Ciudad: <b>'.$spain_city.'</b><br />' : '';
	$htmlData='
<html>
<head></head>
<body>
<div style=font-family:arial,verdana;font-size:1em;color:#000;display:inline-block;width:99%;float:left;text-align:left;>
Nombre: <b>'.$nombre.'</b><br />
E-mail: <b>'.$email.'</b><br />
Tel&eacute;fono: <b>'.$telefono.'</b><br />
'.$htmlDataCity.'
Fecha del registro: <b>'.date('d-m-Y').'</b><br />
Referencia de la landing: <b>'.$crea.'</b><br />
Source: <b>'.$idSource.'</b><br />
'.$addEmailData.'
</div>7
</body>
</html>
';

	$arrData=array('user'=>$crea, 'data'=>array(
			array(
			'receiver'=>$defaultReceiver,
			'subject'=>$subjectSuffix,
			'html'=>$htmlData,
			'text'=>$htmlData
			)
		)
	);

	$fields_string = json_encode($arrData);
	$url = $url_ws . $file_ws;//. '?user='.$crea;
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_POST, 1);
	curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, $timeout);
	$raw = curl_exec($ch);
	$statusWs = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);

	if(isset($resCurl['correct']) && $resCurl['correct'] < 1)
		$error=1;

	$resCurl['error']=$error;

	/**
	 * START INSERT DATA LOPD 2018
	 * SE HA INSERTADO CORRECTAMENTE EL LEAD EN LA BBDD (ID REGCORD = $idRegitro)
	 * SE INSERTAN LOS PARAMETROS NECESARIOS PARA LA TRAZA DE LA LOPD
	 * M.F. 2018.03.07
	 */
	if($idRegitro)
	{
		$query='INSERT INTO %s (id_lead, id_landing, status_accept_privacy_policy, status_accept_legal_basis, status_accept_customer_data_transfer, insert_date) VALUES (%d, %d, %d, %d, %d, NOW())';
		$query=sprintf($query, $table_lopd_lead_company_legal_epigraphs, (int)$idRegitro, (int)$id_crea, (int)$lopdPrivacyPolicy, (int)$lopdLegalBasis, (int)$lopdLegalDataSent);
		$conexion->ejecuta_query($query);
	}
	// END INSERT DATA LOPD 2018

	$conexion->disconnectDB();

	if($error){
		$msg_err = 'Parece que el problema se debe a lo siguiente: "' . $resCurl->mensaje . '".';
	}else
	{
		$tmpCrea = $_SESSION['idCreatividad'];
		$query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
		$query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
		$conexion->getResultSelectArray($query);
		$resQuery = $conexion->tResultadoQuery;
		$idUpdate = $resQuery[0]['id'];
		$query = 'UPDATE %s SET http_referer=\'transaction Email: %s\' WHERE id=\'%d\'';
		$query = sprintf($query, $table_front_actions, ($statusWs == 200 ? 'OK' : 'ERROR'), (int) $idUpdate);
		$conexion->ejecuta_query($query);

		unset($_SESSION['vAbogados'.date(Yimis)]); // SE DESTRUYE LA REFERENCIA DE LA SESION
		$tmpName = session_name('vAbogados'.date(Yimis));
		$_SESSION['namePromo'] = session_name(); // SE CONSTRUYE UNA NUEVA SESION PARA PERMITIR REGISTROS NUEVOS DESDE LA MISMA APERTURA
		$_SESSION['idCreatividad'] = $tmpCrea;

		$resCurl['error']=0;
	}
} else {
	if ($telefono == '') {
		$error = 1;
		$msg_err = 'Falta el campo Teléfono. Error 104';
		$resCurl['msg'] = $msg_err;
	}
}

if($error)
{
	$query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
	$query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
	$conexion->getResultSelectArray($query);
	$resQuery = $conexion->tResultadoQuery;
	$idUpdate = $resQuery[0]['id'];
	$query = 'UPDATE %s SET http_referer=CONCAT(http_referer, \', ERROR CTC: %s\') WHERE id=\'%d\'';
	$query = sprintf($query, $table_front_actions, $resCurl->mensaje . $tokenError, (int) $idUpdate);
	$conexion->ejecuta_query($query);
}
$conexion->disconnectDB();
if(!$emailingSource) {
	$res = json_encode($resCurl);
	die($res);
}
