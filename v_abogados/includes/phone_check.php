<?php
/*
 * REMOTE PHONE NUMBER CHECK
 */
@session_start();

//$paintErrors=true;//$xx=ini_get_all();//echo '<pre>';print_r($xx);

$debug = (!empty($_REQUEST['debug']));
if ($debug)
ini_set('display_errors',$paintErrors);

if(empty($_SESSION['namePromo']))
{
	$res['success']=false;
	$res=json_encode($res);
	die($res);
}

$res=array();
$_SESSION['skipLogAction']=true;	// EVITA LOG INNECESARIO
include('../../conf/config_web.php');
include('../conf/config_web.php');
$_SESSION['skipLogAction']=false;	// SE REACTIVAN LOS LOGS BBDD


if($controlChekAlgorithm != $_REQUEST['hashPipe'])	// SI EL CHECK NO COINCIDE, SE DEVELVE UN JSON FALSO
{
	$resCheck=false;
	$userAccessSec=rand(0,10000);
	$idCustomerSec=rand(888,10000);
	$response['success']=false;
	$response['errorMessage']='Parece que el número <b>'.$_REQUEST['telefono'].'</b> no es correcto.';
  $response['orig'] = $idCustomerSec;
	$_SESSION['sessionCountPromo']++;
	$response['error']=true;
//	$response['idcheck']=$idCustomerSec;
	$res=json_encode($response);

	$line='';
	$logFile = $path_raiz_includes.'log/leads_hack-p_'.date('Y-m-d').$_REQUEST['mf2017'].'.txt';
	if(isset($_SERVER['HTTP_REFERER']))
		$line.=date('d-m-Y H:i:s').', procedencia: '.$_SERVER['HTTP_REFERER']."\r\n";

	if(isset($_SERVER['REQUEST_URI']))
		$line.='params: '.$_SERVER['REQUEST_URI']."\r\n";

	$line.='phone        : '.$_REQUEST['telefono']."\r\n";
	$line.='URL server   : '.$_SERVER['SERVER_NAME']."\r\n";
	$line.='hash por GET : '.$_REQUEST['hashPipe']."\r\n";
	$line.='hash esperado: '.$controlChekAlgorithm."\r\n";
	$line.='IP cliente   : '.$_SERVER['REMOTE_ADDR']."\r\n";
	$line.='--'."\r\n";
	$dataFileLog=$line;
	$handleLog=@fopen($logFile,'a+');
	@fwrite($handleLog, strip_tags($dataFileLog));
	@fclose($handleLog);
	die($res);
}

if(@$arr_creas[(int)$_REQUEST['cr']]['skip_check_phone'])	// SE OMITE EFECTUAR EL CHECK DEL TELEFONO
{
	$res['success']=true;
	$laAccion='phone-check-skipped';
}
else
{
	include('../../includes/check_phone.class.php');

	$telf = 0;
	if (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) {
			$telf = (int)trim($_REQUEST['telefono']);
	} elseif (!empty($_REQUEST['telefono_ctc']) && is_numeric($_REQUEST['telefono_ctc'])) {
			$telf = (int)trim($_REQUEST['telefono_ctc']);
	}

	if((int)$telf != $telf)
	{
		$res=$response['success'];
		$res=json_encode($res);
		die($res);
	}

	$phoneToCheck=str_replace($arr_sql_inject_original, $arr_sql_inject_cambio, $telf);

	$objeto=new remoteCheckPhone();
	$objeto->landingMainName=$landingMainName;
	$objeto->phoneCheckUserAccess=$phoneCheckUserAccess;
	$objeto->phoneCheckPasswordAccess=$phoneCheckPasswordAccess;
	$objeto->phoneCheckUrlTarget=$phoneCheckUrlTarget;
	$res['success']=$objeto->phoneCheckRemote($phoneToCheck) ? true : false;	// BRIDGE TO CHECK PHONE NUMBER;
	if ($debug)
		echo ($objeto->phoneCheckRemote($phoneToCheck));
	$res['errorMessage']='Parece que el número <b>'.$phoneToCheck.'</b> no es correcto.';

//	$laAccion=$res['success'] ? 'phone-check-OK;'.$_REQUEST['telefono'].';'.$objeto->idChekTel : 'phone-check-ERROR;'.$_REQUEST['telefono'].';'.$objeto->idChekTel;
	$laAccion=$res['success'] ? 'phone-check-OK;'.$phoneToCheck.';'.$objeto->idChekTel : 'phone-check-ERROR;'.$phoneToCheck.';'.$objeto->idChekTel;
}

$table_front_actions=$prefixTbl.'log_web_actions'.date('Y');
/*
 * start LOG ACTIONS
 */
if(!empty($_SESSION['namePromo']))
{
	$pagToLog=$_SERVER['PHP_SELF'];
	$pagToLog=substr($pagToLog,strrpos($pagToLog,'/')+1);
	$pagToLog=substr($pagToLog,0,strpos($pagToLog,'.'));
	$laAccion=$laAccion ? $laAccion : '?';

	$query='INSERT INTO %s (session_id,fecha,ip_usuario,script,accion,id_crea,action_number)VALUES(\'%s\',NOW(),\'%s\',\'%s\',\'%s\',%d,%d)';
	$query=sprintf($query,$table_front_actions,$_SESSION['namePromo'],$_SERVER['REMOTE_ADDR'],$pagToLog,$laAccion,$idLandingCr,$_SESSION['sessionCountPromo']);
	$conexion->ejecuta_query($query);
}
	$_SESSION['sessionCountPromo']++;
/*
 * end LOG ACTIONS
 */

$res=json_encode($res);
die($res);
?>