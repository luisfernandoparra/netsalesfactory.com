<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
?>
						<div class="error"><ul></ul></div>
						<p>SOLICITE INFORMACI&Oacute;N</span></p>

						<div class="fila">
							<div class="fleft">
								<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda user-name" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre y Apellidos&lt;/strong&gt; es obligatorio" placeholder=" Nombre y Apellidos" aria-required="true" />
							</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<input type="tel" maxlength="9" name="telefono" id="telefono" class="celda user-phone" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder=" Teléfono" aria-required="true" />
							</div>
						</div>

						<div class="row">
							<div class="fleft">
								<input type="email" name="email" id="email" maxlength="100" class="celda user-mail" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder=" E-mail" aria-required="true" />
							</div>
						</div>
<?php
if($composeCitiesBlock)
{
	$htmlOption='<option value="">Indique la provincia</option>';

	foreach($spainProvinces as $valProv=>$provinceName)
		$htmlOption.='<option value="'.$valProv.'">'.$provinceName.'</option>';

?>

						<div class="row">
							<div class="fleft">
								<select name="province" id="province" class="celda-date" required="required" data-msg-required="El campo &lt;strong&gt;Provincia&lt;/strong&gt; es obligatorio">
									<?=$htmlOption;?>
								</select>
							</div>
						</div>

<?php
}
?>

						<div class="legal" style="padding-top:10px;">
							<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;He le&iacute;do y acepto la
							<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'privacy_policies.php';?>" data-ref="" target="_blank">Pol&iacute;tica de Privacidad</a>
						</div><!--/12-->

						<div class="row">
							<div class="espacio-btn">
								<input class="green sendData" type="button" id="btnProcesar" name="btnProcesar" value="INF&Oacute;RMESE">
							</div>
						</div>

						<div class="clearfix"></div>

