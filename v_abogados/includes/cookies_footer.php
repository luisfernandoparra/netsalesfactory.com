<?php
@session_start();
$cookiesMinTimeLimitStart = $minTimeCookiesStart; // TIEMPO MAXIMO ANTES DE LANZAR AUTOMATICAMENTE LAS COOKIES
//$_SESSION['cookiesMan']=1;
// START TIME CHECK COOKIES CONTROL INIT VARS
$timeUpdateCheckLanding = 5000;  // INTERVALO DE TIEMPO PARA EJECUTAR AUTOMATICAMENTE AJAX TEMPORIZADO (en milisegundos)
$jsInitStartXCounterLanding = 'var startCheckCookieLanding=0;';
ini_set('display_errors',$paintErrors);
//echo '-->'.($objSiteData->htmlCookiesContent);
$objSiteData->htmlCookiesContent=trim($objSiteData->htmlCookiesContent) ? $objSiteData->htmlCookiesContent : '<br /><br /><br /><center>No se ha podido cargar la información solicitada, lamentamos las molestias.<br /><br />Puede intentarlo más tarde.</center>';
$cookiesContentHtml=$objSiteData->htmlCookiesContent;

$arrContentsRightBox=array();
$arrContentsRightBox['title']['lopd_table_title']=$objSiteData->lopd['lopd_table_title'];
$arrContentsRightBox['title']['lopd_statement_responsible']=$objSiteData->lopd['lopd_statement_responsible'];
$arrContentsRightBox['summary']['lopd_statement_responsible']=$objSiteData->lopd['lopd_summary_responsible'];
$arrContentsRightBox['title']['lopd_statement_purpose']=$objSiteData->lopd['lopd_statement_purpose'];
$arrContentsRightBox['summary']['lopd_statement_purpose']=$objSiteData->lopd['lopd_summary_purpose'];
$arrContentsRightBox['title']['lopd_statement_legitimation']=$objSiteData->lopd['lopd_statement_legitimation'];
$arrContentsRightBox['summary']['lopd_statement_legitimation']=$objSiteData->lopd['lopd_summary_legitimation'];
$arrContentsRightBox['title']['lopd_statement_recipients']=$objSiteData->lopd['lopd_statement_recipients'];
$arrContentsRightBox['summary']['lopd_statement_recipients']=$objSiteData->lopd['lopd_summary_recipients'];
$arrContentsRightBox['title']['lopd_statement_rights']=$objSiteData->lopd['lopd_statement_rights'];
$arrContentsRightBox['summary']['lopd_statement_rights']=$objSiteData->lopd['lopd_summary_rights'];
$arrContentsRightBox['title']['lopd_statement_origin']=$objSiteData->lopd['lopd_statement_origin'];
$arrContentsRightBox['summary']['lopd_statement_origin']=$objSiteData->lopd['lopd_summary_origin'];

/**
 * START COOKIES HTML & COMPOSE DATA
 * NUEVA GESTION DE LOS CONTENIDSO DINAMICOS DE LAS COKKIES DEL SITE
 *
 * M.F. 2018.02.28
 */
if(isset($id_crea))
{
	$headersCookiesTable=array(
		'td_company_cookies_title'=>'Cookies propias'
		,'th_domain_name_title'=>'Dominio bajo el cual figura la cookie'
		,'th_cookie_name_title'=>'Nombre'
		,'th_cookie_purpose_title'=>'Finalidad'
		,'th_cookies_more_info_title'=>'M&aacute;s Informaci&oacute;n'
	);

	$query='SELECT cc.business_name, cc.cookie_name, cc.cookie_domain, cc.description, cc.live_time, cc.extra_info, cc.is_third_partie FROM %s AS cl RIGHT JOIN %s AS cc ON cc.id_cookie=cl.id_cookie WHERE cl.id_landing=%d && cc.b_enabled && !cc.is_third_partie';
	$query=sprintf($query, $table_cookies_landings, $table_cookies_contents,(int)$id_crea);
	$conexion->getResultSelectArray($query);
	$resSql=$conexion->tResultadoQuery;
	$objSiteData->ownCookiesLanding=$resSql;
	$counterCookies=0;
	
	if(count($objSiteData->ownCookiesLanding))
	{
		$stNewrHtml=$objSiteData->lopd['lopd_more_info_alt'];;
		$stNewrHtml.='<br /><div class=mainInfoCookies>';
		$stNewrHtml.='<table class="landingInfoCookies" border="1">';
		$stNewrHtml.='<tr><td colspan=4><center>'.$headersCookiesTable['td_company_cookies_title'].'</center></td></tr>';
		$stNewrHtml.='<tr>';
		$stNewrHtml.='<th>'.$headersCookiesTable['th_domain_name_title'].'</th>';
		$stNewrHtml.='<th>'.$headersCookiesTable['th_cookie_name_title'].'</th>';
		$stNewrHtml.='<th>'.$headersCookiesTable['th_cookie_purpose_title'].'</th>';
		$stNewrHtml.='<th>'.$headersCookiesTable['th_cookies_more_info_title'].'</th>';
		$stNewrHtml.='</tr>';
		$pos0=0; $strTd[0]=array();
		$rowPos0=null;

		foreach($objSiteData->ownCookiesLanding as $key=>$data)
		{
			if($objSiteData->ownCookiesLanding[$key+1]['cookie_domain'] == $data['cookie_domain'])// && !$pos0
			{
				if(is_null($rowPos0))
					$rowPos0=$key;

				$pos0++;
				$strTd[0][$rowPos0][$data['cookie_domain']]='<td rowspan='.($pos0+1).'>'.$data['cookie_domain'].'</td>';
			}
			else
			{
				$rowPos0=null;

				if(!$pos0)
					$strTd[0][$key][$data['cookie_domain']]='<td>'.$data['cookie_domain'].'</td>';

				$pos0=0;
			}
			$counterCookies++;
		}

		foreach($objSiteData->ownCookiesLanding as $key=>$data)
		{
			$stNewrHtml.='<tr>';
			$stNewrHtml.=$strTd[0][$key][$data['cookie_domain']];
			$stNewrHtml.='<td>'.$data['cookie_name'].'</td>';
			$stNewrHtml.='<td>'.$data['description'].'</td>';
			$stNewrHtml.='<td>'.(substr(trim($data['extra_info']),0,4) == 'http' ? '<a href=\''.$data['extra_info'].'\' target=\'_blank\'>'.$data['extra_info'].'</a>' : $data['extra_info']).'</td>';
			$stNewrHtml.='</tr>';
			$stNewrHtml.='';
		}

		$stNewrHtml.='</table>';
		$stNewrHtml.='</div><br />{{{}}}';
		$cookiesContentHtml=str_replace('[[[]]]', $stNewrHtml, $objSiteData->htmlCookiesContent);
	}
	else
	{
		if(!$counterCookies)
			$cookiesContentHtml=str_replace('[[[]]]', 'Más información:{{{}}}', $objSiteData->htmlCookiesContent);
		else
		{
			$stNewrHtml='Más información:{{{}}}';
			$cookiesContentHtml=str_replace('[[[]]]', $stNewrHtml, $objSiteData->htmlCookiesContent);
		}
	}


	$query='SELECT cc.business_name, cc.cookie_name, cc.cookie_domain, cc.description, cc.live_time, cc.extra_info, cc.is_third_partie FROM %s AS cl RIGHT JOIN %s AS cc ON cc.id_cookie=cl.id_cookie WHERE cl.id_landing=%d && cc.b_enabled && cc.is_third_partie=1';
	$query=sprintf($query, $table_cookies_landings, $table_cookies_contents,(int)$id_crea);
	$conexion->getResultSelectArray($query);
	$resSql=$conexion->tResultadoQuery;
	$objSiteData->thirdPartyCookiesLanding=$resSql;


	if(count($objSiteData->thirdPartyCookiesLanding))
	{
		$headersCookiesTable['td_company_cookies_title']='Cookies de terceros';
		$stNewrHtml='<br /><div class=mainInfoCookies>';
		$stNewrHtml.='<table class="landingThirdPartieInfoCookies" border="1">';
		$stNewrHtml.='<tr><td colspan=4><center>'.$headersCookiesTable['td_company_cookies_title'].'</center></td></tr>';
		$stNewrHtml.='<tr>';
		$stNewrHtml.='<th>'.$headersCookiesTable['th_domain_name_title'].'</th>';
		$stNewrHtml.='<th>'.$headersCookiesTable['th_cookie_name_title'].'</th>';
		$stNewrHtml.='<th>'.$headersCookiesTable['th_cookie_purpose_title'].'</th>';
		$stNewrHtml.='<th>'.$headersCookiesTable['th_cookies_more_info_title'].'</th>';
		$stNewrHtml.='</tr>';
		$pos0=0; $strTd[0]=array();
		$rowPos0=null;

		foreach($objSiteData->thirdPartyCookiesLanding as $key=>$data)
		{
			if($objSiteData->thirdPartyCookiesLanding[$key+1]['cookie_domain'] == $data['cookie_domain'])// && !$pos0
			{
				if(is_null($rowPos0))
					$rowPos0=$key;

				$pos0++;
				$strTd[0][$rowPos0][$data['cookie_domain']]='<td rowspan='.($pos0+1).'>'.$data['cookie_domain'].'</td>';
			}
			else
			{
				$rowPos0=null;

				if(!$pos0)
					$strTd[0][$key][$data['cookie_domain']]='<td>'.$data['cookie_domain'].'</td>';

				$pos0=0;
			}
			$counterCookies++;
		}

		foreach($objSiteData->thirdPartyCookiesLanding as $key=>$data)
		{
			$stNewrHtml.='<tr>';
			$stNewrHtml.=$strTd[0][$key][$data['cookie_domain']];
			$stNewrHtml.='<td>'.$data['cookie_name'].'</td>';
			$stNewrHtml.='<td>'.$data['description'].'</td>';
			$stNewrHtml.='<td>'.(substr(trim($data['extra_info']),0,4) == 'http' ? '<a href=\''.$data['extra_info'].'\' target=\'_blank\'>'.$data['extra_info'].'</a>' : $data['extra_info']).'</td>';
			$stNewrHtml.='</tr>';
			$stNewrHtml.='';
		}
		$stNewrHtml.='</table>';
		$stNewrHtml.='</div><br /><br />';
		$cookiesContentHtml=str_replace('{{{}}}', $stNewrHtml, $cookiesContentHtml);
	}
	else
	{

		if($counterCookies)
			$cookiesContentHtml=str_replace('{{{}}}', '<br />', $cookiesContentHtml);
		else
			$cookiesContentHtml=str_replace('Más información:{{{}}}', '', $cookiesContentHtml);
	}
}
// END COOKIES HTML & COMPOSE DATA


if (!@isset($_SESSION['startCheckCookieLanding'])) { // START VISITOR TIME COUNTER
	$_SESSION['startCheckCookieLanding'] = ceil(microtime(true));
	$_SESSION['sess_cookie_manage'] = 0;
	$jsInitStartXCounter = 'var startCheckCookieLanding=1;';
}

$elapsedTimeStart = ceil(microtime(true) - $_SESSION['startCheckCookieLanding']);
// END TIME CHECK COOKIES CONTROL INIT VARS


//Modificado por LFP 2013.11.21
/*
 * CARGA DEL ARCHIVO CSS PARA LOS CONTENIDOS Y GESTION DE LAS COOKIES
 */
if(isset($arr_creas) && isset($arr_creas[$id_crea]['cookie_css']) && !empty($arr_creas[$id_crea]['cookie_css']))
{
   $styleSheetFooter = trim($arr_creas[$id_crea]['cookie_css']);
}
else
{
	// START EXCEPCIONES PARA EL CSS SEGUN id_creatividad
	$styleSheetFooter='lopd_common';
	//$styleSheetFooter = 'footer_cookies';
	$styleSheetFooterExtra = '';
	$styleSheetFooter.=$styleSheetFooterExtra . '.css';
	// END EXCEPCIONES PARA EL CSS SEGUN id_creatividad
}

?>
<style type="text/css">

.modalBottomCookies{
background-color: rgba(255,255,255,0.6);
width:100%;
z-index:1000;
bottom:0px;
position:fixed;
}
.modalBottomCookies .innerCookiesModal{
border-top:1px solid #fff;padding:28px 0;width:99%!important;}
.modalBottomCookies .contentModalBottomCookies{max-width:930px;margin:0 auto;padding:7px 17px 7px 17px;vertical-align:middle;text-align:justify;background-color:#f1f1f1;-moz-border-radius:4px; -webkit-border-radius:4px;border-radius:4px; -webkit-box-shadow:inset 0 0 2px 0 rgba(0,0,0,0.2);box-shadow:inset 0 0 2px 0 rgba(0,0,0,0.2);border:2px solid #999;outline:1px solid #fff;position:relative;}
.modalBottomCookies .contentModalBottomCookies p{font:normal 11px/16px Arial,Helvetica,sans-serif;color:#333;text-shadow:0 1px 0 #fff;margin:0;}
.modalBottomCookies .contentModalBottomCookies p strong{background:url(img/icoInfoCookiesBottom.png) 0 3px no-repeat;display:block;padding-left:19px;margin-bottom:3px;line-height:19px;font-weight:bold;}
.modalBottomCookies .contentModalBottomCookies p a{font-weight:bold;color:#0097c8;}
.modalBottomCookies .contentModalBottomCookies a.closeModalBottomCookies{float:right;text-decoration:none;font-size:14px;color:#333;text-align:right;right:0px;font-family:arial,verdana;}
.modalBottomCookies .contentModalBottomCookies a.closeModalBottomCookies:hover{color:#000;}
.modalBottomCookies .contentModalBottomCookies p.cerrar{text-align:right;}
</style>


<div class="modalBottomCookies" id="modalBottomCookies">
	<div class="innerCookiesModal">
		<div class="contentModalBottomCookies">
			<a title="Cerrar" class="closeModalBottomCookies" href="javascript:void(0);">X</a>
			<p><strong>Uso de cookies</strong></p>
			<p>Utilizamos cookies propias, de analítica y de terceros para mejorar tu experiencia de usuario. Al continuar con la navegación consideramos que aceptas la instalación y el uso de cookies, <a href="#null" class="smooth cookie_info isBottomModal">más información aquí</a>.</p>
		</div>
	</div>
</div>

<link href="<?=$path_raiz_aplicacion_local?>css/<?=$styleSheetFooter?>" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function drawDefaultPix()	// IF COOKIES ARE APPROVED
{
	var isGooglePix="<?=(count($objSiteData->semGoogleParams) && isset($objSiteData->semGoogleParams))?>" ? 1 : 0;

	if(isGooglePix){	// GOOGLE LEAD - 20140704 M.F.
		includeScript("//www.googleadservices.com/pagead/conversion.js","js");
		$('#googlePx0').append('<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/'+google_conversion_id+'/?value=0&amp;guid=ON&amp;script=0"/>');
	}

	if(googleSEO)	// ONLY FOR THE `seo` LANDINGS SEM HAS googleSEO DEFINED
	{
		(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","http://www.google-analytics.com/analytics.js","ga");
		ga("create",googleSEO,"auto");
		ga("send","pageview");
	}

	var $element=$("body");
}

function includeScript(file_path, type)
{
  document.head = document.head || document.getElementsByTagName('head'[0]);
  if(type == "js")
  {
		var j = document.createElement("script");
		j.type = "text/javascript";
		j.src = file_path;
		document.head.appendChild(j);
  }
  return;
}

// START COOKIES CONTROL
var checkElapsedTime;
var cookiesEnabled = 0;
var cookiesManaged = 0;
//// END COOKIES CONTROL


function manageCookies(start)
{
	if(cookiesManaged > 0)
		return;
	cookiesEnabled = start;
	cookiesManaged += 1;

	$("#pie_galletero_right").slideUp("fast", function() {
		$("#pie_galletero_right").css("width", "0px !important");
		$("#pie_galletero_right").css("width", "0px !important");
		$("#pie_galletero_left").removeClass("pie_galletero_left");
		$(".pie_galletero_left").css("width", "98% !important", function() {
			$(".cookie_info").css("text-align", "center");
		});
		$(".cookie_info").css("text-align", "center");
	});

	if(start == 1){
		drawDefaultPix();
		$.ajaxSetup({async: false});
		$.ajax({
			url:"<?=$path_raiz_aplicacion_local ?>ajaxs/cookies_ajax_pixels.php",
			method:"post",
			dataType:"json",
			data:{isCookie:cookiesEnabled, fuente:id_source},
			cache:false
			,success:function(response)
			{
				if(response.pixel != undefined){
				$.each(response.pixel,function(key,val)
				{
					$("#footerWebPage").append(val);
				});
				}
			}
		});
		$.ajaxSetup({async: true});
	}
}

/* START MOD POL COO M.F. 2014.12.23 */
var firstStep=true;
function eraseModalBottomCookies()
{
	$(".modalBottomCookies").fadeOut("fast");
}
/* END MOD POL COO M.F. 2014.12.23 */


$(document).ready(function(){

/* START MOD POL COO M.F. 2014.12.23 */
	$(".closeModalBottomCookies").click(function(){
		eraseModalBottomCookies();
		setVisitorAction(this);
	});
/* END MOD POL COO M.F. 2014.12.23 */

	$("#cookie_acept").click(function() {
			manageCookies(1);
			setVisitorAction(this," [cookies accepted]");
	});

	$("#cookie_rejection").click(function() {
			manageCookies(0);
			setVisitorAction(this," [cookies rejected]");
	});

});
</script>

<div id="boxMobile" class="defaultSideSlider">
    <div id="contentSidelSider">
			<center><?=@$arrContentsRightBox['title']['lopd_table_title']?></center>
			<table class="tblSideSlider">
				<tr>
					<td><?=@$arrContentsRightBox['title']['lopd_statement_responsible']?><br><a href="#inner_modal_20" title="<?=$objSiteData->lopd['lopd_more_info_alt']?>" data-info="show modal:lopd_statement_responsible"><span class="circle_lopd">&gt;</span></a></td>
					<td><?=@$arrContentsRightBox['summary']['lopd_statement_responsible']?></td>
				</tr>
				<tr>
					<td><?=@$arrContentsRightBox['title']['lopd_statement_purpose']?><br><a href="#inner_modal_21" title="<?=$objSiteData->lopd['lopd_more_info_alt']?>" data-info="show modal:lopd_statement_purpose"><span class="circle_lopd">&gt;</span></a></td>
					<td><?=@$arrContentsRightBox['summary']['lopd_statement_purpose']?></td>
				</tr>
				<tr>
					<td><?=@$arrContentsRightBox['title']['lopd_statement_legitimation']?><br><a href="#inner_modal_22" title="<?=$objSiteData->lopd['lopd_more_info_alt']?>" data-info="show modal:lopd_statement_legitimation"><span class="circle_lopd">&gt;</span></a></td>
					<td><?=@$arrContentsRightBox['summary']['lopd_statement_legitimation']?></td>
				</tr>
				<tr>
					<td><?=@$arrContentsRightBox['title']['lopd_statement_recipients']?><br><a href="#inner_modal_23" title="<?=$objSiteData->lopd['lopd_more_info_alt']?>" data-info="show modal:lopd_statement_recipients"><span class="circle_lopd">&gt;</span></a></td>
					<td><?=@$arrContentsRightBox['summary']['lopd_statement_recipients']?></td>
				</tr>
				<tr>
					<td><?=@$arrContentsRightBox['title']['lopd_statement_rights']?><br><a href="#inner_modal_24" title="<?=$objSiteData->lopd['lopd_more_info_alt']?>" data-info="show modal:lopd_statement_rights"><span class="circle_lopd">&gt;</span></a></td>
					<td><?=@$arrContentsRightBox['summary']['lopd_statement_rights']?></td>
				</tr>
				<tr>
					<td><?=@$arrContentsRightBox['title']['lopd_statement_origin']?><br><a href="#inner_modal_25" title="<?=$objSiteData->lopd['lopd_more_info_alt']?>" data-info="show modal:lopd_statement_origin"><span class="circle_lopd">&gt;</span></a></td>
					<td><?=@$arrContentsRightBox['summary']['lopd_statement_origin']?></td>
				</tr>
			</table>
    </div>
    <ul id="tabSidelSider">
			<li>
				<img id="arrowMobile" onclick="toggleMobile('boxMobile');" src="../img/left_arrow_slider.png" title="Ocultar / ver la información básica sobre Protección de Datos" width="24" />
			</li>
    </ul>
</div>

<div id="pie_galletero" class="hide" style="<?=$id_crea > 0 ? 'display:block' : ''?>;">
    <div id="pie_galletero_left" class="pie_galletero_left">
			<span id="blockTxtPieAsk">Utilizamos cookies propias, de analítica y de terceros para mejorar tu experiencia de usuario. Si aceptas la instalación de cookies o continúas navegando, consideramos que aceptas su uso.<br /></span>
    <div style="float:left;width:100%;">
			<center><a href="#null" class="smooth cookie_info">Más información aquí</a></center>
    </div>
    </div>
  <div id="pie_galletero_right"><center>
        <input type="button" id="cookie_acept" name="cookie_acept" value="Acepto" class="cookieButton" /> <input type="button" id="cookie_rejection" name="cookie_rejection" value="No Acepto" class="cookieButton" />
		</center>
    </div>
    <div class="clearfix">&nbsp;</div>
</div>


<div id="modal_info_cook" style="display:none;">
	<div class="boxContentsCookies">
<?php
echo $cookiesContentHtml;
?>
		<br /><br />
		<center>
			<a class="modalCloseBtn" onclick="$.modal.close();return false;" href="#null" >cerrar</a>
		</center>
		<br />
	</div>
</div>

<div id="modal_pp" style="display:none;">
	<div class="boxContentsPp">
<?php
echo $objSiteData->landingPrivacyPolicyFile;
?>
		<br /><br />
		<center>
			<a class="modalCloseBtn" onclick="$.modal.close();return false;" href="#null" >cerrar</a>
		</center>
		<br />
	</div>
</div>

<div id="modal_atd" style="display:none;">
	<div class="boxContentsPp">
<?php
echo $objSiteData->landingDataTransferFile;
?>
		<br /><br />
		<center>
			<a class="modalCloseBtn" onclick="$.modal.close();return false;" href="#null" >cerrar</a>
		</center>
		<br />
	</div>
</div>

<div id="legal_notices" style="display:none;">
	<div class="boxContentsLegalNotices">
<?php
echo $objSiteData->landingTermsFile;
?>
		<br /><br />
		<center>
			<a class="modalCloseBtn" onclick="$.modal.close();return false;" href="#null" >cerrar</a>
		</center>
		<br />
	</div>
</div>

<div id="footerWebPage" style="display:block;"></div>
