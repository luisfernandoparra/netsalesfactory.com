<?php
$idLandingFormatted=str_pad($objSiteData->landingId,3,'0',STR_PAD_LEFT);
$paramCityLanding='city';	// NOMBRE DEL PARAMETRO QUE IDENTIFICA LA CIUDAD PARA DIBUJAR TAMBIEN EL TLF CORRESPONDIENTE

$arrCitiesName=array(
'alicante' => 'Alicante',
'albacete' => 'Albacete',
'badajoz' => 'Badajoz',
'barcelona' => 'Barcelona',
'bilbao' => 'Bilbao',
'burgos' => 'Burgos',
'caceres' => 'Cáceres',
'ciudad-real' => 'Ciudad Real',
'gerona' => 'Gerona',
'las-palmas' => 'Las Palmas',
'la-rioja' => 'La Rioja',
'lleida' => 'Lleida',
'madrid' => 'Madrid',
'malaga' => 'Málaga',
'murcia' => 'Murcia',
'pontevedra' => 'Pontevedra',
'salamanca' => 'Salamanca',
'sevilla' => 'Sevilla',
'tarragona' => 'Tarragona',
'valencia' => 'Valencia',
'valladolid' => 'Valladolid',
'vitoria' => 'Vitoria',
'zaragoza' => 'Zaragoza'
);

$arrCitiesNameTop=array(
'alicante' => 'alicante',
'albacete' => 'albacete',
'badajoz' => 'badajoz',
'barcelona' => 'barcelona',
'bilbao' => 'bilbao',
'burgos' => 'burgos',
'caceres' => 'caceres',
'ciudad-real' => 'ciudad_real',
'gerona' => 'gerona',
'las-palmas' => 'las_palmas',
'la-rioja' => 'la_rioja',
'lleida' => 'lleida',
'madrid' => 'madrid',
'malaga' => 'malaga',
'murcia' => 'murcia',
'pontevedra' => 'pontevedra',
'salamanca' => 'salamanca',
'sevilla' => 'sevilla',
'tarragona' => 'tarragona',
'valencia' => 'valencia',
'valladolid' => 'valladolid',
'vitoria' => 'vitoria',
'zaragoza' => 'zaragoza'
);

$arrCitiesPhones=array(
'alicante' => '965 12 36 84',
'barcelona' => '93 222 40 15',
'bilbao' => '94 421 30 13',
'burgos' => '947 203 129',
'ciudad-real' => '926 920 945',
'gerona' => '968 218 331',
'las-palmas' => '928 381 102<br>928 381 202',
'madrid' => '900 102 722',
'malaga' => '952 217 346',
'murcia' => '968 21 83 31',
'pontevedra' => '986 851 606',
'salamanca' => '923 260 434',
'sevilla' => '900 102 722',
'valencia' => '963 604 688<br>963 891 692',
'valladolid' => '983 20 05 92',
'vitoria' => '945 23 01 00<br>945 14 76 68'
);

$businessHours=(date('H') >= 9 && date('H') <= 20);	// HORARIO DE OFICINA, PARA DETERMINAR EL TEXTO A MOSTRAR SOBRE EL FORMULARIO

parse_str($_SERVER['QUERY_STRING'],$arrParams);
$strNewParams='';
foreach($arrParams as $key=>$value)
{
	if($key == 'cr')
		continue;

	if($key == 'city')
	{
		$strNewParams.='prevCity='.$value.'&';
		$strNewParams.=$key.'=girona&';
		continue;
	}
	$strNewParams.=$key.'='.$value.'&';
}
$strNewParams=substr($strNewParams,0,-1);
//$hideLangChange=(!isset($_REQUEST[$paramCityLanding]) || $arrCitiesName[@$_REQUEST[$paramCityLanding]] != 'Gerona') ? '$(".language").hide();' : '';
$hideLangChange='$(".language").hide();';
?>
<script type="application/javascript">
$(document).ready(function(){
<?php
$jsTImgTopLeft='';

if(isset($_REQUEST[$paramCityLanding]) && $_REQUEST[$paramCityLanding])
{
//	$fieldSpainCity='<input type="hidden" name="spain_city" value="'.$arrCitiesName[$_REQUEST[$paramCityLanding]].'" />';

	$tmpImgLogo=isset($arrCitiesNameTop[$_REQUEST[$paramCityLanding]]) ? $arrCitiesNameTop[$_REQUEST[$paramCityLanding]] : 'hm';
	$jsTImgTopLeft.='
	$(".logo").attr("src","img/logo_'.$tmpImgLogo.'.png");
';


	if($businessHours)	// PARA HORARIO DE OFICINA
	{
		$tmpCityName=isset($arrCitiesName[$_REQUEST[$paramCityLanding]]) ? $arrCitiesName[$_REQUEST[$paramCityLanding]] : 'su ciudad';

		$jsTImgTopLeft.='
		$(".cityName").html("'.$tmpCityName.'");
	';
	}
	else	// RESTO DEL DIA
	{
		$jsTImgTopLeft.='
	$(".dayMsg").hide();
	$(".nightMsg").show();
	';
		$jsTImgTopLeft.=''.$jsTImgTopLeft;
	}

	//$tmpPhoneCity=isset($arrCitiesPhones[$_REQUEST[$paramCityLanding]]) ? $arrCitiesPhones[$_REQUEST[$paramCityLanding]] : '900 102 722';
	$tmpPhoneCity='900 102 722';	// FORZADO PARA TODOS IGUAL
	$jsTImgTopLeft.='
	$(".cityPhone").html("'.$tmpPhoneCity.'");
	$("#spain_city").val("'.$arrCitiesName[$_REQUEST[$paramCityLanding]].'");
';

	echo $jsTImgTopLeft;
}
echo $hideLangChange;
?>
//	$(".language").attr("href","http://lean.abogadosclausulasuelo.es/despatx/?<?=$strNewParams?>");
});
</script>
<header>
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
	echo $objSiteData->headerTopContent;
    
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
</header>

<section class="basic-content">
	<section class="small-content">
		<section class="skeleton">
<?php

if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div class="marco">
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!
			</div>
			<span class="bolo"></span>
';
}

?>
	</section>
	<section class="formulario">

		<form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
			<input type="hidden" name="destino" id="destino" value="">
			<input type="hidden" name="spain_city" id="spain_city" value="" />
<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
$formScriptName='form_script_'.$idLandingFormatted.'.php';
}

$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
include($formScriptName);
?>

			</form>
		</section>
	</section>
</section>

<section class="text-content">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;

//	if($arrParams['city'] == 'sevilla')	// EXCEPCION SOLO PARA SEVILLA (M.F. 2017.06.21)
//	{
//		echo '<script>$(".box1").hide();$(".box2, .box3").css("width","47%");</script>';
//	}

}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
</section>

<footer>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
</footer>
