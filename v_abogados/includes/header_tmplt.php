<?php
//header('X-Content-Type-Options:nosniff');header('X-Frame-Options:SAMEORIGIN');header('X-XSS-Protection:1;mode=block');
$sec_tok='0'.$id_client.'_'.'DJK'.date('dmd').$arr_creas[$id_crea]['nombpromo'];
$sec_tok=md5($sec_tok);
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$arr_creas[$id_crea]['title']?></title>
<?php
//	ONLY FOR MOBILE DEVICES
//echo '$layoutType='.$_SERVER['SERVER_NAME'];
if($is_movil == 1 || $layoutType)
{
?>
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-control" content="no-store" />

<?php
}
if(count(@$objSiteData->SeoSiteParams))
{
	if(@$objSiteData->SeoSiteParams['meta_canonical'])
		echo '<link rel="canonical" href="'.$objSiteData->SeoSiteParams['meta_canonical'].'" />
';
//if($debugModeNew)echo'<pre>';print_r($objSiteData->SeoSiteParams);echo'</pre>';

}
// NOTA: SE HA INTENCIONALMENTE COLOCADO UNA VARIABLE CON TRES DIGITOS (PRECEDIDOS DE CEROS) PARA EL CSS PRINCIPAL
$defaultCSS='000';
@$localLandingCssFile='css/'.$prefixCss.'css_'.str_pad($id_crea,3,'0',STR_PAD_LEFT).'.css';
$localLandingCssFile=(file_exists($path_raiz_includes_local.$localLandingCssFile)) ? $path_raiz_aplicacion_local.$localLandingCssFile : $path_raiz_aplicacion_local.'css/'.$prefixCss.'css_'.$defaultCSS.'.css';

{
	echo '<link rel="icon" href="img/favicon_lean.png" type="image/x-icon" />
<link rel="shortcut icon" href="img/favicon_lean.png" type="image/x-icon" />
';
}


$jsAddParam='';	// OBJETO PARA PARAMETROS ADICIONALES (M.F. 2017.06.26)

if(isset($_REQUEST['addParam']) && count($_REQUEST['addParam']))
{
	$jsAddParam='{';
	foreach($_REQUEST['addParam'] as $key=>$value)
	{
		$jsAddParam.='"'.mysql_escape_string($key).'":"'.mysql_escape_string($value).'",';
	}

	$jsAddParam=substr($jsAddParam,0,-1);
	$jsAddParam=strip_tags($jsAddParam).'}';
}
?>

<base href="<?=$path_raiz_aplicacion_local?>" />
<link href="<?=$path_raiz_aplicacion_local?>js/modal/css/modal.css" rel="stylesheet" type="text/css" />
<link href="<?=$path_raiz_aplicacion_local?>js/facebox/facebox.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$path_raiz_aplicacion_local?>css/common.css" rel="stylesheet" type="text/css" />
<link href="<?=$localLandingCssFile?>" rel="stylesheet" type="text/css" />


<script type="application/javascript">
var root_path = "<?=$path_raiz_aplicacion;?>";<?php /*ruta global para las imgs del facebox y del modal*/?>
var root_path_local = "<?=$path_raiz_aplicacion_local;?>";<?php /*ruta global para ajax*/?>
var nomb_promo = "<?=$arr_creas[$id_crea]['nombpromo']?>";
var id_source = "<?=(int)$id_source?>";
var id_client = "<?=$id_client?>";
var id_crea = <?=(int)$id_crea?>;
var layoutType = <?=(int)$layoutType?>;	// M.F. 2014.12.18
var thankyoupage = "<?=$url_thankyouPage?>";
var arr_campanas = [<?php  echo $campanas?>];
var is_ctc = "<?=trim($is_ctc)?>";
var is_movil = "<?=trim($is_movil)?>";
var root_path_ws = "<?=$path_raiz_ws;?>";
var url_ws = "<?=$url_ws;?>";
var file_ws = "<?=$file_ws;?>";
var arr_pixel_JS = ["<?=$arr_pixel_JS[0];?>","<?=$arr_pixel_JS[1]?>","<?=$arr_pixel_JS[2]?>"];
var tduid = "<?=$tduid?>";
var trdbl_organization = "<?=$organization?>"; //Pixel Tradedoubler. Organizacion
var trbdl_event = "<?=$event?>";
var trbdl_program = "<?php echo $trbdl_program; ?>";
var nombCliente = "<?=$nombCliente?>";
var is_prov = "<?=trim($is_prov)?>";
var sec_tok= "<?=trim($sec_tok)?>";

var refC = "<?=(int)$id_crea?>";

var addparam=<?=$jsAddParam ? $jsAddParam : '""'?>;

var googleSEM = "";
var googleRemarketingOnly = <?=$objSiteData->semGoogleParams['remarketing_only'] ? 1 : 0?>;
var googleSEO = 0;
var google_conversion_id;
</script>
<?php
$blockBodySEM='';
//$remarketingLandingAdWords=false;	// CONTROL FOR GOOGLE REMARKETING ONLY (2014.11.21 - M.F.)

/*
 * SI LA PROMO TIENE DATOS PARA SEM (Search Engine Marketing)
 * se rellena el array correspondiente para construir
 * el pixel correspondiente (M.F. 2014.06.26)
 */

if(count($objSiteData->semGoogleParams))
{
	if(@$objSiteData->semGoogleParams['google_ga'])	// SOLO PARA `seo` Y PARA LAS LANDINGS ´SEM´ QUE TENGAN CONFIGURADO EL `id GA`
	{
		echo '
<script type="application/javascript">
var googleSEO = "'.$objSiteData->semGoogleParams['google_ga'].'";
</script>
';
	}

	$outDem='//www.googleadservices.com/pagead/conversion/';
	$outJs='
/* <![CDATA[ */
';
	$outJsAcceptRegister='';

	foreach($objSiteData->semGoogleParams as $key=>$value)
	{
		switch($key)
		{
			case 'conversion_id';
				$outDem.=$value.'/';
				$bodyJsGoogle[$key]=$value;
				$outJs.='var google_'.$key.'='.$value.';
';
				break;
			case 'remarketing_only';
				$outDem.=$value.'/';
				$bodyJsGoogle[$key]=$value ? 'true' : 'false';
				$outJs.='var google_'.$key.'=true;
';
				echo '<script>var xxx_'.$key.'=true;</script>';
				break;
			case 'conversion_label';
				if($key != 'remarketingLandingAdWords')	// SKIP GOOGLE REMARKETING ONLY EXCEPTION PARAM (2014.11.21 - M.F.)
				{
					$bodyJsGoogle[$key]=$value;
					$outDem.='?label='.$value.'&amp;guid=ON&amp;script=0';
				}

				break;
			case 'id_sem';
			case 'id_landing';
				break;
			default:
				$bodyJsGoogle[$key]=$value;
		}
	}
	$outJs.='var google_custom_params=window.google_tag_params;
';
	$outJs.='
/* ]]> */
';

	$commonGoogleParams='
		var google_conversion_id = '.$bodyJsGoogle['conversion_id'].';
		var google_conversion_language = "'.$bodyJsGoogle['conversion_language'].'";
		var google_remarketing_only = '.$bodyJsGoogle['remarketing_only'].';
';

	if($objSiteData->semGoogleParams['remarketing_only'])
		$specificGoogleParams='';
	else
		$specificGoogleParams='
		var google_conversion_format = "'.$bodyJsGoogle['conversion_format'].'";
		var google_conversion_color = "'.$bodyJsGoogle['conversion_color'].'";
		var google_conversion_label = "'.($bodyJsGoogle['conversion_label'] === 'remarketingLandingAdWords' ? '' : $bodyJsGoogle['conversion_label']).'";
';

	$blockBodySEM='
<script type="application/javascript">
'.$outJs.$outJsAcceptRegister.'googleSEM="'.$outDem.'";

$(document).ready(function(){
});

	function googleAdWords()
	{
		/* <![CDATA[ /
		'.$commonGoogleParams.'
		'.$specificGoogleParams.'
		/ ]]> */
		includeScript("//www.googleadservices.com/pagead/conversion.js","js");
	}

</script>
';
}

/*
 * EXCEPCION GOOGLE REMARKETING ONLY EXCEPTION PARAM
 * en la BBDD el campo conversion_label debe ser igual a "remarketingLandingAdWords"
 * (M.F. 2014.11.21)
 */
//echo '-->'.$objSiteData->semGoogleParams['remarketing_only'];
if($objSiteData->semGoogleParams['remarketing_only'])
{
	$blockBodySEM.='
<script type="application/javascript">
/* <![CDATA[ */
var google_conversion_id = '.$objSiteData->semGoogleParams['conversion_id'].';
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>

<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/'.$objSiteData->semGoogleParams['conversion_id'].'/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
';
}

// CHECK PARA SABER SI SE DEBE VISUALIZAR EL BLOQUE DEL DESPLEGABLE DE LAS PROVINCIAS (M.F. 2017.07.24)
/**
 * @author LFP (2018.02.15)
 * LA variable $composeCitiesBlock estaba mal validada. No solamente tiene que venir el  campo city, sino que debe existir [corregido]
 */

$spainProvinces=array(
//	'alava'=>'Alava',
//	'albacete'=>'Albacete',
	'alicante'=>'Alicante',
//	'almeria'=>'Almería',
//	'asturias'=>'Asturias',
//	'avila'=>'Avila',
	'badajoz'=>'Badajoz',
	'barcelona'=>'Barcelona',
	'bilbao'=>'Bilbao',
	'burgos'=>'Burgos',
	'caceres'=>'Cáceres',
//	'cadiz'=>'Cádiz',
//	'cantabria'=>'Cantabria',
//	'castellon'=>'Castellón',
	'ciudad-real'=>'Ciudad Real',
//	'cordoba'=>'Córdoba',
//	'la-coruna'=>'La Coruña',
//	'cuenca'=>'Cuenca',
	'gerona'=>'Gerona',
	'granada'=>'Granada',
//	'guadalajara'=>'Guadalajara',
//	'guipuzcoa'=>'Guipúzcoa',
//	'huelva'=>'Huelva',
//	'huesca'=>'Huesca',
//	'islas-baleares'=>'Islas Baleares',
//	'jaen'=>'Jaén',
	'leon'=>'León',
	'lleida'=>'Lleida',
//	'lerida'=>'Lérida',
//	'lugo'=>'Lugo',
	'madrid'=>'Madrid',
	'malaga'=>'Málaga',
	'murcia'=>'Murcia',
//	'navarra'=>'Navarra',
//	'orense'=>'Orense',
	'oviedo'=>'Oviedo',
//	'palencia'=>'Palencia',
	'las-palmas'=>'Las Palmas',
	'pamplona'=>'Pamplona',
//	'pontevedra'=>'Pontevedra',
	'la-rioja'=>'La Rioja',
	'salamanca'=>'Salamanca',
//	'segovia'=>'Segovia',
	'sevilla'=>'Sevilla',
//	'soria'=>'Soria',
	'tarragona'=>'Tarragona',
//	'santa-cruz-de-tenerife'=>'Santa Cruz de Tenerife',
//	'teruel'=>'Teruel',
//	'toledo'=>'Toledo',
	'valencia'=>'Valencia',
	'valladolid'=>'Valladolid',
//	'vizcaya'=>'Vizcaya',
	'zamora'=>'Zamora',
	'zaragoza'=>'Zaragoza'
);

$composeCitiesBlock=isset($_GET['city']) && $_GET['city'] !='' && array_key_exists(strtolower(trim($_GET['city'])), $spainProvinces)? false : true;

?>
<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/jquery-1.10.1.min.js"></script>
<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/jquery.validate.min.js"></script>
<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/modal/js/jquery.simplemodal.min.js"></script>
<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/facebox/facebox.min.js"></script>
<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/funciones_comunes.min.js"></script>

<script>
var validator;

$(document).ready(function(){
	$("#telefono").val("");

	$("#enviarPorMailSolicitaInfo").validate({	// CHECK PHONE NUMBER
		onkeyup:false,
		onclick:false,
		submitHandler:function(form){},
		rules:{
			nombre:{
				required:true,
				dataFilter:function(value){
					var patt = /[a-z]/;
				}
			},
			telefono:{
				required:true,
				remote:{
					url:root_path_local+"includes/phone_check.php",
			    contentType:"application/json; charset=utf-8",
					data:{cr:<?=(int)$id_crea?>,hashPipe:"<?=$controlChekAlgorithm?>"},
					dataFilter:function(response){
						var jsonResponse="";
						jsonResponse = JSON.parse(response);
//							setVisitorAction("res_phone_check:", (jsonResponse.success ? jsonResponse.success : jsonResponse.errorMessage))
							if(jsonResponse.success == false)
							{
								return "\"" + jsonResponse.errorMessage + "\"";
							}else{
								return true;
							}
						return false;
					}
				}
			}
			,email:{
				required:true,
				remote:{
					url:root_path +"includes/api_email_check.php",
			    contentType:"application/json; charset=utf-8",
					data:{cr:<?=(int)$id_crea?>,id_client:id_client, hashPipe:"<?=$controlChekAlgorithm?>"},
					beforeSend: function(){
						$("#btnProcesar").prop("disabled","true").css("opacity","0.8");
					},
					complete: function(){
						$("#btnProcesar").prop("disabled","").css("opacity","1");
					},
					dataFilter:function(response){
						var jsonResponse="";
						jsonResponse=JSON.parse(response);
//							setVisitorAction("res_email_check:", (jsonResponse.success ? jsonResponse.success : jsonResponse.errorMessage))
							if(jsonResponse.success == false)
							{
								return "\"" + jsonResponse.errorMessage +  "\"";
							}else{
								return true;
							}
						return false;
					}
				}
			}
		},
		errorLabelContainer: $("#enviarPorMailSolicitaInfo div.error")
	});


	validator=$("#enviarPorMailSolicitaInfo").validate({
		errorContainer:$('div.error'),
		errorLabelContainer:$('div.error ul'),
		wrapper: 'li'
	})

	$("select[name='province']").change(function(){
		$("input[name='spain_city']").val(this.value);
	});
<?php
if(isset($_GET['city']) && $_GET['city'])	// FORZAR LA INCLUSION DE LA CIUDAD SI ES CORRECTA Y LLEGA POR URL (M.F. 2018.02.05)
{
	$tmpUtf=strtr(utf8_decode($_GET['city']), utf8_decode('ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),'AAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');

	foreach($spainProvinces as &$tmpValue)
	{
		$tmpValue=strtolower($tmpValue);
		$tmpValue=strtr(utf8_decode($tmpValue), utf8_decode('ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),'AAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');

		if($tmpValue == strtolower($tmpUtf))
		{
			echo 'setTimeout(\'$("#spain_city").val("'.$_GET['city'].'");\',500);';
			break;
		}
	}
}
?>

// START LOPD CODE MODIFICATIONS
	$(".link_pp").click(function(event){
		event.stopPropagation();
<?php
// SI LA LONGITUD DE LA CADENA ES INFERIOR A 100, SE CONSIDERA COMO LINK Y SE ABRE UNA VENTANA EXTERNA PARA CARGAR EL CONTENIDO
if(isset($arr_creas[$objSiteData->landingId]['protecciondatos']) && (strlen($arr_creas[$objSiteData->landingId]['protecciondatos']) < 100))
	echo 'this.href="'.$arr_creas[$objSiteData->landingId]['protecciondatos'].'";setVisitorAction(this," [external_link]");return;';
?>
		$("#modal_pp").modal({
			containerCss:{
				backgroundColor:"#fff",
				borderColor:"#000",
				height:"80%",
				padding:4,
				width:"80%"
			}
			,overlayCss: {backgroundColor:"#aaa"}
			,onClose:function(dialog){dialog.data.fadeOut('fast',function(){dialog.container.hide('fast',function(){dialog.overlay.fadeOut('fast',function(){$.modal.close();});});});}
		});
		setVisitorAction(this);
		return false;
	});

	$(".modal_atd").click(function(event){
		event.stopPropagation();
<?php
// SI LA LONGITUD DE LA CADENA ES INFERIOR A 100, SE CONSIDERA COMO LINK Y SE ABRE UNA VENTANA EXTERNA PARA CARGAR EL CONTENIDO
if(isset($arr_creas[$objSiteData->landingId]['landingDataTransferFile']) && (strlen($arr_creas[$objSiteData->landingId]['landingDataTransferFile']) < 100))
	echo 'this.href="'.$arr_creas[$objSiteData->landingId]['landingDataTransferFile'].'";setVisitorAction(this," [external_link]");return;';
?>
		$("#modal_atd").modal({
			containerCss:{
				backgroundColor:"#fff",
				borderColor:"#000",
				height:"80%",
				padding:4,
				width:"80%"
			}
			,overlayCss: {backgroundColor:"#aaa"}
			,onClose:function(dialog){dialog.data.fadeOut('fast',function(){dialog.container.hide('fast',function(){dialog.overlay.fadeOut('fast',function(){$.modal.close();});});});}
		});
		setVisitorAction(this);
		return false;
	});

	$(".legal_notices").click(function(event){
		event.stopPropagation();
<?php
// SI LA LONGITUD DE LA CADENA ES INFERIOR A 100, SE CONSIDERA COMO LINK Y SE ABRE UNA VENTANA EXTERNA PARA CARGAR EL CONTENIDO
if(isset($arr_creas[$objSiteData->landingId]['condiciones']) && strlen($arr_creas[$objSiteData->landingId]['condiciones']) < 100)
	echo 'this.href="'.$arr_creas[$objSiteData->landingId]['condiciones'].'";setVisitorAction(this," [external_link]");return;';
?>

		$("#legal_notices").modal({
			containerCss:{
				backgroundColor:"#fff",
				borderColor:"#000",
				height:"80%",
				padding:4,
				width:"80%"
			}
			,overlayCss: {backgroundColor:"#aaa"}
			,onClose:function(dialog){dialog.data.fadeOut('fast',function(){dialog.container.hide('fast',function(){dialog.overlay.fadeOut('fast',function(){$.modal.close();});});});}
		});
		setVisitorAction(this);
		return false;
	});

	$("a[href^='#inner_modal_']").click(function(event){
		event.stopPropagation();
		$.modal("<div class='modallegalInfo'><center><br><br><br><span style='display:inline-block;padding:0px;margin-right:5%;width:50px;'><img src='<?=$path_raiz_aplicacion?>img/wait_200_200.png' class='loadingModal no-style'></span></center></div>",{
			containerCss:{
				backgroundColor:"#fff",
				borderColor:"#000",
				padding:8,
				width:"65%"
			}
			,overlayCss: {backgroundColor:"#aaa"}
			,onClose:function(dialog){dialog.data.fadeOut('fast',function(){dialog.container.hide('fast',function(){dialog.overlay.fadeOut('fast',function(){$.modal.close();});});});}
		});

		drawModalData(this);
		setVisitorAction(this);
		return false;
	});

	$(".cookie_info").click(function(){
		$("#modal_info_cook").modal({
			containerCss:{
				backgroundColor:"#fff",
				borderColor:"#000",
				height:"80%",
				padding:4,
				width:"80%"
			}
			,overlayCss: {backgroundColor:"#aaa"}
			,onClose:function(dialog){dialog.data.fadeOut('fast',function(){dialog.container.hide('fast',function(){dialog.overlay.fadeOut('fast',function(){$.modal.close();});});});}
		});
		setVisitorAction(this);
		return false;
	});

	$('body').on("click",".Xbutton",function(){
		$.modal.close();
		setVisitorAction(this);
	});

	toggleMobile('boxMobile',1);	// PANEL LATERAL INFOS LEGALES
// END LOPD CODE MODIFICATIONS

//	$(".simplemodal-close").click(function(){
//console.log("CCCCC: ",this)
//	})

});

// ***** START PANEL LATERAL && LOPD
var arrInnerModalContent=new Array();
var arrInnerModalHeaderTitle=new Array();

arrInnerModalHeaderTitle["responsible"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_title_responsible'])?>';
arrInnerModalHeaderTitle["purpose"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_title_purpose'])?>';
arrInnerModalHeaderTitle["legitimation"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_title_legitimation'])?>';
arrInnerModalHeaderTitle["recipients"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_title_recipients'])?>';
arrInnerModalHeaderTitle["rights"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_title_rights'])?>';
arrInnerModalHeaderTitle["origin"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_title_origin'])?>';

arrInnerModalContent["responsible"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_content_responsible'])?>';
arrInnerModalContent["purpose"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_content_purpose'])?>';
arrInnerModalContent["legitimation"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_content_legitimation'])?>';
arrInnerModalContent["recipients"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_content_recipients'])?>';
arrInnerModalContent["rights"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_content_rights'])?>';
arrInnerModalContent["origin"]='<?=str_replace("\r\n",'',$objSiteData->lopd['lopd_content_origin'])?>';

function drawModalData(data)	// DISPLAY MODAL LOPD DATA
{
	var strNoContents="<span style=display:inline-block;height:20%;color:red;width:100%;text-align:center;>no se ha especificado el texto</span>";
	var idElem=data.getAttribute("href").substr(13,2);
	var idElem=Math.ceil(idElem);
	var refType=data.getAttribute("data_ref_type") ? data.getAttribute("data_ref_type") : 0;
	lastDataType=refType;

	idElem=data.getAttribute("data-info").substr(26, 20) ? data.getAttribute("data-info").substr(26, 20) : 0;
	var tmpContent=arrInnerModalContent[idElem] ?  arrInnerModalContent[idElem] : strNoContents;
	var tmpTitle=arrInnerModalHeaderTitle[idElem] ?  arrInnerModalHeaderTitle[idElem] : "<span style=text-align:center;>SIN TITULAR</span>";
	$(".modallegalInfo").html("<div class='innerModalContent'><div class='innerModalHeader'><div class='innerModalTitle'>"+tmpTitle+"</div></div>"+tmpContent+" <center><div class='modalCloseBtn Xbutton'>cerrar</div></center></div></div>");

	return false;
}

function serverError(response)
{
	$.modal(""+response.msg+"",{
		containerCss:{
			backgroundColor:"#fff",
			borderColor:"#000",
			padding:8,
			height:"30%",
			width:"60%"
		}
		,overlayCss: {backgroundColor:"#aaa"}
		,onClose:function(dialog){dialog.data.fadeOut('fast',function(){dialog.container.hide('fast',function(){dialog.overlay.fadeOut('fast',function(){$.modal.close();});});});}
	});
}

function setVisitorAction(el,param)
{
	var tmpParam=(typeof el == "object") ? el.getAttribute("data-info") : el;
	if(!param)
		param="";

	if(tmpParam)
	{
		param=tmpParam+" / "+param;
	}
	else
	{
		if(el.getAttribute("class"))
			param=el.getAttribute("class")+" / "+param;
	}

	$.ajax({async:true,url:root_path_local+"ajaxs/visitor_actions.php",method:"post",dataType:"json",data:{action:"setVisitorAction",detAction:param},cache:false,async:false
		,success:function(response){
			if(response.error)
			{
				if(response.error == 2)
					document.location=root_path_local+"?<?=$_SERVER['QUERY_STRING']?>";

				setTimeout(function(){$.modal.close();},10);
				setTimeout(function(){serverError(response);},1000);
				return false;
			}
		}
	});
	return;
}

function toggleMobile(id,who)
{
	var el=document.getElementById(id);
	var img=document.getElementById("arrowMobile");
	var boxMobile=el.getAttribute("class");
	var currAct="hideLopdPanel";

	if(boxMobile == "defaultSideSlider"){
		el.setAttribute("class", "leftSidelSider");
		delayPanel(img, "../img/left_arrow_slider.png", 400);
		currAct="showLopdPanel";
	}
	else{
		el.setAttribute("class", "defaultSideSlider");
		delayPanel(img, "../img/right_arrow_slider.png", 400);
	}

	if(!who) setVisitorAction("LOPD: ",currAct);
}

function delayPanel(elem, src, delayTime){
	window.setTimeout(function(){elem.setAttribute("src", src);}, delayTime);
}
// ***** END PANEL LATERAL && LOPD

</script>

<?php
if(!empty($arr_creas[$id_crea]['js']) && trim($arr_creas[$id_crea]['js']) != '' )
{
?>
	<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/<?=trim($arr_creas[$id_crea]['js'])?>"></script>
<?php

}else{
?>
	<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/validacion.min.js" async="async"></script>
<?php
}

if($layoutType && $arr_creas[$id_crea]['mobile_auto_modal_on_open'])  // GESTION MODAL "LLAMAME"
{
  echo '
<style>
.modalCllame{
display:block;
width:100%;
left:0px!important;
color:#009CD9;
}

.closeModalCall{
color:#000;
text-decoration:none;
}
.callMeText{
font-size:150%;
font-weight:bold;
}
.modalCloseImg{display:none!important;}

</style>';
}
?>
<style type="text/css">
label{display:block;text-align:left;color:#0178c8;font-weight:bold;padding-top:0px;margin-bottom:5px;}
/*estilos mensajes error*/
div.error1{display : none; border:2px solid #D81E05; }
div.error1{margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
div.error{ display : none; border:2px solid #D81E05; }
.error{color:#D81E05; background:#FCF1F0;}
div.error ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error ul li{margin:0 0 10 0;list-style-type:none;}
div.error ul li label{font-weight:normal}
div.error, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error, textarea.error{border:2px solid #D81E05;background-color:#FCF1F0;}
input.ok{background-color:#EFFFDA}

.innerModal{
display:inline-block;
background:#fff;
text-align:center;
width:100%;
height:250px;
}

.modal{
height:680px!important;
}

#simplemodal-container a.modalCloseImg{background:url(<?=$path_raiz_aplicacion_local;?>img/close_modal.png)no-repeat 2px 0px rgba(0, 0, 0, 0)!important;right:-8px;top:-14px;}
#facebox img{width:32px!important;height:1px!important;text-align:center;}
</style>
<script type="application/javascript">
var isModalDraw=false;
var leadNumber = Math.round((new Date().getTime() * Math.random()));
function offModal(){$.modal.close();}

$(document).ready(function(){
	$(".innerModal").css("height",(document.body.clientHeight)+"px");
	$("#informacion").delay(500).slideDown("slow");
});
</script>

<script type="application/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-71103740-9', 'auto');
ga('send', 'pageview');
</script>


<!--[if lte IE 8]><link rel="stylesheet" href="<?=$path_raiz_aplicacion_local?>css/ie.css" /><![endif]-->
</head>
