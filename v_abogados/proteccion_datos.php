<?php
@session_start();
include('../conf/config_web.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<!--<link href="nuevalanding/dental 21/laser_prostatico.css" rel="stylesheet" type="text/css" />-->
<?php
if($_REQUEST['deviceType'] == 'mob_'){
?>
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=2.0; user-scalable=yes;">
<meta http-equiv="Pragma" content="no-cache">
<?php
}
?>

<style>
.Part{
	font-family:arial,verdana;
	font-size:1em!important;
}
</style>

</head>

<div class="conditions">

<h1>AVISO LEGAL DE V ABOGADOS</h1>

<p><b>Condiciones generales de uso de la web www.vabogados.com</b></p>

<p>V Abogados le informa de que el acceso y uso de la p&aacute;gina web www.vabogados.com y todos los subdominios y directorios incluidos bajo la misma: www.vabogados.com, www.accionesbankia.es, www.abogadosclausulasuelo.es, www.valoresantander.es, www.abogadospreferentes.com, www.abogadosirph.es, www.bonosbancopopular.es, est&aacute;n sujetos a los t&eacute;rminos que se detallan en este Aviso Legal.</p>

<p>Por ello, si las consideraciones detalladas en este apartado no son de su conformidad, rogamos no haga uso de ellas, ya que el uso que realice de las mismas quedar&aacute; sujeto a la aceptaci&oacute;n de los t&eacute;rminos legales recogidos en este texto. El acceso a esta p&aacute;gina web es responsabilidad exclusiva de los USUARIOS y supone aceptar y conocer las advertencias legales, condiciones y t&eacute;rminos de uso contenidos en ella.</p>

<p><b>Informaci&oacute;n legal</b></p>

<p>Conforme a lo dispuesto en el Art. 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Informaci&oacute;n y de Comercio Electr&oacute;nico (LSSI-CE) y la Ley 56/2007, de 28 de diciembre, de Medidas de Impulso de la Sociedad de la Informaci&oacute;n, se informa que las presentes p&aacute;ginas web: www.vabogados.com, www.accionesbankia.es, www.abogadosclausulasuelo.es, www.valoresantander.es, www.abogadospreferentes.com, www.abogadosirph.es, www.bonosbancopopular.es, pertenecen a Viciano Servicios Jur&iacute;dicos, S.L.P., con domicilio social sito en la calle Antonio Maura, 16 – 3º Izq. 28014, Madrid,  correo electr&oacute;nico: despacho@viciano.com y n&uacute;mero de tel&eacute;fono: 91 308 17 96 e identificada con n&uacute;mero de CIFN-86103652 e inscrita en el Registro Mercantil de Madrid, Tomo 28.474, libro 0, folio 71, Secci&oacute;n 8ª, hoja n&uacute;mero M-512667, inscripci&oacute;n 1ª.</p>

<p><b>POL&iacute;TICA DE PRIVACIDAD V ABOGADOS</b></p>

<p><b>Pol&iacute;tica de privacidad y protecci&oacute;n de datos</b></p>

<p>Viciano Servicios Jur&iacute;dicos, S.L.P. (en adelante “V Abogados”) creadora de este sitio web cumple con las directrices de la Ley Org&aacute;nica 15/1999 de 13 de diciembre de Protecci&oacute;n de Datos de Car&aacute;cter Personal, el Real Decreto 1720/2007 de 21 de diciembre por el que se aprueba el Reglamento de desarrollo de la Ley Org&aacute;nica y dem&aacute;s normativa vigente en cada momento, y vela por garantizar un correcto uso y tratamiento de los datos personales del usuario.</p>

<p>Para ello, junto a cada formulario de recabo de datos de car&aacute;cter personal, en los servicios que el usuario pueda solicitar a V Abogados, har&aacute; saber al usuario de la existencia y aceptaci&oacute;n de las condiciones particulares del tratamiento de sus datos en cada caso, inform&aacute;ndole de la responsabilidad del fichero creado, la direcci&oacute;n del responsable, la posibilidad de ejercer sus derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n u oposici&oacute;n, la finalidad del tratamiento y las comunicaciones de datos a terceros en su caso.</p>

<p>Asimismo, V Abogados informa que da cumplimiento a la Ley 34/2002 de 11 de julio, de Servicios de la Sociedad de la Informaci&oacute;n y el Comercio Electr&oacute;nico y le solicitar&aacute; su consentimiento al tratamiento de su correo electr&oacute;nico con fines comerciales en cada momento.</p>

<p><b>Propiedad Intelectual e Industrial</b></p>

<p>VAbogados por si o como cesionaria, es titular de todos los derechos de propiedad intelectual e industrial de su p&aacute;gina web, as&iacute; como de los elementos contenidos en la misma (a t&iacute;tulo enunciativo, im&aacute;genes, sonido, audio, v&iacute;deo, software o textos; marcas o logotipos, combinaciones de colores, estructura y dise&ntilde;o, selecci&oacute;n de materiales usados, programas de ordenador necesarios para su funcionamiento, acceso y uso, etc.), titularidad de V Abogados o bien de sus licenciantes.</p>

<p>Todos los derechos reservados. En virtud de lo dispuesto en los art&iacute;culos 8 y 32.1, p&aacute;rrafo segundo, de la Ley de Propiedad Intelectual, quedan expresamente prohibidas la reproducci&oacute;n, la distribuci&oacute;n y la comunicaci&oacute;n p&uacute;blica, incluida su modalidad de puesta a disposici&oacute;n, de la totalidad o parte de los contenidos de esta p&aacute;gina web, con fines comerciales, en cualquier soporte y por cualquier medio t&eacute;cnico, sin la autorizaci&oacute;n de V Abogados. El usuario se compromete a respetar los derechos de Propiedad Intelectual e Industrial titularidad de V Abogados. Podr&aacute; visualizar los elementos del portal e incluso imprimirlos, copiarlos y almacenarlos en el disco duro de su ordenador o en cualquier otro soporte f&iacute;sico siempre y cuando sea, &uacute;nica y exclusivamente, para su uso personal y privado. El usuario deber&aacute; abstenerse de suprimir, alterar, eludir o manipular cualquier dispositivo de protecci&oacute;n o sistema de seguridad que estuviera instalado en el las p&aacute;ginas de V Abogados.</p>


<p><b>Exclusi&oacute;n de garant&iacute;as y responsabilidad</b></p>

<p>La empresa no se hace responsable, en ning&uacute;n caso, de los da&ntilde;os y perjuicios de cualquier naturaleza que pudieran ocasionar, a t&iacute;tulo enunciativo: errores u omisiones en los contenidos, falta de disponibilidad del portal o la transmisi&oacute;n de virus o programas maliciosos o lesivos en los contenidos, a pesar de haber adoptado todas las medidas tecnol&oacute;gicas necesarias para evitarlo.</p>

<p><b>Modificaciones de la p&aacute;gina, condiciones duraci&oacute;n</b></p>

<p>V Abogados se reserva el derecho de efectuar sin previo aviso las modificaciones que considere oportunas en su portal, pudiendo cambiar, suprimir o a&ntilde;adir tanto los contenidos y servicios que se presten a trav&eacute;s de la misma como la forma en la que &eacute;stos aparezcan presentados o localizados en su portal. Adem&aacute;s podr&aacute; modificar en cualquier momento las condiciones aqu&iacute; determinadas, siendo debidamente publicadas como aqu&iacute; aparecen.</p>

<p>La vigencia de las citadas condiciones ir&aacute; en funci&oacute;n de su exposici&oacute;n y estar&aacute;n vigentes hasta que sean modificadas por otras debidamente publicadas.</p>

<p><b>Derecho de exclusi&oacute;n</b></p>

<p>Desde V Abogados nos reservamos el derecho a denegar o retirar el acceso a portal y/o los servicios ofrecidos sin necesidad de preaviso, a instancia propia o de un tercero, a aquellos usuarios que incumplan las presentes Condiciones Generales de Uso.</p>

<p><b>Generalidades</b></p>

<p>V Abogados perseguir&aacute; el incumplimiento de las presentes condiciones as&iacute; como cualquier utilizaci&oacute;n indebida de su portal ejerciendo todas las acciones civiles y penales que le puedan corresponder en derecho.</p>

<p><b>Legislaci&oacute;n aplicable y jurisdicci&oacute;n</b></p>

<p>La relaci&oacute;n entre V Abogados y el usuario se regir&aacute; por la normativa espa&ntilde;ola vigente y cualquier controversia se someter&aacute; a la jurisdicci&oacute;n de los Tribunales de Madrid, a la que el usuario se somete expresamente.</p>

</div>
<br /><br />
</BODY>
</html>
<?
//print_r($_REQUEST['deviceType']);
?>