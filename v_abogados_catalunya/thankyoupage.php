<?php
/**
 * Página de gracias
 */
@session_start();
include('../conf/config_web.php');	// ONLY GLOBAL CONFIG
ini_set('display_errors',0);
include('conf/config_web.php');
include($path_raiz_includes . 'includes/initiate_thankyoupage.php');
include($path_raiz_includes . 'class/class.pixels.management.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>¡Gracias de <?php echo ucwords($landingMainName); ?>!</title>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width,initial-scale=1"/> 
<link href="<?php echo $path_raiz_aplicacion_local; ?>css/estilos_thanks.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- ************************* 
    PIXELES DE MAKE 
****************************** -->
<!-- Google Tag Manager  for MAKE -->
<script>
dataLayer = [{
	'pageCategory':'signup',
	'visitorType':'high-value',
	'transactionTotal':'1',
	'transactionId':'<?=$_REQUEST['leadNumber']?>',
	'template' : 'thankyoupage'
}];
</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TFTJXD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TFTJXD');</script>
<!-- End Google Tag Manager for MAKE -->


<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-71103740-9', 'auto');
ga('send', 'pageview');
</script>

	<header>
		<section class="title-head">
			<img class="logo" src="<?php echo $path_raiz_aplicacion_local; ?>img/logo.png" alt="Valores Santander">
			<img class="telefono" src="<?php echo $path_raiz_aplicacion_local; ?>img/telefono.png" alt="900 102 722">
		</section>
	</header>

  <section class="content">
		<img class="fondo" src="<?php echo $path_raiz_aplicacion_local; ?>img/fondo.jpg" alt="Valores Santander"/>
		<img class="img_mobile" src="<?php echo $path_raiz_aplicacion_local; ?>img/img_mobile.jpg" alt="Valores Santander"/>
  </section>

	<footer>
		<p class="pie"></p>
	</footer>

<?php
if (!empty($_REQUEST['debug'])) {
    new dBug($_REQUEST);
}
echo $path_raiz_aplicacion_local
include($path_raiz_includes . 'includes/pixels_thankyoupage.php');
session_destroy();
?>
</body>
</html>