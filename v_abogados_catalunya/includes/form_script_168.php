<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
?>
						<div class="error"><ul></ul></div>
						<p>SOL&middot; LICITI INFORMACI&Oacute;</p>

						<div class="fila">
							<div class="fleft">
								<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda user-name" maxlength="100" required data-msg-required="El camp &lt;strong&gt;Nom i cognoms&lt;/strong&gt; &eacute;s obligatori" placeholder=" Nom i cognoms" aria-required="true" />
							</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<input type="tel" maxlength="9" name="telefono" id="telefono" class="celda user-phone" required data-msg-required="El camp &lt;strong&gt;Tel&egrave;fon&lt;/strong&gt; &eacute;s obligatori" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El camp &lt;strong&gt;Tel&egrave;fon&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El camp &lt;strong&gt;Tel&egrave;fon&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder=" Tel&egrave;fon" aria-required="true" />
							</div>
						</div>

						<div class="row">
							<div class="fleft">
								<input type="email" name="email" id="email" maxlength="100" class="celda user-mail" required data-msg-required="El camp &lt;strong&gt;E-mail&lt;/strong&gt; &eacute;s obligatori" data-msg-email="El camp &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder=" E-mail" aria-required="true" />
							</div>
						</div>


						<div class="legal" style="padding-top:10px;">
							<input required="" data-msg-required="Has de llegir i marcar la casella de la &lt;strong&gt;Pol&iacute;tica de privacitat&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Has de llegir i marcar la casella de la política de privacitat" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;He llegit i accepto la
							<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'privacy_policies.php';?>" data-ref="" target="_blank">Pol&iacute;tica de privacitat</a>
						</div><!--/12-->

						<div class="row">
							<div class="espacio-btn">
								<input class="green sendData" type="button" id="btnProcesar" name="btnProcesar" value="INFORMI'S">
							</div>
						</div>

						<div class="clearfix"></div>

