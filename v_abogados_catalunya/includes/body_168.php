<?php
$idLandingFormatted=str_pad($objSiteData->landingId,3,'0',STR_PAD_LEFT);
$paramCityLanding='city';	// NOMBRE DEL PARAMETRO QUE IDENTIFICA LA CIUDAD PARA DIBUJAR TAMBIEN EL TLF CORRESPONDIENTE

$arrCitiesName=array(
'barcelona' => 'Barcelona',
'girona' => 'Girona'
);

$arrCitiesNameTop=array(
'barcelona' => 'barcelona',
'girona' => 'girona'
);

$arrCitiesPhones=array(
'barcelona' => '93 222 40 15',
'girona' => '968 218 331'
);

$businessHours=(date('H') >= 9 && date('H') <= 20);	// HORARIO DE OFICINA, PARA DETERMINAR EL TEXTO A MOSTRAR SOBRE EL FORMULARIO

parse_str($_SERVER['QUERY_STRING'],$arrParams);
$strNewParams='';
foreach($arrParams as $key=>$value)
{
	if($key == 'cr')
	continue;

	if($key == 'city')
	{
		$strNewParams.=$key.'='.$arrParams['prevCity'].'&';
		continue;
	}
	$strNewParams.=$key.'='.$value.'&';
}
$strNewParams=substr($strNewParams,0,-1);

?>
<script type="application/javascript">
$(document).ready(function(){
<?php
$jsTImgTopLeft='';

if(isset($_REQUEST[$paramCityLanding]) && $_REQUEST[$paramCityLanding])
{
	$tmpImgLogo=isset($arrCitiesNameTop[$_REQUEST[$paramCityLanding]]) ? $arrCitiesNameTop[$_REQUEST[$paramCityLanding]] : 'hm';
	$jsTImgTopLeft.='
	$(".logo").attr("src","img/logo_'.$tmpImgLogo.'.png");
';

	if($businessHours)	// PARA HORARIO DE OFICINA
	{
		$tmpCityName=isset($arrCitiesName[$_REQUEST[$paramCityLanding]]) ? $arrCitiesName[$_REQUEST[$paramCityLanding]] : 'seva ciutat';
		$jsTImgTopLeft.='
	$(".cityName").html("'.$tmpCityName.'");
	';
	}
	else	// RESTO DEL DIA
	{
		$jsTImgTopLeft.='
	$(".dayMsg").hide();
	$(".nightMsg").show();
	';
		$jsTImgTopLeft.=''.$jsTImgTopLeft;
	}

	//$tmpPhoneCity=isset($arrCitiesPhones[$_REQUEST[$paramCityLanding]]) ? $arrCitiesPhones[$_REQUEST[$paramCityLanding]] : '900 102 722';
	$tmpPhoneCity='900 102 722';
	$jsTImgTopLeft.='
	$(".cityPhone").html("'.$tmpPhoneCity.'");
	$("#spain_city").val("'.$arrCitiesName[$_REQUEST[$paramCityLanding]].'");
';

	echo $jsTImgTopLeft;
}
?>
	$(".language").prop("http://lean.abogadosclausulasuelo.es/despacho/?<?=$strNewParams?>");
});

</script>

<header>
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
	echo $objSiteData->headerTopContent;
    
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
</header>

<section class="basic-content">
	<section class="small-content">
		<section class="skeleton">
<?php

if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div class="marco">
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!
			</div>
			<span class="bolo"></span>
';
}

?>
	</section>
	<section class="formulario">

		<form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
			<input type="hidden" name="destino" id="destino" value="">
			<input type="hidden" name="spain_city" id="spain_city" value="" />
<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
$formScriptName='form_script_'.$idLandingFormatted.'.php';
}

$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
include($formScriptName);
?>

			</form>
		</section>
	</section>
</section>

<section class="text-content">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
</section>

<footer>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
</footer>
