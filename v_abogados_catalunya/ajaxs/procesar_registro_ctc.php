<?php

/**
 * Modificación LFP 2015.03.23
 * Añadimos variable para procesar los datos que vienen de acción mobile emailing Gmail
 * Para eso añadimos variable $emailingSource que será true si viene de gmail
 * Si esta variable es true, no hacemos el die del final y tenemos los datos en un array
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');
/**
 * Config loaded viene del fichero bridge. Si está definida y es true, es que ya ha cargado los configs
 */
if(empty($configLoaded)){
	include('../../conf/config_web.php');
	include('../conf/config_web.php');
}

$resCurl = array();
$raw = '';
$tokenError = '';
$resCurl['error'] = 1;
//$sOutput['id'] = -1;

$emailingSource = (!empty($_REQUEST['emailingsource']) && strtoupper(trim($_REQUEST['emailingsource'])) == 'GMAIL');

$nombre = (!empty($_REQUEST['nombre'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', ucwords(strtolower(trim($_REQUEST['nombre'])))) : '';

$apellidos = (!empty($_REQUEST['apellidos'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', ucwords(strtolower(trim($_REQUEST['apellidos'])))) : '';
$email = (!empty($_REQUEST['em'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,EMAIL', strtolower(trim($_REQUEST['em']))) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', $_REQUEST['provincia_text']) : (normalizeInputData('INPUT,STRINGS,SQLINJECTION', @$_REQUEST['provincia']) ? $_REQUEST['provincia'] : 0);

$legal = (!empty($_REQUEST['cblegales'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['cblegales'])) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : 1;
$permanencia = (!empty($_REQUEST['perm'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['perm'])) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;

$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';
$id_crea=(!empty($_REQUEST['id_crea'])) ? trim($_REQUEST['id_crea']) : 0;

//echo 'IdClient: '.$_REQUEST['idclient']. ' Crea:'.$crea;
$idSource = (!empty($_REQUEST['id_source']) && is_numeric($_REQUEST['id_source'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['id_source'])) : 1;
$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['idclient'])) : '';

$sourcetype = (!empty($_REQUEST['sourcetype'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['sourcetype'])) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['campaign'])) : '0';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['fuente'])) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['prioridad'])) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['telefono'])) : '';
//die('Normalizado:'.normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER',trim($_REQUEST['telefono'])));
$use_priority = (!empty($_REQUEST['use_priority']) && is_numeric($_REQUEST['use_priority']) && $_REQUEST['use_priority'] == 1 && $prioridad != '') ? 1 : 0;

$sec_tok = !empty($_REQUEST['sec_tok']) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', $_REQUEST['sec_tok']) : 0;
$spain_city= !empty($_REQUEST['spain_city']) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', $_REQUEST['spain_city']) : null;

$check_sec_tok = '0'.$id_client . '_' . 'DJK' . date('dmd') . '' . $crea;
//$check_sec_tok = '0'.$id_client.'_'.'DJK'.date('mHd').$arr_creas[$id_crea]['nombpromo'];
$check_sec_tok = md5($check_sec_tok);

//echo $id_client .' , $crea= '. $crea."\r\n";
//echo $sec_tok .' != '. $check_sec_tok;die();
if ($sec_tok != $check_sec_tok) {
    $telefono = '';
    $tokenError = ', Error de TOKEN de suguridad';
    $resCurl['msg'] = 'Error Token';
}
//die();
$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

/**
 * POR DEFECTO SE ENVIAN LAS DIRECCIONES DE "$transactionEmailDoctorSender"
 *
 * EN CASO DE QUE EXISTA "$transactionEmailDoctorSenderSpecivicCrea[$id_crea]" SE UTILIZAN LOS EMAILS AHI CONTENIDOS
 */
$defaultReceiver=(isset($transactionEmailDoctorSenderSpecivicCrea[$id_crea])) ? $transactionEmailDoctorSenderSpecivicCrea[$id_crea] : $transactionEmailDoctorSender;

$campos = array('idclient' => $id_client,'id_source' => $idSource, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad,
    'nombre' => $nombre, 'apellidos' => $apellidos, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI,
    'vcc' => 'fintel', 'pwd' => $secret_pwd, 'usr' => $secret_usr,
    'use_priority' => $use_priority);

//,"serverIp"=>"84.127.240.42"

if (!empty($_SESSION['ws_ip_id']) && is_numeric($_SESSION['ws_ip_id'])) {
    $campos['idIp'] = $_SESSION['ws_ip_id'];
}

$conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);

if (!$conexion->connectDB()){
	$msg_err = 'No se ha podido conectar a la BBDD. Error 102';
	$resCurl['msg'] = $msg_err;
}

//echo $spain_city.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();

if($telefono != '')
{
	$cur_conn_id = $conexion->get_id_conexion();
	$nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
	$nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');

	$telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
	$sql = 'INSERT INTO %s (id_cliente,id_source,nombre,apellidos,telefono,id_provincia,b_legales,email) VALUES (\'%d\',\'%d\',\'%s\',\'%s\',\'%s\',\'%d\', %d,\'%s\');';
	$sql = sprintf($sql, $table_registros, $id_client, $idSource, $nombreIns, $crea, $telefonoIns, (int) $_REQUEST['provincia'], $legal, $email);
//echo $sql.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();
	$conexion->ejecuta_query($sql);
	$id = mysql_insert_id();
	$resCurl['id']=$id;
	$conexion->disconnectDB();

	switch((int)$id_client)
	{
		case 9999:
			$fileCsv = '../v_abogadosfiles/bonospopular-'.date('Ymd').'.csv';
			break;

		default:
			$fileCsv = '../v_abogadosfiles/vabogados-'.date('Ymd').'.csv';
	}

	$headerFile='';
	$headerCity=$spain_city ? 'ciudad;' : '';

	if(!file_exists($fileCsv))
		$headerFile='Nombre; E-mail; Teléfono; ciudad; crea; ID source'."\r\n";;

	$fp=@fopen($fileCsv,'a') or die('Unable to open file!');
	$line = $headerFile.$nombre.';'.$email.';'.$telefono.';'.($spain_city ? $spain_city.';' : ';').$crea.';'.$idSource."\r\n";
	$raw=$error;
	$error=fwrite($fp, $line);
	fclose($fp);
	$error=(int)$error > 0 ? '' : 1;

	$htmlDataCity=$spain_city ? 'Ciudad: <b>'.$spain_city.'</b><br />' : '';
	$htmlData='
<html>
<head></head>
<body>
<div style=font-family:arial,verdana;font-size:1em;color:#000;display:inline-block;width:99%;float:left;text-align:left;>
Nombre: <b>'.$nombre.'</b><br />
E-mail: <b>'.$email.'</b><br />
Tel&eacute;fono: <b>'.$telefono.'</b><br />
'.$htmlDataCity.'
Fecha del registro: <b>'.date('d-m-Y').'</b><br />
Referencia de la landing: <b>'.$crea.'</b><br />
Source: <b>'.$idSource.'</b><br />
</div>
</body>
</html>
';

	$arrData=array('user'=>$crea, 'data'=>array(
			array(
			'receiver'=>$defaultReceiver,
			'subject'=>'Datos registro landing V abogados',
			'html'=>$htmlData,
			'text'=>$htmlData
			)
		)
	);

	$fields_string = json_encode($arrData);
	$url = $url_ws . $file_ws;//. '?user='.$crea;
//echo  $url;die();
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_POST, 1);
	curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, $timeout);
	$raw = curl_exec($ch);
	$statusWs = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
//echo $statusWs.') ---- WS devuelve=['.$url."\r\nRAW=".($raw);echo"\r\n";echo $fields_string.'<hr><pre>';print_r($arrData);print_r($raw);die();
	if(isset($resCurl['correct']) && $resCurl['correct'] < 1)
		$error=1;

	$resCurl['error']=$error;
//echo $htmlData.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();

	if($error){
		$msg_err = 'Parece que el problema se debe a lo siguiente: "' . $resCurl->mensaje . '".';
	}else
	{
		$tmpCrea = $_SESSION['idCreatividad'];
		$query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
		$query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
		$conexion->getResultSelectArray($query);
		$resQuery = $conexion->tResultadoQuery;
		$idUpdate = $resQuery[0]['id'];
		$query = 'UPDATE %s SET http_referer=\'transaction Email: %s\' WHERE id=\'%d\'';
		$query = sprintf($query, $table_front_actions, ($statusWs == 200 ? 'OK' : 'ERROR'), (int) $idUpdate);
		$conexion->ejecuta_query($query);

		unset($_SESSION['vAbogados'.date(Yimis)]); // SE DESTRUYE LA REFERENCIA DE LA SESION
		$tmpName = session_name('vAbogados'.date(Yimis));
		$_SESSION['namePromo'] = session_name(); // SE CONSTRUYE UNA NUEVA SESION PARA PERMITIR REGISTROS NUEVOS DESDE LA MISMA APERTURA
		$_SESSION['idCreatividad'] = $tmpCrea;

		$resCurl['error']=0;
	}
} else {
	if ($telefono == '') {
		$error = 1;
		$msg_err = 'Falta el campo Teléfono. Error 104';
		$resCurl['msg'] = $msg_err;
	}
}

if($error)
{
	$query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
	$query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
	$conexion->getResultSelectArray($query);
	$resQuery = $conexion->tResultadoQuery;
	$idUpdate = $resQuery[0]['id'];
	$query = 'UPDATE %s SET http_referer=CONCAT(http_referer, \', ERROR CTC: %s\') WHERE id=\'%d\'';
	$query = sprintf($query, $table_front_actions, $resCurl->mensaje . $tokenError, (int) $idUpdate);
	$conexion->ejecuta_query($query);
}
$conexion->disconnectDB();
if(!$emailingSource) {
	$res = json_encode($resCurl);
	die($res);
}
?>