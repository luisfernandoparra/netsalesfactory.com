<?php
/*
 * DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED DEPRECATED 
 * REMOTE EMAIL CHECK --- DEPRECATED
 * 
 * 'apiError' ES UNA MICRO-CLASE PARA GESTIONAR EVENTUALES
 * ERRORES DE COMUNICACION CON LA API DE VERIFICACION DE E-MAIL
 * Y PERMITIR PROSEGUIR CON EL REGISTRO DE LA ACTUAL LANDING
 */
@session_start();

/**
 * MICRO CLASE PARA GESTIONAR ERROR CON EMAIL API CHECK
 */
class apiError
{
	public $status;	public $apiError;	public $additionalStatus;
	function __construct(){$this->status=true; $this->apiError=true; $this->additionalStatus='ERROR-API-SKIP';}
}

$_SESSION['skipLogAction']=true;	// EVITA LOG INNECESARIO
include('../../conf/config_web.php');
include('../conf/config_web.php');
$_SESSION['skipLogAction']=false;	// SE REACTIVAN LOS LOGS BBDD
$apiKey='1CEB2B97'; //apikey definitivo
$apiUrl='https://api.emailverifyapi.com/api/a/v1?';

$email=$_REQUEST['email'] ? strip_tags(trim($_REQUEST['email'])) : '';
$res=array('success'=>false);
$arrErrors=array(
		'MailboxDoesNotExist'=>'no existe el correo'
);

if(empty($_SESSION['namePromo']) || !$email)
{
	$res=json_encode($res);
	die($res);
}

$fieldsWs=array();
$fieldsWs['key']=$apiKey;
$fieldsWs['email']=$email;
$fieldsWs['correct']=1;

$qs=http_build_query($fieldsWs);
$url=$apiUrl.$qs;
$ch = curl_init();
$timeout = 0; // set to zero for no timeout
curl_setopt ($ch, CURLOPT_URL, $url);
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
$resWs = curl_exec($ch);
curl_close($ch);

$res['wsResponse']=(json_decode($resWs));
$res['success']=$res['wsResponse']->status == 'Ok' ? true : false;
$res['error']=$res['wsResponse']->additionalStatus;

if(!is_string($resWs) || substr($res['wsResponse']->Message,0,7) === 'No HTTP')	//  ERROR DE API, COMUNICACION O NO SE HA RECIBIDO RESPUESTA
{
	$res['wsResponse']=new apiError();
	$res['success']=$res['wsResponse']->status == 'Ok' ? true : false;
	$res['error']=$res['wsResponse']->additionalStatus;
}

$res['errorMessage']=($res['wsResponse']->additionalStatus && $res['wsResponse']->additionalStatus != 'Success') ? 'Parece que <b>'.$arrErrors[$res['wsResponse']->additionalStatus].'</b>.' : '';
$laAccion=$res['success'] ? 'email-check-OK;'.$email.';' : 'email-check-ERROR;'.$email.';'.$res['wsResponse']->additionalStatus;

$table_front_actions=$prefixTbl.'log_web_actions'.date(Y);

/*
 * start LOG ACTIONS
 */
if(!empty($_SESSION['namePromo']))
{
	$pagToLog=$_SERVER['PHP_SELF'];
	$pagToLog=substr($pagToLog,strrpos($pagToLog,'/')+1);
	$pagToLog=substr($pagToLog,0,strpos($pagToLog,'.'));
	$laAccion=$laAccion ? ($res['wsResponse']->apiError ? $res['wsResponse']->additionalStatus.';'.$email : $laAccion) : '?';
	$query='INSERT INTO %s (session_id,fecha,ip_usuario,script,accion,id_crea,action_number)VALUES(\'%s\',NOW(),\'%s\',\'%s\',\'%s\',%d,%d)';
	$query=sprintf($query,$table_front_actions,$_SESSION['namePromo'],$_SERVER['REMOTE_ADDR'],$pagToLog,$laAccion,$idLandingCr,$_SESSION['sessionCountPromo']);
	$conexion->ejecuta_query($query);
}
	$_SESSION['sessionCountPromo']++;
/*
 * end LOG ACTIONS
 */

$res=json_encode($res);
die($res);
?>