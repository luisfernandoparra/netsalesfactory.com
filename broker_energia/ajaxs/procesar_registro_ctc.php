<?php
@session_start();
header('Content-Type: text/html; charset=utf-8');
include('../../conf/config_web.php');
include('../conf/config_web.php');
include($path_raiz_includes.'includes/capping_control.php');
$resCurl = new stdClass();
$raw='';
$sOutput['id'] = -1;
$cli = (empty($_REQUEST['cli'])) ? '' : $_REQUEST['cli'];

$nombre = (!empty($_REQUEST['nombre'])) ? trim($_REQUEST['nombre']) : '';
$apellidos = (!empty($_REQUEST['apellidos'])) ? trim($_REQUEST['apellidos']) : '';
$email = (!empty($_REQUEST['em'])) ? trim($_REQUEST['em']) : '';

$legal = (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';
$producto = (!empty($_REQUEST['producto'])) ? trim($_REQUEST['producto']) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : '1';
$permanencia = (!empty($_REQUEST['perm'])) ? trim($_REQUEST['perm']) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;
$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';

$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? trim($_REQUEST['idclient']) : '';
$sourcetype = (!empty($_REQUEST['sourcetype'])) ? trim($_REQUEST['sourcetype']) : '';
$campaign = (!empty($_REQUEST['campaign'])) ? trim($_REQUEST['campaign']) : '';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? trim($_REQUEST['prioridad']) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? trim($_REQUEST['telefono']) : '';
$use_priority = (!empty($_REQUEST['use_priority']) && is_numeric($_REQUEST['use_priority']) && $_REQUEST['use_priority']==1 && $prioridad !='') ? 1 : 0;

$docType = (!empty($_REQUEST['doc_type'])) ? trim($_REQUEST['doc_type']) : '';
$docNum = (!empty($_REQUEST['doc_num'])) ? trim($_REQUEST['doc_num']) : '';

$arrBirthDate=explode('/',$_REQUEST['birth_date']);
$birthDate = !empty($_REQUEST['birth_date']) ? date('Y-m-d',strtotime($arrBirthDate[2].'-'.$arrBirthDate[1].'-'.$arrBirthDate[0])) : '';

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;
//,"serverIp"=>"84.127.240.42"
//$campos['urlOrigen']=$_SERVER['HTTP_REFERER'];	// ADDED 2016.02.23 M.F.
//$campos['cli']=$cli;	// ADDED 2016.04.13 M.F.

if ($telefono != '') {
    $conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
    if (!$conexion->connectDB()) {
        $msg_err = 'No se ha podido conectar a la BBDD. Error 102';
    } else {
        $cur_conn_id = $conexion->get_id_conexion();
        /* $sql1="SET AUTOCOMMIT=0;";
          $conexion->ejecuta_query($sql1);

          $sql2="BEGIN";
          $conexion->ejecuta_query($sql2);
         */
        $nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
        $nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');
        $telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');

				//	 SE COLOCA EL VALOR $producto EN EL CAMPO doc_type AL NO EXISTIR EN LA BBDD UN CAMPO ESPECIFICO PARA TAL VALOR
        $sql = "INSERT INTO %s (id_cliente,nombre,apellidos,telefono,id_provincia,b_legales,email,doc_type,doc_num,birth_date) VALUES ('%d','%s','%s','%s','%d', %d, '%s', %d, '%s', '%s');";
        $sql = sprintf($sql, $table_registros, $id_client, $nombreIns, $crea, $telefonoIns, (int) $_REQUEST['provincia'], $legal, $email, (int)$producto, $docNum, $birthDate);
        $conexion->ejecuta_query($sql);
        $id = mysql_insert_id();

				$campos = array('nameuser' => $nombre, 'numberPhone' => $telefono, 'email' => $email, 'product' => $producto);
				$campos['idcampaign']='ENERGIA';
				$campos['provincia']='MADRID';
				$campos['canal']='NT';
				$campos['date']=date('d-m-Y');
				$campos['hour']=date('H:i:s');
				$campos['origin']='';

				$qs = http_build_query($campos);
				$url = $url_ws . $file_ws . '?' . $qs;
        $conexion->disconnectDB();

        $ch = curl_init();
        $timeout = 0; // set to zero for no timeout
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $raw = curl_exec($ch);
        curl_close($ch);

				$newlines = array("\t", "\n", "\r", "\x20\x20", "\0", "\x0B");
				$responseWs = str_replace($newlines, '', html_entity_decode($raw));

				
				$resCurl->mensaje='';
				$error=strpos($responseWs,'messageRigthInsert(\'Ok\'') ? null : true;

				$resCurl->error=$error;
				$resCurl->success=!$error;

        if($error) {
            $msg_err = 'Parece que el problema se debe a lo siguiente: "' . $resCurl->mensaje . '".';
        } else {
					$resCurl->mensaje='';
					preg_match('/(?<=\<body\>)(\s*.*\s*)(?=\<\/body\>\<)/', $responseWs, $stringResponse);

					$sql='UPDATE %s SET response_ws=\'%s\' WHERE id_registro=%d';	// SE ACTUALIZA EL REGISTRO CON EL "ID" DEVUELTO POR EL WS
					$sql=sprintf($sql, $table_registros, $stringResponse[0], $id);
					$conexion->ejecuta_query($sql);

					$msg_err='';
					$tmpCrea=$_SESSION['idCreatividad'];
					unset($_SESSION['namePromo']);	// SE DESTRUYE LA REFERENCIA DE LA SESION
					$tmpName=session_name('bip_drive'.date(Yimis));
					$_SESSION['namePromo']=session_name();	// SE CONSTRUYE UNA NUEVA SESION PARA PERMITIR REGISTROS NUEVOS DESDE LA MISMA APERTURA
					$_SESSION['idCreatividad']=$tmpCrea;

					if($producto && $id)
					{
						$sql='UPDATE %s SET defaulter_status=%d WHERE id=%d';
						$sql=sprintf($sql, $table_registros, $resCurl->error, $id);
					}
        }
    }
} else {
    if ($telefono == '') {
        $error = 1;
        $msg_err = 'Falta el campo Teléfono. Error 104';
    }
}

if($error)
{
	$query='SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
	$query=sprintf($query,$table_front_actions,$email,$_SESSION['namePromo']);
	$conexion->getResultSelectArray($query);
	$resQuery=$conexion->tResultadoQuery;
	$idUpdate=$resQuery[0]['id'];
	$query='UPDATE %s SET http_referer=\'ERROR CTC: %s\' WHERE id=\'%d\'';
	$query=sprintf($query,$table_front_actions,$resCurl->mensaje,(int)$idUpdate);
	$conexion->ejecuta_query($query);
//echo $query;print_r($resQuery);
}
else
{
	$conexion->ejecuta_query($sql);

}

$res = json_encode($resCurl);
//echo $res."\r\n".'error=['.$error."]\r\n".$telefono;print_r($responseWs);echo"\r\n---->";print_r($_REQUEST);print_r($raw);die();
die($res);
