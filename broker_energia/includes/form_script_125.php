<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
?>
	<div id="campos">
		<div class="error"><ul></ul></div>
		<p>
			<span class="offer">OFERTA</span> de Luz y Gas Natural<br>
			<span class="cheap-text">AHORRA HASTA UN 30%</span><br>
			<span class="information">¡Solicita informaci&oacute;n!</span>
		</p>

		<div class="fila">
			<div class="fleft">
				<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre y apellidos&lt;/strong&gt; es obligatorio" placeholder="Nombre y apellidos" aria-required="true" />
			</div>
		</div>

		<div class="row">
			<div class="col8">
				<input type="tel" maxlength="9" name="telefono" id="telefono" class="celda" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Teléfono" aria-required="true" />
			</div>
		</div>

		<div class="row">
			<div class="col8">
				<input type="email" name="email" id="email" maxlength="150" class="celda" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;E-mail&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="E-mail" aria-required="true" />
			</div>
		</div>

		<div class="fila">
			<div class="col8">

				<select class="celda-servicio product" type="text" name="producto" id="producto" maxlength="100" required data-msg-required="Debes seleccionar un &lt;strong&gt;Producto&lt;/strong&gt;" placeholder="Producto" aria-required="true">
					<option value="">Producto</option>
					<option value="1">Luz</option>
					<option value="2">Gas Natural</option>
				</select>
			</div>
		</div>

		<div class="legal" style="padding-top:10px;">
			<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;Acepto la <a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'privacy_policies.php';?>" data-ref="" target="_blank">pol&iacute;tica de privacidad</a>
		</div>

		<div class="row">
			<div class="espacio_btn">
				<input class="button green sendData" type="button" id="btnProcesar" name="btnProcesar" value="¡Informadme!">
			</div>
		</div>

		<div class="clearfix"></div>
	</div>
