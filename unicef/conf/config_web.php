<?php
/*
 * NOTA ACERCA DE INBOUND:
 * LOS TELEFONOS INBOUND SE ENCUENTRAN EN EL
 * `index.php` AS SER NECESARIO RECIBIR `$id_source`
 * QUE SE PROCESA EN `includes/initiate.php` Y SE
 * CARGA DESPUES DE ESTE SCRIPT DE CONFIGURACION
 * 
 */
//***************************************
// START CONFIG DINAMICA (M.F. 19.11.2013)
$sitesPruebas=($_SERVER['SERVER_ADDR'] == '139.162.246.12' || $_SERVER['SERVER_ADDR'] == '127.0.0.1' || substr($_SERVER['SERVER_ADDR'], 0, 9) == '192.168.2'); // SITIOS PARA PRUEBAS
$paintErrors=false;
$minTimeCookiesStart=30;  // TIEMPO (EN SEGUNDOS) PARA ACTIVAR AUTOMATICAMENTE LAS COOKIES
$subFolderLanding='unicef';

if($sitesPruebas){
	$subFolderLanding='unicef';
//    $url_local = $fromServer.'/netsalesfactory.com/promociones/sanitas-new-layout/';
	$url_local = $fromServer.'/netsalesfactory.com/promociones';
	$url_local_condiciones = $fromServer.'/netsalesfactory.com/promociones';
	$dir_raiz_aplicacion_local = $dir_raiz_aplicacion . 'unicef/';
	$path_raiz_aplicacion_local = $dir_raiz_aplicacion_local . '';
	$path_raiz_includes_local = $path_raiz_includes . 'unicef/';
	$url_condiciones = $url_local .'/'.$subFolderLanding .'/condiciones.php';
	$paintErrors=1;
	$minTimeCookiesStart=3000;
//	echo "SITE EN PRUEBAS:: testeando. Este texto está en el config web ";
} else {
	$url_local = 'http://www.dentalsanitas.es/promociones';
	$url_local_condiciones = 'http://www.xxxxxxxxxxxxxxxx.es';
	$dir_raiz_aplicacion_local = $dir_raiz_aplicacion . $subFolderLanding.'/';
	$path_raiz_aplicacion_local = '/' . $dir_raiz_aplicacion_local;
	$path_raiz_includes_local = $path_raiz_includes . $subFolderLanding.'/';
	$url_condiciones = $url_local .'/'.$subFolderLanding . '/condiciones.html';
}
// END CONFIG DINAMICA (M.F. 19.11.2013)

//Variable para la tabla REGISTROS, para diferenciar qué cliente es
$id_client = (int)$_REQUEST['idClient'] ? (int)$_REQUEST['idClient'] : 9;

$arr_client_campaign=array(
	9 => array(
		'prefFichExtraccion' => 'xxxxxxxxxxxxxxxx',
		'refInconcert' => array('xxxxxxxxxxxxxxxx', 'xxxxxxxxxxxxxxxx')
	)
);

$prefFichExtraccion = $arr_client_campaign[$id_client]['prefFichExtraccion'];
$arr_campanas = $arr_client_campaign[$id_client]['refInconcert'];

//Array con todas las creas existentes. 
/*
 * Notas 2014:
 * 1. El parámetro `manageDevice` condiciona la detección del dispositivo que carga la promo
 * 2. `new_template` gestiona el tipo de modelo a seguir con la composición de la promo,
 * - 0: modo antiguo con archivos distintos para contenidos según PC o MOBILE
 * - 1: modo nuevo un archivo único para todos los dispositivos
 */
if($sitesPruebas)
{
//echo'<pre>';print_r($objSiteData);echo'</pre>';
}

$arr_creas = array(
	1 =>array(
		'title' => 'Unicef - 11',
		'body' => '',
		'nombpromo' => 'UNO',
		'is_mobile' => 0,'is_compro_prov' => 1,
		'condiciones' => '',
		'protecciondatos' => $url_local_condiciones . '/'. $subFolderLanding .'/proteccion_datos.php',
		'cookie_css' => '',
		'manageDevice' => 1,
		'new_template' => 1,
		'js' => '',
		'mobile_auto_modal_on_open'=>0,
		'inbound'=>0
	)
);

//Parámetros del pixel de TradeDoubler
$organization = 'xxxxxxxxxxxxxxxx';
$event = 'xxxxxxxxxxxxxxxx';
$trbdl_program='xxxxxxxxxxxxxxxx';
$secret_pwd_conf='p3Rd€r_N@da';
$secret_usr=date('Y-m-d H:i:s');
$secret_pwd = md5($secret_usr.$secret_pwd_conf);

$nombCliente = 'UNICEF';

//Pixel expecíficos
$isSpecificPixel = 1;

//Array de display
$arr_isSpecificPixel = array(
//	'2261683' => '<img src="http://ads.creafi-online-media.com/pixel?id=2357960&t=2" width="1" height="1" />',
//	'1945955' => '<img src="https://ad.yieldmanager.com/pixel?id=2318907&t=2" width="1" height="1" />',
//	'2262090' => '<img src="https://www.wtp101.com/pixel?id=16499" width="1" height="1" border="0" />',
//	'2341830' => '<img src="http://www.geoads.net/lead/dvdp/8fdb58a27f0f4c56348b57606/" width="1" height="1" border="0" />',	// ADDED 14.04.2014
//	'2358884' => '<img src="http://www.geoads.net/lead/dvdp/8fdb58a27f0f4c56348b57606/" width="1" height="1" border="0" />',	// ADDED 14.04.2014
//	'2264375' => '<script src="http://ib.adnxs.com/seg?add=685954&t=1" type="text/javascript"></script>',
//	'2331667' => '<img src="http://www.eveen.es/conversion/?aid=3&cid=1" width="1" height="1" />',	// ADDED 14.04.2014
//	'2271169' => '<script language="JavaScript1.1" src="http://pixel.mathtag.com/event/js?mt_id=224629&mt_adid=111634&v1=&v2=&v3=&s1=&s2=&s3="></script>',
//	'1985489' => '<img src="https://secure.adnxs.com/seg?add=1046253&t=2" width="1" height="1" />',
//	'2395854' => '<img src="http://www.fshost.es/aff_l?offer_id=156" width="1" height="1" />',
//	'2399134' => '' //	INSERTADO EL 09.04.2014
// START PIXEL DE PRUEBAS (M.F. 19.11.2013)
//	,'9999999' => '<script language="JavaScript1.1" src="http://wrap.tradedoubler.com/wrap?id=8498"></script>'
// END PIXEL DE PRUEBAS (M.F. 19.11.2013)
	);


/*
 * Para el cliente 2264375, lanzaremos 2... deber·n ser seguidos.
 * El parámetro `isImageObject` permite crear una imagen si está habilitado, en caso contrario se construirá un JS.
 */
$arr_isSpecificPixelJS = array(
//	'2264375'=>array(0=> 'https://secure.adnxs.com/px?id=73601&t=1',1 => 'https://secure.adnxs.com/seg?add=685956&t=1','isImageObject'=>0),
//  '2271169'=>array(0=> 'http://pixel.mathtag.com/event/js?mt_id=224630&mt_adid=111634&v1=&v2=&v3=&s1=&s2=&s3=',1 => '','isImageObject'=>0),
//	'2371425'=>array(0=> 'https://secure.adnxs.com/px?id=151505&seg=1432958&order_id=[XXfeaturedparamXX]&t=1',1 => '','isImageObject'=>0),
//	'2083732'=>array(0=> 'http://app.linkemann.com/pixel.gif.php?cid=408&leadid=[XXfeaturedparamXX]&t=1',1 => '','isImageObject'=>1)
);


/*
 * SE SOBREESCRIBEN EL ARRAY `$arr_creas` CON LOS DATOS ALMACENADOS EN LA BBDD
 * CORRESPONDIENTES A LA PROMOCION ACTUAL SI ESTA EXISTE
 * NECESARIO TAMBIEN PARA MANTENER COMPATIBILIDAD CON LAS PROMOS ANTERIORES
 * 
 * (M.F. mayo 2014)
 * 
 */

if(@$isDataDb && @$objSiteData->landingId)
{
	$arr_creas[$objSiteData->landingId]['title']=$objSiteData->webTitle;
	$arr_creas[$objSiteData->landingId]['body']=$objSiteData->bodyScript;
	$arr_creas[$objSiteData->landingId]['nombpromo']=$objSiteData->landingName;
	$arr_creas[$objSiteData->landingId]['is_mobile']=0;
	$arr_creas[$objSiteData->landingId]['is_compro_prov']=1;
	$arr_creas[$objSiteData->landingId]['condiciones']=$objSiteData->landingTermsFile;
	$arr_creas[$objSiteData->landingId]['protecciondatos']=$objSiteData->landingPrivacyPolicyFile;
	$arr_creas[$objSiteData->landingId]['cookie_css']=$objSiteData->footerCookiesCssFile;
	$arr_creas[$objSiteData->landingId]['mobile_auto_modal_on_open']=$objSiteData->mobileAutoModal;
	$arr_creas[$objSiteData->landingId]['manageDevice']=1;
	$arr_creas[$objSiteData->landingId]['new_template']=1;
	$arr_creas[$objSiteData->landingId]['js']='';
}

if($debugModeNew)
{
//	$debugDujok=true;
//echo 'local-conf:<pre>';print_r($objSiteData);echo '</pre>';
//echo $queryOne.'<pre>';print_r($datosRespuesta);
}
?>