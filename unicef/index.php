<?php
/*
 * PROMOCION UNICEF, JUNIO 2014
 */
//unset($_SESSION['namePromo']);
@session_start();

if(empty($_SESSION['namePromo']))
{
	$tmpName=session_name('unicef'.date(Ymdis).rand(10,99));
	$_SESSION['namePromo']=session_name();
	$_SESSION['sessionCountPromo']=-1;
	$_SESSION['idCreatividad']=@$_REQUEST['cr'] ? @$_REQUEST['cr'] : @$id_crea;
}
include('../conf/config_web.php');
include('conf/config_web.php');
ini_set('display_errors',$paintErrors);
include($path_raiz_includes.'includes/initiate.php');
include($path_raiz_includes_local.'includes/local_vars.php');
$layoutType=null;
$prefixCss='';
$id_crea= $id_crea ? $id_crea : $objSiteData->landingId;

// START INBOUND PHONE SELECT
// (necesario que esté en este punto puesto que ´$id_source´ se recupera desde `initiate.php`)
$inbound_phone_default='0';
$arr_inbound_phones_sources=array(
	''=>'111111111'
);
$isInbound=@$_REQUEST['inbound'] ? (int)@$_REQUEST['inbound'] : '';

$inbound_phone=$arr_inbound_phones_sources[$id_source] ? $arr_inbound_phones_sources[$id_source] : $inbound_phone_default;	// INBOUND PHONE DISPLAY
$inbound_prefix=$isInbound ? 'inb_' : '';
// END INBOUND PHONE SELECT


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
/*
 * SEGUN EL TIPO DE DISPOSITIVO UTILIZADO PARA ABRIR LA PROMOCION
 * $layoutType = PREFIJO DEL DISPOSITIVO UTILIZADO
 */
if((@$arr_creas[$id_crea]['manageDevice']))
{
	include($path_raiz_includes.'includes/index_mobile_manager.php');	// SCRIPT PARA DETECTAR EL DISPOSITIVO ABRE LA WEB
//echo '<pre>';print_r($_SESSION['deviceMobileData']);print_r($_SERVER);echo '</pre>';
}

/*
 * SE EFECTUA EL LOG (COMPLETO) SOLO PRIMER ACCESO
 */
if($_SESSION['sessionCountPromo'] == 0)
{
	if(isset($_SESSION['deviceMobileData']))
	{
		$query='INSERT INTO %s (session_id,fecha,ip_usuario,script,accion,http_referer,user_agent,id_crea,device_type,device_os,device_browser,action_number)VALUES(\'%s\',NOW(),\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',%d,\'%s\',\'%s\',\'%s\',%d)';
		$query=sprintf($query,$table_front_actions,$_SESSION['namePromo'],$_SERVER['REMOTE_ADDR'],$pagToLog,$laAccion,$_SERVER['HTTP_REFERER'],$_SERVER['HTTP_USER_AGENT'],(int)$_REQUEST['cr'],$_SESSION['deviceMobileData'][0],$_SESSION['deviceMobileData'][1],$_SESSION['deviceMobileData'][2],(int)$_SESSION['sessionCountPromo']);
	}
	else
	{
		$query='INSERT INTO %s (session_id,fecha,ip_usuario,script,accion,http_referer,user_agent,id_crea,action_number)VALUES(\'%s\',NOW(),\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',%d,%d)';
		$query=sprintf($query,$table_front_actions,$_SESSION['namePromo'],$_SERVER['REMOTE_ADDR'],$pagToLog,$laAccion,$_SERVER['HTTP_REFERER'],$_SERVER['HTTP_USER_AGENT'],(int)$id_crea,(int)$_SESSION['sessionCountPromo']);
	}
	$conexion->ejecuta_query($query);
}

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// SOLO TESTS::PARA FORZAR EL TIPO DE DISPOSITIVO A MOBILE
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if(@$_REQUEST['forzarmovil'])
{
	$layoutType=$arr_creas[$id_crea]['new_template'] ? 1 : 'mob_';
	$is_movil=1;
}

//if(@$objSiteData->landingId)	// PARA PROMOS BASADAS EN BBDD
$arr_creas[$id_crea]['js']='';

//	START EXCEPCION PARA PERMITIR COMPONER LA PROMO CORRECTAMENTE EN EL IFRAME DE LOS SORTEOS, GTP (M.F. 20140226)
$layoutSorteo='';
$arraySorteos=array(
);

if(isset($_SERVER['HTTP_REFERER']) && in_array($_SERVER['HTTP_REFERER'],$arraySorteos))
{
  $layoutSorteo='sorteoIframe';
}
//	END EXCEPCION PARA PERMITIR COMPONER LA PROMO CORRECTAMENTE EN EL IFRAME DE LOS SORTEOS

if($layoutType)
{
	$is_movil=1;
}

// START EXCEPCION PARA LA IMAGEN DE LA CABECERA PARA UNA FECHA EN UNAS DETERMINADAS HORAS Y LA MODAL DE "LLAMAR"
$mobileImagePostName='';
$fechaActivacion='20140306';  // DIA DE ACTIVACION PARA LA EXCEPCION

//$mobileArraDayException=array(2=>'',3=>'',4=>'',6=>'',7=>'');	// UTILIZAR ESTA LINEA SOLO EN CASO SE DEBAN DESHABILITAR LAS PROMOS "mobile"
$mobileArraDayException=array(2=>'_callme',3=>'_callme',4=>'_callme',6=>'_callme',7=>'_callme');
$mobileRangeTimeException=(date('H') >= 10 && date('H') <= 13) ? DATE('Ymd') : '';  // parte final del nombre de la clase para estar activada desde las 10 a las 14 horas (SOLO SI NECESARIO)
// ********* SOLO PARA PRUEBAS, DESCOMENTAR LA DE ARRIBA ***************
//$mobileRangeTimeException=(date('H') >= 10 && date('H') <= 14) ? $fechaActivacion : '';

/*
 * VARIABLES PARA CONTROLAR EL ORARIO
 * DE APARICION LA VENTANA MODAL DE "LLAMAR"
 */
$allowDays=(date('w') >= 1 && date('w') <= 5) ? true : false; // SOLO DIAS LABORALES
$timeStartModal=1000; // HORA + MINUTO DE INICIO DE HABILITACION
$timeEndModal=2030;	// HORA + MINUTO FIN DE HABILITACION

$allowHours=(date('Hi') >= $timeStartModal && date('Hi') <= $timeEndModal) ? true : false; // HORARIO RESTRINGIDO, DESDE ... HASTA
$mobileRangeTimeException=($allowDays && $allowHours) ? $fechaActivacion : null;


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// MODAL DE "LLAMAR"
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if($layoutType == 'mob_' && array_key_exists($id_crea,$mobileArraDayException) && $mobileRangeTimeException)
  $mobileImagePostName=$mobileArraDayException[$id_crea];	// PARA CONSTRUIR EL NOMBRE LA IMAGEN DE LA CABECERA

//////if(array_key_exists($id_crea,$mobileArraDayException) && $mobileRangeTimeException)
// END EXCEPCION PARA LA IMAGEN DE LA CABECERA PARA UNA FECHA Y UNAS DETERMINADAS HORAS

include($path_raiz_includes_local . 'includes/header'.($arr_creas[$id_crea]['new_template'] ? '_tmplt' : '').'.php');
?>
<body class="landing">
<?php
$condicionesPromo=@$arr_creas[$objSiteData->landingId]['condiciones'];
$privacyPolicy=@$arr_creas[$objSiteData->landingId]['protecciondatos'];

/*
 * SE DETERMINA SI EL VALOR $condicionesPromo ES UN ARCHIVO
 * O UN TEXTO ALMACENADO EN LA BBDD
 */
if(strlen($condicionesPromo) > 100 && !strripos($condicionesPromo,'.php'))	// ESTE VALOR NO PERTENECE A UN ARCHIVO FISICO
	$condicionesPromo=$path_raiz_aplicacion_local.'includes/landing_conditions.php?cr='.(int)$id_crea;	// SE CARGA EL SCRIPT CORRESPONDIENTE PARA DIBUJAR EL TEXTO PRESENTE EN LA BBDD
else
	$condicionesPromo=$path_raiz_aplicacion_local.'includes/'.$condicionesPromo;
//echo$condicionesPromo;
if(strlen($privacyPolicy) > 100 && !strripos($privacyPolicy,'.php'))	// ESTE VALOR NO PERTENECE A UN ARCHIVO FISICO
	$privacyPolicy='includes/privacy_policies.php?cr='.(int)$id_crea;	// SE CARGA EL SCRIPT CORRESPONDIENTE PARA DIBUJAR EL TEXTO PRESENTE EN LA BBDD

$privacyPolicy=$privacyPolicy ? $path_raiz_aplicacion_local.$privacyPolicy : $path_raiz_aplicacion_local.'includes/privacy_policies.php';

/*
 * INCLUDES PARA EL CONTENIDO PRINCIPAL DE LA PROMO
 * - body de la promocion actual
 * - cookies_footer
 */
$bodyScript=$arr_creas[$id_crea]['body'] ? $arr_creas[$id_crea]['body'] : 'body_undefined.php';
include($path_raiz_includes_local . 'includes/'.$bodyScript);
include($path_raiz_includes_local . 'includes/cookies_footer.php');
?>
</body>
</html>
