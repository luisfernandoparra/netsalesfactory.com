<?php
$idLandingFormatted=str_pad($objSiteData->landingId,3,'0',STR_PAD_LEFT);
?>
<header class="lite">
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
	echo $objSiteData->headerTopContent;
else
	echo '<br /><center><b>!! SIN CABECERA DEFINIDA !!</b><center>';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';

?>
</header>
<div id="cuerpo" class="row" style="background-image:url(<?=$localLandingCuerpoImage?>); background-position:right top">
	<div id="informacion" class="col4 <?=$layoutType ? '' : 'hide'?> 1boxMarco">
<?php

if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div style="disply:inline-block;text-align:center;width:100%;margin-left:40px;font-size:1em;font-weight:bold;"><center><br /><br />
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!<br /><br /></center>
			</div>
			<span class="bolo"></span>
';
}

?>
	</div>

	<div id="accion" class="col8">
		<div style="padding:15px 10px 5px 10px;overflow:hidden;">

			<form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
				<input type="hidden" name="destino" id="destino" value="">
				<section class="boxRight white">

<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
	$formScriptName='form_script_'.$idLandingFormatted.'.php';
}

$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
	echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
	include($formScriptName);
?>

				</section>

			</form>
		</div>
  </div>
	<div class="clearfix"></div>


	<div class="table productos colum4" style="width:100%">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
	</div>
</div>

<footer>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
</footer>
