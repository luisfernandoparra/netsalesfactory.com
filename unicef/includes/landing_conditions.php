<?php
@session_start();
include('../../conf/config_web.php');
$query='SELECT landing_terms_file FROM %s WHERE id=%d';
$query=sprintf($query,$table_landings_site_config,(int)$idLandingCr);
$conexion->getResultSelectArray($query);
$txtToDraw=$conexion->tResultadoQuery;
$txtToDraw=$txtToDraw[0];
$txtToDraw=$txtToDraw['landing_terms_file'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Condiciones de Contratación</title>

<style>
body, .conditions{font-family:Arial, Helvetica, sans-serif, Gadget, sans-serif;color:#000000;background-color:#fff}
.conditions{padding:10px;}
</style>

</head>
<body>

	<div class="conditions">
<?php
if(!(int)$idLandingCr)
	echo '<center>Lamentablemente ha ocurrido un error al intentar cargar la información solicitada.<br /><br />Por favor, inténtelo más tarde.</center>';
else
	echo $txtToDraw;
?>
	</div>
	<script>
		console.log("<?=$idLandingCr?>")
	</script>
</body>
</html>