<?php

/**
 * Modificación LFP 2015.03.23
 * Añadimos variable para procesar los datos que vienen de acción mobile emailing Gmail
 * Para eso añadimos variable $emailingSource que será true si viene de gmail
 * Si esta variable es true, no hacemos el die del final y tenemos los datos en un array
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');

/**
 * Config loaded viene del fichero bridge. Si está definida y es true, es que ya ha cargado los configs
 */
function getTextBetweenTags($string, $tagname) {
    $pattern = "/<$tagname>(.*?)<\/$tagname>/";
    preg_match($pattern, $string, $matches);
    return $matches[1];
}

if (empty($configLoaded)) {
    include('../../conf/config_web.php');
    include('../conf/config_web.php');
}
$resCurl = array();
$raw = '';
$resCurl['error'] = 1;


$id_crea = (empty($_REQUEST['id_crea'])) ? 99 : (int) $_REQUEST['id_crea'];
$cli = (empty($_REQUEST['cli'])) ? '' : $_REQUEST['cli'];
$sbs_curso = (empty($sbs_cursos[$id_crea])) ? $sbs_curso : (int) $sbs_cursos[$id_crea];

//$sOutput['id'] = -1;

$emailingSource = (!empty($_REQUEST['emailingsource']) && strtoupper(trim($_REQUEST['emailingsource'])) == 'GMAIL');

$nombre = (!empty($_REQUEST['nombre'])) ? trim($_REQUEST['nombre']) : '';
$apellidos = (!empty($_REQUEST['apellidos'])) ? trim($_REQUEST['apellidos']) : '';
$email = (!empty($_REQUEST['em'])) ? trim($_REQUEST['em']) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? $_REQUEST['provincia_text'] : (@$_REQUEST['provincia'] ? $_REQUEST['provincia'] : 0);

$legal = (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : 1;
$permanencia = (!empty($_REQUEST['perm'])) ? trim($_REQUEST['perm']) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;
$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';

$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? trim($_REQUEST['idclient']) : '';
$sourcetype = (!empty($_REQUEST['sourcetype'])) ? trim($_REQUEST['sourcetype']) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? trim($_REQUEST['campaign']) : '0';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? trim($_REQUEST['prioridad']) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? trim($_REQUEST['telefono']) : '';
$use_priority = (!empty($_REQUEST['use_priority']) && is_numeric($_REQUEST['use_priority']) && $_REQUEST['use_priority'] == 1 && $prioridad != '') ? 1 : 0;

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

$campos = array('idclient' => $id_client, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad,
    'nombre' => $nombre, 'apellidos' => $apellidos, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI,
    'vcc' => 'fintel', 'pwd' => $secret_pwd, 'usr' => $secret_usr,
    'use_priority' => $use_priority);

//,"serverIp"=>"84.127.240.42"

if (!empty($_SESSION['ws_ip_id']) && is_numeric($_SESSION['ws_ip_id'])) {
    $campos['idIp'] = $_SESSION['ws_ip_id'];
}

$qs = http_build_query($campos);
$url = $url_ws . $file_ws . '?' . $qs;

//echo $url.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();
if ($telefono != '') {
    $conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
    if (!$conexion->connectDB()) {
        $msg_err = 'No se ha podido conectar a la BBDD. Error 102';
    } else {
        $cur_conn_id = $conexion->get_id_conexion();
        $nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
        $nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');

        $telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
        $sql = 'INSERT INTO %s (id_cliente, nombre, apellidos,telefono,id_provincia,b_legales,email) VALUES (\'%d\',\'%s\',\'%s\',\'%s\',\'%d\', %d,\'%s\');';
        $sql = sprintf($sql, $table_registros, $id_client, $nombreIns . ' ' . $apellidos, $crea, $telefonoIns, (int) $_REQUEST['provincia'], $legal, $email);
//echo $sql.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();
        $conexion->ejecuta_query($sql);
        $id = mysql_insert_id();
        $conexion->disconnectDB();
//echo $url.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();

        $fields = array(
            'Nombre' => $nombreIns,
            'Apellidos' => $apellidos,
            'Telefono' => $telefonoIns,
            'Pais' => $sbs_pais,
            'Ciudad' => '',
            'Mail' => $email,
            'Origen' => $sbs_origen,
            'Curso' => $sbs_curso,
            'Comentarios' => '',
            'Sexo' => $sexo,
            'Campana' => $sbs_campana,
            'cli' => $cli,
            'Key' => $sbs_key
        );



        $fields_string = http_build_query($fields);
        $ch = curl_init();

//set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $sbs_url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        $timeout = 0; // set to zero for no timeout

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
//execute post
        $result = curl_exec($ch);

//close connection
        curl_close($ch);

        $result_ws = getTextBetweenTags($result, 'Ack');

        $error = true;
        $response_ws = 0;
        if (strpos($result, '<Result>OK</Result>') !== false) {
            
            //Accedemos a ver el idchannel del emaily telefono
            $cls_misc = new cls_misc();
            $arr_idchann = json_decode($cls_misc->ws_getIdChannels($email, $telefono),true);
            $id_ret = $id.'-'.$arr_idchann['email'].'-'.$arr_idchann['telf'];
            unset($cls_misc);
            
            $error = false;
            $response_ws = 1;
            $resCurl['error'] = 0;
            $resCurl['id'] = $id_ret;
            //echo "OOOOKKKKKKKK";
            $tmpCrea = $_SESSION['idCreatividad'];
            $tmpName = session_name('sbs' . date(Yimis));
        }
        $sql = 'update %s set response_ws=%d,response_extended=\'%s\' where id_registro=%d';
        $sql = sprintf($sql, $table_registros, $response_ws, $result_ws, $id);
        //die($sql);
       // $resCurl['sql'] = $sql;
        $conexion->ejecuta_query($sql);


        /*  $ch = curl_init();
          $timeout = 0; // set to zero for no timeout
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
          $raw = curl_exec($ch);
          curl_close($ch);
          $resCurl = json_decode($raw);
          $error = $resCurl->error;

          if ($error) {
          $msg_err = 'Parece que el problema se debe a lo siguiente: "' . $resCurl->mensaje . '".';
          } else {
          $tmpCrea = $_SESSION['idCreatividad'];
          //            unset($_SESSION['namePromo']); // SE DESTRUYE LA REFERENCIA DE LA SESION (comentado el 2016.02.04)
          $tmpName = session_name('sbs' . date(Yimis));
          //            $_SESSION['namePromo'] = session_name(); // SE CONSTRUYE UNA NUEVA SESION PARA PERMITIR REGISTROS NUEVOS DESDE LA MISMA APERTURA (comentado el 2016.02.04)
          $_SESSION['idCreatividad'] = $tmpCrea;
          if (!$emailingSource) {
          die($raw);
          } else {
          $resCurl = json_decode($raw, true);
          }
          }
         * 
         */
    }
} else {
    if ($telefono == '') {
        $error = 1;
        $msg_err = 'Falta el campo Teléfono. Error 104';
    }
}

if ($error) {
    $query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
    $query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
    $conexion->getResultSelectArray($query);
    $resQuery = $conexion->tResultadoQuery;
    $idUpdate = $resQuery[0]['id'];
    $query = 'UPDATE %s SET http_referer=\'ERROR CTC: %s\' WHERE id=\'%d\'';
    $query = sprintf($query, $table_front_actions, $resCurl->mensaje, (int) $idUpdate);
    $conexion->ejecuta_query($query);
}

if (!$emailingSource) {
    $res = json_encode($resCurl);
    die($res);
}
?>