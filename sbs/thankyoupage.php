<?php
/**
 * Página de gracias
 */
@session_start();
include('../conf/config_web.php');
ini_set('display_errors',0);
include('conf/config_web.php');
include($path_raiz_includes . 'includes/initiate_thankyoupage.php');
include($path_raiz_includes . 'class/class.pixels.management.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>¡Gracias de <?php echo ucwords($landingMainName); ?>!</title>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width,initial-scale=1"/> 
<link href="<?php echo $path_raiz_aplicacion_local; ?>css/estilos_thanks.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- ************************* 
    PIXELES DE MAKE 
****************************** -->
<!-- Google Tag Manager  for MAKE -->
<script>
dataLayer = [{
	'pageCategory':'signup',
	'visitorType':'high-value',
	'transactionTotal':'1',
	'transactionId':'<?=$_REQUEST['leadNumber']?>',
	'template' : 'thankyoupage'
}];
</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TFTJXD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TFTJXD');</script>
<!-- End Google Tag Manager for MAKE -->

	<header>
		<section class="title-head">
			<img class="logo" src="<?php echo $path_raiz_aplicacion_local; ?>img/thak_logo.jpg" alt="SBS">		
		</section>
	</header>

  <section class="content">
    <img class="photo" src="<?php echo $path_raiz_aplicacion_local; ?>img/background1100.jpg" title="SBS formulario correcto"/> 
    <img class="photo-mini" src="<?php echo $path_raiz_aplicacion_local; ?>img/background400.jpg" title="SBS formulario correcto"/> 
    <section class="contact">
			<img class="phone" src="<?php echo $path_raiz_aplicacion_local; ?>img/thankyou_phone.png" alt="SBS phone"/> 
			<h1>En breve nos pondremos en contacto contigo.</h1>
    </section>
	</section>

	<footer>
	</footer>

<?php
if (!empty($_REQUEST['debug'])) {
    new dBug($_REQUEST);
}
include($path_raiz_includes . 'includes/pixels_thankyoupage.php');
session_destroy();
?>
</body>
</html>