<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
?>

		<div class="error"><ul></ul></div>
		<p><span class="form-text">BECA DEL 30%</span><br />
		PAGOS FRACCIONADOS<br />
		SIN INTER&Eacute;S<br />
		<span class="form-text2">FEBRERO 2016</span>
		</p>

		<div class="fila">
			<div class="fleft">
				<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" placeholder="Nombre" aria-required="true" />
			</div>
		</div>

		<div class="fila">
			<div class="fleft">
				<input type="text" name="apellidos" id="apellidos" value="<?=$apellidos?>" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Apellidos&lt;/strong&gt; es obligatorio" placeholder="Apellidos" aria-required="true" />
			</div>
		</div>

		<div class="row">
			<div class="col8">
				<input type="email" name="email" id="email" maxlength="100" class="celda" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="Email" aria-required="true" />
			</div>
		</div>

		<div class="fila">
			<div class="col8">
				<input type="tel" maxlength="9" name="telefono" id="telefono" class="celda" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Teléfono" aria-required="true" />
			</div>
		</div>

		<div class="legal" style="padding-top:10px;">
			<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;Acepto la 
			<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'privacy_policies.php';?>" data-ref="" target="_blank">política de privacidad</a>
			y <a class="enlace_condiciones" href="<?=$condicionesPromo ? $condicionesPromo : 'landing_conditions.php';?>" data-ref="" target="_blank">Términos y Condiciones</a>
		</div><!--/12-->

		<div class="row">
			<div class="espacio_btn">
				<input class="button green sendData" type="button" id="btnProcesar" name="btnProcesar" value="¡CONTÁCTANOS!">
			</div>
		</div>

		<div class="clearfix"></div>
