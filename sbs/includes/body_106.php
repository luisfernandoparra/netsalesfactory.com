<link href="<?=$path_raiz_aplicacion_local?>css/<?=$id_crea?>font-awesome.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$path_raiz_aplicacion_local?>js/global.js"></script>
<?php
$idLandingFormatted=str_pad($objSiteData->landingId,3,'0',STR_PAD_LEFT);
?>
<header class="">
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
	echo $objSiteData->headerTopContent;
    
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
</header>

<section class="basic-content">
	<section class="small-content">
<?php

if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div class="marco">
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!
			</div>
			<span class="bolo"></span>
';
}

?>
	<section class="formulario">

		<form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
			<input type="hidden" name="destino" id="destino" value="">

<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
$formScriptName='form_script_'.$idLandingFormatted.'.php';
}

$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
include($formScriptName);
?>

			</form>
		</section>
	</section>
</section>

<section class="text-content">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
</section>

<footer>

<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
</footer>

