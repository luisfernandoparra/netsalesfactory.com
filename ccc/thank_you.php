<?php
/**
 * THANK YOU PAGE
 *
 */
?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>¡Gracias!</title>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width,initial-scale=1"/>
<link href="css/thankyou_styles.css" rel="stylesheet" type="text/css">
</head>
<body>


<!-- Google Tag Manager  for MAKE -->
<script>
dataLayer = [{
	'pageCategory':'signup',
	'visitorType':'high-value',
	'transactionTotal': 1,
	'transactionId':'<?=$_REQUEST['targetData']?>',
	'template' : 'thankyoupage'
}];
</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WKQRSR3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WKQRSR3');</script>
<!-- End Google Tag Manager for MAKE -->

	<section class="contenedor">
	<section class="text">
		<p class="thanks">&iexcl;Gracias!</p>
		<p class="principal">Enseguida nos pondremos en contacto contigo.</p>

	</section>
			<section class="foto">
			<img src="img/img_ppal.jpg" width="228" height="53" alt=""/>
			 </section>

	</section>
</body>
</html>