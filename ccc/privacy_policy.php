<?php
/**
 * PRIVACY POLICY
 *
 */
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Pol&iacute;tica de privacidad</title>
</head>

<body>
	<h1>POL&Iacute;TICA DE PRIVACIDAD</h1>
	<h2>Raz&oacute;n Social</h2>

En cumplimiento del art&iacute;culo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Informaci&oacute;n y del Comercio Electr&oacute;nico, a continuaci&oacute;n se reflejan los siguientes datos:<br> 

La empresa titular del dominio web www.cursosccc.com  es “Centro para la Cultura y el Conocimiento S.A. (CCC)”, con sede en C/Orense 20, 1&ordm;, 28020 Madrid CIF A20052031, est&aacute; registrado el 3 de enero de 1978, Folio 31, Tomo 337, Hoja SS-1626.<br> 

	<h2>Personas Usuarias</h2>

El acceso y/o uso de este portal atribuye la condici&oacute;n de persona usuaria que acepta, desde dicho acceso y/o uso, las condiciones de uso del mismo que aqu&iacute; se especifican. Estas ser&aacute;n de aplicaci&oacute;n independientemente de las condiciones generales de contrataci&oacute;n que en su caso resulten de obligado cumplimiento.<br> 

	<h2>Pol&iacute;tica informativa de protecci&oacute;n de datos personales</h2>

Como titular responsable de los ficheros automatizados derivados de esta website, “Centro para la Cultura y el Conocimiento S.A. (CCC)” pone en conocimiento de las personas visitantes y usuarias de su p&aacute;gina web su pol&iacute;tica de protecci&oacute;n y tratamiento de datos de car&aacute;cter personal, que ser&aacute; aplicable en caso de que se rellene alguno de los formularios donde se recaben datos de car&aacute;cter personal, garantiz&aacute;ndose el cumplimiento de las obligaciones dispuestas por la normativa de protecci&oacute;n de datos personales.<br> 

Los datos personales recabados de las personas usuarias de nuestra web se recoger&aacute;n en el correspondiente fichero inscrito en el Registro General de la Agencia Espa&ntilde;ola de Protecci&oacute;n de Datos, de cuyo tratamiento es responsable “Centro para la Cultura y el Conocimiento S.A. (CCC)”, limit&aacute;ndose su recogida a los datos necesarios para la finalidad  de gestionar las consultas remitidas por este medio y mantene informado/a, por medios electr&oacute;nicos o f&iacute;sicos, de descuentos especiales y nuevos cursos de CCC o de promociones de productos y servicios de empresas que colaboran con CCC en el desarrollo formativo y que pueden ser destu inter&eacute;s, as&iacute; como para elaborar perfiles, estad&iacute;sticas o encuestas relacionadas con los servicios solicitados y sin los cuales no podr&aacute; ofrecerse el servicio solicitado por las personas usuarias. “Centro para la Cultura y el Conocimiento S.A. (CCC)” adopta las medidas de &iacute;ndole t&eacute;cnica y organizativa necesarias para garantizar la seguridad de los datos de car&aacute;cter personal y evitar as&iacute; su alteraci&oacute;n, p&eacute;rdida, tratamiento o acceso no autorizado, habida cuenta del estado de la tecnolog&iacute;a de acuerdo con lo establecido por la normativa de protecci&oacute;n de datos.<br> 

	<h2>Derechos</h2>

Los personas usuarias de esta web tienen reconocidos y podr&aacute;n ejercitar los derechos de acceso, cancelaci&oacute;n rectificaci&oacute;n y oposici&oacute;n a sus datos personales (Derechos ARCO).<br> 

En caso de requerir el ejercicio de estos derechos, podr&aacute; dirigirse por escrito remitiendo carta ordinaria por correo postal a la direcci&oacute;n de “Centro para la Cultura y el Conocimiento S.A. (CCC)” antes indicada o remitir un correo electr&oacute;nico a la siguiente direcci&oacute;n info@cursosccc.com, adjuntando, en ambos casos copia de documento acreditativo de su identidad.<br> 

	<h2>Voluntariedad</h2>

“Centro para la Cultura y el Conocimiento S.A. (CCC)”  informa a las personas usuarias de la p&aacute;gina web del car&aacute;cter voluntario u, en su caso, obligatorio de facilitar los datos de car&aacute;cter personal que le sean solicitados en los formularios de acceso y registro a los servicios y/o contenidos ofrecidos en la p&aacute;gina web.<br> 

Asimismo, “Centro para la Cultura y el Conocimiento S.A. (CCC)”  informa que en cumplimiento de la Ley 34/2002 de 11 de julio, de Servicios de la Sociedad de la Informaci&oacute;n y el Comercio Electr&oacute;nico solicita su consentimiento para el tratamiento de su correo electr&oacute;nico con fines comerciales, promocionales o divulgativos de la actividad de la organizaci&oacute;n.<br> 
A tal efecto, las personas usuarias de nuestra web, mediante la marcaci&oacute;n de la casilla correspondiente, aceptan que sus datos personales sean tratados para tales finalidades mediante remisi&oacute;n de comunicaciones por e-mail, fax, SMS, MMS o mensajer&iacute;a instant&aacute;nea, comunidades sociales o cualesquier otro medio electr&oacute;nico o f&iacute;sico, presente o futuro, que posibilite realizar comunicaci&oacute;n comercial o divulgativa relacionada con productos o servicios ofrecidos por la organizaci&oacute;n.<br> 

	<h2>Cesi&oacute;n de Datos</h2>

Los datos personales que sean facilitados por las personas usuarias de nuestra web no ser&aacute;n cedidos a terceros, salvo en el caso de que el cumplimiento de una obligaci&oacute;n legal as&iacute; lo ordene. En el caso de que, adem&aacute;s, “Centro para la Cultura y el Conocimiento S.A. (CCC)” lo considere necesario para el cumplimiento de finalidades leg&iacute;timas de la organizaci&oacute;n se solicitar&aacute; previamente el consentimiento previo de la persona a trav&eacute;s de los formularios espec&iacute;ficos contenidos en esta website.<br> 

	<2>Actualizaci&oacute;n Peri&oacute;dica</2>

“Centro para la Cultura y el Conocimiento S.A. (CCC)”  puede modificar esta Pol&iacute;tica de Privacidad en atenci&oacute;n al cumplimiento de obligaciones legales, o de su adaptaci&oacute;n a las instrucciones dictadas por la Agencia Espa&ntilde;ola de Protecci&oacute;n de Datos o los Tribunales competentes estatales o europeo.<br> 

	<h2>Veracidad de los datos</h2>

Las personas usuarias responder&aacute;n, en cualquier caso, de la veracidad de los datos facilitados y se hacen responsables de comunicar a “Centro para la Cultura y el Conocimiento S.A. (CCC)” la modificaci&oacute;n en los mismos, quedando la organizaci&oacute;n exenta de cualquier tipo de responsabilidad a este respecto.<br> 
	
</body>
</html>
