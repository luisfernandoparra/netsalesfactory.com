<?php
/**
 * PRIVACY POLICY
 *
 */
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Pol&iacute;tica de privacidad</title>
</head>

<body>

	<h1>POL&Iacute;TICA DE USO DE COOKIES</h1><br>
	<h2>Datos identificativos</h2><br>

En cumplimiento de la Ley 34/2002, de Servicios de la Sociedad de la Informaci&oacute;n y de Comercio Electr&oacute;nico, as&iacute; como del conjunto de la normativa que regula el comercio electr&oacute;nico, le informamos que esta p&aacute;gina Web es propiedad de “Centro para la Cultura y el Conocimiento S.A. (CCC).<br><br>  

Sede en C/Orense 20, 1&ordm;, 28020 Madrid CIF A20052031, est&aacute; registrado el 3 de enero de 1978, Folio 31, Tomo 337, Hoja SS-1626.<br><br> 

	<h2>&iquest;Qu&eacute; es una cookie?</h2><br>

Una cookie es un archivo que se instala en el ordenador de los usuarios, que tiene como finalidad recordar la navegaci&oacute;n de usuarios durante su visita a la p&aacute;gina web con el objeto de adecuar y mejorar los contenidos a la navegaci&oacute;n de los usuarios de nuestra web.<br><br> 

El usuario tiene plena capacidad para controlar el uso de cookies aceptando o rechazando su instalaci&oacute;n a voluntad mediante la configuraci&oacute;n del navegador. En ocasiones, algunas p&aacute;ginas requieren obligatoriamente para su funcionamiento la instalaci&oacute;n de las cookies de forma que si no se acepta su instalaci&oacute;n el usuario no podr&aacute; acceder correctamente a esas p&aacute;ginas.<br><br> 

Algunas de nuestras p&aacute;ginas instalan cookies de terceros con la finalidad de gestionar la informaci&oacute;n de la navegaci&oacute;n de los visitantes de la web y, cuando realicemos acciones promocionales conjuntas con otras entidades, con la finalidad de observar el correcto desarrollo de las mismas.<br><br>

	<h2>&iquest;Qu&eacute; tipos de cookies utiliza esta p&aacute;gina web?</h2><br>

Cookies propias: aqu&eacute;llas que se env&iacute;an al equipo terminal del usuario desde un equipo o dominio gestionado por el propio editor y desde el que se presta el servicio solicitado por el usuario.<br><br> 
Cookies de terceros: aqu&eacute;llas que se env&iacute;an al equipo terminal del usuario desde un equipo o dominio que no es gestionado por el editor, sino por otra entidad que trata los datos obtenidos trav&eacute;s de las cookies.<br><br> 
Cookies persistentes: son un tipo de cookies en el que los datos siguen almacenados en el terminal y pueden ser accedidos y tratados durante un periodo definido por el responsable de la cookie, y que puede ir de unos minutos a varios a&ntilde;os.<br><br> 
Cookies de an&aacute;lisis: nos permiten cuantificar el n&uacute;mero de usuarios y as&iacute; realizar la medici&oacute;n y an&aacute;lisis estad&iacute;stico de la utilizaci&oacute;n que hacen los usuarios del servicio ofertado. Para ello se analiza su navegaci&oacute;n en nuestra p&aacute;gina web con el fin de mejorar la oferta de productos o servicios que le ofrecemos.
Cookies de publicidad comportamental: son aqu&eacute;llas que permiten la gesti&oacute;n, de la forma m&aacute;s eficaz posible, de los espacios publicitarios que, en su caso, el editor haya incluido en una p&aacute;gina web, aplicaci&oacute;n o plataforma desde la que presta el servicio solicitado. Estas cookies almacenan informaci&oacute;n del comportamiento de los usuarios obtenida a trav&eacute;s de la observaci&oacute;n continuada de sus h&aacute;bitos de navegaci&oacute;n, lo que permite desarrollar un perfil espec&iacute;fico para mostrar publicidad en funci&oacute;n del mismo.<br><br> 
	<h2>&iquest;C&oacute;mo usamos las cookies?</h2><br>

Al acceder a nuestras p&aacute;ginas, si no tienes instaladas las opciones de navegador que bloquean las instalaci&oacute;n de las cookies, entendemos que nos est&aacute;s dando tu consentimiento para instalarlas en el equipo desde el que accedas y tratar la informaci&oacute;n sobre tu navegaci&oacute;n en nuestras p&aacute;ginas y para que puedas utilizar algunas funcionalidades que te permiten interactuar con otras aplicaciones. Con estos datos en ning&uacute;n caso podemos saber tus datos personales (nombre, apellidos u otros de contacto), ni desde qu&eacute; lugar en concreto est&aacute;s accediendo.<br><br> 

Esta informaci&oacute;n la utilizamos para mejorar nuestras p&aacute;ginas, detectar nuevas necesidades y valorar las mejoras a introducir con la finalidad de prestar un mejor servicio a las personas que nos visiten adapt&aacute;ndolas por ejemplo a los sistemas operativos y navegadores m&aacute;s utilizados.<br><br>

	<h2>&iquest;Qu&eacute; cookies de terceros usamos?</h2><br>

Para obtener esta informaci&oacute;n utilizamos estas herramientas y cada una de ellas inserta su propia cookie:<br><br> 

	Adap.tv y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace <a href="http://www.adap.tv/privacy" target="_blank">(http://www.adap.tv/privacy)</a><br><br> 

	Adconion y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace <a href="http://adconion.com/privacy-policy" target="_blank">(http://adconion.com/privacy-policy)</a><br><br> 

	AddThis y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace <a href="http://www.addthis.com/privacy/privacy-policy#.UcFmnflFo3A" target="_blank">(http://www.addthis.com/privacy/privacy-policy#.UcFmnflFo3A)</a><br><br> 

	Adswizz  y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace<a href="http://www.adswizz.com/news/privacy-policy" target="_blank">(http://www.adswizz.com/news/privacy-policy)</a><br><br> 

	AppNexus y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace<a href="http://appnexus.com/platform-policy" target="_blank">(http://appnexus.com/platform-policy)</a><br><br> 

	Double Click y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace<a href="http://www.google.com/policies/technologies/ads/" target="_blank">(http://www.google.com/policies/technologies/ads/)</a><br><br> 

	Google Adwords Conversion   y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace <a href="http://www.google.com/intl/en/policies/privacy/" target="_blank">(http://www.google.com/intl/en/policies/privacy/)</a><br><br> 

	Google Analytics   y se usan para realizar la medici&oacute;n y an&aacute;lisis estad&iacute;stico . Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace<a href="http://www.google.com/analytics/" target="_blank">(http://www.google.com/analytics/)</a><br><br> 

	Marin Search Marketer  y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace <a href="http://www.marinsoftware.com/privacy/privacy-central" target="_blank">(http://www.marinsoftware.com/privacy/privacy-central)</a><br><br> 

	Mouseflow y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace <a href="http://mouseflow.com/privacy/" target="_blank">(http://mouseflow.com/privacy/)</a><br><br> 

	Platform161 y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace <a href="http://platform161.com/company/privacy-policy/" target="_blank">(http://platform161.com/company/privacy-policy/)</a><br><br> 

	Right Media y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace<a href="http://info.yahoo.com/privacy/us/biz/rightmedia/details.html" target="_blank">(http://info.yahoo.com/privacy/us/biz/rightmedia/details.html)</a><br><br> 

	TradeDesk y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace<a href="http://www.thetradedesk.com/privacy-policy/" target="_blank">(http://www.thetradedesk.com/privacy-policy/)</a><br><br> 

	TradeDoublery se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace <a href="http://www.tradedoubler.com/es-es/privacy-policy/" target="_blank">(http://www.tradedoubler.com/es-es/privacy-policy/)</a><br><br> 

	Weborama y se usan para analizar el comportamiento  y gestionar la publicidad. Si desea tener mas informaci&oacute;n sobre para que va a usar la cookies siga el enlace <a href="http://www.weborama.com/e-privacy/our-commitment/" target="_blank">(http://www.weborama.com/e-privacy/our-commitment/)</a><br><br> 

	<h2>&iquest;C&oacute;mo puedes modificar el comportamiento de las cookies en tu navegador?</h2><br>

Puedes revocar tu permiso y gestionar la forma en que tu navegador controla las cookies (permitir, conocer, bloquear o eliminar las cookies instaladas en tu equipo) mediante la configuraci&oacute;n de las opciones del navegador que utilizas.<br><br> 

Encontrar&aacute;s la forma de hacerlo para cada navegador en las siguientes referencias:<br><br> 

	Chrome: <a href="http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647" target="_blank">http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647</a><br><br> 

	Explorer: <a href="http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9" target="_blank">http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9</a><br><br> 

	Firefox: <a href="http://support.mozilla.org/es/kb/cookies-informacion-que-los-sitios-web-guardan-en-" target="_blank">http://support.mozilla.org/es/kb/cookies-informacion-que-los-sitios-web-guardan-en-</a><br><br> 

 

	<h2>Notas adicionales</h2><br>

La organizaci&oacute;n CCC no se hace responsable del contenido ni de la veracidad de las pol&iacute;ticas de privacidad que puedan tener los terceros que, en su caso, se mencionen en la pol&iacute;tica de cookies.<br><br> 

Los navegadores web son las herramientas encargadas de almacenar las cookies y desde este lugar debe efectuar su derecho a eliminaci&oacute;n o desactivaci&oacute;n de las mismas. CCC no puede garantizar la correcta o incorrecta manipulaci&oacute;n de las cookies por parte de los mencionados navegadores.<br><br> 

En algunos casos es necesario instalar cookies para que el navegador no olvide su decisi&oacute;n de no aceptaci&oacute;n de las mismas.<br><br> 

Para cualquier duda o consulta acerca de esta pol&iacute;tica de cookies no dude en comunicarse con nosotros dirigiéndose por escrito a la direcci&oacute;n precedentemente indicada o la direcci&oacute;n de correo info@cursosccc.com.<br><br> 

	
</body>
</html>

<div class="closeCookiesPolicy" style="display:block;float:right;font-size:60% !important;"><a href="#null">Cerrar</a></div>
