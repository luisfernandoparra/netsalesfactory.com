<?php
/**
 * LANDING CCC 001 - DEFAULT BODY
 *
 * UTILIZAR SIEMPRE PARA PRUEBAS EL TLF QUE INICIE CON
 * 777 777 7xx
 */
?>

	<header>
		<img class="logo" src="img/<?php echo $ladingId?>/logo.jpg" width="228" height="108" alt=""/>
	</header>


	<section class="basic_content">

		<section class="small_content">

			<section class="skeleton">
				<article class="headline">
					<p>CURSO DE <span>GUITARRA</span></p>
				</article>
				<article class="text_principal">
					<p>Aprende a tocar la guitarra de manera r&aacute;pida y sencilla con el Curso de Guitarra de CCC. <span>Consigue una formaci&oacute;n musical casi sin darte cuenta.</span></p>
				</article>
				<article class="discount_summer">
					<p><span>AHORA</span> benef&iacute;ciate de un <span>50% de descuento</span> en tu matr&iacute;cula</p>
				</article>
		  </section>

			<section class="formulario">
				<article class="telephone_request_call <?php echo ($eventType == 'CTC' && (!$isMobile || $isTablet)) ? 'hide' : ($eventType == 'CTC' ? 'hide' : (($displayInbound) ? 'hide' :''))?>">
					<p class="telephone_request_call_description">Tel&eacute;fono gratuito</p>
					<p class="telephone_request_call_digits">900 20 21 28</p>
				</article>


				<!--BOTON LLAMADA DIRECTA-->
				<article class="formulario_btn_call_user <?php echo ($isMobile && !$isTablet ? ($eventType == 'CTC' ? 'hide' : (!$displayInbound && ($isMobile && !$isTablet)) ? 'hide' : '') : 'hide')?>">
					<input type="hidden" name="target_one" id="target_one" value="">
					<div id="campos">
						<div class="error"><ul></ul></div>

						<div class="row">
							<div class="espacio_btn_call">
								<input class="button green callNumber" type="button" id="btnInbound" name="btnInbound" value="LLÁMANOS" data-rel="external" />
							</div>
						</div>

						<div class="clearfix"></div>
					</div>
				</article>
				<!--FIN BOTÓN LLAMADA DIRECTA->


				<!--FORMULARIO TELEFONO Y BOTÓN-->
				<article class="formulario_write_send_telephone">
					<p>D&eacute;janos tu tel&eacute;fono y te contamos todos los detalles:</p>

					<form method="post" action="thank_you.php" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo" enctype="multipart/form-data">
						<input type="hidden" name="targetData" id="targetData" value="0">

						<div class="error"></div>
						<div class="fila">
							<div class="col8">
								<input type="tel" maxlength="9" name="telefono_callme" id="telefono_callme" class="celda phone" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Teléfono" aria-required="true" data-rule-morecheckdata="true" />
							</div>
						</div>

						<div class="telephone_btn">
							<div class="legal" style="padding-top:10px;">
								<input required="required" data-msg-required="Debe leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debe leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" aria-required="true" />&nbsp;&nbsp;Acepto la <a class="enlace_condiciones" href="<?php echo $linksWeb['privavyPolicyPage']?>" data-ref="" target="_blank">pol&iacute;tica de privacidad</a>
							</div>

							<div class="row">
								<div class="espacio_btn">
									<input class="button green sendData" type="submit" id="btnProcesar" name="btnProcesar" value="ENVIAR">
								</div>
							</div>
						</div>

						<div class="clearfix"></div>
					</form>

				</article>
				<!--FIN FORMULARIO TELEFONO Y BOTÓN-->


				<!-- TLF BOTTOM -->
				<article class="telephone_no_request_call <?php echo (($eventType == 'INB' && (!$isMobile || $isTablet)) ? 'hide' : (!$displayInbound && ($isMobile && !$isTablet) ? ($eventType == 'CTC' ? '' : 'hide') : ''))?>" >
					<p class="telephone_no_request_call_description">Tel&eacute;fono gratuito</p>
					<p class="telephone_no_request_call_digits">900 20 21 28</p>
				</article>

		  </section>

	  </section>

</section>

	<section class="text_content">
		<article class="text_description <?php echo ($isMobile && !$isTablet ? 'hide' : '')?>">
			<p>Tanto si sabes algo de guitarra como si no sabes nada, al finalizar el curso tendr&aacute;s los conomientos para ganarte la vida como profesor de guitarra.</p>
		</article>

		<article class="offer">
			<img src="img/<?php echo $ladingId?>/offer.png" width="278" height="173" alt=""/> </article>

		<article class="extra_info">
			<article class="advantages_info deskOne">
				<img src="img/<?php echo $ladingId?>/icon1.png" width="45" height="45" alt=""/>
				<p>Pago a plazos sin intereses</p>
			</article>

			<article class="advantages_info deskTwo">
				<img src="img/<?php echo $ladingId?>/icon2.png" width="45" height="45" alt=""/>
				<p>Tutor <br>personal</p>
			</article>

			<article class="advantages_info deskThree">
				<img src="img/<?php echo $ladingId?>/icon3.png" width="45" height="45" alt=""/>
				<p>Garant&iacute;a de aprendizaje</p>
			</article>

			<article class="advantages_info deskFour">
				<img src="img/<?php echo $ladingId?>/icon4.png" width="45" height="45" alt=""/>
				<p>Campus <br>online</p>
			</article>

		</article>
	</section>

	<footer>
	 <section class="footer">
		<p>© Centro para la Cultura y el Conocimiento. <a href="<?php echo $linksWeb['privavyPolicyPage']?>" target="_blank">Pol&iacute;tica de privacidad</a> | <a href="<?php echo $linksWeb['cookiesPage']?>" target="_blank">Pol&iacute;tica de Cookies</a> | <a href="<?php echo $linksWeb['legalNoticePage']?>" target="_blank">Aviso Legal</a> |</p>
		</section>
	</footer>

	<div id="startScroll"></div>
	<div id="timerDisplay"></div>

	<div class="modalBottomCookies" id="modalBottomCookies">
		<div class="innerCookiesModal">
			<div class="contentModalBottomCookies">
				<a title="Cerrar" class="closeModalBottomCookies" href="javascript:void(0);">X</a>
				<p><strong>Uso de cookies</strong></p>
				<p>Utilizamos cookies propias, de analítica y de terceros para mejorar tu experiencia de usuario. Al continuar con la navegación consideramos que aceptas la instalación y el uso de cookies, <a target="_blank" id="id_enlace_politica_privacidad_cookies" href="<?php echo $linksWeb['cookiesPage']?>">más información aquí</a>.</p>
			</div>
		</div>
	</div>



	<div id="pie_galletero">
			<div id="pie_galletero_left" class="pie_galletero_left">
					<span id="blockTxtPieAsk">Utilizamos cookies propias, de analítica y de terceros para mejorar tu experiencia de usuario. Si aceptas la instalación de cookies o continúas navegando, consideramos que aceptas su uso.<br></span>
					<div style="float:left;width:100%;">
							<center><a href="#startScroll" id="cookie_info" class="smooth">Más información aquí</a></center>
					</div>
			</div>
			<div id="pie_galletero_right"><center>
							<input type="button" id="cookie_acept" name="cookie_acept" value="Acepto" class="cookieButton"> <input type="button" id="cookie_rejection" name="cookie_rejection" value="No Acepto" class="cookieButton">
					</center>
			</div>
			<div class="clearfix">&nbsp;</div>
	</div>

