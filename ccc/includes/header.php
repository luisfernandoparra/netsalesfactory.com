<?php
/**
 * COMMON HEADER SCRIPT
 */

// LOCAL CUSTOM ARRAYS
$arrMessagesPhoneCheck=[
	'default'=>'Debe introducir un n&uacute;mero de tel&eacute;fono'
	,'error'=>'Parece que el n&uacute;mero de tel&eacute;fono no es v&aacute;lido'
	,'duplicate'=>'El tel&eacute;fono introducido <b>ya existe</b> en nuestros archivos'
	,'communication_error'=>'Parece que ha habido un fallo en la comunicaci&oacute;n...'
];

$linksWeb['privavyPolicyPage']='privacy_policy.php';
$linksWeb['legalNoticePage']='legal_notice.php';
$linksWeb['cookiesPage']='cookies.php';


/**
 * Si estamos en modo Debug
 * @param boolean $debug True: estamos debuggeando
 */
$debug=!empty($_GET['debug']);

/**
 * Evento proveniente de la Newsletter
 * @param string $eventType Evento proveniente de la Newsletter. Valores CTC INB
 */
$eventType=(!empty($_GET['eventType']) && in_array(strtoupper($_GET['eventType']), $arr_eventTypes)) ? strtoupper($_GET['eventType']) : 'CTC';

/**
 * Vemos si el Call está en horario de apertura
 */
$month=date('n'); //Mes sin cero inicial
$day=date('N'); //Dia de la semana
$arr_ccOpened=$arr_cc_schedule['0'];

if(array_key_exists($month, $arr_cc_schedule)){
	________________debug('Pillado Mes', $month);
	$arr_ccOpened=$arr_cc_schedule[$month];
}

________________debug('Mes scheduled', $arr_ccOpened);

$arr_cc_Opened_day=$arr_ccOpened['0'];

if (array_key_exists($day, $arr_ccOpened)) {
	$arr_cc_Opened_day=$arr_ccOpened[$day];
	________________debug('Dia scheduled Entrado', $day);
}

________________debug('Dia scheduled', $arr_cc_Opened_day);

$ccOpened=(!in_array($day,$arr_ccOpened['EXCLUDE'])) && (int) date('Hm') >= (int) $arr_cc_Opened_day['FROM'] && (int) date('Hm') < (int) $arr_cc_Opened_day['TO'];

/**
 * Recogemos el dispositivo, en este caso SOLO nos interesa si navega por Móvil
 */
require('lib/mobile_detect.php');
$device=new Mobile_Detect();
$isMobile=$device->isMobile();
$isTablet=$device->isTablet();
$userAgent=$device->getUserAgent();
$isMac=!$isMobile && (strpos($userAgent, 'Mac OS') !== false);
________________debug('getUserAgent', $device->getUserAgent());

unset($devices);

/*
 * Variable que dirá si se puede mostrar Inbound.
 * Lógica:
 *   Horario del Call abierto
 *   Tipo de evento INB (inbound)
 *   Is Móvil
 *   No es una tablet. Hemos visto que isMobile devuelve tanto dispositivos móviles como tablet. En una tablet no podemos asegurar que se pueda procesar el inbound, por lo que lo tratamos como si fuese un 
 *   ordenador de sobremesa.
 */

$displayInbound=$ccOpened && $eventType == 'INB' && $isMobile && !$isTablet;

________________debug('IsMobile', $isMobile);
________________debug('IsTablet', $isTablet);

________________debug('Evento', $eventType);
________________debug('ccOpened', $ccOpened);
________________debug('isMac', $isMac);
________________debug('displayInbound', $displayInbound);

/**
 * Función de Debuggeo que mostrará por pantalla la información pasada
 * @param type $title
 * @param type $obj
 */
function ________________debug($title, $data){
	if($GLOBALS['debug']){
		echo '<hr><h2>' . $title . '</h2>';
		if(is_array($data) || is_object($data)){
			echo '<pre>';
			print_r($data);
			echo '</pre>';
		} else {
			echo $data;
		}
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $commonVars[$ladingId]['pageTitle']?></title>

<link href="css/<?php echo $ladingId?>_styles.css" rel="stylesheet" type="text/css">
<link href="css/footer_cookies.css" rel="stylesheet" type="text/css">

<script src="js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/<?php echo $ladingId?>_ccc.js"></script>

<script type="text/javascript">
var tel=<?php echo $inboundTelf;?>;
var validator;
var isCheck=0;
var msgCheckPhone="<?php echo $arrMessagesPhoneCheck['default']?>";

// START COOKIES CONTROL
var firstStep=true;
var checkElapsedTime;
var cookiesEnabled=0;
var cookiesManaged=0;
//// END COOKIES CONTROL


function manageCookies(start)
{
	cookiesEnabled=start;
	cookiesManaged=1;
	$("#pie_galletero_right").slideUp("fast", function(){
		$("#pie_galletero_right").css("width", "0px !important");
		$("#pie_galletero_right").css("width", "0px !important");
		$("#pie_galletero_left").removeClass("pie_galletero_left");
		$(".pie_galletero_left").css("width", "98% !important", function(){
				$("#cookie_info").css("text-align", "center");
		});
		$("#cookie_info").css("text-align", "center");
	});

	if(start == 1){
		drawDefaultPix();
	}
}

function drawDefaultPix(){}

/**
 * Función que lanza el inbound
 * @returns
 */
function triggerInbound(){
	if(<?php echo preg_match('/Android/',$userAgent)?>)
		window.location.href='tel://'+tel;
	else
		window.location.href='tel://'+tel;
}

function eraseModalBottomCookies()
{
	$(".modalBottomCookies").fadeOut("fast");
}

$(document).ready(function(){

	$('#btnInbound').click(function(e){
<?php
if($displayInbound)
    echo 'triggerInbound();';
?>

	});

<?php
if($displayInbound)
   echo 'triggerInbound();';
?>

	$("#enviarPorMailSolicitaInfo").validate({
		onkeyup:false,
		onclick:false
		,showErrors:function(errorMap, errorList){
			if(errorList[0].element.name == "telefono_callme")
				errorList[0].message=msgCheckPhone;

			this.defaultShowErrors();
		}
		,rules:{
			cblegales:{
				required:true
			}
			,telefono_callme:{
				required:true
				,number:true
				,remote:{
					url:"includes/<?php echo $commonVars[$ladingId]['scriptCheckPhone']?>",
			    contentType:"application/json; charset=utf-8",
					data:{Nombre:"", submedio:"<?php echo $arr_ccc_submedio['EMAIL']?>", codcurso:"<?php echo $ccc_course?>", cod_postal_callme: "",urlCustomer:"<?php echo $ccc_url_ws?>"},cache:false,async:false,
					dataFilter:function(response){
						var jsonResponse=JSON.parse(response) ? JSON.parse(response) : undefined;

						if(jsonResponse.success == true && jsonResponse.status == 1)
						{
							$("#targetData").val(jsonResponse.id);
							validator.resetForm();
							isCheck=telefono_callme.value;
							return true;
						}

						if(isCheck == $("#telefono_callme").val())
							return true;

						msgCheckPhone=(jsonResponse.status == "duplicate") ? "<?php echo $arrMessagesPhoneCheck['duplicate']?>" : "<?php echo $arrMessagesPhoneCheck['error']?>";
						return false;
					}
				}
			}
		}
		,messages:{
			telefono_callme:"<?php echo $arrMessagesPhoneCheck['default']?>"
    }
		,errorLabelContainer: $("#enviarPorMailSolicitaInfo div.error")
	});


	$.validator.addMethod(
		"morecheckdata",
		function(value, element){
			var check=/^[9|6|7|8][0-9]{8}$/.test(value);
			if(!check)
			{
				msgCheckPhone="Debe introducir un n&uacute;mero de tel&eacute;fono";
			}
			return check;
		}
	);


//	$("#telefono_callme").on("change",function(){
//		if(!this.value)
//			return false;
//			validator.resetForm();
//	})

	validator=$("#enviarPorMailSolicitaInfo").validate({
		errorContainer:$('div.error'),
//		errorLabelContainer:$('div.error'),
		wrapper:'li'
	});


	$(window).click(function(){
		if (firstStep === true)
		{
			manageCookies(1);
			setTimeout("eraseModalBottomCookies()", 500)
		}
		firstStep=false;
	});

	$(window).on('scroll', function(){
		if (firstStep === true)
		{
			manageCookies(1);
			setTimeout("eraseModalBottomCookies()", 500)
		}
		firstStep=false;
	})

	$(".closeModalBottomCookies").click(function(){
		eraseModalBottomCookies()
	});

	$(".closeCookiesPolicy").click(function(){
		$('#info_cook').slideUp("slow");
		return false;
	});

	$("#cookie_acept").click(function(){
		manageCookies(1);
	});

	$("#cookie_rejection").click(function(){
		manageCookies(0);
	});

	$("#pie_galletero_left").click(function (e) {
		e.preventDefault();
		$('#info_cook').show();
		$('html,body').animate({scrollTop: $("#startScroll").offset().top}, 500);
	});

});

function includeScript(file_path, type)
{
	document.head=document.head || document.getElementsByTagName('head'[0]);
	if (type == "js")
	{
		var j=document.createElement("script");
		j.type="text/javascript";
		j.src=file_path;
		document.head.appendChild(j);
	}
	return;
}

</script>

</head>

