<?php
/**
 * Fichero de entrada desarrollado para CCC
 * El sistema recibirá de una newsletter una acción de click (botón Llamame o Te Llamamos) y el sistema, dependiendo de la acción y del horario, 
 * mostrará creatividades, módulos y textos
 * 
 * @date 2017.07.05
 * @version 1.1
 * @author Netsales (LFP) <luisfer.parra@netsales.es>
 * @author Netsales (MF) <mario.francescutto@netsales.es>
 * 
 * @param string $eventType Evento proveniente de la newsletter. Si ha clicado el botón de CTC o Inboun. valores CTC, INB
 * @param boolean $debug Si estamos en modo debug
 * @param integer $_REQUEST[ladingRef] Opcional para identificar la landing a duibujar
 * 
 * Variables para seleccionar qué mostrar en el front:
 * @param boolean $displayInbound Si se muestra el botón de Inbound. Para ello se deben presentar varias opciones:
 *  - Es móvil
 *  - Está en horario de apertura del CAll
 *  - No es una Tablet
 *  - Es un mac (facetime). Desactivado ya que el call no tiene Facetime
 */
include('conf/conf.ig.php');


$headerPage=(file_exists('includes/'.$ladingId.'header.php') ? $ladingId.'_header.php' : 'header.php');
include('includes/'.$headerPage);

?>
<body>


<!-- Google Tag Manager  for MAKE -->
<script>
dataLayer=[{
	'pageCategory':'signup',
	'visitorType':'high-value',
	'template' : 'home'
}];
</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WKQRSR3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WKQRSR3');</script>
<!-- End Google Tag Manager for MAKE -->


<?php
/**
 * BODY & FORM CONTENTS
 */
include('includes/'.$ladingId.'_body.php');
?>


<div id="info_cook" style="display:none;">

<?php
/**
 * COOKIES TEXTS
 */
include('cookies_body_contents.php');
?>
</div>

<div id="footerWebPage" style="display:block;"></div>

</body>
</html>
