<?php

/**
 * Fichero de configuracón y miscelaneas
 */
/**
 * Array con las posibles acciones de los botones. 
 *      CTC: Click To call. Deja tu teléfono que el call te llamará
 *      INB: Inbound. El usuario es quien va a llamar
 * 
 */
$arr_eventTypes = array('CTC', 'INB');


$arrValidLanding=[1];	// LANDING DEFINIDAS Y VALIDAS

$ladingId=isset($_REQUEST['ladingRef']) && $_REQUEST['ladingRef'] ? $_REQUEST['ladingRef'] : 1;
$ladingId=in_array($ladingId, $arrValidLanding) ? $ladingId : 1;

$commonVars['001']['pageTitle']='CCC curso guitarra';
$commonVars['001']['scriptCheckPhone']='check_data.php';

$ladingId=str_pad($ladingId,3,'0',STR_PAD_LEFT);

//echo '<hr>'.$ladingId;
/**
 * Horario de apertura del Call.
 * De momento lo dejamos como horas fijas
 * Months=>Dias de semana=>HOrario apertura
 * 0: default
 * Según está ahora, por defecto (mes 0) para todos los días de la semana
 */
$arr_cc_schedule = array(
    '0' => array(
        '0' => array('FROM' => '930', 'TO' => '1830'),
        '5' => array('FROM' => '930', 'TO' => '1730'),
        'EXCLUDE' => array('6', '7')
    ),
    '8' => array(
        '0' => array('FROM' => '930', 'TO' => '1730'),
        'EXCLUDE' => array('6', '7')
    )
);
/**
 * Definici´o de teléfono para realizar inbound
 */
$inboundTelf='900202128';

/**
 * identificador del curso
 */
$ccc_course = '0352711';
/**
 * Parámetro de submedio, dependiendo del source. Por defecto para nosotros supongo será EMAIL
 */
$arr_ccc_submedio = array(
    'EMAIL' => 'AGIJ',
    'SEM' => 'AGIK',
    'RRSS' => 'AGIL',
    'RETARGETING' => 'AGIM',
    'OTHER' => 'AGIN'
);

//$ccc_url_ws = 'http://www.cursosccc.com/mailing/clicktocall-externo.php?Nombre=[%NOMBRE%]&telefono_callme=[%TELEFONO%]&submedio=[%SUBMEDIO%]&codcurso=[%CODCURSO%]&cod_postal_callme=[%CODCP%]';
$ccc_url_ws='http://www.cursosccc.com/mailing/clicktocall-externo.php';
