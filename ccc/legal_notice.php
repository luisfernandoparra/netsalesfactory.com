<?php
/**
 * LEGAL NOTICE
 *
 */
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Pol&iacute;tica de privacidad</title>
</head>

<body>

	<h1>AVISO LEGAL</h1>
Centro para la Cultura y el Conocimiento S.A. es un centro de formaci&oacute;n a distancia y elearning autorizado en virtud del Real Decreto 2641/1980, O.M. 29/6/1981 y orden 6/10/1982 (B.O.P.V.).

Con sede en C/Orense 20, 1&ordm;, 28020 Madrid CIF A20052031, est&aacute; registrado el 3 de enero de 1978, Folio 31, Tomo 337, Hoja SS-1626. Contacto: info@cursosccc.com

 

	<h2>Somos una web de confianza. Estamos adheridos a CONFIANZAONLINE</h2>

	El sello distintivo de CONFIANZAONLINE reconoce la transparencia y credibilidad de las webs adheridas, acreditando su compromiso &eacute;tico y social a todos los usuarios.Confianza Online Las partes se someten, a su elecci&oacute;n, para la resoluci&oacute;n de los conflictos y con renuncia a cualquier otro fuero, a los juzgados y tribunales del domicilio del usuario. Asimismo, como entidad adherida a Confianza Online y en los t&eacute;rminos de su C&oacute;digo &Eacute;tico, en caso de controversias relativas a la contrataci&oacute;n y publicidad online, protecci&oacute;n de datos y protecci&oacute;n de menores, el usuario podr&aacute; acudir al sistema de resoluci&oacute;n extrajudicial de controversias de Confianza Online <a href="www.confianzaonline.es" target="_blank">(www.confianzaonline.es)</a>

	<h2>Reservados todos los derechos</h2>

Toda la informaci&oacute;n, dise&ntilde;o, im&aacute;genes y otros elementos gr&aacute;ficos contenidos en esta web son parte de una obra cuya propiedad intelectual pertenece a Centro para la Cultura y el Conocimiento S.A.

Los usuarios que visiten esta web tienen permitido visualizar la informaci&oacute;n contenida en la misma, imprimirla, descargarla y realizar reproducciones privadas en su equipo inform&aacute;tico; por el contrario, queda desautorizado el uso de esta informaci&oacute;n y elementos gr&aacute;ficos para su reproducci&oacute;n a trav&eacute;s de cualquier medio que est&eacute; alojado o soportado en un servidor conectado a una red pública (Internet) o a una red local, salvo previa autorizaci&oacute;n por parte de Centro para la Cultura y el Conocimiento S.A.

Centro para la Cultura y el Conocimiento S.A. responde por el correcto funcionamiento de su web y la veracidad de sus contenidos. Sin embargo, no se hace responsable de cualquier incidencia que imposibilite la visita a la web originada por una interrupci&oacute;n o defectuosa prestaci&oacute;n del suministro de telecomunicaciones ajeno a esta empresa;Centro para la Cultura y el Conocimiento S.A. declina igualmente toda responsabilidad derivada de los da&ntilde;os y perjuicios que pudiera sufrir el visitante de su web en sus medios inform&aacute;ticos o telem&aacute;ticos a consecuencia de la acci&oacute;n de un tercero, o provocada por conflictos sociales u otros conflictos de fuerza mayor, incluidos los requerimientos u &oacute;rdenes administrativas o judiciales, o saturaciones sean o no intencionadas.

	<h2>Medici&oacute;n de audiencia de esta web</h2>

Este sitio web utiliza las herramientas de Google Analytics para la medici&oacute;n de audiencia y an&aacute;lisis del comportamiento de los visitantes.

A los efectos de esta medici&oacute;n no queda registrado ningún tipo de dato personal. La medici&oacute;n de audiencia la realizamos con el objetivo de mejorar la experiencia de navegaci&oacute;n del visitante en nuestra web, adecuarla a sus solicitudes y acelerar el proceso de localizaci&oacute;n de la informaci&oacute;n.

La herramienta que utilizamos para medir nuestra audiencia es Google Analytics, si quieres puedes conocer cu&aacute;l es la Pol&iacute;tica de Privacidad de Google Analytics. 

Consulta nuestra Pol&iacute;tica de uso de Cookies.

	
</body>
</html>
