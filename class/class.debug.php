<?php

//********************************************************************************************
//Librería de control de consultas ejecutadas
//
//         Funciones incorporadas:
//                     CDEBUG(): constructor de la clase
//					   _CDEBUG(): destructor de la clase
//                     insertToDebug($query,$mNumRegs,$mStatusQuery): inserta en el array del debug la query asociada a un onejto CBBDD
//                     showAllQuerys(): muestra el listado de querys ejecutadas con exito
//                     getQueryError(): muestra la consulta que ha fallado
//                     showServerVars(): muestra las $_SERVER variables
//                     showSessionVars(): muestra las $_SESSION variables
//                     showFilesVars(): muestra las $_FILES variables
//                     showRequestVars(): muestra las $_REQUEST variables
//                     showGetVars(): muestra las $_GET variables
//					   showPostVars(): muestra las $_POST variables
//					   showCookieVars(): muestra las $_COOKIE variables
//					   showAllVars(): muestra TODAS las variables
//         Caracteristicas: 
//********************************************************************************************
class CDEBUG {
        //************************************************************************
        //                      VARIABLES DE CLASE
        //************************************************************************
        var $tConsultasejecutadas;//Es un array de objetos donde guardamos las consultas que se han ejcutado correctamente                                  		
        var $tConsultaError;     //es un objeto que guarda la ultima consulta que resulto erronea
                                 
        //************************************************************************
        //                      METODOS DE CLASE
        //************************************************************************
        //......................................................................
        //.......................CONSTRUCTOR....................................
        //......................................................................
		function CDEBUG()
		{
           //INICIALIZACION DE LAS VARIABLES
		   //variables relacionadas con las consultas a bbdd;
		   //1) arrays de consultas ok           		   
           $this->tConsultasDebugEjecutadas=array();
		   $this->tNumRegsConsultasDebug=array();
		   $this->tStatusConsultasDebug=array();
		   //2) array de consultas erroneas		   
           $this->tConsultasErroneas=array();
		   $this->tConsultaErrorProducido=array();
		   
		   //3)variables relacionadas con los errores
		   $this->tErrores=array();
		   		   		   
		}
        //......................................................................
		//............DESTRUCTOR................................................
        //......................................................................
		function _CDEBUG()
		{
			//variables relacionadas con las consultas a bbdd
           $this->tConsultasDebugEjecutadas = null;
		   $this->tNumRegsConsultasDebug=null;
		   $this->tStatusConsultasDebug = null;
           $this->tConsultasErroneas = null;		  
		   $this->tConsultaErrorProducido = null;
		   
		   //3)variables relacionadas con los errores
		   $this->tErrores = null;		   
		}
		//......................................................................
        //............INSERTAR ERRORES EN EL ARRAY DE ERRORES ..............
        //......................................................................

		function insertErrorInToDebug($error){		
		   	array_push ($this->tErrores,$error);
		}
        //......................................................................
        //............INSERTAR OBJETO BBDD EN EL ARRAY DE CONSULTAS..............
        //......................................................................

        function insertQueryToDebug($query,$mNumRegs,$mStatusQuery,$errorQuery="")
        {
			
			if(strtoupper($mStatusQuery)=='S'){
					array_push ($this->tConsultasDebugEjecutadas,$query);//guardamos num reg dicha consulta
					array_push ($this->tNumRegsConsultasDebug,$mNumRegs); //guardamos num reg dicha consulta
					array_push ($this->tStatusConsultasDebug,$mStatusQuery);//guardamos status consulta					
			}
			else{
					//echo "entro por error<br>";
					array_push ($this->tConsultasErroneas,$query);//guardamos num reg dicha consulta
					array_push ($this->tConsultaErrorProducido,$errorQuery);//guardamos num reg dicha consulta								
			}
			 return;
        }//end function insertToDebug
        
		function showAllErrors()
        {	
			echo "<pre>";
			echo "<font size='4'><strong>=============================== ERRORES  ==================================</strong></font><br>";
			for($i=0;$i<count($this->tErrores);$i++){				
				echo "<br>///////////////----- ERROR $i::: -----///////////////<br>";
				$error_format = $this->tErrores[$i];
              	echo "&nbsp;&nbsp;".$error_format."<BR>";              	
			}				
			echo "</pre>";
		
        }//end function
		
		function showAllQuerys()
        {	
					
			echo "<pre>";
			echo "<font size='4'><strong>=============================== CONSULTAS  ==================================</strong></font><br>";
			
			for($i=0;$i<count($this->tConsultasDebugEjecutadas);$i++){	
				$this->getQueryExito();	
				/*		
				echo "<br><b>///////////////----- CONSULTA $i ejecutada con exito:::".$this->tStatusConsultasDebug[$i]." -----///////////////</b><br>";
				$consulta_format = $this->tConsultasDebugEjecutadas[$i];
              	echo "&nbsp;&nbsp;".$consulta_format."<BR>";
              	echo "&nbsp;&nbsp;<strong>NUM REG:</strong><b>".$this->tNumRegsConsultasDebug[$i]."</b><BR>";
				*/				
			}
			//echo "------jfsljf".$i."<br>";
			//echo "<font size='4'><strong>======================= CONSULTAS ERRONEAS ====================================</strong></font><br>";
			for($j=0;$j<count($this->tConsultasErroneas);$j++){	
				$this->getQueryError();	
				/*		
				echo "<br><b>///////////////----- CONSULTA $i ejecutada de forma erronea::: -----///////////////</b><br>";
				$consulta_format = $this->tConsultasErroneas[$j];
              	echo "&nbsp;&nbsp;".$consulta_format."<BR>";
              	echo "&nbsp;&nbsp;<strong>ERROR PRODUCIDO:</strong>".$this->tConsultaErrorProducido[$j];
				*/
			}	
			echo "</pre>";
			
        }//end function
		
		function getQueryError(){
				$i = count($this->tConsultasErroneas)-1;	
				echo "<br><b>///////////////----- CONSULTA $cont ejecutada de forma erronea::: -----///////////////</b><br>";
				$consulta_format = $this->tConsultasErroneas[$i];
              	echo "&nbsp;&nbsp;".$consulta_format."<BR>";
              	echo "&nbsp;&nbsp;<strong style=\"color:#ff0000;\">ERROR PRODUCIDO:</strong><span style=\"color:#ff0000;\">".$this->tConsultaErrorProducido[$i],"</span>";
		}
		
		function getQueryExito(){
				$i = count($this->tConsultasDebugEjecutadas)-1;
				echo "<br><b>///////////////----- CONSULTA $i ejecutada con exito:::".$this->tStatusConsultasDebug[$i]." -----///////////////</b><br>";
				$consulta_format = $this->tConsultasDebugEjecutadas[$i];
              	echo "&nbsp;&nbsp;".$consulta_format."<BR>";
              	echo "&nbsp;&nbsp;<strong>NUM REG:</strong><b>".$this->tNumRegsConsultasDebug[$i]."</b><BR>";				
		}  
		 
		/*				
        function getQueryError()
        {
			echo "<pre>";
            echo "<font size='4'><strong>============== CONSULTA ERRONEA ======================</strong></font><br>".$this->tConsultaError."<br>";
			echo "ERROR PRODUCIDO:".$this->tConsultaErrorProducido;
			echo "</pre>";            
        }//end function 
        */
		function showServerVars(){
			if(count($_SERVER)>0){				
				echo "<pre>";
				echo "<font size='4'><strong>=================================== SERVER ===================================</strong></font><br>";
				print_r($_SERVER);//
				echo "</pre>";
			}
		}//end 
		
		function showSessionVars(){
			if(count($_SESSION)>0){				
				echo "<pre>";
				echo "<font size='4'><strong>=================================== SESSION ==================================</strong></font><br>";
				print_r($_SESSION);//
				echo "</pre>";
			}
		}//end
		
		function showFilesVars(){
			if(count($_FILES)>0){				
				echo "<pre>";
				echo "<font size='4'><strong>=================================== FILES ====================================</strong></font><br>";
				print_r($_FILES);//
				echo "</pre>";
			}
		}//end
				
		function showRequestVars(){
			if(count($_REQUEST)>0){				
				echo "<pre>";
				echo "<font size='4'><strong>=================================== REQUEST ==================================</strong></font><br>";
				print_r($_REQUEST);//
				echo "</pre>";
			}
		}//end
		
		function showGetVars(){
			if(count($_GET)>0){				
				echo "<pre>";
				echo "<font size='4'><strong>=================================== GET ======================================</strong></font><br>";
				print_r($_GET);//
				echo "</pre>";
			}
		}//end
		function showPostVars(){
			if(count($_POST)>0){				
				echo "<pre>";
				echo "<font size='4'><strong>=================================== POST =====================================</strong></font><br>";
				print_r($_POST);//
				echo "</pre>";
			}
		}//end
		
		function showCookieVars(){
			if(count($_COOKIE)>0){				
				echo "<pre>";
				echo "<font size='4'><strong>=================================== COOKIE ====================================</strong></font><br>";
				print_r($_COOKIE);//
				echo "</pre>";
			}
		}//end
		
		function showAllVars(){
				$this->showServerVars();
				$this->showSessionVars();
				$this->showFilesVars();
				$this->showRequestVars();
				$this->showGetVars();
				$this->showPostVars();								
				$this->showCookieVars();
		}//end                                 
}//end class
?>
