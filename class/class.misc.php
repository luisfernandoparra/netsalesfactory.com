<?php

/**
 * @version 1.0
 * @date 2016.02.11
 *
 * @author LFP
 * Clase que contendrá funciones globales necesarias o tontunadas
 */
class cls_misc {

//put your code here
    /**
     * Variable que dirá si se está en modo debug o no
     * @var boolean 
     */
    private $debug = false;

    /**
     * Array que contendrá los parámetros para acceder al WS que recoje los id channels de email y teléfono
     * @var array 
     */
    private $ws_getIdChannel = array(
        'url' => 'https://www.ws-server.netsales.es/webservices/CrmErp/get_idchannel_fromCRM.php',
        'usr' => 'netsalesLandingsServer',
        'pwd' => '874d245cd448d93a44c04c1af1462225');

    /**
     * Variables de Conexion
     */
    /*
      var $conexion;
      var $id_conexion;
      var $conexion_exists;
     */

    public function ws_getIdChannels($email, $telefono) {
        $params = array('usr' => $this->ws_getIdChannel['usr'], 'pwd' => $this->ws_getIdChannel['pwd'], 'email' => $email, 'telf' => $telefono);
        $url = $this->ws_getIdChannel['url'] . '?' . http_build_query($params);
        //echo $url;
        $resp = file_get_contents($url);
        //echo "<pre>";
        //print_r($resp);
        //die('out');
        /**
         * Rellenamos los datos que estén vacíos con 0
         *     die(json_encode(array('result' => 'OK', 'error' => 0, 'email' => $idChannEml, 'telf' => $idChannTelf)));
         */
        $arr_resp = json_decode($resp, true);
        //if ($arr_resp['error']==0) {
        $e = (empty($arr_resp['email'])) ? 0 : $arr_resp['email'];
        $t = (empty($arr_resp['telf'])) ? 0 : $arr_resp['telf'];
        $arr_resp['email'] = $e;
        $arr_resp['telf'] = $t;
        //}
        return json_encode($arr_resp);
    }

    /**
     * Setter de la URL, User o Password del WS que accede a recoger el idchannel de email y teléfono
     * @param string $url URL del ws
     * @param string $usr Usuario del wS
     * @param string $pwd Password del WS
     */
    function set_ws_getIdChannel($url = '', $usr = '', $pwd = '') {
        if (!empty($url))
            $this->ws_getIdChannel['url'] = $url;
        if (!empty($usr))
            $this->ws_getIdChannel['usr'] = $usr;
        if (!empty($pwd))
            $this->ws_getIdChannel['pwd'] = $pwd;
    }

    /**
     * Setter para debuggear
     * @param type $debug
     */
    function set_debug($debug) {
        $this->debug = $debug;
    }

    /**
     * función pública que debuggeara la entrada
     * @param string $title título que mostrará
     * @param variant $data dato a debuggear
     * @param boolean $forceDebug Si forzamos el debuggeo, para cosas temporales
     */
    public function ____debug($title, $data, $forceDebug = false) {
        if ($this->debug || $forceDebug) {
            echo '<hr><h2>' . $title . '</h2>';
            if (is_array($data) || is_object($data)) {
                echo '<pre>';
                print_r($data);
                echo '</pre>';
            } else {
                echo $data;
            }
        }
    }

    /**
     * Constructor de la clase
     */
    function __construct() {
        /*
          Conexion
         */
        /*
          $this->conexion = new CBBDD($GLOBALS['db_type'], $GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pass'], $GLOBALS['db_name'], $GLOBALS['debug_pagina']);
          $this->conexion_exists = $this->conexion->connectDB();
          $this->conexion->get_id_conexion();
         */
    }

    /**
     * Destructor de la clase
     */
    public function __destruct() {
        /*
          if ($this->conexion_exists) {
          $this->conexion->disconnectDB();
          unset($this->conexion);
          }
         */
    }

}
