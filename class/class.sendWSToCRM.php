<?php

/**
 * Clase que mandará datos por curl al CRM (u otro sistema).
 *
 * 
 * @author luisfernandoparra
 * @version 1.0
 */
class class_WS2CRM extends CBBDD {

//put your code here
    /**
     * Objeto de conexión de la bbdd
     * @var Object BBDD 
     */
    private $conexion;

    /**
     * Tabla registros
     * @var string 
     */
    private $table_registros;

    /**
     * variable que dirá si está debuggeando o no
     * @var boolean
     */
    private $dBug = false;
    /**
     * Variable que si existe, no se utilizará inner join para buscar el premio en la tabla de premios.
     * @var string
     */
    private $premio;

    /**
     * Array con variables de configuración. 
     * Tendremos valores estáticos y pero también se podrán añadir
     * La clave del array será el id_cliente de los datos a enviar, y luego todos los valores de configuración en otro array adicional
     * La clave del segundo nivel es la crea
     * El 0 es crea por defecto
     * @var array 
     */
    private $arr_ws_config = array(44 =>
        array(33 => 'http://176.58.125.192/crm/ws_ns/server.php?fuente=186&nombre=[%%NOMBRE%%]&apellido1=[%%APELLIDOS%%]&email=[%%EMAIL%%]&telefono=[%%TELEFONO%%]&ip=[%%IP%%]&fecha_inscripcion=[%%FECHA_INSCRIPCION%%]&url_sorteo=[%%URL_SORTEO%%]&premio=[%%PREMIO%%]&cod_afiliado=[%%COD_AFILIADO%%]&afiliado=[%%AFILIADO%%]&id_ext=[%%ID_EXT%%]',
            32 => 'http://176.58.125.192/crm/ws_ns/server.php?fuente=185&nombre=[%%NOMBRE%%]&apellido1=[%%APELLIDOS%%]&email=[%%EMAIL%%]&ip=[%%IP%%]&fecha_inscripcion=[%%FECHA_INSCRIPCION%%]&url_sorteo=[%%URL_SORTEO%%]&premio=[%%PREMIO%%]&cod_afiliado=[%%COD_AFILIADO%%]&afiliado=[%%AFILIADO%%]&id_ext=[%%ID_EXT%%]',
            0 => 'http://176.58.125.192/crm/ws_ns/server.php?fuente=186&nombre=[%%NOMBRE%%]&apellido1=[%%APELLIDOS%%]&email=[%%EMAIL%%]&ip=[%%IP%%]&fecha_inscripcion=[%%FECHA_INSCRIPCION%%]&url_sorteo=[%%URL_SORTEO%%]&premio=[%%PREMIO%%]&cod_afiliado=[%%COD_AFILIADO%%]&afiliado=[%%AFILIADO%%]&id_ext=[%%ID_EXT%%]'),
        45 => array(0 => 'http://176.58.125.192/crm/ws_ns/server.php?fuente=187&nombre=[%%NOMBRE%%]&apellido1=[%%APELLIDOS%%]&email=[%%EMAIL%%]&ip=[%%IP%%]&fecha_inscripcion=[%%FECHA_INSCRIPCION%%]&url_sorteo=[%%URL_SORTEO%%]&premio=[%%PREMIO%%]&cod_afiliado=[%%COD_AFILIADO%%]&afiliado=[%%AFILIADO%%]&id_ext=[%%ID_EXT%%]'),
        46 => array(0 => 'http://176.58.125.192/crm/ws_ns/server.php?fuente=190&nombre=[%%NOMBRE%%]&apellido1=[%%APELLIDOS%%]&email=[%%EMAIL%%]&ip=[%%IP%%]&fecha_inscripcion=[%%FECHA_INSCRIPCION%%]&url_sorteo=[%%URL_SORTEO%%]&premio=[%%PREMIO%%]&cod_afiliado=[%%COD_AFILIADO%%]&afiliado=[%%AFILIADO%%]&id_ext=[%%ID_EXT%%]'));
//http://176.58.125.192/crm/ws_ns/server.php?fuente=26&email=EMAIL&telefono=TELEFONO&ip=IP&fecha_inscripcion=FECHA_INSCRIPCION&url_sorteo=URL_SORTEO&premio=PREMIO&cod_afiliado=COD_AFILIADO&afiliado=AFILIADO&id_ext=ID_EXT
    /**
     * Array con todos los WS a enviar
     * @var array 
     */
    private $arr_ws_tosend = array();

    /**
     * Función pública que cogerá los datos de un clicnete, y opcional de una crea específica, y lso enviará al WS específico.
     * @param integer $id_cliente identificador del cliente
     * @param integer $id_crea opcional. Crea que realizará la query
     */
    public function getData($id_cliente, $id_crea = false) {
        if (!array_key_exists($id_cliente, $this->arr_ws_config))
            exit();
        $arr = $this->_getDataFromDB($id_cliente, $id_crea);
        foreach ($arr as $dat) {
            $id_crea = (array_key_exists($dat['ID_CREA'], $this->arr_ws_config[$id_cliente])) ? $dat['ID_CREA'] : 0;
            $url_ws = $this->arr_ws_config[$id_cliente][$id_crea];
            $this->arr_ws_tosend[$dat['ID_EXT']] = $this->_compose_url($dat, $url_ws);
        }
        $this->_manage_send();
        $this->__debug($this->arr_ws_tosend, 'WS');
    }

    /**
     * Funcón privada que gestiona y actualiza el array a enviar con los datos dle WS
     */
    private function _manage_send() {
        foreach ($this->arr_ws_tosend as $id => $url) {
            $res = $this->__send_by_curl($url);
            $this->__debug($res, 'ENVIO');
            $sql = 'update %s set b_extraccion=1 where id_registro=%d';
            $sql = sprintf($sql, $this->table_registros, $id);
            $this->conexion->ejecuta_query($sql);
        }
    }

    /**
     * Función que envía por curl los datos del ws
     * @param string $url url con los datos y todo.
     * @return string-objet resultado del ws
     */
    private function __send_by_curl($url) {
        $output = array('result' => 1);
        $timeout = 0; // set to zero for no timeout
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $respuesta = curl_exec($ch);
        curl_close($ch);
        if ($respuesta === false) {
            $output['error'] = curl_errno($ch) . " - " . curl_error($ch);
            $output['result'] = 0;
            return $output;
        }
        
        return $respuesta;
    }

    /**
     * Fiunción privada que recorrerá el dato y creará una url que será la que se deberá enviar al cliente
     * @param array $dat array unidimensional con los datos
     * @param string $url url a modificar
     * @return string URL generada
     */
    private function _compose_url($dat, $url) {
        //$this->__debug($this->conexion->tNombreColumnas, $url);
        foreach ($this->conexion->tNombreColumnas as $col) {
            $column = strtoupper(trim($col));
            $url = str_replace('[%%' . $column . '%%]', $dat[$column], $url);
        }
        return $url;
    }

    /**
     * Función privada que accede a la bbdd y saca los datos a enviar
     * @param integer $id_cliente identificador del cliente
     * @param integer $id_crea opcional.
     */
    private function _getDataFromDB($id_cliente, $id_crea) {
        $sql = 'select id_registro as ID_EXT, reg.id_crea as ID_CREA, if(nombre is null,\'UNKNOWN\',nombre) as NOMBRE, if(apellidos is null,\'UNKNOWN\',apellidos) as APELLIDOS, email as EMAIL, telefono as TELEFONO, ip as IP, url as URL_SORTEO, date_format(fecha_insercion,\'%Y-%m-%d\') as FECHA_INSCRIPCION, ';
         if (empty($this->premio)) {
             $sql .='land.landing_name as PREMIO, ';
             $innerSiteConfig = ' inner join nl_landings_site_config land on reg.id_crea=land.id and land.client_id=' . $id_cliente;
         } else {
             $sql .= '\''.$this->premio.'\' as PREMIO, ';
             $innerSiteConfig='';
         }
               $sql .=  'reg.id_source as COD_AFILIADO, sourc.source as AFILIADO ';
        $sql .= ' from ' . $this->table_registros . ' reg '.$innerSiteConfig . ' left join sources sourc on reg.id_source=sourc.id_source where id_cliente=' . $id_cliente;
        if ($id_crea) {
            $sql .= ' and id_crea=' . $id_crea;
        }
        $sql .= ' and b_extraccion=0 order by id_crea';
        $this->conexion->getResultSelectArray($sql);
        $arr = $this->conexion->tResultadoQuery;
        $this->__debug($this->conexion->tNombreColumnas, 'NOMBRE COLS');
        $this->__debug($sql, 'query');
        $this->__debug($arr, 'datos');
        return $arr;
    }

    /*     * ************* ************* */
// COPNSTRUCT AND DESTRUCTS 
    /*     * ***************** ****** */
/**
 * Setter que pone un premio, y así no lo busca en la bbdd
 * @param string $premio nombre del premio
 */
    function setPremio($premio) {
        $this->premio = $premio;
    }

        
    
    /**
     * Función que carga datos GLOBALES
     */
    private function ___config() {
        $this->table_registros = $GLOBALS['table_registros'];
        $this->conexion = new CBBDD($GLOBALS['db_type'], $GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pass'], $GLOBALS['db_name']);
        $this->conexion->connectDB();
    }

    /**
     * Función para hacer debug
     * @param undefined $debug objeto a debuggear
     * @param string $comment Comentario adicional a poner.
     */
    private function __debug($debug, $comment = '') {
        if ($this->dBug) {
            if ($comment != '') {
                echo '<h1>' . $comment . '</h1>';
            }
            if (is_array($debug) || is_object($debug)) {
                new dBug($debug);
            } else {
                echo '<br>' . $debug;
            }
        }
    }

    /**
     * Construcción
     * @param boolean $dBug sí eestamos en fase de debug
     */
    function __construct($dBug = false) {
        $this->dBug = $dBug;
        $this->___config();
    }

    public function __destruct() {
        unset($this->conexion);
        unset($this->arr_ws_config);
        unset($this->arr_ws_tosend);
    }

}
