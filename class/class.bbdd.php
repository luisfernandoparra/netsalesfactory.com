<?php
//********************************************************************************************
//Librería básica de funciones de manejo de base de datos
//
//         Funciones incorporadas:
//                     connectDB(): funcion que realiza la conexion y elige la BBDD con la que trabajaremos
//                     disconnectDB(): funcion que realiza la desconexion de la BBDD con la que trabajaremos
//                     getResultSelectArray($pQuery): funcion que guarda enun array el reultado de un SELECT
//                     ejecuta_query($pQuery): funcion que ejecuta una consulta que no es un SELECT (TRANSACCIONES EN MYSQL)
//                     get_error(): Obtiene el ultimo error producido
//                     get_consultas(): Obtiene todas las consultas ejecutadas y si estas se ejecutaron con exito o no
//                     get_tipo():  Obtiene el tipo de SGBD al que nos estamos conectando
//                     get_id_conexion(): Obtiene el identificado de conexion asignado por PHP
//                     getNumRows(): Obtiene el numero de registros de la consulta
//                     get_id() : Obtiene el ultimo id de una cosulta INSERT
//         Caracteristicas:
//                    v1:  Soporte acceso a multiples BBDDS: MySQL,SQLserver
//                    v2:  Soporte acceso PostGresSQL
//                         Soporte para transacciones
//
//********************************************************************************************
class CBBDD {
        //************************************************************************
        //                      VARIABLES DE CLASE
        //************************************************************************
        var $idConexion; //identificador de la conexion
        var $dbtype;     //tipo de BBDDs a la que nos conectamos
        var $db;         //nombre de la BBDDs a la que queremos conectarnos
        var $host;         //host donde se encuentra instalado el SGBD
        var $user;         //usuario de acceso a la BBDDs
        var $password;     //password de dicho usuario
        var $port;         //puerto de acceso (Mysql=3306,PostGreSQL=5432)
				var $charset;      //charset a utilizar (Por defecto utf-8)
        var $mQueryConExito;  //indica si la query se ejecuto con exito o no
        var $mError;       //guarda el ultimo error producido
       	var $mErrno;       //guarda el numero correspondiente al error producido
        var $mNumRegs;     //numero de registros que devuelve la ultima consulta
        var $tNumRegsConsultas;//array con el numero de registros de todas las consultas ejecutadas.Bueno para TRAZAS
        var $tResultadoQuery;//Es un array de dimension 2, las columnas corresponden a las
                             //columnas solicitadas y las filas corresponden a los registros encontrados
                             //Es un array de filas, que son a su vez array de columnas
        var $Id;             //guardamos el ultimo identificador de un INSERT
        var $tNombreColumnas;//Es un array de dimension 1, con los nombres de las columnas solicitadas en
                             //el mismo orden en el que están en el array tqueryResult

        var $tConsultasejecutadas;//Es un array donde guardamos todas las consultas que se han ejecutado
                                  //INteresante para las trazas
        var $tRegistroConsultas; //es un array donde guardamos si la consulta se ejecuto con exito o no
                                 //INteresante para las trazas

        //************************************************************************
        //                      METODOS DE CLASE
        //************************************************************************
        //......................................................................
        //.......................CONSTRUCTOR....................................
        //......................................................................
		function CBBDD($dbtype, $host, $user, $password, $db='',$objdebug='',$charset='utf8',$port=1312) //
		{
			//INICIALIZACION DE LAS VARIABLES;
			$this->idConexion = null;
			$this->dbtype = $dbtype;     //tipo de BBDDs a la que nos conectamos
			$this->host = $host;         //host donde se encuentra instalado el SGBD
			$this->user = $user;         //usuario de acceso a la BBDDs
			$this->password = $password;     //password de dicho usuario
			$this->db = $db;         //nombre de la BBDDs a la que queremos conectarnos
			$this->objdebug = $objdebug;
			$this->port=$port;        //Creo que esto es sólo para POSTGRESQL
//			$this->port=1312;        //Creo que esto es sólo para POSTGRESQL
			$this->charset=$charset; 		//charset
			$this->mNumRegs=0;     //numero de registros que devuelve la consulta
			$this->tNumRegsConsultas=array();
			$this->mError = null;
			$this->mErrno = null;
			$this->Id = null;
			$this->mQueryConExito = 'S';
			$this->tResultadoQuery=array();
			$this->tNombreColumnas=array();
			$this->tConsultasejecutadas=array();
			$this->tRegistroConsultas=array();
		}
        //......................................................................
		//............DESTRUCTOR................................................
        //......................................................................
		function _CBBDD()
		{
			$this->obj=null;
			$this->idConexion = null;
			$this->dbtype = null;
			$this->host = null;
			$this->user = null;
			$this->password = null;
			$this->db = null;
			$this->objdebug=null;
			$this->port = null;
			$this->charset=null;
			$this->mError = null;
			$this->mErrno = null;
			$this->Id = null;
			$this->mQueryConExito = 'N';
			$this->mNumRegs=0;
			$this->tNumRegsConsultas=null;
			$this->tResultadoQuery=null;
			$this->tNombreColumnas=null;
			$this->tConsultasejecutadas=null;
			$this->tRegistroConsultas=null;
		   //$this->tDebug=null;
		}

		//CONNECT SEGUN BBDD
        function connectDB()
        {
//echo "dbtype:::".(trim($this->port))."<br>";
         	$con=false;
					$obj=false;
					$obj = new CBBDDMsSqli($this->dbtype,$this->host,$this->user,$this->password,$this->db,$this->objdebug,$this->charset,$this->port);

				//CASOS DE BBDD
//				switch(strtoupper(trim($this->dbtype))){
//					/*case 'POSTGRESSQL': $obj = new CBBDDPostgress($this->dbtype,$this->host,$this->user,$this->password,$this->db,$this->objdebug,$this->charset);
//										break;
//					case 'MYSQL': 		$obj = new CBBDDMySql($this->dbtype,$this->host,$this->user,$this->password,$this->db,$this->objdebug,$this->charset);
//										break;
//					case 'MSSQL': 		$obj = new CBBDDMsSql($this->dbtype,$this->host,$this->user,$this->password,$this->db,$this->objdebug,$this->charset);
//										break;
//                                              * */
//
//					case 'MSSQLI':		$obj = new CBBDDMsSqli($this->dbtype,$this->host,$this->user,$this->password,$this->db,$this->objdebug,$this->charset);
//
//										break;
//				}
				//var_dump($obj);
				if($obj){
					$this->obj=$obj;
					$con = $obj->connectDB();

					if($con){
						$this->idConexion=$con;
					}

					//establecemos el charset
//echo '<PRE>'.$this->charset;print_r($con);die();
					$this->setCharset($this->charset,$con);
				}
				return $con;//devuelve true o false

        }//end function connectDB

		//DISCONNECT SEGUN BBDD
        function disconnectDB(){

				if($this->idConexion){

				$this->obj->disconnect($this->idConexion);
					//$this->_CBBDD();si pongo esto hay que poner el disconnet al final de la pagina pues si no cualqueir cosa despues de este no funcionaria
				}
        }//end function disconnectDB

		function setCharset($chst,$idconn){
			return $this->obj->setCharset($chst,$this->get_id_conexion());
		}

		function sendEmailReport($pQuery,$type) {
			if(QUERY_SEND_EMAIL_TRACKING === 1) {
				 $url = (!empty($_SERVER['HTTPS'])) ? 'https://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
				$body = 'ERROR '.date('Y-m-d H:i:s')." \n QUERY: ".$pQuery." \n ERRN: ".$this->mErrno." \n ERRORMSG: ". $this->mError." \n PAG: ".$url;
				$subject = 'GTP6: ERROR EN '.$type.' '.$_SERVER['SERVER_NAME'].' '.date('Y-m-d H:i:s');
				/*$line = $url.";".$pQuery.";".$this->mError.";".$this->mErrno.";".$type."\r\n";
				$fp = fopen($GLOBALS['FileUploadDirectoryLogError'].date('Y-m-d').".csv",'a');
				fwrite($fp,$line);
				fclose($fp);*/
//echo '-->'.QUERY_EMAIL_TRACKING_TO,$subject,$body;die();
				@mail(QUERY_EMAIL_TRACKING_TO,$subject,$body);
			}
		}

		//Funcion que ejecuta una sentencia SELECT y guarda el Resultado en un array
    function getResultSelectArray($pQuery, $keyIdLabel=null)
		{
			$cur_id_conn = $this->get_id_conexion();
			//echo "current con SELECT ".$cur_id_conn."<br>";
			if(trim($this->mError)!='' || !$cur_id_conn) {return false;}
			else{//si no hay errores
					if($pQuery){
//						 $this->obj->executeQuery('SET NAMES utf8',$cur_id_conn);	// ADDED BY M.F. 2017.01.02
						 $mResult = $this->obj->executeQuery($pQuery,$cur_id_conn);
						 array_push($this->tConsultasejecutadas,$pQuery);

						 if(!$mResult){
								 $this->mError = $this->obj->getLastError($cur_id_conn);
								 //Caso de error en la consulta registramos dicho error
								 $this->mQueryConExito = 'N';
								 array_push ($this->tRegistroConsultas,$this->mQueryConExito); //guardamos si la consulta se ejecuto con exito o no

//								 if($this->objdebug!='')
//									$this->objdebug->insertQueryToDebug($pQuery,'-1',$this->mQueryConExito,$this->mError);

								 $this->sendEmailReport($pQuery,'QUERY');
								 return false;
						}
						 else{
						 		$this->mNumRegs = $this->obj->getNumberRows($mResult);
								array_push ($this->tNumRegsConsultas,$this->mNumRegs);
								array_push ($this->tRegistroConsultas,$this->mQueryConExito);

//								if($this->objdebug!='')
//									$this->objdebug->insertQueryToDebug($pQuery,$this->mNumRegs,$this->mQueryConExito); //insertamos consulta en el debug

								$i = 0;

								$this->tResultadoQuery = array();
								while($i < $this->mNumRegs) {
									$vRow = $this->obj->getFila($mResult);
									//Construimos el array con los registros de la consulta
									$keyIndex=$keyIdLabel ? $vRow[$keyIdLabel] : $i;

									$this->tResultadoQuery[$keyIndex] = $vRow;

//										if($i == 0){
//												//Construimos el array con los campos de la consulta
//												$k=0;
//												while( list( $aCampo, $eValor ) = each( $this->tResultadoQuery[$keyIndex]) ){
//													   $this->tNombreColumnas[$k] = trim($aCampo);
//													   $k++;
//												}
//										}
									if($i == 0)	//Construimos el array con los campos de la consulta
									{
										$k=0;
										foreach($vRow as $aCampo => $eValor)
										{
											if($aCampo != $keyIndex)
											{
												$this->tNombreColumnas[$k] = trim($aCampo);
												$k++;
											}
										}
									}
									$i++;
								}
								return true;
						}
					}
			}
		}

		//EJECUTAR QUERY INSERT,UPDATE,DELETE
		function ejecuta_query($pQuery)
    {
			$mResult=0;
			$cur_id_conn = $this->get_id_conexion();
			if(trim($this->mError)=='' && $cur_id_conn){//si no hay errores
//				$this->obj->setCharset('utf8');
//				mysql_set_charset('utf8',$cur_id_conn);
//				$this->obj->executeQuery('SET NAMES utf8',$cur_id_conn);	// ADDED BY M.F. 2017.01.02
				$mResult = $this->obj->executeQuery($pQuery,$cur_id_conn);

				if(!$mResult && $this->mQueryConExito=='S'){
					$this->mQueryConExito= 'N';
					$this->mError = $this->obj->getLastError($cur_id_conn);
					$this->sendEmailReport($pQuery,'EXECUTION');
				}

				$this->mNumRegs = $this->obj->getAffectedRows($cur_id_conn);

				if(strpos(strtoupper(trim($pQuery)),'INSERT')===0){//solo en caso de insertar, calculamos el ultimo id insertado.MODIFIED 20/09/2008
					$this->Id = $this->obj->getLastIdInserted($cur_id_conn);
				}

				array_push($this->tConsultasejecutadas,$pQuery); //guardamos dicha consulta
				array_push($this->tNumRegsConsultas,$this->mNumRegs); //guardamos num registros de  dicha consulta
				array_push($this->tRegistroConsultas,$this->mQueryConExito); //guardamos si la consulta se ejecuto con exito o no
				//METEMOS EL OBJETO EN DEBUG
				if($this->objdebug!=''){
//					$this->objdebug->insertQueryToDebug($pQuery,$this->mNumRegs,$this->mQueryConExito,$this->mError); //insertamos consulta en el debug
				}
//echo '( '.$mResult.' )$cur_id_conn='.print_r($cur_id_conn);

				if(isset($_SESSION['usuario']) && isset($_REQUEST['accion']) && (@$_REQUEST['accion'] != 'edit'))	// SOLO PARA LOGS DEL ADMIN
				{
					$keyCheck=0; $formName=''; $details='';

					foreach($_REQUEST as $key=>$data)
					{
						if(is_array($data))
							$data=implode(',',$data);

						$details.=$key.'='.$data.';';
					}

					$idRecordToSave=isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
					$script=(empty($_SERVER['HTTP_REFERER'])) ? '' : $_SERVER['HTTP_REFERER'];

					$arrPath=explode('/',$script);

					foreach($arrPath as $key=>$value)
					{
						if($keyCheck)
						{
							$formName=$value;
							break;
						}

						if($value == ADMINFOLDER)
							$keyCheck=1;
					}

					$query='INSERT INTO %s (id_usuario, fecha, ora, url, ip_usuario, accion, details, id_elemento, formulario) VALUES (%d,CURDATE(),CURTIME(),\''.$script.'\',\''.$_SERVER['REMOTE_ADDR'].'\',\''.$_REQUEST['accion'].'\',\''.$details.'\','.$idRecordToSave.',\''.$formName.'\')';
					$query=sprintf($query, 'log_accesos_backoffice',(int)$_SESSION['usuario']);

					if($pQuery == 'BEGIN')
					{
//						$this->obj->executeQuery('SET NAMES utf8',$cur_id_conn);	// ADDED BY M.F. 2017.01.02
						$this->obj->executeQuery($query,$cur_id_conn);
					}
						
				}
			}

			return $mResult;
		}

		function getNumRows(){
			return $this->mNumRegs;
		}

		//DEVOLVEMOS EL OBJETO
		function getInstance(){
			return $this;
		}
		function get_tipo(){
                 return $this->dbtype;
        }
        function get_id_conexion(){
                 return $this->obj->get_id_conexion();
        }
        function get_id(){
            return $this->Id;
        }
		function getHost(){
			return $this->host;
		}
		function getUser(){
			return $this->user;
		}
		function getClave(){
			return $this->password;
		}
		function getPort(){
			return $this->port;
		}
		function getDb(){
			return $this->db;
		}

		//......................................................................
        //............METODOS DE GESTION DE ERRORES.............................
        //......................................................................
        function getError(){
            return $this->mError;
        }

        function get_consultas(){   //TRAZAS
            $res='</pre><hr><h2>CONSULTAS</h2>';
          for($i=0;$i<count($this->tConsultasejecutadas);$i++){
              $res.='<br><b>--------CONSULTA '.$i.' ejecutada con exito::'.$this->tRegistroConsultas[$i].'-------</b><br>';
              $res.=$this->tConsultasejecutadas[$i].'<BR>';
              $res.='NUM REG:<b>'.$this->tNumRegsConsultas[$i].'</b><BR>';
          }//end for
					return $res;
        }

		//ESTABLECEMOS VALORES
		function setError($val){
			$this->mError = $val;
		}
		function setQueryStatus($val){
			$this->mQueryConExito = $val;
		}
		function insertErrorInToDebug($val){
			return;
			$a=$this->getError();
			$this->objdebug->insertErrorInToDebug($a); //insertamos error en el debug
		}

	public function logBackOffice($idRef=0, $scriptName='',$resulAction=null)
	{
		$urlLog='';
//echo"\n\n";print_r(key($_REQUEST));

		if(!$idRef)
			$idRecordToSave=isset($_REQUEST['mainKeyId']) ? (int)$_REQUEST['mainKeyId'] : (isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : $idRef);
		else
			$idRecordToSave=$idRef;

		$currentAction=@$_REQUEST['action'];
		$currentAction=isset($_REQUEST['searchPhrase']) && $_REQUEST['searchPhrase'] ? $currentAction.' / find: '.$_REQUEST['searchPhrase'] : $currentAction;

		$scriptName=isset($_REQUEST['modName']) ? $_REQUEST['modName'].$scriptName : (isset($_REQUEST['from']) ? $_REQUEST['from'].$scriptName : $scriptName);
		$scriptName=isset($_REQUEST['moduleFolder']) && $_REQUEST['moduleFolder'] ? '['.$_REQUEST['moduleFolder'].'] '.$scriptName : $scriptName;
		if(@$_SESSION['id']<3)return;	// SOLO PARA TEST DE SESION

		if(isset($_SESSION['userAdmin']['userId']) && isset($currentAction) && (@$currentAction != 'edit'))	// SOLO PARA LOGS DEL ADMIN
		{
			$keyCheck=0; $details='';
			$tmpReferrer=@$_SERVER['HTTP_REFERER'];
			$tmpReferrer=$tmpReferrer ? urldecode($tmpReferrer) : $tmpReferrer;
			$urlLog=(empty($tmpReferrer) && $scriptName) ? $_SERVER['SCRIPT_NAME'] : $urlLog ? $urlLog : $tmpReferrer;
			$arrPath=explode('/',$scriptName);
		}

		$currentAction=isset($currentAction) ? $currentAction : 'view';
		$currentAction=isset($_REQUEST['newStatusValue']) ? $currentAction.', now='.$_REQUEST['newStatusValue'] : $currentAction;
		$currentAction=isset($resulAction) ? $currentAction.'::'.$resulAction : $currentAction;
		$addInfo=@key(@$_REQUEST['db']) ? ', '.key(@$_REQUEST['db']) : '';
		$scriptName=(isset($scriptName) ? $scriptName : basename($_SERVER['SCRIPT_NAME']));

		if($currentAction == 'login')
		{
			$currentAction.=': '.@$_REQUEST['user'];
			$_SESSION['userAdmin']['userId']=@$this->rowData ? $this->rowData : @$_SESSION['userAdmin']['userId'];
		}

		if(!$scriptName && !$addInfo || $scriptName == 'common_admin.php')
			$scriptName='table='.(isset($_REQUEST['tblName']) ? $_REQUEST['tblName'] : (isset($_REQUEST['mainTable']) ? $_REQUEST['mainTable'] : @$scriptName));

		if($currentAction == 'getTableListData' && $scriptName == '[_modules/backoffice_logs/] ')	// EVITA EL LOG DEL MODULO "backoffice_logs"
			return false;

		$query='INSERT INTO '.QUERY_TABLES_PREFIX.TABLE_CURRENT_LOGS.' (id_user, log_date, log_time, url, ip_user, action, id_element, script) VALUES ('.(int)@$_SESSION['id']['userId'].', CURDATE(), CURTIME(),\''.$urlLog.'\',\''.$_SERVER['REMOTE_ADDR'].'\',\''.$currentAction.'\','.(int)@$idRecordToSave.',\''.$scriptName.$addInfo.'\')';
		$cur_id_conn = $this->get_id_conexion();
		$this->obj->executeQuery($query,$cur_id_conn);

//echo $resulAction."]\n".$query."\nscriptName=".$scriptName.' <--> '.$addInfo."\n";print_r($_REQUEST);print_r($_SERVER);print_r($_SESSION);die('----');
//echo"\n\n";print_r(key($_REQUEST['db']));
//echo basename($_SERVER['SCRIPT_NAME']). '<pre>';print_r($_SESSION);print_r($_REQUEST);print_r($_SERVER);//print_r($this);
		return;
	}

}//END CLASE


class CBBDDMsSqli extends CBBDD
{
		//......................................................................
        //............METODOS DE CONEXION,DESCONEXION DE LA BBDD.............................
        //......................................................................
		function connectDB(){//CONEXION CON MYSQL
//if($this->getPort() != '1312'){print_r($this);echo '<p>'.$this->getPort();die();}
			   $conn = mysqli_connect($this->getHost(),$this->getUser(), $this->getClave(),$this->getDb(),$this->getPort());

				if( !$conn ) {
					$this->setError('No se puede conectar con el servidor :').mysqli_connect_errno().' '.mysqli_connect_error();
					$this->insertErrorInToDebug('No se puede conectar con el servidor:');
					return false;
					 }/*
					 elseif(!(mysql_select_db($this->getDb(),$conn))) {//
						$this->setError("No se puede conectar con la BBDD:");
					$this->insertErrorInToDebug("No se puede conectar con la BBDD:");
					return false;
					 }*/
				//echo "conn:::::".$conn."<br>";
				$this->idConexion = $conn;
				return $conn;
		}
		function disconnect($idConexion=null){
			if (is_null($idConexion)) {
					$idConexion = $this->idConexion;
			}
			mysqli_close($idConexion);
		}

		function setCharset($chst,$idConexion){
			if (!mysqli_set_charset($idConexion, $chst)) {
				$this->setError(mysqli_error($idConexion));
			}
		}

		function getNumberRows($mResult){ //NUM ROWS DE UN SELECT
			if(!$mResult) return 0;
			else return mysqli_num_rows($mResult);
		}

		function getNumRows(){
			return $this->num_rows;
		}

		function getFila($mResult){ //GET FILA
				return mysqli_fetch_assoc($mResult);
		}
		function executeQuery($pQuery,$idConexion){//EJECUTAR QUERY
				return @mysqli_query($idConexion,$pQuery);
		}

		function getAffectedRows($idConexion){ //NUM ROWS DE UN INSERT,UPDATE,DELETE
			//echo "getAffectedRows:::---".$idConexion."----<br>";
			if(!$idConexion) return 0;
			else{ return mysqli_affected_rows($idConexion);}//$idConexion
		}

		function getLastError($idConexion){
//echo "getLastError:::".$this->idConexion."---".$idConexion."<br>";
			 if(!$idConexion) return false;
			 else{ return mysqli_error($idConexion);}//$idConexion
		}

		function get_id_conexion(){
			return $this->idConexion;
		}

		function getLastIdInserted($idConexion){
			if(!$idConexion) return 0;
			return mysqli_insert_id($idConexion);
		}

}
?>