<?php

@session_start();

/**
 * Clase que gestionará todo lo relacionado con el pixel: administrador y lanzamiento de éste
 *
 * @author luisfernandoparra
 */
class pixel extends CBBDD {

//put your code here
    /**
     * Objeto de conexión de la bbdd
     * @var Object BBDD 
     */
    private $conexion;
    /**
     * variables de conexión y configuración locales
     */

    /** BBDD * */
    private $db_type;
    private $db_host;
    private $db_user;
    private $db_pass;
    private $db_name;

    /** tablas * */
    private $table_pixels_pages;
    private $table_pixels_pages_dynamic_parameters;
    private $table_pixels_dynamic_parameters;
    private $table_pixels_management;
    private $table_pixels_management_pages;
    private $table_pixels_output_format;

    /**
     * variable que dirá si está debuggeando o no
     * @var boolean
     */
    private $dBug = false;

    /**
     * Array con los valores o tags que estarán como inputs
     * @var array 
     */
    private $arr_pix_vars_input = array();

    /**
     * Array con los valores o tags que estarán como outputs (evals)
     * @var array 
     */
    private $arr_pix_vars_output = array();

    /**
     * Prefijo del tag. 
     * @var String
     */
    private $tag_prefix = '';

    /**
     * Sufijo del tag
     * @var string
     */
    private $tag_sufix = '';

    /**
     * nombre de la página donde se ejecuta
     * @var string
     */
    private $script_name;

    /**
     * url de la página que se ejecuta
     * @var string
     */
    private $script_url;

    /**
     * document root de la página
     * @var string
     */
    private $document_root;

    /**
     * Identificador del source o fuente
     * @var integer
     */
    private $id_source = '';

    /**
     * identicador del sorteo
     * @var integer
     */
    private $id_crea = '';

    /**
     *  Identificador de la página
     * @var integer
     */
    private $id_page = 0;

    /**
     * Si el lanzamiento del pixel depende de la política de cookies. si es 1 dependerá.
     * @var integer(boolean)
     */
    private $cookie_policy = 0;
    /**
     * 2014.08.20
     * Variable específica para el entorno de Sanitas-Nectar etc donde los leads son de distinto tipo
     * @var String 
     */
    private $environment_add_info = '';

    /**
     * Constante que define el tag incluido en la tabla pixels_output_format que define el formato de devolución del pixel
     */
    const tag_outputformat = '[%%pixeloutputformat%%]';
    
    /** 
     * Array con gestión especia.
     * id_pixel=>array('entrada'=>'salidanoscript','otromas'=>'salida')
     * @var Array Array que gestionará las entradas salidas
     */
    private $array_special_management = array(8=>array('<noscript>'=>'[NOSCRIPT]','</noscript>'=>'[/NOSCRIPT]'));
    /**
     * Función que retorna simplemente el pixel
     * @param string $pixel pixel de entrada
     * @param type $output
     * @return string
     */

    private function __return_format($pixel, $output) {
        return str_replace(self::tag_outputformat, $pixel, $output);
    }

    /**
     * Función que cogerá el script name de la página y asignará el id_page a su valor, si existe
     */
    private function __getPageId() {
        $sql = 'select id_pix_page from %s where script_name =  \'%s\'';
        $sql = sprintf($sql, $this->table_pixels_pages, $this->script_name);
        $this->conexion->getResultSelectArray($sql);
        $arr = $this->conexion->tResultadoQuery;
        if (count($arr) > 0) {
            $this->id_page = $arr[0]['id_pix_page'];
        }
        $this->__debug($arr, '__getPageId');
        $this->__debug($sql, '__getPageId_sql');
    }

    /**
     * Funcón que recoge el nombre de la página php
     */
    private function __getPageName() {
        $this->script_name = strtolower(trim($_SERVER['SCRIPT_NAME']));
        $this->script_url = strtolower(trim(@$_SERVER['REDIRECT_URL']));
        $this->document_root = strtolower(trim(@$_SERVER['DOCUMENT_ROOT']));
        $this->__debug('script_name:'.$this->script_name . " -- script_url:" . $this->script_url . " -- document_root:" . $this->document_root, '_getPageName');
        
        
        $this->__getPageId();
        /* $root = strtolower(trim($_SERVER['SCRIPT_FILENAME']));
          $tmp = explode('/',$root);
          $this->script_name = end($tmp);
          unset($tmp);
         * 
         */
    }

    /**
     * Función que devolverá en un array las variables y su conversión. Realizamos la query según la página
     * @param boolean $searchbyscript Si los datos los coge desde la página de script o forzando la página
     */
    private function __getPixelsVars($searchbyscript = true) {
        //Solo lo e jecutamos si el array de input está vacío... es decir, si está vacío es que no hay o no se ha ejecutado.
        if (empty($this->arr_pix_vars_input)) {
            $sql = 'SELECT pages.id_pix_page as id,params.tag, params.php_code FROM %s pages';
            $sql.= ' INNER JOIN %s pagesparam ON pages.id_pix_page = pagesparam.id_pix_page';
            $sql.= ' INNER JOIN %s params ON pagesparam.id_pix_parameter = params.id_pix_parameter';
            $sql = sprintf($sql, $this->table_pixels_pages, $this->table_pixels_pages_dynamic_parameters, $this->table_pixels_dynamic_parameters);
            if ($searchbyscript) {

                $sql.= ' WHERE pages.script_name =  \'%s\'';
                $sql = sprintf($sql, $this->script_name);
            } else {
                $sql.= ' WHERE pages.id_pix_page =  \'%d\'';
                $sql = sprintf($sql, $this->id_page);
            }

            $this->conexion->getResultSelectArray($sql);
            $arr = $this->conexion->tResultadoQuery;
            $this->__debug($arr, '__getPixelsVars');
            $this->__debug($sql, '__getPixelsVars_sql');
            if (count($arr) > 0 && !$searchbyscript) {
                $this->id_page = $arr[0]['id'];
            }
            foreach ($arr as $key => $dat) {
                $this->arr_pix_vars_input[] = $this->tag_prefix . trim($dat['tag']) . $this->tag_sufix;
                $tmp = $this->__normalize_pix_phpcode(trim($dat['php_code']));

                $this->arr_pix_vars_output[] = eval('return ' . $tmp . ';');
            }
            
        }
        //new dBug($this->arr_pix_vars_output);
        //new dBug($this->arr_pix_vars_input);
        //$this->arr_pix_vars_output = array_map(array($this,'__normalize_pix_phpcode'),$this->arr_pix_vars_output);
    }

    /**
     * Reemplaza los tags por su correspondiente valor. 
     * Modificado 2014.08.07. Tenemos en cuenta si el pixel hay que reemplazar cosas especiales
     * @param String $datum pixel donde se cambiará el tag por su valor
     * @param Integer $pixoutputformat identificador de la tabla pixels_output_format
     * @return String
     */
    private function __replace_tag_for_value($datum,$pixoutputformat) {
        if (array_key_exists($pixoutputformat,$this->array_special_management)) {
            $arr_tmp = $this->array_special_management[$pixoutputformat];
            foreach ($arr_tmp as $key => $value) {
                $datum = str_replace($key, $value, $datum);
            }
        }
		$this->__debug($this->arr_pix_vars_input);
		$this->__debug($this->arr_pix_vars_output);
		
        return str_replace($this->arr_pix_vars_input, $this->arr_pix_vars_output, $datum);
    }

    /**
     * Normalizar la entrada. Ahora mismo solo gestiona variables normales $variable o $_SESSION['SDFJJ']
     * @param string $datum valor a normalizar
     */
    private function __normalize_pix_phpcode($datum) {
        if (strpos(strtoupper($datum), '_SESSION') !== false) {

            return $datum;
        } else {
            $datum_mid = str_replace('$', '', $datum);
            $dat_init = str_replace($datum, '$GLOBALS[\'', $datum);
            return $dat_init . $datum_mid . "']";
        }
    }

    /**
     * Función pública que devolverá un array con los píxeles a poner
     * @param integer $id_crea crea o sorteo o identificador principal que gestiona el pixel
     * @return array Array con el listado de pixeles que se lanzarán
     */
    public function get_pixels($id_crea, $id_pagina = 0) {
        $output = array();
        $this->id_crea = $id_crea;
        if ($id_pagina > 0 || $this->id_page > 0) {
            $this->id_page = ($id_pagina > 0) ? $id_pagina : $this->id_page;
            $this->__getPixelsVars(false);
        } else
            $this->__getPixelsVars();
        //echo $this->id_page . " -- " . $this->id_crea;
        if ($this->id_page > 0 && $this->id_crea > 0) {
            //Sacamos los pixeles asociados
            $sql = 'SELECT distinct pix.pixel_code, (pix.b_cookie_policy + 0) as cookie_policy, (pix.b_dynamic_parameters + 0) as dynamic_parameters, outformat.output_code as out_format,'
                    . 'pix.id_pix_outputformat FROM %s pix ';
            $sql .= ' INNER JOIN %s pag ON pix.id_pixel = pag.id_pixel AND pag.id_crea =%d and pag.id_pix_page=%d';



            if ($this->id_source)
                $sql .= ' AND pag.id_source=' . $this->id_source;
            else
                $sql .= ' AND (pag.id_source="" or pag.id_source is null)';
            //añadimos el inner join con el formato de salida: pixels_output_format
            $sql .= ' INNER JOIN %s outformat ON pix.id_pix_outputformat=outformat.id_pix_outputformat';
            $sql .= ' WHERE pix.b_active =1';
            if ($this->cookie_policy > 0)
                $sql .= ' AND pix.b_cookie_policy=b\'1\'';
            else
                $sql .= ' AND pix.b_cookie_policy=b\'0\'';
            //if ($this->environment_add_info == '')
                ' AND pix.environment_add_info="'.trim($this->environment_add_info).'"';
            $sql = sprintf($sql, $this->table_pixels_management, $this->table_pixels_management_pages, $this->id_crea, $this->id_page, $this->table_pixels_output_format);
            //die($sql);
            
            $this->conexion->getResultSelectArray($sql);
            $arr = $this->conexion->tResultadoQuery;
            $this->__debug($arr, 'get_pixels');
            $this->__debug($sql, 'get_pixels_sql');
            //die('bye');
            foreach ($arr as $key => $datum) {
                $id_outputformat = $datum['id_pix_outputformat'];
                $code = $datum['pixel_code'];
                //if ($datum['dynamic_parameters'] == 1) {
                $code = $this->__replace_tag_for_value($code,$id_outputformat);
                //}
				$this->__debug($code,'CÓDIGO DE LO OTRO');
                $outputformat = $datum['out_format'];
                //echo "cópdigo: " . $code;
                $output[] = $this->__return_format($code, $outputformat);
            }
            unset($arr);
        }
        //new dBug($output);
        return $output;
    }

    /*     * ************* */
    /* CONSTRUCTORS   */

    public function set_Id_source($id_source) {
        $this->id_source = $id_source;
    }

    public function set_Id_crea($id_crea) {
        $this->id_crea = $id_crea;
    }

    public function set_Cookie_policy($cookie_policy) {
        $this->cookie_policy = $cookie_policy;
    }

    public function set_default_return_format($default_return_format) {
        $this->default_return_format = $default_return_format;
    }

    public function set_tag_prefix($tag_prefix) {
        $this->tag_prefix = $tag_prefix;
    }

    public function set_tag_sufix($tag_sufix) {
        $this->tag_sufix = $tag_sufix;
    }
    
    public function set_id_page($id_page) {
        $this->id_page = $id_page;
    }
    
    public function set_environment_add_info($environment_add_info) {
        $this->environment_add_info = $environment_add_info;
    }

    
    
    /**
     * Función para hacer debug
     * @param undefined $debug objeto a debuggear
     * @param string $comment Comentario adicional a poner.
     */
    private function __debug($debug, $comment = '') {
        if ($this->dBug) {
            if ($comment != '') {
                echo '<h1>' . $comment . '</h1>';
            }
            if (is_array($debug) || is_object($debug)) {
                new dBug($debug);
            } else {
                echo '<br>' . $debug;
            }
        }
    }

    /**
     * Función que inicializará o será de configuración de las variables globales a gestionar
     * para otros módulos, cambiar los datos o inicializarlos aquí
     */
    private function __config() {
        $this->db_host = $GLOBALS['db_host'];
        $this->db_name = $GLOBALS['db_name'];
        $this->db_pass = $GLOBALS['db_pass'];
        $this->db_type = $GLOBALS['db_type'];
        $this->db_user = $GLOBALS['db_user'];

        $this->table_pixels_dynamic_parameters = $GLOBALS['table_pixels_dynamic_parameters'];
        $this->table_pixels_pages = $GLOBALS['table_pixels_pages'];
        $this->table_pixels_pages_dynamic_parameters = $GLOBALS['table_pixels_pages_dynamic_parameters'];
        $this->table_pixels_management = $GLOBALS['table_pixels_management'];
        $this->table_pixels_management_pages = $GLOBALS['table_pixels_management_pages'];
        $this->table_pixels_output_format = $GLOBALS['table_pixels_output_format'];
    }

    /**
     * Variable ue dirá si debuggeamos o que
     * @param boolean $dBug Si estamos en desarroollo /debuggeando
     */
    function __construct($dBug = false) {
        // die('del construct '.TABLE_PIXELS_PAGES." del otro ".self::table_pixels_pages);
        $this->dBug = $dBug;
        $this->__config();

        $this->conexion = new CBBDD($this->db_type, $this->db_host, $this->db_user, $this->db_pass, $this->db_name);
        $this->conexion->connectDB();
        //$this->cur_conn_id = $this->conexion->get_id_conexion();
        //  $this->conexion = $conexion;

        $this->__getPageName();
    }

    function __destruct() {
        if ($this->conexion)
            $this->conexion->disconnectDB();
        unset($this->conexion);
        unset($this->arr_pix_vars_input);
        unset($this->arr_pix_vars_output);
    }

}
