<?php
/**
 * CLASE "LOCAL" DEL MODULO PARA EL API EMAIL PING-CHECK
 * 
 * 2014.11.18 M.F.
 * v.1.0.0
 */

class EmailCheck{
	public $currIp;
	public $fieldsWs;
	public $currEmail;
	public $id_client;

	function __construct($sitesPruebas=0,$currPath='')
	{
		$this->currIp;
		$this->path_raiz_includes='';
		$this->sitesPruebas=$sitesPruebas;
		$this->currEmail=null;
		$this->id_client=null;
		$this->serverWsUrl=$this->sitesPruebas ? 'http://139.162.246.12/stat-server/webservices/ping_server/ws_email_check.php' : 'http://212.71.235.216/webservices/ping_server/ws_email_check.php';	// PRUEBAS : REAL
	}


	/**
	 * RETURN CURRENT CLIENT IP ADDRESS
	 * 
	 * @return string
	 */
	private function getRealIpAddr()
	{
		if(!empty($_SERVER['HTTP_CLIENT_IP']))	//check ip from share internet
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		else
			$ip=$_SERVER['REMOTE_ADDR'];
		return $ip;
	}


	/**
	 * EJECUTAR WS PUENTE PARA COMPROBACION E-MAIL
	 * 
	 * @param varchar $secret_pwd
	 * @return string
	 */
	public function execBridgeLocalWs($secret_pwd=null)
	{
		$userAccess=md5(date('Y-m-d H:i:s'));
		$passwordAccess=md5($userAccess.$secret_pwd);
		$timeout=10;

		$this->fieldsWs['email']=$this->currEmail;
//		$this->fieldsWs['currIp']=$this->getRealIpAddr();	// RETRIEVE CURRENT CLIENT IP - SIRVE ??
		$this->fieldsWs['usr']=$userAccess;
		$this->fieldsWs['pwd']=$passwordAccess;
		$this->fieldsWs['id_client']=$this->id_client;
		$ws_fields=http_build_query($this->fieldsWs);

		$ch=curl_init();
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1');
		curl_setopt($ch,CURLOPT_URL,$this->serverWsUrl);
		curl_setopt($ch,CURLOPT_POST,count($this->fieldsWs));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $ws_fields);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		curl_setopt($ch,CURLOPT_HEADER,0);
		$res=curl_exec($ch);
		curl_close($ch);

		$newlines=array("\t","\n","\r","\x20\x20","\0","\x0B");
		$resClean=str_replace($newlines,'',html_entity_decode($res));
		return $resClean;
	}


	/**
	 * CLASS DESTRUCTOR
	 */
	public function __EmailCheck()
	{
		unset($this);
	}

}
