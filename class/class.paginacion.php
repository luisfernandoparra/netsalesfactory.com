<?php

class paginacion
{

    //********************************************************************
    // Variables de la clase
    //********************************************************************

    var $tam_pag; //numero maximo de registros por pagina
    var $total_registros;  //total de registros de la consulta
    var $num_paginacion; //numero maximo de links por pagina
    var $pag_act;  //pagina actual en la que me encuentro dentro de la paginacion
    var $pag_inicial; //primer link que se muestra en la paginacion
    var $anterior; //pagina anterior
    var $posterior; //pagina posterior;
    var $total_paginas_a_mostrar; //Numero total de paginas existentes
    var $num_paginas_a_mostrar;//Numero de paginas a mostrar
    var $registro_inicio;//Registro de inicio
    var $cabeceras;//Array con los campos de la consulta
    var $recordset;//array con el resultado de la cosulta
    var $ordenado;//indica por que campo de la consulta estamos ordenando
    var $est_cabecera;//estilo de las cabeceras
    var $est_registro;//estilo de los registros
    var $est_paginacion;//estilo de la paginacion

    
    //********************************************************************
    // Constructor de la clase
    //********************************************************************
        function paginacion($tam_pag,$total_registros,$num_paginacion,$pag_act,$array_cabeceras,$array_recordset,$ordenado_por,$est_cabecera,$est_registro,$est_paginacion){
                 $this->tam_pag = $tam_pag;
                 $this->total_registros = $total_registros;
                 $this->num_paginacion = $num_paginacion;
                 $this->pag_act = $pag_act;
                 $this->pag_inicial =1;
                 $this->anterior = $this->pag_act-1; //link a la pagina anterior;
                 $this->posterior = $this->pag_act+1; //link a la pagina posterior;
                 $this->total_paginas_a_mostrar = ceil($this->total_registros / $this->tam_pag);
                 $this->num_paginas_a_mostrar= $this->num_paginacion;
                 $this->registro_inicio = ($this->pag_act -1)* $this->tam_pag;
                 $this->cabeceras = $array_cabeceras;
                 $this->recordset = $array_recordset;
                 $this->ordenado = $ordenado_por;
                 $this->est_cabecera = $est_cabecera;
                 $this->est_registro = $est_registro;
                 $this->est_paginacion = $est_paginacion;
        }
    
    //********************************************************************
    // Funcion que pinta las cabeceras(nombres de los campos)
    //********************************************************************
        function mostrar_cabeceras(){
                 //echo "<tr>";
                 for($j=0;$j<count($this->cabeceras);$j++){

                     echo "<td><a class='".$this->est_cabecera."' href='$PHP_SELF?ordenacion=".$this->cabeceras[$j]."'>".$this->cabeceras[$j]."</a></td>";
                     //echo "<td>".$this->cabeceras[$j]."</td>";
                 }
                 //echo "</tr>";
       }

    //***********************************************************************************
    // Funcion que presenta los registros (valores de los campos)
    //***********************************************************************************
      function mostrar_registros(){
          for($i=$this->registro_inicio;$i<min($this->registro_inicio + $this->tam_pag,$this->total_registros);$i++){
              echo "<tr>";
              echo $this->mostrar_registro_actual($i);
              echo "</tr>";
          }
      }
    
	//***********************************************************************************
    // Funcion que presenta el registro actual (valores de los campos)
    //***********************************************************************************
      function mostrar_registro_actual($actual){     
			for($j=0;$j<count($this->cabeceras);$j++){
                    $cadena="<td class='".$this->est_registro."'>".$this->recordset[$actual][$this->cabeceras[$j]]."</td>";
              }
			 return $cadena; 
	  }
	  		  
    //********************************************************************
    // Funcion que pinta los links de la paginacion
    //********************************************************************
      function mostrar_paginacion (){

        //NUM REG INICIAL QUE MOSTRAMOS

        if($this->total_paginas_a_mostrar > $this->num_paginacion){
               //$num_paginas_a_mostrar= $this->num_paginacion;
               $mitad = ceil($this->num_paginacion / 2);
               //echo "MITAD:$mitad<br>";
               //RECALCULAMOS EL NUMERO MAXIMO DE LINKS EN CASO DE QUE SEA MENOR QUE EL NUMERO TOTAL DE PAGINAS A MOSTRAR

               if($this->pag_act > $mitad){ //si pincho en un link mayor a la mitad muevo el indice inicial y final de los links a presentar
                      //manteniendo el numero maximo de links a presentar
                      

                      $mover = $this->pag_act - $mitad; //contador que voy a mover
                      $this->pag_inicial = $this->pag_inicial + $mover;  //pagina inicial ahora
                      
                      if($this->total_paginas_a_mostrar - $this->pag_inicial >= $this->num_paginacion) //hay mas de $num_paginacion links por mostrar
                      {

                             //echo "ENTRO1<br>";
                             $this->num_paginas_a_mostrar = $this->num_paginas_a_mostrar + $mover; //pagina final ahora
                      }
                      else{
                           $this->num_paginas_a_mostrar =  $this->total_paginas_a_mostrar; //pagina final ahora
                           //echo "ENTRO2<br>";
                      }
                      //echo "<script>alert('INICIO".($this->pag_inicial)."');</script>";
                      //echo "<script>alert('FIN".($this->num_paginas_a_mostrar)."');</script>";
                      //echo "<script>alert('TOTAL".($this->total_paginas_a_mostrar)."');</script>";

               }
        }
        else{
            $this->num_paginas_a_mostrar = $this->total_paginas_a_mostrar;//link maximo a mostrar
        }
        //Presentacion de los elementos y de  la paginacion
        //$this->mostrar_registros();
        $this->pag_anterior();
        $this->numeros_paginacion();
        $this->pag_posterior();
      }
    
    //********************************************************************
    // Funcion que pinta el link a la pagina anterior
    //********************************************************************
      function pag_anterior(){
           //1)***Mostrado de Anterior
           if(($this->tam_pag < $this->total_registros) && $this->pag_act!=1){
                    echo "<a class='".$this->est_paginacion."' href='$PHP_SELF?ordenacion=".$this->ordenado."&pag_actual=".$this->anterior."'><b><span class='link'>&lt;&lt;&nbsp;</span></b></a>";
           }
      }
    
    //********************************************************************
    // Funcion que pinta los numeros de la paginacion
    //********************************************************************

      function numeros_paginacion(){
        //2)***Mostrado de la paginacion numeradas por paginas

        $k=$this->pag_inicial;
        for($i=0;$i<=$this->total_registros; $i=$i+$this->tam_pag)
		{
                 //echo "K:$k<br>";
                //MOstramos paginacion solo si el numero de registros a mostrar
                //por pagina es menor que el numero de registros totales
                if($this->tam_pag < $this->total_registros){
                    if($k <= $this->num_paginas_a_mostrar){
                        if($k==$this->pag_act){
                            $link="<b class='".$this->est_paginacion."'>$k</b></b>";
                        }
                        else{
                            $link="<a class='".$this->est_paginacion."' href='$PHP_SELF?ordenacion=".$this->ordenado."&pag_actual=$k'><span class='link'>$k</span></a>";
                        }
                        echo "&nbsp;&nbsp;$link";
                    }
                }
                $k++;
		}//end for
      }
    
    //********************************************************************
    // Funcion que pinta el link a la pagina posterior
    //********************************************************************

      function pag_posterior(){
            //3)***Mostrado de posterior
            if($this->tam_pag < $this->total_registros){
               if($this->total_paginas_a_mostrar < $this->num_paginacion){
                   if($this->total_paginas_a_mostrar!=$this->pag_act){
                       echo "<a class='".$this->est_paginacion."' href='$PHP_SELF?ordenacion=".$this->ordenado."&pag_actual=".$this->posterior."'><b><span class='link'>&nbsp;&gt;&gt;</span></b></a>";
                   }
               }
               else{
                   if($this->num_paginas_a_mostrar < $this->total_paginas_a_mostrar){
                   echo "<a class='".$this->est_paginacion."' href='$PHP_SELF?ordenacion=".$this->ordenado."&pag_actual=".$this->posterior."'><b>&gt;&gt;</b></a>";
                   }
               }

            }
       }
    
    //********************************************************************
    // Funcion que pinta el numero de pagina en el que me encuentro
    //********************************************************************
      function mostrar_pagina_actual(){
            return "PAGINA ".$this->pag_act." DE ".$this->total_paginas_a_mostrar;
      }
    
    //********************************************************************
    // Funcion que pinta el rango de registros actuales dentro de la paginacion
    //********************************************************************
      function mostrar_reg_actual(){
          if($this->total_registros > ($this->registro_inicio + $this->tam_pag)){
              if($this->pag_act==1){
                $inicio=1;
              }
              else{
                $inicio = $this->registro_inicio;
              }
              echo "REGISTROS DESDE ". $inicio." A ".($this->registro_inicio + $this->tam_pag)."<br>";
          }
          else{
              echo "REGISTROS DESDE ".$this->registro_inicio." A ".$this->total_registros."<br>";
          }
        return;
      }
    
	  //********************************************************************
      // Funcion que recupera el registro  desde el que empieza el listado
      //********************************************************************

      function get_reg_inicio(){
	  	return $this->registro_inicio;
	  }
    	
	  //********************************************************************
      // Funcion que recupera numero maximo de registros por pagina
      //********************************************************************

      function get_reg_x_pagina(){
	  	return $this->tam_pag;
	  }
	  
	  //********************************************************************
      // Funcion que recupera el total de registros de la query
      //********************************************************************

      function get_total_registros(){
	  	return $this->total_registros;
	  }
	  	
	  
    
}//end class

?>




