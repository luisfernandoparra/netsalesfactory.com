<?php

/**
 * PHP que contendrá funciones comunes al entorno
 */
function validateTelefono($telefono) {
    return ($telefono != '' && is_numeric($telefono) && ($telefono[0] >= 6 && $telefono[0] <= 9) && strlen($telefono) == 9);
}

function getRealIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}