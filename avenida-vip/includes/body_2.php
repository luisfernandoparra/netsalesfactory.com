<?php
// START EXCEPCION PARA LA IMAGEN DE LA CABECERA PARA UNA FECHA Y UNAS DETERMINADAS HORAS
$cssclassSupplementalTitle='';
$arraDayException=array(4=>'_xcptn',7=>'_xcptn');
$rangeTimeException='';
$rangeTimeException=(date('H') >= 10 && date('H') <= 13) ? 'X' : '';  // parte final del nombre de la clase para estar activada desde las 10 a las 14 horas
		
if(array_key_exists($id_crea,$arraDayException))
{
//  $cssclassSupplementalTitle=' _'.DATE('Ymd').$rangeTimeException;
}
// END EXCEPCION PARA LA IMAGEN DE LA CABECERA PARA UNA FECHA Y UNAS DETERMINADAS HORAS

$cr = $_REQUEST ['cr'];
  
  if ($cr == 7){
	$padd=190;
  }else{
	$padd=340;
  }
?>

<div class="mainContent" style="display:block;position:relative;padding-bottom:<?php echo $padd; ?>px;">
<div id="contenedor_centrado">
 
 <div class="titulo <?=$cssclassSupplementalTitle?>"></div>
 
 <div class="form-contenido">
     <div class="formulario">
        
               
            <div class="contenedor_form">
                <p><strong>Solicita información GRATIS</strong></p>
                <p><strong>Te llamamos nosotros</strong></p>
                <form action="" method="get">
                    <div class="fila">
                        <div class="fleft">Nombre*<input type="text" class="celda" id="nombre" name="nombre" maxlength="499" value="<?php echo $nombre; ?>" /></div>
                    </div>
                    <div class="fila">
                        <div class="fleft">Teléfono* <input type="text" class="celda" id="telefono" name="telefono" maxlength="9"  /></div>
                    </div>
                    <div class="fila">
                        <div class="fleft">Provincia* <select class="celda" id="sel_prov" name="sel_prov" >
                                        <option value=""> </option>
                            <?php 
                                                    foreach ($arr_prov as $key=>$value) {
                                                            $selected = "";
                                                            $valor = strtolower(trim($value['name']));
                                                            $valor = str_replace($arr_input,$arr_output,$valor);
                                                            if ($valor==$provincia) {
                                                                    $selected = " selected ";	
                                                            }
                                                            echo "<option value='".$value['id']."'".$selected.">".$value['name']."</option>";
                                                    }
                                            ?>
                        </select></div>
                    </div>
                    <div class="fila">
                        <div class="fleft">E-mail* <input type="text" class="celda" name="email" id="email" value="<?php echo $email; ?>" /></div>
                    </div>
              </form>   
                 <div class="legal">
                   <input type="checkbox" id="cblegales" value="1" name="cblegales" />  He leído y acepto la <a href="#null" onClick="MM_openBrWindow('<?php echo $url_protecciondatos; ?>','','scrollbars=yes,width=600,height=720'); return false;">política de privacidad</a>
                 </div>  
                 <input name="btnProcesar" id="btnProcesar" value="Solicitar información >" class="btn_solicitar" type="button" />
            </div><!-- contenedor_form-->
          </div>
          
   </div>  <!-- contenedor_form contenido-->   
    <div class="sello"></div>

   <div class="coberturas_servicios clearfix <?=$layoutSorteo?>" >
        
            <div class="coberturas">
            
			  <ul>
				  <h1>Coberturas</h1>
				  <li>Odontología preventiva.</li>
				  <li>Intervenciones quirúrgicas.</li>
				  <li>Odontología conservadora.</li>
				  <li>Endodoncia.</li>
				  <li>Odontología estética.</li>
				  <li>Odontopediatría.</li>
				  <li>Prótesis.</li>
				  <li>Periodoncia.</li>
				  <li>Ortodoncia.</li>
				  <li>Implantología.</li>
				  <li>Diagnóstico por imagen.</li>
				  <li>Patología Articulación Temporomandibular.</li>
			  </ul>
			  <a class="pdfCondiciones" href="<?=$url_local?>/sanitas-new-web/get_pdf.php" target="_blank"><img src="<?=$url_local?>/sanitas-new-web/img/p.gif" border="0" width="225" height="45" /></a>
			</div> 
            
            <div class="servicios">
                <ul>
                    <h1>Servicios incluidos</h1>
                    
                    <li>Consulta odontológica: exploración y diagnóstico.</li>
                    <li>Limpieza bucal</li>
                    <li>Cirugía</li>
                    <li>Extracción simple, pieza incluida.</li>
                    <li>Estudio Radiológico</li>
                    <li>Estudio Implantológico</li>
                    <li>Ortodoncia: consulta, estudio completo, extracción simple y protector bucal.</li>
                    <li>Diagnóstico por imagen: Cefalometría, Tomografía, Ortopantomografía</li>
                    <li>Férula para blanqueamiento de fotoactivación.</li>
               </ul>
               <br />
            </div>
            
          <a class="enlace_condiciones" href="#" target="_blank" onClick="MM_openBrWindow('<?php echo $url_condiciones; ?>','','scrollbars=yes,width=1060,height=720'); return false;">Ver las condiciones generales de Sanitas</a>

        <div class="pie">
        <br />
		<p>www.sanitas.es | © Todos los derechos reservados</p>
        </div>
            
    </div>
    
 </div>
 </div>
<?php
?>