<?php
/**
 * Body para avenida vip francés
 */
?>
<div class="mainContent" style="display:block;position:relative;padding-bottom:<?php echo @$padd; ?>px;">
    <div id="contenedor_centrado">
        <section class="contenido">
            <section class="grupo">
                <header>

                    <figure class="cabecera1"><img src="<?= $path_raiz_aplicacion_local ?>img/<?php echo $img_header; ?>"></figure>
                    <figure class="cabecera2"><img src="<?= $path_raiz_aplicacion_local ?>img/<?php echo $img_header_mobile; ?>"></figure>

                </header>
                <div class="texto">S&eacute;lectionnez vos pr&eacute;ferences et inscrivez-vous </div>

                <div class="fotos cookiesEnable">
                    <a id="salu" href="#0"><img class="selectetable _salu" src="<?= $path_imgs_preferences ?>moto.jpg" border="0"><h3>Moto et Voiture</h3></a>
                    <a id="moda" href="#0"><img class="selectetable _moda" src="<?= $path_imgs_preferences ?>salu.jpg" border="0"><h3>Beaut&eacute;</h3></a>
                    <a id="viaj" href="#0"><img class="selectetable _viaj" src="<?= $path_imgs_preferences ?>tenis.jpg" border="0"><h3>Sport</h3></a>
                    <a id="form" href="#0"><img class="selectetable _form" src="<?= $path_imgs_preferences ?>segu.jpg" border="0"><h3>Famille</h3></a>
                    <a id="banc" href="#0"><img class="selectetable _banc" src="<?= $path_imgs_preferences ?>ener.jpg" border="0"><h3>Restaurants</h3></a>
                    <a id="moto" href="#0"><img class="selectetable _moto" src="<?= $path_imgs_preferences ?>moda.jpg" border="0"><h3>Mode</h3></a>
                    <a id="tele" href="#0"><img class="selectetable _tele" src="<?= $path_imgs_preferences ?>viaj.jpg" border="0"><h3>Voyage</h3></a>
                    <a id="segu" href="#0"><img class="selectetable _segu" src="<?= $path_imgs_preferences ?>tele.jpg" border="0"><h3>Hi tech</h3></a>
                    <a id="ener" href="#0"><img class="selectetable _ener" src="<?= $path_imgs_preferences ?>banc.jpg" border="0"><h3>Shopping</h3></a>
                </div>

                <div class="formulario">
                    <h1>Validez et compl&eacute;tez vos donn&eacute;es: </h1>

                    <form action="" method="get">
                        <input type="hidden" name="data_salu" id="data_salu" value="0">
                        <input type="hidden" name="data_moda" id="data_moda" value="0">
                        <input type="hidden" name="data_viaj" id="data_viaj" value="0">
                        <input type="hidden" name="data_form" id="data_form" value="0">
                        <input type="hidden" name="data_banc" id="data_banc" value="0">
                        <input type="hidden" name="data_moto" id="data_moto" value="0">
                        <input type="hidden" name="data_tele" id="data_tele" value="0">
                        <input type="hidden" name="data_segu" id="data_segu" value="0">
                        <input type="hidden" name="data_ener" id="data_ener" value="0">
                        <div class="fila">
                            <input name="tratamiento" type="radio" value="H" class="cookiesEnable">M.<input name="tratamiento" type="radio" value="M" class="cookiesEnable">Mme.
                        </div>
                        <div class="fila">
                            <div class="fleft">Pr&eacute;nom<input type="text" class="celda" id="nombre" name="nombre" maxlength="499" value="<?= $nombre ?>"></div>
                        </div>
                        <div class="fila">
                            <div class="fleft">Nom de famille <input type="text" class="celda" id="apellidos" name="apellidos" maxlength="499" value="<?= $apellidos ?>"></div>
                        </div>
                        <div class="fila">
                            <div class="fleft">E-mail<input type="text" class="celda" name="em" id="email" value="<?= $email ?>" onblur="this.value = (this.value==&#39;&#39;)?(&#39;Email&#39;):(this.value)" onClick="this.value=(this.value==&#39;Email&#39;)?(&#39;&#39;):(this.value)"></div>
                        </div>
                        <div class="fecha_nacimiento">
                            <div id="fecha_nombre">Date de naissance </div>
                            <div id="capa_fecha">
                                <select name="dia_fech" id="dia_fech" class="f_dia cookiesEnable">
                                    <?php
                                    $outDay = '';
                                    for ($day = 1; $day < 32; $day++) {
                                        $outDay.='<option value="' . str_pad($day, 2, '0', STR_PAD_LEFT) . '"';
                                        $outDay.='';
                                        $outDay.='>' . str_pad($day, 2, '0', STR_PAD_LEFT) . '</option>';
                                    }
                                    echo $outDay;
                                    ?>
                                </select>
                                <select name="mes_fech" id="mes_fech" class="f_mes cookiesEnable">
                                    <option value="01">Janvier</option><option value="02" selected="selected">F&eacute;vrier</option><option value="03">Mars</option><option value="04">Abril</option><option value="05">Mai</option><option value="06">Juin</option><option value="07">Juillet</option><option value="08">Ao&ucirc;t</option><option value="09">Septembre</option><option value="10">Octobre</option><option value="11">Novembre</option><option value="12">D&eacute;cembre</option>	                  		</select>
                                <select name="ano_fech" id="ano_fech" class="f_ano cookiesEnable">
                                    <?php
                                    $outYear = '';
                                    $startYear = date('Y') - 18;
                                    for ($yearX = $startYear; $yearX > 1899; $yearX--) {
                                        $outYear.='<option value="' . $yearX . '"';
                                        $outYear.='';
                                        $outYear.='>' . $yearX . '</option>';
                                    }
                                    echo $outYear;
                                    ?>
                                </select>
                            </div>
                        </div> 
                        <div class="fila">
                            <div class="fleft">Code Postal<input type="text" class="celda" id="cp" name="cp" maxlength="5" value=""></div>
                        </div>

                        <div class="legal">
                            <input type="checkbox" id="cblegales" value="1" name="cblegales"> J&#39;ai lu et j&#39;accepte les <a class="modalMain" href="#0">conditions g&eacute;n&eacute;rales d&#39;utilisation</a>
                                                     <!--<input type="checkbox" id="cblegales" value="1" name="cblegales">   J&#39;ai lu et j&#39;accepte les <a class="enlace_pp" href="politica.html" target="_blank">pconditions g&eacute;n&eacute;rales d&#39;utilisation</a>-->
                        </div>
                        <input name="btnProcesar" id="btnProcesar" value="J'accepte" class="green" type="button">
                    </form>

                </div>
            </section>
        </section>
    </div>
</div>

<footer>

    AVIS JURIDIQUE 
    En conformit&eacute; avec les dispositions de la loi 34/2002 du 11 Juillet, la Soci&eacute;t&eacute; des services dinformation et de commerce &eacute;lectronique, a rapport&eacute; l&#39;information g&eacute;n&eacute;rale de ce site, www.<?php echo $site;?>. Responsable d&#39;une part, le fournisseur des biens ou des services contract&eacute;s par l&#39;utilisateur est <?php echo $disclaimer_field1;?>
    &agrave; travers la marque <?php echo $nombCliente;?>, &eacute;tablie sur <?php echo $disclaimer_field2;?> et adresse e-mail info@<?php echo $site;?>.&nbsp;
</footer>