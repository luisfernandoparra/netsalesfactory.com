<?php
@session_start();
?>  
<p><b>Comment utilisons-nous les cookies?</b>
<p>Nous utilisons les cookies ou d&#39;autres dispositifs de stockage et de r&eacute;cup&eacute;ration pour &eacute;ffectuer un suivi des interractions des usagers.</p>
<p>Les cookies permettent de reconna&iacute;tre le navigateur d&#39;un usager ainsi que le type de dispositif depuis lequel l a acc&eacute;d&eacute; au site web. Ils s&#39;utilisent pour faciliter la visite suivante et rentre l&#39;exploitation du site web plus intuitive et ergonomique.</p>
<p>Quels types de cookies utilisons-nous?</p>
<p>Le site web utilise les formats de cookies suivants:</p>
<p><b>-Cookies de pr&eacute;f&eacute;rence ou de personnalisation: </b>ils garantissent &agrave; l&#39;utilisateur un acc&egrave;s au service selon des caract&eacute;ristiques g&eacute;n&eacute;rales pr&eacute;d&eacute;finies dans le terminal de l&#39;utilisateur (langue, type de navigateur, configuration r&eacute;gionale, etc.)</p>
<p><b>-Cookies techniques:</b> ils permettent &agrave; l&#39;utilisateur de naviguer dans une page web, une plateforme ou une application et d&#39;utiliser les diff&eacute;rentes options et services qu&#39;ils proposent, comme par exemple contrôler l&#39;&eacute;mission des donn&eacute;es, identifier la sesi&oacute;n, acc&eacute;der aux services restreints, r&eacute;aliser un processus d&#39;achat et de commande, solliciter une inscription ou participer &agrave; un &eacute;v&egrave;nement, utiliser les options de s&eacute;curit&eacute; durant la navigation ou partager des contenus par le biais des r&eacute;seaux sociaux.</p>
<p><b>-Cookies de sesi&oacute;n:</b> ils permettent au site web de reconna&icirc;tre les informations accumul&eacute;es durant la session afin d&#39;&eacute;viter que l&#39;utilisateur ait &agrave; envoyer plusieurs fois la m&ecirc;me information, qui ne lui ser&aacute; demand&eacute;e qu&#39;une fois.</p>
<p><b>-Cookies d&#39;analyse:</b> lorsque l&#39;usager acc&egrave;de au site web, ils permettent de collecter des informations de mani&egrave;re standard et anonyme afin de cr&eacute;er un profil de navigation et de comportement sur le net.</p>
<p>Cookies autoris&eacute;s sur le site web par Google Analytics: </p>
<p>"_ga": N&eacute;cessaire au bon fonctionnement de Google Analytics, il a une dur&eacute;e de vie de 2 ans. </p>
<p>Ce cookie est utilis&eacute; pour assurer la compatibilit&eacute; avec les sites o&ugrave; se trouvent encore les anciens codes de suivi de type urchin.js.</p>
<p>Pour obtener plus d&#39;information au sujet de Google Analytics, consulter: http://goo.gl/KxpaZ.</p>
<p><b>Comment d&eacute;sactiver la configuration des cookies:<b></p>
<p>Il est possible de d&eacute;sactiver la configuration des cookies dans tous les navigateurs, qui proposent un syst&egrave;me de gesti&oacute;n des cookies. L&#39;utilisateur exerce ainsi un contr&ocirc;le optimal de ses donn&eacute;es confidentielles.</p>
<p>Ces outils de contr&ocirc;le se trouvent dans “options” ou “pr&eacute;f&eacute;rences” depuis le menu de l&#39;utilisateur. Ci-dessous la proc&eacute;dure de d&eacute;sactivation des cookies sous diff&eacute;rents navigateurs: </p>
<p>Internet Explorer (goo.gl/ksN5y)</p>
<p>Dans le menu Options, s&eacute;lectionner “Options Internet” et cliquer sur l&#39;onglet Confidentialit&eacute;. Appara&icirc;tra un curseur gr&acirc;ce auquel on pourra configurer la quantit&eacute; de cookies autoris&eacute;s: Bloquer tous les cookies, Haute s&eacute;curit&eacute;, S&eacute;curit&eacute; moyenne haute, S&eacute;curit&eacute; moyenne (par d&eacute;faut), Faible s&eacute;curit&eacute; et Accepter tous les cookies.</p>
<p>•	Mozilla Firefox (goo.gl/F5pHX)</p>
<p>Dans le menu Options, s&eacute;lectionner “Options”. S&eacute;lectionner l&#39;&eacute;tiquette de confidentialit&eacute; dans l&#39;onglet options. Dans le menu d&eacute;roulant qui appara&icirc;t, choisir “configuration personnalis&eacute;e pour l&#39;historique”. Appara&icirc;tront les options de configuration des cookies o&ugrave; l&#39;on pourra activer ou d&eacute;sactiver les cookies en cochant les cases correspondantes.</p>
<p>•	Google Chrome (goo.gl/8cAo)</p>
<p>Dans le menu de configuration, s&eacute;lectionner “Montrer Configuration Avanc&eacute;e” dans la partie inf&eacute;rieure de la page. S&eacute;lectionner la touche “configuration du Contenu” dans la section Confidentialit&eacute;. Dans la partie sup&eacute;rieure de la page apparaissent les informations concernant les cookies et en permet la gesti&oacute;n. On y peut aussi effecer tous les cookies qui se trouvent dans le navigateur.</p>
<p>•	Safari (goo.gl/KFBFh)</p>
<p>Dans le menu de configuration, s&eacute;lectionner “Pr&eacute;f&eacute;rences” afin d&#39;ouvrir la fen&ecirc;tre de Confidentialit&eacute;. S&eacute;lectionner l&#39;option d&eacute;sir&eacute;e dans la section “bloquer les cookies”. Attention, certaines fonctions du site necessitent l&#39;usage des cookies et peuvent s&#39;en trouver compromises.</p>
<p>Si vous ne souhaitez pas &ecirc;tre suivis par l&#39;interm&eacute;diaire de cookies, Google propose un compl&eacute;ment &agrave; installer dans votre navigateur. Pour le t&eacute;l&eacute;charger, suivre le lien suivant: goo.gl/up4ND.</p>
<p>Un cookie additionnel vous sera envoy&eacute; afin de sauvegarder et prendre en compte votre configuration de l&#39;utilisation des cookies. Il ne sera donc pas n&eacute;cessaire d&#39;accepter l&#39;utilisation des cookies chaque fois que vous acc&eacute;der au site web.</p>
<p>Cookies sur les dispositifs mobiles: </p>
<p>Nous utilisons aussi les cookies ou d&#39;autres dispositifs de stockage sur les dispositifs mobiles.</p>
<p>De la m&ecirc;me façon que sur un navigateur desktop, les navigateurs des dispositifs mobiles proposent diff&eacute;rentes options de confidentialit&eacute; grâce auxquelles on pourra activer ou d&eacute;sactiver les cookies. Pour modifier les options de confidentialit&eacute;, suivre les instructions correspondantes au navigateur de votre dispositif mobile. Ci-dessous quelques exemples:</p>
  <ul class="ulCookies">
	<li><p>IOS: <a href="http://goo.gl/pRkla" target="_blank">goo.gl/pRkla</a></p></li>
	<li><p>Windows Phone: <a href="http://goo.gl/Rx8QQ" target="_blank">goo.gl/Rx8QQ</a></p></li>
	<li><p>Chrome Mobile: <a href="http://goo.gl/Rx8QQ" target="_blank">goo.gl/Rx8QQ</a></p></li>
	<li><p>Opera Mobile: <a href="http://goo.gl/pRkla" target="_blank">goo.gl/XvmTG</a></p></li>
  </ul>