<?php
@session_start();
// DEFINE COOKIES TEXT VARS 
$cookieCompanyNameFull='utilizamos';
$cookieCompanyNameShort='XXXXXXXXXXXXXXXX';
/*
 * LISTA DE LAS COOKIES UTILIZADAS EN ESTA PAGINA
 */
$arrThirdCookies=array('_ga'=>'Es necesaria para el funcionamiento de Google Analytics y tiene un período de caducidad de 2 años');

?>
<b>Cómo <?=$cookieCompanyNameFull?> las cookies:</b><br /><br />
<p><?=ucfirst($cookieCompanyNameFull)?> las cookies u otros dispositivos de almacenamiento y recuperación de información para realizar un seguimiento de las interacciones de los usuarios.</p>
<p>Las <i>cookies</i> permiten reconocer el navegador de un usuario, así como el tipo de dispositivo desde el que se accede al sitio web, y se utilizan para facilitar la próxima visita del usuario y hacer que el sitio web o las aplicaciones resulten más útiles.<br /><br />

Tipos de cookies que utilizamos:</p>

<p>El sitio web  utiliza los siguientes tipos de cookies:</p>
<ul class="ulCookies">
  <li><p><i>Cookies de preferencias o personalización:</i> Permiten al usuario acceder al servicio con algunas características de carácter general predefinidas en función de una serie de criterios en el terminal del usuario (idioma, tipo de navegador, configuración regional, etc.).</p></li>
  <li><p><i>Cookies técnicas:</i> Son aquéllas que permiten al usuario la navegación a través de una página web, plataforma, o aplicación y la utilización de las diferentes opciones o servicios que en ella existan como, por ejemplo, controlar el tráfico de la comunicación de datos, identificar la sesión, acceder a partes de acceso restringido, realizar el proceso de compra de un pedido, realizar la solicitud de inscripción o participación en un evento, utilizar elementos de seguridad durante la navegación o compartir contenidos a través de redes sociales.</p></li>
  <li><p><i>Cookies de Sesión:</i> Permiten al sitio web reconocer la información almacenada durante la sesión del usuario para evitar que el sitio web realice solicitudes de información ya facilitadas, de forma que no se pedirá la información dada anteriormente.</p></li>
  <li><p><i>Cookies analíticas:</i> Cuando el usuario se introduce en el sitio web, a través de esta herramienta se recopila información anónima de manera estándar sobre la navegación del usuario y sus patrones de comportamiento.</p></li>
</ul><br />

<p>Google Analytics habilita en el dominio del sitio web las cookies denominadas:</p>

<ul class="ulCookies">
<?php
/*
 * 	PAINT THE COOKIES LIST FROM "$arrThirdCookies"
 */
foreach($arrThirdCookies as $cookieName=>$cokkieDesc)
{
  echo '<li><p>"'.$cookieName.'": '.$cokkieDesc.'.</p></li>';
}
?>

</ul>
<p>Esta cookie se sigue escribiendo para asegurar la compatibilidad con las webs donde está instalado el antiguo código de seguimiento urchin.js.</p>

<p>Para obtener más información acerca de Google Analytics puede dirigirse al siguiente enlace: <a href="http://goo.gl/KxpaZ" target="_blank">http://goo.gl/KxpaZ</a>.</p>
<br />
<b>Cómo desactivar la configuración de las cookies:</b>

<p>Todos los navegadores permiten hacer cambios para desactivar la configuración de las cookies. Este es el motivo por el que la mayoría de navegadores ofrecen la posibilidad de administrar las cookies, para obtener un control más preciso sobre la privacidad.</p>

<p>Estos ajustes se encuentran ubicados en las "opciones" o "preferencias" del menú de su navegador. A continuación podrá encontrar los links de cada navegador para deshabilitar las cookies siguiendo las instrucciones:</p>
<ul class="ulCookies">
<li>Internet Explorer (<a href="http://goo.gl/ksN5y" target="_blank">goo.gl/ksN5y</a>)</li>
<p>En el menú de herramientas, seleccione "Opciones de Internet". Haga clic en la pestaña de privacidad. Verá un cursor de desplazamiento para configurar la privacidad que tiene seis posiciones que le permite controlar la cantidad de cookies que se instalarán: Bloquear todas las cookies, Alta, Media Alto, Media (nivel por defecto), Baja, y Aceptar todas las cookies.</p>

<li>Mozilla Firefox (<a href="http://goo.gl/F5pHX" target="_blank">goo.gl/F5pHX</a>)</li>
<p>En el menú de herramientas, seleccione "opciones". Seleccione la etiqueta de privacidad en el recuadro de opciones. Del menú desplegable elija "usar configuración personalizada para el historial". Esto mostrará las opciones de cookies y podrá optar por activarlas o desactivarlas marcando la casilla correspondiente.</p>

<li>Google Chrome (<a href="http://goo.gl/8cAo" target="_blank">goo.gl/8cAo</a>)</li>
<p>En el menú de configuración, seleccione "mostrar configuración avanzada" en la parte inferior de la página. Seleccione la tecla de "configuración de contenido" en la sección de privacidad.
La sección de la parte superior de la página que aparece le da información sobre las cookies y le permite fijar las cookies que prefiera. También le permite borrar cualquier cookie que tenga almacenada en ese momento.</p>


<li>Safari (<a href="http://goo.gl/KFBFh" target="_blank">goo.gl/KFBFh</a>)</li>
<p>En el menú de configuración, seleccione la opción de "preferencias". Abra la pestaña de privacidad. Seleccione la opción que quiera de la sección de "bloquear cookies". Recuerde que ciertas funciones y la plena funcionalidad del Sitio pueden no estar disponibles después de deshabilitar los cookies.</p>
</ul>


<p>Si desea no ser rastreado por las cookies, Google ha desarrollado un complemento para instalar en su navegador al que puede acceder en el siguiente enlace: <a href="http://goo.gl/up4ND" target="_blank">goo.gl/up4ND</a>.<br />
A partir de la opción que tome acerca del uso de cookies en los sitios web, se le enviará una cookie adicional para salvaguardar su elección y que no tenga que aceptar el uso de cookies cada vez que acceda al Sitio Web.</p>
<br />
<p><b>Cookies en los dispositivos móviles:</b></p>

<p>También usamos cookies u otros dispositivos de almacenamiento en dispositivos móviles.</p>

<p>Al igual que sucede en los navegadores de ordenadores, lo navegadores de los dispositivos móviles permiten realizar cambios en las opciones o ajustes de privacidad para desactivar o eliminar las cookies.

Si desea modificar las opciones de privacidad siga las instrucciones especificadas por el desarrollador de su navegador para dispositivo móvil.

A continuación podrá encontrar algunos ejemplos de los links que le guiarán para modificar las opciones de privacidad en su dispositivo móvil:</p>

  <ul class="ulCookies">
	<li><p>IOS: <a href="http://goo.gl/pRkla" target="_blank">goo.gl/pRkla</a></p></li>
	<li><p>Windows Phone: <a href="http://goo.gl/Rx8QQ" target="_blank">goo.gl/Rx8QQ</a></p></li>
	<li><p>Chrome Mobile: <a href="http://goo.gl/Rx8QQ" target="_blank">goo.gl/Rx8QQ</a></p></li>
	<li><p>Opera Mobile: <a href="http://goo.gl/pRkla" target="_blank">goo.gl/XvmTG</a></p></li>
  </ul>
