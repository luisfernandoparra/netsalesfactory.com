<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
if($is_movil == 1 || $layoutType == 'mob_'){
?>
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<meta http-equiv="Pragma" content="no-cache">
<?php
}
?>
<title><?php echo $nombCliente; ?></title>

<link href="<?php echo $path_raiz_aplicacion; ?>js/modal/css/modal.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $path_raiz_aplicacion; ?>js/facebox/facebox.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $path_raiz_aplicacion; ?>js/window/css/window.css" rel="stylesheet" type="text/css" />

<!--- <link href="<?php echo $path_raiz_aplicacion_local; ?>css/<?=$layoutType?>css_<?=$id_crea?>.css" rel="stylesheet" type="text/css" /> -->
<!--- <link href="<?php echo $path_raiz_aplicacion_local; ?>css/common.css" rel="stylesheet" type="text/css" /> -->

<link href="<?php echo $path_raiz_aplicacion_local; ?>css/csscommon.php" rel="stylesheet" type="text/css" />

<!--
<?='<hr>'.$path_raiz_aplicacion_local?>
<?='<hr>'.$path_raiz_includes_local?>
-->
<script type="text/javascript">
var root_path = "<?php echo $path_raiz_aplicacion;?>";<?php /*ruta global para las imgs del facebox y del modal*/?>
var root_path_local = "<?php echo $path_raiz_aplicacion_local;?>";<?php /*ruta global para ajax*/?>
var nomb_promo = "<?php echo $arr_creas[$id_crea]['nombpromo']; ?>";
var id_source = "<?php echo trim($id_source); ?>";
var id_client = "<?php echo $id_client; ?>";




var arr_campanas = [<?php  echo $campanas; ?>];
var is_ctc = "<?php echo trim($is_ctc); ?>";
var is_movil = "<?php echo trim($is_movil); ?>";

var root_path_ws = "<?php echo $path_raiz_ws;?>";
var url_ws = "<?php echo $url_ws;?>";
var file_ws = "<?php echo $file_ws;?>";
var arr_pixel_JS = ["<?php echo $arr_pixel_JS[0];?>","<?php echo $arr_pixel_JS[1];?>"];
var tduid = "<?php echo $tduid; ?>";
  var trdbl_organization = "<?php echo $organization; ?>"; //Pixel Tradedoubler. Organizacion
  var trbdl_event = "<?php echo $event; ?>";
  var nombCliente = "<?php echo $nombCliente; ?>";
var is_prov = "<?php echo trim($is_prov); ?>";
var nombCliente = "<?php echo ucwords(trim($nombCliente));?>";
var lang = "<?php echo $lang;?>";
</script>

<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/modal/js/jquery.simplemodal.1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/facebox/facebox.min.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/window/js/window.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/funciones_comunes.js"></script>
<?php
$js_lang_path = $path_raiz_aplicacion_local.'js/'.$lang.'/lang.js';
//if (file_exists($js_lang_path)) {
    ?>
<script type="text/javascript" src="<?php echo $js_lang_path;  ?>"></script>
<?php 
//} else {
//    echo 'NO existe vaya mierda '.$js_lang_path;
//}
if (!empty($arr_creas[$id_crea]['js']) && trim($arr_creas[$id_crea]['js'])!='' ) {
?>
	<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/<?php echo trim($arr_creas[$id_crea]['js']); ?>"></script>
<?php

} else {
?>
	<script type="text/javascript" src="<?php echo $path_raiz_aplicacion_local;  ?>js/validacion.js"></script>
<?php
}
?>
<style>
.modalBlck{
}
</style>
<script type="text/javascript">
function modalPrivacy()	// NO MOBILE MODAL
{
	var msg='<div style="display:block;width:100%;height:100%;background:#fff;"><iframe style="width:100%;height:100%;background:#fff;" frameborder="no" src="<?=$path_raiz_aplicacion_local?>privacy_policy.php?cr=<?php echo $id_crea;?>&lang=<?php echo $lang;?>"></iframe></div>';
	$.facebox(msg,'modalPop');
	return false;
}

$(document).ready(function(){

  $(".enlace_pp").click(function(){	// ONLY MOBILE DEVICES
	$.facebox({ajax:'<?=$url_local?>privacy_policy.php?deviceType=<?=$layoutType?>&cr=<?php echo $id_crea;?>&lang=<?php echo $lang;?>'},'modalPop');
	return false;
  });

  $(".modalMain").click(function(){	// NO MOBILE DEVICES
	modalPrivacy()
	return false;
  });

  $(".fotos a").click(function(e){	// NO MOBILE DEVICES
	var tmpElem=$(this).children("a[hover]");
	var tmpText=$(this).children("h3");

	if($("._"+tmpElem.context.id).hasClass("elemSelected"))
	{
	  $("._"+tmpElem.context.id).removeClass("elemSelected");
	  $($(this).children("h3")).removeClass("txtVisible");
	  $("input[name=data_"+tmpElem.context.id+"]").val(0)
	}
	else
	{
	  $($(this).children("h3")).addClass("txtVisible");
	  $("input[name=data_"+tmpElem.context.id+"]").val(1)
	  $("._"+tmpElem.context.id).addClass("elemSelected");
	}
	return false;
  });

});
</script>

</head>
