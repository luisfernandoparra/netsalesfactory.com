<?php
/**
 * @date 2016.06.22. Añadimos la posibilidad de idioma, recibiendo el parámetro en lang= ISO2 (es, fr...)
 */
//Local_vars.
//Variable s locales
$url_condiciones = (isset($arr_creas) && !empty($arr_creas[$id_crea]['condiciones'])) ? trim($arr_creas[$id_crea]['condiciones']) : '';
$url_protecciondatos = (isset($arr_creas) && !empty($arr_creas[$id_crea]['protecciondatos'])) ? trim($arr_creas[$id_crea]['protecciondatos']) : '';

$lang = (!empty($_REQUEST['lang']) && (strtolower(trim($_REQUEST['lang']))!='es'|| strtolower(trim($_REQUEST['lang']))!='')) ? strtolower(trim($_REQUEST['lang'])) : 'es';


$debug = (!empty($_REQUEST['debug']));

$site_tmp = trim(strtolower($_SERVER['HTTP_HOST']));
//$site_tmp = 'www.lecoindestendances.com';
//$site_tmp = 'www.boulevardvip.com';
$tmp = explode('.', $site_tmp);
$cont = count($tmp);
$site = $tmp[$cont - 2] . '.' . $tmp[$cont - 1];

if ($debug) {
    echo "<hr>";
    echo "Site tmp: $site_tmp Site: $site";
}

//echo $site;

$conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
$conn= $conexion->connectDB();
$sql = 'select id_site,site, site_url, img_background, color_formulario, color_texto, color_boton, color_linea,img_header,img_header_mobile,disclaimer_field1,disclaimer_field2 from sites_definition where LOWER(site_url)=\'%s\'';
$sql = sprintf($sql, $site);
$conexion->getResultSelectArray($sql);
$arr = $conexion->tResultadoQuery;

if ($debug) {
    echo "<hr>";
    echo "Sql: $sql<br>";
    echo "conexion: ".$conn;
    echo "<pre>";
    print_r($arr);
}

$_SESSION['CSS']['groundsite'] = $id_client . '/' . $groundsite;
$img_header = $id_client . '/' . $img_header;
$img_header_mobile = $id_client . '/' . $img_header_mobile;
if (count($arr) > 0) {
    $colorFormulario = (!empty($arr[0]['color_formulario'])) ? $arr[0]['color_formulario'] : $colorFormulario;
    $colorTextoFormulario = (!empty($arr[0]['color_texto'])) ? $arr[0]['color_texto'] : $colorTextoFormulario;
    $color_linea_morada = (!empty($arr[0]['color_linea'])) ? $arr[0]['color_linea'] : $color_linea_morada;
    $colorBoton = (!empty($arr[0]['color_boton'])) ? $arr[0]['color_boton'] : $colorBoton;
    $id_client = (!empty($arr[0]['id_site'])) ? $arr[0]['id_site'] : $id_client;
    $groundsite = (!empty($arr[0]['img_background']) && file_exists(($pathlocal . 'img/' . $id_client . '/' . $arr[0]['img_background']))) ? $id_client . '/' . $arr[0]['img_background'] : $id_client . '/' . $groundsite;
//echo "AQUI ".$groundsite;
    $_SESSION['CSS']['groundsite'] = $groundsite;
    $nombCliente = (!empty($arr[0]['site'])) ? $arr[0]['site'] : $nombCliente;
//&& file_exists(($path_raiz_aplicacion_local . 'img/' . $id_client . '/' . $arr[0]['img_header']))

    $img_header = (!empty($arr[0]['img_header']) ) ? $id_client . '/' . $arr[0]['img_header'] : $img_header;
    $img_header_mobile = (!empty($arr[0]['img_header_mobile']) && file_exists(($pathlocal . 'img/' . $id_client . '/' . $arr[0]['img_header_mobile']))) ? $id_client . '/' . $arr[0]['img_header_mobile'] : $img_header_mobile;

    $disclaimer_field1 = (!empty($arr[0]['disclaimer_field1'])) ? $arr[0]['disclaimer_field1'] : $disclaimer_field1;
    $disclaimer_field2 = (!empty($arr[0]['disclaimer_field2'])) ? $arr[0]['disclaimer_field2'] : $disclaimer_field1;

    /* echo $pathlocal . 'img/' . $id_client . '/' . $arr[0]['img_header'];
      if (file_exists($pathlocal . 'img/' . $id_client . '/' . $arr[0]['img_header']))
      echo 'PUES CLARO';
      else echo 'nooooo';
     * 
     */
}
$path_imgs_preferences = $path_raiz_aplicacion_local . 'img/preferences/'.((!empty($arr_creas[$id_crea]['path_imgs_preferences'])) ? trim($arr_creas[$id_crea]['path_imgs_preferences']) : '');
if (file_exists($path_raiz_includes_local.'img/'.$id_client.'/preferences/')) {
    $path_imgs_preferences = $path_raiz_aplicacion_local.'img/'.$id_client.'/preferences/';
} 



$conexion->disconnectDB();
$_SESSION['CSS']['colorFormulario'] = $colorFormulario;
$_SESSION['CSS']['colorTextoFormulario'] = $colorTextoFormulario;
$_SESSION['CSS']['color_linea_morada'] = $color_linea_morada;
$_SESSION['CSS']['colorBoton'] = $colorBoton;
$_SESSION['disclaimer_field1'] = $disclaimer_field1;
$_SESSION['disclaimer_field2'] = $disclaimer_field2;
$_SESSION['site'] = $site;
$_SESSION['path_imgs_preferences'] = $path_imgs_preferences;


//echo "exists: ".file_exists($path_raiz_includes_local.'img/'.$id_client.'/preferences/').' -> '.$path_raiz_includes_local.'img/'.$id_client.'/preferences/';
//echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";

?>