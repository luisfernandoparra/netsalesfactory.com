<?php
$lang_error_1 = 'Faltan Datos. Error 101';
$lang_error_2 = 'No se ha podido conectar a la BBDD. Error 102';
$lang_error_3 = 'Parece que ya ha contestado e esta encuesta anteriormente, solo puede hacerlo una vez!';
$lang_error_4 = 'Se ha producido un error al introducir los datos, si lo desea, puede intentarlo más tarde.';
$lang_error_5 = 'Falta rellenar el campo del correo electrónico. Error 106';
