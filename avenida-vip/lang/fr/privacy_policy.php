<?php
?>
<h1>Politique de confidentialit&eacute; et de protection des donn&eacute;es de <?php echo $disclaimer_field1;?></h1>

<p><b>1. Pr&eacute;sentation et droit d&#39;information</b></p>

<p>Netsales Factory SLU (d&eacute;sormais <?php echo $disclaimer_field1;?>) prend tr&egrave;s au s&eacute;rieux la prot&eacute;ction et la confidentialit&eacute; de ses donn&eacute;es personnelles. De fait, ses informations personnelles sont conserv&eacute;es de mani&egrave;re s&ucirc;re et sont trait&eacute;es avec le maximum de prudence. La pr&eacute;sente politique de prot&eacute;ction des donn&eacute;es r&eacute;gule l&#39;acc&egrave;s et l&#39;utiliation du service de <?php echo $disclaimer_field1;?>. L&#39;entreprise Netsales, domicili&eacute;e <?php echo $disclaimer_field2;?>, met &agrave; disposition des usagers (d&eacute;sormais les "Usagers" ou "l&#39;Usager") d&#39;internet int&eacute;ress&eacute;s par les services (d&eacute;sormais les "Services") et contenus (d&eacute;sormais les "Contenus") plac&eacute;s sur le site Web. En cliquant sur les mots "CONTINUER" ou "ENVOYER" du site web, les Usagers acceptent express&eacute;ment la pr&eacute;sente Politique de Confidentialit&eacute; et Prot&eacute;ction des donn&eacute;es.</p>

<p><b>2. Recommandations </b></p>

<p>Nous vous prions de lire attentivement et de suivre les indications suivantes:<p>

<p>-Utiliser des mots de passe alphanum&eacute;riques d&#39;au moins 8 caract&egrave;res et comportant au moins une majuscule.<p>

<p>- Le site web n&#39;est pas destin&eacute; &agrave; un public mineur. Activez le contr&ocirc;le parental pour contr&ocirc;ler l&#39;acc&egrave;s d&#39;&eacute;ventuels mineurs &agrave; Internet. Pensez aussi &agrave; les informer de la politique de s&eacute;curit&eacute; sur Internet.</p>

<p>-Veillez &agrave; avoir &agrave; tout moment un anti-virus actif et &agrave; jour afin de garantir la s&eacute;curit&eacute; de votre software, de vos donn&eacute;es et de toute application que la navigation sur Internet pourrait exposer &agrave; d&#39;&eacute;ventuels virus.</p>

 <p>- Veuillez lire et v&eacute;rifier les conditions g&eacute;n&eacute;rales d&#39;utilisation et la politique de confidentialit&eacute; et de protection des donn&eacute;es qui est mise &agrave; votre disposition dans le site web.</p>


<p><b>3. TRAITEMENT DES DONN&Eacute;ES, OBJECTIF DU TRAITEMENT ET CONSENTEMENT &Agrave; LEUR UTILISATION.</b></p>

<p>Les donn&eacute;es des utilisateurs receuillies par le biais des formulaires de registre en ligne disponibles sur le site web sont collect&eacute;es par et pour Netsales afin de mettre &agrave; leur disposition les services propos&eacute;s sur le site. L&#39;utilisateur prendra note que ses donn&eacute;es (nom, pr&eacute;nom, num&eacute;ro de t&eacute;l&eacute;phone, adresse, entreprise, poste, site web, adresse mail, etc) seront consign&eacute;es dans un fichier automatique et qu&#39;il pourra par la suite recevoir les newsletters, offres et promotions des services <?php echo $disclaimer_field1;?> par courrier, t&eacute;l&eacute;phone, SMS/MMS ou par tout autre moyen de communication &eacute;lectronique &eacute;quivalent, en accord avec la Loi Fondamentale de Prot&eacute;ction des Donn&eacute;es Personnelles 15/1999 du 13 D&eacute;cembre (plus tard, LFPD),  la Loi des Services de la Soci&eacute;t&eacute; d&#39;Information et du Commerce &eacute;lectronique 34/2002 du 11 Juin, et la Loi G&eacute;n&eacute;rale des T&eacute;l&eacute;communications 32/2003 du 3 Novembre. En envoyant le formulaire de registre rempli, l&#39;utilisateur conssent &agrave; ce que <?php echo $disclaimer_field1;?> et ses partenaires (membres du groupe FAST TRACK NETWORK SL tels que FENIX DATA S.L.U.; MAXIDAT S.L.U.; DUJOK ICONSULTING S.L.; FINTEL TELEMARKETING S.L.U.; KARPE DEAL S.L., PICARISIMO S.L.)  exploitent ses donn&eacute;es (nom, pr&eacute;nom, adresse postale et donn&eacute;es de g&eacute;olocalisation) ou les c&egrave;dent (y compris pour &eacute;purer les donn&eacute;es personnelles) &agrave; d&#39;autres entreprises d&#39;Espagne ou de l&#39;Union Europ&eacute;enne pour qu&#39;il recoive par la suite, &agrave; travers de notre r&eacute;seau ou d&#39;un autre, des publicit&eacute;s, des offres comerciales et des offres de service par courrier, par t&eacute;l&eacute;phone, par mail, par SMS/MMS ou par tout autre moyen de communication &eacute;lectronique &eacute;quivalent. Les dites offres pourront se r&eacute;f&eacute;rer aux secteurs suivants:</p>

<p>-Marketing ou relevants de la F&eacute;d&eacute;ration Espagnole du Commerce &eacute;lectronique et du Marketing Digital (FECEMD)</p>

<p><b>-T&eacute;l&eacute;communications:</b> Produits et Services de T&eacute;l&eacute;communication et Technologie.</p>

<p><b>-Finance:</b> Pr&ecirc;ts &eacute;mis par des entit&eacute;s financi&egrave;res, d&#39;assurance et de protection sociale.</p>

<p><b>-Loisirs:</b> Edition, Tourisme, Sport, Philat&eacute;lie, Photographie, Divertissement, Vente de Jouets, Transports, Jardinerie, Hobbies, Loterie, Communaut&eacute; de Joueurs, Communication et Spectacles.</p>

<p>-Consommation de Masse: &eacute;lectronique, Informatique, Textile, Image et son, Compl&eacute;ments, Ameublement, Bazar, Hygi&egrave;ne et soin (Cosm&eacute;tiques, Parfumerie, Parafarmacie, M&eacute;dicaments Commercialis&eacute;s), Mobilier, Immobilier, Alimentation et Boissons, Sant&eacute; et Beaut&eacute;, Mat&eacute;riel de Bureau, Mode et D&eacute;coration.</p>

<p><b>-Automobile:</b> Produits et Services en rapport avec Automobiles, Motos et Camions. </p>

<p><b>-&Eacute;nergie et Eau:</b> Produits en rapport avec l&#39;&eacute;lectricit&eacute;, l&#39;Hydrocarbure, le Gas et l&#39;Eau.</p>

<p><b>-ONG:</b> Produits et Services et relation avec des ONG.</p>

<p>L&#39;utilisateur accepte et conssent &agrave; ce que l&#39;entreprise <?php echo $disclaimer_field1;?> traite et consulte les fichiers d&#39;un tierce parti afin de d&eacute;terminer son profil et de lui offrir les produits et services les mieux adapt&eacute;s, que ce soient les siens ou ceux d&#39;autres entreprises et en relation avec les secteur &eacute;num&eacute;r&eacute;s plus haut. L&#39;utilisateur accepte expr&eacute;ssement la pr&eacute;sente Politique de Confidentialit&eacute; et conssent au traitement automatis&eacute; des donn&eacute;es fournies.</p>

<p>Cependant, l&#39;utilisateur pourra r&eacute;voquer son consentement &agrave; tout moment et dans tout communiqu&eacute; comercial qu&#39;il recevra en envoyant un mail &agrave; l&#39;adresse suivante: info@masterchollo.com ou un courrier &agrave; <?php echo $disclaimer_field1;?>, <?php echo $disclaimer_field2;?>.</p>


<p><b>4. Authenticit&eacute; des donn&eacute;es et caract&egrave;re obligatoire ou facultatif des informations fournies par l&#39;utilisateur.</b></p>

<p>L&#39;Usager garantit l&#39;authenticit&eacute; des donn&eacute;es personnelles fournies et s&#39;engage &agrave; avertir <?php echo $disclaimer_field1;?> de tout changement. <?php echo $disclaimer_field1;?> se r&eacute;serve le droit d&#39;exclure tout usager dont les donn&eacute;es seraient falsifi&eacute;es. Nous recommandons &agrave; l&#39;utilisateur de proc&eacute;der avec la plus grande prudence et d&#39;utiliser des outils de s&eacute;curit&eacute;s pour garantir la protection de ses donn&eacute;es. <?php echo $disclaimer_field1;?> d&eacute;cline toute r&eacute;sponsabilit&eacute; en cas de soustraction, de modification ou de perte de donn&eacute;es illicites.</p>

<p><b>5. Droits d&#39;acc&egrave;s, de r&eacute;ctification, d&#39;annulation et d&#39;opposition.</b></p>

<p>Nous rappelons que l&#39;utilisateur pourra, &agrave; tout moment, exercer son droit d&#39;acc&egrave;s, de r&eacute;ctification, d&#39;annulation ou d&#39;opposition au traitement g&eacute;n&eacute;ral ou &agrave; fins promotionelles de ses donn&eacute;es personelles en envoyant un mail &agrave; l&#39;adresse suivante: info@masterchollo.com, r&eacute;f&eacute;renc&eacute; "<?php echo $disclaimer_field1;?>-Ejercicio derechos LOPD/LSSI]) et dans lequel devront apparaître: nom, pr&eacute;nom, photocopie d&#39;une pi&egrave;ce d&#39;identit&eacute; (passeport ou autre document valide), sa demande concr&egrave;te, son adresse actuelle, date, signature et documents d&#39;acr&eacute;ditation de la demande formul&eacute;e.</p>


<p><b>6. UTILISATION DES COOKIES</b></p>

<p>En accord avec le D&eacute;cret/Loi Royal 13/2012 du 30 Mars, entr&eacute; en vigueur le 01/04/2012, <?php echo $disclaimer_field1;?> met &agrave; disposition de l&#39;utilisateur, &agrave; partir de son site web,  un service qui lui permet d&#39;accepter ou de refuser l&#39;utilisation des cookies dans son navigateur. En acceptant la pr&eacute;sente Politique de Confidentialit&eacute;, l&#39;utilisateur accepte les services de retargeting qui pourraient survenir &agrave; la suite d&#39;un envoi comercial. L&#39;utilisateur consent &agrave; que les dites communications commerciales comportent des dispositifs de stockage ou des cookies de publicit&eacute; qui s&#39;installeront dans son navigateur. Ces services de retargeting ont pour objectif de proposer &agrave; l&#39;utilisateur des produits et services plus adapt&eacute;s &agrave; son profil grâce &agrave;:</p>

<p>-L&#39;installation de dispositifs de stockage et de r&eacute;cup&eacute;ration des donn&eacute;es ou de cookies en &eacute;quipes terminales dans certains des courriers &eacute;lectroniques envoy&eacute;s aux utilisateurs.</p>

<p>-L&#39;envoi de communications comerciales par courriers &eacute;lectroniques aux utilisateurs dont le navigateur comporterait un cookie suite &agrave; sa visite sur une page web ou &agrave; l&#39;ouverture d&#39;un mail pr&eacute;c&eacute;dent. Notons que dans chaque communication commerciale comportant des cookies se trouve un avertissement &agrave; l&#39;utilisateur, qui par ce biais saura exactement quel type de cookie il contient et comment le d&eacute;sactiver.</p>

<p>En acceptant la Politique de Protection des Donn&eacute;es et de  Confidentialit&eacute; du Site Web, l&#39;utilisateur accepte expr&eacute;ssement l&#39;utilisation des cookies par <?php echo $disclaimer_field1;?>, &agrave; moins d&#39;&eacute;mettre ouvertement le souhait contraire. L&#39;utilisateur ayant autoris&eacute; l&#39;utilisation des cookies peut &agrave; tout moment configurer son navigateur afin d&#39;&ecirc;tre averti de la r&eacute;ception de cookies et &eacute;viter leur installation. Il est vivement recommand&eacute; de consulter les instructions et manuels de votre navigateur. Plus d&#39;informations sur le fonctionnement des cookies sont disponibles sur le site http://www.youronlinechoices.com/es/ </p>


<p>Les cookies sont des fichiers envoy&eacute;s dans le navigateur par l&#39;interm&eacute;diaire d&#39;un serveur web afin d&#39;enregistrer l&#39;activit&eacute; de l&#39;utilisateur sur internet. Les cookies utilis&eacute;s sur le site web s&#39;associent &agrave; un utilisateur unique et anonyme, comme &agrave; son ordinateur, et ne g&eacute;n&egrave;rent pas les donn&eacute;es elles-m&ecirc;mes. &agrave; travers l&#39;usage de cookies il est possible que le serveur o&ugrave; se trouve le site reconnaisse le navigateur de l&#39;utilisateur afin de rendre la navigation plus intuitive en permettant par exemple l&#39;acc&egrave;s direct des utilisateurs d&eacute;j&agrave; enregistr&eacute;s aux offres, services, promotions ou concours qui leur sont r&eacute;serv&eacute;s san savoir &agrave; s&#39;enregistrer &agrave; chaque visite. Les cookies serviront &eacute;galement &agrave; mesurer l&#39;audience et les param&egrave;tres de trafic, contr&ocirc;ler le progres et le nombre d&#39;entr&eacute;es.</p>

<p>Pour utiliser le site web, il n&#39;est pas n&eacute;cessaire que l&#39;utilisateur autorise l&#39;installation des cookies envoy&eacute;s par le site, ou par le tierce parti qui agit en son nom propre, sans qu&#39;il soit n&eacute;cesaire d&#39;ouvrir une session au pr&eacute;alable. Il n&#39;est pas non plus n&eacute;cessaire que l&#39;utilisateur ouvre sa session pour profiter de chaque service dont la prestation n&eacute;cessite un enregistrement pr&eacute;alable ou un “login”</p>

<p><b>7. Mesures de s&eacute;curit&eacute;</b></p>

<p><?php echo $disclaimer_field1;?> maintient son niveau de protection des donn&eacute;es personnelles conforme &agrave; la LOPD  et au D&eacute;cret Royal 1720/2007 du 21 D&eacute;cembre, qui v&eacute;rifie le R&egrave;glement du D&eacute;veloppement de la LOPD au regard des donn&eacute;es personnelles et a d&eacute;ploy&eacute; tous les moyens techniques possibles pour &eacute;viter la perte, le mauvais usage, l&#39;alt&eacute;ration, l&#39;acc&egrave;s non autoris&eacute; et le vol des donn&eacute;es fournies par l&#39;utilisateur par l&#39;interm&eacute;diaire du site web. Cela va sans dire que l&#39;utilisateur doit avoir pleinement conscience du fait que les mesures de s&eacute;curit&eacute; sur internet ne sont pas infaillibles. NETSALEs s&#39;engage &agrave; respecter son devoir de confidentialit&eacute; et de respect des donn&eacute;es personnelles contenues dans le fichier automatique en accord avec la l&eacute;gislation en vigueur, de mani&egrave;re &agrave; garantir un traitement s&ucirc;r en cas d&#39;&eacute;ventuelles cessions.</p>

<p><b>8. LIENS VERS LES PAGES WEB </b></p>

<p>Le site web de <?php echo $disclaimer_field1;?> pourra contenir des liens vers des pages web d&#39;entreprises et de services terciaires. <?php echo $disclaimer_field1;?> d&eacute;cline toute repsonsabilit&eacute; quant aux modalit&eacute;s de protection des donn&eacute;es et de confidentialit&eacute; mises en vigueur par ces derniers. Il est vivement conseill&eacute; de lire les d&eacute;clarations de politique de confidentialit&eacute; de ces pages web qui ne sont pas propri&eacute;t&eacute; de <?php echo $disclaimer_field1;?>, afin de prendre connaissance des modalit&eacute;s d&#39;usage, de traitement et de protection des donn&eacute;es personnelles. Les conditions soumises par ces sites ne peuvent en aucun cas être les mêmes que celles propos&eacute;es par <?php echo $disclaimer_field1;?>.</p>

<p><b>9. Questions</b></p>

<p>En cas de questions sur cette Politique de Confidentialit&eacute;, nous vous prions de nous contacter sur l&#39;adresse e-mail suivante: info@boulevardvip.com</p>

<p><b>10. Changements</b></p>

<p><?php echo $disclaimer_field1;?> se r&eacute;serve le droit de r&eacute;viser sa politique de Confidentialit&eacute; &agrave; tout moment. L&#39;utilisateur s&#39;engage donc &agrave; relire r&eacute;gulierement cette d&eacute;claration de confidentialit&eacute; afin de se tenir au courant de tout changement &eacute;ventuel. </p>

<p><b>11. Conditions particuli&egrave;res </b></p>

<p>L&#39;acc&egrave;s &agrave; certains contenus propos&eacute;s par le site Web peut &ecirc;tre soumis &agrave; des conditions qui leur sont propres et qui, selon les cas, substituent, compl&egrave;tent ou modifient les conditions g&eacute;n&eacute;rales. Ayant acc&eacute;d&eacute; et/ou utilis&eacute; pr&eacute;cedemment ces contenus, l&#39;utilisateur se doit de lire attentivement les conditions particuli&egrave;res correspondantes.</p>
