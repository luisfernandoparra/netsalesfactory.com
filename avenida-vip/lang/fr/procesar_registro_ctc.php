<?php
$lang_error_1 = 'FR Faltan Datos. Error 101';
$lang_error_2 = 'FR No se ha podido conectar a la BBDD. Error 102';
$lang_error_3 = 'Il semble que vous avez déjà répondu à ce questionnaire précédemment, vous ne pouvez le compléter qu´une seule fois.';
$lang_error_4 = 'Il y a eu une erreur lorsque vous avez introduit vos données, si vous le désirez, vous pouvez réessayer plus tard.';
$lang_error_5 = 'FR Falta rellenar el campo del correo electrónico. Error 106';

