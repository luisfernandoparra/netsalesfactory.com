/**
 * Fichero javascript de variables del lenguaje ESPAÑOL
 */

var lang_datos_recogidos = 'Datos recogidos correctamente';
var lang_datos_error = 'Ha habido un error al procesar los datos.<br />Por favor, inténtelo más tarde.';
var lang_datos_recogida_error = 'Error en el procesamiento de Datos';
var lang_register_ok_1 = '<p>Te has registrado <strong>correctamente</strong> en ';
var lang_register_ok_2 = '.</p><p>Si todavía no has dejado tus preferencias, házlo.</p><p>Así te enviaremos unicamente ventajas que sean de <strong>tu interés</strong>.</p><br />';
var lang_alert_sexo = 'Debes indicar tu género';
var lang_alert_nombre = 'Debes rellenar el campo Nombre';
var lang_alert_nombre_2 = "El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios";
var lang_alert_apellido = 'Debes rellenar el campo Apellido';
var lang_alert_apellido_2 = 'El campo Apellidos contiene caracters no válidos. Solo se permiten letras y espacios';
var lang_alert_cp = 'Debes rellenar el campo del código postal';
var lang_alert_cp_2 = 'El código postal solo puede tener 5 dígitos';
var lang_alert_cp_3 = 'El Código Postal solo admite como máximo 5 números sin espacio (p.ej 28000 ó 08348)';
var lang_alert_email = 'Debes rellenar el campo Email';
var lang_alert_email_2 = 'Formato de Email incorrecto';
var lang_alert_politica = 'Debes aceptar la Política de Privacidad';
var lang_alert_telf = 'Debes rellenar el campo Teléfono';
var lang_alert_telf_2 = 'El número de teléfono solo puede tener 9 dígitos';

var lang_alert_error = 'Error en los Datos Introducidos';

