/**
 * Variables del lenguaje FR FRANCES
 */

var lang_datos_recogidos = 'Données collectées correctement';
var lang_datos_error = 'Il y a eu une erreur dans le traitement des données. <br /> Nous vous prions d’essayer plus tard.';
var lang_datos_recogida_error = 'Erreur de traitement des données';
var lang_register_ok_1 = '<p> Vous vous êtes enregistré <strong> correctement </strong> en ';
var lang_register_ok_2 = '.</ p><p> Si vous n’avez pas encore laissé vos préfèrences, on vous remercie de le faire. </p><p> De cette façon nous allons vous envoyer uniquement les avantages qui sont de <strong>votre intérêt </strong>.';
var lang_alert_sexo = 'Nous vous remercions d’indiquer votre sexe';
var lang_alert_nombre = 'Nous vous remercions de compléter le champ “Prénom”';
var lang_alert_nombre_2 = 'Le champ prénom contient des caractères non valides. Sont acceptés niquement des lettres et des espaces';
var lang_alert_apellido = 'Nous vous remercions de compléter le champ “Nom” (“Nom de famille”)';
var lang_alert_apellido_2 = 'Le champ nom contient des caractères non valides. Sont acceptés  uniquement des lettres et des espaces';
var lang_alert_cp = 'Nous vous remercions de compléter le champ “code postal”';
var lang_alert_cp_2 = 'Le code postal doit être composé de 5 chiffres';
var lang_alert_cp_3 = 'Le code postal doit être composée uniquement de 5 chiffres sans espaces ( par exemple 28000 ou 08348)';
var lang_alert_email = 'Nous vous remercions de compléter le champ Email';
var lang_alert_email_2 = 'Le format de l’email n’est pas correct';
var lang_alert_politica = 'Nous vous remercions d’accepter la politique de la privacité';
var lang_alert_telf = 'Nous vous remercions de compléter le champ “Téléphone”';
var lang_alert_telf_2 = 'Le numéro de téléphone peut contenir uniquement 9 chiffres';

var lang_alert_error = 'Erreur des données introduites';