<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<!--<link href="nuevalanding/dental 21/laser_prostatico.css" rel="stylesheet" type="text/css" />-->
<?php
if($_REQUEST['deviceType'] == 'mob_'){
?>
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=2.0; user-scalable=yes;">
<meta http-equiv="Pragma" content="no-cache">
<?php
}
?>
</head>

<body>
<div style=" padding:46px; font-size:12px; font-family:Arial, Helvetica, sans-serif; width:<?=$_REQUEST['deviceType'] ? '90% ' : '450px'?>">
<h3 style="font-family: Arial, Helvetica, sans-serif;">POLÍTICA DE PRIVACIDAD  Y PROTECCIÓN DE DATOS DE NETSALES FACTORY</h3>
<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
<strong>1. PRESENTACIÓN Y DERECHO DE INFORMACIÓN
</strong><br />
<br />
http://www.dentalsanitas.es (en adelante, la “Web"), cuyo titular es NETSALES FACTORY SLU con CIF B-86049400, domicilio en Plaza Callao, nº 4, 6º planta, 28013, en MADRID (en adelante, EL INTERMEDIARIO), con domicilio en el Palacio de la Prensa, Plaza de Callao 4, 6º, 28013 Madrid y con CIF número B-86049400, pone a disposición de los Usuarios (en adelante, los “Usuarios” o el “Usuario”) de Internet interesados en los servicios (en adelante, los “Servicios”) y contenidos (en adelante, los “Contenidos”) alojados en el Sitio Web.
Se entiende que el Usuario acepta expresamente la presente Política de Privacidad y Protección de datos una vez que cumplimente el formulario de registro del Sitio Web haciendo click en la palabra “CONTINUAR” o “ENVIAR”. 
 
<br />
<br />
<strong>2.- RECOMENDACIONES</strong><br />
<br />
Por favor lea detenidamente y siga las siguientes recomendaciones: 
<br />
<br />
- 
Utilice contraseñas con una extensión mínima de 8 caracteres, alfanuméricos y con uso de mayúsculas y minúsculas. 
<br />
- 
El Sitio Web no está dirigido a menores de edad. Por favor active el control parental para prevenir y controlar el acceso de menores de edad a Internet e informar a los menores de edad sobre aspectos relativos a la seguridad. 
<br />
- 
Mantenga en su equipo un software antivirus instalado y debidamente actualizado, que garantice que su equipo se encuentra libre de software maligno, así como de aplicaciones spyware que pongan en riesgo su navegación en Internet, y en peligro la información alojada en el equipo. 
<br />
- 
Revise y lea las condiciones generales de uso y la política de privacidad que la plataforma pone a su disposición en el Sitio Web. <br />
<strong><br />
3.- PROCESAMIENTO DE DATOS PERSONALES, FINALIDAD DEL TRATAMIENTO Y CONSENTIMIENTO PARA LA CESIÓN DE DATOS
</strong><br />
<br />
<br />
Los datos de los Usuarios que se recaban a través de los formularios de registro online disponibles en el Sitio Web, son recabados por NETSALES FACTORY con la finalidad de poder prestarles los Servicios ofrecidos a través del Sitio Web. 
Asimismo, le informamos de que sus datos personales de contacto (nombre, apellidos, teléfono móvil, dirección, empresa, cargo, sitio web, dirección de correo electrónico, etc.) serán incorporados a un fichero automatizado y utilizados para remitirle Newsletters y comunicaciones comerciales y promocionales relacionadas con los servicios de NETSALES FACTORY por carta, teléfono, correo electrónico, SMS/MMS, o por otros medios de comunicación electrónica equivalentes y ello al amparo de lo establecido en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (en adelante, LOPD), en la Ley 34/2002 de 11 de Julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico y en la Ley 32/2003 de 3 de Noviembre, General de Telecomunicaciones.
<br />
<br />
El usuario, una vez cumplimente el formulario de registro del Sitio Web, consiente expresamente que sus datos (nombre, apellidos, dirección postal y datos de geolocalización) puedan ser utilizados por SANITAS S.A. DE SEGUROS, en adelante, SANITAS, para la promoción de sus productos, así como para NETSALES como intermediario (y en particular a las sociedades pertenecientes al grupo FAST TRACK NETWORK SL, y FINTEL MARKETING SLU, este último como Agencia de Mediación de SANITAS), otras previstas en la Ley  o cedidos (incluso para depurar datos personales) a otras entidades españolas o de la Unión Europea para enviarle, por carta, teléfono, correo electrónico, SMS/MMS, o por otros medios de comunicación electrónica equivalentes, a través nuestro o de otras entidades, publicidad y ofertas comerciales y de servicios relacionados con los sectores de:
<br />
<br />
- 
Marketing o pertenecientes a la Federación Española de Comercio Electrónico y Marketing Directo (FECEMD)<br />
- 
Telecomunicaciones: Productos y Servicios de telecomunicaciones y tecnología.<br />
- 
Financiero: Prestados por entidades financieras, Aseguradoras y de Previsión social.
<br />
- 
Ocio: Editorial, Turismo, Deportes, Coleccionismo, Fotografía, Pasatiempos, Juguetería, Transporte, Jardinería, Hobbies, Loterías, peñas de loterías, Comunicación y entretenimiento.
<br />
- 
Gran consumo: Electrónica, Informática, Textil, Imagen y Sonido, Complementos, Hogar, Bazar, Cuidado personal (Cosmética, Perfumería, Parafarmacia, especialidades Farmacéuticas publicitarias) Mobiliario, Inmobiliario, Alimentación y Bebidas, salud y belleza, Material de oficina. Moda y decoración.
<br />
- 
Automoción: Productos y Servicios relacionados con el Automóvil, Motocicletas y Camiones.
<br />
- 
Energía y agua: Productos relacionados con la Electricidad, Hidrocarburos, Gas y Agua.
<br />
- 
ONG: Productos y Servicios relacionados con ONG.<br />
<br />
El usuario acepta y consiente que NETSALES FACTORY trate sus Datos Personales y consulte ficheros de terceras entidades con la finalidad de determinar su perfil y ofrecerle productos y servicios adecuados, propios o de terceras empresas pertenecientes a los sectores anteriormente relacionados. El Usuario acepta expresamente la presente Política de Privacidad y otorga su consentimiento expreso al tratamiento automatizado de los datos personales facilitados. No obstante, el Usuario podrá revocar el consentimiento, en cada comunicado comercial o publicitario que se le haga llegar, y en cualquier momento, mediante notificación en la siguiente dirección de correo electrónico info@comon-line.es mediante carta dirigida a NETSALES FACTORY domiciliada en  el Palacio de la Prensa, Plaza de Callao 4, 6º, 28013 Madrid.<br />
<br />
<strong>4.- CARÁCTER OBLIGATORIO O FACULTATIVO DE LA INFORMACIÓN FACILITADA POR EL USUARIO Y VERACIDAD DE LOS DATOS </strong><br />
<br />
El Usuario garantiza que los datos personales facilitados son veraces y se hace responsable de comunicar a NETSALES FACTORY cualquier modificación de los mismos. 
El Usuario responderá, en cualquier caso, de la veracidad de los datos facilitados, NETSALES FACTORY el derecho a excluir de los servicios registrados a todo Usuario que haya facilitado datos falsos, sin perjuicio de la demás acciones que procedan en Derecho. 
Se recomienda tener la máxima diligencia en materia de Protección de Datos mediante la utilización de herramientas de seguridad, no pudiéndose responsabilizar a NETSALES FACTORY de sustracciones, modificaciones o pérdidas de datos ilícitas.<br />
<br />
<strong>5. DERECHOS DE ACCESO, RECTIFICACIÓN, CANCELACIÓN Y OPOSICIÓN</strong><br />
<br />
Le recordamos que, en el momento que lo desee, pueda ejercitar sus derechos de acceso, rectificación, cancelación y oposición o de oposición al tratamiento general o con fines promocionales de sus datos personales enviándonos una comunicación gratuita a la dirección de correo electrónico legales@fasttracknet.com indicando la referencia:“NETSALES-Ejercicio derechos LOPD/LSSI]) que deberá contener: su nombre y apellidos, fotocopia de su DNI (pasaporte u otro documento válido que lo identifique), petición en que se concreta la solicitud, dirección a efectos de notificaciones, fecha, firma y documentos acreditativos de la petición que formula, en su caso. 
<br />
<br />
<strong>6.- MEDIDAS DE SEGURIDAD
NETSALES FACTORY</strong><br />
<br />
mantiene los niveles de seguridad de protección de datos personales conforme a la LOPD y al Real Decreto 1720/2007, de 21 de diciembre, por el que se aprueba el Reglamento de desarrollo de la LOPD que contengan datos de carácter personal y ha establecido todos los medios técnicos a su alcance para evitar la pérdida, mal uso, alteración, acceso no autorizado y robo de los datos que el usuario facilite a través del Sitio Web, sin perjuicio de informarle de que las medidas de seguridad en Internet no son inexpugnables.
NETSALES FACTORY se compromete a cumplir con el deber de secreto y confidencialidad respecto de los datos personales contenidos en el fichero automatizado de acuerdo con la legislación aplicable, así como a conferirles un tratamiento seguro en las cesiones que, en su caso, puedan producirse.<br />
<br />
<strong>7.- LINKS A PÁGINAS WEB</strong><br />
<br />
El Sitio Web de NETSALES FACTORY podría contener links a páginas web de compañías y entidades de terceros. 
NETSALES FACTORY no puede hacerse responsable de la forma en la que estas compañías tratan la protección de la privacidad y de los datos personales, por lo que le aconsejamos que lea detenidamente las declaraciones de política de privacidad de éstas páginas web que no son propiedad de NETSALES FACTORY con relación al uso, procesamiento y protección de datos personales. Las condiciones que ofrecen éstas páginas web pueden no ser las mismas que las que ofrece NETSALES FACTORY<br />
<br />
<strong>8.- PREGUNTAS</strong><br />
<br />
Si tiene alguna pregunta sobre esta Política de Privacidad, rogamos que se ponga en contacto con nosotros enviando un email a info@netsalesfactory.com.<br />
<br />
<strong>9.- CAMBIOS
NETSALES FACTORY</strong><br />
<br />
se reserva el derecho de revisar su Política de Privacidad en el momento que lo considere oportuno. Por esta razón, le rogamos que compruebe de forma regular esta declaración de privacidad para leer la versión más reciente de la política de privacidad de NETSALES FACTORY.<br />
<br />
<strong>10.- CONDICIONES PARTICULARES </strong><br />
<br />
El acceso a determinados Contenidos ofrecidos a través del Sitio Web puede encontrarse sometido a ciertas condiciones particulares propias que, según los casos, sustituyen, completan las modifican las condiciones generales. Por tanto, con anterioridad al acceso y/o utilización de dichos Contenidos, el Usuario ha de leer atentamente también las correspondientes condiciones particulares. </p>

</div>


</body>
</html>
<?
//print_r($_REQUEST['deviceType']);
?>