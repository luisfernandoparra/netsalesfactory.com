<?php
/**
 * Modificación LFP 2015.03.23
 * Añadimos variable para procesar los datos que vienen de acción mobile emailing Gmail
 * Para eso añadimos variable $emailingSource que será true si viene de gmail
 * Si esta variable es true, no hacemos el die del final y tenemos los datos en un array
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');
/**
 * Config loaded viene del fichero bridge. Si está definida y es true, es que ya ha cargado los configs
 */
if(empty($configLoaded)) {
	include('../../conf/config_web.php');
	include('../conf/config_web.php');
}
$resCurl = array();
$raw = '';
$resCurl['error'] = 1;
$sOutput['id'] = -1;
$cli = isset($_REQUEST['cli']) ? $_REQUEST['cli'] : '';

$emailingSource = (!empty($_REQUEST['emailingsource']) && strtoupper(trim($_REQUEST['emailingsource'])) == 'GMAIL');

$nombre = (!empty($_REQUEST['nombre'])) ? trim($_REQUEST['nombre']) : '';
$apellidos = (!empty($_REQUEST['apellidos'])) ? trim($_REQUEST['apellidos']) : '';
$email = (!empty($_REQUEST['em'])) ? trim($_REQUEST['em']) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? $_REQUEST['provincia_text'] : $_REQUEST['provincia'];

$legal = (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : '1';
$permanencia = (!empty($_REQUEST['perm'])) ? trim($_REQUEST['perm']) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;
$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';

$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? trim($_REQUEST['idclient']) : '';
$sourcetype = (!empty($_REQUEST['sourcetype'])) ? trim($_REQUEST['sourcetype']) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? trim($_REQUEST['campaign']) : '';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? trim($_REQUEST['prioridad']) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? trim($_REQUEST['telefono']) : '';
$use_priority = (!empty($_REQUEST['use_priority']) && is_numeric($_REQUEST['use_priority']) && $_REQUEST['use_priority'] == 1 && $prioridad != '') ? 1 : 0;

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

$campos = array('idclient' => $id_client, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad,
    'nombre' => $nombre, 'apellidos' => $apellidos, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI,
    'vcc' => 'FM', 'pwd' => $secret_pwd, 'usr' => $secret_usr,
    'use_priority' => $use_priority);

if (!empty($_SESSION['ws_ip_id']) && is_numeric($_SESSION['ws_ip_id'])) {
    $campos['idIp'] = $_SESSION['ws_ip_id'];
}

$campos['urlOrigen']=$_SERVER['HTTP_REFERER'];	// ADDED 2016.03.30 M.F.
$campos['cli']=$cli;	// ADDED 2016.04.13 M.F.

$qs = http_build_query($campos);
$url = $url_ws . $file_ws . '?' . $qs;

if ($telefono != '') {
    $conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
    if (!$conexion->connectDB()) {
        $msg_err = 'No se ha podido conectar a la BBDD. Error 102';
    } else {
        $cur_conn_id = $conexion->get_id_conexion();
        /* $sql1="SET AUTOCOMMIT=0;";
          $conexion->ejecuta_query($sql1);

          $sql2="BEGIN";
          $conexion->ejecuta_query($sql2);
         */
        $nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
        $nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');
        $telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
        $sql = "insert into %s (id_cliente,nombre,apellidos,telefono,id_provincia,b_legales,email) values ('%d','%s','%s','%s','%d', %d,'%s');";
        $sql = sprintf($sql, $table_registros, $id_client, $nombreIns, $crea, $telefonoIns, (int) $_REQUEST['provincia'], $legal, $email);
        $conexion->ejecuta_query($sql);
        $id = mysql_insert_id();
				$resCurl['id']=$id;
        $conexion->disconnectDB();

				$fileCsv = '../bip_drivefiles/bipdrive-'.date('Ymd').'.csv';
				$headerFile='';

				if(!file_exists($fileCsv))
					$headerFile='Nombre; E-mail; Teléfono'."\r\n";;

				$fp = @fopen($fileCsv,'a') or die('Unable to open file!');
				$line = $headerFile.$nombre.';'.$email.';'.$telefono."\r\n";
				$raw=$error;
				$error=fwrite($fp, $line);
				fclose($fp);

				$error=(int)$error > 0 ? '' : 1;
				$resCurl['error']=$error;

        if ($error) {
            $msg_err = 'Parece que el problema se debe a lo siguiente: "file error".';
        } else {
            $tmpCrea = $_SESSION['idCreatividad'];
            unset($_SESSION['namePromo']); // SE DESTRUYE LA REFERENCIA DE LA SESION
            $tmpName = session_name('bip_drive' . date('Yimis'));
            $_SESSION['namePromo'] = session_name(); // SE CONSTRUYE UNA NUEVA SESION PARA PERMITIR REGISTROS NUEVOS DESDE LA MISMA APERTURA
            $_SESSION['idCreatividad'] = $tmpCrea;

						$resCurl->mensaje=$error ? 'OK' : 'FILE ERROR';
						$resCurl['error']=0;
        }
    }
} else {
    if ($telefono == '') {
        $error = 1;
        $msg_err = 'Falta el campo Teléfono. Error 104';
    }
}

if ($error) {
    $query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
    $query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
    $conexion->getResultSelectArray($query);
    $resQuery = $conexion->tResultadoQuery;
    $idUpdate = $resQuery[0]['id'];
    $query = 'UPDATE %s SET http_referer=\'ERROR CTC: %s\' WHERE id=\'%d\'';
    $query = sprintf($query, $table_front_actions, $resCurl->mensaje, (int) $idUpdate);
    $conexion->ejecuta_query($query);
}

if (!$emailingSource) {
    $res = json_encode($resCurl);
    die($res);
}
?>