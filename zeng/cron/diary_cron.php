<?php
/**
 * CRON DIARIO X ENVIO DEL DIA ANTERIOR
 * DE LOS ARCHIVOS CSV PRESENTES EN LA CARPETA
 * zengfiles
 * 
 * M.F. 2016.06.20
 * 
 */
header('Content-Type: text/html; charset=utf-8');
include('../../conf/config_web.php');
include('../conf/config_web.php');

$emailsSend=$sitesPruebas ? 'mario.francescutto@netsales.es' : 'mario.francescutto@netsales.es'; //luisfer.parra@netsales.es cesar@cntcommerce.com
$yesterday=date('Ymd') - 1;
$csvName='zeng-'.$yesterday.'.csv';
$downloadLink=$url_local .'/'.$subFolderLanding.'/zengfiles/'.$csvName;

$file = $path_raiz_includes_local.'zengfiles/'.$csvName;

if(!file_exists($file))	// SI NO HAY UN ARCHIVO, SE ABORTA EL SCRIPT
	die();

$file_size = filesize($file);
$handle = fopen($file, 'r');
$content = fread($handle, $file_size);
fclose($handle);
$content = chunk_split(base64_encode($content));

//echo '<hr><pre>'.$content;die();

$eol=PHP_EOL;
$separator=md5(uniqid(time()));
$replyto='noreplay@netsales.es';

$subject = 'Archivo CSV ZENG';

$message = '<h1>ARCHIVO CSV ADJUNTO</h1>Adjunto el archivo CSV con los datos obtenidos del d&iacute;a <b>'.date('d-m-Y',strtotime($yesterday)).'</b><br />Nombre del archivo: <b>'.$csvName.'</b>.<br />';
$message .= '<br />Link para descargar el archivo: <a href="'.$downloadLink.'" target="_blank">Descargar el earchivo</a>.';
$message .= '<br />';

//echo $message;die();


$headers .= 'MIME-Version: 1.0' . $eol;
$headers .= 'Content-Type: multipart/mixed; boundary="' . $separator . '"' . $eol;
$headers .= 'Content-Transfer-Encoding: 7bit' . $eol;
$headers .= 'From: Servicio Netsales <alertas@netsales.es>'.$eol;
$headers .= 'Reply-To: '.$replyto.$eol;
//$headers .= 'X-Mailer: PHP/' . phpversion();

// message
$headers .= '--' . $separator . $eol;
$headers .= 'Content-Type: text/html; charset="iso-8859-1"' . $eol;
$headers .= 'Content-Transfer-Encoding: 8bit' . $eol;
$headers .= $eol;
$headers .= $message . $eol;

// attachment
$headers .= '--' . $separator . $eol;
$headers .= 'Content-Type: application/octet-stream; name="' . $csvName . '"' . $eol;
$headers .= 'Content-Transfer-Encoding: base64' . $eol;
$headers .= 'Content-Disposition: attachment' . $eol;
$headers .= $content . $eol;
$headers .= '--' . $separator . '--';

//'Reply-To: ' . "\r\n" .
@mail($emailsSend, $subject, $message, $headers);

//echo $message;