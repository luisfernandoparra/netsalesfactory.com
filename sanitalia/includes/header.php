<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
if($is_movil == 1){
?>
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php
}
?>
<title><?php echo $arr_creas[$id_crea]['title']; ?></title>

<link href="<?php echo $path_raiz_aplicacion; ?>js/modal/css/modal.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $path_raiz_aplicacion; ?>js/facebox/facebox.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $path_raiz_aplicacion; ?>js/window/css/window.css" rel="stylesheet" type="text/css" />

<link href="<?php echo $path_raiz_aplicacion_local; ?>css/css_<?php echo $id_crea; ?>.css" rel="stylesheet" type="text/css" />


<?php
if(!$isOldBrowser)	// NO FUNCIONA CON I-EXPLORER 8 Y ANTERIORES
{
  echo '
<link href="'.$path_raiz_aplicacion.'css/animate.css" rel="stylesheet" type="text/css" />';
}
?>

<script type="text/javascript" src="<?php echo $path_raiz_aplicacion; ?>js/jquery-1.9.1.min.js"></script>

<?php
if(!$isOldBrowser)	// NO FUNCIONA CON I-EXPLORER 8 Y ANTERIORES, SE OMITE LA CARGA
{
  echo '
<script type="text/javascript" src="'.$path_raiz_aplicacion.'js/jquery.textillate.js"></script>
<script type="text/javascript" src="'.$path_raiz_aplicacion.'js/jquery.lettering.js"></script>';
}
?>

<script type="text/javascript">
var root_path = "<?php echo $path_raiz_aplicacion;?>";<?php /*ruta global para las imgs del facebox y del modal*/?>
var root_path_local = "<?php echo $path_raiz_aplicacion_local;?>";<?php /*ruta global para ajax*/?>
var nomb_promo = "<?php echo $arr_creas[$id_crea]['nombpromo']; ?>";
var id_source = "<?php echo trim($id_source); ?>";
var id_client = "<?php echo $id_client; ?>";

var arr_campanas = [<?php  echo $campanas; ?>];
var is_ctc = "<?php echo trim($is_ctc); ?>";
var is_movil = "<?php echo trim($is_movil); ?>";
var root_path_ws = "<?php echo $path_raiz_ws;?>";
var url_ws = "<?php echo $url_ws;?>";
var file_ws = "<?php echo $file_ws;?>";
var arr_pixel_JS = ["<?php echo $arr_pixel_JS[0];?>","<?php echo $arr_pixel_JS[1];?>"];
var tduid = "<?php echo $tduid; ?>";
var trdbl_organization = "<?php echo $organization; ?>"; //Pixel Tradedoubler. Organizacion
var trbdl_event = "<?php echo $event; ?>";
var nombCliente = "<?php echo $nombCliente; ?>";
var is_prov = "<?php echo trim($is_prov); ?>";

$(document).ready(function(){
  
  $(".enlace_condiciones").click(function(){
	$.facebox({div:'#promoConditions'});
	return false;
  });

  $(".enlace_pp").click(function(){
	var msg='<div style="display:block;width:600px;height:500px;background:#fff;"><iframe style="width:100%;height:100%;background:#fff;" frameborder="no" src="<?=$url_protecciondatos?>"></iframe></div>';
	$.facebox(msg);
	return false;
  });
  $(".srt").css("display","none");
  
  // START ANIMATION FLYn
  var pos_fly0=(parseInt(parseInt($("body").css("width"))/2)-parseInt($("#fly0").css("width"))+70);  // retrieve auto central position element fly0
  $("#fly0").delay(1000).show("fast").animate({
    left:pos_fly0,
	top:(parseInt($(".titulo").css("height"))+10)+"px",
    opacity:'1',
    height:'auto',
    width:parseInt($("#fly0").css("width"))
  },1000,function(){
	// start second animatoin
	  $("#fly1").delay(200).show("fast").animate({
	  left:pos_fly0,
	  top:(parseInt($("#fly0").css("height"))+100)+"px",
	  opacity:'1',
	  height:'100px',
	  width:parseInt($("#fly0").css("width"))
	},1000,function(){
	  $("#fly2").delay(200).show("fast").animate({
		left:pos_fly0,
		top:(parseInt($("#fly0").css("height"))+parseInt($("#fly0").css("height"))+100)+"px",
		opacity:'1',
		height:'auto',
		width:'auto'
	  }); 
	});
  // END ANIMATION FLYn

  });
<?php
/*
if(!$isOldBrowser)	// NO FUNCIONA CON I-EXPLORER 8 Y ANTERIORES
{
  echo '
	$(".tlt2").textillate().show(10,function(){
	  $(".srt").delay(2300).fadeIn("slow");
	  return false;
	});
';
}
else
  echo '$(".titulo").css("background-image","url(img/explorer_cabecera_1.png)");$(".srt").delay(1300).fadeIn("slow");';	// ESTO SOLO PARA I-EXPLORER VIEJOS
*/
?>
});
</script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion; ?>js/modal/js/jquery.simplemodal.1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion; ?>js/facebox/facebox.min.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion; ?>js/window/js/window.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion; ?>js/funciones_comunes.js"></script>
<?php
if(!empty($arr_creas[$id_crea]['js']) && trim($arr_creas[$id_crea]['js'])!='' ){
?>
	<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/<?php echo trim($arr_creas[$id_crea]['js']); ?>"></script>
<?php
} else {
?>
	<script type="text/javascript" src="<?php echo $path_raiz_aplicacion_local;  ?>js/validacion.js"></script>
<?php

}
?>
</head>
