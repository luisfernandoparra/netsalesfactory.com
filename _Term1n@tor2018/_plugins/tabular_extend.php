<?php
/**
 * PLUGIN AUTO UPDATE BOOT GRID
 * EXTEND MAIN INDEX MODULE
 *
 * M.F. 20170609
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();
?>
<script type="text/javascript">
var setAutoUpdateBootGrid=true;

function autoReloadBootGrid(){
	t=setTimeout(function(){
		if(setAutoUpdateBootGrid == true)
			autoReloadBootGrid();
		parent.window.ifrListBlock.gridObj.bootgrid('reload');
	},<?=($autoUpdateTime ? ($autoUpdateTime*1000) : 5000)?>);
	return;
}

</script>
