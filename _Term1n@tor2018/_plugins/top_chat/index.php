<?php
/*
 * @package AJAX_Chat
 * @author Sebastian Tschan
 * @author Philip Nicolcev
 * @copyright (c) Sebastian Tschan
 * @license Modified MIT License
 * @link https://blueimp.net/ajax/
 */
@session_start();
@header('Content-Type: text/html; charset=utf-8');
//include('../../../conf/config_web.php');

// Suppress errors:
ini_set('display_errors', 0);

// Path to the chat directory:
define('AJAX_CHAT_PATH', dirname($_SERVER['SCRIPT_FILENAME']).'/');

// Include custom libraries and initialization code:
require(AJAX_CHAT_PATH.'lib/custom.php');

// Include Class libraries:
require(AJAX_CHAT_PATH.'lib/classes.php');

//if(!isset($_REQUEST['userName']) || !$_REQUEST['userName'])
//	$_REQUEST['userName']=$_SESSION['userAdmin]']['name'];

// Initialize the chat:
$ajaxChat=new CustomAJAXChat();
//echo __LEVEL_ACCESS_GOD.'-'.$_SESSION['userAdmin']['name'];