<?php
//	plug-in translations
//	ITALIAN
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$langTxt['plugTopChat']['header']['titleTxt']='Chat';
$langTxt['plugTopChat']['header']['lastUserMsgTxt']=', l´ultimo messaggio è di ';
$langTxt['plugTopChat']['iNewMsg']['titleTxt']='dice';
$langTxt['plugTopChat']['']['']='';

$contL=0;
$wordSep='_';
$tmpWord=null; $tmpValue=null;

foreach($langTxt as $kL=>$vL)
{
	$tmpWord=$kL;

	foreach($vL as $nn=>$tmpValue1)
	{
		$tmpWord_2=null;
		if(is_array($tmpValue1))
		{
			$tmpWord_3=null;

			foreach($tmpValue1 as $nv=>$tmpValue2)
			{
				$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
				define('__'.strtoupper($tmpWord_3), $tmpValue2);
			}
		}
		else
		{
			$tmpWord_2=$tmpWord.$wordSep.$nn;
			define('__'.strtoupper($tmpWord_2), $tmpValue1);
		}
		$contL++;
	}
}
