/*
 * @package AJAX_Chat
 * @author Sebastian Tschan
 * @copyright (c) Sebastian Tschan
 * @license Modified MIT License
 * @link https://blueimp.net/ajax/
 */

// Overriding client side functionality:
/*
// Example - Overriding the replaceCustomCommands method:
ajaxChat.replaceCustomCommands = function(text, textParts) {
	return text;
}
 */
//console.log(ajaxChat.userID," [custom AJAX] ", ajaxChat)

function customNewMsg(dateObject, userID, userName, userRole, messageID, messageText, channelID, ip)
{
	var chatAlertIconDisplay=parent.document.getElementById("chatAlertIcon").style.display;
	var chatInnerWindow=parent.document.getElementById("chatInnerWindow").offsetLeft == 0 ? 0 : 1;
	// si ESTA OCULTA LA CENTANA DEL CHAT DEL BACK-OFFICE, SE NOTIFICA UN NUEVO MENSAJE EN ENTRADA
	if(messageParts != null && (chatAlertIconDisplay == "none" && chatInnerWindow == 0))
		parent.setInnerChatInfo(1,userName,messageText,channelID);
	else
	{
		parent.updateTopChatInfo(userName,messageText,channelID);
	}

//console.log(ajaxChat.userID," [custom AJAX] ", ajaxChat)
//console.log(chatInnerWindow,"\n userID = "+userID+",\n userName = "+userName+",\n userRole = "+userRole+",\n messageID = "+messageID)
}