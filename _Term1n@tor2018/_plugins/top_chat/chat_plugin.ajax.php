<?php
/**
 * PLUG IN SITE AJAX CHAT INTEGRATION
 * AJAX FUNCTIONS: accion VALUES
 * 
 * 10: CHAT VOLUME SET
 * 12: STORE USER MODAL CHAT DISPLAY STATUS
 * 
 * m.f. 2017.05.15
 */

header('Content-Type: text/html; charset=utf-8');
@session_start();

if(!isset($_SERVER['HTTP_REFERER']) && basename($_SERVER['SCRIPT_NAME']) != 'index.php'){include('strange_errors.php');die();}	//	prevent direct file access

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_REQUEST;

include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');

$currLang=$_SESSION['userAdmin']['pref_']['ref_lang'] ? strtolower($_SESSION['userAdmin']['pref_']['ref_lang']) : 'usa';

if(file_exists($arrPluginAddOns[$arraExtVars['plugInNm']]['dirPlugin'].'lang/lang_'.$currLang.'.php'))	// LOCAL PLUGIN TRANSLATIONS
	include($arrPluginAddOns[$arraExtVars['plugInNm']]['dirPlugin'].'lang/lang_'.$currLang.'.php');

$response['success']=false;

if(!isset($_SERVER['HTTP_REFERER']))
{
  $_REQUEST['txtStrangeError']='direct script access ERROR';
  include('strange_errors.php');
  die();
}


/**
 * CHAT VOLUME SET
 */
if($arraExtVars['accion'] == 10)
{
	include($dirModAdmin.'mod_bbdd.class.php');
	include($arrPluginAddOns[$arraExtVars['plugInNm']]['dirPlugin'].'_class/model.class.php');
	$plugInChat=new PluginTopChat($db_type, $db_host, $db_user, $db_pass, $db_name, $configDataList, $port);
	$resConnexion=$plugInChat->connectDB();
	$resSql=$plugInChat->storeVolumeUser($arraExtVars['tblNameOnline'], $arraExtVars['userID'], $arraExtVars['volumeLevel']);
  $response['success']=$resSql;
  $res=json_encode($response);
  die($res);
}

/**
 * STORE USER MODAL CHAT DISPLAY STATUS
 */
if($arraExtVars['accion'] == 12)
{
  include($web_path.'_inc/login_.class.php');
  $objeto=new LoginCustomer($sql);
  $res=$objeto->updateModalChatStatus($_SESSION['userAdmin']['userId'], 0);
//echo $res.'] (2)---><hr><pre>';print_r($_REQUEST);echo"\nobjeto=";print_r($objeto);echo"\nSESSION=";print_r($_SESSION);echo'</pre>';die(0);
//echo "\n";print_r($_REQUEST);
//echo $web_path."\n";print_r($sql);die();
  $response['success']=$res;
  $res=json_encode($response);
  die($res);
}

//include($web_path.'languages/config.php');
/** END COMMON INCLUDES & ASSETS **/


//		header('Location:login_user.php?usuario='.$res.'&password='.$arraExtVars['passCheck']);