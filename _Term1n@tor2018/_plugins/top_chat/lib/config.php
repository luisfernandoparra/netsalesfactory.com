<?php
/*
 * @package AJAX_Chat
 * @author Sebastian Tschan
 * @copyright (c) Sebastian Tschan
 * @license Modified MIT License
 * @link https://blueimp.net/ajax/
 */
$isPhp7=strpos($_SERVER['SERVER_SOFTWARE'], '/7');

// Define AJAX Chat user roles:
define('AJAX_CHAT_BANNED',6);
define('AJAX_CHAT_CUSTOM',5);
define('AJAX_CHAT_CHATBOT',4);
define('AJAX_CHAT_ADMIN',3);
define('AJAX_CHAT_MODERATOR',2);
define('AJAX_CHAT_USER',1);
define('AJAX_CHAT_GUEST',0);

$chat['server']='178.79.131.159';
$chat['user']='afroditadb';
$chat['pass']='4fr0D1taS3X@';
$chat['base']='ganatuspremios_6_0';
$chat['port']='1312';
$chat['type']='mysqli';

// AJAX Chat config parameters:
$config=array();

if(substr($_SERVER['SERVER_ADDR'],0,8)=='192.168.' || substr($_SERVER['SERVER_ADDR'],0,10)=='192.168.2.' || $_SERVER['SERVER_ADDR']=='localhost' || $_SERVER['SERVER_ADDR']=='139.162.246.12')	// local
{
	$chat['server']='localhost';
	$chat['user']='user_connect';
	$chat['pass']='L1nK4me@!';
	$chat['port']='3306';
	$chat['base']=$isPhp7 ? 'ganatuspremios_6_0' : 'ganatuspremios_6_0';;
}
else
{
	$chat['server']='178.79.159.246';
	$chat['user']='user_connect';
	$chat['pass']='L1nK4me@!';
	$chat['base']='netsalesfactory';
	$chat['port']='3306';
}

if($_SERVER['SERVER_ADDR'] === '192.168.2.102')	 // LOCAL MARIO
{
	$chat['server']='localhost';
	$chat['user']='root';
	$chat['pass']='';
	$chat['base']=$isPhp7 ? 'ganatuspremios_6_0' : 'ganatuspremios_6_0';;
	$chat['port']='3306';
}

// Database connection values:
$config['dbConnection']=array();
$config['dbConnection']['host']=$chat['server'];
$config['dbConnection']['user']=$chat['user'];
$config['dbConnection']['pass']=$chat['pass'];
$config['dbConnection']['name']=$chat['base'];
$config['dbConnection']['port']=$chat['port'];

// Database type:
//$config['dbConnection']['type']='mysqli';
$config['dbConnection']['type']=$chat['type'];

// Database link:
$config['dbConnection']['link']=null;

// Database table names:
//$config['dbTableNames']=array();
$config['dbTableNames']['online']='system_ajax_chat_online';
$config['dbTableNames']['messages']='system_ajax_chat_messages';
$config['dbTableNames']['bans']='system_ajax_chat_bans';
$config['dbTableNames']['invitations']='system_ajax_chat_invitations';

// Available languages:
$config['langAvailable']=array('ca','de','en','es','fr','it','gl','pt-pt','sk','sl','tr','uk');
// Default language:
$config['langDefault']='it';
// Language names (each languge code in available languages must have a display name assigned here):
$config['langNames']=array(
	'ca'=>'Català', 'de'=>'Deutsch', 'en'=>'English', 'es'=>'Español', 'fr'=>'Français', 'gl'=>'Galego', 'it'=>'Italiano',
	'nl'=>'Nederlands', 'nl-be'=>'Nederlands (België)', 'no'=>'Norsk', 'pl'=> 'Polski', 'pt-br'=>'Português (Brasil)', 'pt-pt'=>'Português (Portugal)', 'sk'=> 'Slovenčina', 'sl'=>'Slovensko', 'sr'=>'Srpski', 'sv'=> 'Svenska', 'tr'=>'Türkçe', 'uk'=>'Українська');

// Available styles:
$config['styleAvailable']=array('beige','backOffice','black','grey','Oxygen','Lithium','Sulfur','Cobalt','Mercury','Uranium','Pine','Plum','prosilver','Core','MyBB','vBulletin','XenForo');
$config['styleDefault']='prosilver';

// The encoding used for the XHTML content:
$config['contentEncoding']='UTF-8';
// The encoding of the data source, like userNames and channelNames:
$config['sourceEncoding']='UTF-8';
// The content-type of the XHTML page (e.g. "text/html", will be set dependent on browser capabilities if set to null):
$config['contentType']=null;

// Session name used to identify the session cookie:
//$config['sessionName']='ajax_chat_mf';
$config['sessionName'] = 'PHPSESSID';
// Prefix added to every session key:
$config['sessionKeyPrefix']='ajaxChat';
// The lifetime of the language, style and setting cookies in days:
$config['sessionCookieLifeTime']=365;
// The path of the cookies, '/' allows to read the cookies from all directories:
$config['sessionCookiePath']='/';
// The domain of the cookies, defaults to the hostname of the server if set to null:
$config['sessionCookieDomain']=null;
// If enabled, cookies must be sent over secure (SSL/TLS encrypted) connections:
$config['sessionCookieSecure']=null;

// Default channelName used together with the defaultChannelID if no channel with this ID exists:
$config['defaultChannelName']='Public';
// ChannelID used when no channel is given:
$config['defaultChannelID']=0;
// Defines an array of channelIDs (e.g. array(0, 1)) to limit the number of available channels, will be ignored if set to null:
$config['limitChannelList']=null;

// UserID plus this value are private channels (this is also the max userID and max channelID):
$config['privateChannelDiff']=500000000;
// UserID plus this value are used for private messages:
$config['privateMessageDiff']=1000000000;

// Enable/Disable private Channels:
$config['allowPrivateChannels']=true;
// Enable/Disable private Messages:
$config['allowPrivateMessages']=true;

// Private channels should be distinguished by either a prefix or a suffix or both (no whitespace):
$config['privateChannelPrefix']='[';
// Private channels should be distinguished by either a prefix or a suffix or both (no whitespace):
$config['privateChannelSuffix']=']';

// If enabled, users will be logged in automatically as guest users (if allowed), if not authenticated:
$config['forceAutoLogin']=false;

// Defines if login/logout and channel enter/leave are displayed:
$config['showChannelMessages']=true;

// If enabled, the chat will only be accessible for the admin:
$config['chatClosed']=false;
// Defines the timezone offset in seconds (-12*60*60 to 12*60*60) - if null, the server timezone is used:
$config['timeZoneOffset']=null;
// Defines the hour of the day the chat is opened (0 - closingHour):
$config['openingHour']=0;
// Defines the hour of the day the chat is closed (openingHour - 24):
$config['closingHour']=24;
// Defines the weekdays the chat is opened (0=Sunday to 6=Saturday):
$config['openingWeekDays']=array(0,1,2,3,4,5,6);

// Enable/Disable guest logins: ************************* SOLO DESACTIVAR CUANDO LOGIN PARALELO ESTE OK ******************************************
$config['allowGuestLogins']=true;
// Enable/Disable write access for guest users - if disabled, guest users may not write messages:
$config['allowGuestWrite']=true;
// Allow/Disallow guest users to choose their own userName:
$config['allowGuestUserName']=true;
// Guest users should be distinguished by either a prefix or a suffix or both (no whitespace):
$config['guestUserPrefix']='(';
// Guest users should be distinguished by either a prefix or a suffix or both (no whitespace):
$config['guestUserSuffix']=')';
// Guest userIDs may not be lower than this value (and not higher than privateChannelDiff):
$config['minGuestUserID']=400000000;

// Allow/Disallow users to change their userName (Nickname):
$config['allowNickChange']=false;
// Changed userNames should be distinguished by either a prefix or a suffix or both (no whitespace):
$config['changedNickPrefix']='(';
// Changed userNames should be distinguished by either a prefix or a suffix or both (no whitespace):
$config['changedNickSuffix']=')';

// Allow/Disallow registered users to delete their own messages:
$config['allowUserMessageDelete']=true;

// The userID used for ChatBot messages:
$config['chatBotID']=2147483647;
//$config['chatBotID']=999999;
// The userName used for ChatBot messages
$config['chatBotName']='usuario';
$config['chatBotName']='system-msg';

// Minutes until a user is declared inactive (last status update) - the minimum is 2 minutes:
$config['inactiveTimeout']=2;
// Interval in minutes to check for inactive users:
$config['inactiveCheckInterval']=10;

// Defines if messages are shown which have been sent before the user entered the channel:
$config['requestMessagesPriorChannelEnter']=true;
// Defines an array of channelIDs (e.g. array(0, 1)) for which the previous setting is always true (will be ignored if set to null):
$config['requestMessagesPriorChannelEnterList']=null;
// Max time difference in hours for messages to display on each request:
$config['requestMessagesTimeDiff']=24;
// Max number of messages to display on each request:
$config['requestMessagesLimit']=100;

// Max users in chat (does not affect moderators or admins):
$config['maxUsersLoggedIn']=100;
// Max userName length:
$config['userNameMaxLength']=16;
// Max messageText length:
$config['messageTextMaxLength']=5040;
// Defines the max number of messages a user may send per minute:
$config['maxMessageRate']=40;

// Defines the default time in minutes a user gets banned if kicked from a moderator without ban minutes parameter:
$config['defaultBanTime']=5;

// Argument that is given to the handleLogout JavaScript method:
$config['logoutData']='./?logout=true';

// If true, checks if the user IP is the same when logged in:
$config['ipCheck']=true;

// Defines the max time difference in hours for logs when no period or search condition is given:
$config['logsRequestMessagesTimeDiff']=1;
// Defines how many logs are returned on each logs request:
$config['logsRequestMessagesLimit']=10;

// Defines the earliest year used for the logs selection:
$config['logsFirstYear']=date('Y')-1;

// Defines if old messages are purged from the database:
$config['logsPurgeLogs']=false;
// Max time difference in days for old messages before they are purged from the database:
$config['logsPurgeTimeDiff']=365;

// Defines if registered users (including moderators) have access to the logs (admins are always granted access):
$config['logsUserAccess']=true;
// Defines a list of channels (e.g. array(0, 1)) to limit the logs access for registered users, includes all channels the user has access to if set to null:
$config['logsUserAccessChannelList']=null;

// Defines if the socket server is enabled:
$config['socketServerEnabled']=false;
// Defines the hostname of the socket server used to connect from client side (the server hostname is used if set to null):
$config['socketServerHost']=null;
// Defines the IP of the socket server used to connect from server side to broadcast update messages:
$config['socketServerIP']='127.0.0.1';
// Defines the port of the socket server:
$config['socketServerPort']=1935;
// This ID can be used to distinguish between different chat installations using the same socket server:
$config['socketServerChatID']=0;

//echo $_SERVER['SERVER_ADDR'].'<pre>';print_r($config);echo'</pre>';die();