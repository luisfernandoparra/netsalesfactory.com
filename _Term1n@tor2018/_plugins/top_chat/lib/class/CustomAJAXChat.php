<?php
/*
 * @package AJAX_Chat
 * @author Sebastian Tschan
 * @copyright (c) Sebastian Tschan
 * @license Modified MIT License
 * @link https://blueimp.net/ajax/
 */
@session_start();
@header('Content-Type: text/html; charset=utf-8');

class CustomAJAXChat extends AJAXChat
{
	// Returns an associative array containing userName, userID and userRole
	// Returns null if login is invalid
	function getValidLoginUserData()
	{
		$customUsers=$this->getCustomUsers();

		if($this->getRequestVar('password'))
		{
			// Check if we have a valid registered user:
			$userName=$this->getRequestVar('userName');
			$userName=$this->convertEncoding($userName, $this->getConfig('contentEncoding'), $this->getConfig('sourceEncoding'));
			$password=$this->getRequestVar('password');
			$password=$this->convertEncoding($password, $this->getConfig('contentEncoding'), $this->getConfig('sourceEncoding'));

			foreach($customUsers as $key=>$value)
			{
				if(($value['userName'] == $userName) && ($value['password'] == $password))
				{
					$userData=array();
					$userData['userID']=isset($_SESSION['userAdmin']['userId']) ? $_SESSION['userAdmin']['userId'] : $key;
					$userData['userName']=$this->trimUserName($value['userName']);
					$userData['userRole']=$value['userRole'];
					return $userData;
				}
			}
			
			return null;
		}else{
			// Guest users:
			return $this->getGuestUser();
		}
	}

	// Store the channels the current user has access to
	// Make sure channel names don't contain any whitespace
	function &getChannels()
	{
		if($this->_channels === null)
		{
			$this->_channels=array();
			
			$customUsers=$this->getCustomUsers();
//echo $this->getUserRole();
			// Get the channels, the user has access to:
			if($this->getUserRole() == AJAX_CHAT_GUEST){
				$validChannels=$customUsers[0]['channels'];
			}else{
				$validChannels=$customUsers[$this->getUserID()]['channels'];
			}
			
			// Add the valid channels to the channel list (the defaultChannelID is always valid):
			foreach($this->getAllChannels() as $key=>$value)
			{
				if ($value == $this->getConfig('defaultChannelID'))
				{
					$this->_channels[$key]=$value;
					continue;
				}
				// Check if we have to limit the available channels:
				if($this->getConfig('limitChannelList') && !in_array($value, $this->getConfig('limitChannelList')))
				{
					continue;
				}
				if(in_array($value, $validChannels))
				{
					$this->_channels[$key]=$value;
				}
			}
		}
		return $this->_channels;
	}

	// Store all existing channels
	// Make sure channel names don't contain any whitespace
	function &getAllChannels()
	{
		if($this->_allChannels === null)
		{
			// Get all existing channels:
			$customChannels=$this->getCustomChannels();
			
			$defaultChannelFound=false;
			
			foreach($customChannels as $name=>$id)
			{
				$this->_allChannels[$this->trimChannelName($name)]=$id;
				if($id == $this->getConfig('defaultChannelID')){
					$defaultChannelFound=true;
				}
			}
			
			if(!$defaultChannelFound)
			{
				// Add the default channel as first array element to the channel list
				// First remove it in case it appeard under a different ID
				unset($this->_allChannels[$this->getConfig('defaultChannelName')]);
				$this->_allChannels=array_merge(
					array(
						$this->trimChannelName($this->getConfig('defaultChannelName'))=>$this->getConfig('defaultChannelID')
					),
					$this->_allChannels
				);
			}
		}
		return $this->_allChannels;
	}

	function &getCustomUsers()
	{
		$users=null;
		$decoded=null;
		require(AJAX_CHAT_PATH.'lib/data/users.php');

		if(isset($_REQUEST['tmpPath']))
		{
			require_once($_REQUEST['tmpPath'].'encryption.class.php');
			$converter=new Encryption;
			$decoded=$converter->decode($_SESSION['userAdmin']['currPass']);
		}

		$users[$nnChat]['userRole']=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD ? AJAX_CHAT_ADMIN : AJAX_CHAT_USER;	// ESTABLECER EL ROL DEL CHAT SEGUN NIVEL DE ACCESO DE USUARIO ACTUAL
		$users[$nnChat]['userName']=$_SESSION['userAdmin']['userName'];
		$users[$nnChat]['password']=$decoded;
		$users[$nnChat]['channels']=array(0,1);
		$nnChat++;
		return $users;
	}
	
	function getCustomChannels()
	{
		// List containing the custom channels:
		$channels=null;
		require(AJAX_CHAT_PATH.'lib/data/channels.php');
		// Channel array structure should be:
		// ChannelName => ChannelID
		return array_flip($channels);
	}

}