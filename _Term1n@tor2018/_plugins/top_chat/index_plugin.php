<?php
/**
 * PLUG-IN TOP NOTES
 *
 * M.F. 2017.11.18
 *

-- --------------------------------------------------------

--
-- Table structure for table `system_ajax_chat_bans`
--

CREATE TABLE IF NOT EXISTS `system_ajax_chat_bans` (
  `userID` int(10) unsigned NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin NOT NULL,
  `dateTime` datetime NOT NULL,
  `ip` varbinary(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `system_ajax_chat_invitations`
--

CREATE TABLE IF NOT EXISTS `system_ajax_chat_invitations` (
  `userID` int(10) unsigned NOT NULL,
  `channel` int(10) unsigned NOT NULL,
  `dateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `system_ajax_chat_messages`
--

CREATE TABLE IF NOT EXISTS `system_ajax_chat_messages` (
  `id` int(11) NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin NOT NULL,
  `userRole` int(1) NOT NULL,
  `channel` int(10) unsigned NOT NULL,
  `dateTime` datetime NOT NULL,
  `ip` varbinary(16) NOT NULL,
  `text` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `system_ajax_chat_online`
--

CREATE TABLE IF NOT EXISTS `system_ajax_chat_online` (
  `userID` int(10) unsigned NOT NULL,
  `userName` varchar(64) COLLATE utf8_bin NOT NULL,
  `userRole` int(1) NOT NULL,
  `channel` int(10) unsigned NOT NULL,
  `dateTime` datetime NOT NULL,
  `ip` varbinary(16) NOT NULL,
  `sounds_volume` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `system_ajax_chat_bans`
--
ALTER TABLE `system_ajax_chat_bans`
  ADD PRIMARY KEY (`userID`),
  ADD KEY `userName` (`userName`),
  ADD KEY `dateTime` (`dateTime`);

--
-- Indexes for table `system_ajax_chat_invitations`
--
ALTER TABLE `system_ajax_chat_invitations`
  ADD PRIMARY KEY (`userID`,`channel`),
  ADD KEY `dateTime` (`dateTime`);

--
-- Indexes for table `system_ajax_chat_messages`
--
ALTER TABLE `system_ajax_chat_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `message_condition` (`id`,`channel`,`dateTime`),
  ADD KEY `dateTime` (`dateTime`);

--
-- Indexes for table `system_ajax_chat_online`
--
ALTER TABLE `system_ajax_chat_online`
  ADD PRIMARY KEY (`userID`),
  ADD KEY `userName` (`userName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `system_ajax_chat_messages`
--
ALTER TABLE `system_ajax_chat_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

 */
@session_start();
include($arrPluginAddOns[$plugInNm]['folderPlugin'].'lang/lang_'.strtolower($_SESSION['userAdmin']['pref_']['ref_lang']).'.php');
include($arrPluginAddOns[$plugInNm]['folderPlugin'].'_class/model.class.php');
$localLogoutRole=__LEVEL_ACCESS_MASTER;

$tmpRole=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? 3 : 1;
?>

<link href="<?=$arrPluginAddOns[$plugInNm]['baseFolderPlugin']?>plugin.css" rel="stylesheet">
<li class="dropdown c-white">
	<a data-toggle="dropdown" href="#null" class="<?=$foreAutoColor?>">
		<i id="chatAlertIcon" class="him-icon fa fa-comment p-l-5 p-r-5 c-red bgm-yellow" style="display:none;"></i>
		<i id="chatStdIcon" class="him-icon fa fa-comments p-l-5 p-r-5" title="<?=__PLUGTOPCHAT_HEADER_TITLETXT?>" data-placement="bottom" data-trigger="hover" data-trigger="hover" data-toggle="tooltip"></i>
	</a>
	<ul class="dropdown-menu pull-right chatMainDiv m-b-0 p-b-0 <?=$_SESSION['userAdmin']['pref_']['top_header_theme_bckCol']?>" id="chatInnerWindow">
		<div class="pull-right p-static m-r-20 m-t-5 headerChatButtons">
			<button class="btn btn-xs reloadChatWindow" ><?=__COMMON_GENERICTEXT_RELOADTXT?></button>&nbsp;
			<button class="btn btn-xs closeChatWindow" ><?=__COMMON_BTN_CLOSE?></button>
		</div>
		<li class="card-light">
			<h4 class="m-b-15 lg-toolbar top-chat pull-left m-l-10 c-white"><i class="fa fa-comments f-20"></i>&nbsp;&nbsp;<?=__PLUGTOPCHAT_HEADER_TITLETXT?></h4>
		</li>

		<div style="dsplay:inline-block;width:100%;max-heihgt:50vh!important;overflow:auto;text-align:center;">
			<iframe name='ifrChatContainer' id='ifrChatContainer' src='<?=$arrPluginAddOns[$plugInNm]['baseFolderPlugin']?>?channelID=0&basePlugin=<?=$arrPluginAddOns[$plugInNm]['folderPlugin']?>&userName=<?=$_SESSION['userAdmin']['name']?>&userID=<?=(int)$_SESSION['userAdmin']['userId']?>&userRole=<?=$tmpRole?>' style='border:0px solid #999;width:96%!important;height:50vh!important;' width='100%' ></iframe>

<?php
/**
 * &ajax=false&lastId=<?=(int)$_SESSION['userAdmin']['userId']?>
 */
//echo '<pre>';
//print_r($arrPluginAddOns[$plugInNm]['bbddPlugin']);
//echo '</pre>';

//echo $arrPluginAddOns[$plugInNm]['baseFolderPlugin'];
//include($arrPluginAddOns[$plugInNm]['folderPlugin'].'base/index.php');
//echo $arrPluginAddOns[$plugInNm]['folderPlugin'].'base/index.php';
//echo '<hr>'.dirname($_SERVER['SCRIPT_FILENAME']).'/_plugins/top_chat/base/';
$otherFolder=null;
$outJs='';
$outJsReady='';
//unset($_SESSION['topChat']);
$_SESSION['topChat']['userTable']=__QUERY_TABLES_PREFIX.'adminusers';
$_SESSION['topChat']['fieldIdName']='userId';
$_SESSION['topChat']['fieldPassName']='password';

//ajax=false&lastId=0&getInfos=userID,userName,Role,channelID,channelName&
//echo $plugInNm;
?>
<script type="application/javascript" charset="utf-8">
var chatIdUser="<?=isset($_SESSION['userAdmin']['userId']) && $_SESSION['userAdmin']['userId'] ? $_SESSION['userAdmin']['userId'] : rand(20000,100000)?>";
//console.log("chatIdUser = ",chatIdUser);
//console.log("===> ",document.getElementById("ifrChatContainer").contentWindow.document.getElementById("logoutButton"))
//console.log("===> ",window.frames["ifrChatContainer"].contentWindow.document.getElementById("logoutButton"))
/**
 */
<?php
if($_SESSION['userAdmin']['roleId'] >= $localLogoutRole)
{
?>
var tmpVar;
function enableChatLogoutButton()	// HABILITAR EL BOTON DE SALIDA DEL CHAT PARA EL USUARIO ACTUAL
{
	var theFrameX=document.getElementsByName("ifrChatContainer")[0];
	var theFrameDocument=(theFrameX.contentDocument || theFrameX.contentWindow.document);
	tmpVar=theFrameDocument.getElementById("logoutButton");
	if(tmpVar !== null)
		tmpVar.style.display="inline-block";
	return tmpVar;
}
<?php
}

?>

function hideChat()
{
  $(".closeBoxChat").click();
}

function setVolumeAudioChat(userID, userName, volume)
{
  $.ajax({
    url:"<?=$arrPluginAddOns[$plugInNm]['baseFolderPlugin']?>chat_plugin.ajax.php", method:"post", dataType:"json",
    data:{accion:10,plugInNm:"<?=$plugInNm?>",userID:userID,userName:userName,volumeLevel:volume,tblNameOnline:"<?=$arrPluginAddOns[$plugInNm]['bbddPlugin']['tblNameOnline']?>"}, cache:false, async:true,
    success:function(response)
    {
			if(response.success)
			{
				$(".headerChatButtons").prepend("<span class='p-l-10 p-r-10 c-green highLightProcess'><i class='fa fa-check c-green f-16'></i>&nbsp;<?=__COMMON_GENERICTEXT_OKTXT?></span>");
				$(".highLightProcess").delay(2000).fadeOut().delete();
				return;
			}

			$(".headerChatButtons").prepend("<span class='p-l-10 p-r-10 c-red highLightProcess'><i class='fa fa-close c-red f-16'></i>&nbsp;<?=__COMMON_GENERICTEXT_ERROR?></span>");
			$(".highLightProcess").delay(3000).fadeOut().delete();
    },
    error:function(response){return "<br /><br /><center style=color:red;>ERRORE INATTESO!</center>";}
  })

}

function setInnerChatInfo(status,userName,messageText,channelId)
{
	$("#chatStdIcon").fadeOut("fast",function(){
		$("#chatAlertIcon").fadeIn().attr("title",""+userName+"\n<?=__PLUGTOPCHAT_INEWMSG_TITLETXT?>\n"+messageText);
		$(".top-chat").html("<?=__PLUGTOPCHAT_HEADER_TITLETXT?><?=__PLUGTOPCHAT_HEADER_LASTUSERMSGTXT?><span class='c-blue'>"+userName+(channelId > 0 ? "</span> [privado]" : "</span>"));
	});
}

function updateTopChatInfo(userName,messageText,channelId)
{
	$(".top-chat").html("<?=__PLUGTOPCHAT_HEADER_TITLETXT?><?=__PLUGTOPCHAT_HEADER_LASTUSERMSGTXT?><span class='c-blue f-400'>"+userName+(channelId > 0 ? " </span>[privado]" : "</span>"));
}

$(document).ready(function(){
//	$(".containerMaster").append("<div id='chatPrivateRoom' class='liveChatMF modeChat<?=$_SESSION['userAdmin']['enableSiteChat']?>'><iframe src='<?=$arrPluginAddOns[$plugInNm]['baseFolderPlugin']?>chat_site/?channelID=0&userName=<?=$_SESSION['userAdmin']['name']?>&logout=true' style='border:1px solid #999;width:100%!important;height:100%!important;' width='100%' id='ifrChatContainer' ></iframe><div class='closeBoxChat' title='Nascondere il Chat'>X</div></div>");

  $(".closeBoxChat").click(function(){
//console.log("############ GRABAR PARAMETROS DEL USUARIO EN TABLA DE CONFIG???? ######")
    $.ajax({
      url:"<?=$arrPluginAddOns[$plugInNm]['baseFolderPlugin']?>chat_plugin.ajax.php", method:"post", dataType:"json",
      data:{accion:12}, cache:false, async:true,
      success:function(response)
      {
//console.log(response)
      },
      error:function(response){return "<br /><br /><center style=color:red;>ERRORE INATTESO!</center>";}
    })
    $("#chatPrivateRoom").fadeOut();
  })

	$(".closeChatWindow").click(function(){
		$("body").click();
	});

	$(".reloadChatWindow").click(function(){
		document.getElementById('ifrChatContainer').src=document.getElementById('ifrChatContainer').src;
	});

	$("#chatAlertIcon").click(function(){
		$(this).hide();
		$("#chatStdIcon").show();
	});

	$("#chatAlertIcon").mouseleave(function(){
		$(this).fadeOut(function(){
			$("#chatStdIcon").fadeIn();
		});
	});

<?php
if($_SESSION['userAdmin']['roleId'] >= $localLogoutRole)	// SOLO PARA USUARIOS ALTO NIVEL
	$outJsReady.='setTimeout("enableChatLogoutButton();",3000);';

echo $outJsReady;

?>
});
</script>
<?php
//echo '<pre>';
//print_r($_SESSION['userAdmin']);
//echo '</pre>';
?>
			</div>
		</li>
	</ul>
</li>
