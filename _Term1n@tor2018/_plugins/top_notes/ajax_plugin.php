<?php
/**
 * SPECIFIC SCRIPT ATCIONS
 *
 *
 */
header('Content-Type: text/html; charset=utf-8');
@session_start();

if(!isset($_SERVER['HTTP_REFERER']) && basename($_SERVER['SCRIPT_NAME']) != 'index.php'){include('strange_errors.php');die();}	//	prevent direct file access

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_REQUEST;

include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');

$currLang=$_SESSION['userAdmin']['pref_']['ref_lang'] ? strtolower($_SESSION['userAdmin']['pref_']['ref_lang']) : 'usa';
//echo $arrPluginAddOns['topNotes']['dirPlugin'];die();
if(file_exists($arrPluginAddOns['topNotes']['dirPlugin'].'lang/lang_'.$currLang.'.php'))	// LOCAL PLUGIN TRANSLATIONS
	include($arrPluginAddOns['topNotes']['dirPlugin'].'lang/lang_'.$currLang.'.php');

$response['query']=array();
//echo $arrPluginAddOns['topNotes']['dirPlugin'].'_class/model.class.php'."\n";print_r($arraExtVars);print_r($arrPluginAddOns['topNotes']);die($arrPluginAddOns['topNotes']['dirPlugin'].'lang/lang_'.strtolower($_SESSION['userAdmin']['pref_']['ref_lang']).'.php');

if($arraExtVars['action'] === 'storeUserNote')
{
	$response['success']=false; $resSql=false;

	if((int)$arraExtVars['moduleId'] != $arraExtVars['moduleId'])
	{
		$response['msgPainTxt']="\n???";
		$response['errorText']='Referencia m&oacute;dulo err&oacute;nea!'."\n\n".'operaci&oacute;n cancelada';
		$response['success']=false;
		$res=json_encode($response);
		die($res);
	}

	include($arrPluginAddOns['topNotes']['dirPlugin'].'lang/lang_'.strtolower($_SESSION['userAdmin']['pref_']['ref_lang']).'.php');
	include($dirModAdmin.'mod_bbdd.class.php');
	include($arrPluginAddOns['topNotes']['dirPlugin'].'_class/model.class.php');
	$plugInNotes=new PluginTopNotes($db_type, $db_host, $db_user, $db_pass, $db_name, $configDataList, $port);
	$resConnexion=$plugInNotes->connectDB();
	$resSql=$plugInNotes->storeNote($arrPluginAddOns['topNotes']['tblName'], $_SESSION['userAdmin']['userId'], $arraExtVars['moduleId'], $arraExtVars['recordId'], $arraExtVars['htmlNote']);
//echo $resSql."\n";print_r($arraExtVars);print_r($arrPluginAddOns);print_r($plugInNotes);die('END AJAX FILE !!!!!!!!!!!!!!!!!!!');die();

	if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD)
  {
    $response['query']['sql'][]=$data->query;
		$response['query']['res'][]=$resSql;
  }

	if($resSql) // SQL SUCCESS
	{
		$response['msgText']='<h2 class="c-white">'.($arraExtVars['recordId'] ? __PLUGTOPNOTES_AJAXMSG_UPDATEDNOTE : __COMMON_GENERICTEXT_NEWRECORDTXT.' '.__COMMON_GENERICTEXT_OKTXT).'</h2><br />';
		$response['success']=true;
		$elementDetail=($arraExtVars['recordId'] ? 'noteId: '.$arraExtVars['recordId'] : 'newNoteStored');
		$plugInNotes->logBackOffice(0,null,$elementDetail);
	}
	else	// SQL ERROR
	{
		$response['errorText']=''.__PLUGTOPNOTES_AJAXMSGERROR_NOUUPDATED.''."\n\n".'<br />';
		$response['errorText'].=($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD) ? '<br /><br />'.$plugInNotes->query : '';
		$response['success']=false;
	}

	$res=json_encode($response);
	die($res);
}

if($arraExtVars['action'] === 'deleteNote')
{
	$response['success']=false; $resSql=false;

	if((int)$arraExtVars['recordId'] != $arraExtVars['recordId'])
	{
		$response['msgPainTxt']="\n???";
		$response['errorText']='Referencia m&oacute;dulo err&oacute;nea!'."\n\n".'operaci&oacute;n cancelada';
		$response['success']=false;
		$res=json_encode($response);
		die($res);
	}

	include($arrPluginAddOns['topNotes']['dirPlugin'].'lang/lang_'.strtolower($_SESSION['userAdmin']['pref_']['ref_lang']).'.php');
	include($dirModAdmin.'mod_bbdd.class.php');
	include($arrPluginAddOns['topNotes']['dirPlugin'].'_class/model.class.php');
	$plugInNotes=new PluginTopNotes($db_type, $db_host, $db_user, $db_pass, $db_name, $configDataList, $port);
	$resConnexion=$plugInNotes->connectDB();
	$resSql=$plugInNotes->deleteNote($arrPluginAddOns['topNotes']['tblName'], $_SESSION['userAdmin']['userId'], $arraExtVars['recordId']);

	if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD)
  {
    $response['query']['sql'][]=$data->query;
		$response['query']['res'][]=$resSql;
  }

	if($resSql) // SQL SUCCESS
	{
		$response['msgText']='<h2 class="c-white">'.__PLUGTOPNOTES_AJAXMSG_DELETEDNOTE.'</h2><br />';
		$response['success']=true;
		$elementDetail='noteId: '.$arraExtVars['recordId'];
		$plugInNotes->logBackOffice(0,null,$elementDetail);
	}
	else	// SQL ERROR
	{
		$response['errorText']=''.__PLUGTOPNOTES_AJAXMSGERROR_NOUUPDATED.''."\n\n".'<br />';
		$response['errorText'].=($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD) ? '<br /><br />'.$plugInNotes->query : '';
		$response['success']=false;
	}

	$res=json_encode($response);
	die($res);
}

echo "\n";print_r($arraExtVars);die('END AJAX FILE !!!!!!!!!!!!!!!!!!!');
