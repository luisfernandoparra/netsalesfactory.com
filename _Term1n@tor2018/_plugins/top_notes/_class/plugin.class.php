<?php
/**
 * ELEMNTOS NECESARIOS PARA EL FUNCIONAMIENTO DE LAS NOTAS PERSONALES POR USUARIO
 * DEL BACK-OFFICE
 * 
 */
$plugInNotes=new PluginTopNotes($db_type, $db_host, $db_user, $db_pass, $db_name, $configDataList, $port);
$resConnexion=$plugInNotes->connectDB();

$arrModuleSettings=explode('|', $_REQUEST['optId']);
$refArrPos=count($arrModuleSettings)-1;
$currentModuleStr=end($arrModuleSettings);
$currentEndStr=strlen($currentModuleStr);
$currentModulePosId=strrpos($currentModuleStr, '_')+1;

if($currentEndStr === $currentModulePosId)
{
	$currentModuleId=substr($currentModuleStr,0,$currentModulePosId-1);
	$currentModulePosId=strrpos($currentModuleId, '_');
	$currentModuleId=substr($currentModuleId,$currentModulePosId+1);
}

$contentPlugIn=$plugInNotes->getUserNotes('system_plugin_top_user_notes', null, $_SESSION['userAdmin']['userId'], (int)$currentModuleId);

$tmpCGR='';
$tmpCTN=__COMMON_GENERICTEXT_NOTES;
$tmpCTU=__COMMON_BTN_SAVE;
$tmpCBN=__COMMON_BTN_CANCEL;
$tmpCBC=__COMMON_BTN_CLEAN;
$tmpERA=__COMMON_BTN_DELETE;
$tmpUPBFC=$_SESSION['userAdmin']['pref_']['btn_default_forCol'];
$tmpUPBBC=$_SESSION['userAdmin']['pref_']['btn_default_bckCol'];

$htmlWysiwigEditor="
<div class='modal fade p-t-10 p-l-10' id='htmlWysiEditBox' tabindex='-1' role='dialog' aria-hidden='true' data-backdrop='dynamic' data-keyboard='false'>
	<div class='modal-dialog modal-lg'>
		<div class='modal-content'>
			<div class='modal-header commonHeaderWysiEdit'>
				<h4 class='modal-title' style='color:#333;width:96%!important;'><span class='idTitleHtmlWysiEdit'></span></h4>

				<div id='alerts'></div>
			<div class='btn-toolbar' data-role='editor-toolbar' data-target='#editorWy'>
				<div class='btn-group'>
					<a class='btn dropdown-toggle' data-toggle='dropdown' title='Font'><i class='icon-font'></i><b class='caret'></b></a>
					<ul class='dropdown-menu'>
					</ul>
				</div>
				<div class='btn-group'>
					<a class='btn dropdown-toggle' data-toggle='dropdown' title='Font Size'><i class='icon-text-height'></i>&nbsp;<b class='caret'></b></a>
						<ul class='dropdown-menu'>
						<li><a data-edit='fontSize 5'><font size='5'>Huge</font></a></li>
						<li><a data-edit='fontSize 3'><font size='3'>Normal</font></a></li>
						<li><a data-edit='fontSize 1'><font size='1'>Small</font></a></li>
						</ul>
				</div>
				<div class='btn-group'>
					<a class='btn' data-edit='bold' title='Bold (Ctrl/Cmd+B)'><i class='icon-bold'></i></a>
					<a class='btn' data-edit='italic' title='Italic (Ctrl/Cmd+I)'><i class='icon-italic'></i></a>
					<a class='btn' data-edit='strikethrough' title='Strikethrough'><i class='icon-strikethrough'></i></a>
					<a class='btn' data-edit='underline' title='Underline (Ctrl/Cmd+U)'><i class='icon-underline'></i></a>
				</div>
				<div class='btn-group'>
					<a class='btn' data-edit='insertunorderedlist' title='Bullet list'><i class='icon-list-ul'></i></a>
					<a class='btn' data-edit='insertorderedlist' title='Number list'><i class='icon-list-ol'></i></a>
					<a class='btn' data-edit='outdent' title='Reduce indent (Shift+Tab)'><i class='icon-indent-left'></i></a>
					<a class='btn' data-edit='indent' title='Indent (Tab)'><i class='icon-indent-right'></i></a>
				</div>
				<div class='btn-group'>
					<a class='btn' data-edit='justifyleft' title='Align Left (Ctrl/Cmd+L)'><i class='icon-align-left'></i></a>
					<a class='btn' data-edit='justifycenter' title='Center (Ctrl/Cmd+E)'><i class='icon-align-center'></i></a>
					<a class='btn' data-edit='justifyright' title='Align Right (Ctrl/Cmd+R)'><i class='icon-align-right'></i></a>
					<a class='btn' data-edit='justifyfull' title='Justify (Ctrl/Cmd+J)'><i class='icon-align-justify'></i></a>
				</div>
				<div class='btn-group'>
				<a class='btn dropdown-toggle' data-toggle='dropdown' title='Hyperlink'><i class='icon-link'></i></a>
					<div class='dropdown-menu input-append'>
						<input class='span2' placeholder='URL' type='text' data-edit='createLink'/>
						<button class='btn' type='button'>Add</button>
					</div>
					<a class='btn' data-edit='unlink' title='Remove Hyperlink'><i class='icon-cut'></i></a>
				</div>

				<div class='btn-group'>
					<a class='btn' data-edit='undo' title='Undo (Ctrl/Cmd+Z)'><i class='icon-undo'></i></a>
					<a class='btn' data-edit='redo' title='Redo (Ctrl/Cmd+Y)'><i class='icon-repeat'></i></a>
				</div>
				<input type='hidden' data-edit='inserttext' id='voiceBtn' x-webkit-speech=''>
			</div>
				<div class='fltBtnClose'>
					<button class='btn bgm-gray btn-icon waves-effect waves-circle' data-dismiss='modal'><i class='zmdi zmdi-close'></i></button>
				</div>
			</div>

			<div id='editorWy'>
				".htmlentities($tmpCGR)."
			</div>

			<div class='modal-footer'>
				<span class='notedateView c-gray m-r-10'></span>
				<button type='button' class='btn btn-default $tmpUPBFC $tmpUPBBC deleteNoteButton'><i class='zmdi zmdi-delete'></i> $tmpERA</button>
				<button type='button' class='btn btn-default $tmpUPBFC $tmpUPBBC cleanWysiwigButton'><i class='zmdi zmdi-smartphone-erase'></i> $tmpCBC</button>
				<button data-id='0' type='button' class='btn btn-default $tmpUPBFC $tmpUPBBC updateWysiwigButton'>$tmpCTU</button>
				<button type='button' class='btn btn-default $tmpUPBFC $tmpUPBBC ' data-dismiss='modal'>$tmpCBN</button>
			</div>
		</div>
	</div>
</div>
";
//echo $plugInNotes->query;

$htmlSpecificLibraries.='<script src="'.$web_url.$adminFolder.'/_plugins/whysiwyg_tiny_editor/'.(isset($moduleSetting->arrFieldsFormModal[$key]['actionsForm']->wysiwigEditor['toolBar']) ? $moduleSetting->arrFieldsFormModal[$key]['actionsForm']->wysiwigEditor['toolBar'] : 'default_toolbar').$minExtension.'.js"></script>';
echo $htmlSpecificLibraries;
?>
<link href="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/bootstrap-wysiwyg.css" rel="stylesheet" />
<link href="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/google-code-prettify/prettify.css" rel="stylesheet" />
<link href="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/bootstrap-wysiwyg-awesome.css" rel="stylesheet" />

<script src="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/jquery.hotkeys.js"></script>
<script src="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/google-code-prettify/prettify.js"></script>
<script src="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/bootstrap-wysiwyg.min.js"></script>

<script type="application/javascript">

$(document).ready(function(){
	var baseFieldNameToUpddate="";
	var pluginFolder="<?=$arrPluginAddOns[$plugInNm]['baseFolderPlugin']?>";
	var tmpRexNoteId=0;
	var tmpModNoteId=1;
	var isNewNote=false;
	var positionUserNote=0;

	$(".newUserNote").click(function(){
		$('.notedateView').html("");
		tmpModNoteId=$(this).attr("data-moduleId");
		tmpModNoteContent="";
		tmpRexNoteId=0;
		$(this).attr("data-id",0);
		tmpModNoteContent="";
		$("#editorWy").html("");
		isNewNote=true;
		positionUserNote=$(this).attr("data-moduleid") > 1 ? 1 : 0
		$(".deleteNoteButton").hide()
		$(".idTitleHtmlWysiEdit").html("<b>"+$(this).parent().children(0).html()+(positionUserNote ? " <span class=\"c-white bgm-gray\">&nbsp;<?=$_REQUEST['modLabel']?>&nbsp;" : "")+"<span></b>");
	})

	$(".viewUserNotes").click(function(){
//console.log("--->viewUserNotes:: ",this.parentNode.getAttribute("data-module-pos"))
		$("body").click();
		$("h4[class*='modal-title c-green personalNotes']").html("<?=__PLUGINNOTES_MODALTITLE_TXT?> "+(this.parentNode.getAttribute("data-module-pos") == 1 ? "(<?=$_REQUEST['modLabel']?>)" : "(<?=__PLUGTOPNOTES_BTNVIEWGENERALNOTE_TXT?>)")+"<span class='pull-right m-r-10 f-11 c-gray'>("+this.parentNode.getAttribute("data-date")+")</span>");
		$('#userModuleNotes').modal('toggle');
		$(".contentUserNote").html("<span class='c-black' style=font-family:arial,verdana;>"+$(this).attr("data-content")+"</span>");
	})

	$(".editUserNote").click(function(){
		$('.notedateView').html("("+this.parentNode.parentNode.getAttribute("data-date")+")");
		isNewNote=false;
		positionUserNote=this.id == "moduleUserNote" ? 1 : 0
		$(".idTitleHtmlWysiEdit").html( positionUserNote ? "<b><?=__PLUGTOPNOTES_PREEDIT_TITLE.' <span class=\"c-white bgm-gray\">&nbsp;'.$_REQUEST['modLabel']?>'&nbsp;<span></b>" : "<?=__PLUGTOPNOTES_COMMONEDIT_TITLE?>");
		this.href="#htmlWysiEditBox";
		$('#htmlWysiEditBox').modal('toggle');
		$(".updateWysiwigButton").attr("data-id",$(this).data("id"));
		tmpRexNoteId=$(this).data("id");
		tmpModNoteId=$(this).attr("data-moduleId");
		tmpModNoteContent=$(this).attr("data-content");
		$("#editorWy").html($(this).attr("data-content"));
		$(".deleteNoteButton").show()
		return false;
	})

	$(".modalHtmlWysiEditBox").click(function(){
		baseFieldNameToUpddate=$(this).attr("data-field-name");
//		$(".idTitleHtmlWysiEdit").html("<b>"+$(this).parent().children(0).html()+"</b>");//$(this).attr("data-field-name")
		$('#htmlWysiEditBox').modal('toggle');
  });

	$(".updateWysiwigButton").click(function(){
		$.ajax({
			url:pluginFolder+"ajax_plugin.php",method:"post",dataType:"json",data:{action:"storeUserNote",moduleId:tmpModNoteId, recordId:tmpRexNoteId, htmlNote:$("#editorWy").html()}, cache:false, async:false
			,success:function(response){
				if(!response.success)
				{
					commonNotify('<b><?=__COMMON_GENERICTEXT_ERROR?></b>', '<br /><br /><?=__COMMON_GENERICTEXT_TXTSAVEERROR?><br />'+(response.errorText ? response.errorText : "contactar infomática"), undefined, "left", undefined, 'danger', "animated lightSpeedIn", "rotateOut",9000);
					return false;
				}
				var newHtmlNote=$("#editorWy").html();

				if(isNewNote)	// NOTICE FOR LINK ON NEW NOTE ISERTED
				{
					isNewNote=false;
					$(".newUserNote[data-moduleId="+tmpModNoteId+"]").parent().html("<a class='c-black viewUserNotes' href='#null' ><?=__PLUGTOPNOTES_JSNEWNOTEINSERTED_TXT?></a>");
					commonNotify('<?=__COMMON_NOTIFY_INFONOCONTENTSTITLE?>', '<br /><br /><?=__COMMON_NOTIFY_CORRECTOPERATION?>'+(response.msgText ? response.msgText : ""), undefined, "center", undefined, 'success', "animated flipInX", "zoomOut",3000);
					return;
				}

				if(!positionUserNote)	// GENERAL NOTE
				{
					$("#generalUserNote").attr('data-content', newHtmlNote);
					$("#viewGeneralUserNote").attr('data-content', newHtmlNote);
				}
				else	// SPECIFIC MODULE NOTE
				{
					$("#moduleUserNote").attr('data-content', newHtmlNote);
					$("#viewModuleUserNote").attr('data-content', newHtmlNote);
				}

				commonNotify('<?=__COMMON_NOTIFY_INFONOCONTENTSTITLE?>', '<br /><br /><?=__COMMON_NOTIFY_CORRECTOPERATION?>'+(response.msgText ? response.msgText : ""), undefined, "center", undefined, 'success', "animated flipInX", "zoomOut",3000);
			},error:function(response){
				commonNotify('<b><?=__COMMON_GENERICTEXT_ERROR?></b>', '<br /><br /><?=__PLUGTOPNOTES_JSAJAXMSGERROR_FATALTXT?>'+response.readyState, undefined, "left", undefined, 'danger', "animated lightSpeedIn", "rotateOut",9000);
				return false;
			}
		});

		$('#htmlWysiEditBox').modal('toggle');
		baseFieldNameToUpddate=null;
		tmpRexNoteId=0;	tmpModNoteId=1;
	});

	$(".deleteNoteButton").click(function(){
		$.ajax({
			url:pluginFolder+"ajax_plugin.php",method:"post",dataType:"json",data:{action:"deleteNote", recordId:tmpRexNoteId}, cache:false, async:false
			,success:function(response){
				if(!response.success)
				{
					commonNotify('<b><?=__COMMON_GENERICTEXT_ERROR?></b>', '<br /><br /><?=__COMMON_GENERICTEXT_TXTSAVEERROR?><br />'+(response.errorText ? response.errorText : "contactar infomática"), undefined, "left", undefined, 'danger', "animated lightSpeedIn", "rotateOut",9000);
					return false;
				}

				var newHtmlNote="";
//<i onclick='$(\".refreshCurrentModule\").click();return false;' class='refreshCurrModule pull-right zmdi zmdi-refresh-alt c-black'></i>
				if(!positionUserNote)	// GENERAL NOTE
					$("#generalUserNote").parent().html("<i class='zmdi zmdi-info-outline c-red' ></i><span class='m-l-10 c-red'><?=__PLUGTOPNOTES_JSAJAXDELETED_TXT?></span>");
				else	// SPECIFIC MODULE NOTE
					$("#moduleUserNote").parent().html("<i class='zmdi zmdi-info-outline c-red' ></i><span class='m-l-10 c-red'><?=__PLUGTOPNOTES_JSAJAXDELETED_TXT?></span>");

				commonNotify('<?=__COMMON_NOTIFY_INFONOCONTENTSTITLE?>', '<br /><br /><?=__COMMON_NOTIFY_CORRECTOPERATION?>'+(response.msgText ? response.msgText : ""), undefined, "center", undefined, 'success', "animated flipInX", "zoomOut",3000);
			},error:function(response){
				commonNotify('<b><?=__COMMON_GENERICTEXT_ERROR?></b>', '<br /><br /><?=__PLUGTOPNOTES_JSAJAXMSGERROR_FATALTXT?>'+response.readyState, undefined, "left", undefined, 'danger', "animated lightSpeedIn", "rotateOut",9000);
				return false;
			}
		});

		$('#htmlWysiEditBox').modal('toggle');
		baseFieldNameToUpddate=null;
		tmpRexNoteId=0;	tmpModNoteId=1;
	});

	$(".cleanWysiwigButton").click(function(){$("#editorWy").html("");});
	$('#editorWy').wysiwyg();
});

</script>
