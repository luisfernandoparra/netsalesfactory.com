<?php
/**
 * PLUGIN MODEL SCRIPT
 *
 * M.F. 2017.10.06
 *
 */
class PluginTopNotes extends CBBDD
{
	public $database;
	public $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $defaultOrderField;
	public $keyListLabel;
	public $mainTable;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;

	public function __construct($db_type=null, $db_host='', $db_user='', $db_pass='', $db_name='', $localParams=null, $port='3306')
	{
		$this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
		$this->prefixGtp=(isset($localParams->prefixTbl) && $localParams->prefixTbl) ? $localParams->prefixTbl : __QUERY_TABLES_PREFIX;
		$this->query='';
		$this->mainTable=$this->prefixGtp.$localParams->mainTable;
		$this->listFieldsOutput=''; $this->mainTableFieldsNames=''; $this->defaultWhereSql='';
		$this->skipBdDefault='';
		$this->defaultOrderField=''; $this->keyListLabel=null;
	return;
	}


	/**
	 * DEVUELVE LAS NOTAS DE LA HOME DEL BACK OFFICE
	 * Y EVENTUALMENTE DEL MODULO ABIERTO PARA EL USUARIO ACTIVO
	 *
	 * @param string $mainTable
	 * @param string $fieldName
	 * @param int $mainKeyRecordId
	 * @param int $moduleId
	 * @return array
	 */
	public function getUserNotes($mainTable='', $fieldName='*', $mainKeyRecordId=0, $moduleId=0)
	{
		if(!trim($fieldName))
			$fieldName='id, content_note, id_module, updated';	// FIELDS TO LOAD

    $this->query='SELECT %s FROM %s WHERE id_user=%s && (id_module=%s || id_module=1) ORDER BY id_module';
    $this->query=sprintf($this->query, $fieldName, $mainTable, (int)$mainKeyRecordId, (int)$moduleId);
    $this->getResultSelectArray($this->query);
    $resSql=$this->tResultadoQuery;
    $resQuery=$resSql;
		return $resQuery;
	}


	/**
	 * ACTUALIZA LA NOTA PERSONAL DEL USUARIO
	 * ACCEDIDO PARA EL MODULO ACTUAL
	 *
	 * @param string $mainTable
	 * @param int $userId
	 * @param int $moduleId
	 * @param int $recordId
	 * @param string $htmlContent
	 * @return int
	 */
	public function storeNote($mainTable='', $userId=0, $moduleId=0, $recordId=0, $htmlContent='')
	{
		if(!trim($mainTable))
			$mainTable='system_plugin_top_user_notes';

		$htmlContent=str_replace('"', '\'', $htmlContent);
		$htmlContent=addslashes($htmlContent);
    $this->query='INSERT INTO %s (id, id_user, id_module, content_note, updated) VALUES (%d, %d, %d, TRIM(\'%s\'), NOW()) ON DUPLICATE KEY UPDATE id_user=%d, id_module=%d, content_note=TRIM(\'%s\'), updated=NOW()';
    $this->query=sprintf($this->query, $mainTable, $recordId, $userId, $moduleId, $htmlContent, $userId, $moduleId, $htmlContent);
		$resQuery=$this->ejecuta_query($this->query);
		return $resQuery;
	}


	/**
	 * DELETE USER NOTE
	 *
	 * @param string $mainTable
	 * @param int $userId
	 * @param int $recordId
	 * @return int
	 */
	public function deleteNote($mainTable='', $userId=0, $recordId=0)
	{
		if(!trim($mainTable))
			$mainTable='system_plugin_top_user_notes';

		$htmlContent=str_replace('\'', '"', $htmlContent);
    $this->query='DELETE FROM %s WHERE id=%d && id_user=%d';
    $this->query=sprintf($this->query, $mainTable, (int)$recordId, (int)$userId);
		$resQuery=$this->ejecuta_query($this->query);
		return $resQuery;
	}

}
