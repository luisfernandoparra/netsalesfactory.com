<?php
//	plug-in translations
//	SPANISH
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$langTxt['plugTopNotes']['header']['titleTxt']='Notas personales';
$langTxt['plugTopNotes']['btnViewGeneralNote']['txt']='nota general';
$langTxt['plugTopNotes']['btnViewModuleNote']['txt']='nota del modulo';
$langTxt['plugTopNotes']['preBtnNew']['txt']='Insertar nueva ';
$langTxt['plugTopNotes']['preEdit']['title']='Editando la nota para el módulo ';
$langTxt['plugTopNotes']['commonEdit']['title']='Editando la nota general';
$langTxt['plugTopNotes']['preBtnDisplay']['txt']='Ver la ';
$langTxt['plugTopNotes']['ajaxMsg']['updatedNote']='Nota actualizada';
$langTxt['plugTopNotes']['ajaxMsg']['deletedNote']='Nota eliminada';
$langTxt['plugTopNotes']['ajaxMsgError']['noUupdated']='Imposible INSERTAR/MODIFICAR la nota editada';
$langTxt['plugTopNotes']['jsNewNoteInserted']['txt']='debe actualizar la pagina para ver la nueva nota';
$langTxt['plugTopNotes']['jsAjaxDeleted']['txt']='Nota eliminada (recargar para otras notas)';
$langTxt['plugTopNotes']['jsAjaxMsgError']['fatalTxt']='Error grave de AJAX';
$langTxt['plugTopNotes']['']['']='';


$contL=0;
$wordSep='_';
$tmpWord=null; $tmpValue=null;

foreach($langTxt as $kL=>$vL)
{
	$tmpWord=$kL;

	foreach($vL as $nn=>$tmpValue1)
	{
		$tmpWord_2=null;
		if(is_array($tmpValue1))
		{
			$tmpWord_3=null;

			foreach($tmpValue1 as $nv=>$tmpValue2)
			{
				$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
				define('__'.strtoupper($tmpWord_3), $tmpValue2);
			}
		}
		else
		{
			$tmpWord_2=$tmpWord.$wordSep.$nn;
			define('__'.strtoupper($tmpWord_2), $tmpValue1);
		}
		$contL++;
	}
}
