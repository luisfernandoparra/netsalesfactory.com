<?php
//	plug-in translations
//	ITALIAN
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$langTxt['plugTopNotes']['header']['titleTxt']='Note personali';
$langTxt['plugTopNotes']['btnViewGeneralNote']['txt']='nota generale';
$langTxt['plugTopNotes']['btnViewModuleNote']['txt']='nota del modulo';
$langTxt['plugTopNotes']['preBtnNew']['txt']='Inserire una nuova ';
$langTxt['plugTopNotes']['preEdit']['title']='Edizione della nota per il modulo ';
$langTxt['plugTopNotes']['commonEdit']['title']='Editando la nota generale';
$langTxt['plugTopNotes']['preBtnDisplay']['txt']='Vedere la ';
$langTxt['plugTopNotes']['ajaxMsg']['updatedNote']='Nota aggiornata';
$langTxt['plugTopNotes']['ajaxMsg']['deletedNote']='Nota eliminata';
$langTxt['plugTopNotes']['ajaxMsgError']['noUupdated']='Impossible INSERTARE/AGGIORNARE la nota editata';
$langTxt['plugTopNotes']['jsNewNoteInserted']['txt']='deve ricaricare la pagina per vedere la nuova nota';
$langTxt['plugTopNotes']['jsAjaxDeleted']['txt']='Nota eliminata (ricaricare per altre note)';
$langTxt['plugTopNotes']['jsAjaxMsgError']['fatalTxt']='Grave errore di AJAX';
$langTxt['plugTopNotes']['']['']='';


$contL=0;
$wordSep='_';
$tmpWord=null; $tmpValue=null;

foreach($langTxt as $kL=>$vL)
{
	$tmpWord=$kL;

	foreach($vL as $nn=>$tmpValue1)
	{
		$tmpWord_2=null;
		if(is_array($tmpValue1))
		{
			$tmpWord_3=null;

			foreach($tmpValue1 as $nv=>$tmpValue2)
			{
				$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
				define('__'.strtoupper($tmpWord_3), $tmpValue2);
			}
		}
		else
		{
			$tmpWord_2=$tmpWord.$wordSep.$nn;
			define('__'.strtoupper($tmpWord_2), $tmpValue1);
		}
		$contL++;
	}
}
