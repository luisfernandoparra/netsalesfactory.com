<?php
/**
 * PLUG-IN TOP NOTES
 *
 * M.F. 2017.10.10
 * 
 */
@session_start();
include($arrPluginAddOns[$plugInNm]['folderPlugin'].'lang/lang_'.strtolower($_SESSION['userAdmin']['pref_']['ref_lang']).'.php');
include($arrPluginAddOns[$plugInNm]['folderPlugin'].'_class/model.class.php');
include($arrPluginAddOns[$plugInNm]['folderPlugin'].'_class/plugin.class.php');
$numNotes=count($contentPlugIn);
//$arrDataNotes=[];
?>
<link href="<?=$arrPluginAddOns[$plugInNm]['baseFolderPlugin']?>css/plugin.css" rel="stylesheet">

      <li class="dropdown c-white">
				<a data-toggle="dropdown" href="#null" class="<?=$foreAutoColor?>" title="<?=__PLUGTOPNOTES_HEADER_TITLETXT?>" data-placement="bottom" data-trigger="hover"><i class="him-icon fa fa-file-text-o"></i></a>
        <ul class="dropdown-menu pull-right bgm-white" style="color:#000;width:auto;min-width:20vw;">
          <li>
            <div class="lg-header top-notes"><i class="fa fa-sticky-note-o"></i>&nbsp;&nbsp;<?=__PLUGTOPNOTES_HEADER_TITLETXT?></div>
          </li>
<?php
$outStr='';
$outStr.='<li><a class="c-black modalHtmlWysiEditBox newUserNote" data-toggle="modal" data-moduleId="1" href="#htmlWysiEditBox"><i class="fa fa-file"></i>&nbsp;&nbsp;'.__PLUGTOPNOTES_PREBTNNEW_TXT.__PLUGTOPNOTES_BTNVIEWGENERALNOTE_TXT.'</a></li>';
//$refArrPos
//echo'<PRE> $dataRow= ';print_r($currentModuleId);echo'</PRE>';

if($numNotes || ($currentModuleId != 1))	// SI ES UN MODULO
{
	$newNote='<i class="fa fa-file"></i>';
	$editNote=''; $linkEdit=false; $contentHtmlNote=''; $recordId=0;

	foreach($contentPlugIn as $fieldRow=>$dataRow)	// FIND CURRENT MODULE NOTES & STORE IN $contentHtmlNote
	{
//		$arrDataNotes[$dataRow['id']]=$dataRow['updated'];
		// ************ SOLO SI HAY DATOS PARA LAS NOTAS DE LA HOME ****************
		if($dataRow['id_module'] == 1)
		{
			$displayDate=date('d-m-Y', strtotime($dataRow['updated']));
			$contentHtmlNote=str_replace("\n", '<br />', $dataRow['content_note']);
			$contentHtmlNote=str_replace('"', '\'', $contentHtmlNote);
			$outStr='<li data-date="'.$displayDate.'" data-id="'.$dataRow['id'].'" data-module-pos="0" title="display note"><a id="viewGeneralUserNote" class="c-black viewUserNotes" href="#null" data-content="'.$contentHtmlNote.'"><i id="generalUserNote" class="fa fa-edit pull-right m-t-0 p-t-0 editUserNote" title="edit" data-moduleId="'.$dataRow['id_module'].'" data-id="'.$dataRow['id'].'" data-content="'.$contentHtmlNote.'"></i><i class="fa fa-file-text"></i>&nbsp;&nbsp;'.__PLUGTOPNOTES_PREBTNDISPLAY_TXT.__PLUGTOPNOTES_BTNVIEWGENERALNOTE_TXT.'<span class="m-l-10 c-gray f-11">('.$displayDate.')</span></a></li>'; //strtotime
		}

		if($dataRow['id_module'] == $currentModuleId && $dataRow['id_module'] != 1)
		{
			$contentHtmlNote=str_replace('"', '\'', $contentHtmlNote);
			$contentHtmlNote=str_replace("\n", '<br />', $dataRow['content_note']);
			$recordId=$dataRow['id'];
			$newNote='';
			$editNote='<i id="moduleUserNote" class="fa fa-edit pull-right m-t-0 p-t-0 editUserNote" title="EDIT" data-moduleId="'.$dataRow['id_module'].'" data-id="'.$recordId.'" data-content="'.$contentHtmlNote.'"></i>';
			$linkEdit=true;
		}
	}

	if($linkEdit)
	{
		$displayDate=date('d-m-Y', strtotime($dataRow['updated']));
		$outStr.='<li data-date="'.$displayDate.'" data-id="'.$dataRow['id'].'" data-module-pos="1" title="display note"><a id="viewModuleUserNote" class="c-black viewUserNotes" href="#null" data-content="'.$contentHtmlNote.'" data-id="'.$recordId.'">'.$editNote.'<i class="fa fa-file-text"></i>&nbsp;&nbsp;'.__PLUGTOPNOTES_PREBTNDISPLAY_TXT.__PLUGTOPNOTES_BTNVIEWMODULENOTE_TXT.'<span class="m-l-10 c-gray f-11">('.$displayDate.')</span></a></li>';
	}
	else
	{
		//.'<span class="m-l-10 c-gray f-11">('.(date('d-m-Y', strtotime($dataRow['updated']))).')</span>
		if($currentModuleId && $currentModuleId != 1)	// SE OMITE GENERAR MÁS NOTAS ESTANDO EN LA HOME
			$outStr.='<li title="insert new note"><a class="c-black modalHtmlWysiEditBox newUserNote" data-toggle="modal" data-moduleId="'.$currentModuleId.'" href="#htmlWysiEditBox">'.$newNote.'&nbsp;&nbsp;'.__PLUGTOPNOTES_PREBTNNEW_TXT.__PLUGTOPNOTES_BTNVIEWMODULENOTE_TXT.'</a></li>';
	}
}
else	//	 NUOVA NOTA SOLO PER LA HOME SOLO SE NON ESISTE
{
	$outStr='<li><a class="c-black modalHtmlWysiEditBox newUserNote" data-toggle="modal" data-moduleId="1" href="#htmlWysiEditBox"><i class="fa fa-file"></i>&nbsp;&nbsp;'.__PLUGTOPNOTES_PREBTNNEW_TXT.__PLUGTOPNOTES_BTNVIEWGENERALNOTE_TXT.'</a></li>';
}

echo $outStr;
?>
					<li class="scrollContainer mainBox ">

<?php
if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD && 0)
{
	echo "\n\n".'<pre>';
	echo '<textarea name="tmpArea" style=color:#000;width:40vw;height:50vh;>';
	//echo count($contentPlugIn);
	echo $plugInNotes->query;
	//echo'<pre>';print_r($plugInNotes);echo'</pre>';
	//echo "\n\n[ arrModuleSettings => ".$arrModuleSettings[$refArrPos].']';
//	echo "\n\n arrDataNotes = ";print_r($arrDataNotes);
	echo "\n\n _REQUEST = ";print_r($_REQUEST);
	echo "\n\n contentPlugIn = ";print_r($contentPlugIn);
	echo "\n\n arrModuleSettings = ";print_r($arrModuleSettings);
	echo "\nNum. notes= ".$numNotes;
	//echo "\n\n long= ".$currentEndStr;
	//echo "\n POS ID= ".$currentModulePosId;
	//echo "\n FINAL ID= ".$currentModuleId;
	echo "\n SESSION = ";print_r($_SESSION);
	echo '</textarea>';
	echo "\n\n".'</pre>';
}
?>
							
					</li>
        </ul>
      </li>
