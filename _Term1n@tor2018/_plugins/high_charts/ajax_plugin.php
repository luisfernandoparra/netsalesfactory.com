<?php
/**
 * SPECIFIC PLUGIN AJAX SCRIPT ATCIONS
 *
 * AJAX RESPONSE ACTIONS ==> PARAMETRO ´action` (COMUN PARA TODAS LAS ACCIONES)
 * getSingleChartData: GET NEW DATA FRON AUTO LAPSE TIMER REQUEST
 * getPointDetails: GET SINGLE MOMENT DETAILS
 * storeUserConf: STORE PERSONAL USER CONFIGURATION CHART PARAMETERS
 * zzz: ??
 *
 */
header('Content-Type: text/html; charset=utf-8');
@session_start();
$commonLogQuerySeparator='<hr style=background-color:#58AFF6;color:#58AFF6; />';

function describeQueries($dataObj='', $resSqlParam=null)
{
	$response=null;

	if($_SESSION['userAdmin']['pref_']['b_display_sql'])
	{
		$response=[];
//print_r($dataObj->query);echo $contBlock.")\n-->";print_r($resSqlParam);return ;

		if(is_array($dataObj->query))
		{
			foreach($dataObj->query as $key=>$resSql)
			{
//echo"\n".$key." ---> contBlock = ".$_SESSION['contLogResponse'];
				$response['sql'][$_SESSION['contLogResponse']]=$resSql.'<hr style=background-color:#58AFF6;color:#58AFF6; />';
//				$response['res'][$key]=(isset($dataObj->resQuery[$key]) && $dataObj->resQuery[$key]) ? $dataObj->resQuery[$key] : $resSqlParam;
				$response['res'][$_SESSION['contLogResponse']]=$dataObj->resQuery[$key];
//				$response['contBlock'][$_SESSION['contLogResponse']]=$contBlock;
				$_SESSION['contLogResponse']++;
			}
		}
		else
		{
//echo"\ndin array bloque... contBlock= ".$_SESSION['contLogResponse'];
//			$contBlock=0;
$response['TEST']='$contBlock='.$_SESSION['contLogResponse'];
			$response['sql'][$_SESSION['contLogResponse']]=$dataObj->query;
			$response['res'][$_SESSION['contLogResponse']]=$resSqlParam;
			$_SESSION['contLogResponse']++;
		}
	}
//echo "\n-response ==> ";print_r($response);return ;
	return $response;
}


if(!isset($_SERVER['HTTP_REFERER']) && basename($_SERVER['SCRIPT_NAME']) != 'index.php'){include('strange_errors.php');die();}	//	prevent direct file access

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_REQUEST;

@include('../../../conf/config_web.php');
@include('../../../conf/config.misc.php');
//echo'2.$port='.$port.'<hr>';die();


$provenience=$_SERVER['HTTP_REFERER'];
$modNameFrom=strpos($provenience,'_modules/')+9;
$modName=substr($provenience, $modNameFrom);
$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));
$localModuleFolder='_modules/'.$modName.'/';
$localModuleFolder=isset($arraExtVars['moduleFolder']) ? $arraExtVars['moduleFolder'] : (isset($arraExtVars['from']) ? $arraExtVars['from'] : (isset($arraExtVars['module']) ? $arraExtVars['module'] : $localModuleFolder));

$currLang=$_SESSION['userAdmin']['pref_']['ref_lang'] ? strtolower($_SESSION['userAdmin']['pref_']['ref_lang']) : 'usa';
$response['query']=array();
$_SESSION['contLogResponse']=0;

$pluginLocalfolder=isset($arraExtVars['pluginFolder']) && $arraExtVars['pluginFolder'] ? $_SERVER['DOCUMENT_ROOT'].'/'.$arraExtVars['pluginFolder'] : null;

if(file_exists($pluginLocalfolder.'lang/lang_'.$currLang.'.php'))	// LOCAL PLUGIN TRANSLATIONS
	include($pluginLocalfolder.'lang/lang_'.$currLang.'.php');

if(file_exists($dirModulesAdmin.'_modules/'.$modName.'/config_module.php'))
	include($dirModulesAdmin.'_modules/'.$modName.'/config_module.php');


if(!isset($_SESSION['userAdmin']))	// SE HA PERDIDO LA SESION
{
	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModAdmin.'mod_common_lists.class.php');
	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name, null, $port);
	$resConnexion=$data->connectDB();

	$data->logBackOffice(0,null,$arraExtVars['getTableListData'].' LOST-SESSION');
	$response['status']=-1;
	$response['rows']=-1;
	$response['rowCount']=0;
	$response['current']=0;
	$response['total']=0;
	$response['msgPainTxt']="\n".@$arraExtVars['idRef'];
	$response['errorText']="\n".__AJAX_ERRORBBDD_LOSTCONNECTIONTXT.'<br /><br />';
	$response['errorText'].="\n".__AJAX_ERRORBBDD_LOSTCONNECTIONTXTBOTTOM.'...';
	$res=json_encode($response);
	die($res);
}


/**
 * STORE PERSONAL USER CONFIGURATION CHART PARAMETERS
 *
 * M.F. 2017.08.10
 */
if(isset($arraExtVars['action']) && $arraExtVars['action'] == 'storeUserConf')
{
	$response['error']=false;
	$tmpUserConf=$dirModulesAdmin.'_modules/'.$modName.'/conf_charts/persUserConf_'.$_SESSION['userAdmin']['userId'].'.dat';
	$arrNewConfigurationJson=json_decode($arraExtVars['currentConf'], true);
//echo $modName."\nANTES: ".$tmpUserConf;print_r($arrNewConfigurationJson);

	foreach($arraExtVars as $key=>$value)
	{
		if(substr($key,0,6) !== 'param_')
			continue;
//echo"\n".$key.' = '.$value;
		$response['tmpDbFilters'][$key]=$value;
		$arrNewConfigurationJson[$key]['value']=$value;
	}

	$strDataFile=json_encode($arrNewConfigurationJson, true);
	$fileHandle=fopen($tmpUserConf, 'w');
	$resWrite=fwrite($fileHandle, utf8_decode($strDataFile));
	fclose($fileHandle);

	$response['storedData']=$strDataFile;
	$response['error']=$resWrite ? null : __PLUGINCHART_AJAXWRITEFILEERROR_MSGTXT;
	$response['success']=!$resWrite ? false : true;
	$res=json_encode($response);
	die($res);
}


if(isset($arraExtVars['action']) && $arraExtVars['action'] == 'getSingleChartData')
{
	include($dirModAdmin.'mod_bbdd.class.php');
	include($pluginLocalfolder.'/_mod_bbdd/mod_plugin.class.php');
	$dataModuleBBDD=new HighCharts($db_type, $db_host, $db_user, $db_pass, $db_name,$port);
	$resConnexion=$dataModuleBBDD->connectDB();
	$arrParams=array_merge($arraExtVars['filterDbParams'], $arraExtVars['sqlParams']);
//print_r($arrParams);die();
//	$resBdData=$dataModuleBBDD->updateChartData($arraExtVars['sqlParams'], $arraExtVars['lastMoment']);
	$resBdData=$dataModuleBBDD->updateChartData($arrParams, $arraExtVars['lastMoment']);
	$jsNumElems=count($resBdData); $tmpNewUnixTime=''; $arrNextDateUpdate=[];

	if($jsNumElems)
	{
		$jsOutData='';
		if($resBdData[0])
		{
			$jsOutData.='{';$jsOutDataHour='hour:[';$jsOutDataMinute='minute:[';$jsOutDataValue='value:[';$jsOutDataSecond='second:[';$jsOutDataDate='date:[';

			foreach($resBdData as $k=>$value)
			{
				$tmpValues[]=$value['valueCountChart'];
				$tmpHou=str_pad($value['hourObtained'],2,'0',STR_PAD_LEFT);
				$tmpMin=str_pad($value['dateObtained'],2,'0',STR_PAD_LEFT);
				$tmpSec=str_pad($value['secondObtained'],2,'0',STR_PAD_LEFT);
				$jsOutDataHour.='"'.$tmpHou.'",';
				$jsOutDataMinute.='"'.$tmpMin.'",';
				$jsOutDataValue.=''.$value['valueCountChart'].',';
				$jsOutDataSecond.='"'.$tmpSec.'",';
				$jsOutDataDate.='"'.$value['dateResult'].'",';
			}

			$jsOutDataHour=substr($jsOutDataHour,0,-1).']';
			$jsOutDataMinute=substr($jsOutDataMinute,0,-1).']';
			$jsOutDataValue=substr($jsOutDataValue,0,-1).']';
			$jsOutDataSecond=substr($jsOutDataSecond,0,-1).']';
			$jsOutDataDate=substr($jsOutDataDate,0,-1).']';
			$jsOutData.=$jsOutDataHour.','.$jsOutDataMinute.','.$jsOutDataValue.','.$jsOutDataSecond.','.$jsOutDataDate.'}';

			$tmpNewUnixTime=date('Y-m-d',strtotime($value[$moduleSetting->pluginsTopIndex['sqlParams']['fieldNameHours']]));
			$tmpNewUnixTime=strtotime($tmpNewUnixTime.' '.$tmpHou.':'.$tmpMin.':'.$tmpSec);
			$xx=($tmpNewUnixTime);
			$tmpNewUnixTime=date('Y-m-d H:i:s',$tmpNewUnixTime);
			$xx++;
//echo $tmpNewUnixTime."\n)--->".date('H:i:s',strtotime($tmpHou.':'.$tmpMin.':'.$tmpSec));
//			$tmpNewUnixTime=date('Y-m-d H:i:s',$xx);
			$arrNextDateUpdate['hour']=$tmpHou;
			$arrNextDateUpdate['minute']=$tmpMin;
			$arrNextDateUpdate['second']=$tmpSec;
			$arrNextDateUpdate['value']=$value['valueCountChart'];
			$arrNextDateUpdate['fieldName']=$moduleSetting->pluginsTopIndex['sqlParams']['fieldNameHours'];
			$jsMaxValue=max($tmpValues);
//echo $jsLastMoment."\n)--->".date('Y-m-d',strtotime($value[$moduleSetting->pluginsTopIndex['sqlParams']['fieldNameHours']]));
		}
		else
		{
			$jsOutData.=$nameJsDataOne.'[0]=0;';
		}
	}
	else
	{
		$tmpNewUnixTime=date('Y-m-d',strtotime($arraExtVars['sqlParams']['defaultDateBetweenTo']));
		$tmpNewUnixTime=date('Y-m-d H:i:s',strtotime($tmpNewUnixTime.' '.$arraExtVars['lastMoment']['hour'].':'.$arraExtVars['lastMoment']['minute'].':'.$arraExtVars['lastMoment']['second']));
		$xx=strtotime($tmpNewUnixTime);
		$xx++;
		$tmpNewUnixTime=date('Y-m-d H:i:s',$xx);
		$arrNextDateUpdate['hour']=date('H',$xx);
		$arrNextDateUpdate['minute']=date('i',$xx);
		$arrNextDateUpdate['second']=date('s',$xx);
		$arrNextDateUpdate['value']=0;
		$arrNextDateUpdate['fieldName']=$moduleSetting->pluginsTopIndex['sqlParams']['fieldNameHours'];
	}
//$arraExtVars['sqlParams']['defaultDateBetweenTo']
//echo $jsOutData."\n".__PLUGINCHART_NODATATITLE_TXT.'---> [[[[';print_r($arraExtVars);echo"\n";print_r($resBdData);print_r($dataModuleBBDD);
//echo "\n".date('H:i:s',($arraExtVars['lastMoment']));
//die('----');
//echo"\ndin array bloque... contBlock= ".$_SESSION['contLogResponse'];print_r($_SESSION);

//	$response['TEST']='$contBlock='.$_SESSION['contLogResponse'];

	$response['query']['sql'][]=$dataModuleBBDD->query;
//	$response['newMomentChartMaxValue']=$jsMaxValue ? $jsMaxValue : $tmpNewUnixTime;
//	$response['newMomentChartMaxValue']=$tmpNewUnixTime;
	$response['newMomentChartData']=$jsOutData;
	$response['newUpdateTime']=$tmpNewUnixTime;
	$response['arrNewIsoTime']=[date('Y',$xx),date('m',$xx),date('d',$xx),date('H',$xx),date('i',$xx),date('s',$xx)];
	$response['newUnixTime']=$xx;
	$response['nextDateUpdate']=$arrNextDateUpdate;
	$response['success']=true;
  $res=json_encode($response);
  die($res);
}



if(isset($arraExtVars['action']) && $arraExtVars['action'] == 'resetCfgFile')
{
	$response['success']=false;

	if(!isset($arraExtVars['config_name']) || !file_exists($arraExtVars['config_name']))
	{
		$response['error']='Falta el parametro del nombre del file de configuracion.';
		if($_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD)
			$response['error'].='<br />O el archivo <b>`'.$arraExtVars['config_name'].'`</b> <u>no existe</u>';
	}
	else
	{
		$res=@unlink($arraExtVars['config_name']);
		$response['error']='Fallo al borrar el file de configuración';
		$response['success']=$res;
	}

  $res=json_encode($response);
  die($res);
}


if(isset($arraExtVars['action']) && $arraExtVars['action'] == 'getPointDetails')
{
	include($dirModAdmin.'mod_bbdd.class.php');
	include($pluginLocalfolder.'/_mod_bbdd/mod_plugin.class.php');
	$dataModuleBBDD=new HighCharts($db_type, $db_host, $db_user, $db_pass, $db_name,$port);
	$resConnexion=$dataModuleBBDD->connectDB();
	$arraExtVars['sqlParams']['']=''; $response['success']=false;
	$arrParams=array_merge($arraExtVars['filterDbParams'], $arraExtVars['sqlParams']);
	$resBdData=$dataModuleBBDD->getPointDetails($arraExtVars['methodParams'], $arrParams);
	$response['data']['sqlTitle']='Single point details: '.$arraExtVars['methodParams']['actionDate'];
	$response['data']['rows']=$dataModuleBBDD->data;
	$response['data']['describeQuery']=$dataModuleBBDD->describeQuery;
	$response['data']['headerRows']=$dataModuleBBDD->headerRows;
	$response['query']['sql'][]=$dataModuleBBDD->query;
	$response['success']=count($dataModuleBBDD->data);
  $res=json_encode($response);
  die($res);
}