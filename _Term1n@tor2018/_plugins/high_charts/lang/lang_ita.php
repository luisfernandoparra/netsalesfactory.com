<?php
//	plug-in translations
//	ITALIAN
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$langTxt['plugInChart']['jsChartTitle']['txt']='Grafici definiti per il modulo';
$langTxt['plugInChart']['configChartTitle']['txt']='Configurazione personale del grafico';
$langTxt['plugInChart']['jsChartTitle']['timeUpdateTxt']='auto update ogni';
$langTxt['plugInChart']['jsDetailsLoadingTitle']['Txt']='caricando dati per la data';
$langTxt['plugInChart']['jsDetailsNumRowsTitle']['valueTxt']=' righe visualizzate';
$langTxt['plugInChart']['jsDetailsTitle']['forTxt']='Dettagli del ';
$langTxt['plugInChart']['jsDetailsTitle']['toTxt']='alle ';
$langTxt['plugInChart']['jsChartTip']['warningTxt']='Aggiornamenti per dati già visualizzati!';
$langTxt['plugInChart']['jsBtnUpdateTitle']['alertTxt']='ATENCION\r\nDebe recargar la pagina\r\npara visualizar correctamente todos los datos.\r\nHan entrado más datos para un momento ya dibujado.';
$langTxt['plugInChart']['chartTitle']['txt']='Dati del modulo';
$langTxt['plugInChart']['nodataTitle']['txt']='Attenzione, senza dati riscontrati';
$langTxt['plugInChart']['nodataExplain']['preTxt']='Sembra che ancora non ci siano dati misurabili per el periodo attualmente selezionato.<br /><br />Da ricordare che il periodo automatico di aggiornamenti per i dati attualmente è di ';
$langTxt['plugInChart']['noDataExplain']['posTxt']=' secondi. Comunque, è sempre possibile eseguire l` aggiornamento pigiando il tasto apposito.';
$langTxt['plugInChart']['label']['1']='Azioni registrate';
$langTxt['plugInChart']['labelDay']['txt']='Data';
$langTxt['plugInChart']['labelMoreMinutes']['txt']='Dati per più di 1 minuto';
$langTxt['plugInChart']['labelReduceRange']['txt']='Diminuire il range dei tempi<br>per ottenere più precisione';
$langTxt['plugInChart']['jsAjaxMsgError']['TitleTxt']='Si è verificato un errore';
$langTxt['plugInChart']['jsAjaxMsgError']['TitleContent']='In questo momento sembra si sia verificato un errore nell`aggiornamento del grafico: Provaci più tardi e se questo errore si ripete, si prega di contattare con Informatica.';
$langTxt['plugInChart']['jsMsgErr']['minUpdateValue']='Minimo 10 secondi!';
$langTxt['plugInChart']['jsCurrentAutoUpdateAction']['htmlTxt']='(aggiornamenti automatici abilitati)';
$langTxt['plugInChart']['jsCurrentNoUpdateAction']['htmlTxt']='(aggiornamenti automatici arrestati)';

$langTxt['plugInChart']['jsAjaxMsgError']['FatalTxt']='Errore nell´ajax, stato: ';
$langTxt['plugInChart']['jsAjaxMsgError']['unknownErrorTxt']='<b>SCONOSCIUTO</b><br />Se questo errore si dovesse ripetere,<br />per piacere, contatta con Informatica';
$langTxt['plugInChart']['jsAjaxMsgError']['CommonTxt']='Detagli: ';
$langTxt['plugInChart']['jsAjaxMsgConfigOk']['msgTxt']='Configurazione salvata correttamente';

$langTxt['plugInChart']['btnCommand']['columnTxt']='Barre';
$langTxt['plugInChart']['btnCommand']['LineTxt']='Linee rette';
$langTxt['plugInChart']['btnCommand']['spilineTxt']='Linne curve';
$langTxt['plugInChart']['btnCommand']['areaTxt']='Linee con area';
$langTxt['plugInChart']['btnCommand']['areasplineTxt']='Linee curve con area';
$langTxt['plugInChart']['labelFrom']['txt']='Da';
$langTxt['plugInChart']['labelTo']['txt']='a';
$langTxt['plugInChart']['labelAll']['txt']='Tutto';
$langTxt['plugInChart']['btnCommand']['scatterTxt']='Punti';
$langTxt['plugInChart']['btnCommand']['chartNoUpdateTxt']='Non aggiornare più';
$langTxt['plugInChart']['btnCommand']['chartStartUpdateTxt']='Aggiornare automaticamente ogni';

$langTxt['plugInChart']['formUserConfig']['labelChartType']='tipo di grafico';
$langTxt['plugInChart']['formUserConfig']['labelEnableLocalIp']='omettere la IP locale';
$langTxt['plugInChart']['formUserConfig']['labelParam_dateFrom']='data da';
$langTxt['plugInChart']['formUserConfig']['labelParam_dateTo']='data a';
$langTxt['plugInChart']['formUserConfig']['btnInstantApplyTxt']='Applicare immediatamente';
$langTxt['plugInChart']['formUserConfig']['btnCnfResetTxt']='Reset configurazione del grafico';

$langTxt['plugInChart']['jsFormUserConfig']['confirmResetCfgTitle']='Conferma reset configurazione';
$langTxt['plugInChart']['jsFormUserConfig']['confirmResetCfgMsg']='Attenzione, se accetta cancellare la sua configurazione, tutti i valori personali dei parametri configurati verranno cancellati';
$langTxt['plugInChart']['jsFormUserConfig']['confirmResetBtnConfirm']='eliminare la configurazione';
$langTxt['plugInChart']['jsFormUserConfig']['notificaionAfterResetCfgMsg']='È necessario ricaricarer questa grafica prima di visualizzare la configurazione personalizzata.';

$langTxt['plugInChart']['formUserConfig']['newConfigCreationMsg']='<span class="f-14 -c-black p10">È appena stata resa disponibile la possibilitá di memorizzare alcune delle opzioni per la configurazione dell`attuale grafico; questa caratteristica sarà disponibile dalla prossima carica del grafico per il modulo </span>';
$langTxt['plugInChart']['formUserConfig']['infoUpdateUserConfigTxt']='Può essere necessario ricaricare la pagina per applicare immediatamente tutte le nuove impostazioni';

$langTxt['plugInChart']['ajaxWriteFileError']['msgTxt']='Errore della scrittura del file di configurazione';


$contL=0;
$wordSep='_';
$tmpWord=null; $tmpValue=null;

foreach($langTxt as $kL=>$vL)
{
	$tmpWord=$kL;

	foreach($vL as $nn=>$tmpValue1)
	{
		$tmpWord_2=null;
		if(is_array($tmpValue1))
		{
			$tmpWord_3=null;

			foreach($tmpValue1 as $nv=>$tmpValue2)
			{
				$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
				define('__'.strtoupper($tmpWord_3), $tmpValue2);
			}
		}
		else
		{
			$tmpWord_2=$tmpWord.$wordSep.$nn;
			define('__'.strtoupper($tmpWord_2), $tmpValue1);
		}
		$contL++;
	}
}
