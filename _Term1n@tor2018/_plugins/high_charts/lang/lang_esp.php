<?php
//	plug-in translations
//	SPANISH
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$langTxt['plugInChart']['jsChartTitle']['txt']='Graficas definidas para el m&oacute;dulo';
$langTxt['plugInChart']['configChartTitle']['txt']='Configuraci&oacute;n personal del gr&aacute;fico';
$langTxt['plugInChart']['jsChartTitle']['timeUpdateTxt']='auto update cada';
$langTxt['plugInChart']['jsDetailsLoadingTitle']['Txt']='cargando datos con fecha';
$langTxt['plugInChart']['jsDetailsNumRowsTitle']['valueTxt']=' filas visualizadas';
$langTxt['plugInChart']['jsDetailsTitle']['forTxt']='Detalles para el ';
$langTxt['plugInChart']['jsDetailsTitle']['toTxt']='a las ';
$langTxt['plugInChart']['jsChartTip']['warningTxt']='Actualización apra datos ya dibujados!';
$langTxt['plugInChart']['jsBtnUpdateTitle']['alertTxt']='ATENCIÓN\r\nSe debe volver a cargar la página\r\npara mostrar los datos correctamente.\r\nLos datos son de un lapso de tiempo ya representado!';
$langTxt['plugInChart']['chartTitle']['txt']='Datos del módulo';
$langTxt['plugInChart']['nodataTitle']['txt']='Atenci&oacute;n, no se encontraron datos';
$langTxt['plugInChart']['nodataExplain']['preTxt']='Parece que todav&iacute;a no hay datos medibles para el per&iacute;odo actualmnte seleccionado.<br /><br />Recuerde que el período de las actualizaciones automáticas de los datos actualmente es de ';
$langTxt['plugInChart']['nodataExplain']['posTxt']=' segundos. Sin embargo, siempre se puede hacer manualmente pulsando el botón correspondiente.';
$langTxt['plugInChart']['label']['1']='Acciones grabadas';
$langTxt['plugInChart']['labelDay']['txt']='Fecha';
$langTxt['plugInChart']['labelMoreMinutes']['txt']='Datos para más de 1 minuto';
$langTxt['plugInChart']['labelReduceRange']['txt']='Reducir el rango de tiempo a<br>visualizar para más precisión';
$langTxt['plugInChart']['jsAjaxMsgError']['TitleTxt']='Se ha verificado un error';
$langTxt['plugInChart']['jsAjaxMsgError']['TitleContent']='En este momento parece que ha habido un error en la representación gráfica: Inténtelo más tarde y si el fallo persiste, póngase en contacto con Informatica.';
$langTxt['plugInChart']['jsMsgErr']['minUpdateValue']='M&iacute;nimo 10 segundos!';
$langTxt['plugInChart']['jsCurrentAutoUpdateAction']['htmlTxt']='(actualizaciones automáticas habilitadas)';
$langTxt['plugInChart']['jsCurrentNoUpdateAction']['htmlTxt']='(actualizaciones automáticas detenidas)';

$langTxt['plugInChart']['jsAjaxMsgError']['FatalTxt']='Error en el AJAX, estado: ';
$langTxt['plugInChart']['jsAjaxMsgError']['unknownErrorTxt']='DESCONOCIDO<br>Si este error se repite, por favor contacta con Informática';
$langTxt['plugInChart']['jsAjaxMsgError']['CommonTxt']='Detalles: ';
$langTxt['plugInChart']['jsAjaxMsgConfigOk']['msgTxt']='Configuración guardada correctamente';

$langTxt['plugInChart']['btnCommand']['columnTxt']='Barras';
$langTxt['plugInChart']['btnCommand']['LineTxt']='L&iacute;neas rectas';
$langTxt['plugInChart']['btnCommand']['spilineTxt']='L&iacute;neas suaves';
$langTxt['plugInChart']['btnCommand']['areaTxt']='L&iacute;neas con &aacute;rea';
$langTxt['plugInChart']['btnCommand']['areasplineTxt']='L&iacute;neas suaves y &aacute;rea';
$langTxt['plugInChart']['labelFrom']['txt']='Desde';
$langTxt['plugInChart']['labelTo']['txt']='hasta';
$langTxt['plugInChart']['labelAll']['txt']='Todo';
$langTxt['plugInChart']['btnCommand']['scatterTxt']='Puntos';
$langTxt['plugInChart']['btnCommand']['chartNoUpdateTxt']='No actualizar m&aacute;s';
$langTxt['plugInChart']['btnCommand']['chartStartUpdateTxt']='Actualizar autom&aacute;ticamente cada';

$langTxt['plugInChart']['formUserConfig']['labelChartType']='Tipo de grafico por defecto';
$langTxt['plugInChart']['formUserConfig']['labelEnableLocalIp']='Omitir IP Local';
$langTxt['plugInChart']['formUserConfig']['labelParam_dateFrom']='Desde la fecha';
$langTxt['plugInChart']['formUserConfig']['labelParam_dateTo']='Hasta la fecha';
$langTxt['plugInChart']['formUserConfig']['btnInstantApplyTxt']='Aplicar inmediatamente';
$langTxt['plugInChart']['formUserConfig']['btnCnfResetTxt']='Reset de la configuración';

$langTxt['plugInChart']['jsFormUserConfig']['confirmResetCfgTitle']='Confirma reset configuración';
$langTxt['plugInChart']['jsFormUserConfig']['confirmResetCfgMsg']='Atención, si acepta resetear su configuración, todos los valores personales serán borrados';
$langTxt['plugInChart']['jsFormUserConfig']['confirmResetBtnConfirm']='eliminar configuración';
$langTxt['plugInChart']['jsFormUserConfig']['notificaionAfterResetCfgMsg']='Es necesario recargar este grafico antes de visualizar la configuración personalizada';

$langTxt['plugInChart']['formUserConfig']['newConfigCreationMsg']='<span class="f-14 -c-black p10">Se acaba de activar la posibilidad de memorizar algunas de las opciones de configuración para el grafico actual.<br>Esta característica estará disponible a partir de la siguiente carga del módulo </span>';
$langTxt['plugInChart']['formUserConfig']['infoUpdateUserConfigTxt']='Nota que puede ser necesario recargar la página para activar inmediatamente los cambios efectuados';

$langTxt['plugInChart']['ajaxWriteFileError']['msgTxt']='Error en escritura en el archivo de configuración';


$contL=0;
$wordSep='_';
$tmpWord=null; $tmpValue=null;

foreach($langTxt as $kL=>$vL)
{
	$tmpWord=$kL;

	foreach($vL as $nn=>$tmpValue1)
	{
		$tmpWord_2=null;
		if(is_array($tmpValue1))
		{
			$tmpWord_3=null;

			foreach($tmpValue1 as $nv=>$tmpValue2)
			{
				$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
				define('__'.strtoupper($tmpWord_3), $tmpValue2);
			}
		}
		else
		{
			$tmpWord_2=$tmpWord.$wordSep.$nn;
			define('__'.strtoupper($tmpWord_2), $tmpValue1);
		}
		$contL++;
	}
}
