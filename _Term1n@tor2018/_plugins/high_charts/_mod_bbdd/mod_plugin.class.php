<?php
/**
 * PLUG-IN HIGH CHARTS LOGIC EXTENSION
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class HighCharts extends CBBDD
{
	protected $database;
	public $query;
  public $data=array();

	public function __construct($db_type, $db_host, $db_user, $db_pass, $db_name, $port=1312)
	{
		$this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
		$this->mainTable='system_menu_admin';
		$this->query='';
		return;
	}

/**
 * NOTA IMPORTANTE:
 * "defaultStepGroupDate" ----->  ESTABLECE EL RANGO EFECTIVO A UTILIZAR EN LA QUERY
 *
 *
SELECT main.ip_usuario , COUNT(*) AS valueCountChart , MINUTE(main.fecha) AS dateObtained, HOUR(main.fecha) AS hourObtained FROM `oscar_opere`.`osc_log_web_actions2017` AS main WHERE 1 && DATE(fecha) ='2017-06-23' GROUP BY main.ip_usuario,EXTRACT(MINUTE FROM main.fecha) ORDER BY main.fecha ASC
 * @param array $sqlParams
 * @return array
 */
	public function getInitData($sqlParams=null)
	{
		$whereSql=''; $groupBySql=''; $groupByTwo=='';
    $groupBySql=' GROUP BY '; $selectFieldNameGroupBy=''; $sqlOrder='';
    $groupBySql.=$sqlParams['whereFieldDisctinct'] ? 'main.'.$sqlParams['whereFieldDisctinct'].',' : '';
    $whereSql.=$sqlParams['whereExclusion'] && isset($_REQUEST['enable_local_ip']) && $_REQUEST['enable_local_ip'] == 1 ? '&& main.'.$sqlParams['whereExclusion'].'' : '';
//		$sqlSelectFelds=$sqlParams['whereFieldDisctinct'] ? ' main.'.$sqlParams['whereFieldDisctinct'].' AS ipUser ,' : '';
    $sqlSelectFelds=$sqlParams['selectIpHighlight'] ? ' main.'.$sqlParams['selectIpHighlight'].' AS ipUser ,' : '';

		if($sqlParams['whereRangeType'] == 'DATE')
		{
			$sqlOrder=' ORDER BY main.'.$sqlParams['fieldNameDateBetween'].'';

			switch($sqlParams['defaultStepGroupDate'])
			{
				case 'minute':
//					$groupBySql.='EXTRACT(HOUR FROM main.'.$sqlParams['fieldNameHours'].'), EXTRACT(MINUTE FROM main.'.$sqlParams['fieldNameHours'].')'.$groupByTwo;
					$groupBySql.='DATE(main.'.$sqlParams['fieldNameDateBetween'].'), HOUR(main.'.$sqlParams['fieldNameHours'].'), MINUTE(main.'.$sqlParams['fieldNameHours'].')'.$groupByTwo;
					$selectFieldNameGroupBy=', MINUTE(main.'.$sqlParams['fieldNameHours'].') AS dateObtained, HOUR(main.'.$sqlParams['fieldNameHours'].') AS hourObtained, MAX(SECOND(main.'.$sqlParams['fieldNameHours'].')) AS secondObtained';
//					$whereSql.='&& DATE(main.'.$sqlParams['fieldNameDateBetween'].') =\''.date('Y-m-d',strtotime($sqlParams['defaultDateBetweenTo'])).'\'';
					$whereSql.='&& DATE(main.'.$sqlParams['fieldNameDateBetween'].') BETWEEN \''.date('Y-m-d',strtotime($sqlParams['defaultDateBetweenFrom'])).'\' AND \''.date('Y-m-d',strtotime($sqlParams['defaultDateBetweenTo'])).'\'';
					$sqlOrder=' ORDER BY main.'.$sqlParams['fieldNameDateBetween'].' ,main.'.$sqlParams['fieldNameHours'].' ASC';
					break;
				case 'hour':
					//$groupBySql.='EXTRACT(HOUR FROM main.'.$sqlParams['fieldNameDateBetween'].')'.$groupByTwo;
					$groupBySql.='DATE(main.'.$sqlParams['fieldNameDateBetween'].'), HOUR(main.'.$sqlParams['fieldNameHours'].')';
					$selectFieldNameGroupBy=', HOUR(main.'.$sqlParams['fieldNameDateBetween'].') AS dateObtained';
					$whereSql.='&& DATE(main.'.$sqlParams['fieldNameDateBetween'].') =\''.date('Y-m-d',strtotime($sqlParams['defaultDateBetweenTo'])).'\'';
					$sqlOrder=' ORDER BY main.'.$sqlParams['fieldNameHours'].' ASC';
					break;
				case 'day':
					$groupBySql.='DATE(main.'.$sqlParams['fieldNameDateBetween'].')';
//					$groupBySql.='EXTRACT(DAY FROM main.'.$sqlParams['fieldNameDateBetween'].')'.$groupByTwo;
					$selectFieldNameGroupBy=', DATE(main.'.$sqlParams['fieldNameDateBetween'].') AS dateObtained';
					$whereSql.='&& DATE(main.'.$sqlParams['fieldNameDateBetween'].') BETWEEN \''.date('Y',strtotime($sqlParams['defaultDateBetweenFrom'])).'\' AND \''.$sqlParams['defaultDateBetweenTo'].'\'';
					break;
				case 'week':
					$groupBySql.='EXTRACT(WEEK FROM main.'.$sqlParams['fieldNameDateBetween'].')'.$groupByTwo;
					$selectFieldNameGroupBy=', WEEK(main.'.$sqlParams['fieldNameDateBetween'].') AS dateObtained';
					$whereSql.='&& DATE(main.'.$sqlParams['fieldNameDateBetween'].') BETWEEN \''.date('Y',strtotime($sqlParams['defaultDateBetweenFrom'])).'\' AND \''.$sqlParams['defaultDateBetweenTo'].'\'';
					break;
				case 'month':
					$groupBySql.='EXTRACT(YEAR_MONTH FROM main.'.$sqlParams['fieldNameDateBetween'].')'.$groupByTwo;
					$selectFieldNameGroupBy=', MONTH(main.'.$sqlParams['fieldNameDateBetween'].') AS dateObtained';
					$whereSql.='&& YEAR(main.'.$sqlParams['fieldNameDateBetween'].') BETWEEN \''.date('Y',strtotime($sqlParams['defaultDateBetweenFrom'])).'\' AND \''.date('Y',strtotime($sqlParams['defaultDateBetweenFrom'])).'\'';
					break;
				case 'year':
					$groupBySql.='YEAR(main.'.$sqlParams['fieldNameDateBetween'].')'.$groupByTwo;
					$selectFieldNameGroupBy=', YEAR(main.'.$sqlParams['fieldNameDateBetween'].') AS dateObtained';
					break;
				default:
					$groupBySql='';
			}
		}

//		$groupBySql.=$sqlParams['defaultStepGroupDate'] ? '`'.$sqlParams['fieldNameDateBetween'].'`' : '`'.$sqlParams['fieldNameDateBetween'].'`';

		$this->query='SELECT %s COUNT(*) AS valueCountChart, DATE(main.'.$sqlParams['fieldNameDateBetween'].') AS dateResult %s
FROM `%s`.`%s` AS main 
WHERE 1 %s
%s %s';
		$this->query=sprintf($this->query, $sqlSelectFelds, $selectFieldNameGroupBy, ($sqlParams['mainBBDD'] ? $sqlParams['mainBBDD'] : $this->db), $sqlParams['mainTableName'], $whereSql, $groupBySql, $sqlOrder);
//echo $this->query.'<hr>';return;//whereExcusion
		$this->getResultSelectArray($this->query);
		$response=$this->tResultadoQuery;

		$this->data=array();
		$this->data=$response;
		return $this->data;
  }

	/**
	 *
	 * @param type $sqlParams
	 * @param type $updateParams
	 * @return type
	 */
	public function updateChartData($sqlParams=null, $updateParams=null)
	{
		$whereSql=' && DATE(main.'.$sqlParams['fieldNameDateBetween'].') = \''.$sqlParams['defaultDateBetweenTo'].'\' && DATE_FORMAT(main.'.$updateParams['fieldName'].',\'%H,%i,%s\') > \''.$updateParams['hour'].','.$updateParams['minute'].','.$updateParams['second'].'\'';
		$whereSql.=$sqlParams['whereExclusion'] && isset($sqlParams['enable_local_ip']) && $sqlParams['enable_local_ip'] == 1 ? '&& main.'.$sqlParams['whereExclusion'].'' : '';
		$groupBySql.='GROUP BY EXTRACT(HOUR FROM main.'.$sqlParams['fieldNameHours'].'), EXTRACT(MINUTE FROM main.'.$sqlParams['fieldNameHours'].')';
//		$sqlOrder=' ORDER BY DATE(main.'.$sqlParams['fieldNameDateBetween'].') DESC'.($updateParams['fieldName'] != '' ? ', DATE(main.'.$updateParams['fieldName'].') DESC' : '');
		$sqlOrder=' ORDER BY DATE(main.'.$sqlParams['fieldNameDateBetween'].') DESC';
		$sqlSelectFelds='COUNT(*) AS valueCountChart, main.*';
		$selectFieldNameGroupBy=', MINUTE(main.'.$sqlParams['fieldNameHours'].') AS dateObtained, HOUR(main.'.$sqlParams['fieldNameHours'].') AS hourObtained, MAX(SECOND(main.'.$sqlParams['fieldNameHours'].')) AS secondObtained';
		$sqlLimit='';
		$this->query='SELECT %s %s, DATE(main.'.$sqlParams['fieldNameDateBetween'].') AS dateResult 
FROM `%s`.`%s` AS main 
WHERE 1 %s %s %s
%s';
		$this->query=sprintf($this->query, $sqlSelectFelds, $selectFieldNameGroupBy, ($sqlParams['mainBBDD'] ? $sqlParams['mainBBDD'] : $this->db), $sqlParams['mainTableName'], $whereSql, $groupBySql, $sqlOrder, $sqlLimit);

//echo $this->query."\n".__PLUGINCHART_NODATATITLE_TXT.'--->$sqlParams=';print_r($sqlParams);echo"\n(updateParams)=";print_r($updateParams);print_r($_REQUEST);return;
		$this->getResultSelectArray($this->query);
		$response=$this->tResultadoQuery;
		$this->data=array();
		$this->data=$response;
		return $this->data;
	}


	public function getPointDetails($methodParams=null, $sqlParams=null)
	{
		$queryFiledNames='DESCRIBE `%s`.`%s`';	$whereSql='';
		$queryFiledNames=sprintf($queryFiledNames, ($sqlParams['mainBBDD'] ? $sqlParams['mainBBDD'] : $this->db), $sqlParams['mainTableName']);
		$this->getResultSelectArray($queryFiledNames);
//		$mainTableFieldsNames=array();
		$this->headerRows=$this->tResultadoQuery;

		$sqlSelectFelds='*'; $whereSql=''; $groupBySql=''; $sqlOrder=' ORDER BY main.'.$sqlParams['fieldNameHours'].' DESC'; $sqlLimit='';
//echo $methodParams['actionDate'];die();
		$whereSql.=$sqlParams['whereExclusion'] && isset($sqlParams['enable_local_ip']) && $sqlParams['enable_local_ip'] == 1 ? '&& main.'.$sqlParams['whereExclusion'].'' : '';
		$whereSql.=' && DATE(main.'.$sqlParams['fieldNameDateBetween'].') =\''.$methodParams['actionDate'].'\'';

		if($methodParams['actionTime'])
			$whereSql.=' && HOUR(main.'.$sqlParams['fieldNameHours'].') =\''.substr($methodParams['actionTime'],0,2).'\' && MINUTE(main.'.$sqlParams['fieldNameHours'].') =\''.substr($methodParams['actionTime'],3,2).'\'';

		$this->query='SELECT %s FROM `%s`.`%s` AS main 
WHERE 1 %s %s %s 
%s';
		$this->query=sprintf($this->query, $sqlSelectFelds, ($sqlParams['mainBBDD'] ? $sqlParams['mainBBDD'] : $this->db), $sqlParams['mainTableName'], $whereSql, $groupBySql, $sqlOrder, $sqlLimit);
		$this->getResultSelectArray($this->query);
		$response=$this->tResultadoQuery;
		$this->data=$response;
		$this->describeQuery=$queryFiledNames;
		//$this->data=array_merge($response,$mainTableFieldsNames);
//		return $this->query;
//echo $this->query."\n".'--->$sqlParams=';print_r($sqlParams);echo"\n(methodParams)=";print_r($methodParams);return;
	}

}