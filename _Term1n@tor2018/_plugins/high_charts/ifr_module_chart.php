<?php
/**
 * CHART PLUGIN -- IFRAME ---
 *
 * $objModuleParams = OBJETO CON LAS CONFIGURACIONES BASICAS DE LOS DATOS A REPRESENTAR
 * $arrChartTypes = ARRAY CON EL TIPO DE GRAFICOS REPRESENTABLES
 * $prefixChartParams = PREFIJO PARA VARIABLES INTERNAS GENERADAS EN LAS RECARGAS DEL PLUG-IN ACTUAL
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

/**
1. funcion para simular "la escritura":
$(function(){
  simulateTyping('texto...', '#txt')

  function simulateTyping(str, textAreaId) {
    var textArea = $(textAreaId);
    var currentCharIndex = 0;
    function typeChar(){
      if (currentCharIndex >= str.length)
        return;
      var char = str[currentCharIndex];
      textArea.val(textArea.val() + char);
      currentCharIndex ++;
      setTimeout(typeChar, 500);
    }

    typeChar();

  }
})
 */

if(count($_GET))
  $arraExtVars=$_REQUEST;
else
  $arraExtVars=$_POST;

if(!$arraExtVars['moduleAdmin'])
{
	die('<br><br><div class="alert alert-danger"><center><h2>HA OCURRIDO UN ERROR AL INTENTAR CARGAR ESTE PULG-IN!</h2></center></div>');
}

if(!isset($_SESSION['userAdmin']['userId']))
{
	echo '<header style="color:#000;font-family:arial,verdana;text-align:center;padding:2%;padding-top:6px;font-size:1.2em;">';
	echo '<div style="margin-bottom:2%;"><a style="color:#000;padding:4px;padding-left:24px;padding-right:24px;border:1px solid #999;text-decoration:none;background:#ddd;" href="../../index.php" target="_top">RELOAD</a></div>';
	echo '<div style="font-weight:normal;color:red;">';
	echo 'Parece que has estado inactivo con esta herramienta demasiado tiempo<br />o has dejado abierta esta pagina saliendo de tu sesi&oacute;n de usuario.';
	echo '<br /><br />Sembra che sei stato inattivo con questo strumento troppo a lungo<br />o che hanno lasciato aperta questa pagina dalla vostra sessione utente';
	echo '<br /><br />It seems that you`ve been inactive with this tool too long<br />or who have left open this page from your user session';
	echo '</div>';
	echo '<div style="font-weight:normal;color:#333;margin-top:2%;">';
	echo 'Debes identificarte nuevamente.';
	echo '<br /><br />&Egrave; necessario accedere di nuovo.';
	echo '<br /><br />You need to log in again.';
	echo '</div>';
	echo '</header>';
	die();
}

include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');
include($dirModulesAdmin.$arraExtVars['moduleAdmin'].'config_module.php');

$objModuleParams=isset($arraExtVars['moduleParams']) ? $arraExtVars['moduleParams'] : '';
$objModuleParams=json_decode($objModuleParams);
$strHeaderControls='';
$nameJsDataOne='arrDataA';
$arrJsChartParams=[''];
$highgChartTimeLapse=($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD) ? 30000 : 60000; // INTERVAL UPDATE CHART TIME IN MILLISECONDS
$prefixChartParams='chartParam_';
$jsCode='';	// ADDITIONAL JS CODE
$additionalScripts='';
$htmlSpecificLibraries='';

include($dirModAdmin.'mod_bbdd.class.php');
include($dirPluginsAdmin.$moduleSetting->pluginsTopIndex['customPath'].'/_mod_bbdd/mod_plugin.class.php');

$dataSiteBBDD=new HighCharts($db_type, $db_host, $db_user, $db_pass, $db_name, $port);
$resConnexion=$dataSiteBBDD->connectDB();
//echo $port.'!!!-----><pre>';print_r($dataSiteBBDD);
// START PLUG-IN LANGUAGE TRANSLACTIONS
if(isset($_SESSION['userAdmin']['pref_']['ref_lang']))
{
	$dirLang=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath'].'/lang/';
	$filLang=$dirLang.'lang_'.strtolower($_SESSION['userAdmin']['pref_']['ref_lang']).'.php';
	if(file_exists($filLang))
		include($filLang);
}
// END PLUG-IN LANGUAGE TRANSLACTIONS





/**
 * START PERSONAL USER FORM CONFIG PLUGIN "saveUserConfig"
 *
 * (pluginUserConfig)
 */
$objUserChartConf=new stdClass();
$objUserChartConf->config_version=['type'=>'hide','value'=>'0'];
$objUserChartConf->param_enable_local_ip=['type'=>'checkbox','value'=>'1','dataType'=>'int','targetWindow'=>'parent'];

//$objUserChartConf->param_ChartType=['type'=>'select','value'=>'column','select_values'=>'area,areaspline,column,line,scatter,spline'];
//$objUserChartConf->param_dateFrom=['type'=>'date','value'=>date('Y-m-d'),'dataType'=>'date_usa','classCss'=>'col-sm-5 f-12 p-r-5 p-l-5 date-picker-chart-filter'];
//$objUserChartConf->param_dateTo=['type'=>'text','value'=>date('Y-m-d'),'dataType'=>'date_usa','classCss'=>'col-sm-6 f-12 p-r-5 p-l-5'];	//$displayTooltipDetail
$objUserChartConf->param_EnableLocalIp=1;
$tmpUserConf=$dirModulesAdmin.$arraExtVars['moduleAdmin'].'conf_charts/persUserConf_'.$_SESSION['userAdmin']['userId'].'.dat';
$tmpFolder=$dirModulesAdmin.$arraExtVars['moduleAdmin'].'conf_charts';
//$tmpFileName='persUserConf_'.$_SESSION['userAdmin']['userId'].'.dat';
$strForm='<div class="text-left p-0 w-100">'; $strFormError=''; $jsUserChartConf='';

if(file_exists($tmpUserConf))
{
	unset($objUserChartConf);
	$plainDataUserConfig=file_get_contents($tmpUserConf);
	$objUserChartConf=json_decode($plainDataUserConfig, true);
//echo $plainDataUserConfig.'<pre>';print_r($objUserChartConf['param_enable_local_ip']['value']);echo'</pre>';
	$strDataFile='{';
	foreach($objUserChartConf as $paraConf=>$dataConf)
	{
		$strDataFile.='"'.$paraConf.'":"'.$dataConf.'",';
//$objUserChartConf->$paraConf->value='XXXXXXXX';
//echo '<hr>'.$objUserChartConf{$paraConf}{'value'};
//echo'<br>--->'.$paraConf;
		if(isset($dataConf['type']))
		{
//echo' -'.$dataConf['type'];
			switch($dataConf['type'])
			{
				case 'select':
					$strForm.='<span class="pull-left col-sm-4 m-t-10"><span class="c-bluegray">'.__PLUGINCHART_FORMUSERCONFIG_LABELCHARTTYPE.':</span> <span class="m-r-10">
						<select name="'.$paraConf.'" class="selectpicker" title="'.__PLUGINCHART_FORMUSERCONFIG_LABELCHARTTYPE.'" placeholder=" " required="required" data-none-selected-text="nessuna selezione">';
					if($dataConf['select_values'])
					{
						$arrVSelectValues=split(',',$dataConf['select_values']);
						if(count($arrVSelectValues))
							foreach($arrVSelectValues as $selectValue)
							{
								$selectedOption=$selectValue == $dataConf['value'] ? 'selected="selected"' : '';
								$strForm.='<option value="'.$selectValue.'" '.$selectedOption.'>'.$selectValue.'</option>';

							}
					}
					$strForm.='</select></span></span>';
					break;

				case 'checkbox':
					$strForm.='<div class="col-sm-4 pull-left m-t-10"><label><span class="c-bluegray">'.__PLUGINCHART_FORMUSERCONFIG_LABELENABLELOCALIP.':</span><input name="'.$paraConf.'" type="checkbox" value="'.$dataConf['value'].'" class="m-l-10 '.($dataConf['dataType'] == 'int' ? 'updateCheckValue' : '').'" '.($dataConf['value'] ? 'checked="checked"' : '').' /><input name="'.$paraConf.'" value=""  type="hidden" /><i class="input-helper"></i></label></div>';
					break;

				case 'date':
					$jsUserChartConf.='$(".datePickerConfig").datetimepicker({
	format:"YYYY-MM-DD"
	,locale:""
});';
/**
<input id="chart_range_date" data-name="chart_range_date" name="chart_range_date" value="" data-date-format="YYYY-MM-DD" class="form-control date-picker-chart-filter f-12 c-gray bgm-white" title="Data da ... a" style="width:150px;text-align:center;" type="text">
 * date('d/m/Y',strtotime($dataConf['value']))
 * 
 */					$strForm.='<span class="col-sm-4 pull-left m-t-10"><label class=""><span class="pull-left c-bluegray">'.constant('__PLUGINCHART_FORMUSERCONFIG_LABEL'.strtoupper($paraConf)).':</span><input name="'.$paraConf.'" type="text" value="'.$dataConf['value'].'" class="m-l-10 '.($dataConf['classCss'] ? $dataConf['classCss'] : '').' datePickerConfig" /></label></span>';
					break;

				case 'text':
					$strForm.='<span class="col-sm-4 pull-left m-t-10"><label class=""><span class="pull-left c-bluegray">'.constant('__PLUGINCHART_FORMUSERCONFIG_LABEL'.strtoupper($paraConf)).':</span>
						<input name="'.$paraConf.'" type="text" value="'.$dataConf['value'].'" class="m-l-10 '.($dataConf['classCss'] ? $dataConf['classCss'] : '').' datePickerConfig" /></label></span>';
					break;

				case 'hide':
					if($paraConf === 'config_version')
					{
						$tmpVersion=(int)$dataConf['value'];
						$strForm.='<input name="'.$paraConf.'" type="hidden" value="'.(++$tmpVersion).'" />';
					}
					else
						$strForm.='<input name="'.$paraConf.'" type="hidden" value="'.$dataConf['value'].'" />';
					break;

				default:
					$strFormError.='<div class="pull-left w-100 alert alert-danger"><i class=\'fa fa-warning f-20 m-r-10 m-b-0 p-0 c-white \'></i><span class=c-amber>ERROR, parece que hay algo en: </span>'.$paraConf.'</div>';
			}
		}
	}

	$strForm.='</div>'.$strFormError;
	$strForm.='<br /><div class="m-t-20 pull-left w-100 f-14 '.$_SESSION['userAdmin']['pref_']['btn_default_forCol'].' p10 text-center">'.__PLUGINCHART_FORMUSERCONFIG_INFOUPDATEUSERCONFIGTXT.'</div>';

	$additionalConfigCommandsAvailable='<button type="button" class="btn btn-xs btn-default '.$_SESSION['userAdmin']['pref_']['btn_default_forCol'].' c-orange" name="resetConfigFile" >'.__PLUGINCHART_FORMUSERCONFIG_BTNCNFRESETTXT.'</button>';
	$additionalConfigCommandsAvailable.='<button type="submit" onclick="instantUpdate=1;" class="btn btn-xs btn-default '.$_SESSION['userAdmin']['pref_']['btn_default_forCol'].' '.$_SESSION['userAdmin']['pref_']['btn_default_bckCol'].'" name="instanCongigUpdate" >'.__PLUGINCHART_FORMUSERCONFIG_BTNINSTANTAPPLYTXT.'</button>';
	$additionalConfigCommandsAvailable.='<button type="submit" onclick="instantUpdate=0;" class="btn btn-xs btn-default '.$_SESSION['userAdmin']['pref_']['btn_default_forCol'].' '.$_SESSION['userAdmin']['pref_']['btn_default_bckCol'].'" name="standardUpdate" >'.__COMMON_BTN_SAVE.'</button>';
//echo $strDataFile.'<br>LEIDO....: '.$tmpFileName;
//print_r(json_decode($strDataFile));
}
else
{
	$resCreate=mkdir($tmpFolder, 0775);	// SI NO EXISTE, SE CREA LA CARPETA PARA EL USUARIO ACTUAL
//	if($resCreate)	// FOLDER CORRECT CREATION
	// SE CREA EL ARCHIVO PARA EL USUARIO ACTIVO CON LA CONFIGURACION BASICA POR DEFECTO
	{
		$strDataFile=json_encode($objUserChartConf, true);

		$fileHandle=fopen($tmpUserConf, 'a+');
		fwrite($fileHandle, utf8_decode($strDataFile));
		fclose($fileHandle);
		$strForm.=__PLUGINCHART_FORMUSERCONFIG_NEWCONFIGCREATIONMSG.'<b>'.$arraExtVars['modLabel'].'</b>';
		$additionalConfigCommandsAvailable='<span class="m-r-20 m-t-10 f-12 c-red">'.__COMMON_GENERICTEXT_RELOADTXT.' '.$arraExtVars['modLabel'].'</span>';

//		$additionalConfigCommandsAvailable='<button type="button" class="btn btn-xs btn-default '.$_SESSION['userAdmin']['pref_']['btn_default_forCol'].' '.$_SESSION['userAdmin']['pref_']['btn_default_bckCol'].'" onclick="window.parent.parent.document.getElementsByClassName(\'refreshCurrentModule\').addEventListener("click");" >'.__COMMON_GENERICTEXT_RELOADTXT.' '.$arraExtVars['modLabel'].'</button>';

//		$additionalConfigCommandsAvailable='<button type="button" class="btn btn-xs btn-default '.$_SESSION['userAdmin']['pref_']['btn_default_forCol'].' '.$_SESSION['userAdmin']['pref_']['btn_default_bckCol'].'" onclick="window.parent.top.$(\'.refreshCurrentModule\').click();" >'.__COMMON_GENERICTEXT_RELOADTXT.' '.$arraExtVars['modLabel'].'</button>';

//echo '<br>creado: '.$tmpFileName;
//echo '<br>';print_r(json_decode($strDataFile));
	}
//echo $tmpFolder.$tmpFileName.')<br>'.$tmpUserConf;
}

if(is_array($objUserChartConf))	// INDISPENSABLE NORMALIZAR EL OBJETO
{
	$bufferPersanolaConf=json_decode(json_encode($objUserChartConf), FALSE);
	$objUserChartConf=new stdClass();
	$objUserChartConf=$bufferPersanolaConf;
	unset($bufferPersanolaConf);
}
// END PERSONAL USER CONFIG PLUGIN

//echo '---> enable_local_ip ='.isset($arraExtVars['enable_local_ip']);

/**
 * START AJUSTE DE LAS VARIABVLES SEGUN EL ORIGEN DE LA APERTURA DEL PLUGIN
 */
$arraExtVars['enable_local_ip']=isset($objUserChartConf->param_enable_local_ip->value) && !isset($arraExtVars['enable_local_ip']) ? $objUserChartConf->param_enable_local_ip->value : $arraExtVars['enable_local_ip'];
$_REQUEST['enable_local_ip']=$arraExtVars['enable_local_ip'];
// END AJUSTE DE LAS VARIABVLES SEGUN EL ORIGEN DE LA APERTURA DEL PLUGIN




if(isset($arraExtVars['chartParam_StartDate']) && $arraExtVars['chartParam_StartDate'])
{
	$moduleSetting->pluginsTopIndex['sqlParams']['defaultDateBetweenFrom']=$arraExtVars['chartParam_StartDate'];
}

if(isset($arraExtVars['chartParam_EndDate']) && $arraExtVars['chartParam_EndDate'])
{
	$moduleSetting->pluginsTopIndex['sqlParams']['defaultDateBetweenTo']=$arraExtVars['chartParam_EndDate'];
}

if($_SESSION['userAdmin']['pref_']['b_debugSession'] && $_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD)	// DEBUG::SOLO PARA MAXIMO NIVEL DE ADMINISTRACION
{
	$strHeaderControls.='<span><a href=\'debugPlugInVars\' class=\'btn btn-xs bgm-indigo waves-effect openModaldebugPlugIn\' data-toggle=\'modal\'><i class=\'zmdi zmdi-bug \'></i> Ver variables</a></span>';
	$strHeaderControls.='<span><a href=\'debugPlugInSql\' class=\'btn btn-xs m-l-10 bgm-indigo waves-effect openModaldebugPlugIn\' data-toggle=\'modal\'><i class=\'fa fa-database f-10 \'></i> Ver sql</a></span>';
}

// HEADER ADDITIONAL DATE FILTER
if($moduleSetting->pluginsTopIndex['allowedFilterDates'] === '_true')
{
//form-control input-append col-lg-1 pull-left
	$strHeaderControls.='<span class=\'pull-right m-l-10\'><input id=\'chart_range_date\' data-name=\'chart_range_date\' name=\'chart_range_date\' value=\'\' data-date-format=\'YYYY-MM-DD\' class=\'form-control date-picker-chart-filter f-12 c-gray bgm-white\' title=\'Data da ... a\' style=\'width:150px;text-align:center;\' type=\'text\' /></span>';

	$jsCode.='
setTimeout("window.parent.datePickerChart(\''.date('d/m/Y',strtotime($moduleSetting->pluginsTopIndex['sqlParams']['defaultDateBetweenFrom'])).'\',\''.date('d/m/Y',strtotime($moduleSetting->pluginsTopIndex['sqlParams']['defaultDateBetweenTo'])).'\');",1000);
';
}



//echo '---> is_array ='.is_array($objUserChartConf);echo isset($arraExtVars['enable_local_ip']);
	$strHeaderControls.='<div class=\'toggle-switch pull-right m-l-10 m-r-5\' data-ts-color=\'red\' ><input id=\'enable_local_ip\' data-name=\'enable_local_ip\' name=\'enable_local_ip\' value=\'1\' class=\'\' title=\'Habilitar/deshabilitar IP local\' type=\'checkbox\'  '.($arraExtVars['enable_local_ip'] == 1 ? 'checked' : '').'/><label for=\'enable_local_ip\' class=\'ts-helper\'></label></div>';


//echo '<pre>pluginsTopIndex->sqlParams:: ';print_r($moduleSetting->pluginsTopIndex['sqlParams']['defaultDateBetweenFrom']);echo'</pre>';
$resBdData=$dataSiteBBDD->getInitData($moduleSetting->pluginsTopIndex['sqlParams']);
$sqlParamsSerialized=json_encode($moduleSetting->pluginsTopIndex['sqlParams']);

if(!count($resBdData))	// SI NO HAY DATOS, SE DEBE INICIALIZAR EL ARRAY
{
	$resBdData=[0];
}

$jsNumElems=count($resBdData);

$htmlSpecificLibraries.='<link href="'.$web_url.$adminFolder.'/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />';
$htmlSpecificLibraries.='<script src="'.$web_url.$adminFolder.'/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>';

?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath']?>/js/highstock.js"></script>
<script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath']?>/js/modules/exporting.js"></script>


<?php
// START COMMON AND VENDOR CSS & JAVASCRIPT COMMON LIBRARIES
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_comm_vendor_CSS.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_javascript_libraries.php');
// END COMMON AND VENDOR CSS & JAVASCRIPT COMMON LIBRARIES

$htmlSpecificLibraries.='<link href="'.$web_url.$adminFolder.'/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />';
$htmlSpecificLibraries.='<script src="'.$web_url.$adminFolder.'/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';

echo $additionalScripts;
echo'<style>.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){width:99%!important;}</style>';	// NECESARIO X v.1.11.2 (2016.09.13 M.F.)
echo $htmlSpecificLibraries;


/**
 * NO DATA FOR SELECTED PERIOD
 */
if(!$resBdData[0])
{
	echo '<div class="content p-20 error" style="height:100vh;min-width:310px;"><center><h2 class="alert-warning p-20">'.__PLUGINCHART_NODATATITLE_TXT.'</h2></center><p>'.__PLUGINCHART_NODATAEXPLAIN_PRETXT.'<b>'.($highgChartTimeLapse/1000).'</b>'.__PLUGINCHART_NODATAEXPLAIN_POSTXT.'</p>';

	if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD)
	{
		echo '<hr />';
		echo $dataSiteBBDD->query;
	}
	echo '</div>';
}

/**
 * START OBTENER EL TIPO DE GRAFICO SELECCIONADO
 * PARA PODER DIBUJARLO OTRA VEZ AL RECARGAR LA PAGINA
 */
$listChartTypes='\''.$prefixChartParams.'line\',\''.$prefixChartParams.'column\',\''.$prefixChartParams.'spline\',\''.$prefixChartParams.'area\',\''.$prefixChartParams.'areaspline\',\''.$prefixChartParams.'scatter\'';
$arrChartTypes=str_replace('\'','',$listChartTypes);
$arrChartTypes=explode(',',$arrChartTypes);
//echo'<pre>';print_r($arrChartTypes);
$currChartType='column';

if(isset($arraExtVars))
{
	foreach($arraExtVars as $reqField=>$reqVal)
	{
		if(in_array($reqField, $arrChartTypes) && $reqVal)	// && $reqVal
			$currChartType=substr($reqField,11);
	}
}
// END OBTENER EL TIPO DE GRAFICO SELECCIONADO

?>

<!--<link href="<?=$web_url.$adminFolder?>/css/app_2.min.css" rel="stylesheet">
<link href="<?=$web_url.$adminFolder?>/css/dujok.min.css" rel="stylesheet">-->
<link href="<?=$web_url.$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath']?>/css/highcharts.css" rel="stylesheet">

<style>
.highcharts-credits{display:none;}
.tmpTblData>tbody>tr:nth-child(odd){background-color:dimgrey;color:#ddd;}
.tmpTblData>tbody>tr:nth-child(even){color:#ddd;}
.tmpTblData>tbody>tr:nth-child(even):hover{background-color:#222;color:#eee;}
.tmpTblData>tbody>tr:nth-child(odd):hover{background-color:#777;color:#fff;}

.datepicker, .table-condensed{
font-size:11px;
padding:0px;
margin:0px;
}

.bootstrap-datetimepicker-widget td{
font-size:11px;
padding:0px;
margin:0px;
}

.bootstrap-datetimepicker-widget table{
background-color:#eee;
}
</style>

<script type="text/javascript">
<!--
var pluginFolder="<?=$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath'].'/'?>";

function openPluginModal(el)
{
	$("#"+el).modal('toggle');
}

function openChartConfig(el)
{
	$("#"+el).modal('toggle');
}

function createInputParam(el, fName="reloadPlugin")	//	CREATE DYNAMICALLY NEW INPUT TAGS
{
	if($("input[name="+el+"]").length) return;	// IF TAG EXIST, SLIP NEW CREATION
	$("#"+fName).append('<input type="hidden" name="'+el+'" value="0" />');
//console.log("creado: ",el)
	return;
}

function toggleInputParam(el, fName="reloadPlugin")	// TOGGLE INPUT LOGIC VALUES
{
	var tmpVal=$("input[name="+el+"]").val();
	tmpVal=(tmpVal == "false") ? 0 : tmpVal;
	tmpVal=parseInt(tmpVal) == 0 ? 1 : 0;
	$("input[name="+el+"]").val(tmpVal);
	$("button[name="+el+"]").attr("data-value", tmpVal);
	return tmpVal;
}

var instantUpdate=false;

$(document).ready(function(){
	window.parent.$(".reloadCurrentModal").css("color","");
	window.parent.$(".currentAction").html("");
	window.parent.$(".otherHeaderCommands").fadeIn();
	window.parent.$(".headerPlugin").html("<span class='pull-left'><i class='zmdi zmdi-chart <?=$foreAutoColor?>'></i> <?=__PLUGINCHART_JSCHARTTITLE_TXT.' `'.$arraExtVars['modLabel']?>`&nbsp;</span> <?=$strHeaderControls?>");
//	window.parent.$(".headerPlugin").append(' <button type="button" class="btn btn-xs btn-default waves-effect" title="<?=__COMMON_GENERICTEXT_RELOADTXT?>" onclick="$(\'.reloadCurrentModal\').click();"><i class="zmdi zmdi-refresh-sync f-18"></i></button>');

	window.parent.$("#ifrmHighChrarts").css("height","70vh");

	var tmpTypeRef="";
	var arrChartTypes=[<?=$listChartTypes?>];	// ALLOWED CHART TYPES

	// Set chart types
	$.each(arrChartTypes,function(i, type){
		tmpTypeRef=type.substr(11);
		createInputParam(type);

    $('.' + type).click(function(){
			var tmpByPoint=0;
			$.each(arrChartTypes,function(i, v){
//console.log("elem: ",i," valor= ",v);//colorByPoint
				if(v != type)
					$("input[name="+v+"]").val(0);
				else
				{
					tmpByPoint=0;
					if(v === "chartParam_column" || v === "chartParam_scatter")
						tmpByPoint=1;
					$("input[name="+v+"]").val(1);
				}
			});
//console.log(window.thisChart.options.series[0].colorByPoint)
			thisChart.series[0].update({
				type: type.substr(11)
				,colorByPoint:tmpByPoint
			});
    });
	});

	$('button[data-name=chart_auto_update]').click(function(){
		var locRes=0;
		createInputParam("chart_auto_update");
		locRes=toggleInputParam("chart_auto_update");

		if(locRes)
		{
			$("button[data-name=chart_auto_update]").removeClass("bgm-amber").addClass("bgm-gray");
			$("button[data-name=chart_auto_update]").text("<?=html_entity_decode(__PLUGINCHART_BTNCOMMAND_CHARTNOUPDATETXT)?>");
			window.parent.$(".currentAction").html("<span class=\'f-12 c-green m-l-5 \'><?=__PLUGINCHART_JSCURRENTAUTOUPDATEACTION_HTMLTXT?></span> <i class=\'zmdi zmdi-eye f-16 m-r-10 p-0 c-green \'></i> ");
			updateChartValues();
		}
		else
		{
			$("button[data-name=chart_auto_update]").removeClass("bgm-gray").addClass("bgm-amber");
			$("button[data-name=chart_auto_update]").text("<?=html_entity_decode(__PLUGINCHART_BTNCOMMAND_CHARTSTARTUPDATETXT.' '.($highgChartTimeLapse/1000))?>");
			clearInterval(setChartIntervalUpdates);
			window.parent.$(".currentAction").html("<span class=\'f-12 c-gray m-l-5 \'><?=__PLUGINCHART_JSCURRENTNOUPDATEACTION_HTMLTXT?></span> <i class=\'zmdi zmdi-eye-off f-16 m-r-10 p-0 c-blue \'></i> ");
		}
	});

	$(document).on("click",".hideSqlRow",function(){
		$(this).parent().parent().fadeOut(200);
	});

//	$('#button').click(function(){
    var selectedPoints = thisChart.getSelectedPoints();
////console.log("click punto.... ", selectedPoints)
//	});

	$("body").on("click",".updateCheckValue",function(){
		$("input[name="+this.name+"]").val(this.checked ? 1 : 0);
	});


//	$(".instanCongigUpdate").click(function(){
//	})

	$('form[name=pluginUserConfig]').on('submit', function(e){
		var dataSend=new FormData($("form[name=pluginUserConfig]")[0]);
		dataSend.append("action", "storeUserConf");
//		dataSend.append("moduleAdmin",'<?=$arraExtVars['moduleAdmin']?>');
		dataSend.append("currentConf",'<?=$plainDataUserConfig?>');

		$.ajax({
			data:dataSend
			,url:"<?=$web_url?>"+pluginFolder+"ajax_plugin.php"
			,method:"post",dataType:"json",cache:false,async:true,contentType:false,processData:false
			,success:function(response){
				if(!response.success)
				{
					commonNotify("<b><?=__COMMON_GENERICTEXT_ERROR?></b>", "<br /><br /><?=__PLUGINCHART_JSAJAXMSGERROR_COMMONTXT?>"+(response.error !== null ? response.error : "<?=__PLUGINCHART_JSAJAXMSGERROR_UNKNOWNERRORTXT?>"), undefined, "left", undefined, 'danger', "animated zoomInUp", "rotateOut",9000);
					return false;
				}

//console.log("updated OK")
				commonNotify("<?=__PLUGINCHART_JSAJAXMSGCONFIGOK_MSGTXT?>","", undefined, "center", undefined, 'success', "animated flipInX", "zoomOut", response.timeWait ? response.timeWait : 3000);
				openPluginModal("modalChartConfig");
				if(!instantUpdate)
				{
					setTimeout("parent.$('.reloadCurrentModal').click();",6000);
					swal('<?=__COMMON_NOTIFY_INFONOCONTENTSTITLE?>', '<?=strip_tags(__PLUGINCHART_FORMUSERCONFIG_INFOUPDATEUSERCONFIGTXT)?>'+"\n\n"+ '<?=__COMMON_GENERICTEXT_PLEASEWAIT?>', 'info');
				}
				else
					parent.$('.reloadCurrentModal').click();

				return false;
			}
			,error:function(response){
				commonNotify('<b><?=__COMMON_GENERICTEXT_ERROR?></b>', '<br /><br /><?=__PLUGINCHART_JSAJAXMSGERROR_FATALTXT?>'+response.readyState, undefined, "left", undefined, 'danger', "animated lightSpeedIn", "rotateOut",9000);
				return false;
			}
		});


//console.log("submit user config",e);
//console.log("submit user config",this);
		return false;
	});

	$("button[name=resetConfigFile]").click(function(){
		swal({
			title:"<?=__PLUGINCHART_JSFORMUSERCONFIG_CONFIRMRESETCFGTITLE?>",
			text:"<?=__PLUGINCHART_JSFORMUSERCONFIG_CONFIRMRESETCFGMSG?>",
			type:"warning",
			showCancelButton:true,
			confirmButtonText:"<?=__PLUGINCHART_JSFORMUSERCONFIG_CONFIRMRESETBTNCONFIRM?>",
			closeOnConfirm:true,
			closeOnCancel:true,
			timer:10000,
		},
		function(){
			$.ajax({async:true,url:"<?=$web_url?>"+pluginFolder+"ajax_plugin.php",method:"post",dataType:"json",data:{action:"resetCfgFile",config_name:"<?=$tmpUserConf?>"},cache:false,async:false
				,success:function(response){
					if(!response.success)
					{
						commonNotify('<b><?=__COMMON_GENERICTEXT_ERROR?></b>', '<br /><br /><?=__COMMON_AJAXMSG_GENERICERRORRESSQL?><br />'+(response.error ? response.error : "contactar infomática"), undefined, "left", undefined, 'danger', "animated lightSpeedIn", "rotateOut",9000);
						return false;
					}
					$("#modalChartConfig").modal('toggle');
					commonNotify('<?=__COMMON_NOTIFY_INFONOCONTENTSTITLE?>', '<br /><br /><?=__COMMON_NOTIFY_CORRECTOPERATION?>', undefined, "center", undefined, 'success', "animated flipInX", "zoomOut",3000);
					$("#modalChartConfig").html('<div class="modal-dialog modal-lg" style="max-height:60vh;width:30vw;"><div class="modal-content p-20 m-10 f-16 c-red"><center><h2><?=__COMMON_GENERICTEXT_WARNING?></h2><?=__PLUGINCHART_JSFORMUSERCONFIG_NOTIFICAIONAFTERRESETCFGMSG?></center></div></div>');
				},error:function(response){
					commonNotify('<b><?=__COMMON_GENERICTEXT_ERROR?></b>', '<br /><br /><?=__PLUGINCHART_JSAJAXMSGERROR_FATALTXT?>'+response.readyState, undefined, "left", undefined, 'danger', "animated lightSpeedIn", "rotateOut",9000);
					return false;
				}
			});
		});
	});

<?php
echo $jsUserChartConf;
?>

	window.parent.initializeFilters();
	//setTimeout("setChartColors();",1000);
});

-->
</script>

</head>
<body>
<div class="mainContainer" style="display:block;max-height:100vh!important;height:92%;overflow:auto;">

<form action="" method='post' id='reloadPlugin' name='reloadPlugin' onsubmit="" enctype='multipart/form-data'>
<?php
$outHtmlForm='';
foreach($arraExtVars as $fn=>$fv)
{
	$outHtmlForm.='<input type="hidden" name="'.$fn.'" value="'.$fv.'" />'."\r\n";
}

echo $outHtmlForm;
?>
</form>

	<!--<div id="container" style="height:80vh;min-width:310px;"></div>-->
	<div id="container" style="height:93vh;min-width:310px;"></div>
	<div class="containerCommands bgm-white f-12" style="height:4vh;width:auto;">
		<button class="btn btn-xs m-r-5 <?=$prefixChartParams?>column" data-name="<?=$prefixChartParams?>column" data-value="<?=isset($arraExtVars[$prefixChartParams.'column']) && $arraExtVars[$prefixChartParams.'column'] ? (int)$arraExtVars[$prefixChartParams.'column'] : 0 ?>"><?=__PLUGINCHART_BTNCOMMAND_COLUMNTXT?></button>
		<button class="btn btn-xs m-r-5 <?=$prefixChartParams?>line" data-name="<?=$prefixChartParams?>line" data-value="<?=isset($arraExtVars[$prefixChartParams.'line']) && $arraExtVars[$prefixChartParams.'line'] ? (int)$arraExtVars[$prefixChartParams.'line'] : 0 ?>"><?=__PLUGINCHART_BTNCOMMAND_LINETXT?></button>
		<button class="btn btn-xs m-r-5 <?=$prefixChartParams?>spline" data-name="<?=$prefixChartParams?>spline" data-value="<?=isset($arraExtVars[$prefixChartParams.'spline']) && $arraExtVars[$prefixChartParams.'spline'] ? (int)$arraExtVars[$prefixChartParams.'spline'] : 0 ?>"><?=__PLUGINCHART_BTNCOMMAND_SPILINETXT?></button>
		<button class="btn btn-xs m-r-5 <?=$prefixChartParams?>area" data-name="<?=$prefixChartParams?>area" data-value="<?=isset($arraExtVars[$prefixChartParams.'area']) && $arraExtVars[$prefixChartParams.'area'] ? (int)$arraExtVars[$prefixChartParams.'area'] : 0 ?>"><?=__PLUGINCHART_BTNCOMMAND_AREATXT?></button>
		<button class="btn btn-xs m-r-5 <?=$prefixChartParams?>areaspline" data-name="<?=$prefixChartParams?>areaspline" data-value="<?=isset($arraExtVars[$prefixChartParams.'areaspline']) && $arraExtVars[$prefixChartParams.'areaspline'] ? (int)$arraExtVars[$prefixChartParams.'areaspline'] : 0 ?>"><?=__PLUGINCHART_BTNCOMMAND_AREASPLINETXT?></button>
		<button class="btn btn-xs m-r-5 <?=$prefixChartParams?>scatter" data-name="<?=$prefixChartParams?>scatter" data-value="<?=isset($arraExtVars[$prefixChartParams.'scatter']) && $arraExtVars[$prefixChartParams.'scatter'] ? (int)$arraExtVars[$prefixChartParams.'scatter'] : 0 ?>"><?=__PLUGINCHART_BTNCOMMAND_SCATTERTXT?></button>
		<!--<button class="btn btn-xs chart_no_update bgm-gray"><?=__PLUGINCHART_BTNCOMMAND_CHARTNOUPDATETXT?></button>-->
		<button class="btn btn-xs <?=isset($arraExtVars['chart_auto_update']) && $arraExtVars['chart_auto_update'] ? 'bgm-gray' : 'bgm-amber' ?>" data-name="chart_auto_update" data-value="<?=isset($arraExtVars['chart_auto_update']) && $arraExtVars['chart_auto_update'] ? (int)$arraExtVars['chart_auto_update'] : 0 ?>"><?=isset($arraExtVars['chart_auto_update']) && $arraExtVars['chart_auto_update'] ? __PLUGINCHART_BTNCOMMAND_CHARTNOUPDATETXT : __PLUGINCHART_BTNCOMMAND_CHARTSTARTUPDATETXT.' '.($highgChartTimeLapse/1000) ?></button>

		<!--<button class="btn btn-xs start_chart_update bgm-amber c-black hide"><?=__PLUGINCHART_BTNCOMMAND_CHARTSTARTUPDATETXT.' '.($highgChartTimeLapse/1000)?>"</button>-->
		<!--<span class="m-l-10 bgm-white f-12" title="Siempre en milisegundos (divide x 1000 y obtienes los segundos), mínimo 10 segundos!">Intervalo: <input class="" name="chartIntervalCheckData" value="<?=$highgChartTimeLapse?>" min="10000" /></span>-->

	</div>

<?php

$txtChartTitle=__PLUGINCHART_CHARTTITLE_TXT.' '.$arraExtVars['modLabel'];

if($jsNumElems)
{
//echo'<pre>';print_r($resBdData);
	$jsOutData=''; $jsOutPointColor='';
	
	if($resBdData[0])
	{
		$jsOutData.='{';$jsOutDataHour='hour:[';$jsOutDataMinute='minute:[';$jsOutDataValue='value:[';$jsOutDataSecond='second:[';$jsOutDataDate='date:[';
		$jsOutPointColor='[';
//		$jsOutPointColor.='"#ccc","#ccc","#ccc","#ccc","#ccc",';

		foreach($resBdData as $k=>$value)
		{
//echo'<br>';print_r($value['ipUser']);
			$tmpValues[]=$value['valueCountChart'];
			$tmpHou=str_pad($value['hourObtained'],2,'0',STR_PAD_LEFT);
			$tmpMin=str_pad($value['dateObtained'],2,'0',STR_PAD_LEFT);
			$tmpSec=str_pad($value['secondObtained'],2,'0',STR_PAD_LEFT);
			$jsOutDataHour.='"'.$tmpHou.'",';
			$jsOutDataMinute.='"'.$tmpMin.'",';
			$jsOutDataValue.=''.$value['valueCountChart'].',';
			$jsOutDataSecond.='"'.$tmpSec.'",';
			$jsOutDataDate.='"'.$value['dateResult'].'",';
//echo '-'.$moduleSetting->pluginsTopIndex['configLocalChart']['localIpKey'].'|'.$value['ipUser'];

			if($value['ipUser'] == $moduleSetting->pluginsTopIndex['configLocalChart']['localIpKey'])	// IP LOCAL
			{
//	$jsOutPointColor.='{color:"red"},';
//				$jsOutPointColor.='"#cccccc",';
//				$jsOutPointColor.='{color:"rgb(0,100,55,.2)"},';
				$jsOutPointColor.='"rgb(0,100,55,.2)",';
//	$jsOutData.='color:\'red\',';
//echo'<p>'.$value['ipUser'];//print_r($resBdData);
			}
			else
				$jsOutPointColor.='"rgb(200,100,55,.5)",';
//				$jsOutPointColor.='"#000000",';
//	$jsOutPointColor.='"#FFCF83",';
			}

		$jsLastMoment='hour:"'.$tmpHou.'",minute:"'.$tmpMin.'",second:"'.$tmpSec.'",fieldName:"'.$moduleSetting->pluginsTopIndex['sqlParams']['fieldNameHours'].'"';
		$jsOutDataHour=substr($jsOutDataHour,0,-1).']';
		$jsOutDataMinute=substr($jsOutDataMinute,0,-1).']';
		$jsOutDataValue=substr($jsOutDataValue,0,-1).']';
		$jsOutDataSecond=substr($jsOutDataSecond,0,-1).']';
		$jsOutDataDate=substr($jsOutDataDate,0,-1).']';
		$jsOutPointColor=substr($jsOutPointColor,0,-1).']';

//		$jsOutData.=$jsOutDataHour.','.$jsOutDataMinute.','.$jsOutDataValue.','.$jsOutDataSecond.','.$jsOutDataDate.',color:'.$jsOutPointColor.'}';
//		$jsOutData.=$jsOutDataHour.','.$jsOutDataMinute.','.$jsOutDataValue.','.$jsOutDataSecond.','.$jsOutDataDate.',color:"red"}';
		$jsOutData.=$jsOutDataHour.','.$jsOutDataMinute.','.$jsOutDataValue.','.$jsOutDataSecond.','.$jsOutDataDate.'}';
	}
	else
	{
		$jsOutData.='{value:0}';
		$txtChartTitle=''.__COMMON_GENERICTEXT_WARNING.': <b>'.__COMMON_GENERICTEXT_NODATA.'</b>';
	}

	$jsMaxValue=max($tmpValues);
}


$jsOutPointColor=$jsOutPointColor? $jsOutPointColor : '""';	// EVITA ERRORES SI NO HAY DATOS EN EL RANGO A VISUALIZAR POR DEFECTO
//echo $jsOutPointColor;
$arrShortMonths=explode(',',__COMMON_DATESHORTMONTHS_LIST);
$strShortMonths='';

foreach($arrShortMonths as $shortMonth)
	$strShortMonths.='"'.$shortMonth.'",';

$strShortMonths=substr($strShortMonths,0,-1);
$displayTooltipDetail=0;


/**
 * CONOCER LA DIFERENCIA EN DIAS ENTRE LAS FECHAS DE INICIO Y FIN
 */
if(isset($arraExtVars['chartParam_StartDate']) && $arraExtVars['chartParam_StartDate'])
{
	$datetime1=new DateTime($arraExtVars['chartParam_StartDate']);
	$datetime2=new DateTime($arraExtVars['chartParam_EndDate']);
	$interval=$datetime1->diff($datetime2);
	$currentInterval=$interval->format('%a');
	$displayTooltipDetail=((int)$currentInterval <= 3) ? 1 : 0;
//	echo $currentInterval.' --- '.$displayTooltipDetail;
}


?>
<script type="text/javascript">
var highgChartMaxValue=<?=$jsMaxValue ? $jsMaxValue : 0?>;//1000;
var highgChartTimeLapse=<?=isset($highgChartTimeLapse) ? $highgChartTimeLapse : 10000?>;	// IN SECONDS TIME
var lastChartMoment={<?=$jsLastMoment?>};
var tmpUnixLastUpdate=0, unixMoment=0; tipWarningMoment=null;
var setChartIntervalUpdates={};

window.onbeforeunload=function(){
	delete Highcharts;
	delete thisChart;
};

// START HIGH-CHARTS
Highcharts.setOptions({
	global:{
		useUTC:false
	}
	,lang:{
		rangeSelectorFrom:"<?=__PLUGINCHART_LABELFROM_TXT?>"
		,rangeSelectorTo:"<?=__PLUGINCHART_LABELTO_TXT?>"
		,shortMonths:[<?=$strShortMonths?>]
	}
});

function updateChartValues()
{
	setChartIntervalUpdates=setInterval(function(){
		window.parent.$(".currentAction").html("<span class=\' f-10 f-700 m-r-10 p-0 msgBlink brd-2\' style=background:red!important;>&FilledSmallSquare;</span> ");

		var resAjax={};

		var tmpDbFilters=$("input[name^=dbFilter_]");
		var filterDbParams={};

		$.each(tmpDbFilters,function(i, v){
			var filterName=v.name;
			filterName=filterName.substr(9);
			filterDbParams[filterName]=v.value;
		});

		resAjax=$.ajax({
			url:"<?=$web_url?>"+pluginFolder+"ajax_plugin.php",
			method:"post", dataType:"json", cache: false,
			data:{action:"getSingleChartData",mainTableName:"<?=$moduleSetting->mainTable?>",filterDbParams:filterDbParams,sqlParams:<?=$sqlParamsSerialized?>,moduleFolder:"<?=$arraExtVars['modulePath']?>",pluginFolder:"<?=$folderRootApp?>"+pluginFolder,lastMoment:lastChartMoment}
			,success:function(response)
			{
				if(!response.success)
				{
					window.parent.$(".currentAction").html("<span class=\'tmpMsg p-l-15 f-12 p-r-5 p-b-10 msgBlink\' data-trigger=\'hover\' data-toggle=\'popover\' data-placement=\'top\' data-original-title=\'<?=__PLUGINCHART_JSAJAXMSGERROR_TITLETXT?>\' data-content=\'<?=__PLUGINCHART_JSAJAXMSGERROR_TITLECONTENT?>\'><?=__COMMON_GENERICTEXT_WARNING?> - <i class=\'zmdi zmdi-alert-circle f-20 m-r-10 m-b-0 p-0 c-red \'></i></span> ");
					window.parent.$('.tmpMsg').popover();
					return false;
				}

				tipWarningMoment=null;

				if(response.success && response.newMomentChartData)
				{
//					$(".sqlDebug").append("<span><br /><br /><span class=\'col-sm-12 mblm-item bs-item z-depth-1-top bgm-softyellow c-black\'>"+response.newUpdateTime+"<button type=\'button\' class=\'btn btn-xs btn-default pull-right hideSqlRow\'>x</button></span><br />"+response.query.sql[0]+"</span>");
//console.log("all updates OK: ",response)
					var arrData=eval("(function(){return " + response.newMomentChartData + ";})()");	// SERIALIZED DATA TO "arrData" OBJECT

					if(!response.newMomentChartData)
					{
//console.log("1.hacer que recoja el momento actual y setear: ",response.newMomentChartMaxValue)
						return false;
					}

					var res=composeChartData(arrData);

					var tmpData=[];

					for(ind=0; ind < res.length; ++ind)
					{
						tmpUnixMoment=res[ind][0];
						unixMoment=res[ind][0];

						if(tmpUnixLastUpdate == res[ind][0])
						{
							$(".sqlDebug").append("<span><br /><br /><span class=\'col-sm-12 mblm-item bs-item z-depth-1-top bgm-softyellow c-red f-700\'>"+response.newUpdateTime+" (+"+response.nextDateUpdate.value+")<button type=\'button\' class=\'btn btn-xs btn-default pull-right hideSqlRow\'>x</button></span><br />"+response.query.sql[0]+"</span>");
							tipWarningMoment="!!!!!!!!";
							unixMoment=res[ind][0]+1;
							window.parent.$(".currentAction").html("<span class=\'tmpMsg p-l-15 f-12 p-r-5 p-b-5 msgBlink\' title=\'<?=__PLUGINCHART_JSBTNUPDATETITLE_ALERTTXT?>\'><?=__COMMON_GENERICTEXT_WARNING?> <i class=\'zmdi zmdi-alert-circle f-20 m-r-10 m-b-0 p-0 c-amber \'></i></span> ");
							window.parent.$(".reloadCurrentModal").css("color","red");
							window.parent.$(".reloadCurrentModal").attr("title","<?=__PLUGINCHART_JSBTNUPDATETITLE_ALERTTXT?>");
							var x=new Date(response.arrNewIsoTime[0],response.arrNewIsoTime[1],response.arrNewIsoTime[2],response.arrNewIsoTime[3],response.arrNewIsoTime[4],response.arrNewIsoTime[5]).getTime();
//										var x=new Date(response.arrNewIsoTime[0],response.arrNewIsoTime[1],response.arrNewIsoTime[2],response.arrNewIsoTime[3],response.arrNewIsoTime[4],response.arrNewIsoTime[5]).getTime();
x=parseInt(response.newUnixTime+"000");
//console.log("!!!!!!!. series.addPoint= ",x,", ",response.nextDateUpdate.value," !! - ",response.arrNewIsoTime)
							thisChart.series[0].addPoint([x, Math.ceil(response.nextDateUpdate.value)], true, true);
						}
						else
						{
							$(".sqlDebug").append("<span><br /><br /><span class=\'col-sm-12 mblm-item bs-item z-depth-1-top bgm-softyellow c-black\'>"+response.newUpdateTime+" (+"+response.nextDateUpdate.value+")<button type=\'button\' class=\'btn btn-xs btn-default pull-right hideSqlRow\'>x</button></span><br />"+response.query.sql[0]+"</span>");

//										series.addPoint([unixMoment, res[ind][1]], true, true);
							thisChart.series[0].addPoint([unixMoment, res[ind][1]], true, true);
//console.log("STANDARD. series.addPoint= ",unixMoment,", ",res[ind][1]," OK - ",response.arrNewIsoTime,", response:",response.query.sql)
							tmpUnixLastUpdate=res[ind][0];
							window.parent.$(".currentAction").html("");
						}
					}
//console.log("lastChartMoment=",lastChartMoment)
					lastChartMoment={hour:arrData.hour[0],minute:arrData.minute[0],second:arrData.second[0],value:arrData.value[0],fieldName:"<?=$moduleSetting->pluginsTopIndex['sqlParams']['fieldNameHours']?>"};
					return false;
				}
				else	// NO NEW DATA, UPDATE LAST CHECK TIME
				{
					lastChartMoment={hour:response.nextDateUpdate['hour'],minute:response.nextDateUpdate['minute'],second:response.nextDateUpdate['second'],value:0,fieldName:response.nextDateUpdate['fieldName']};
					window.parent.$(".currentAction").html("");
				}
//return false;
			},error:function(response){
				window.parent.$(".currentAction").html("<span class=\'tmpMsg\' data-trigger=\'hover\' data-toggle=\'popover\' data-placement=\'top\' data-original-title=\'<?=__PLUGINCHART_JSAJAXMSGERROR_TITLETXT?>\' data-content=\'<?=__PLUGINCHART_JSAJAXMSGERROR_TITLECONTENT?>\'><?=__COMMON_GENERICTEXT_WARNING?> <i class=\'zmdi zmdi-alert-circle f-20 m-r-10 m-b-0 p-0 c-red msgBlink \'></i></span> ");
				window.parent.$('.tmpMsg').popover();
				return response.readyState;
			}
		});

//console.log("series.... ",series)
//					series2.addPoint([x, yy], true, true);
	}, highgChartTimeLapse);
}

function composeChartData(tmpData)
{
//console.log("--->tmpData: ",tmpData)
	var <?=$nameJsDataOne?>=tmpData; var eta_ms=0; var pointColor="red";
	var tmpData=[];
console.log("--->1.tmpData: ",<?=$nameJsDataOne?>)
	for(ind=0; ind < <?=$nameJsDataOne?>.value.length; ++ind)
	{
//console.log(ind," )---> ",<?=$nameJsDataOne?>.color[ind]);
//console.log(ind," )---> ",<?=$nameJsDataOne?>.date[ind]," ==> ",<?=$nameJsDataOne?>.date[ind].substr(8,2))
		eta_ms=new Date(<?=$nameJsDataOne?>.date[ind].substr(0,4),<?=$nameJsDataOne?>.date[ind].substr(5,2)-1,<?=$nameJsDataOne?>.date[ind].substr(8,2), <?=$nameJsDataOne?>.hour[ind], <?=$nameJsDataOne?>.minute[ind]).getTime();
		tmpData.push([
			eta_ms, <?=$nameJsDataOne?>.value[ind]
//			eta_ms, <?=$nameJsDataOne?>.value[ind],<?=$nameJsDataOne?>.color[ind]
		]);
	}
console.log("--->2.tmpData: ",tmpData)
	return tmpData;
}


function fnOpenChartElementDetails(refElem,pY,pDate,pTime)
{
	var pluginFolder="<?=$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath'].'/'?>";
	openPluginModal("modalChartElementDetail");
	$(".contentElementDetail").html("<center><i class='fa fa-circle-o-notch fa-spin fa-3x fa-fw f-16' ></i> <div class='p-t-20 p-b-20 c-white'><?=__PLUGINCHART_JSDETAILSLOADINGTITLE_TXT?> <b>"+pDate+"</b> <?=__PLUGINCHART_JSDETAILSTITLE_TOTXT?><b>"+pTime+"</b> ...</div></center>");
	$(".contentElementHeader").html("<?=__PLUGINCHART_JSDETAILSTITLE_FORTXT?> <b>"+pDate+"</b> <?=__PLUGINCHART_JSDETAILSTITLE_TOTXT?><b>"+pTime+"</b>");
	var methodParams={};
	methodParams.refElem="";
	methodParams.dataValue=pY;
	methodParams.actionDate=pDate;
	methodParams.actionTime=pTime;

	var tmpDbFilters=$("input[name^=dbFilter_]");
	var filterDbParams={};

	$.each(tmpDbFilters,function(i, v){
		var filterName=v.name;
		filterName=filterName.substr(9);
		filterDbParams[filterName]=v.value;
//console.log(filterName,"] tmpDbFilters= ",v.value)
	});
//console.log("filterDbParams = ",filterDbParams)

console.log("methodParams.....",$("input[name^=dbFilter_]"))
	resAjax=$.ajax({
		url:"<?=$web_url?>"+pluginFolder+"ajax_plugin.php",
		method:"post", dataType:"json", cache: false,
		data:{action:"getPointDetails",filterDbParams:filterDbParams, methodParams:methodParams, mainTableName:"<?=$moduleSetting->mainTable?>",sqlParams:<?=$sqlParamsSerialized?>,moduleFolder:"<?=$arraExtVars['modulePath']?>",pluginFolder:"<?=$folderRootApp?>"+pluginFolder}
		,success:function(response)
		{
			var outHtml="";
			var numRows=0;
			if(!response.data)
			{
				alert("error: NO AJAX DATA !!!!");
				return;
			}

			if(response.data.headerRows.length)	// CONTRUCCION DE LAS CABECERAS Y DATOS DE LOS DETALLES
			{
				outHtml+='<table class="tmpTblData table table-striped table-responsive table-condensed"><thead><tr>';
				$.each(response.data.headerRows,function(i, v){	// TABLE HEADER
					outHtml+='<th>'+v.Field+'</th>';
				});
				outHtml+='</tr></thead>';
				outHtml+='<tbody>';
				var arrTrDetails=new Array();

				$.each(response.data.rows,function(i, dataRow){	// TABLE DATA ROWS
					outHtmlTr='<tr>';

					$.each(dataRow,function(n, v){
						outHtmlTr+='<td>'+v+'</td>';
					});

					outHtmlTr+='</tr>';
					arrTrDetails[i]=outHtmlTr;
				});

				outHtml+='</tbody></table>';

				$(".contentElementDetail").html(outHtml);

					var timeT=200;

					$.each(arrTrDetails,function(n, v){
						setTimeout(function(){$(".contentElementDetail tbody").append(v)}, timeT)
						timeT+=(timeT/arrTrDetails.length*4)/2;
//						timeT+=100;
						numRows=n;
					});
				$(".contentElementHeader").append(" ("+(numRows+1)+" <?=__PLUGINCHART_JSDETAILSNUMROWSTITLE_VALUETXT?>)")
			}

			$(".sqlDebug").append("<span><br /><br /><span class=\'col-sm-12 mblm-item bs-item z-depth-1-top bgm-softyellow c-black\'>"+response.data.sqlTitle+"<button type=\'button\' class=\'btn btn-xs btn-default pull-right hideSqlRow\'>x</button></span><br />"+response.query.sql[0]+"</span>");

			return;
		},error:function(response){
			window.parent.$(".currentAction").html("<span class=\'tmpMsg c-red\' data-trigger=\'hover\' data-toggle=\'popover\' data-placement=\'top\' data-original-title=\'<?=__PLUGINCHART_JSAJAXMSGERROR_TITLETXT?>\' data-content=\'Parece que ha ocurrido un error al intentar recuperar los detalles de la fecha y momento solicitados....\'><?=__COMMON_GENERICTEXT_WARNING?> <i class=\'zmdi zmdi-alert-circle f-20 m-r-10 m-b-0 p-0 c-red msgBlink \'></i></span> ");
			window.parent.$('.tmpMsg').popover();
			$(".contentElementDetail").html("<center><h1><i class='fa fa-exclamation-triangle c-red' ></i></h1> <div class='p-t-20 p-b-20 c-orange f-16'>Se ha verificado un error al intentar recuperar los detalles de la fecha y el momento solicitados.</div></center>");
			return response.readyState;
		}
	});


//console.log(thisChart,"@@@@@@@@@@@@ = ",resAjax);
}
var btn_index=null;
// Create the chart

//var thisChart=new Highcharts();
var thisChart=Highcharts.stockChart('container',{
	xAxis:{
		events:{
			setExtremes: function(e){
				if(typeof(e.rangeSelectorButton) !== 'undefined'){
					var c=e.rangeSelectorButton.count;
					var t=e.rangeSelectorButton.type;
//					var btn_index=null;
					if(c == 5 && t == "minute"){
						btn_index=1;
					}else if(c == 10 && t == "minute"){
						btn_index=2;
					}else if(c == 1 && t == "hour"){
						btn_index=3;
					}else if(c == 1 && t == "day"){
						btn_index=4;
					}else if(c == 0 && t == "all"){
						btn_index=0;
					}

					// Store btn_index in a cookie here and use it
					// to initialise rangeSelector -> selected next
					// time the chart is loaded
					createInputParam("<?=$prefixChartParams?>extreme_range");
					$("input[name='<?=$prefixChartParams?>extreme_range']").val(btn_index);
//console.log(btn_index," )xAxis-events-setExtremes: [",c+" _ "+t+"]");
				}
			}
		}
	}
	,tooltip:{
		formatter:function(){
			var s=(tipWarningMoment == null ? "--" : "ATENCION!!!!")+" "+this.x;

//			if(Highcharts && <?=$displayTooltipDetail?>)
			if(Highcharts)
			{
				s=(tipWarningMoment == null ? "" : "<b><?=html_entity_decode(__COMMON_GENERICTEXT_WARNING)?></b><br /><?=html_entity_decode(__PLUGINCHART_JSCHARTTIP_WARNINGTXT)?><br /><br />")+Highcharts.dateFormat('%d/%m/%Y - <b>%H:%M', this.x) + '</b>';
				s+='<br/><?=defined($moduleSetting->pluginsTopIndex['chartParams']['tooltip1']) ? __PLUGINCHART_LABEL_1 : 'valor'?>: ' + this.y + '';
			}

			if(Highcharts && <?=($displayTooltipDetail == 0 ? 1 : 0)?> && !btn_index)
			{
				s="<?=__PLUGINCHART_LABELDAY_TXT?>: <b>"+Highcharts.dateFormat('%d/%m/%Y', this.x) + '</b>';
				s+='<br/><?=__PLUGINCHART_LABELMOREMINUTES_TXT?>: <b>'+this.y+'</b><br>(<?=__PLUGINCHART_LABELREDUCERANGE_TXT?>)';
			}
			return s;
		}
	},series:[
		{
			colorByPoint:<?=(isset($arraExtVars[$prefixChartParams.'column']) && $arraExtVars[$prefixChartParams.'column'] == 1 || isset($arraExtVars[$prefixChartParams.'scatter']) && $arraExtVars[$prefixChartParams.'scatter'] == 1 && $jsOutPointColor) ? 1 : 0 ?>,
			cursor:'pointer',

			data:(function(){
				var data=[]; var <?=$nameJsDataOne?>={}; var eta_ms=0;
				data=composeChartData(<?=$jsOutData?>);
				return data;
			}())
    ,events:{
        click:function(event){
//console.log(event.point.index,"] event.point.series.data[: ",event.point.y)
					var tmpPoint=event.point.series.data[event.point.index];//[event.point.index].select();
					var tmpDate=Highcharts.dateFormat('%Y-%m-%d', event.point.x);
					var tmpTime=Highcharts.dateFormat('%H:%M', event.point.x);
					$(".dynamicChartEvents").append("Click en elemento: <b>"+event.point.index+"</b><br>valor: <i>"+event.point.y+"</i><br>fecha: <i>"+tmpDate+"</i><br>Time: <i>"+tmpTime+"</i><hr />")
					fnOpenChartElementDetails(event.point.index,event.point.y,tmpDate,tmpTime);
        }
			}

			,colors:<?=$jsOutPointColor?>

		}
	]

	,chart:{
		type:'<?=$currChartType?>'

		,plotOptions:{
			series:{
				fillOpacity:0.1
			}
    }
		,events:{
			load:function(){
/**
 * HACER LA CARGA DESDE EL REQUEST CON LA VARIABLE SETEADA ANTERIORMENTE EN EL GRAFICO
 */
//				this.rangeSelector.clickButton(<?=isset($arraExtVars[$prefixChartParams.'extreme_range']) && $arraExtVars[$prefixChartParams.'extreme_range'] ? (int)$arraExtVars[$prefixChartParams.'extreme_range'] : '4' /* SET CHART RANGE */?>, true);
				if("<?=(int)$arraExtVars[$prefixChartParams.'extreme_range']?>" > 0 )
					this.rangeSelector.clickButton(<?=isset($arraExtVars[$prefixChartParams.'extreme_range']) ? (int)$arraExtVars[$prefixChartParams.'extreme_range'] : '4' /* SET CHART RANGE */?>, true);

				var series=this.series[0];
//	$('.highcharts-range-selector-buttons').each(function(i, v){
//console.log("-----====",series[0])
//	});
//this.series[0].update();
				<?=isset($arraExtVars['chart_auto_update']) && $arraExtVars['chart_auto_update'] ? 'updateChartValues();' : '' /* AUTO UPDATE AJAX NEW DATA */?>
//				series.series[0].update();
//if(thisChart.series[0].data.length)
{
//	this.options.plotOptions.colorByPoint=1;
//	this.options.chart.plotOptions.colorByPoint=1
//	this.series.plotOptions.colorByPoint=1
//console.log(this.options.chart.plotOptions.colorByPoint)
//colorByPoint
//this.series.colorByPoint=1;
	//setTimeout("setChartColors();",10)
//console.log(this.series)
}

			}
		}
	}

	,rangeSelector:{
		buttons:[
		,{
			count:5,
			type:'minute',
			text:'5 M'
		},{
			count:10,
			type:'minute',
			text:'10 M'
		},{
			count:1,
			type:'hour',
			text:'1 H'
		},{
			count:1,
			type:'day',
			text:'1 D'
		},{
			count:0,
			type:'all',
			text:'<?=__PLUGINCHART_LABELALL_TXT?> '
		}],
		inputEnabled:true
//		,selected:1
	}
	,scrollbar:{enabled:false}
	,title:{text:"<?=$txtChartTitle?> (<i><?=__PLUGINCHART_JSCHARTTITLE_TIMEUPDATETXT?> <?=($highgChartTimeLapse/1000)?>''</i>)"}
	,exporting:{
		enabled:false
	}

});
// END HIGH-CHARTS


<?php
echo $jsCode;
?>

function setChartColors()
{
	console.log("function::setChartColors()");
//thisChart.options.series[0].colors[2]
console.log(" ]function::setChartColors ",window.thisChart.series[0])
//window.thisChart.options.series[0].colors=<?=$jsOutPointColor?>;
	$.each(window.thisChart.series[0].data, function(i, point){
//console.log(i,window.thisChart.options.series[0])
		if(i % 2)
		{
			window.thisChart.options.series[0].colors[i]="rgb(0,100,55,.2)";
//			point.graphic.attr({ fill: '#FF0000' });
		}
	});
	window.thisChart.redraw();
	//window.Highcharts.reflow();
}
</script>

</div>

<div class="modal fade left" id="debugPlugInSql" tabindex="-1" role="dialog" aria-labelledby="-------" aria-hidden="true" data-backdrop="false" data-keyboard="true">
	<div class="modal-dialog modal-lg" style="max-height:60vh;width:85vw;">
		<div class="modal-content">
			<div class="modal-header p-10">
				<span class="pull-left c-deeporange f-500 f-16">HISTORICO SQL`s</span>
				<span class="pull-right">
					<button class='btn btn-xs btn-danger c-white p-l-15 p-r-15 <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?>' data-dismiss='modal'>X</button>
				</span>
			</div>
			<div class="content p-10 c-clack sqlDebug" style="max-height:60vh;overflow:auto;">
<?php
echo $dataSiteBBDD->query;
?>
				</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?>" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
			</div>
		</div>
	</div>
</div>

<!-- MODAL DE CONFIGURACION DEL MODULO ACTUAL -->
<div class="modal fade" id="modalChartConfig" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" data-backdrop="true" data-keyboard="true">
	<div class="modal-dialog modal-lg" style="max-height:60vh;width:50vw;">
		<div class="modal-content">
			<form action="" method='post' name='pluginUserConfig' onsubmit="//return false;" enctype='multipart/form-data'>

			<div class="modal-header p-10 <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?>">
				<span class="pull-left <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> f-500 f-16"><i class='fa fa-cog fa-spin f-20' ></i> <?=__PLUGINCHART_CONFIGCHARTTITLE_TXT?></span>
				<span class="pull-right">
					<button class='btn btn-xs p-l-15 p-r-15 <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?>' data-dismiss='modal'>X</button>
				</span>
			</div>
			<div class="content p-20 c-black" style="height:65vh;overflow:auto;">
<?php
echo $strForm;
?>
				
			</div>
			<div class="modal-footer">
				<?=$additionalConfigCommandsAvailable?>
				<button type="button" class="btn btn-xs btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?>" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
			</div>
			</form>
		</div>
	</div>
</div>



<!-- MODAL CON DETALLES DEL MOMENTO SELECCIONADO DEL GRAFICO PRINCIPAL  -->
<div class="modal fade" id="modalChartElementDetail" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" data-backdrop="true" data-keyboard="true">
	<div class="modal-dialog modal-lg" style="min-height:60vh;max-height:60vh;width:80vw;">
		<div class="modal-content bgm-gray">
			<div class="modal-header p-10"><i class='pull-left fa fa-info-circle f-20 c-indigo' ></i>
				<span class="pull-left c-indigo f-500 f-16 contentElementHeader"> Detalles......</span>
				<span class="pull-right">
					<button class='btn btn-xs bgm-gray p-l-15 p-r-15' data-dismiss='modal'>X</button>
				</span>
			</div>
			<div class="content p-0 contentElementDetail bgm-black c-gray" style="max-height:60vh;overflow:auto;">
				<center><i class='fa fa-hourglass-o f-16 c-white' ></i></center>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?>" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
			</div>
		</div>
	</div>
</div>


<?php
/**
 * ONLY FOR GOD ADMINISTRATORS
 */
if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD)
{
?>
<div class="modal fade left" id="debugPlugInVars" tabindex="-1" role="dialog" aria-labelledby="-------" aria-hidden="true" data-backdrop="false" data-keyboard="true">
	<div class="modal-dialog modal-lg" style="max-height:60vh;width:85vw;">
		<div class="modal-content">
			<div class="modal-header ">
				<span class="pull-left c-deeporange f-500 f-16">Window debug vars</span>
				<span class="pull-right">
					<a href="#jsOutData" class='btn btn-default btn-xs'>jsOutData</a>
					<a href="#jsOutPointColor" class='btn btn-default btn-xs'>jsOutPointColor</a>
					<a href="#chartParams" class='btn btn-default btn-xs'>Chart config</a>
					<a href="#personalConfig" class='btn btn-default btn-xs'>Personal config</a>
					<a href="#resBdData" class='btn btn-default btn-xs'>resBdData</a>
					<a href="#arraExtVars" class='btn btn-default btn-xs'>arraExtVars</a>
					<a href="#objModuleParams" class='btn btn-default btn-xs'>objModuleParams</a>
					<a href="#req" class='btn btn-default btn-xs'>Request</a>
					<a href="#sess" class='btn btn-default btn-xs'>Session</a>
					<a href="#dynamicChartEvents" class='btn btn-default btn-xs'>Dynamic chart events</a>
					<button class='btn btn-xs btn-danger c-white p-l-15 p-r-15' data-dismiss='modal'>X</button>
				</span>
			</div>
			<div class="content" style="display:inline-block;overflow:auto;;width:100%;max-height:74vh;">
<?php
if($_SESSION['userAdmin']['pref_']['b_debugSession'] && 1)
{
	echo '<div class="card-body w-full p-10" style="white-space:pre-wrap;word-wrap:break-all;word-break:break-all;"><pre style="display:inline-block;float:left;width:99%;white-space:pre-wrap;word-wrap:break-all;word-break:break-all;">';
	echo'<a name="jsOutData" class=" f-400">$jsOutData: </a>'.$jsOutData;
	echo'<hr /><a name="jsOutPointColor" class=" f-400">$jsOutPointColor: </a>'.$jsOutPointColor;
	echo'<hr /><a name="chartParams" class=" f-400">CHART configuration: </a>';print_r($moduleSetting->pluginsTopIndex);
	echo'<hr /><a name="personalConfig" class=" f-400">Personal configuration: </a>';print_r($objUserChartConf);
	echo'<hr /><a name="resBdData" class=" f-400">$resBdData (CHART DATA): </a>';print_r($resBdData);
	echo'<hr /><a name="arraExtVars" class=" f-400">$arraExtVars: </a>';print_r($arraExtVars);
	echo'<hr /><a name="objModuleParams" class=" f-400">$objModuleParams: </a>';print_r($moduleSetting);
	echo'<hr /><a name="sess" class=" f-400">SESSION: </a>';print_r($_SESSION);
	echo'<hr /><a name="req" class=" f-400">REQUEST: </a>';print_r($arraExtVars);
	echo'<hr /><a name="dynamicChartEvents" class=" f-400">Dynamic chart events: </a><br /><span class="dynamicChartEvents"></span>';
	echo'</pre></div>';
}
?>
				</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?>" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
			</div>
		</div>
	</div>
</div>
<?php
}
?>


</body>
</html>

<?php
if($_SESSION['userAdmin']['pref_']['b_debugSession'] && $_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD && 0)	// DEBUG::SOLO PARA MAXIMO NIVEL DE ADMINISTRACION
{
	echo '<div class="card-body w-full p-10" style="white-space:pre-wrap;word-wrap:break-all;word-break:break-all;"><pre style="display:inline-block;float:left;width:99%;background:#FFFFEB;white-space:pre-wrap;word-wrap:break-all;word-break:break-all;">DEBUG DATA:<br/>';
	echo'<p>$jsOutData = '.$jsOutPointColor;
	echo'<p>$jsOutData = '.$jsOutData;
	echo'<p>$resBdData (CHART DATA)=';print_r($resBdData);
	echo'<p>SESSION=';print_r($_SESSION);
	echo'<p>$arraExtVars=';print_r($arraExtVars);
	echo'<p>$objModuleParams=';print_r($moduleSetting);
//print_r($dataSiteBBDD);
	echo'</pre></div>';
}
