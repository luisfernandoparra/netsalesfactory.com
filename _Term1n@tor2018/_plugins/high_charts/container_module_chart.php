<?php
/**
 * CHART PLUGIN
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$urlHighChartsIframe=$web_url.$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath'].'/ifr_module_chart.php?moduleAdmin='.$arraExtVars['moduleAdmin'].'&modLabel='.$arraExtVars['modLabel'];
$urlHighChartsIframe=html_entity_decode($urlHighChartsIframe);
$additionalScripts=''; $jsCode=''; $htmlSpecificLibraries='';

$htmlSpecificLibraries.='<link href="'.$web_url.$adminFolder.'/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />';
$htmlSpecificLibraries.='<script src="'.$web_url.$adminFolder.'/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>';
echo'<style>.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){width:99%!important;}</style>';	// NECESARIO X v.1.11.2 (2016.09.13 M.F.)

echo $htmlSpecificLibraries;

//echo $web_url.$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath'];
// HEADER ADDITIONAL DATE FILTER
if($moduleSetting->pluginsTopIndex['allowedFilterDates'] === '_true')
{
	echo '<script type="text/javascript" src="'.$web_url.$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath'].'/js/daterangepicker.min.js"></script>';
	echo '<link rel="stylesheet" type="text/css" href="'.$web_url.$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath'].'/css/daterangepicker.css" />';

	/**
	 * PLUG-IN PARA SELECCIONAR RANGOS DE FECHAS RAPIDAMENTE
	 *
	 * http://www.daterangepicker.com/#options
	 */
	$jsCode.='
function datePickerChart(dateFrom, dateTo)
{
	$(".date-picker-chart-filter").daterangepicker({
		timePicker:false
		,startDate:dateFrom ? dateFrom : "'.date('d/m/Y').'"
		,endDate:dateTo ? dateTo : "'.date('d/m/Y').'"
		,maxDate:"'.date('d/m/Y').'"
		,opens:"right"
		,autoApply:true
//		,timePickerIncrement: 90
//		,locale:{format:"MM-DD-YYYY"}
	}
	,function(start, end, label){
//console.log("nuova data selezionata: " + start.format("YYYY-MM-DD") + " to " + end.format("YYYY-MM-DD"));
		document.getElementById("ifrmHighChrarts").contentWindow.createInputParam("chartParam_StartDate");
		document.getElementById("ifrmHighChrarts").contentWindow.createInputParam("chartParam_EndDate");
		document.getElementById("ifrmHighChrarts").contentWindow.$("input[name=chartParam_StartDate]").val(start.format("YYYY-MM-DD"));
		document.getElementById("ifrmHighChrarts").contentWindow.$("input[name=chartParam_EndDate]").val(end.format("YYYY-MM-DD"));
	}
);

/*
	$(".date-picker-chart-filter").datetimepicker({
		format:"DD/MM/YYYY"
		,locale:""
	});
//.datepicker{background-color: #ccc;}
*/
}
';
}

/**
 * START BLOCK CUSTOM FILTERS
 */
foreach($moduleSetting->arrFieldsFormModal as $mainField=>$tmpData)
{
	$fieldsCurrentTable[]=$mainField;
}

$taglHtmlDisplayFields='<div class="m-l-15 bgm-white w-100"><select id="dbFilter_hideDetailsFields" name="dbFilter_hideDetailsFields" class="selectpicker p-l-5" multiple="multiple" title="'.__PLUGINCHART_HIDEDETAILSFIELDS_PLACEHOLDER.'">';

foreach($fieldsCurrentTable as $fieldName)
	$taglHtmlDisplayFields.='<option value="'.$fieldName.'">'.$fieldName.'</option>';

$taglHtmlDisplayFields.='</select></div>';
// END BLOCK CUSTOM FILTERS

?>
<style>


.msgBlink{
  background-color:transparent;
  color: red;
  display: inline-block;
  text-align: center;
}
@-webkit-keyframes glowing{
  0%{background-color:transparent; -webkit-box-shadow: 0 0 3px #ffffee;}
  50%{background-color:transparent; -webkit-box-shadow: 0 0 20px #FF0000;}
  100%{background-color:transparent; -webkit-box-shadow: 0 0 3px #ffffee;}
}

@-moz-keyframes glowing{
  0%{background-color:transparent; -moz-box-shadow: 0 0 3px #ffffee;}
  50%{background-color:transparent; -moz-box-shadow: 0 0 20px #FF0000;}
  100%{background-color:transparent; -moz-box-shadow: 0 0 3px #ffffee;}
}

@-o-keyframes glowing{
  0%{background-color:transparent; box-shadow: 0 0 3px #ffffee;}
  50%{background-color:transparent; box-shadow: 0 0 20px #FF0000;}
  100%{background-color:transparent; box-shadow: 0 0 3px #ffffee;}
}

@keyframes glowing{
  0%{background-color:transparent; box-shadow: 0 0 3px #ffffee;}
  50%{background-color:transparent; box-shadow: 0 0 20px #FF0000;}
  100%{background-color:transparent; box-shadow: 0 0 3px #ffffee;}
}

.msgBlink{
  -webkit-animation: glowing 200ms infinite;
  -moz-animation: glowing 200ms infinite;
  -o-animation: glowing 200ms infinite;
  animation: glowing 200ms infinite;
}

select.selectpicker{display: inline !important;}	/* importante para permitir visualizar otros selectores presentes en el modulo abierto*/
</style>

<script>
$(document).ready(function(){
	// STORE SRC URL IN TO IFRAME
	$(".modalHighCharts").click(function(el){
		$("#ifrmHighChrarts").attr("src",'<?=$urlHighChartsIframe?>');
	});

	$("#contentHighCharts").on("hide.bs.modal",function(el){
		//	UNLOAD PLUGIN SCRIPT
		document.getElementById("ifrmHighChrarts").src="_blank.html";
	});

// BUTTON RELOAD MODAL
	$(".reloadCurrentModal").click(function(){
		document.getElementById("ifrmHighChrarts").contentDocument.forms.reloadPlugin.submit();
	});

	$(document).on("click",".openModaldebugPlugIn",function(){
		document.getElementById("ifrmHighChrarts").contentWindow.openPluginModal($(this).attr("href"));
	});

	$(".pluginChartConfig").click(function(){	// OPEN MODAL "modalChartConfig"
		document.getElementById("ifrmHighChrarts").contentWindow.openChartConfig("modalChartConfig");
	});

	$("#dbFilter_hideDetailsFields").change(function(){	// FIELDS TO EXCUTE FROM DETAILS TABULAR DATA LIST
		var tmpList='';
		$.each(this.options,function(i, v){
			if(v.selected)
				tmpList+=v.value+",";
		});

		tmpList=tmpList.slice(0,-1);
		document.getElementById("ifrmHighChrarts").contentWindow.$("input[name=dbFilter_hideDetailsFields]").val(tmpList);
	});

	$("body").on("click","#enable_local_ip",function(){	// FIELDS TO EXCUTE FROM DETAILS TABULAR DATA LIST
		document.getElementById("ifrmHighChrarts").contentWindow.$("input[name=enable_local_ip]").val(this.checked ? 1 : 0);
		document.getElementById("ifrmHighChrarts").contentWindow.$("input[name=dbFilter_enable_local_ip]").val(this.checked ? 1 : 0);
	});

});

<?php
echo $jsCode;
?>

function initializeFilters()	// SE INICIALIZAN LOS CAMPOS ADICIONALES NECESARIOS PARA EL FORMULARIO
{
	document.getElementById("ifrmHighChrarts").contentWindow.createInputParam("dbFilter_hideDetailsFields");
	document.getElementById("ifrmHighChrarts").contentWindow.createInputParam("enable_local_ip");
	document.getElementById("ifrmHighChrarts").contentWindow.createInputParam("dbFilter_enable_local_ip");
}
</script>


<div class="modal fade" id="contentHighCharts" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="true">
	<div class="modal-dialog w-100 m-10 p-t-30">
		<div class="modal-content">
			<div class="modal-header <?=$_SESSION['userAdmin']['pref_']['top_header_theme_bckCol']?>">
				<div class="modal-title <?=$foreAutoColor?> headerPlugin pull-left" style='width:auto!important;font-size: 23px;font-weight: 500;line-height: 1.1;'>
					<span class="">
						<div class="preloader pls-white c-res"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>
					</span>
						 Cargando...
				</div>
				<span class="otherHeaderCommands hide pull-left m-r-10"><?=$taglHtmlDisplayFields?></span>
				<button type="button" class="btn btn-default waves-effect pull-left m-l-20" title="<?=__COMMON_GENERICTEXT_RELOADTXT?>" onclick="$('.reloadCurrentModal').click();"><i class="zmdi zmdi-refresh-sync f-18"></i></button>
				<div class="fltBtnClose">
					<button type="button" class="btn btn-default bg-black-trp <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> pluginChartConfig" title="<?=__PLUGINCHART_BTNSETTINGSTITLE_TXT?>"><i class='zmdi zmdi-settings <?=$foreAutoColor?> f-18'></i></button>&nbsp;&nbsp;
					<button class='btn btn-icon bg-black-trp <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect waves-circle actCancel' data-dismiss='modal'><i class='zmdi zmdi-close <?=$foreAutoColor?>'></i></button>
				</div>
			</div>

			<div class="modal-body" style="overflow:hidden">
<?php
//$urlHighChartsIframe=$web_url.$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath'].'/ifr_module_chart.php?web_url='.$web_url.'&pluginParams='.serialize($moduleSetting->pluginsTopIndex).'&moduleParams='.json_encode($moduleSetting).'';
?>
				<iframe style='width:100%!important;' id='ifrmHighChrarts' name='ifrmHighChrarts' src='#' frameborder='0' marginheight='0' marginwidth='0' scrolling='no' width='100%' height='10%'></iframe>
			</div>

			<div class="modal-footer">
				<span class="currentAction"></span>
				<button type="submit" class="btn btn-default reloadCurrentModal waves-effect" title="<?=__COMMON_GENERICTEXT_RELOADTXT?>"><i class="zmdi zmdi-refresh-sync f-18"></i></button>
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> actCancel" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
			</div>
		</div>
	</div>
</div>
