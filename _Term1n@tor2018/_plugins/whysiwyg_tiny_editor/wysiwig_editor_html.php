<?php
/**
 * TINY WYSIWIG PLUG-IN HTML FILE
 *
 * 2017.06.14 (M.F.)
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$tmpCGR=__COMMON_GENERICTEXT_RETRIEVINGDATA;
$tmpCTN=__COMMON_GENERICTEXT_NOTES;
$tmpCTU=__COMMON_GENERICTEXT_UPDATE;
$tmpCBN=__COMMON_BTN_CANCEL;
$tmpCBC=__COMMON_BTN_CLEAN;
$tmpUPBFC=$_SESSION['userAdmin']['pref_']['btn_default_forCol'];
$tmpUPBBC=$_SESSION['userAdmin']['pref_']['btn_default_bckCol'];

$htmlWysiwigEditor= "
<div class='modal fade p-t-10 p-l-10' id='htmlWysiEditBox' tabindex='-1' role='dialog' aria-hidden='true' data-backdrop='dynamic' data-keyboard='false'>
	<div class='modal-dialog modal-lg'>
		<div class='modal-content'>
			<div class='modal-header commonHeaderWysiEdit'>
				<h4 class='modal-title' style='color:#333;width:96%!important;'><span class='idTitleHtmlWysiEdit'></span></h4>

				<div id='alerts'></div>
			<div class='btn-toolbar' data-role='editor-toolbar' data-target='#editorWy'>
				<div class='btn-group'>
					<a class='btn dropdown-toggle' data-toggle='dropdown' title='Font'><i class='icon-font'></i><b class='caret'></b></a>
					<ul class='dropdown-menu'>
					</ul>
				</div>
				<div class='btn-group'>
					<a class='btn dropdown-toggle' data-toggle='dropdown' title='Font Size'><i class='icon-text-height'></i>&nbsp;<b class='caret'></b></a>
						<ul class='dropdown-menu'>
						<li><a data-edit='fontSize 5'><font size='5'>Huge</font></a></li>
						<li><a data-edit='fontSize 3'><font size='3'>Normal</font></a></li>
						<li><a data-edit='fontSize 1'><font size='1'>Small</font></a></li>
						</ul>
				</div>
				<div class='btn-group'>
					<a class='btn' data-edit='bold' title='Bold (Ctrl/Cmd+B)'><i class='icon-bold'></i></a>
					<a class='btn' data-edit='italic' title='Italic (Ctrl/Cmd+I)'><i class='icon-italic'></i></a>
					<a class='btn' data-edit='strikethrough' title='Strikethrough'><i class='icon-strikethrough'></i></a>
					<a class='btn' data-edit='underline' title='Underline (Ctrl/Cmd+U)'><i class='icon-underline'></i></a>
				</div>
				<div class='btn-group'>
					<a class='btn' data-edit='insertunorderedlist' title='Bullet list'><i class='icon-list-ul'></i></a>
					<a class='btn' data-edit='insertorderedlist' title='Number list'><i class='icon-list-ol'></i></a>
					<a class='btn' data-edit='outdent' title='Reduce indent (Shift+Tab)'><i class='icon-indent-left'></i></a>
					<a class='btn' data-edit='indent' title='Indent (Tab)'><i class='icon-indent-right'></i></a>
				</div>
				<div class='btn-group'>
					<a class='btn' data-edit='justifyleft' title='Align Left (Ctrl/Cmd+L)'><i class='icon-align-left'></i></a>
					<a class='btn' data-edit='justifycenter' title='Center (Ctrl/Cmd+E)'><i class='icon-align-center'></i></a>
					<a class='btn' data-edit='justifyright' title='Align Right (Ctrl/Cmd+R)'><i class='icon-align-right'></i></a>
					<a class='btn' data-edit='justifyfull' title='Justify (Ctrl/Cmd+J)'><i class='icon-align-justify'></i></a>
				</div>
				<div class='btn-group'>
				<a class='btn dropdown-toggle' data-toggle='dropdown' title='Hyperlink'><i class='icon-link'></i></a>
					<div class='dropdown-menu input-append'>
						<input class='span2' placeholder='URL' type='text' data-edit='createLink'/>
						<button class='btn' type='button'>Add</button>
					</div>
					<a class='btn' data-edit='unlink' title='Remove Hyperlink'><i class='icon-cut'></i></a>
				</div>

				<div class='btn-group'>
					<a class='btn' data-edit='undo' title='Undo (Ctrl/Cmd+Z)'><i class='icon-undo'></i></a>
					<a class='btn' data-edit='redo' title='Redo (Ctrl/Cmd+Y)'><i class='icon-repeat'></i></a>
				</div>
				<input type='text' data-edit='inserttext' id='voiceBtn' x-webkit-speech=''>
			</div>



				<div class='fltBtnClose'>
					<button class='btn bgm-gray btn-icon waves-effect waves-circle' data-dismiss='modal'><i class='zmdi zmdi-close'></i></button>
				</div>
			</div>

			<div id='editorWy'>
				$tmpCGR
			</div>

			<div class='modal-footer'>
					<button type='button' class='btn btn-default $tmpUPBFC $tmpUPBBC cleanWysiwigButton'><i class='zmdi zmdi-delete'></i> $tmpCBC</button>
					<button type='button' class='btn btn-default $tmpUPBFC $tmpUPBBC updateWysiwigButton'>$tmpCTU</button>
					<button type='button' class='btn btn-default $tmpUPBFC $tmpUPBBC ' data-dismiss='modal'>$tmpCBN</button>
			</div>
		</div>
	</div>
</div>
";

	if($moduleSetting->arrFieldsFormModal[$key]['actionsForm']->wysiwigEditor['toolBar'])
		$htmlSpecificLibraries.='<script src="'.$web_url.$adminFolder.'/_plugins/whysiwyg_tiny_editor/default_toolbar.js"></script>';

?>
<link href="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/bootstrap-wysiwyg.css" rel="stylesheet" />
<link href="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/google-code-prettify/prettify.css" rel="stylesheet" />
<link href="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/bootstrap-wysiwyg-awesome.css" rel="stylesheet" />
<script src="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/jquery.hotkeys.js"></script>
<script src="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/google-code-prettify/prettify.js"></script>
<script src="<?=$web_url.$adminFolder?>/_plugins/whysiwyg_tiny_editor/bootstrap-wysiwyg.min.js"></script>

<script type="application/javascript">

$(document).ready(function(){
	var baseFieldNameToUpddate="";

	$(".modalHtmlWysiEditBox").click(function(){
		baseFieldNameToUpddate=$(this).attr("data-field-name");
		$(".idTitleHtmlWysiEdit").html("editando <b>"+$(this).parent().children(0).html()+"</b>");//$(this).attr("data-field-name")
		$("#editorWy").html($("textarea[name='db[<?=$moduleSetting->mainTable?>]["+$(this).attr("data-field-name")+"]']").val());
  });

	$(".updateWysiwigButton").click(function(){
		$("textarea[name='db[<?=$moduleSetting->mainTable?>]["+baseFieldNameToUpddate+"]']").val($("#editorWy").html());
		$('#htmlWysiEditBox').modal('toggle');
		baseFieldNameToUpddate=null;
	});

	$(".cleanWysiwigButton").click(function(){$("#editorWy").html("");});

	$('#editorWy').wysiwyg();

});
</script>
