<?php
//	local language
//	ITALIAN

$langTxt['localAjax']['errorBbdd']['lostConnectionTxt']='Sembra che il tempo della tua sessione sia scaduto!';
$langTxt['localAjax']['errorBbdd']['lostConnectionTxtBottom']='È necessario ri-convalidare l`accesso per continuare ...';

$langTxt['localAjax']['removeApplyListFieldFilter']['titleErrorLbl']='Origine dell` errore';
$langTxt['localAjax']['removeApplyListFieldFilter']['explainErrorLbl']='<b>Impossibile</b> connettersi al <b>database</b>';
$langTxt['localAjax']['createListFieldFilter']['txtExplain']='<b>mancano pararametri</b><br />sembra che non siano stati definiti tutti i parametri<br />richiesti per creare el filtro</b>.<br /><br />&Egrave; necessario <b>controllare il file di configurazione</b>.';

$langTxt['localAjax']['notify']['successFilterCreationTxt']='Creato il filtro<br />per la colonna';

$langTxt['localAjax']['dateLabel']['Search']='Cercare';
$langTxt['localAjax']['dateLabel']['from']='Da ';
$langTxt['localAjax']['dateLabel']['to']='fino a';
$langTxt['localAjax']['dateLabel']['placeholderFrom']='Data `da` o solo un giorno...';
$langTxt['localAjax']['dateLabel']['placeholderto']='Qui fino alla data di ...';

$langTxt['localAjax']['btn']['RemoveSearchFilterTxt']='Rimuovere questo filtro di ricerca';
$langTxt['localAjax']['btn']['ApplySearchFilterTxt']='Applicare la ricerca';
$langTxt['localAjax']['btn']['RemoveThisDate']='Cancella questa data';

