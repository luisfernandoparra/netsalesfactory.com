<?php
//	local language
//	SPANISH

$langTxt['localAjax']['errorBbdd']['lostConnectionTxt']='Parece que el tiempo de su sesi&oacute;n ha expirado!';
$langTxt['localAjax']['errorBbdd']['lostConnectionTxtBottom']='Debes volver a validar tu acceso para proseguir...';

$langTxt['localAjax']['removeApplyListFieldFilter']['ConnectionErrorLbl']='Acci&oacute;n del error';
$langTxt['localAjax']['removeApplyListFieldFilter']['explainErrorLbl']='<b>No</b> se ha podido establecer la <b>conexi&oacute;n a la base de datos</b>';
$langTxt['localAjax']['createListFieldFilter']['txtExplain']='<b>faltan par&aacute;rametros</b><br />parece que no est&aacute;n definidos todos los par&aacute;metros<br />necesarios para crear el filtro requerido</b>.<br /><br />Es necesario revisar el archivo de configuraci&oacute;n.';

$langTxt['localAjax']['notify']['successFilterCreationTxt']='Creado el filtro<br />para la columna';

$langTxt['localAjax']['dateLabel']['Search']='Buscar';
$langTxt['localAjax']['dateLabel']['from']='Desde';
$langTxt['localAjax']['dateLabel']['to']='Hasta';
$langTxt['localAjax']['dateLabel']['placeholderFrom']='Fecha `desde` o solo un d&iacute;a...';
$langTxt['localAjax']['dateLabel']['placeholderto']='Aquí la fecha `hasta`...';

$langTxt['localAjax']['btn']['RemoveSearchFilterTxt']='Quitar este filtro de la b&uacute;squeda';
$langTxt['localAjax']['btn']['ApplySearchFilterTxt']='Aplicar la b&uacute;squeda';
$langTxt['localAjax']['btn']['RemoveThisDate']='Borra esta fecha';
