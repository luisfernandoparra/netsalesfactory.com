<?php
//	local language
//	ITALIAN

$langTxt['localAjax']['common']['referenceMissingDetailTxt']='operazione annullata';
$langTxt['localAjax']['common']['folderErrorTxt']='Errore nella directory';
$langTxt['localAjax']['common']['abortedOperationIxt']='Operazione interrotta';
$langTxt['localAjax']['common']['unaivalableOrNotExistTx']='non esiste oppure è iaccessibile';
$langTxt['localAjax']['common']['incompleteOperationIxt']='Non è stato possibile completare l`operazione';
$langTxt['localAjax']['common']['fileErrorIxt']='Errore nel file';
$langTxt['localAjax']['common']['fileCopyErrorIxt']='Errore nella copia dei file';
$langTxt['localAjax']['common']['filePhysicalCopyErrorIxt']='È accaduto nella copia fisica del file';
$langTxt['localAjax']['common']['reviewChangesMadeTxt']='Si prega di rivedere le modifiche';
$langTxt['localAjax']['common']['detailErrorTxt']='dettaglio dell`errore';
$langTxt['localAjax']['common']['hasBeenImpossibleTxt']='È stato impossibile';
$langTxt['localAjax']['common']['unfinishedOperationTxtLong']='Operazione NON completata con successo';
$langTxt['localAjax']['common']['unfinishedOperationTxt']='operazione senza completare';
$langTxt['localAjax']['common']['alertFinishOperationTxt']='finire l`operazione';
$langTxt['localAjax']['common']['seemsMissingTxt']='Sembra mancare';
$langTxt['localAjax']['common']['moduleParameterTxt']='il parametro';
$langTxt['localAjax']['common']['relatedQueryErrorIxt']='si è verificato un errore relativo alla tabella correlata';

$langTxt['localAjax']['deleteRecord']['referenceMissingTxt']='Riferimento mancante!';
$langTxt['localAjax']['deleteRecord']['sqlErrorDeleteFailTxt']='Si è verificato un errore nell`eliminazione del record selezionato!';
$langTxt['localAjax']['deleteRecord']['sqlErrorDeleteFailBottomTxt']='operazione annullata';
$langTxt['localAjax']['deleteRecord']['resultNoRecordsTxt']='Impossibile eliminare questo record'."\n".'(controlla le attuali restrizioni)';
$langTxt['localAjax']['deleteRecord']['deleteSuccessTxt']='Record cancellato';
$langTxt['localAjax']['insertError']['insertBbddAbortTxt']='Inserimento abortito';
$langTxt['localAjax']['insertError']['InsertIncompleteTxt']='completar la Inserci&oacute;n';

$langTxt['localAjax']['folderError']['folderNoAvailableTxt']='Il direttorio non è accessibile';
$langTxt['localAjax']['getConfigData']['noApplyHighlightFuncTxt']='Impossibile applicare la funzione per <b>evidenziare della sintassi</b> del codice sorgente che si doveva mostrare per';

$langTxt['localAjax']['getHelpModule']['specificHelpModuleTitle']='Sostegno specifico per il modulo corrente';
$langTxt['localAjax']['getHelpModule']['BackOfficeNotesTitle']='Note sul Back-Office';
$langTxt['localAjax']['getHelpModule']['configFileTitle']='File di configurazione';
$langTxt['localAjax']['getHelpModule']['boTxt']='B.O.';
$langTxt['localAjax']['getHelpModule']['configModuleTxt']='Config-module';
$langTxt['localAjax']['getHelpModule']['moduleGenericErrorTxt']='Non è stato possibile reperire <i>l`aiuto</i> per';

$langTxt['localAjax']['updateConfigModule']['fileCopyBackUpErrorTxt']='Fallimento nella realizzazione del back-up';
$langTxt['localAjax']['updateConfigModule']['updateConfigFileErrorTxt']='Si è verificato un errore nell`aggiornamento nel file di configurazione';
$langTxt['localAjax']['updateConfigModule']['updateSuccessTxt']='Impostazioni aggiornate correttamente';
