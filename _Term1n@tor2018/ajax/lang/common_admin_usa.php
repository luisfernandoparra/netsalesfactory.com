<?php
//	local language
//	SPANISH

$langTxt['localAjax']['common']['referenceMissingDetailTxt']='operación cancelada';
$langTxt['localAjax']['common']['folderErrorTxt']='Error de carpeta';
$langTxt['localAjax']['common']['abortedOperationIxt']='Operación abortada';
$langTxt['localAjax']['common']['unaivalableOrNotExistTx']='no existe o no es accesible';
$langTxt['localAjax']['common']['incompleteOperationIxt']='No ha sido posible completar la operación';
$langTxt['localAjax']['common']['fileErrorIxt']='Error de archivo';
$langTxt['localAjax']['common']['fileCopyErrorIxt']='Error en la copia del archivo';
$langTxt['localAjax']['common']['filePhysicalCopyErrorIxt']='Ha ocurrido en la copia física del archivo';
$langTxt['localAjax']['common']['reviewChangesMadeTxt']='Por favor, revisa los cambios efectuados';
$langTxt['localAjax']['common']['detailErrorTxt']='detalle del error';
$langTxt['localAjax']['common']['hasBeenImpossibleTxt']='Ha sido imposible';
$langTxt['localAjax']['common']['unfinishedOperationTxtLong']='Operación NO finalizada correctamente';
$langTxt['localAjax']['common']['unfinishedOperationTxt']='operación sin completar';
$langTxt['localAjax']['common']['alertFinishOperationTxt']='completar la operaci&oacute;n';
$langTxt['localAjax']['common']['seemsMissingTxt']='Parece que falta';
$langTxt['localAjax']['common']['moduleParameterTxt']='el par&aacute;metro';
$langTxt['localAjax']['common']['relatedQueryErrorIxt']='an error has been verified relative to the related table';

$langTxt['localAjax']['deleteRecord']['referenceMissingTxt']='Falta la referencia!';
$langTxt['localAjax']['deleteRecord']['sqlErrorDeleteFailTxt']='Ha ocurrido un error al intentar eliminar el registro seleccionado!';
$langTxt['localAjax']['deleteRecord']['sqlErrorDeleteFailBottomTxt']='operación cancelada';
$langTxt['localAjax']['deleteRecord']['resultNoRecordsTxt']='NO se ha podido elimar este registro'."\n".'(verifica las restricciones para poder borrarlo)';
$langTxt['localAjax']['deleteRecord']['deleteSuccessTxt']='Registro eliminado';
$langTxt['localAjax']['insertError']['insertBbddAbortTxt']='Inserci&oacute;n&nbsp;abortada';
$langTxt['localAjax']['insertError']['InsertIncompleteTxt']='completar la Inserci&oacute;n';

$langTxt['localAjax']['folderError']['folderNoAvailableTxt']='la carpeta no es accesible';
$langTxt['localAjax']['getConfigData']['noApplyHighlightFuncTxt']='No se ha podido aplicar la función para <b>resaltar la sintaxis</b> del codigo fuente que se debía mostrar para';

$langTxt['localAjax']['getHelpModule']['specificHelpModuleTitle']='Ayudas específicas del módulo actual';
$langTxt['localAjax']['getHelpModule']['BackOfficeNotesTitle']='Anotaciones sobre el Back-Office';
$langTxt['localAjax']['getHelpModule']['configFileTitle']='Archivo de configuración';
$langTxt['localAjax']['getHelpModule']['boTxt']='B.O.';
$langTxt['localAjax']['getHelpModule']['configModuleTxt']='Config-module';
$langTxt['localAjax']['getHelpModule']['moduleGenericErrorTxt']='No se ha podido cargar <i>la ayuda</i> para';

$langTxt['localAjax']['updateConfigModule']['fileCopyBackUpErrorTxt']='Fallo en la copia de seguridad';
$langTxt['localAjax']['updateConfigModule']['updateConfigFileErrorTxt']='Ha ocurrido un error en la actualización de la configuración';
$langTxt['localAjax']['updateConfigModule']['updateSuccessTxt']='Configuración actualizada correctamente';
