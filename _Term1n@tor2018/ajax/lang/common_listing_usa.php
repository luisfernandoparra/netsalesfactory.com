<?php
//	local language
//	USA

$langTxt['localAjax']['errorBbdd']['lostConnectionTxt']='It seems that the time of your session has expired!';
$langTxt['localAjax']['errorBbdd']['lostConnectionTxtBottom']='It is necessary to convalidate the access to continue';

$langTxt['localAjax']['removeApplyListFieldFilter']['titleErrorLbl']='???';
$langTxt['localAjax']['removeApplyListFieldFilter']['explainErrorLbl']='???';
$langTxt['localAjax']['createListFieldFilter']['txtExplain']='???';

$langTxt['localAjax']['notify']['successFilterCreationTxt']='???';

$langTxt['localAjax']['dateLabel']['Search']='Search';
$langTxt['localAjax']['dateLabel']['from']='From';
$langTxt['localAjax']['dateLabel']['to']='To';
$langTxt['localAjax']['dateLabel']['placeholderFrom']='Date `from` or just one day...';
$langTxt['localAjax']['dateLabel']['placeholderto']='Here the date until...';

$langTxt['localAjax']['btn']['RemoveSearchFilterTxt']='Remove this filter from search';
$langTxt['localAjax']['btn']['ApplySearchFilterTxt']='Apply the search';
$langTxt['localAjax']['btn']['RemoveThisDate']='Delete this date';
