<?php
/**
 * SPECIFIC SCRIPT ATCIONS
 *
 * AJAX RESPONSE ACTIONS ==> PARAMETRO ´action` (COMUN PARA TODAS LAS ACCIONES)
 * XXX: ??
 *
 */
header('Content-Type: text/html; charset=utf-8');
@session_start();
$commonLogQuerySeparator='<hr style=background-color:#58AFF6;color:#58AFF6; />';

function describeQueries($dataObj='', $resSqlParam=null)
{
	$response=null;

	if($_SESSION['userAdmin']['pref_']['b_display_sql'])
	{
		$response=[];

		if(is_array($dataObj->query))
		{
			foreach($dataObj->query as $key=>$resSql)
			{
				$response['sql'][$_SESSION['contLogResponse']]=$resSql.'<hr style=background-color:#58AFF6;color:#58AFF6; />';
				$response['res'][$_SESSION['contLogResponse']]=$dataObj->resQuery[$key];
				$_SESSION['contLogResponse']++;
			}
		}
		else
		{
$response['TEST']='$contBlock='.$_SESSION['contLogResponse'];
			$response['sql'][$_SESSION['contLogResponse']]=$dataObj->query;
			$response['res'][$_SESSION['contLogResponse']]=$resSqlParam;
			$_SESSION['contLogResponse']++;
		}
	}
	return $response;
}


if(!isset($_SERVER['HTTP_REFERER']) && basename($_SERVER['SCRIPT_NAME']) != 'index.php'){include('strange_errors.php');die();}	//	prevent direct file access

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_REQUEST;

@include('../../conf/config_web.php');
@include('../../conf/config.misc.php');

$provenience=$_SERVER['HTTP_REFERER'];
$modNameFrom=strpos($provenience,'_modules/')+9;
$modName=substr($provenience, $modNameFrom);
$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));
$localModuleFolder='_modules/'.$modName.'/';
$localModuleFolder=isset($arraExtVars['moduleFolder']) ? $arraExtVars['moduleFolder'] : (isset($arraExtVars['from']) ? $arraExtVars['from'] : (isset($arraExtVars['module']) ? $arraExtVars['module'] : $localModuleFolder));

$currLang=$_SESSION['userAdmin']['pref_']['ref_lang'] ? strtolower($_SESSION['userAdmin']['pref_']['ref_lang']) : 'usa';
$response['query']=array();
$_SESSION['contLogResponse']=0;

// START LOCAL MODULE LANGUAGE TRANSLACTIONS
if(isset($currLang) && $localModuleFolder)
{
	$targetModuleFGolder=isset($arraExtVars['from']) ? $arraExtVars['from'] : @$arraExtVars['moduleFolder'];
	$dirLang=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$targetModuleFGolder.'lang/';
	$filLang=$dirLang.'module_'.$currLang.'.php';
	$haveTranslations=false;

	$tmpLocalTranslationScript='/lang/'.pathinfo(__FILE__, PATHINFO_FILENAME).'_'.$currLang.'.php';
	$localTranslationsScript=pathinfo(__FILE__);
	$localTranslationsScript=$localTranslationsScript['dirname'].$tmpLocalTranslationScript;
//	$localTranslationsScript=$dirModulesAdmin.''.pathinfo(__FILE__, PATHINFO_FILENAME);

	if(file_exists($localTranslationsScript))	// INCLUDE DEL ARCHIVO LOCAL DE LAS TRADUCCIONES DEL SCRIPT ACTUAL
	{
		include($localTranslationsScript);
		$haveTranslations=true;
	}

	if(file_exists($filLang))	// INCLUDE DE LAS TRADUCCIONES GLOBALES
	{
		include($filLang);
		$haveTranslations=true;
	}

	if($haveTranslations)
	{
		$wordSep='_'; $tmpWord=null; $tmpValue=null;

		foreach($langTxt as $kL=>$vL)
		{
			$tmpWord=$kL;

			foreach($vL as $nn=>$tmpValue1)
			{
				$tmpWord_2=null;
				if(is_array($tmpValue1))
				{
					$tmpWord_3=null;

					foreach($tmpValue1 as $nv=>$tmpValue2)
					{
						$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
            if(defined('__'.strtoupper($tmpWord_3)) !== true)
              define('__'.strtoupper($tmpWord_3), $tmpValue2);
					}
				}
				else
				{
					$tmpWord_2=$tmpWord.$wordSep.$nn;

          if(defined('__'.strtoupper($tmpWord_2)) !== true)
            define('__'.strtoupper($tmpWord_2), $tmpValue1);
				}
			}
		}

	}
}
// END LOCAL MODULE LANGUAGE TRANSLACTIONS

if(!isset($_SESSION['userAdmin']))	// SE HA PERDIDO LA SESION
{
	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModAdmin.'mod_common_lists.class.php');
	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
	$resConnexion=$data->connectDB();

	$data->logBackOffice(0,null,$arraExtVars['getTableListData'].' LOST-SESSION');
	$response['status']=-1;
	$response['rows']=-1;
	$response['rowCount']=0;
	$response['current']=0;
	$response['total']=0;
	$response['msgPainTxt']="\n".@$arraExtVars['idRef'];
	$response['errorText']="\n".__AJAX_ERRORBBDD_LOSTCONNECTIONTXT.'<br /><br />';
	$response['errorText'].="\n".__AJAX_ERRORBBDD_LOSTCONNECTIONTXTBOTTOM.'...';
	$res=json_encode($response);
	die($res);
}


/**
 * GET DATA LIST FIELDS & STATUS ROW
 *
 * OBTENCION DE DATOS PARA EL LISTADO TABULAR
 *
 * modificado 2017.05.22 (M.F.)
 *
 */
if($arraExtVars['action'] == 'findHelp')
{
	$tmpWordToFind=@trim($arraExtVars['wordFind']);
	include($dirModAdmin.'mod_bbdd.class.php');
	$dataModuleBBDD=new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
	$resConnexion=$dataModuleBBDD->connectDB();
	$dataModuleBBDD->logBackOffice(0,null,$tmpWordToFind);
	$response['txtError']='';
	$minCharToSearch=3;

	if(strlen($tmpWordToFind) < $minCharToSearch)
	{
		$response['word_find']=$tmpWordToFind;
		$response['txtError']='<span class=\'c-red b-700 f-20 fa fa-exclamation-circle\'>&nbsp;&nbsp;Debe escribir mínimo '.$minCharToSearch.' letras</span><br /><br /><center>Ahora ha escrito (sin espacios)<br><span class=\'c-black b-700 f-20 \'>'.$tmpWordToFind.'</span></center>';
		$response['success']=true;
		$res=json_encode($response);
		die($res);
	}

	$arrCurrUrl=parse_url($arraExtVars['currUrl']);
	$resultsFound=array(); $arrParams=array(); $totalResults=0;

	if($arrCurrUrl['query'])	// SI HAY PARAMETROS EN LA URL SE GENERA EL ARRAY CON LOS MISMOS
	{
		$currModule=urldecode($arrCurrUrl['query']);
		foreach(explode('&',$arraExtVars['currUrl']) as $data)
		{
			$param=explode('=', $data);
			if($param)
			{
				$paramName=urldecode($param[0]);
				$posFirstParam=strpos($paramName,'?');
				$paramName=$posFirstParam ? substr($paramName,$posFirstParam+1) : $paramName;
				$arrParams[$paramName]=urldecode($param[1]);
			}
		}
	}

	if($arrParams['moduleAdmin'])	//COINCIDENCIAS EN LA AYUDA DE USUARIO
	{
		$response['moduleName']=$arrParams['modName'];
		$scriptFileName='help_module.php';
		$moduleHelpFile=$dirModulesAdmin.'/'.$arrParams['moduleAdmin'].$scriptFileName;
		$helpFileContent=strip_tags(file_get_contents($moduleHelpFile));

//		$tmpPattern="/\w+".$tmpWordToFind."\w+/i";
		$tmpPattern="/\b".$tmpWordToFind."\b/i";
		$tmpResults=substr_count($helpFileContent, $tmpWordToFind);

		if($tmpResults)
		{
			$resultsFound[$scriptFileName]['numberResults']=$tmpResults;
			$resultsFound[$scriptFileName]['label']='Ayudas de usuario (<i>'.$arrParams['modName'].'</i>)';
			$resultsFound[$scriptFileName]['backgroundColor']='bgm-teal';
			$resultsFound[$scriptFileName]['dataAction']='basicHelpOpen';
			$resultsFound[$scriptFileName]['dataTab']='tabUsu';
			$totalResults+=$tmpResults;
		}
	}

	if($_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER)
	{
		$scriptFileName='_modules/help_modules_contents.php';
		$moduleHelpFile=$dirModulesAdmin.''.$scriptFileName;
		$helpFileContent=strip_tags(file_get_contents($moduleHelpFile));
		//$tmpResults=preg_match_all($tmpPattern, $helpFileContent);
		$tmpResults=substr_count($helpFileContent, $tmpWordToFind);

		if($tmpResults)
		{
			$resultsFound[$scriptFileName]['numberResults']=$tmpResults;
			$resultsFound[$scriptFileName]['label']='Contenidos generales';
			$resultsFound[$scriptFileName]['backgroundColor']='bgm-cyan';
			$resultsFound[$scriptFileName]['dataAction']='basicHelpOpen';
			$resultsFound[$scriptFileName]['dataTab']='tabSys';
			$totalResults+=$tmpResults;
		}
	}

	if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD)	// SOLO CON MAXIMO NIVEL DE ACCESO
	{
		$scriptFileName='help_system.php';
		$moduleHelpFile=$dirModulesAdmin.'/'.$arrParams['moduleAdmin'].$scriptFileName;

		if(file_exists($moduleHelpFile))
		{
			$helpFileContent=strip_tags(file_get_contents($moduleHelpFile));
			$tmpPattern="/\b".$tmpWordToFind."\b/i";
			$tmpResults=substr_count($helpFileContent, $tmpWordToFind);

			if($tmpResults)
			{
				$resultsFound[$scriptFileName]['numberResults']=$tmpResults;
				$resultsFound[$scriptFileName]['label']='Ayudas de <i>Sistema</i>';
				$resultsFound[$scriptFileName]['backgroundColor']='bgm-indigo';
				$resultsFound[$scriptFileName]['dataAction']='basicHelpOpen';
				$resultsFound[$scriptFileName]['dataTab']='tabSys';
				$totalResults+=$tmpResults;
			}
		}

		$scriptFileName='_modules/help_common_system.html';
		$moduleHelpFile=$dirModulesAdmin.''.$scriptFileName;
		$helpFileContent=strip_tags(file_get_contents($moduleHelpFile));
		//$tmpResults=preg_match_all($tmpPattern, $helpFileContent);
		$tmpResults=substr_count($helpFileContent, $tmpWordToFind);

		if($tmpResults)
		{
			$resultsFound[$scriptFileName]['numberResults']=$tmpResults;
			$resultsFound[$scriptFileName]['label']='anotaciones del back-office';
			$resultsFound[$scriptFileName]['backgroundColor']='bgm-deeporange';
			$resultsFound[$scriptFileName]['dataAction']='basicHelpOpen';
			$resultsFound[$scriptFileName]['dataTab']='tabBO';
			$totalResults+=$tmpResults;
		}

		$scriptFileName='_modules/help_config_modules.html';
		$moduleHelpFile=$dirModulesAdmin.''.$scriptFileName;
		$helpFileContent=strip_tags(file_get_contents($moduleHelpFile));
		//$tmpResults=preg_match_all($tmpPattern, $helpFileContent);
		$tmpResults=substr_count($helpFileContent, $tmpWordToFind);

		if($tmpResults)
		{
			$resultsFound[$scriptFileName]['numberResults']=$tmpResults;
			$resultsFound[$scriptFileName]['label']='Ayudas de sistema';
			$resultsFound[$scriptFileName]['backgroundColor']='bgm-blue';
			$resultsFound[$scriptFileName]['dataAction']='basicHelpOpen';
			$resultsFound[$scriptFileName]['dataTab']='tabMC';
			$totalResults+=$tmpResults;
		}
	}
//	$resultsFound[$scriptFileName]['label']='Ayudas de sistema';
//	$resultsFound[$scriptFileName]['backgroundColor']='bgm-orange';
//	$totalResults+=$resultsFound[$scriptFileName]['numberResults'];
//	include($dirModAdmin.'mod_common_lists.class.php');
//	if(file_exists($dirModulesAdmin.'_modules/'.$modName.'/config_module.php'))
//		include($dirModulesAdmin.'_modules/'.$modName.'/config_module.php');

/*
echo"\n... ".$moduleHelpFile;
print_r($arraExtVars);echo"\nresultados en por archivos: ";print_r($resultsFound);
*/

//die();

//	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting);
//	$resConnexion=$data->connectDB();

	$response['word_find']=$tmpWordToFind;
	$response['resultsFound']=$resultsFound;
	$response['totalResults']=$totalResults;
	$response['totalResultsTxt']='&nbsp;&nbsp;'.__COMMON_JSHELP_AJAXMATCHESPRETXT.' <b>'.$totalResults.'</b> '.__COMMON_JSHELP_AJAXMATCHESPOSTTXT.':<br>';
	$response['success']=$totalResults ? true : false;


  $res=json_encode($response);
  die($res);
}
