<?php
/**
 * SPECIFIC SCRIPT ATCIONS
 *
 * AJAX RESPONSE ACTIONS ==> PARAMETRO action` (COMUN PARA TODAS LAS ACCIONES)
 * createListFieldFilter: CREATE FILTER FOR COLUMN IN TABULAR LIST
 * removeListFieldFilter: DELETE FILTER FOR COLUMN IN TABULAR LIST AND RESET BBDD SPECIFIC FILTER
 *
 */
header('Content-Type: text/html; charset=utf-8');
@session_start();

if(!isset($_SERVER['HTTP_REFERER']) && basename($_SERVER['SCRIPT_NAME']) != 'index.php'){include('strange_errors.php');die();}

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_REQUEST;

@include('../../conf/config_web.php');
@include('../../conf/config.misc.php');
$currLang='usa';
$currLang=isset($_SESSION['userAdmin']['pref_']['ref_lang']) ? strtolower($_SESSION['userAdmin']['pref_']['ref_lang']) : 'usa';
$response['query']=array(0);

// START LOCAL MODULE LANGUAGE TRANSLACTIONS
if((isset($arraExtVars['from']) ||  isset($arraExtVars['moduleFolder'])))
{
	$targetModuleFGolder=isset($arraExtVars['from']) ? $arraExtVars['from'] : @$arraExtVars['moduleFolder'];
	$dirLang=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$targetModuleFGolder.'lang/';
	$filLang=$dirLang.'module_'.$currLang.'.php';
	$haveTranslations=false;
	$tmpLocalTranslationScript='/lang/'.pathinfo(__FILE__, PATHINFO_FILENAME).'_'.$currLang.'.php';
	$localTranslationsScript=pathinfo(__FILE__);
	$localTranslationsScript=$localTranslationsScript['dirname'].$tmpLocalTranslationScript;

	if(file_exists($localTranslationsScript))	// INCLUDE DEL ARCHIVO LOCAL DE LAS TRADUCCIONES DEL SCRIPT ACTUAL
	{
		include($localTranslationsScript);
		$haveTranslations=true;
	}

	if(file_exists($filLang))	// INCLUDE DE LAS TRADUCCIONES GLOBALES
	{
		include($filLang);
		$haveTranslations=true;
	}

	if($haveTranslations)
	{
		$wordSep='_'; $tmpWord=null; $tmpValue=null;

		foreach($langTxt as $kL=>$vL)
		{
			$tmpWord=$kL;

			foreach($vL as $nn=>$tmpValue1)
			{
				$tmpWord_2=null;
				if(is_array($tmpValue1))
				{
					$tmpWord_3=null;

					foreach($tmpValue1 as $nv=>$tmpValue2)
					{
						$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
						define('__'.strtoupper($tmpWord_3), $tmpValue2);
					}
				}
				else
				{
					$tmpWord_2=$tmpWord.$wordSep.$nn;
					define('__'.strtoupper($tmpWord_2), $tmpValue1);
				}
			}
		}

	}
}
//echo "\n".$localTranslationsScript;print_r($langTxt);
// END LOCAL MODULE LANGUAGE TRANSLACTIONS
//echo $currLang.' ) __AJAX_ERRORBBDD_LOSTCONNECTIONTXT='.defined(__AJAX_ERRORBBDD_LOSTCONNECTIONTXT);die("\nFIN");
@include($dirModAdmin.'mod_bbdd.class.php');

//echo '__AJAX='.(__LOCALAJAX_ERRORBBDD_LOSTCONNECTIONTXTBOTTOM);die("\nFIN");
if(!isset($_SESSION['userAdmin']))	// SE HA PERDIDO LA SESION
{
	include($dirModAdmin.'mod_common_lists.class.php');
	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting, $port);
	$resConnexion=$data->connectDB();
	$data->logBackOffice(0,null,$arraExtVars['getTableListData'].' LOST-SESSION');
	$response['success']=false;
	$response['errorText']="\n".__LOCALAJAX_ERRORBBDD_LOSTCONNECTIONTXT.'<br /><br />';
	$response['errorText'].="\n".__LOCALAJAX_ERRORBBDD_LOSTCONNECTIONTXTBOTTOM;
	$res=json_encode($response);
	die($res);
}

/**
 * ONLY ACTION LOGS
 */
if($arraExtVars['action'] == 'removeListFieldFilter' || $arraExtVars['action'] == 'applyListFieldFilter')
{
	include($dirModAdmin.'mod_common_lists.class.php');
  include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleFolder'].'config_module.php');
	$localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleFolder'].'_mod_bbdd/';
	include($localModelsFolder.'module_model.class.php');
	include($dirModAdmin.'mod_common_edit.class.php');
	$data=new CommonEditingModel($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting, $port);
	$resConnexion=$data->connectDB();

	if(!isset($resConnexion))	// FALLO DE CONEXION
	{
		$data->logBackOffice(0,null,''.'CONNECTION-ERROR');
    $response['success']=false;
    $response['msgPainTxt']="\n".__LOCALAJAX_REMOVEAPPLYLISTFIELDFILTER_TITLEERRORLBL.': <b>'.$arraExtVars['action'].'</b>';
    $response['errorText']="\n<br />".__LOCALAJAX_REMOVEAPPLYLISTFIELDFILTER_EXPLAINERRORLBL.'.<br /><br />';
    $response['errorText'].="\n".__COMMON_GENERICTEXT_ERROCONFIGMODULEEND;
		$res=json_encode($response);
		die($res);
	}

	$elementDetail=isset($arraExtVars['realValueFind']) && $arraExtVars['realValueFind'] ? ',value='.$arraExtVars['realValueFind'] : null;
	$data->logBackOffice(0,null,' '.$arraExtVars['fieldName'].$elementDetail);
	$response['success']=true;
  $res=json_encode($response);
  die($res);
}


/**
 * CREATE FILTER FOR COLUMN IN TABULAR LIST
 *
 * OBTENCION DE DATOS PARA EL LISTADO TABULAR
 *
 */
if($arraExtVars['action'] == 'createListFieldFilter')
{
	if(isset($arraExtVars['settingsField']) && $arraExtVars['settingsField'])	// mode
		$filterQueryParams=json_decode($arraExtVars['settingsField']);
	else
	{
		$response['success']=true;
		$response['errorModule']=true;
		$response['textMsg']='<b>'.__COMMON_GENERICTEXT_WARNING.'</b>: '.__LOCALAJAX_CREATELISTFIELDFILTER_TXTEXPLAIN;
		$response['errorText'].="\n".__COMMON_GENERICTEXT_ERROCONFIGMODULEEND;
		$res=json_encode($response);
		die($res);
	}

  include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleFolder'].'config_module.php');

	if($filterQueryParams->mode == 'BBDD')	// SE DEBEN OBTENER DATOS DESDE LA BBDD
	{
		$tableSql=$moduleSetting->arrFieldsFormModal[$arraExtVars['fieldName']]['fieldTableName'];
		include($dirModAdmin.'mod_common_edit.class.php');
		$data=null;
		$localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleFolder'].'_mod_bbdd/';
		include($localModelsFolder.'module_model.class.php');
		$dataModuleBBDD=new CommonEditingModel($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting, $port);
		$resConnexion=$dataModuleBBDD->connectDB();

		if(!isset($resConnexion))	// FALLO DE CONEXION
		{
			$data->logBackOffice(0,null,''.'CONNECTION-ERROR');
			$response['success']=false;
			$response['msgPainTxt']="\n".@$arraExtVars['idRef'];
			$response['errorText']="\n".__COMMON_BBDD_TXTNOCONNECTIONBBDD.'.<br /><br />';
			$response['errorText'].="\n".__COMMON_GENERICTEXT_ERROCONFIGMODULEEND;
			$res=json_encode($response);
			die($res);
		}

		$data=new ModelLocalModule($dataModuleBBDD);
    $resTxt='';
    $specificSql=isset($moduleSetting->arrFieldsFormModal[$arraExtVars['fieldName']]['tabularFilter']['sqlWhere']) && $moduleSetting->arrFieldsFormModal[$arraExtVars['fieldName']]['tabularFilter']['sqlWhere'] ? $moduleSetting->arrFieldsFormModal[$arraExtVars['fieldName']]['tabularFilter']['sqlWhere'] : null;
		$isLocaleMethod=method_exists($data->getSpecificRecordData) ? true : false;

		if($isLocaleMethod)
		{
			$resData=$data->getSpecificRecordData(0, $tableSql, $moduleSetting->arrFieldsFormModal[$arraExtVars['fieldName']]['arrFieldsTagSelect'], $specificSql, $moduleSetting->arrFieldsFormModal[$arraExtVars['fieldName']]['orderSelectField']);

			if($_SESSION['userAdmin']['pref_']['b_display_sql'])
			{
				$response['query']['sql'][]=$data->query;
				$response['query']['res'][]=$resData;
			}
		}
		else
		{
			$resData=$dataModuleBBDD->getSpecificRecordData(0, $tableSql, $moduleSetting->arrFieldsFormModal[$arraExtVars['fieldName']]['arrFieldsTagSelect'], $specificSql, $moduleSetting->arrFieldsFormModal[$arraExtVars['fieldName']]['orderSelectField']);

			if($_SESSION['userAdmin']['pref_']['b_display_sql'])
			{
				$response['query']['sql'][]=$dataModuleBBDD->query;
				$response['query']['res'][]=$resData;
			}
		}
//		$data->commonDb->logBackOffice(null,null,' '.$arraExtVars['fieldName']);
		$dataModuleBBDD->logBackOffice(null,null,' '.$arraExtVars['fieldName']);
	}

	if($filterQueryParams->mode == 'STATIC')	//	SE GENERA EL ARRAY DE DATOS STANDARD X CREAR EL TAG NECESARIO
	{
		if(isset($filterQueryParams->dataFields) && isset($filterQueryParams->dataValues))
		{
			$cont=0;
			foreach($filterQueryParams->dataValues as $key=>$value)
			{
				$resData[$cont]['id_list']=$key;
				$resData[$cont]['text_list']=$value;
				$cont++;
			}
		}

		// FILTROS DE TIPO FECHA
		if(isset($filterQueryParams->tag->filterType) && $filterQueryParams->tag->filterType == 'date')
		{
			$resData[]['value']='';
			$resData[]['fieldName']=$arraExtVars['fieldName'];
		}

		include_once($dirModAdmin.'mod_common_edit.class.php');
		$dataModuleBBDD=new CommonEditingModel($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting, $port);
		$resConnexion=$dataModuleBBDD->connectDB();
		$dataModuleBBDD->logBackOffice(null,null,' (STATIC) '.$arraExtVars['fieldName']);
	}

//  if(isset($arraExtVars['settingsField']) && isset($_SESSION['userAdmin']['pref_']['b_debugSession']) && $_SESSION['userAdmin']['pref_']['b_debugSession'])
  if(isset($filterQueryParams))	// ============== HABILITAR ESTA INSTRUCCION CUANDO TODO OK =============
  {
//    $arrParamsField=json_decode($arraExtVars['settingsField']);
		if(isset($filterQueryParams->tag->filterType) && $resData)
		{
			include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_classes/lists_filter_extended.class.php');
			$objHtml=new filterExtendedSupport($arraExtVars['fieldName'], $moduleSetting->arrFieldsFormModal[$arraExtVars['fieldName']]);

//echo $filterQueryParams->tag->filterType;print_r($resData);die("\nNO!");
			switch($filterQueryParams->tag->filterType)
			{
				case 'select':
					$objHtml->getSelectFilter($resData);
					break;
				case 'radio':
					$objHtml->getRadioFilter($resData);
					break;
				case 'check':
//					$objHtml->getSelectFilter($resData);
//echo "\n";print_r($data);//die();
					break;
				case 'date':
					$objHtml->getDateFilter($resData);
					break;
			}
		}

//echo "\n--->";print_r($resData);//die();
		$response['textMsg']='<br>'.__LOCALAJAX_NOTIFY_SUCCESSFILTERCREATIONTXT.':<br><br><center><span class=\'p-5 bgm-teal f-16\'>'.$moduleSetting->arrFieldsFormModal[$arraExtVars['fieldName']]['label'].'</span></center>';
  }

	$response['htmlObjectStart']=isset($objHtml->htmlBoxStart) ? $objHtml->htmlBoxStart : null;
	$response['htmlObjectEnd']=isset($objHtml->htmlBoxEnd) ? $objHtml->htmlBoxEnd : null;
	$response['htmlObject']=isset($objHtml->html) ? $objHtml->html : null;
	$response['filterFieldNameResponse']=@$arraExtVars['filterFieldName'];
	$response['success']=true;
  $res=json_encode($response);
  die($res);
}


if($arraExtVars['action'] == 'createDateFieldFilter')
{
print_r($moduleSetting);
print_r($_SESSION);
print_r($_REQUEST);
echo "\n".') LIST REQUEST ---><hr><pre>';print_r($arraExtVars);//print_r($data);echo'</pre>';die(0);
}