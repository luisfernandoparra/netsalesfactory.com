<?php
/**
 * SPECIFIC SCRIPT ATCIONS
 *
 * AJAX RESPONSE ACTIONS ==> PARAMETRO ´action` (COMUN PARA TODAS LAS ACCIONES)
 * logout: LOGOUT ACTION
 * getTableListData: GET DATA LIST FOR CURRENT PAGE
 * getHelpModule: LOAD HELP CURRENT MODULE + COMMON HELP FILES
 * deleteRecord: DELETE SELECTED RECORD
 * insert: INSERT NEW RECORD INTO ACTIVE MODULE
 * update: UPDATE EDITED RECORD OF CURRENT MODULE
 * getConfigData: LOAD CURRENT CONGURATION MODULE
 * deleteBackUpConfigModule: DELETE SELECTED BACKUP FILE
 * restoreBackUpConfigModule: RESTORE SELECTED BACKUP FILE
 * newStatusRecord: ENABLE / DISABLE RECORD (id) FROM TABLE NAME (tblName)
 * readConfigModuleFile: READ CONTENTS OF SELECTED BACKUP FILE
 *
 */
header('Content-Type: text/html; charset=utf-8');
@session_start();

if(!isset($_SERVER['HTTP_REFERER']) && basename($_SERVER['SCRIPT_NAME']) != 'index.php'){include('strange_errors.php');die();}	//	prevent direct file access

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_REQUEST;

@include('../../conf/config_web.php');
@include('../../conf/config.misc.php');

$provenience=$_SERVER['HTTP_REFERER'];
$modNameFrom=strpos($provenience,'_modules/')+9;
$modName=substr($provenience, $modNameFrom);
$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));
$localModuleFolder='_modules/'.$modName.'/';
$localModuleFolder=isset($arraExtVars['moduleFolder']) ? $arraExtVars['moduleFolder'] : (isset($arraExtVars['from']) ? $arraExtVars['from'] : (isset($arraExtVars['module']) ? $arraExtVars['module'] : $localModuleFolder));

$currLang=$_SESSION['userAdmin']['pref_']['ref_lang'] ? strtolower($_SESSION['userAdmin']['pref_']['ref_lang']) : 'usa';

// START LOCAL MODULE LANGUAGE TRANSLACTIONS
if(isset($currLang) && $localModuleFolder)
{
	$targetModuleFGolder=isset($arraExtVars['from']) ? $arraExtVars['from'] : @$arraExtVars['moduleFolder'];
	$dirLang=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$targetModuleFGolder.'lang/';
	$filLang=$dirLang.'module_'.$currLang.'.php';
	$haveTranslations=false;

	$tmpLocalTranslationScript='/lang/'.pathinfo(__FILE__, PATHINFO_FILENAME).'_'.$currLang.'.php';
	$localTranslationsScript=pathinfo(__FILE__);
	$localTranslationsScript=$localTranslationsScript['dirname'].$tmpLocalTranslationScript;
//	$localTranslationsScript=$dirModulesAdmin.''.pathinfo(__FILE__, PATHINFO_FILENAME);

	if(file_exists($localTranslationsScript))	// INCLUDE DEL ARCHIVO LOCAL DE LAS TRADUCCIONES DEL SCRIPT ACTUAL
	{
		include($localTranslationsScript);
		$haveTranslations=true;
	}

	if(file_exists($filLang))	// INCLUDE DE LAS TRADUCCIONES GLOBALES
	{
		include($filLang);
		$haveTranslations=true;
	}

	if($haveTranslations)
	{
		$wordSep='_'; $tmpWord=null; $tmpValue=null;

		foreach($langTxt as $kL=>$vL)
		{
			$tmpWord=$kL;

			foreach($vL as $nn=>$tmpValue1)
			{
				$tmpWord_2=null;
				if(is_array($tmpValue1))
				{
					$tmpWord_3=null;

					foreach($tmpValue1 as $nv=>$tmpValue2)
					{
						$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
            if(defined('__'.strtoupper($tmpWord_3)) !== true)
              define('__'.strtoupper($tmpWord_3), $tmpValue2);
					}
				}
				else
				{
					$tmpWord_2=$tmpWord.$wordSep.$nn;

          if(defined('__'.strtoupper($tmpWord_2)) !== true)
            define('__'.strtoupper($tmpWord_2), $tmpValue1);
				}
			}
		}

	}
}
// END LOCAL MODULE LANGUAGE TRANSLACTIONS

if(!isset($_SESSION['userAdmin']))	// SE HA PERDIDO LA SESION
{
	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModAdmin.'mod_common_lists.class.php');
	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
	$resConnexion=$data->connectDB();

	$data->logBackOffice(0,null,$arraExtVars['getTableListData'].' LOST-SESSION');
	$response['status']=-1;
	$response['rows']=-1;
	$response['rowCount']=0;
	$response['current']=0;
	$response['total']=0;
	$response['msgPainTxt']="\n".@$arraExtVars['idRef'];
	$response['errorText']="\n".__AJAX_ERRORBBDD_LOSTCONNECTIONTXT.'<br /><br />';
	$response['errorText'].="\n".__AJAX_ERRORBBDD_LOSTCONNECTIONTXTBOTTOM.'...';
	$res=json_encode($response);
	die($res);
}

/**
 * LOGOUT ACTION
 */
if($arraExtVars['action'] == 'logout')
{
	include($dirModAdmin.'mod_bbdd.class.php');
	$dataModuleBBDD=new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name);
	$resConnexion=$dataModuleBBDD->connectDB();

	if($resConnexion)
		$dataModuleBBDD->logBackOffice();

	unset($_SESSION['userAdmin']);
	unset($_SESSION['id']);
	unset($_SESSION);
	session_destroy();
	$response['success']=true;
  $res=json_encode($response);
  die($res);
}


/**
 * GET DATA LIST FIELDS & STATUS ROW
 *
 * OBTENCION DE DATOS PARA EL LISTADO TABULAR
 *
 * modificado 2017.05.22 (M.F.)
 *
 */
if($arraExtVars['action'] == 'getTableListData')
{
	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModAdmin.'mod_common_lists.class.php');

	if(file_exists($dirModulesAdmin.'_modules/'.$modName.'/config_module.php'))
		include($dirModulesAdmin.'_modules/'.$modName.'/config_module.php');
//echo'--->';print_r($moduleSetting);
	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting);
	$resConnexion=$data->connectDB();

	if(!isset($_SESSION['userAdmin']))	// SE HA PERDIDO LA SESION
	{
		$data->logBackOffice(0,null,''.$arraExtVars['getTableListData'].' LOST-SESSION');
    $response['status']=-1;
    $response['rows']=-1;
    $response['rowCount']=0;
    $response['current']=0;
    $response['total']=0;
		$res=json_encode($response);
		die($res);
	}

  if(!$data->idConexion->sqlstate)
  {
    $response['status']=-1;
    $response['rows']=-1;
    $response['rowCount']=0;
    $response['current']=0;
    $response['total']=0;
    $res=json_encode($response);
    die($res);
  }

  // CURRENT LOCAL MODULE PARAMS
  include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleFolder'].'config_module.php');
//echo '==>__COMMON_ADMIN.PHP::';print_r($arraExtVars['moduleFolder']);
	$data->current=isset($arraExtVars['current']) ? (int)$arraExtVars['current'] : 1;
	$data->rowCount=$arraExtVars['rowCount'];
	$data->searchPhrase=$arraExtVars['searchPhrase'];
	$data->sortList=@$arraExtVars['sort'];
	$data->sortList=@$arraExtVars['sort'] ? $arraExtVars['sort'] : @$moduleSetting->defaultOrderField;
	$data->includeNumbers=@$arraExtVars['includeNumbers'];
	$data->moduleBBDD=isset($moduleSetting->moduleBBDD) ? $moduleSetting->moduleBBDD : null;	// PERMITE ESTABLECER OTRA BBDD PARA TRABAJAR EN EL MODULO ACTUAL
	$data->mainTable=$moduleSetting->mainTable;
	$data->fieldNames=$arraExtVars['fieldNames'];
	$data->listFieldsOutput=$arraExtVars['listFieldsOutput'];
	$data->defaultWhereSql=$arraExtVars['defaultWhereSql'];
	$data->skipBdDefault=isset($data->skipBdDefault) ? $data->skipBdDefault : $arraExtVars['skipBdDefault'];
	$data->additionalFilters=isset($arraExtVars['additionalFilters']) ? $arraExtVars['additionalFilters'] : null;
	$data->searchValue=$arraExtVars['searchPhrase'];
	$data->moduleFieldsStructure=$moduleSetting->arrFieldsFormModal;
	$resSql=$objData=$data->getRowsDataList();  // CARGA DE LOS DATOS DESDE LA BBDD PARA GENERAR EL LISTADO
	$arrPositinField=array();
	$response['rows']=null;

  // CONSTRUCCION DE CADA CELADA CON LOS VALORES A VISUALIZAR EN EL LISTADO
  if(count($objData) && count($arraExtVars['fieldNames']))
  {
		if(count($arraExtVars['fieldNames']) > 1)
		{
			foreach($objData as $key=>$value)
			{
				$countPos=0;
				foreach($arraExtVars['fieldNames'] as $fieldName=>$type)
				{
          if(!isset($value[$fieldName]))
            continue;

          $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
          $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
          $valueToDisplay=str_replace($search, $replace, $value[$fieldName]);

          if(isset($data->moduleFieldsStructure[$fieldName]['arrValues']))  // PERMITE DIBUJAR UN ALIAS AL VALOR ALMACENADO EN BBDD
            $valueToDisplay=$data->moduleFieldsStructure[$fieldName]['arrValues'][$value[$fieldName]];

          // REMOVE ALL HTML CODE
          if(isset($moduleSetting->arrFieldsFormModal[$fieldName]['tabularListData']['removeHtml']))
            $valueToDisplay=strip_tags($valueToDisplay);

          // OUTPUT DATE FORMAT
          if(isset($moduleSetting->arrFieldsFormModal[$fieldName]['tabularListData']['dateFormat']))
            $valueToDisplay=date($moduleSetting->arrFieldsFormModal[$fieldName]['tabularListData']['dateFormat'],strtotime($valueToDisplay));

					if(in_array($fieldName, $arrEnabledFieldStatus))  // CREA CONTROL PARA GESTION DE REGISTROS ACTIVOS/INACTIVOS (para bootgrid)
						$response['rows'][$key]['status']=$value[$fieldName] == 1 ? -1 : 2;

					$response['rows'][$key][$fieldName]=$valueToDisplay;

					$arrPositinField[$fieldName]=$countPos;
					$countPos++;
				}
			}
		}
  }

	if($response['rows'] == null)
	{
		$response['rows']=0;
//echo count($arraExtVars['fieldNames']).') fieldNames='.count($arraExtVars['fieldNames']);print_r($arraExtVars['fieldNames']);print_r($objData);
	}

//  else
//    $response['rows']=0;
//echo $db_type.') LIST REQUEST ---><hr><pre>';print_r($objData);print_r($data);echo'</pre>';die(0);

	$response['current']=isset($arraExtVars['current']) ? (int)$arraExtVars['current'] : 1;
	$response['total']=(int)$data->totalNumRows;
	$response['rowCount']=(int)$data->numRowsQuery;
	$response['dujokPosTd']=$arrPositinField;

  if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD)
	{
    $response['query']['sql'][]=$data->query;
		$response['query']['res'][]=$resSql;
	}

  $res=json_encode($response);
  die($res);
}


/**
 * ERASE RECORD FROM TABLE NAME=tblName
 */
if($arraExtVars['action'] == 'deleteRecord')
{
	$response['success']=false; $resSql=false;

	if(!isset($arraExtVars['idRef']))
	{
		$response['msgPainTxt']="\n???";
		$response['errorText']=__LOCALAJAX_DELETERECORD_REFERENCEMISSINGTXT."\n\n".__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
		$response['success']=false;
		$res=json_encode($response);
		die($res);
	}

	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModAdmin.'mod_common_lists.class.php');
	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
	$resConnexion=$data->connectDB();
	$eraseOptions=null;

	if(isset($arraExtVars['eraseOptions']))
		$eraseOptions=json_decode($arraExtVars['eraseOptions'],true);

	$resSql=$data->commonDeleteRecord($arraExtVars['mainTable'], $arraExtVars['keyListLabel'], $arraExtVars['idRef'], $eraseOptions);

	if($_SESSION['userAdmin']['pref_']['b_display_sql'])
	{
    $response['query']['sql'][]=$data->query;
		$response['query']['res'][]=$resSql;
	}

	if($resSql) // SQL SUCCESS
	{
		$response['msgPainTxt']="\n".@$arraExtVars['idRef'];
		$response['msgText']='<h2 class="c-white">'.__LOCALAJAX_DELETERECORD_DELETESUCCESSTXT.'</h2><br />ID = '.@$arraExtVars['idRef'];
		$response['success']=true;

		if(!$data->mNumRegs)	// SI LA SQL NO DEVUELVE REGISTROS ELIMINADOS
		{
			$response['msgPainTxt']="\n".@$arraExtVars['idRef'];
			$response['errorText']="\n".__LOCALAJAX_DELETERECORD_RESULTNORECORDSTXT;
			$response['errorText'].=($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD) ? "\n\n".$data->query : '';
			$response['success']=false;
		}
	}
	else	// SQL ERROR
	{
		$response['msgPainTxt']="\n".@$arraExtVars['idRef'];
		$response['errorText']=__LOCALAJAX_DELETERECORD_SQLERRORDELETEFAILTXT."\n\n".__LOCALAJAX_DELETERECORD_SQLERRORDELETEFAILBOTTOMTXT;
		$response['errorText'].=($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD) ? "\n".$data->query : '';
		$response['success']=false;
	}

	$res=json_encode($response);
	die($res);
}

/**
 * ENABLE / DISABLE RECORD FROM TABLE NAME=tblName
 */
if($arraExtVars['action'] == 'newStatusRecord')
{
	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModAdmin.'mod_common_lists.class.php');
	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
	$resConnexion=$data->connectDB();

	$resSql=$data->updateStatusRow($arraExtVars['tblName'], $arraExtVars['id'], null, $arraExtVars['fieldNameToSet'], $arraExtVars['newStatusValue']);

	if($_SESSION['userAdmin']['pref_']['b_display_sql'])
	{
    $response['query']['sql'][]=$data->query;
    $response['query']['res'][]=$resSql;
	}

	$response['success']=$resSql;
	$response['newStatus']=($arraExtVars['newStatusValue'] == 1) ? 0 : 1;
  $res=json_encode($response);
  die($res);
}

if($arraExtVars['action'] == 'insert' && isset($arraExtVars['mainKeyName']))
{
	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModAdmin.'mod_common_lists.class.php');
	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
	$resConnexion=$data->connectDB();
  $msgInfo='';

	$methodName=str_replace('-', ' ', $arraExtVars['action']);
	$methodName=ucfirst($methodName);
	$methodName=str_replace(' ', '', $methodName);
	$tableName=str_replace('_', ' ', key($arraExtVars['db']));
	$tableName=ucwords($tableName);
	$tableName=str_replace(' ', '', $tableName);
	$methodName='bd'.$methodName.'_'.$tableName;
	$insertBd=true;

	$provenience=$_SERVER['HTTP_REFERER'];
//	$modNameFrom=strpos($provenience,'_modules/')+9;
//	$modName=substr($provenience, $modNameFrom);
//	$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));

	if(file_exists($dirModulesAdmin.'_modules/'.$modName.'/config_module.php'))
		include($dirModulesAdmin.'_modules/'.$modName.'/config_module.php');


	/**
	 * START GESTION DE CAMPOS TRATADOS DE MANERA DIFERENTE SEGÚN LA LA TABLA PROCESADA
	 */
	if(key($arraExtVars['db']) == 'adminusers')	// PASSWORD USUARIOS ADMIN
	{
		$passUser=trim($arraExtVars['db'][key($arraExtVars['db'])]['password']);
		$passUser=md5($passUser.$clave_acceso);
		$arraExtVars['db'][key($arraExtVars['db'])]['password']=$passUser;
	}
	// END GESTION DE CAMPOS TRATADOS DE MANERA DIFERENTE SEGÚN LA LA TABLA PROCESADA

	if(isset($objForm) && method_exists($objForm, $methodName))
	{
    echo $modName."\n existe ".$methodName."\n!!!!!!GESTION PERSONALIZADA PARA ESTE MODULO ";die();
	}
	else
	{
		foreach($moduleSetting->arrFieldsFormModal as $tmpFieldName=>$value)  // SE RECORREN TODOS LOS CAMPOS A EVALUAR (TAMBIEN COPIAR (SI HUBIERE) LOS ARCHIVOS AL SERVIDOR)
		{
			$isErasable=true;

			if(isset($value['encrypt']))	// SE NECESITA ENCRITTAR UN CAMPO ESPECIFICO
			{
				if($value['encrypt'] == 'md5')	// CRIPTADO DEL CAMPO EN MD5 NORMAL
				{
					$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]=md5($arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]);
				}
			}

			if($value['formType'] == 'file')	// ONLY FOR DATA input TYPE file
			{

        if(!file_exists($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder']))
        {
					$response['msgTitle']=__COMMON_GENERICTEXT_ERROR.': '.__LOCALAJAX_FOLDERERROR_UNAIVALABLEORNOTEXISTTX.' <span class="c-red bgm-white">` '.$moduleSetting->arrFieldsFormModal[$tmpFieldName]['htmlFolder'].'` </span>';
					$response['msgPainTxt']=__LOCALAJAX_FOLDERERROR_FOLDERNOAVAILABLETXT."\n".$moduleSetting->arrFieldsFormModal[$tmpFieldName]['htmlFolder'];
					$response['errorText']=__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
					$response['msgText']='<h2 class="c-yellow">'.__LOCALAJAX_COMMON_ABORTEDOPERATIONIXT.'</h2><br />'.__LOCALAJAX_COMMON_FOLDERERRORTXT;
					$response['success']=0;
					$insertBd=false;
					break;
        }

				$imageFileName=@$_FILES['arrFiles']['name'][key($arraExtVars['arrFiles'])][$tmpFieldName];
				$tmpName=strtolower($imageFileName);
				$tmpName=str_replace(' ','_',$tmpName);
				$tmpName=preg_replace('/[^a-zA-Z0-9_.-]/', '', $tmpName);
				$tmp_file=$tmpName;

				if($tmp_file == @$arraExtVars['previousImage'][$tmpFieldName])
				{
					$msgInfo.=' ('.__AJAX_COMMON_FILENOTSENTTXT.' `'.$tmp_file.'`)' ;
					continue;
				}


				// SE SUSTITUYE UNA IMAGEN EXISTENETE ANTERIOR EN LA BBDD
				if(@$arraExtVars['previousImage'][$tmpFieldName] != $tmp_file && $tmp_file)
				{

					$msgInfo.=' ('.__AJAX_COMMON_FILECHANGEDERRORTXT.' `'.$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName].'` '.__COMMON_GENERICTEXT_FORTXT.' `'.$tmp_file.'`)' ;
					$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]=$tmp_file;

					// SE HA OPTADO POR BORRAR UNA IMAGEN Y DESPUES COLOCAR OTRA NUEVA
					if(@$arraExtVars['previousImage'][$tmpFieldName] && @$arraExtVars['eraseFiles'][key($arraExtVars['db'])][$tmpFieldName] != $tmp_file && @$arraExtVars['previousImage'][$tmpFieldName] == @$arraExtVars['previousImage'][@$arraExtVars['eraseFiles'][key($arraExtVars['db'])][$tmpFieldName]])
					{
						unlink($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$arraExtVars['previousImage'][$tmpFieldName]);
					}
					else
					{
            // SE HA CAMBIADO LA IMAGEN EXISTENTE EN BBDD POR OTRA, LA ANTIGUA SE SUPRIME DEL SERVIDOR
            if($arraExtVars['previousImage'][$tmpFieldName] && $tmp_file != @$arraExtVars['previousImage'][$tmpFieldName])
            {
  						unlink($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$arraExtVars['previousImage'][$tmpFieldName]);
            }
					}

					$isErasable=false;
				}

				if($tmpName && file_exists($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$tmp_file) && $tmp_file != @$arraExtVars['previousImage'][$tmpFieldName])
				{
					$response['msgTitle']=__COMMON_GENERICTEXT_ERROR.': '.__COMMON_GENERICTEXT_THEFILETXT.' <span class="c-red bgm-white">`'.$tmp_file.'`</span> '.__COMMON_GENERICTEXT_ALREADYEXISTINFOLDERTXT;
					$response['msgPainTxt']=__COMMON_GENERICTEXT_ERROR.': '.__COMMON_GENERICTEXT_THEFILETXT.' `'.$tmp_file.'` '.__COMMON_GENERICTEXT_ALREADYEXISTINFOLDERTXT;
					$response['errorText']=__LOCALAJAX_COMMON_INCOMPLETEOPERATIONIXT.' ';
					$response['msgText']='<h2 class="c-yellow">'.__LOCALAJAX_COMMON_ABORTEDOPERATIONIXT.'</h2><br />'.__LOCALAJAX_COMMON_FILEERRORIXT;
					$response['success']=0;
					$insertBd=false;
					break;
				}

				if(@$arraExtVars['eraseFiles'][key($arraExtVars['db'])][$tmpFieldName] == $tmpFieldName && $isErasable)
				{
					if(file_exists($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]))
					{
						unlink($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]);
						$isErasable=false;
					}
					$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]='';	// SE VACIA EL VALOR DEL CAMPOPARA LA IMAGEN SUPRIMIDA
				}

				// SUSTITUCION DE UN ARCHIVO POR OTRO
				if(isset($_FILES['arrFiles']['tmp_name'][key($arraExtVars['arrFiles'])][$tmpFieldName]) && $arraExtVars['previousImage'][$tmpFieldName] != @$arraExtVars['arrFiles'][key($arraExtVars['db'])][$tmpFieldName]['fileName'] && $arraExtVars['previousImage'][$tmpFieldName] && ($arraExtVars['previousImage'][$tmpFieldName] != $tmp_file))
				{
					@unlink($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$arraExtVars['previousImage'][$tmpFieldName]);	//  SE ELIMINA EL ANTERIOR ARCHIVO A SUSTITUIR
				}

				if(!$tmp_file)
					continue;

				$resFile=@copy($_FILES['arrFiles']['tmp_name'][key($arraExtVars['arrFiles'])][$tmpFieldName], $moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$tmp_file);
				$response['success']=$resFile;

				if(!$resFile)	// SI HAY ERRORES EN LA COPIA, SE FINALIZA COMO ERRO
				{
					$response['msgTitle']=__LOCALAJAX_COMMON_FILECOPYERRORIXT.' '.$msgInfo;
					$response['msgPainTxt']=__LOCALAJAX_COMMON_FILECOPYERRORIXT.' '.$msgInfo;
					$response['errorText']=__LOCALAJAX_COMMON_FILEPHYSICALCOPYERRORIXT.' '.$tmp_file;
					$response['msgText']='<h2>'.__LOCALAJAX_COMMON_ABORTEDOPERATIONIXT.'</h2><br />'.__LOCALAJAX_COMMON_FILEERRORIXT;
					$response['success']=0;
					$insertBd=false;
					break;
				}
			}
		}


		/**
		 * SE EJECUTA EL INSERT EN LA BBDD
		 * SOLO SI NO OCURREN ERRORES ANTES
		 */
		if($insertBd)
		{
			$res=$data->commonInsert(key($arraExtVars['db']), $arraExtVars['mainKeyName'], $arraExtVars['mainKeyId'], $arraExtVars['db'][key($arraExtVars['db'])]);
			$response['success']=(int)$res;

			if($_SESSION['userAdmin']['pref_']['b_display_sql'])
			{
				$response['query']['sql'][]=$data->query;
		    $response['query']['res'][]=$res;
			}

			if(!$res)
			{
				$response['errorText']=__LOCALAJAX_COMMON_REVIEWCHANGESMADETXT;
				$response['msgTitle']='<h4>'.__AJAX_INSERTERROR_INSERTBBDDABORTTXT.'</h4>';
				$response['msgPainTxt']=__AJAX_INSERTERROR_INSERTBBDDABORTTXT.(isset($data->mError) && $_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD ? ', '.__LOCALAJAX_COMMON_DETAILERRORTXT.': '.$data->mError : '');;
				$response['msgText']=__LOCALAJAX_COMMON_HASBEENIMPOSSIBLETXT.'<br />'.__LOCALAJAX_INSERTERROR_INSERTINCOMPLETETXT.'';
			}
		}
//echo $insertBd."\n NO existe ".$methodName.', se insertarà con una funcion standard';
	}

	$res=json_encode($response);
	die($res);

echo "\nFIN"."\n".'[---><pre>';print_r($arraExtVars);
echo '[--->files: ';print_r($_FILES);echo'</pre>';die(0);
}


/**
 * ACCION PARA INSERTAR / ACTUALIZAR REGISTROS EN BBDD
 *
 * IMPORTANTE
 * LAS ESTRUCTURA DEL ARRAY DE DATOS RECIBIDO DE LOS CAMPOS DEL PROCESO
 * RECIBIDO EN ESTE AJAX ES EL SIGUIENTE:
 * NIVEL 1: "db" IDENTIFICA QUE TODOS LOS ELEMENTOS SUBYACIENTES PERTENECEN A LA BBDD
 * NIVEL 2: TABLE_NAME ES EL NOMBRE DE LA TABLA A LA QUE PERTENECEN LOS ELEMENTOS DEL SIGUIENTE NIVEL
 * NIVEL 3: FIELD_NAMES SON LOS NOMBRES DE LOS CAMPOS
 * NIVEL 4: **ARRAY_VALUES** PARA CAMPOS CON MULTIPLES VALORES (HABITUALMENTE check-boxes, selects-multiples)
 *
 */
if($arraExtVars['action'] == 'update' && isset($arraExtVars['mainKeyId']))
{
	include($dirModAdmin.'mod_bbdd.class.php');

	$methodName=str_replace('-', ' ', $arraExtVars['action']);
	$methodName=ucfirst($methodName);
	$methodName=str_replace(' ', '', $methodName);
	$tableName=str_replace('_', ' ', key($arraExtVars['db']));
	$tableName=ucwords($tableName);
	$tableName=str_replace(' ', '', $tableName);
	$methodName='bd'.$methodName.'_'.$tableName;
	$updateBd=true;

	$provenience=$_SERVER['HTTP_REFERER'];
//	$modNameFrom=strpos($provenience,'_modules/')+9;
//	$modName=substr($provenience, $modNameFrom);
//	$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));

	if(file_exists($dirModulesAdmin.'_modules/'.$modName.'/config_module.php'))
		include($dirModulesAdmin.'_modules/'.$modName.'/config_module.php');

	include($dirModAdmin.'mod_common_edit.class.php');
	$data=new CommonEditingModel($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting);
	$resConnexion=$data->connectDB();
//echo "\n>";print_r($moduleSetting);

	if(method_exists(@$objForm, $methodName))
	{
		$response['errorText']='CREAR METODO ESPECIFICO PARA ESTA ACCION';
		$response['msgTitle']='<h4>Operaci&oacute;n&nbsp;ABORTADA!</h4>';
		$response['msgPainTxt']=__LOCALAJAX_COMMON_ABORTEDOPERATIONIXT.'!';
		$response['msgText']=__LOCALAJAX_COMMON_HASBEENIMPOSSIBLETXT.'<br />completar la operaci&oacute;n';
echo "\n existe ".$methodName."\n!!!!!!gestion personalizada para este modulo ";die();
	}
	else
	{
		/**
		 * *********************************************
		 * METODO COMUN DE UPDATE PARA TODOS LOS MODULOS
		 * *********************************************
		 *
		 */
		$msgInfo='';

    // SE RECORREN TODOS LOS CAMPOS A EVALUAR PARA COPIAR (SI HUBIERE) LOS ARCHIVOS AL SERVIDOR
		foreach($moduleSetting->arrFieldsFormModal as $tmpFieldName=>$value)
		{
			$isErasable=true;

			if(isset($value['encrypt']))	// SE NECESITA ENCRITTAR UN CAMPO ESPECIFICO
			{
					if($value['encrypt'] == 'md5')	// CRIPTADO DEL CAMPO EN MD5 NORMAL
					{
						$passUser=trim($arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]);
						$passUser=md5($passUser.$clave_acceso);
						$resData=$data->getCurrentFieldValue(key($arraExtVars['db']), $arraExtVars['mainKeyName'], $arraExtVars['mainKeyId'], $arraExtVars['db'][key($arraExtVars['db'])], $tmpFieldName);

						if($resData != $arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName])	// NO CORRESPONDE AL VALOR ALMACENADO PREVIAMENTE, SE DEBE ACTUALIZAR
						{
							$passUser=trim($arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]);
							$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]=md5($passUser);
						}
				}
			}

			if($value['formType'] == 'file')	// ONLY FOR DATA input TYPE file
			{

        if(!file_exists($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder']))
        {
					$response['msgTitle']=__COMMON_GENERICTEXT_ERROR.': '.__LOCALAJAX_FOLDERERROR_UNAIVALABLEORNOTEXISTTX.' <span class="c-red bgm-white">` '.$moduleSetting->arrFieldsFormModal[$tmpFieldName]['htmlFolder'].'` </span>';
					$response['msgPainTxt']=__LOCALAJAX_FOLDERERROR_FOLDERNOAVAILABLETXT.':'."\n".$moduleSetting->arrFieldsFormModal[$tmpFieldName]['htmlFolder'];
					$response['errorText']=__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
					$response['msgText']='<h2 class="c-yellow">'.__LOCALAJAX_COMMON_ABORTEDOPERATIONIXT.'</h2><br />'.__LOCALAJAX_COMMON_FOLDERERRORTXT;
					$response['success']=0;
					$updateBd=false;
					break;
//echo"\n".'field name ='.$tmpFieldName;
//echo"\n".'targetFolder ='.($moduleSetting->arrFieldsFormModal[$tmpFieldName]['htmlFolder']);
//echo"\n".'========>('.@$arraExtVars['previousImage'][$tmpFieldName];
//echo")\n arraExtVars=";print_r($arraExtVars);
//echo"\nfiles : ";
//print_r($_FILES);
//die("\nSTOP");
        }

				$imageFileName=@$_FILES['arrFiles']['name'][key($arraExtVars['arrFiles'])][$tmpFieldName];
				$tmpName=strtolower($imageFileName);
				$tmpName=str_replace(' ','_',$tmpName);
				$tmpName=preg_replace('/[^a-zA-Z0-9_.-]/', '', $tmpName);
				$tmp_file=$tmpName;

				if($tmp_file == @$arraExtVars['previousImage'][$tmpFieldName])
				{
					$msgInfo.=' ('.__AJAX_COMMON_FILENOTSENTTXT.' `'.$tmp_file.'`)' ;
					continue;
				}


				// SE SUSTITUYE UNA IMAGEN EXISTENETE ANTERIOR EN LA BBDD
				if(@$arraExtVars['previousImage'][$tmpFieldName] != $tmp_file && $tmp_file)
				{

					$msgInfo.=' ('.__AJAX_COMMON_FILECHANGEDERRORTXT.' `'.$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName].'` '.__COMMON_GENERICTEXT_FORTXT.' `'.$tmp_file.'`)' ;
					$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]=$tmp_file;

					// SE HA OPTADO POR BORRAR UNA IMAGEN Y DESPUES COLOCAR OTRA NUEVA
					if(@$arraExtVars['previousImage'][$tmpFieldName] && @$arraExtVars['eraseFiles'][key($arraExtVars['db'])][$tmpFieldName] != $tmp_file && @$arraExtVars['previousImage'][$tmpFieldName] == @$arraExtVars['previousImage'][@$arraExtVars['eraseFiles'][key($arraExtVars['db'])][$tmpFieldName]])
					{
						unlink($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$arraExtVars['previousImage'][$tmpFieldName]);
					}
					else
					{
            // SE HA CAMBIADO LA IMAGEN EXISTENTE EN BBDD POR OTRA, LA ANTIGUA SE SUPRIME DEL SERVIDOR
            if($arraExtVars['previousImage'][$tmpFieldName] && $tmp_file != @$arraExtVars['previousImage'][$tmpFieldName])
            {
  						unlink($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$arraExtVars['previousImage'][$tmpFieldName]);
            }
					}

					$isErasable=false;
				}

				if($tmpName && file_exists($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$tmp_file) && $tmp_file != @$arraExtVars['previousImage'][$tmpFieldName])
				{
					$response['msgTitle']=__COMMON_GENERICTEXT_ERROR.': '.__COMMON_GENERICTEXT_THEFILETXT.' <span class="c-red bgm-white">`'.$tmp_file.'`</span> '.__COMMON_GENERICTEXT_ALREADYEXISTINFOLDERTXT;
					$response['msgPainTxt']=__COMMON_GENERICTEXT_ERROR.': '.__COMMON_GENERICTEXT_THEFILETXT.' `'.$tmp_file.'` '.__COMMON_GENERICTEXT_ALREADYEXISTINFOLDERTXT;
					$response['errorText']=__LOCALAJAX_COMMON_INCOMPLETEOPERATIONIXT.' ';
					$response['msgText']='<h2 class="c-yellow">'.__LOCALAJAX_COMMON_ABORTEDOPERATIONIXT.'</h2><br />'.__LOCALAJAX_COMMON_FILEERRORIXT;
					$response['success']=0;
					$updateBd=false;
					break;
				}

				if(@$arraExtVars['eraseFiles'][key($arraExtVars['db'])][$tmpFieldName] == $tmpFieldName && $isErasable)
				{
					if(file_exists($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]))
					{
						unlink($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]);
						$isErasable=false;
					}
					$arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]='';	// SE VACIA EL VALOR DEL CAMPOPARA LA IMAGEN SUPRIMIDA
				}

				// SUSTITUCION DE UN ARCHIVO POR OTRO
				if(isset($_FILES['arrFiles']['tmp_name'][key($arraExtVars['arrFiles'])][$tmpFieldName]) && $arraExtVars['previousImage'][$tmpFieldName] != @$arraExtVars['arrFiles'][key($arraExtVars['db'])][$tmpFieldName]['fileName'] && $arraExtVars['previousImage'][$tmpFieldName] && ($arraExtVars['previousImage'][$tmpFieldName] != $tmp_file))
				{
					@unlink($moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$arraExtVars['previousImage'][$tmpFieldName]);	//  SE ELIMINA EL ANTERIOR ARCHIVO A SUSTITUIR
				}

				if(!$tmp_file)
					continue;

				$resFile=@copy($_FILES['arrFiles']['tmp_name'][key($arraExtVars['arrFiles'])][$tmpFieldName], $moduleSetting->arrFieldsFormModal[$tmpFieldName]['targetFolder'].$tmp_file);
				$response['success']=$resFile;

				if(!$resFile)	// SI HAY ERRORES EN LA COPIA, SE FINALIZA COMO ERRO
				{
					$response['msgTitle']=__LOCALAJAX_COMMON_FILECOPYERRORIXT.' '.$msgInfo;
					$response['msgPainTxt']=__LOCALAJAX_COMMON_FILECOPYERRORIXT.' '.$msgInfo;
					$response['errorText']=__LOCALAJAX_COMMON_FILEPHYSICALCOPYERRORIXT.' '.$tmp_file;
					$response['msgText']='<h2>'.__LOCALAJAX_COMMON_ABORTEDOPERATIONIXT.'</h2><br />'.__LOCALAJAX_COMMON_FILEERRORIXT;
					$response['success']=0;
					$updateBd=false;
					break;
				}
			}

			/**
			 * AL NO SER UN CAMPO DE LA MAIN TABLE, HA DE GESTIONARSE ANTES
			 * PARA PERMITIR GESTIONAR POSIBLES ERRORES Y ES SU CASO IMPEDIR
			 * LA ACTUALIZACION DE LA MAIN TABLE
			 */
			if(isset($value['excludeMainTable']) && $value['excludeMainTable'])	// NO ES UN CAMPO DE LA MAIN TABLE
			{
				if(count($moduleSetting->arrFieldsFormModal[$tmpFieldName]['sourceTagBbddData']))
				{
					$resData=$data->updateRelatedTable($moduleSetting->arrFieldsFormModal[$tmpFieldName]['sourceTagBbddData']->relatedTableName, $moduleSetting->arrFieldsFormModal[$tmpFieldName]['sourceTagBbddData']->keyFieldName, $moduleSetting->arrFieldsFormModal[$tmpFieldName]['sourceTagBbddData']->arrRelatedFields[0], $arraExtVars['mainKeyId'], $arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]);

					if(!$resData)
					{
						$response['msgTitle']=__COMMON_GENERICTEXT_ERROR.': '.__COMMON_GENERICTEXT_THEFILETXT.'<span class="c-red bgm-white">`'.$tmp_file.'`</span> ';
						$response['msgPainTxt']=' '.__LOCALAJAX_COMMON_RELATEDQUERYERRORIXT;
						$response['errorText']=__LOCALAJAX_COMMON_INCOMPLETEOPERATIONIXT.' ';
						$response['msgText']='<h2 class="c-yellow">'.__LOCALAJAX_COMMON_ABORTEDOPERATIONIXT.'</h2><br />'.__LOCALAJAX_COMMON_FILEERRORIXT;
						$response['success']=0;
						$updateBd=false;
					}
				}
				unset($arraExtVars['db'][key($arraExtVars['db'])][$tmpFieldName]);	// SE OMITE MANEJAR ESTE CAMPO EN LA TABLA PRINCIPAL
			}
		}

		if($updateBd)	// SE EJECUTA EL UPDATE DE LA BBDD SOLO SI NO OCURREN ERRORES ANTES
		{
			/**
			 * START GESTION DE CAMPOS TRATADOS DE MANERA DIFERENTE SEGÚN LA LA TABLA PROCESADA
			 */
			if(key($arraExtVars['db']) == 'adminusers')
			{
				// CHECK PASSWORD USUARIO ADMIN CON MD5 + HASH
				$passUser=trim($arraExtVars['db'][key($arraExtVars['db'])]['password']);
				$passUser=md5($passUser.$clave_acceso);
				$resData=$data->getCurrentFieldValue(key($arraExtVars['db']), $arraExtVars['mainKeyName'], $arraExtVars['mainKeyId'], $arraExtVars['db'][key($arraExtVars['db'])], 'password');

				if($resData != $arraExtVars['db'][key($arraExtVars['db'])]['password'])	// NO CORRESPONDE AL VALOR ALMACENADO PREVIAMENTE, SE DEBE ACTUALIZAR
				{
					$passUser=trim($arraExtVars['db'][key($arraExtVars['db'])]['password']);
					$arraExtVars['db'][key($arraExtVars['db'])]['password']=md5($passUser.$clave_acceso);
				}
			}
			// END GESTION DE CAMPOS TRATADOS DE MANERA DIFERENTE SEGÚN LA LA TABLA PROCESADA

			$res=$data->commonUpdate(key($arraExtVars['db']), $arraExtVars['mainKeyName'], $arraExtVars['mainKeyId'], $arraExtVars['db'][key($arraExtVars['db'])]);
			$response['success']=(int)$res;

			if($_SESSION['userAdmin']['pref_']['b_display_sql'])
			{
				$response['query']['sql'][]=$data->query;
				$response['query']['res'][]=$res;
			}

			if(!$res)
			{
				$response['errorText']=__LOCALAJAX_COMMON_REVIEWCHANGESMADETXT;
				$response['msgTitle']='<h4>'.__LOCALAJAX_COMMON_UNFINISHEDOPERATIONTXTLONG.'</h4>';
				$response['msgPainTxt']=__LOCALAJAX_COMMON_UNFINISHEDOPERATIONTXT.(isset($data->mError) && $_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD ? ', '.__LOCALAJAX_COMMON_DETAILERRORTXT.': '.$data->mError : '');
				$response['msgText']=__LOCALAJAX_COMMON_HASBEENIMPOSSIBLETXT.'<br />'.__LOCALAJAX_COMMON_ALERTFINISHOPERATIONTXT;
			}
		}
	}

  $res=json_encode($response);
  die($res);
}

/**
 * LOCAL MODULE HELP + ALL COMMON HELP FILES
 */
if($arraExtVars['action'] == 'getHelpModule')
{
	if(isset($arraExtVars['renderHtml']) && !@$_SESSION['userAdmin']['pref_'])
		die('<span style="color:red;font-size:2em;font-family:verdana,arial;"><center>'.__AJAX_ERRORBBDD_LOSTCONNECTIONTXT.'!</center></span><br />');

	if(!isset($arraExtVars['modulePath']) || !$arraExtVars['modulePath'])
	{
    $response['success']=false;
    $response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-block-alt"></i></h1>'.__AJAX_COMMON_SEEMSMISSINGTXT.'<br /><h1>'.__AJAX_COMMON_MODULEPARAMETERTXT.' <u>modulePath</u></h1></span><br />';
    $res=json_encode($response);
    die($res);
	}

  include($dirModulesAdmin.'_modules/help_modules_contents.php');

  $res='<script src="'.$web_url.$adminFolder.'/vendors/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>';
	$resContentsSys=null; $resContents=null; $resContentsBO=null; $resContentsMC=null;

	// SOLO USUARIOS CON EL MAXIMO NIVEL DE ACCESO
	if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD)
	{
		$res.='<div class="card-body card-padding"><div role="tabpanel"><ul class="tab-nav" role="tablist" data-tab-color="green">';
		$res.='<li class="active"><a href="#tabSys" aria-controls="mainSys" role="tab" data-toggle="tab" aria-expanded="false">'.__COMMON_GENERICTEXT_SYSTEMTXT.'</a></li>';
		$res.='<li><a href="#tabUsu" aria-controls="mainSys" role="tab" data-toggle="tab" aria-expanded="false" title="'.__LOCALAJAX_GETHELPMODULE_SPECIFICHELPMODULETITLE.'">'.__COMMON_GENERICTEXT_USERTXT.'</a></li>';
		$res.='<li><a href="#tabBO" aria-controls="mainSys" role="tab" data-toggle="tab" aria-expanded="false" title="'.__LOCALAJAX_GETHELPMODULE_BACKOFFICENOTESTITLE.'">'.__LOCALAJAX_GETHELPMODULE_BOTXT.'</a></li>';
		$res.='<li><a class="mainHelConfigModule" href="#tabMC" aria-controls="mainSys" role="tab" data-toggle="tab" aria-expanded="false" title="'.__LOCALAJAX_GETHELPMODULE_CONFIGFILETITLE.' (config_module.php)">'.__LOCALAJAX_GETHELPMODULE_CONFIGMODULETXT.'</a></li>';
	  $res.='</ul>';
		$res.='<div class="tab-content">';
		$res.='<div class="tab-pane fade active in" id="tabSys">';
		$resContentsSys=@file_get_contents($dirModulesAdmin.$arraExtVars['modulePath'].'help_system.php');
		$res.=$resContentsSys;
		$res.='</div>';
		$res.='<div class="tab-pane fade" id="tabBO">';
		$resContentsBO=@file_get_contents($dirModulesAdmin.'_modules/help_common_system.html');
		$res.=$resContentsBO;
		$res.='</div>';
		$res.='<div class="tab-pane fade" id="tabMC">';
		$resContentsMC=@file_get_contents($dirModulesAdmin.'_modules/help_config_modules.html');
		$res.=$resContentsMC;
		$res.='</div>';
		$res.='<div class="tab-pane fade" id="tabUsu">';
	}

	// AYUDA ESPECIFICA PARA EL MODULO ABIERTO (PARA TODOS LOS USUARIOS)
	$resContents=@file_get_contents($dirModulesAdmin.$arraExtVars['modulePath'].'help_module.php');
	$res.=$resContents;

	// SOLO USUARIOS CON EL MAXIMO NIVEL DE ACCESO
	if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD)
		$res.='</div></div></div>';

	// PERMITE INCLUIR LA NUMERACION AUTOMATICA PARA LOS TABS PRESENTES EN LOS ARCHIVOS
	$res.='<script type="text/javascript">
	$(document).ready(function(){

	$(".gotoHelpTag").click(function(){
		var tmpTarget=$(this).attr("data-target-tag");
		var skipScroll=$(this).attr("data-no-scroll") ? true : false;
		if(tmpTarget != null)
			$.each(tmpTarget.split(","),function(n,v){
				if($(v))
				{
					$(v).click();//.focus();
					if($(v).parents("div")[2].id != "accordFMD")	// EXCEPCIONES PARA OMITIOR EL SCROLL AUTOMATICO
					{
						if(skipScroll) return true;
						$(v).parents().animate({scrollTop:200},"slow");
					}
				}
				return true;
			})
	});

	$(".gotoHelpTag").each(function(){
		$(this).attr("data-trigger","hover");
		$(this).attr("data-placement","auto");
		$(this).attr("data-content",$(this).text());
		$(this).popover({title: "Ir a"});
	})

	$(".help-basic-blocks").bootstrapWizard({tabClass:"fw-nav",nextSelector:".next",previousSelector: ".previous"	});
	$(\'[data-toggle="tooltip"]\').tooltip();
});</script>';

  if(!$resContents && !$resContentsSys  && !$resContentsBO)	// NO SE HAN ENCONTRADO CONTENIDOS A MOSTRAR
  {
    $response['success']=false;
    $response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-block-alt"></i></h1>'.__LOCALAJAX_GETHELPMODULE_MODULEGENERICERRORTXT.'  <h1>'.$arraExtVars['moduleTitle'].'</h1></span><br />';
    $res=json_encode($response);
    die($res);
  }

  include($dirModAdmin.'mod_bbdd.class.php');
  include($dirModAdmin.'mod_common_lists.class.php');
  $data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
  $resConnexion=$data->connectDB();
  $data->logBackOffice(0,$arraExtVars['modulePath'],' '.$arraExtVars['moduleTitle']);

	$response['success']=true;
	$response['txtContents']='<span class="c-black ">'.$res.'</span><br />';

  if(isset($arraExtVars['renderHtml']) && $arraExtVars['renderHtml'])
  {
    $htmlOut='<span class="c-black ">'.$res.'</span><br />';
  }
  else
  {
  	$res=json_encode($response);
  	die($res);
  }
//echo $res."\n".date('H:i:s').'] getHelpModule:: '.$dirModulesAdmin.$arraExtVars['modulePath'].'config_module.php'."\n";print_r($arraExtVars);echo'</pre>';die(0);
}

/**
 * HTML FORMAT AND READ CURRENT MODULE CONFIG FILE
 */
if($arraExtVars['action'] == 'getConfigData')
{
	if(!isset($arraExtVars['modulePath']) || !$arraExtVars['modulePath'])
	{
    $response['success']=false;
    $response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-block-alt"></i></h1>'.__AJAX_COMMON_SEEMSMISSINGTXT.'<br /><h1>'.__AJAX_COMMON_MODULEPARAMETERTXT.' <u>modulePath</u></h1></span><br />';
    $res=json_encode($response);
    die($res);
	}

  include($dirModulesAdmin.'_modules/help_modules_contents.php');	// BBDD METHODS
  @include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.ADMINFOLDER.'/_classes/'.'common_ajax_methods.class.php');	// COMMON AJAX METHODS
  $res=false;
	$ajax=new CommonAjaxMethods();
	$fileContent=$ajax->getConfigModuleFile($dirModulesAdmin, $arraExtVars);	// LOAD FILE CONTENTS

	$language='php';
	$res=$ajax->highLightContentFile($language, $fileContent, $dirModulesAdmin, $arraExtVars['moduleTitle'], @$_SESSION['userAdmin']['pref_']['view_line_numbers']);	// PHP CONTENTS SYNTAX HIGHLIGHTER

	if($_SESSION['userAdmin']['pref_']['b_display_sql'])
	{
    $response['query']['sql'][]=$data->query;
		$response['query']['res'][]=$res;
	}

  if(!$res || !trim($res))
  {
    $response['success']=false;
    $response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-thumb-down"></i></h1> <h4>'.__AJAX_GETCONFIGDATA_NOAPPLYHIGHLIGHTFUNCTXT.' <u>'.$arraExtVars['moduleTitle'].'</u></h4></span><br />';
    $res=json_encode($response);
    die($res);
  }

	$txtContentsHighLight='<form method="post" id="cfgEdit" name="cfgEdit" onsubmit="return false;" enctype="multipart/form-data"><div id="note-editor-1" contenteditable="true" class="scrContents html-editor-airmod note-air-editor note-editable panel-body">'.$res.'</div></form><br /><div class="text-center w-100"><button class="btn c-black waves-effect" data-dismiss="modal">'.__COMMON_BTN_CLOSE.'</button>&nbsp;&nbsp;<button class="btn btn-success hec-save">'.__COMMON_BTN_SAVE.'</button></div><br /><br />';

	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModAdmin.'mod_common_lists.class.php');
	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
	$resConnexion=$data->connectDB();
  $data->logBackOffice(null,$arraExtVars['modulePath']);
	$response['success']=true;//w-100
	$response['txtContents']=$txtContentsHighLight;
	$res=json_encode($response);
	die($res);
}

/**
 * UPDATE CURRENT MODULE CONFIG FILE
 */
if(isset($arraExtVars['action']) && $arraExtVars['action'] == 'updateConfigModule')
{
  include($dirModulesAdmin.'_modules/help_modules_contents.php');	// BBDD METHODS
  @include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.ADMINFOLDER.'/_classes/'.'common_ajax_methods.class.php');	// COMMON AJAX METHODS
  $res=false;
	$ajax=new CommonAjaxMethods();
	$targetFileName=str_pad(@$_SESSION['userAdmin']['userId'],3,'0',STR_PAD_LEFT).'-'.date('Ymd').'_'.$arraExtVars['fileName'];
	$res=$ajax->backUpFile($arraExtVars['fileName'], $dirModulesAdmin.$arraExtVars['pathFile'], null, $dirModulesAdmin.$arraExtVars['pathFile'].'_backups/'); // BACK-UP FOR ORIGINAL FILE

	if(!$res)
	{
    $response['success']=false;
    $response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-block-alt"></i></h1><h2>'.__LOCALAJAX_UPDATECONFIGMODULE_FILECOPYBACKUPERRORTXT.'</h2></span><br />';
    $res=json_encode($response);
    die($res);
	}

	$contentClean=$arraExtVars['fileContent'];
	$contentClean=str_replace('<br>',"\n",$contentClean);
	$contentClean=str_replace('</br>',"\n",$contentClean);
	$contentClean=str_replace('<br />',"\n",$contentClean);
	$contentClean=strip_tags($contentClean);
	$contentClean=html_entity_decode($contentClean);
//	$asciiChars='a-zA-Z0-9\s`~!@#$%^&*()_+-={}|:;<>?,.\/\"\'\\\[\]';
//	$contentClean=preg_replace("/[^$asciiChars]/", '', $contentClean);
	$res=$ajax->updateConfigModuleFile($arraExtVars['fileName'], $dirModulesAdmin.$arraExtVars['pathFile'], $contentClean);

	if(!$res)
	{
    $response['success']=false;
    $response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-block-alt"></i></h1><h2>'.__LOCALAJAX_UPDATECONFIGMODULE_UPDATECONFIGFILEERRORTXT.'</h2></span><br />';
    $res=json_encode($response);
    die($res);
	}

  include($dirModAdmin.'mod_bbdd.class.php');
  include($dirModAdmin.'mod_common_lists.class.php');
  $data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
  $resConnexion=$data->connectDB();
  $data->logBackOffice(null,$arraExtVars['pathFile']);

  $response['success']=true;
  $response['msgPainTxt']="\n".@$arraExtVars['idRef'];
  $response['msgText']='<h3 class="c-white">'.__LOCALAJAX_UPDATECONFIGMODULE_UPDATESUCCESSTXT.'<br /><br /></h3><h2 class="c-white"><center>'.$arraExtVars['fileName'].'<center></h2><br />('.(number_format($res,0,'.','.').' bytes, <span class="c-yellow">'.$ajax->backUpFilename.'</span>)');
  $res=json_encode($response);
  die($res);

//echo$res."\n".$contentClean;
//echo "\n".$targetFileName."\n".'AJAX FILE ====>'."\n";
////print_r($arraExtVars);
//print_r($_SESSION);
//die();
}

/**
 * DELETE CONFIG MODULE BACK-UP FILE
 */
if($arraExtVars['action'] == 'deleteBackUpConfigModule')
{
	$response['success']=false; $resSql=false;

	if(!isset($arraExtVars['fileName']))
	{
		$response['msgPainTxt']="\n???";
		$response['errorText']='Falta la referencia!'."\n\n".__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
		$response['success']=false;
		$res=json_encode($response);
		die($res);
	}

  $res=@file_exists($dirModulesAdmin.$arraExtVars['folder'].$arraExtVars['fileName']);

  if($res)
  {
    (int)$resDelete=@unlink($dirModulesAdmin.$arraExtVars['folder'].$arraExtVars['fileName']);

    if(!$resDelete)
    {
      $response['msgPainTxt']="\n???";
      $response['errorText']='Error al borrar!'."\n\n".__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
      $response['success']=false;
      $res=json_encode($response);
      die($res);
    }

  }
  else
  {
		$response['msgPainTxt']="\n???";
		$response['errorText']='Archivo no accesible!'."\n\n".__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
		$response['success']=false;
		$res=json_encode($response);
		die($res);
  }

  include($dirModAdmin.'mod_bbdd.class.php');
  include($dirModAdmin.'mod_common_lists.class.php');
  $data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
  $resConnexion=$data->connectDB();
  $data->logBackOffice(-100,$arraExtVars['folder'],' file deleted: '.$arraExtVars['fileName']);
  $response['success']=true;
	$res=json_encode($response);
	die($res);
}

/**
 * RESTORE BACKUP CONFIG FILES
 */
if($arraExtVars['action'] == 'restoreBackUpConfigModule')
{
	$response['success']=false; $resSql=false;
  $currentModuleFolder=substr($dirModulesAdmin.$arraExtVars['folder'],0,-9);
  $currentFullConfigFile=$currentModuleFolder.'config_module.php';
  $folderDeletedFiles=$currentModuleFolder.'_deleted/';

	if(!isset($arraExtVars['fileName']))
	{
		$response['msgPainTxt']="\n???";
		$response['errorText']='Falta la referencia!'."\n\n".__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
		$response['success']=false;
		$res=json_encode($response);
		die($res);
	}

  $res=@file_exists($dirModulesAdmin.$arraExtVars['folder'].$arraExtVars['fileName']);

  if($res)  // SI EXISTE EL ARCHIVO A RESTAURAR
  {
    $resCurrentConfigFile=@file_exists($currentFullConfigFile);
    if(!(int)$resCurrentConfigFile) // NO EXISTE O NO SE ACCEDE AL ARCHIVO DE CONFIGURACION ACTUAL
    {
      $response['msgPainTxt']="\n???";
      $response['errorText']='Error al restaurar!'."\n\n".__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
      $response['success']=false;
      $res=json_encode($response);
      die($res);
    }

    $res=(is_dir($folderDeletedFiles) || mkdir($folderDeletedFiles));  // CHECK DELETED FILES FOLDER
    if(!@(int)$res)
    {
      $response['msgPainTxt']="\n???";
      $response['errorText']='Error de carpetas'."\n\n".__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
      $response['success']=false;
      $res=json_encode($response);
      die($res);
    }

    $res=@copy($currentModuleFolder.'config_module.php', $folderDeletedFiles.'deleted_'.date('Ymd').'_'.date('Hi').'-'.$_SESSION['userAdmin']['userId'].'_config_module.php');
    if(!@(int)$res)
    {
      $response['msgPainTxt']="\n???";
      $response['errorText']='Error de carpetas'."\n\n".__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
      $response['success']=false;
      $res=json_encode($response);
      die($res);
    }

    (int)$resDelete=@unlink($currentFullConfigFile);  // BORRADO DEL ACTUAL ARCHIVO DE CONFIGURACION

    if($resDelete)
      $resRestore=@copy($dirModulesAdmin.$arraExtVars['folder'].$arraExtVars['fileName'], $currentFullConfigFile); // SE RESTAURA EL BACK-UP

    if(!@(int)$resRestore)
    {
      $response['msgPainTxt']="\n???";
      $response['errorText']='Error al restaurar!'."\n\n".__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
      $response['success']=false;
      $res=json_encode($response);
      die($res);
    }
    $response['success']=true;
  }
  else  // ERROR, EL ARCHIVO A RESTAURAR NO EXISTE
  {
		$response['msgPainTxt']="\n???";
		$response['errorText']='Archivo no accesible!'."\n\n".__LOCALAJAX_COMMON_REFERENCEMISSINGDETAILTXT;
		$response['success']=false;
		$res=json_encode($response);
		die($res);
  }

  include($dirModAdmin.'mod_bbdd.class.php');
  include($dirModAdmin.'mod_common_lists.class.php');
  $data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
  $resConnexion=$data->connectDB();
  $data->logBackOffice(-50, $arraExtVars['folder'], $arraExtVars['fileName']);

	$res=json_encode($response);
	die($res);
}

/**
 * DISPLAY ON SCREEN SELECTED BACKUP FILE OLD VERSION
 */
if($arraExtVars['action'] == 'readConfigModuleFile')
{
	if(!isset($arraExtVars['fileName']) || !$arraExtVars['folder'])
	{
    $response['success']=false;
    $response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-block-alt"></i></h1>'.__AJAX_COMMON_SEEMSMISSINGTXT.'<br /><h1>'.__AJAX_COMMON_MODULEPARAMETERTXT.' <u>folder</u> o <u>file name</u></h1></span><br />';
    $res=json_encode($response);
    die($res);
	}

  include($dirModulesAdmin.'_modules/help_modules_contents.php');
  $res=false;

	$res=@file_exists($dirModulesAdmin.$arraExtVars['folder'].$arraExtVars['fileName']);

  if(!$res)
  {
    $response['success']=false;
    $response['errorText']='Parece que no existe el archivo: '.$arraExtVars['fileName'].'';
    $res=json_encode($response);
    die($res);
  }
//echo $res."\n".date('H:i:s').'] :: '.$dirModulesAdmin.$arraExtVars['folder'].''.$arraExtVars['fileName']."\n";print_r($arraExtVars);echo'</pre>';die(0);
	$res=@file_get_contents($dirModulesAdmin.$arraExtVars['folder'].$arraExtVars['fileName']);

  if(!$res)
  {
    $response['success']=false;
    $response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-block-alt"></i></h1> No se ha podido cargar <i>la ayuda</i> para <h1>'.$arraExtVars['moduleTitle'].'</h1></span><br />';
    $res=json_encode($response);
    die($res);
  }

//echo $res."\n".date('H:i:s').']:: '.$res.'config_module.php'."\n";print_r($arraExtVars);echo'</pre>';die(0);
  $res=str_replace('<?php','',$res);
  $res='Back-up: <b>'.$arraExtVars['fileName'].'</b><hr>'.$res;

	$response['success']=true;
	$response['theAction']=$arraExtVars['nextAction'];
  $buttonAction='';

  switch($arraExtVars['nextAction'])
  {
    case 'kill':
      $buttonAction='<button class="btn btn-sm bgm-red waves-effect waves-ripple doDelete" data-file="'.$arraExtVars['fileName'].'">Eliminar</button>&nbsp;&nbsp;';
      break;
    case 'restore':
      $buttonAction='<button class="btn btn-sm bgm-amber c-black waves-effect waves-ripple doRestore" data-file="'.$arraExtVars['fileName'].'">Restaurar</button>&nbsp;&nbsp;';
      break;
  }

    include($dirModAdmin.'mod_bbdd.class.php');
    include($dirModAdmin.'mod_common_lists.class.php');
    $data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
    $resConnexion=$data->connectDB();
    $data->logBackOffice(null,$arraExtVars['folder'],(isset($arraExtVars['nextAction']) ? ' '.$arraExtVars['nextAction'].': '.$arraExtVars['fileName'] : ' '.$arraExtVars['fileName']));

	$response['txtContents']='<pre class="pull-left text-left c-blue p-0 m-0 f-14 b-0 ">'.$res.'</pre><br /><br /><br /><div class="pull-left text-center w-100 p-10 m-t-20 m-b-20 bgm-white">'.$buttonAction.' <button class="btn btn-sm waves-effect waves-ripple" data-dismiss="modal">'.__COMMON_BTN_CLOSE.'</button></div>';
//	$response['txtContents']=''.$res.'';
	$res=json_encode($response);
	die($res);
}

/**
 *
 * IMPORTANTE:
 *
 * NO SE PUEDE FINALIZAR ESTE SCRIPT CON NADA
 *
 * SEGUN LA CARGA DEL MISMO, PUEDE SER QUE SEA UN INCLUDE
 *
 */
//echo 'END AJAX FILE !!!!!!!!!!!!!!!!!!! <pre>'."\n";print_r($arraExtVars);print_r($res);echo'</pre>';die(0);
