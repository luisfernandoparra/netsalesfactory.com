<?php
/**
 * EDIT MODULE admin
 *
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');

ini_set('display_errors', $displayErrors);
error_reporting($errorReportDefault);
$msgModuleError=null;
if(empty($_SESSION['id'])){echo '<script>parent.document.location="'.$web_url.$adminFolder.'/_modules/login/";</script>';}

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
if($hashSecutityScript != __HASH_CONTROL_SCRIPT) die(__MSG_ACCESS_ERROR);

/**
 *
 */
if($arraExtVars['action'] == 'edit' && isset($arraExtVars['id']))
{
	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModAdmin.'mod_common_lists.class.php');
	$dataModule=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name, null, $port);
	$resConnexion=$dataModule->connectDB();

	if(!$resConnexion)
	{
		$msgModuleError='<div class="text-center w-100  pull-left"><div class="alert alert-danger p-25 f-16">'.strtoupper(__COMMON_BBDD_TXTCONNECTERROR).'<br><br>'.__COMMON_BBDD_TXTNOCONNECTIONBBDD.'<br><br>'.__COMMON_GENERICTEXT_ERROCONFIGMODULEEND.'<br></div></div>';
		//die('<hr />');
	}

}

// HTML (HEADER/TOP) + ACCIONES COMUNES JS PARA LA EDICION DEL MODULO ACTUAL
include('../common_edit_html_top.php');
?>

<script type="text/javascript">

function initializeModule()
{
  $(".contentForm").show();
}

$(document).ready(function(){
	parent.$(".headerAdditionalInfo").html("");

	$(window).load(function(){
<?php
	if($arraExtVars['action'] == 'edit') // DIBUJA EL TITULAR DE LA VENTANA MODAL DE EDICION
		echo 'parent.$(".headerAdditionalInfo").removeClass("hide").html("- ID: <span class=\'text-uppercase\'>'.@$arraExtVars['id'].'</span>");';

	if($arraExtVars['action'] == 'new') // DIBUJA EL TITULAR PARA LA INSERCION DE UN NUEVO REGISTRO
		echo '';
?>
//	parent.$(".idTitleRef").append("<button class=\'btn bgm-gray btn-icon waves-effect waves-circle waves-float pull-right f-700 actCancel\' data-dismiss=\'modal\'><i class=\'zmdi zmdi-close\'></i></button>");
      setTimeout("initializeModule()",100);
//console.log("---> <?=$arraExtVars['action']?>")
//		parent.$(".headerAdditionalInfo").removeClass("hide").html("- ID: <span class='text-uppercase' style=color:#000;> <?=@$arraExtVars['id']?></span>").delay(10,function(){
//		});
	});

<?php
$iframeAction=ucfirst($arraExtVars['action']);
echo '	parent.$("#ifrm'.$iframeAction.'Record").css("height",parent.$("#boxContent'.$iframeAction.'Record").css("height"));parent.$("#boxContent'.$iframeAction.'Record").show();';
?>
});
</script>
</head>

<body>

<div class="contentForm" style="display:none;">
<?php
/**
 * ERROR EN LA CARGA DE LOS DATOS
 */
if($msgModuleError)
	die($msgModuleError);
	else
	{
echo $_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/';
echo '<hr>cargar datos del registro<hr><pre>';
print_r($dataModule);
print_r(@$arraExtVars);
	}



// TESTS...
for($n=0;$n<rand(1,10);$n++)
{echo '<pre>block-test:'.$n.'<p>';print_r($arraExtVars);echo '</pre>';}
?>
</div>

</body>
</html>