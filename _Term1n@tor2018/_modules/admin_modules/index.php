﻿<?php
/**
 * MAIN MODULE - users
 * DEFAULT ACTION: list record data
 *
 *
 * mysql >= 5.5
 CREATE TABLE test2 (a INT NOT NULL AUTO_INCREMENT, PRIMARY KEY (a)) ENGINE=MyISAM SELECT p.id,p.email,cs.sorteo FROM premios AS p INNER JOIN concursos_sorteos AS cs ON p.id_sorteo=cs.id_sorteo WHERE p.b_activo=1 AND p.email LIKE '%gmail.com' ORDER BY p.email;
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
if($hashSecutityScript != __HASH_CONTROL_SCRIPT) die(__MSG_ACCESS_ERROR);

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$modeLoadConfig=1;
$folderCommonModules=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/';
$folderModule=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'];
include('config_module.php');

if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD )	// GOD ACCESS LEVEL
	include($folderCommonModules.'common_restore_modules.php');

?>

<script type="text/javascript">
function openModalEditRecord(recordId, dataRow)	// SE CARGA EL SCRIPT CON EL ALTO MAXIMO DE LA CAJA "MODAL" A DIBUJAR PARA LA DIMENSION ACTUAL DE LA PANTALLA
{
	var additionalParams="";

	if(dataRow != undefined)
		$.each(dataRow[0].dataset,function(dataName, dataValue){
			additionalParams+="&"+dataName+"="+encodeURIComponent(dataValue);
		});

	var tmpHeight=$("body").prop("clientHeight");
	$("#modalWiderRefEdit").click();
  $("#ifrmEditBlock").attr("src","<?=$arraExtVars['moduleAdmin']?>ifr_edit.php?action=edit&ref_mod=<?=$hashSecutityScript?>&from=<?=$arraExtVars['moduleAdmin']?>&id="+recordId+"&module[name]=<?=$arraExtVars['modLabel']?>&module[modIcon]=<?=$moduleSetting->moduleIcon?>&editHeight="+tmpHeight+$.trim(additionalParams)+"").css("height",tmpHeight+"px");
	return true;
}

$(document).ready(function(){

	var tmpHeight=$("#mCSB_1").height()-140;

  $(window).load(function(){	// AL FINALIZAR COMPLETAMENTE DE DIBUJAR EL MODULO, CARGA EL IFRAME DEL LISTADO AL FINALIZAR DE CARGAR COMPLETAMENTE LA PAGINA
		var configDataList={};	// OBJETO CON LOS DATOS DE CONFIGURACION PARA LA BBDD DEL MODULO ACTUIAL
<?php
/**
 * SERIALIZACION DE LOS PARAMETROS ESPECIFICOS
 * DEL MODULO ACTUAL NECESARIOS PARA EL IFRAME
 * DEL LISTADO DEL MISMO MODULO
 */
$outJsParamsModule='';

foreach($moduleSetting as $field => $value)
{
	$valueJs='';

	if(count($value) > 1)
	{
		$valueJs='[';
		foreach($value as $arrValues)
		{
			$valueJs.=''.(str_replace(' ','_',$arrValues)).'="'.$arrValues.'",';
		}
		$valueJs=substr($valueJs,0,-1);
		$valueJs.=']';
 	}
	else
		$valueJs='"'.$value.'"';

	$outJsParamsModule.=''.$field.':'.$valueJs.',';
}

$outJsParamsModule=substr($outJsParamsModule,0,-1);
?>
		var configDataList=serializeDataArray({<?=$outJsParamsModule?>}, "configDataList");
		$("#ifrListBlock").attr("src","ifr_common_listing.php?action=list&modulePath=<?=$arraExtVars['moduleAdmin']?>&"+configDataList).removeClass("hidden");
  });

	// NEW RECORD MODAL
	$(".modalNewRecord").click(function(){
		$("#modalWiderRefNewRecord").click();
		$(".contentForm").html("......");
		window.parent.$(".idTitleRef").html("<span style=font-style:italic;><div class='preloader pls-pink pl-xs'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div> <?=__COMMON_JSTITLE_TXTNEWRECORDLOADING?>...<span>");
		$("#ifrmNewBlock").attr("src","<?=@$arraExtVars['moduleAdmin']?>ifr_edit.php?action=new&ref_mod=<?=@$hashSecutityScript?>&from=<?=@$arraExtVars['moduleAdmin']?>&module[name]=<?=@$arraExtVars['modLabel']?>&module[moment]=miValor2&editHeight="+(tmpHeight)+"").css("height",(tmpHeight)+"px");
		return true;
  });

});

</script>

	<div class="card-header">
			<h2>Contenido del módulo <?=$arraExtVars['modLabel']?>
					<small>Ensure that the data attribute [data-identifier="true"] is set on one column header.</small>
			</h2>
	</div>


<!-- START DATA LISTING -->
	<iframe style='height:30vh;' class="hidden" id='ifrListBlock' name='ifrListBlock' src='_blank.html' frameborder='0' marginheight='0' marginwidth='0' scrolling='no' width='100%'></iframe>
<!-- END DATA LISTING -->

<!-- START HTML LOCAL MODULE/TAGS/ELEMENTS--->
<a id="modalWiderRefEdit" data-toggle="modal" href="#editRowModal" class="hide" title="Editar el registro" >editRow</a>
<a id="modalWiderRefDelete" data-toggle="modal" href="#modaldeleteRow" class="hide" title="Eliminar el registro" >deleteRow</a>
<a id="modalWiderRefNewRecord" data-toggle="modal" href="#modalNewRecord" class="hide" title="" >newRec</a>

<div class="modal fade" id="modalNewRecord" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-record-insert">
				<div class="modal-content">

					<div class="modal-header commonHeaderInsert">
            <div class="fltBtnClose">
              <button class='btn bgm-gray btn-icon waves-effect waves-circle actCancel' data-dismiss='modal'><i class='zmdi zmdi-close'></i></button>
            </div>
						<h4 class="modal-title" style='color:#333;width:96%!important;'><span class="idTitleRef"></span><span class="headerAdditionalInfo hide"></span></h4>
					</div>

					<div class="modal-body" id="boxContentNewRecord" style="overflow:hidden">
            <iframe style='width:100%!important;' id='ifrmNewBlock' name='ifrmNewBlock' src='#null' frameborder='0' marginheight='0' marginwidth='0' scrolling='auto' width='100%' height='100%'></iframe>
          </div>

					<div class="modal-footer">
							<button type="button" class="btn btn-default">Insertar</button>
							<button type="button" class="btn btn-default actCancel" data-dismiss="modal">Cancelar</button>
					</div>

				</div>
		</div>
</div>

<div class="modal fade" id="editRowModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-record-edit">
				<div class="modal-content">
					<div class="modal-header commonHeaderEdit">
						<h4 class="modal-title" style='color:#333;width:96%!important;'><span class="idTitleRef"></span><span class="headerAdditionalInfo hide"></span></h4>
            <div class="fltBtnClose">
              <button class='btn bgm-gray btn-icon waves-effect waves-circle actCancel' data-dismiss='modal'><i class='zmdi zmdi-close'></i></button>
            </div>
					</div>

					<div class="modal-body" id="boxContentEditRecord" style="overflow:hidden">
            <iframe style='width:100%!important;' id='ifrmEditBlock' name='ifrmEditBlock' src='#null' frameborder='0' marginheight='0' marginwidth='0' scrolling='auto' width='100%' height='100%'></iframe>
          </div>

					<div class="modal-footer">
							<button type="button" class="btn btn-default">Guardar</button>
							<button type="button" class="btn btn-default actCancel" data-dismiss="modal">Cancelar</button>
					</div>
				</div>
		</div>
</div>

<div class="modal fade" data-modal-color="red" id="modaldeleteRow" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
					<h4 class="modal-title">Eliminar el registro</h4>
			</div>
			<div class="modal-body">
					<p>Desea realmente eliminar el registro id: <span class="idTitleRef"></span></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect">Eliminar</button>
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>

<?php
// COMMON HTML CONTENTS
if($_SESSION['userAdmin']['roleId'] > 2 )	// MINIMUN ACCESS LEVEL
	@include('_modules//common_html_contents_all.php');

if($_SESSION['userAdmin']['roleId'] > 7)	// INTERMEDIATE ACCESS LEVEL
	@include('_modules//common_html_contents_role_8.php');

if($_SESSION['userAdmin']['roleId'] == 10 )	// GOD ACCESS LEVEL
	@include('_modules//common_html_contents_role_10.php');
?>
<!-- END HTML LOCAL MODULE/TAGS/ELEMENTS--->
