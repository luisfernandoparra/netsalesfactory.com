<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">
		<ul class="tab-nav text-center fw-nav">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Notas preliminares</a></li>
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>Algunas notas a considerar en la versión beta <span class="c-gray">(hasta que no esté acabado...)</span>.</p>
					<ul>
						<li class="m-b-10">Se debe configurar correctamente el campo que servirá de indice para la edicion de la tabla seleccionada</li>
						<li class="m-b-10">Hacer un instalador desde este modulo????</li>
						<li class="m-b-10">Habilitar la posibilidad de borrar tablas que incluyan "_DEPRECATED" en su nombre</li>
						<li class="m-b-10"></li>
					</ul>
				</div>


<!--
				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>
-->
			</div>
	</div>
</div>
