<?php
/**
 * DATE UPDATE: 2016-11-04 07:46 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * LANG ELEMENTS
 * 
 * IMPORTANTE, EL PARAMETRO orderTabularList PRESENTE EN LOS CAMPOS
 * INDICA LA POSICION Y DONDE DEBE APARECER EN EL LISTADO TABULAR DEL MODULO
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$moduleSetting=new stdClass();

// MANDATORY PARAMS
$moduleSetting->moduleBBDD=null;
$moduleSetting->mainTable='customers_site_config';
//$moduleSetting->foreignKeys='id,id_lang';	// IMPORTANTE, SIN ESPACIOS
$moduleSetting->keyListLabel='id';
$moduleSetting->orderField='prefix';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->fieldNameStatusRow='is_enabled';
$moduleSetting->isCanEnabled=true;
$moduleSetting->allowNewRecord=true;


// OPTIONAL PARAMS
$moduleSetting->moduleCommenHeader='<small>Modulo adicional con parametros de clientes sin utilizar actualmente (2018)</small>';
$moduleSetting->numRecDisplay='[10,30,50]'; // EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;

/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array(
		' && !'.$moduleSetting->fieldNameStatusRow=>''
//		,' && !(SELECT COUNT(*) FROM `'.__QUERY_TABLES_PREFIX.'landings_site_config` WHERE client_id = '=>'[[idRef]])'	// EVITAR BORRAR REGISTROS EN TABLAS RELACIONALAS
	)
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode( $arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS

$order=0;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id'=>array(
    'formType'=>'hidden'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>0
    ,'label'=>''
    ,'helpText'=>''
    ,'visible'=>0
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
	,'client_id'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>2]
		,'tabularFilter'=>['mode'=>'BBDD','origin'=>__QUERY_TABLES_PREFIX.'landings_customers','tag'=>array('filterType'=>'select')]
		,'required'=>1
		,'fieldTableName'=>__QUERY_TABLES_PREFIX.'landings_customers'
		,'arrFieldsTagSelect'=>array('id_landing','prefix') // PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
		,'includeMainBBDD'=>true
		,'whereSelectField'=>null
		,'orderSelectField'=>'prefix'
    ,'label'=>' Cliente'
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
	)
  ,'landing_identifier'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>3,'columnWidth'=>2]
    ,'required'=>1
    ,'label'=>'Identificador de la landing'
    ,'helpText'=>''
		,'helpTextExtend'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Identificador'
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'root_local_path'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
  	,'tabularListData'=>['columnOrder'=>4,'columnWidth'=>2]
    ,'required'=>1
    ,'label'=>'root local path'
    ,'helpText'=>''
		,'helpTextExtend'=>'No finalizar la ruta con `/`'
    ,'visible'=>1
    ,'placeHolder'=>'root local path'
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'txt_title_site'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
  	,'tabularListData'=>['columnOrder'=>5,'columnWidth'=>2]
		,'required'=>1
    ,'label'=>'Título del navegador'
    ,'helpText'=>'Título navegador'
		,'helpTextExtend'=>'texto que identifica la página abierta en el navegador'
    ,'visible'=>1
    ,'placeHolder'=>'nombre archivo CSS'
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'root_local_includes_path'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'root local path includes'
    ,'helpText'=>''
		,'helpTextExtend'=>'No finalizar la ruta con `/`'
    ,'visible'=>1
    ,'placeHolder'=>'root local path includes'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'root_path'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'root path'
    ,'helpText'=>''
		,'helpTextExtend'=>''
    ,'visible'=>1
    ,'placeHolder'=>'root_path'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'url_admin'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'url admin'
    ,'helpText'=>''
		,'helpTextExtend'=>''
    ,'visible'=>1
    ,'placeHolder'=>'url admin'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'url_root_app'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'url root app'
    ,'helpText'=>''
		,'helpTextExtend'=>''
    ,'visible'=>1
    ,'placeHolder'=>'url_root_app'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'url_local'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'url local'
    ,'helpText'=>''
		,'helpTextExtend'=>''
    ,'visible'=>1
    ,'placeHolder'=>'url local'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'url_base'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'url base'
    ,'helpText'=>''
		,'helpTextExtend'=>''
    ,'visible'=>1
    ,'placeHolder'=>'url base'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'is_enabled'=>array(
		'formType'=>'checkbox'
		,'dataType'=>'toggle'
		,'tabularListData'=>['columnOrder'=>5]	// PARA QUE NO VISUALICE EN EL LISTADO, PONER EL MISMO NUMERO DEL ULTIMO UTILIZADO
		,'required'=>0
		,'toggleParams'=>array('tsColor'=>'green')
		,'label'=>'Estado'
		,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')	// PARA ESTE CONTROL, SIEMPRE EL PRIMER VALOR ES EL NEGATIVO Y EL SEGUNDO EL POSITIVO
		,'helpText'=>'Habilitar / deshabilitar'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>2
		,'excludeAutoForm'=>false
		,'order'=>++$order
  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  0=>array(
    'groupTextTitle'=>'Definición parámetros de clientes de ls landings'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
  ,1=>array(
    'groupTextTitle'=>'Otras configuraciones utilizadas en algunas viejas landings'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);
