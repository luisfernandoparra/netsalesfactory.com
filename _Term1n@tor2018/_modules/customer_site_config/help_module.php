<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">

		<ul class="tab-nav text-center fw-nav">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Intro</a></li>
			<!--<li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">otro TAB</a></li>-->
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>Breve introducción para el módulo actual.</p>
					<ol>
						<li class="m-b-10">Confifuraciones específicas de los clientes disponibles para las landings </li>
						<li class="m-b-10">El campo <strong>"Cliente"</strong>, clientes existentes en la BBDD.</li>
						<li class="m-b-10">El campo <strong>"Identificador de la landing"</strong>, sirve para asignar un identificador interno de la landing, este valor no aparece al público, deben ser utilizadas letras anglosajonas sin símbolos ni espacios.</li>
						<li class="m-b-10">El campo <strong>"root local path"</strong>, define el nombre de la carpeta para las landings del cliente editado, estas carpetas son las que cuelgan del servidor en <i>"promociones/"</i>.</li>
						<li class="m-b-10">El campo <strong>"Título del navegador"</strong>, el el título que aparece en el navegador/browser.</li>
						<li class="m-b-10">El campo <strong>"Estado"</strong>, habilita/deshabilita el registro.</li>
					</ol>
					El resto de campos no está siendo más utilizado en las landings más recientes.
				</div>
				<div class="tab-pane fade f-16" id="tab2">
						<p>El archivo "config_module.php" contiene la gran parte de parámetros necesarios para el funcionamiento de los módulos del back-office.</p>
						<p>Debemos recordar que este archivo contiene también las definiciones para los campos a utilizar en el listado principal y en la edición del mismo, esto para todos los módulos con estructuras simples.</p>
				</div>
			</div>

	</div>
</div>
