<?php
/**
 * DATE UPDATE: 2018-02-07 07:16 (Mario Francescutto)
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * LANDING-CONTENTS
 * 
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=$_SESSION['userAdmin']['roleId'];  // AJUSTA AUTOMATICAMENTE EL MAXIMO ROL PARA EL USUARIO ACCEDIDO
$minRoleAdim=1;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>Contenidos HTML de las landings</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='lopd_front_contents';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='lopd_id';
$moduleSetting->keyListLabel='lopd_id';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->isCanEnabled=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole ? true : false;
$moduleSetting->fieldNameStatusRow='';
//$moduleSetting->denyOtherUsers=[$_SESSION['userAdmin']['roleId'] => 'roleId'];
$moduleSetting->allowNewRecord=true;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->minEnableLevel=999;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[10,25,50]'; // array, EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;
$moduleSetting->defaultOrderField=['lopd_id'=>'DESC'];
$moduleSetting->allowCopyRecord=true;	// NEW 2018
$moduleSetting->minCopyRecordLevel=__LEVEL_ACCESS_MASTER;	// NEW 2018
$moduleSetting->arrCopyExcludeFields=array('lopd_id_landing');

/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array(
//		' && '.$moduleSetting->fieldNameStatusRow.' = '=>0
//		,'&& roleId <= '=>$_SESSION['userAdmin']['roleId']
// EXAMPLE WITH MORE SENTENCES:
//		,'|| ('=>''
//		,'theme = '=>'\'123\''
//		,')'=>''
	)
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode( $arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS

$arrScriptFormName=array(
  0=>'Standard',
  1=>'Espec&iacute;fico',
);

//echo $moduleSetting->moduleIcon.'<pre>';print_r($arrEraseOptions);echo'</pre>';
$order=0;

//echo $web_url.$adminFolder;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'lopd_id'=>array(
    'formType'=>'text'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>1
    ,'label'=>'ref.'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>true
    ,'order'=>++$order
  )
	,'lopd_id_landing'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>4]
		,'tabularFilter'=>['mode'=>'BBDD','origin'=>__QUERY_TABLES_PREFIX.'landings_site_config','tag'=>array('filterType'=>'select')]
		,'required'=>1
		,'fieldTableName'=>__QUERY_TABLES_PREFIX.'landings_site_config'
		,'arrFieldsTagSelect'=>array('id','CONCAT("<span style=color:red;>",id,"</span> - ",landing_name)') // PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
		,'includeMainBBDD'=>true
		, 'whereSelectField'=> (isset($_REQUEST['id'])) ? null : ' id NOT IN (SELECT lopd_id_landing FROM '.$moduleSetting->prefixTbl.$moduleSetting->mainTable.')'
		,'orderSelectField'=>'id'
    ,'label'=>' ID - Nombre landing'
    ,'helpText'=>'Id + Referecia de la landing'
    ,'visible'=>1
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
	)
  ,'lopd_table_title'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>3,'columnWidth'=>4]
    ,'required'=>0
    ,'label'=>'Titular Box flotante'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_more_info_alt'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Texto botón ALT'
    ,'placeHolder'=>'Texto para el icono que abre la ventana emergente'
    ,'colsClass'=>3
    ,'helpText'=>'Texto que aparece en el icono'
    ,'helpTextExtend'=>'que abre la ventana emergente para mostrar todo el contenido'
    ,'visible'=>1
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_statement_responsible'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Responsable'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>'Titular para `el responsable`'
    ,'visible'=>1
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_statement_purpose'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Finalidad'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>'Titular para `Finalidad`'
    ,'visible'=>1
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_statement_legitimation'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Legitimación'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>'Titular para `Legitimación`'
    ,'visible'=>1
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_statement_recipients'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Destinatarios'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>'Titular para `Destinatarios`'
    ,'visible'=>1
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_statement_rights'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Derechos'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>'Titular para `Derechos`'
    ,'visible'=>1
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_statement_origin'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Información adic.'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>'Titular para `Información adicional`'
    ,'visible'=>1
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_summary_responsible'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Resumen de `Responsable`'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>'Resumen para el contenido de `Responsable`'
    ,'visible'=>1
    ,'formGroup'=>3
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_summary_purpose'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Resumen de `Finalidad`'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>'Resumen para el contenido de `Finalidad`'
    ,'visible'=>1
    ,'formGroup'=>3
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_summary_legitimation'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Resumen de `Legitimación`'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>'Resumen para el contenido de `Legitimación`'
    ,'visible'=>1
    ,'formGroup'=>3
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_summary_recipients'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Resumen de `Destinatarios`'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>'Resumen para el contenido de `Destinatarios`'
    ,'visible'=>1
    ,'formGroup'=>3
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_summary_rights'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Resumen de `Derechos`'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>'Resumen para el contenido de `Derechos`'
    ,'visible'=>1
    ,'formGroup'=>3
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_summary_origin'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Resumen de `Información adic.`'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>'Resumen para el contenido de `Información adicicional`'
    ,'visible'=>1
    ,'formGroup'=>3
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_title_responsible'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Responsable'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>4
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_title_purpose'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Finalidad'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>4
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_title_legitimation'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Legitimación'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>4
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_title_recipients'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Destinatarios'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>4
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_title_rights'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Derechos'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>4
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_title_origin'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Información adic.'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>3
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>4
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_content_responsible'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Responsable'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>5
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_content_purpose'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Finalidad'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>5
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_content_legitimation'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Legitimación'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>5
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_content_recipients'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Destinatarios'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>5
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_content_rights'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Derechos'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>5
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'lopd_content_origin'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Información adic.'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>4
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>5
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  1=>array(
    'groupTextTitle'=>'Asignación contenidos LOPD para las landings'
    ,'additionalInfo'=>'Textos comunes Box flotante LOPD LOPD (box flotante)'
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
	,2=>array(
    'groupTextTitle'=>'Titulares de los enunciados LOPD (box flotante)'
    ,'additionalInfo'=>'Titular enunciado LOPD del box flotante'
    ,'groupIco'=>'zmdi zmdi-view-subtitles'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
	,3=>array(
    'groupTextTitle'=>'Resumenes de los contenidos LOPD LOPD'
    ,'additionalInfo'=>'Resumenes de los enunciados del box flotante LOPD (box flotante)'
    ,'groupIco'=>'zmdi zmdi-comment-text'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
	,4=>array(
    'groupTextTitle'=>'Titulares ventanas modales LOPD'
    ,'additionalInfo'=>'Titulares HTML para las ventanas modales de los contenidos LOPD'
    ,'groupIco'=>'fa fa-window-maximize'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
	,5=>array(
    'groupTextTitle'=>'Contenidos ventanas modales LOPD'
    ,'additionalInfo'=>'Contenidos HTML para las ventanas modales LOPD'
    ,'groupIco'=>'zmdi zmdi-receipt'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);
