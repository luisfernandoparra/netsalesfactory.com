<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">

		<ul class="tab-nav text-center fw-nav">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Principales funciones</a></li>
			<li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">Notas</a></li>
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>Desglose de las principales funciones del módulo actual.</p>
					<p>En este módulo están presentes para las páginas públicas todas las informaciones que se visualizan en el box flotante para los textos legales de la LOPD de cada landing y que abren las ventanas modales relacionadas.</p>
					<ol>
						<li class="m-b-10"><b>ID - Nombre landing </b> para referenciar los contenidos que se definen en este módulo. Recordar que el ID del record de esta tabla (aquí identificado como <i>ref.</i>) no es el ID de la landing, por ese motivo se ha incluido junto al <i>nombre</i> el valor del <i>ID</i> que si referencia específicamente la landing editada.</li>
						<li class="m-b-10"><b>Titular Box flotante</b> es el texto del encabezado del box flotante de los textos legales de la LOPD.</li>
						<li class="m-b-10"><b>Texto botón ALT</b> sirve para introducir el comentario que aparece cuando el internauta se coloca encima de las imágenes que permiten abrir las ventanas modales con las informaciones completas de cada punto de la LOPD.</li>

						<li class="m-b-10"><i>Bloques de contenidos</i>: se agrupan todos los contenidos restantes en 4 bloques, cada elemento incluye un mini editor y un pequeño visor HTML, todos ellos tienen la misma subdivisión de elementos:
							<ul type="A">
								<li><b>Responsable</b></li>
								<li><b>Finalidad</b></li>
								<li><b>Legitimación</b></li>
								<li><b>Destinatarios</b></li>
								<li><b>Derechos</b></li>
								<li><b>Información adicional</b> (Información adic.)</li>
							</ul>
						</li>
						<li class="m-b-10">Bloque: <b>Titulares de los enunciados LOPD (box flotante)</b> agrupa todos los titulares del box flotante situados inicialmente a la izquierda del mismo.</li>
						<li class="m-b-10">Bloque: <b>Resumenes de los contenidos LOPD LOPD</b> Permite introducir los textos que aparecen como forma resumida de cada apartado de la LOPD en el box modal flotante.</li>
						<li class="m-b-10">Bloque: <b>Titulares ventanas modales LOPD</b>, aquí se introducen los textos para los titulares de las ventanas modales de los textos completos de la LOPD.</li>
						<li class="m-b-10">Bloque: <b>Contenidos ventanas modales LOPD</b> y este último grupo es el que contiene todos los textos completos de las ventanas modales para la LOPD.</li>
					</ol>
				</div>
				<div class="tab-pane fade f-16" id="tab2">
						<p>En cada grupo de los <i>Bloques de contenidos</i> se incluyen para cada campo tres botones adicionales:
						<ul>
							<li class="m-b-10"><button class="btn btn-xs bgm-gray waves-effect">HTML</button>: abre una ventana modal que facilita el contenido en HTML puro.</li>
							<li class="m-b-10"><button class="btn btn-xs bgm-cyan waves-effect">Visualiza Wysiwig</button>: permite visualizar el contenido introducido de manera similar al real.</li>
							<li class="m-b-10"><button class="btn btn-xs bgm-cyan waves-effect">Editor Wysiwig</button>: es un pequeño editor HTML que facilita la edición de los contenidos a visualizar.</li>
						</ul>
						</p>
						<p>Notar que los textos de los <i>tres botones</i> pueden diferir de los expuestos en esta sección de ayuda ya que estos son <i>sensibles al idioma utilizado</i> por el operador de este mismo back-office.</p>
				</div>

				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>

			</div>

	</div>
</div>
