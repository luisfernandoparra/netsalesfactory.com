<?php
/**
 * DATE UPDATE: 2017-09-06 17:19 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * ADMIN USERS
 * 
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=__LEVEL_ACCESS_GOD;
$minRoleAdim=__LEVEL_ACCESS_VISIT;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>comentarios espec&iacute;ficos del m&oacute;dulo actual....</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='log_backoffice_'.date('Y');
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='id';
$moduleSetting->keyListLabel='id';
$moduleSetting->offsetStart=0;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD ? true : false;
$moduleSetting->fieldNameStatusRow=null;
$moduleSetting->allowNewRecord=false;
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[10,25,50,200,500]';
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->isErasable=false;
$moduleSetting->isCanEnabled=false;
$moduleSetting->defaultOrderField=['log_date'=>'DESC','log_time'=>'DESC'];
$moduleSetting->minEnableLevel=__LEVEL_ACCESS_GOD;

$order=0;
//echo $web_url.$adminFolder;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id'=>array(
    'formType'=>'hidden'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>0
    ,'label'=>'ID'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'id_user'=>array(
		'formType'=>'select'
		,'dataType'=>'integer'
		,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>1]
		,'required'=>0
		,'tabularFilter'=>['mode'=>'BBDD','origin'=>'self','tag'=>array('filterType'=>'select')]
		,'label'=>' Usuario'
		,'isDisabled'=>true
		,'fieldTableName'=>__QUERY_TABLES_PREFIX.'adminusers'  // NOMBRE DE LA TABLA ORIGEN A UTILIZAR PARA LA EXTRACCION DE DATOS PARA OBTENER LOS VALORES DEL CAMPO "SELECT"
		,'arrFieldsTagSelect'=>array('userId','user') // PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
		,'whereSelectField'=>null
		,'orderSelectField'=>'user'
		,'helpText'=>'Usuario accedido'
		,'helpTextExtend'=>'identificador del usuario'
		,'visible'=>1
		,'placeHolder'=>''
		,'formGroup'=>1
		,'excludeAutoForm'=>false
		,'order'=>++$order
  )
  ,'log_date'=>array(
    'formType'=>'date'
    ,'dataType'=>'date'
    ,'tabularListData'=>['columnOrder'=>3,'columnWidth'=>1]
    ,'required'=>0
		,'colsClass'=>1
    ,'tabularFilter'=>['mode'=>'STATIC','origin'=>'','tag'=>array('filterType'=>'date')]
    ,'label'=>' Fecha'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Fecha del evento'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'log_time'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>4,'columnWidth'=>1]
    ,'required'=>0
		,'colsClass'=>1
    ,'label'=>'Hora'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Momento del evento'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'action'=>array(
    'formType'=>'email'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>5,'columnWidth'=>3]
    ,'required'=>0
    ,'label'=>'Acci&oacute;n'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Acci&oacute;n / detalles'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'ip_user'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
		,'colsClass'=>1
    ,'label'=>'IP'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'IP'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'script'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>6,'columnWidth'=>3]
    ,'required'=>0
    ,'label'=>'Script/M&oacute;dulo'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Script/M&oacute;dulo del evento'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'id_element'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>7,'columnWidth'=>1]
    ,'required'=>0
		,'colsClass'=>2
    ,'label'=>'Ref. evento'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Identificador del evento (cuando disponible)'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'url'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Url'
    ,'rowsData'=>2
		,'actionsForm'=>(object)['isHtmlBtn'=>true]
    ,'colsClass'=>10
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'URL del evento'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  1=>array(
    'groupTextTitle'=>'Gesti&oacute;n de usuario para el back-office'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
//	,2=>array(
//		'groupTextTitle'=>'Otros detalles'
//		,'additionalInfo'=>''
//		,'groupIco'=>'zmdi zmdi-format-color-fill'
//		,'groupHeaderClass'=>'bg-black-trp'
//	)
);

