<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">
		<ul class="tab-nav text-center fw-nav" data-tab-color="green">
			<li class=""><a href="#tab-mod-1" data-toggle="tab" aria-expanded="false">nota sobre este mismo módulo</a></li>
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade f-16" id="tab-mod-1">
          <br />Todas las principales acciones a la BBDD quedan registradas en la BBDD.
          <br /><br />También se guardan algunas acciones específicas de AJAX, sin acceso necesario a alguna tabla de la BBDD.
          <br /><br />Las únicas acciones "de módulo" por acceso a BBDD que no se almacenan, son las del listado normal de este mismo módulo "backoffice_logs".
          <br /><br />En la edici&oacute;n de los registros, se ha posibilitado la opción de abrir una modal para poder abarcar cualquier contenido para el campo <b>URL</b>, al poder ser &eacute;ste muy largo y no caber por entero en el espacio asignado a su caja contenedora.
				</div>

				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>

			</div>
	</div>
</div>
