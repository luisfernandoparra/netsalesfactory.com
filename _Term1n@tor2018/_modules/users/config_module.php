<?php
/**
 * DATE UPDATE: 2016-11-02 16:36 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * ADMIN USERS
 * 
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=$_SESSION['userAdmin']['roleId'];  // AJUSTA AUTOMATICAMENTE EL MAXIMO ROL PARA EL USUARIO ACCEDIDO
$minRoleAdim=1;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>comentarios espec&iacute;ficos del m&oacute;dulo actual....</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='adminusers';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='user';
$moduleSetting->keyListLabel='userId';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->isCanEnabled=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole ? true : false;
$moduleSetting->fieldNameStatusRow='b_enabled';
$moduleSetting->denyOtherUsers=[$_SESSION['userAdmin']['roleId'] => 'roleId'];
$moduleSetting->allowNewRecord=true;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->minEnableLevel=__LEVEL_ACCESS_GOD;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[10,25,50]'; // array, EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;

/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array(
		' && !'.$moduleSetting->fieldNameStatusRow => ''
		,' && !(SELECT COUNT(*) FROM `'.__QUERY_TABLES_PREFIX.'adminusers_preferences` WHERE user_id = '=>'[[idRef]])'	// EVITAR BORRAR REGISTROS EN TABLAS RELACIONALAS
		,' && roleId <= '=>$_SESSION['userAdmin']['roleId']
	)
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode( $arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS



//echo $moduleSetting->moduleIcon.'<pre>';print_r($arrEraseOptions);echo'</pre>';
$order=0;

//echo $web_url.$adminFolder;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'userId'=>array(
    'formType'=>'text'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>1
    ,'label'=>'Usuario'
    ,'helpText'=>''
    ,'visible'=>0
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'user'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>1]
    ,'required'=>1
    ,'label'=>'Usuario'
    ,'helpText'=>'Nombre del usuario de acceso al back-office de GTP 6'
    ,'visible'=>1
    ,'placeHolder'=>'El nombre de usuario para acceder al back-office'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'password'=>array(
    'formType'=>'password'
    ,'dataType'=>'varchar'
    ,'required'=>1
    ,'label'=>'Contrase&ntilde;a'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'La contrase&ntilde;a de acceso'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'name'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>3,'columnWidth'=>2]
    ,'required'=>1
    ,'label'=>'Nombre'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'surname'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Apellidos'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'email'=>array(
    'formType'=>'email'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>4,'columnWidth'=>3]
    ,'required'=>1
    ,'label'=>'E-mail'
    ,'helpText'=>''
	,'helpTextExtend'=>'Debes introducir una dirección de correo electrónico'
//    ,'tagPattern'=>'[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$'
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'roleId'=>array(
    'formType'=>'text'
    ,'dataType'=>'slider'
    ,'tabularListData'=>['columnOrder'=>5,'columnWidth'=>1]
    ,'sliderConf'=>array('startValue'=>0,'endValue'=>$maxRoleAdim,'stepValue'=>1)
    ,'required'=>0
    ,'label'=>'Nivel  del role'
    ,'helpText'=>'Niveles de permisos para este gestor, m&aacute;s bajo = '.$minRoleAdim.', m&aacute;s alto = '.$maxRoleAdim
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'profile_image'=>array(
    'formType'=>'file'
    ,'dataType'=>'imageFile'  // allFile / imageFile
    ,'required'=>false
    ,'label'=>'Im&aacute;gen del usuario'
    ,'helpText'=>'Im&aacute;gen del usuario'
    ,'visible'=>true
    ,'placeHolder'=>'Foto / im&aacute;gen del usuario'
    ,'htmlFolder'=>$web_url.$adminFolder.'/profile_images/'
    ,'targetFolder'=>$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/profile_images/'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'b_enabled'=>array(
    'formType'=>'checkbox'
    ,'dataType'=>'toggle'
    ,'tabularListData'=>['columnOrder'=>5]
    ,'required'=>0
    ,'toggleParams'=>array('tsColor'=>'green')
    ,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')	// PARA ESTE CONTROL, SIEMPRE EL PRIMER VALOR ES EL NEGATIVO Y EL SEGUNDO EL POSITIVO
    ,'label'=>'Estado del usuario'
    ,'helpText'=>'Habilitar / deshabilitar este registro'
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  1=>array(
    'groupTextTitle'=>'Gesti&oacute;n de usuario para el back-office'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
  ,2=>array(
    'groupTextTitle'=>'Otras opciones del usuario'
    ,'additionalInfo'=>'Par&aacute;metros funcionales'
    ,'groupIco'=>'zmdi zmdi-view-compact'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);
