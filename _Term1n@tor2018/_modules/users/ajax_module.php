<?php
/**
 * LOCAL AJAX MODULE
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');

ini_set('display_errors',1);
error_reporting($errorReportDefault);
$moduleParams='';

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
$response['query']=array();

if($hashSecutityScript != __HASH_CONTROL_SCRIPT){
  die(__MSG_ACCESS_ERROR);
}


if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$tmpVars=json_decode(file_get_contents('php://input'), true);	// DECODE JSON DATA

if(count($tmpVars))
  $arraExtVars=$tmpVars;

// CARGAR TABLA LOGS APACHE
function password_http()
{
	shell_exec('tail /etc/.htpasswd');
	// Ver usuarios: password
	shell_exec('htpasswd -bsc /etc/.htpasswd usuario contrasenha');
	echo "Carga de LOG de Apache realizada.";
}

/*
if(isset($arraExtVars['moduleAdmin']) && $arraExtVars['moduleAdmin'])
{
  $localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'_mod_bbdd/';
  $localModuleFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'';
	$provenience=$_SERVER['HTTP_REFERER'];
	$modNameFrom=strpos($provenience,'_modules/')+9;
	$modName=substr($provenience, $modNameFrom);
	$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));

	if(file_exists($localModelsFolder.'/module_model.class.php'))
	{
		include($dirModAdmin.'mod_bbdd.class.php');
		include($localModelsFolder.'module_model_ajax.class.php');
	}

	$dataModuleBBDD=null; $msgModuleError=' Error en el script Ajax: <br>'.__FILE__;
	$dataModuleBBDD=new ModelLocalModuleAjax($db_type, $db_host, $db_user, $db_pass, $db_name, null, $port);
	$resConnexion=$dataModuleBBDD->connectDB();
	$cur_conn_id=$dataModuleBBDD->idConexion;
}
*/

if(isset($arraExtVars['moduleAdmin']) && $arraExtVars['moduleAdmin'])
{
  $localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'';

	if(file_exists($localModelsFolder.'auth_user_access.class.php'))
	{
		include($localModelsFolder.'auth_user_access.class.php');
	}
}

//$htpasswdObj=new Htpasswd('../../.htpasswd');
$htpasswdObj=new Htpasswd($passwordsSuthFile);

// SE INTENTA INSERTAR EL USUARIO
$htpasswdObj->addUser($arraExtVars['db']['adminusers']['user'], $arraExtVars['db']['adminusers']['password']);

// SI ES UNA NUEVA PASSWORD O SE HA CAMBIADO, SE EDITA
if($htpasswdObj->statusEndProcess === 'userAlreadyExists' && $_SESSION['tmpUserPass'] != $arraExtVars['db']['adminusers']['password'])
	$htpasswdObj->updateUser($arraExtVars['db']['adminusers']['user'], $arraExtVars['db']['adminusers']['password']);

//print_r($htpasswdObj->statusEndProcess);

//$clearTextPassword=$arraExtVars['db']['adminusers']['password'];
//$password=crypt($clearTextPassword, base64_encode($clearTextPassword));
$result=0;

//echo'$password='.$password;print_r($arraExtVars);

//if(!$resConnexion)
//{
//  $msgModuleError='<div class="text-center w-100  pull-left"><div class="alert alert-danger p-25 f-16">'.strtoupper(__COMMON_BBDD_TXTCONNECTERROR).'<br><br>'.__COMMON_BBDD_TXTNOCONNECTIONBBDD.'<br><br>'.__COMMON_GENERICTEXT_ERROCONFIGMODULEEND.'<br></div></div>';
//}



$response['zzzzzzzzzzzz']=$passwordsSuthFile;
$response['usu']=$arraExtVars['db']['adminusers']['user'];
$response['pass']=$arraExtVars['db']['adminusers']['password'];
$response['success']=$htpasswdObj->statusEndProcess;
$response['status']=$htpasswdObj->curProcess;
$response['tmp p_pass']=$_SESSION['tmpUserPass'];
$response['curr_pass']=$arraExtVars['db']['adminusers']['password'];
$response['errorTxt']='Ha ocurrido un error al intentar guardar la password.<br />'.($htpasswdObj->errorMsg ? 'DETALLE: <B>'.$htpasswdObj->errorMsg.'</b>' : 'error NO DEFINIDO');
unset($_SESSION['tmpUserPass']);
$res=json_encode($response);
die($res);