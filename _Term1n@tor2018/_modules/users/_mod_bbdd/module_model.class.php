<?php
/**
 *	CLASE CON LAS FUNCIONES DE BBDD NECESARIAS PARA CREAR LAS LISTAS DE LA BBSDD STANDARD DE GTP6
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class ModelLocalModule extends CBBDD
{
	protected $database;
	private $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $defaultOrderField;
	public $keyListLabel;
	public $mainTable;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;

	public function __construct($db_type=null, $db_host='', $db_user='', $db_pass='', $db_name='', $localParams=null, $port='3306')
	{
		$this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
		$this->prefixGtp=__QUERY_TABLES_PREFIX;
		$this->query='';
		$this->mainTable=$this->prefixGtp.$localParams->mainTable;
		$this->listFieldsOutput=''; $this->mainTableFieldsNames=''; $this->defaultWhereSql='';
		$this->defaultOrderField='';
		return;
	}


	/**
	 * DEVUELVE LA LISTA DE ELEMENTOS ENCONTRADOS PARA LA TABLA ESPECIFICADA (SI OMITIDA = mainTable)
	 *
	 * @param int $idRecord
	 * @param string $localTable
	 * @param array $arrFieldsTagSelect	DEBE CONTENER LOS NOMBRES DE LOS CAMPOS PARA OBTENER LOS DATOS, EL PRIMERO SIEMPRE EL ID DE LA TABLA
	 * @return array
	 */
//	public function getSpecificRecordData($idRecord=0 ,$localTable=null, $arrFieldsTagSelect=null, $sqlWhere=null, $fieldOrder=null)
//	{
//		$sqlOrder=isset($fieldOrder) ? ' ORDER BY '.$fieldOrder : '';
//		$sqlWhere=isset($sqlWhere) ? ' && '.$sqlWhere : '';
//		$arrAliasNames=array('id_list', 'text_list');
//
//		if(count($arrFieldsTagSelect))
//		{
//			$fieldsNames='';
//			foreach($arrFieldsTagSelect as $key=>$value)
//				$fieldsNames.=$value.' AS '.$arrAliasNames[$key].', ';
//
//			$fieldsNames=substr($fieldsNames,0,-2);
//		}
//
//		$localTable=isset($localTable) ? $localTable : $this->commonDb->mainTable;
//		$query='SELECT '.$fieldsNames.' FROM `%s`.`%s` WHERE 1 '.$sqlWhere.$sqlOrder;
//		$this->query=sprintf($query, $this->commonDb->db, $localTable);
////echo '<hr>'.$this->query.' <hr><pre>';print_r($arrFieldsTagSelect);return;
//		$this->commonDb->getResultSelectArray($this->query);
////    $rowData['select=id_father']=$this->commonDb->tResultadoQuery;
//    $res=$this->commonDb->tResultadoQuery;
//
//		return $res;
//	}

}