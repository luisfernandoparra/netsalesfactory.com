<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">

		<ul class="tab-nav text-center fw-nav">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Intro</a></li>
			<!--<li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">otro TAB</a></li>-->
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>Breve introducción para el módulo de usuarios.</p>
					<ol>
						<li class="m-b-10">Con este módulo se gestionan los uasuarios de esta misma herramienta de administración.</li>
						<li class="m-b-10">Desde el campo "Nivel del role" se establecen los permisos de acceso y/o funciones permitidas para cada usuario. El numero más alto indica un mayor nivel de acceso.</li>
						<li class="m-b-10">En esta herramienta se utiliza el "e-mail" como identificador del usuario y se deberá introducir el la pntalla de LOGIN junto a la password (que no puede ser recuperada) y que se almacena codificada en la BBDD.</li>
						<li class="m-b-10">En este módulo  se almacenan solo los datos básicos de los administradores del site, Más características se pueden gestionar desde el módulo de "Configuraciones personales".</li>
						<li class="m-b-10">Recordar que las acciones posibles están sub-editadas al nivel del usuario accedido, es posible que algunas funciones no estén disponibles en ciertos niveles de acceso.</li>
						<li class="m-b-10">Para mayor seguridad, quedan almacenadas las operaciones sobre la BBDD efectuadas con esta herramienta.</li>
					</ol>
				</div>
				<div class="tab-pane fade f-16" id="tab2">
				</div>

<!-- PAGINACION SOLO SI NECESARIA
				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>
-->
			</div>

	</div>
</div>
