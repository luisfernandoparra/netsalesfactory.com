<?php
/**
 * EDIT MODULE users
 *
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');

ini_set('display_errors', $displayErrors);
error_reporting($errorReportDefault);
if(empty($_SESSION['id'])){echo '<script>parent.document.location="'.$web_url.$adminFolder.'/_modules/login/";</script>';}

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
if($hashSecutityScript != __HASH_CONTROL_SCRIPT) die(__MSG_ACCESS_ERROR);

$localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'].'_mod_bbdd/';
$localModuleFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'];
include($localModuleFolder.'config_module.php');	// LOCAL MODULE CONFIGURATION PARAMS

$msgModuleError=null; $dataModuleBBDD=null;
$iframeAction=ucfirst($arraExtVars['action']);  // NORMALIZA EL NOMBRE DE LA ACCION PARA IDENTIFICAR EL ELEMENTO A MANEJAR

/**
 *	START CARGA DE DATOS PROPIOS DEL REGISTRO A EDITAR/INSERTAR
 */
include($dirModAdmin.'mod_bbdd.class.php');
include($dirModAdmin.'mod_common_edit.class.php');
include($localModelsFolder.'module_model.class.php');

$dataModuleBBDD=new CommonEditingModel($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting, $port);
$resConnexion=$dataModuleBBDD->connectDB();

if(!$resConnexion)
{
	$msgModuleError='<div class="text-center w-100  pull-left"><div class="alert alert-danger p-25 f-16">'.strtoupper(__COMMON_BBDD_TXTCONNECTERROR).'<br><br>'.__COMMON_BBDD_TXTNOCONNECTIONBBDD.'<br><br>'.__COMMON_GENERICTEXT_ERROCONFIGMODULEEND.'<br></div></div>';
}

/**
 * SE OBTIENEN LOS DATOS RELATIVOS AL REGISTRO EDITADO (campos standard)
 *
 * $resDataForm ES EL ARRAY CON LOS DATOS DEL ACTUAL REGISTRO
 *
 */
$resDataForm=[];
if(isset($arraExtVars['id']))
	$resDataForm=$dataModuleBBDD->getCommonRecordData((int)$arraExtVars['id'], (isset($arraExtVars['foreignKey']) ? $arraExtVars['foreignKey'] : ''));

$dataLocalModel=new ModelLocalModule($dataModuleBBDD);
//	END CARGA DE DATOS PROPIOS DEL REGISTRO A EDITAR/INSERTAR


/**
 * FUNCIONES COMUNES PARA LOS FORMULARIOS DE LOS MODULOS DEL BACK-OFFICE
 */
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_classes/common_forms_methods.class.php');
$objForm=new CommonModulesMethods();

// HTML (HEADER/TOP) + ACCIONES COMUNES JS PARA LA EDICION DEL MODULO ACTUAL
include('../common_edit_html_top.php');


$isAuthorized=true;
//echo '<pre>';print_r($resDataForm);echo '</pre>';
if(isset($resDataForm['userId']) && ($resDataForm['userId'] != $_SESSION['userAdmin']['userId']) && $resDataForm['roleId'] > $_SESSION['userAdmin']['roleId'])
{
  if($resDataForm['roleId'] >= __LEVEL_ACCESS_GOD)	// SOLO PARA USUARIOS DE MAX NIVEL
  {
    echo '<br /><br /><div class="alert alert-danger w-100 p-20 f-16"><center>'.__MOD_ROLE_NOROLEACCESSTXT.'</center></div><br />';
    $isAuthorized=false;
  }
}

if($resConnexion && $isAuthorized)
{
  $arrGroups=array();
	$countPos=0;

  foreach($moduleSetting->arrFieldsFormModal as $fieldname=>$value)  // SE RECORREN TODOS LOS CAMPOS A DIBUJAR
  {
    $tmpFieldGroupValue=isset($moduleSetting->arrFieldsFormModal[$fieldname]['formGroup']) ? $moduleSetting->arrFieldsFormModal[$fieldname]['formGroup'] : $tmpFieldGroupValue;

		foreach($value as $ff=>$vv) // SE AGRUPAN SEGUN EL "formGroup" QUE TIENEN ASIGNADO
			$arrGroups[$tmpFieldGroupValue][$fieldname][$ff]=$vv;

    if(@$moduleSetting->arrGoupsForm[$countPos])
    {
			$arrGroups[$countPos]['sys_configGroupForm']=$moduleSetting->arrGoupsForm[$countPos];
			unset($moduleSetting->arrGoupsForm[$countPos]);
    }

		$countPos++;
  }


  /**
   * RECORRER TODOS LOS CAMPOS A COLOCAR EN EL FORMULARIO
   * PARA CREAR EL BOX CORRESPONDIENTE
   */
  $outFormBoxField="\r\n\t".'<div class="card-body card-padding">';

	$count=0;
	$jsForm='';	// JS QUE SE CREA DINAMICAMENTE EN LA CARGA DE LOS CAMPOS QUE FORMAN EL FORMULARIO DE EDICION / INSERCION

	foreach($arrGroups as $refGroup=>$groupFields)
	{
		if($refGroup)	// SE OMITE COMPONER ESTOS TAGS A LOS ELEMENTOS PERTENECIENTES AL GRUPO 0
		{
			$outFormBoxField.="\r\n\t".'<div class="row">';
			$outFormBoxField.="\r\n\t\t".'<div class="boxField form-group bs-item z-depth-1 col-sm-12 p-20">';
		}

		if(isset($groupFields['sys_configGroupForm']) && count($groupFields['sys_configGroupForm']))	// CABECERA DEL GRUPO DEL FORMULARIO
		{
			$outFormBoxField.=$objForm->buildGroupHeader($groupFields['sys_configGroupForm'], $count);
		}

		if($refGroup)
			$outFormBoxField.="\r\n\t\t".'<div class="containerGroup" data-ref="'.$count.'">';

		foreach($groupFields as $fieldName=>$paramField)
		{
			$resHTML=''; $tmpSqlSelect=null;

			if(in_array($fieldName, $arrEnabledFieldStatus) && ($_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole) && isset($arraExtVars['id']))	// SOLO PARA LA EDICION DE REGISTROS
			{
				$jsForm.='setButtonEnableDisable('.(int)@!$resDataForm[$fieldName].', '.$arraExtVars['id'].', "'.$fieldName.'");';
			}

			if(isset($paramField['excludeAutoForm']) && $paramField['excludeAutoForm'])
				continue;

			$defaultFormType=isset($paramField['formType']) ? $paramField['formType'] : 'text';
			$defaultLabel=isset($paramField['label']) ? $paramField['label'] : '';
			$defaultPlaceHolder=isset($paramField['placeHolder']) ? $paramField['placeHolder'] : '';
			$methodName=str_replace('-', ' ', $defaultFormType);
			$methodName=ucfirst($methodName);
			$methodName=str_replace(' ', '', $methodName);
			$methodName='buildForm'.$methodName;

      if(isset($paramField['whereSelectField']) && $paramField['whereSelectField'] == $fieldName)
        $tmpSqlSelect=$fieldName.' = '.$resDataForm[$fieldName];
      else
        $tmpSqlSelect=@$paramField['whereSelectField'];

			if($defaultFormType != 'hidden')
			{
				if(method_exists($objForm, $methodName) && isset($paramField['visible']) && $paramField['visible'])	// CONSTRUCCION HTML DEL ELEMENTO DEL FORMULARIO
				{
					if(isset($dataLocalModel) && isset($paramField['sourceTagBbddData']))	// OBTENCION DATOS DESDE TABLAS RELACIONADAS INDEPENDIENTES A LA MAIN TABLE
						$resDataForm['select='.$fieldName]=$dataModuleBBDD->getRelatedDataField((int)@$arraExtVars['id'], @$paramField['fieldTableName'], $paramField['sourceTagBbddData'], $paramField['whereSelectField'], @$paramField['orderSelectField'], @$paramField['onlyFieldValueName']);

					if(isset($dataLocalModel) && count(@$paramField['arrFieldsTagSelect']) && !isset($paramField['sourceTagBbddData']))
						$resDataForm['select='.$fieldName]=$dataModuleBBDD->getSpecificRecordData($resDataForm[$fieldName], @$paramField['fieldTableName'], $paramField['arrFieldsTagSelect'], $paramField['whereSelectField'], @$paramField['orderSelectField'], @$paramField['onlyFieldValueName']);

					if(isset($dataLocalModel) && count(@$paramField['arrFieldsTagText']) && isset($resDataForm) && !isset($paramField['sourceTagBbddData']))
						$resDataForm['displayValue='.$fieldName]=$dataModuleBBDD->getSpecificRecordData((int)@$arraExtVars['id'], @$paramField['fieldTableName'], $paramField['arrFieldsTagText'], $tmpSqlSelect, @$paramField['orderSelectField']);

					if($fieldName === 'password')	$_SESSION['tmpUserPass']=@$resDataForm[$fieldName];		// SOLO PARA AUTENTICACION EN SERVIDORES CON "basic auth"

					$resHTML=$objForm->$methodName($fieldName, $paramField, @$resDataForm, $moduleSetting->mainTable);
				}

				$outFormBoxField.=$resHTML;
			}
			else	// HIDDEN FIELDS
			{
				$outFormBoxField.="\r\n\t\t\t\t".'<input type="'.$defaultFormType.'" name="db['.$moduleSetting->mainTable.']['.$fieldName.']" value="'.@$resDataForm[$fieldName].'" />';
			}
			$count++;
		}

		if($refGroup)	// SE OMITE COMPONER ESTOS TAGS A LOS ELEMENTOS PERTENECIENTES AL GRUPO 0
		{
			$outFormBoxField.="\r\n\t\t\t".'</div>';
			$outFormBoxField.="\r\n\t\t".'</div>';
			$outFormBoxField.="\r\n\t".'</div>';
		}
	}

  $outFormBoxField.="\r\n".'</div>';
}

$saveButtonState=true;

// OMITIR PODER EFECTUAR CAMBIOS SEGUN USUARIO ACCEDIDOS Y ROL INFERIOR AL DEL REGISTRO EDITADO
if(count($resDataForm) && isset($moduleSetting->denyOtherUsers) && $arraExtVars['id'] != $_SESSION['id'] && $_SESSION['userAdmin']['roleId'] < $resDataForm[$moduleSetting->denyOtherUsers[key($moduleSetting->denyOtherUsers)]])
	$saveButtonState=false;

?>

<script type="text/javascript">
<!--
/**
 * ***** initializeModule() :: MUY IMPORTANTE *****
 * para la visualizacion correctamente el MODULO DE EDICION/INSERCION:
 *
 * REDIMENSIONA EL ALTO DEL IFRAME CONTENEDOR DEL FORMULARIO DE INSERCION DE DATOS
 * SEGUN LA ALTYRA ACTUAL DE LA CAJA CONTENEDORA
 */
function initializeModule()
{
  $(".contentForm").show();
  parent.$("#ifrm<?=$iframeAction?>Record").css("height",parent.$("#boxContent<?=$iframeAction?>Record").css("height"));parent.$("#boxContent<?=$iframeAction?>Record").show();
	return;
}

$(document).ready(function(){
	/**
	 * ACCION DE EDICION / INSERCION DE REGISTRO
	 */
	$('#moduleMainForm').on('submit', function(e){
		e.preventDefault();

		if(("mainKeyId=",$("input[name=mainKeyId]").val() > 0))
			$(".waitMessage").addClass("bgm-lightblue");

		$(".waitMessage").html(("mainKeyId=",$("input[name=mainKeyId]").val() > 0) ? "<br /><br /><p>Actualizando el registro, referencia: <span class='c-black c-back bgm-white p-r-5 p-l-5'>"+$("input[name=mainKeyId]").val()+"</span><br /><br /></p><center><div class='preloader pls-yellow'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div></center><br />" : "Insertanto el nuevo registro....<br /><br /><center><div class='preloader pls-white'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div></center><br />");

		var dataSend=new FormData($('#moduleMainForm')[0]);

		$('input[type=file]').each(function (index, value)
    {
			dataSend.append($(this).attr('name')+"[fileName]", $(this).attr('data-image-src'));
			dataSend.append("previousImage["+$(this).attr('data-image-delete')+"]", $(this).attr('data-previous-image'));	// NECESARIO PARA ELIMINAR FISICAMENTE LAS IMAGENES NO UTILIZADAS

			if(this.attributes.value)
				dataSend.append($(this).attr('data-image-delete'), this.attributes.value.value);

			var tmpName="db[<?=$moduleSetting->mainTable?>]["+$(this).attr('data-image-delete')+"]";

			if(this.attributes.value && $(this).attr('data-previous-image'))
			{
				if($(this).val())
					dataSend.append(tmpName, $(this).val());
				else
					dataSend.append(tmpName, this.attributes.value.value);
			}
			else
			{
				dataSend.append(tmpName, $(this).val());
			}
		});

		parent.$(".actCancel").attr("disabled","disabled");

		if($("input[name='action']").val() == "update")
		{
			parent.$(".saveButton").attr("disabled","disabled");
			parent.$(".enableDisableButton").attr("disabled","disabled");
		}
		else
		{
			parent.$(".insertButton").attr("disabled","disabled");
		}

		$("#modalWiderUpdating").click();	// OPEN MODAL INFO OPERATION

		$.ajax({
		data: dataSend
		,url: root_path + "ajax/common_admin.php", method: "post", dataType: "json", cache: false, async: true, contentType: false, processData:false
		,success:function(response){
			var tmpSql=response.query.sql ? response.query.sql : 0;
			if(tmpSql) window.parent.commonNotify("","<br />"+tmpSql+"<br />", undefined, undefined, undefined,(response.success ? "info" : "danger"), undefined, undefined, (response.success ? 6000 : 20000));

			if(!response.success)// && !response.msgTitle
			{
				commonRestoreErrorForm(response,0);
				return false;
			}
			else
			{
<?php
if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
{
?>
				dataSend.append("action", "passStore");
				dataSend.append("moduleAdmin", "<?=$arraExtVars['from']?>");

				var resTmpAjax=$.ajax({
					data:dataSend,
					url:"ajax_module.php",method:"post",dataType:"json", cache:false, async:true, contentType: false, processData:false
					,success:function (respTwo){
						if(!respTwo.success)// && !respTwo.msgTitle
						{
							window.parent.commonNotify("ERROR","<br />"+respTwo.errorTxt+"<br />", undefined, undefined, undefined,(respTwo.success ? "info" : "danger"), undefined, undefined, (respTwo.success ? 6000 : 10000));
							return false;
						}
						else
						{
							return true;
						}
					}
					,error:function (respTwo){
						window.parent.commonNotify("ERROR GRAVE","<br />en el ajax  para <b>`.htpasswd`</b><br />contactar informática.", undefined, undefined, undefined,"danger", undefined, undefined, 20000);
						 return false;
					}
				});
<?php
}
?>
				window.parent.commonNotify(response.msgTitle ? response.msgTitle : "<?=__COMMON_NOTIFY_CORRECTOPERATION?>", response.msgText ? "<br />"+response.msgText : "", undefined, "center", undefined, 'success', "animated flipInX", undefined);
			}

			parent.$(".saveButton").attr("disabled",false);
			parent.$(".insertButton").attr("disabled",false);
			parent.$(".enableDisableButton").attr("disabled",false);
			parent.$(".actCancel").attr("disabled",false);
			parent.$(".actCancel").click();
			parent.window.ifrListBlock.gridObj.bootgrid('reload');
			return;
		}
		,error:function(response){
				commonRestoreErrorForm(response,1);
				return false;
			}
		});

		return false;
	});

	parent.$(".headerAdditionalInfo").html("");

	$(window).load(function(){
    <?=$moduleSetting->isEditable ? 'parent.setTimeout(\'$(".saveButton").fadeIn();\',300);' : 'parent.$(".saveButton").hide();' ?>
    parent.setTimeout('$(".insertButton").fadeIn();',300);
		first_input=$('input[type=text], select')[0];	// SELECCIONA EL PRIMER INPUT DE TIPO TEXTO DEL FORMULARIO

		if(first_input != undefined){setTimeout("setFieldFocus();",400)};
<?php

if($arraExtVars['action'] == 'edit') // DIBUJA EL TITULAR DE LA VENTANA MODAL DE EDICION
{
  echo 'parent.$(".headerAdditionalInfo").removeClass("hide").html("- ID: <span class=\'text-uppercase\'>'.@$arraExtVars['id'].'</span>");';
}

if($arraExtVars['action'] == 'new')
{
//    echo 'parent.$("#ifrm'.$iframeAction.'Record").css("height",parent.$("#boxContent'.$iframeAction.'Record").css("height"));parent.$("#boxContent'.$iframeAction.'Record").show();';
}

if(strlen($jsForm) && $moduleSetting->isCanEnabled)	// SOLO SI SE HAN CREADO INSTRUCCIONES JS, SE PINTAN EN EL FORMULARIO
{
	echo '
parent.setTimeout(\'$(".enableDisableButton").fadeIn();\',600);
';
	echo $jsForm;
}

if(!$saveButtonState)	// DESHABILITAR BOTONES SEGUN CRITERIOS
{
	echo '
parent.$(".saveButton").attr("disabled","disabled");
parent.$(".enableDisableButton").attr("disabled","disabled");
';
}

?>
    setTimeout("initializeModule();vAlignImages();",100);

	}); // END DOCUMENT LOAD
}); // END DOCUMENT READY

function vAlignImages()
{
  $('.fileinput-new').each(function(index, value){$(value).click();return false;});
  return;
}

function setButtonEnableDisable(statusToSet, idRecord, fieldName)
{
	parent.$(".enableDisableButton").html(statusToSet ? "<i class='zmdi zmdi-check'></i>&nbsp;&nbsp;<?=__COMMON_BTN_ENABLERECORD?>" : "<span class='ORANGE'>&nbsp;<i class='zmdi zmdi-block'></i>&nbsp;<?=__COMMON_BTN_DISABLERECORD?>&nbsp;</span>");
	parent.$(".enableDisableButton").attr("data-id", idRecord);
	parent.$(".enableDisableButton").attr("data-field-name", fieldName);
	parent.$(".enableDisableButton").attr("data-status", statusToSet);

	if(statusToSet = 0)
		parent.$(".enableDisableButton").addClass("bgm-orange");

	return;
}
-->
</script>
</head>

<body>

<div class="contentForm card" style="display:none;">
  <form action="" method='post' id='moduleMainForm' name='moduleMainForm' onsubmit='//return false;' enctype='multipart/form-data'>
		<input name="mainKeyId" type="hidden" value="<?=(int)@$arraExtVars['id']?>" />
		<input name="mainKeyName" type="hidden" value="<?=$moduleSetting->keyListLabel?>" />
		<input name="action" type="hidden" value="<?=isset($arraExtVars['id']) ? 'update' : 'insert'?>" />

<?php
if($msgModuleError)
{
  /**
   * ERROR EN LA CARGA DE LOS DATOS
   */
	die($msgModuleError);
}
else
{
	// SE DIBUJAN LOS ELEMENTOS DEL FORMULARIO A MOSTRAR
	echo $outFormBoxField;
?>
		<input id="tmpSubmit" name="tmpSubmit" type="submit" class="hide" />
  </form>

<?php
//echo '<pre>';
//print_r($objForm);
//print_r($moduleSetting->arrFieldsFormModal);
//print_r(@$arraExtVars);
//print_r($dataModuleBBDD);
//print_r($moduleSetting->arrGoupsForm);
//print_r($arrGroups);
//print_r($moduleSetting->arrFieldsFormModal);
//echo '</pre>';
}

// TESTS...
//for($n=0;$n<rand(1,10);$n++){echo '<pre>block-test:'.$n.'<p>';print_r($arraExtVars);echo '</pre>';}
?>
</div>
<a id="modalWiderUpdating" data-toggle="modal" href="#bbddOperation" class="hide" title="" >BBDD-operation</a>

<div class="modal fade" data-modal-color="blue" id="bbddOperation" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><i><?=__COMMON_MODALMSG_UPDATINGOPERATIONTITLEH4?></i></h4>
			</div>
			<div class="modal-body waitMessage">
					<p><?=__COMMON_MODALMSG_WAITMESSAGE?><span class="idTitleRef"></span></p>
			</div>
		</div>
	</div>
</div>

<?php
//
if($_SESSION['userAdmin']['roleId'] > __LEVEL_ACCESS_VISITOR)	// MINIMUN ACCESS LEVEL
	include('../common_html_contents_all.php');

?>
</body>
</html>