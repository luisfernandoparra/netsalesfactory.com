<?php
/**
 * DATE UPDATE: 2018-03-14 10:48 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * LEAD FRONT WEB LOGS
 * 
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=__LEVEL_ACCESS_GOD;
$minRoleAdim=__LEVEL_ACCESS_VISIT;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>acciones efectuadas en el front.</small>';

// MANDATORY PARAMS
$moduleSetting->moduleBBDD=$bbdd_logs;
//$moduleSetting->mainTable='log_front_web_'.date('Y');
$moduleSetting->mainTable='log_web_actions2018';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='id';
$moduleSetting->keyListLabel='id';
$moduleSetting->offsetStart=0;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->fieldNameStatusRow=null;
$moduleSetting->allowNewRecord=false;
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[10,25,50,200,500]';
$moduleSetting->minEditLevel=__LEVEL_ACCESS_MASTER;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD ? true : false;
$moduleSetting->isCanEnabled=false;
$moduleSetting->defaultOrderField=['fecha'=>'DESC'];
//$moduleSetting->defaultOrderField=['log_date'=>'DESC','log_time'=>'DESC'];


$order=0;
//echo $web_url.$adminFolder;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id'=>array(
    'formType'=>'hidden'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>0
    ,'label'=>'ID'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'id_crea'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>3,'columnWidth'=>2]
		,'tabularFilter'=>['mode'=>'BBDD','origin'=>__QUERY_TABLES_PREFIX.'landings_site_config','tag'=>array('filterType'=>'select')]
		,'required'=>0
		,'isDisabled'=>true
		,'fieldTableName'=>__QUERY_TABLES_PREFIX.'landings_site_config'
		,'arrFieldsTagSelect'=>array('id','landing_name') // PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
		,'includeMainBBDD'=>true
		,'whereSelectField'=>null
		,'orderSelectField'=>'landing_name'
    ,'label'=>' Landing'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Landing'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'fecha'=>array(
    'formType'=>'date'
    ,'dataType'=>'timestamp'
    ,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>2]
    ,'tabularFilter'=>['mode'=>'STATIC','tag'=>array('filterType'=>'date')]
    ,'required'=>0
    ,'label'=>' Fecha'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Fecha del evento'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'http_referer'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Url'
    ,'rowsData'=>2
		,'actionsForm'=>(object)['isHtmlBtn'=>true]
    ,'colsClass'=>12
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'URL del evento'
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'accion'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>4,'columnWidth'=>3]
    ,'required'=>0
    ,'label'=>'Acci&oacute;n'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Acci&oacute;n / detalles'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'ip_usuario'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'IP'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'IP'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'script'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>5,'columnWidth'=>1]
    ,'required'=>0
    ,'label'=>'Script/M&oacute;dulo'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Script/M&oacute;dulo del evento'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'session_id'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>6,'columnWidth'=>2]
    ,'required'=>0
    ,'label'=>'Ref. sesi&oacute;n'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Ref. sesion'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  1=>array(
    'groupTextTitle'=>'Log acciones leads'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
	,2=>array(
		'groupTextTitle'=>'Otros detalles'
		,'additionalInfo'=>''
		,'groupIco'=>'zmdi zmdi-format-color-fill'
		,'groupHeaderClass'=>'bg-black-trp'
	)
);


//	PLUG-IN HIGH-CHARTS CONFIGURATION (TABLA: sys_plugin_conf_high_charts)
/*
$moduleSetting->pluginsTopIndex=[
	'plugInName'=>'highCharts'
	,'configLocalChart'=>[
		'localIpKey'=>'62.14.196.157'
	]
	,'customPath'=>'high_charts'
	,'chartParams'=>['tooltip1'=>__PLUGINCHART_LABEL_1]
	,'allowedFilterDates'=>'_true'	// '_true' OR 'no'
	,'sqlParams'=>[
		'mainBBDD'=>$bbdd_logs
		,'mainTablePrefix'=>__QUERY_TABLES_PREFIX
		,'mainTableName'=>__QUERY_TABLES_PREFIX.$moduleSetting->mainTable
		,'whereFieldDisctinct'=>'ip_usuario'
		,'whereRangeType'=>'DATE'
		,'fieldNameDateBetween'=>'fecha'
		,'whereExclusion'=>'ip_usuario!="'.($sitesPruebas ? $_SERVER['REMOTE_ADDR'] : '62.14.196.157').'"'
//		,'whereExclusion'=>'ip_usuario!="192.168.2.102"'
		,'fieldNameHours'=>'log_time'
		,'defaultDateBetweenFrom'=>date('Y-m-').'01'
		,'defaultDateBetweenTo'=>date('Y-m-d')
		,'defaultStepGroupDate'=>'minute'	// VALID PARAMS: minute, hour, day, week, month, year
	]
];
*/
