<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">
		<ul class="tab-nav text-center fw-nav" data-tab-color="green">
			<li class=""><a href="#tab-mod-1" data-toggle="tab" aria-expanded="false">nota sobre este mismo módulo</a></li>
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade f-16" id="tab-mod-1">
          <br />Estas son todas las principales acciones efectuadas por los leads hacia la BBDD.
          <br /><br />También se guardan algunas acciones específicas efectuadas en las páginas.
					<p>Algunos comportamientos que vienen almacenados:</p>
					<ul>
						<li><b>checkValidDomain</b>: corresponde a la apertura inicial de la Web de VTP.</li>
						<li><b>A{btnBoxOpinions:=Opiniones</b>: Donde siempre "<i>A{</i>" está a indicar una "acción" del lead.</li>
						<li><b>A{bottomOpenModalCookies:=Política de cookies</b>: Acción que indica la <i>apertura de la ventana modal de las cookies</i> por parte del lead al presionar el control especiífico presente en el pié de página.</li>
						<li><b>A{btnCloseCookies:=Cerrar</b>: Acción que indica el <i>cierre de la ventana modal de las cookies</i> por parte del lead.</li>
					</ul>
					<p>Y muchas más, disponibles para permitir el análisis de los comportamientos de los leads en la Web de VTP (esta parte está desarrollada para uso futuro a perfeccionar).</p>
				</div>
<!--
				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>
-->
			</div>
	</div>
</div>
