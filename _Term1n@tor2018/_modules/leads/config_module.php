<?php
/**
 * DATE UPDATE: 2017-09-13 10:42 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * ADMIN LEADS
 * 
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=$_SESSION['userAdmin']['roleId'];  // AJUSTA AUTOMATICAMENTE EL MAXIMO ROL PARA EL USUARIO ACCEDIDO
$minRoleAdim=1;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>comentarios espec&iacute;ficos del m&oacute;dulo actual....</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='leads';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='id_lead';
$moduleSetting->keyListLabel='id_lead';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->isCanEnabled=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole ? true : false;
$moduleSetting->fieldNameStatusRow='b_enabled';
$moduleSetting->denyOtherUsers=$_SESSION['userAdmin']['roleId'] <= __LEVEL_ACCESS_GOD;
$moduleSetting->allowNewRecord=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->minEnableLevel=__LEVEL_ACCESS_MASTER;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[10,25,50]'; // array, EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;

/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array(
		' && '.$moduleSetting->fieldNameStatusRow.' = '=>0
		,'&& roleId <= '=>$_SESSION['userAdmin']['roleId']
// EXAMPLE WITH MORE SENTENCES:
//		,'|| ('=>''
//		,'theme = '=>'\'123\''
//		,')'=>''
	)
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode( $arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS



//echo $moduleSetting->moduleIcon.'<pre>';print_r($arrEraseOptions);echo'</pre>';
$order=0;

//echo $web_url.$adminFolder;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id_lead'=>array(
    'formType'=>'text'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>1
    ,'label'=>'Id lead'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>true
    ,'order'=>++$order
  )
  ,'phone'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>1]
    ,'required'=>1
    ,'label'=>'Tel&eacute;fono'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'name'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>3]
    ,'required'=>1
    ,'label'=>'Nombre'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Nombre'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'last_name1'=>array(
     'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>4,'columnWidth'=>2]
    ,'required'=>1
    ,'label'=>'Apellido'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Apellido'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'last_name2'=>array(
     'formType'=>'text'
    ,'dataType'=>'varchar'
		,'colsClass'=>2
    ,'label'=>'Segundo apellido'
    ,'helpText'=>'no utilizado actualmente'
    ,'visible'=>1
    ,'placeHolder'=>'Apellido 2'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'email'=>array(
     'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>5,'columnWidth'=>2]
    ,'required'=>1
    ,'label'=>'email'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'email'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'zip_code'=>array(
     'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>1
		,'colsClass'=>1
    ,'label'=>'CP'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'CP'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'gender'=>array(
     'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
		,'colsClass'=>1
    ,'label'=>'Sexo'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'ip_address'=>array(
     'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
 		,'colsClass'=>1
   ,'label'=>'Ip'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'password'=>array(
     'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
 		,'colsClass'=>2
   ,'label'=>'Password'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Password'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'insert_date'=>array(
    'formType'=>'date'
    ,'dataType'=>'timestamp'
		,'tabularListData'=>['columnOrder'=>6,'columnWidth'=>2]
    ,'required'=>0
    ,'label'=>'Fecha registro'
		,'colsClass'=>2
    ,'helpText'=>''
    ,'isDisabled'=>true
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'b_email_valid'=>array(
    'formType'=>'checkbox'
    ,'dataType'=>'toggle'
    ,'required'=>0
    ,'toggleParams'=>array('tsColor'=>'green')
    ,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')
    ,'label'=>'Email OK'
    ,'helpText'=>'Email verificado'
    ,'visible'=>1
    ,'isDisabled'=>($_SESSION['userAdmin']['roleId'] < __LEVEL_ACCESS_GOD ? true : false)
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'b_phone_valid'=>array(
    'formType'=>'checkbox'
    ,'dataType'=>'toggle'
    ,'required'=>0
    ,'toggleParams'=>array('tsColor'=>'green')
    ,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')
    ,'label'=>'Tel&eacute;fono OK'
    ,'helpText'=>'Tel&eacute;fono verificado'
    ,'visible'=>1
    ,'isDisabled'=>($_SESSION['userAdmin']['roleId'] < __LEVEL_ACCESS_GOD ? true : false)
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'id_channel_eml'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
		,'colsClass'=>2
    ,'label'=>'id_channel_eml'
    ,'helpText'=>'ID de registro del CRM'
    ,'helpTextExtend'=>'es el identificador devuelto por el CRM para el email del lead'
    ,'visible'=>1
    ,'isDisabled'=>true
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'id_channel_phone'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
		,'colsClass'=>2
    ,'label'=>'id_channel_phone'
    ,'helpText'=>'ID de registro del CRM'
    ,'helpTextExtend'=>'es el identificador devuelto por el CRM para el teléfono del lead'
    ,'visible'=>1
    ,'isDisabled'=>true
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'utm_campaign'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
		,'colsClass'=>2
    ,'label'=>'Ref. ingenious'
    ,'helpText'=>'Referencia de ingenious'
    ,'helpTextExtend'=>'es el identificador devuelto por INGENIOUS (conocido también como ´utm_campaign´)'
    ,'visible'=>1
    ,'isDisabled'=>true
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'id_affiliate'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
		,'colsClass'=>2
    ,'label'=>'Id fuente'
    ,'helpText'=>'Id fuente o afiliado'
    ,'helpTextExtend'=>'es el identificador de la fuente'
    ,'isDisabled'=>true
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'birth_date'=>array(
    'formType'=>'date'
    ,'dataType'=>'date'
    ,'required'=>0
		,'colsClass'=>1
    ,'label'=>'Nacimiento'
    ,'helpText'=>'Fecha de nacimiento'
    ,'isDisabled'=>true
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'dismiss_date'=>array(
    'formType'=>'date'
    ,'dataType'=>'date'
    ,'required'=>0
		,'colsClass'=>1
    ,'label'=>'fecha rechazo'
    ,'helpText'=>'Fecha de rechazo del lead'
    ,'helpTextExtend'=>'NOTA IMPORTANTE: si se visualiza `1970-01-01` significa que NO hay fecha de rechazo'
    ,'isDisabled'=>true
    ,'displayFieldOn'=>'{"notEqual":{"value":"0000-00-00 00:00:00","action":"return"}}'
    ,'visible'=>($_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD ? true : false)
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'b_enabled'=>array(
    'formType'=>'checkbox'
    ,'dataType'=>'toggle'
    ,'tabularListData'=>['columnOrder'=>6,'columnWidth'=>1]
    ,'required'=>0
    ,'toggleParams'=>array('tsColor'=>'green')
    ,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')	// PARA ESTE CONTROL, SIEMPRE EL PRIMER VALOR ES EL NEGATIVO Y EL SEGUNDO EL POSITIVO
    ,'label'=>'Estado'
    ,'helpText'=>'Habilitar / deshabilitar el lead'
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  1=>array(
    'groupTextTitle'=>'Datos personales'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
  ,2=>array(
    'groupTextTitle'=>'Otros datos / parámetros del lead'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi zmdi-book'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);
//echo'<pre>';print_r($moduleSetting);echo'</pre>';