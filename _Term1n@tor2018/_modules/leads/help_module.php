<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">

		<ul class="tab-nav text-center fw-nav">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Leads</a></li>
			<li><a href="#tab2" data-toggle="tab" aria-expanded="false">Los campos</a></li>
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>Listado de leads registrados.</p>
					<ol>
						<li class="m-b-10">Aquí se recogen todos los registros de los leads efectuados (en VTP).</li>
						<li class="m-b-10">Recordar que algunos de los campos definidos en este módulo existen para mantener la compatibilidad de GTP 5.5 (por ejemplo, <i>last_name1</i>).</li>
						<li class="m-b-10">El registro del lead queda almacenado cuando se verifican y rellenan correctamente los campos presentes en el formulario. Importante recordar que el lead <b>no estará activo</b> hasta que no efectue su confirmación a través del link que recibirá por e-mail. A través de este módulo es posible <i>activar / desactivar</i> leads independientemente.</li>
					</ol>
				</div>

				<div class="tab-pane fade f-16" id="tab2">
					<p>Desglose de los elementos que componen el módulo de traducciones de las preguntas.</p>
					<ol>
						<li class="m-b-10"><strong><span class="c-bluegray">[id_lead]</span></strong>, autonumérico (referencia para localizar el registro).</li>
						<li class="m-b-10"><strong>"Teléfono" <span class="c-green">[phone]</span></strong>, el número de teléfono.</li>
						<li class="m-b-10"><strong>"Nombre" <span class="c-green">[name]</span></strong>, el nombre del lead.</li>
						<li class="m-b-10"><strong>"Apellido" <span class="c-green">[last_name1]</span></strong>, el apellido del lead.</li>
						<li class="m-b-10"><strong>"Apellido (2)" <span class="c-green">[last_name2]</span></strong>, el segundo apellido del lead <b>(no utilizado actualmente)</b>.</li>
						<li class="m-b-10"><strong>"Email" <span class="c-green">[email]</span></strong>, dirección de correo electrónico del lead.</li>
						<li class="m-b-10"><strong>"CP" <span class="c-green">[zip_code]</span></strong>, código postal del lead.</li>
						<li class="m-b-10"><strong>"Sexo" <span class="c-green">[gender]</span></strong>, Género o sexo del lead.</li>
						<li class="m-b-10"><strong>"Ip" <span class="c-green">[ip_address]</span></strong>, dirección IP utilizada por el navegador en el momento del registro.</li>
						<li class="m-b-10"><strong>"Password" <span class="c-green">[password]</span></strong>, contraseña del lead que le llegará por notificación electrónica tras el correcto registro.</li>
						<li class="m-b-10"><strong>"Nacimiento" <span class="c-green">[birth_date]</span></strong>, fecha de nacimiento del lead.</li>
						<li class="m-b-10"><strong>"Fecha registro" <span class="c-green">[insert_date]</span></strong>, fecha en a que se ha efectuado el registro en la BBDD.</li>
						<li class="m-b-10"><strong>"Email OK" <span class="c-green">[b_email_valid]</span></strong>, indica si el correo electrónico utilizado por el lead es correcto y existe.</li>
						<li class="m-b-10"><strong>"Tel&eacute;fono OK" <span class="c-green">[b_phone_valid]</span></strong>, indica si el teléfono indicado por el lead es correcto y existe.</li>
						<li class="m-b-10"><strong>"Estado" <span class="c-green">[b_enabled]</span></strong>, estado en BBDD del lead, este se activa automáticamente si el lead confirma el e-mail de registro.</li>
						<li class="m-b-10"><strong>"ID channel eml" <span class="c-green">[id_channel_eml]</span></strong>, es el identificador devuelto por el CRM para el email del lead.</li>
						<li class="m-b-10"><strong>"ID channel phone" <span class="c-green">[id_channel_phone]</span></strong>, es el identificador devuelto por el CRM para el teléfono del lead.</li>
						<li class="m-b-10"><strong>"Ref. ingenious" <span class="c-green">[utm_campaign]</span></strong>, es el identificador devuelto por <i>Ingenious</i>.</li>
						<li class="m-b-10"><strong>"Id fuente" <span class="c-green">[id_affiliate]</span></strong>, es el identificador de la fuente.</li>
					</ol>
				</div>

				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>

			</div>

	</div>
</div>
