<?php
@header('Content-Type: text/html; charset=utf-8');
@session_start();

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;
?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php
// START COMMON AND VENDOR CSS & JAVASCRIPT COMMON LIBRARIES
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_comm_vendor_CSS.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_javascript_libraries.php');
// END COMMON AND VENDOR CSS & JAVASCRIPT COMMON LIBRARIES
//echo date('H:i:s').'<pre>';print_r($_REQUEST);echo'</pre>';
?>

<script type="text/javascript">
$(document).ready(function(){
	parent.$(".headerAdditionalInfo").html("");

	// ACCIONES COMUNES AL ACABAR DE CARGAR EL IFRAME DE NUEVO REGISTRO DEL MODULO ABIERTO
	$(window).load(function(){
		// DIBUJA EL TITULAR DE LA VENTANA MODAL DE NUEVO REGISTRO
		window.parent.$(".idTitleRef").html("<span class='color-block p-5'><i class='zmdi zmdi-edit'>&nbsp;&nbsp;</i>Inserción de nuevo registro para el módulo: <b><?=$arraExtVars['module']['name']?></b> <span>")
	});
});
</script>
