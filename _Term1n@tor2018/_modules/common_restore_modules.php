<?php
/**
 * RESTORE CONFIG FILES DISPLAY BLOCKS
 * AND MANAGE INNER ACTIONS
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$backUpsFolder=$arraExtVars['moduleAdmin'].'_backups/';

$outputFiles=array();
foreach(new RecursiveIteratorIterator(
	new RecursiveDirectoryIterator($backUpsFolder, FilesystemIterator::SKIP_DOTS | FilesystemIterator::UNIX_PATHS)) as $value){
			if($value->isFile()){
				$outputFiles[]=array($value->getMTime(), $value->getRealPath());
			}
}

usort($outputFiles, function($a, $b){
   return $a[0] < $b[0];
});

if(count($outputFiles))	// EXISTEN ARCHIVOS EN LA CARPETA DE BACKUPS
{
	$otputRestoreBlocks='<div class="list-group"><div class="m-l-20 m-t-10 c-blue"><i class="zmdi zmdi-file">&nbsp;&nbsp;</i> '.__RIGHTSIDEBAR_LABEL_PREVIOUSRESTORETXT.'</div>';

	foreach($outputFiles as $data)  // SE CREAN LA LISTA + TEXTOS NECESARIOS PARA GESTIONAR LOS ARCHIVOS DE BACK-UP DEL MODULO ABIERTO
	{
		$backUpFileName=substr($data[1],-17);
		$onlyFileName=substr($data[1],-37);

		if('config_module.php' == $backUpFileName)
		{
			$otputRestoreBlocks.='<a class="list-group-item media restoreFile" data-toggle="modal" href="#" title="Restore file" data-module-path="'.$backUpsFolder.'">';
			$otputRestoreBlocks.='<div class="media-body" data-name="'.$onlyFileName.'">';
			$otputRestoreBlocks.='<small class="btnRestoreConfigFile" title="'.__COMMON_GENERICTEXT_RESTORE.': '.$onlyFileName.'">'.date('d-m-Y / H:i:s',$data[0]).'';  // FECHA DEL ARCHIVO DE BACK-UP
			$otputRestoreBlocks.='<button class="pull-right killConfigFile btn bgm-red btn-xs waves-effect" title="'.__RIGHTSIDEBAR_BTN_BACKUPDELETETITLE.'"><i class="zmdi zmdi-delete"></i></button>';
			$otputRestoreBlocks.='<button class="pull-right btn bgm-lightgreen btn-xs waves-effect actDisplayFile m-r-5" title="'.__RIGHTSIDEBAR_BTN_VIEWFILECONTENTSTITLE.'" >&nbsp;<i class="zmdi zmdi-zoom-in"></i>&nbsp;</button>';
			$otputRestoreBlocks.='</small>';
			$otputRestoreBlocks.='</div>';
			$otputRestoreBlocks.='</a>';
		}
	}

	$otputRestoreBlocks.='</div>';
}

$otputRestoreBlocks=isset($otputRestoreBlocks) ? $otputRestoreBlocks : '';
?>
<script type="text/javascript">
$(document).ready(function(){
	$(".sidebar .configDiv").append('<?=$otputRestoreBlocks?>');

	// BUTTON ACTION: RELOAD CURRENT MODULE
	$(document).on("click",".reloadModule",function(){
    document.location.href="<?=basename($_SERVER["SCRIPT_FILENAME"]).'?'.$_SERVER['QUERY_STRING']?>";
  });

  // FINAL BUTTON ACTION EXCECUTE AFTER FILE DISPLAY IN TO SCREEN
	$(document).on("click",".doRestore, .doDelete",function(){
    $("#displayBakUpFile").modal('toggle');

    if(this.getAttribute("class").indexOf("doDelete") > -1) // BORRAR BACK-UP
      $(".actDeleteConfigBackUp").click();
 
    if(this.getAttribute("class").indexOf("doRestore") > -1) // RESTAURAR BACK-UP
      $(".actRestoreConfigBackUp").click();
 	});

	$(".btnRestoreConfigFile").click(function(){
		$(".actRestoreConfigBackUp").attr("disabled",false);
		$(".actDisplayConfigBackUpFile").attr("disabled",false);
		$(".idTitleRef").html("");
    $("#modalRestoreConfigFile").modal();
    $(".idTitleRef").html("<br /><b>"+$(this).parent().attr("data-name")+"</b>");
    $(".actRestoreConfigBackUp").attr("data-file",$(this).parent().attr("data-name"));
    $(".actDisplayConfigBackUpFile").attr("data-file",$(this).parent().attr("data-name"));
	});

	// SE PROCEDE A RESTAURAR LA COPIA DE SEGURIDAD DEL ARCHIVO SELECCIONADO
	$(".actRestoreConfigBackUp").click(function(){
    var tmpName=this.getAttribute("data-file");
		this.setAttribute("disabled",true);
    commonNotify("<?=__COMMON_NOTIFY_INFOACTIONEXECUTING?>","", undefined, undefined, undefined, 'info');
    $(".actDisplayConfigBackUpFile").attr("disabled",true);
		$(".idTitleRef").html("<br><br><center><i><?=ucfirst(__COMMON_GENERICTEXT_RESTORING)?>...</i><br /><br /><?=__COMMON_GENERICTEXT_PLEASEWAIT?></center>");

		$.ajax({
			data:{action: "restoreBackUpConfigModule", fileName: tmpName, folder: this.getAttribute("data-dir")}
			,url:root_path + "ajax/common_admin.php", method: "post", dataType: "json", cache: false, async: true
			,success:function(response){
				if(!response.success)// ERROR
				{
          commonNotify("<?=__COMMON_NOTIFY_RESTOREERROR?>",undefined, undefined, undefined, undefined, 'danger');
          $(".idTitleRef").html("<br/ ><br/ ><div class='p-10 alert-danger'><b><?=__COMMON_GENERICTEXT_ERROR?></b>&nbsp;&nbsp;<br><br><?=__COMMON_AJAXMSG_UNABLERESTOREFILETXT?>: <b>"+$(".actDisplayConfigBackUpFile").attr("data-file")+"</b></div>");
					var res=$(document).find("[data-name='"+tmpName+"']");
          $(res).addClass("bgm-amber");
					return false;
				}

        var res=$(document).find("[data-name='"+tmpName+"']");
        $(res).parent().fadeOut();
        commonNotify("<?=__COMMON_NOTIFY_RESTORESUCCESS?>","", undefined, undefined, undefined, 'success');
        $(".idTitleRef").html("<br/ ><br/ ><div class='p-10 alert-warning'><b><?=__COMMON_NOTIFY_CORRECTOPERATION?></b>&nbsp;&nbsp;<br><br><?=__COMMON_AJAXMSG_SUCCESSRESTOREFILETXT?>:<br />"+tmpName+"<br /><br />(<?=__COMMON_AJAXMSG_INFORELOADPAGEAFTERRESTORETXT?>)</div>");
        $(".actRestoreConfigBackUp").parent().append("&nbsp;<button class='btn btn-sm bgm-deeporange waves-effect waves-ripple reloadModule' ><?=__COMMON_GENERICTEXT_RELOADTXT?></button>")
				return false;
			}
			,error:function(response){
					return false;
			}
		});
  });


	// BUTTON ACTION: DISPLAY ONLY BACK-UP FILE MODAL
	$(".actDisplayFile").click(function(){
		$(".actRestoreConfigBackUp").attr("disabled",false);
		$(".actDisplayConfigBackUpFile").attr("disabled",false);
		$(".idTitleRef").html("");
		$(".actDeleteConfigBackUp").attr("disabled",false);
		$(".actDisplayConfigBackUpFile").attr("disabled",false);
		$(".idTitleRef").html("");
		$("#modalRestoreConfigFile").modal();
    $(".actRestoreConfigBackUp").attr("data-file",$(this).parent().attr("data-name"));
    $(".actDisplayConfigBackUpFile").attr("data-file",$(this).parent().attr("data-name"));
    $(".restoreDisp").attr("data-next", "");
    setTimeout('$(".restoreDisp").click();$(".restoreDisp").attr("data-next", "restore");',200);
		return true;
	});

	// BUTTON ACTION: DELETE BACK-UP CONFIG FILE
	$(".killConfigFile").click(function(){
		$(".actDeleteConfigBackUp").attr("disabled",false);
		$(".actDisplayConfigBackUpFile").attr("disabled",false);
		$(".idTitleRef").html("");
		$("#modaldeleteConfigFile").modal();
		$(".idTitleRef").html("<br /><b>"+$(this).parent().parent().attr("data-name")+"</b>");
		$(".actDeleteConfigBackUp").attr("data-file",$(this).parent().parent().attr("data-name"));
    $(".actDisplayConfigBackUpFile").attr("data-file",$(this).parent().parent().attr("data-name"));
		return false;
	});


	// VISUALIZAR EL ARCHIVO ANTES DE APLICAR LA ACCION
	$(".actDisplayConfigBackUpFile").click(function(){
//console.log(this.getAttribute("data-file")," -> cliccato -> ", this.getAttribute("data-dir"))
    this.setAttribute("disabled",true);
    $(".actRestoreConfigBackUp").attr("disabled",true);
    $(".actDeleteConfigBackUp").attr("disabled",true);
    $(".idTitleRef").html("<br><br><center><i><?=__COMMON_GENERICTEXT_LOADCONTENTSTXT?>...</i><br /><br /><?=__COMMON_GENERICTEXT_PLEASEWAIT?></center>");

		$.ajax({
			data:{action: "readConfigModuleFile", fileName:this.getAttribute("data-file"), folder:this.getAttribute("data-dir"), nextAction:$(this).attr("data-next")}
			,url:root_path + "ajax/common_admin.php", method: "post", dataType: "json", cache: false, async: true
			,success:function(response,ex,el){
				if(!response.success)// && !response.msgTitle
				{
          $(".idTitleRef").html("<br/ ><br/ ><div class='p-10 alert-danger'><b><?=__COMMON_GENERICTEXT_ERROR?></b>&nbsp;&nbsp;<br><br>Imposible leer <b>"+$(".actDisplayConfigBackUpFile").attr("data-file")+"</b></div>");
          swal({
            title:"<?=__COMMON_GENERICTEXT_ERROR?>",
            text:response.errorText,
            type:"error",
            showCancelButton:false,
            confirmButtonText:"<?=__COMMON_BTN_CLOSE?>",
            closeOnConfirm:false,
            closeOnCancel:false,
            timer:10000,
          });
  				return false;
				}

        // SUCCESS FILE READ: DISPLAY FILE CONTENTS
        $('#displayBakUpFile').modal('toggle');
        $(".idTitleRef").html("<br>(cargado correctamente)");

        if($("#modaldeleteConfigFile").css("display") == "block")
          $("#modaldeleteConfigFile").modal('toggle');

        if($("#modalRestoreConfigFile").css("display") == "block")
          $("#modalRestoreConfigFile").modal('toggle');

        $(".bakUpFileBody").html(""+response.txtContents+"");
				return true;
			}
			,error:function(response){
        $(".idTitleRef").html("<br/ ><br/ ><div class='p-10 alert-danger'><b><?=__COMMON_GENERICTEXT_ERROR?></b>&nbsp;&nbsp;<br><br><?=__COMMON_AJAXMSG_UNBELREADTXT?> <b>"+$(".actDisplayConfigBackUpFile").attr("data-file")+"</b></div>");
        swal({
          title:"<?=__COMMON_GENERICTEXT_ERROR?>",
          text:"<?=__COMMON_JSGENERIC_UNEXPECTEDERROR?>!",
          type:"error",
          showCancelButton:false,
          confirmButtonText:"<?=__COMMON_BTN_CLOSE?>",
          closeOnConfirm:false,
          closeOnCancel:false,
          timer:10000,
        });
  			return false;
			}
		});
  });


	// SE PROCEDE A BORRAR EL ARCHIVO DEL SERVIDOR
	$(".actDeleteConfigBackUp").click(function(){
    commonNotify("<?=__COMMON_NOTIFY_INFOACTIONEXECUTING?>","", undefined, undefined, undefined, 'info');
    var tmpName=this.getAttribute("data-file");
		this.setAttribute("disabled",true);
    $(".actDisplayConfigBackUpFile").attr("disabled",true);
		$(".idTitleRef").html("<br><br><center><i><?=__COMMON_GENERICTEXT_DELETING?>...</i><br /><br /><?=__COMMON_GENERICTEXT_PLEASEWAIT?></center>");

		$.ajax({
			data:{action: "deleteBackUpConfigModule", fileName:tmpName, folder:this.getAttribute("data-dir")}
			,url:root_path + "ajax/common_admin.php", method: "post", dataType: "json", cache: false, async: true
			,success:function(response){
				if(!response.success)// && !response.msgTitle
				{
          commonNotify("<?=__COMMON_NOTIFY_DELETEERROR?>",undefined, undefined, undefined, undefined, 'danger');
          $(".idTitleRef").html("<br/ ><br/ ><div class='p-10 alert-danger'><b><?=__COMMON_GENERICTEXT_ERROR?></b>&nbsp;&nbsp;<br><br><?=__COMMON_JSGENERIC_DELETEERRORTXT?>: <b>"+$(".actDisplayConfigBackUpFile").attr("data-file")+"</b></div>");
					var res=$(document).find("[data-name='"+tmpName+"']");
          $(res).addClass("bgm-orange");
					return false;
				}

        var res=$(document).find("[data-name='"+tmpName+"']");
        $(res).parent().fadeOut();
        commonNotify("<?=__COMMON_NOTIFY_DELETESUCCESS?>","", undefined, undefined, undefined, 'success');
        $(".idTitleRef").html("<br/ ><br/ ><div class='p-10 alert-warning c-black'><b><?=__COMMON_GENERICTEXT_OKTXT?></b>&nbsp;&nbsp;<br><br><?=__COMMON_AJAXMSG_SUCCESSDELETEFILETXT?>:<br />"+tmpName+"</div>");
				return false;
			}
			,error:function(response){
          $(".idTitleRef").html("<br/ ><br/ ><div class='p-10 alert-danger'><b><?=__COMMON_AJAXMSG_TRYINGTOERASEERRORTXT?></b>&nbsp;&nbsp;<br><br><?=__COMMON_GENERICTEXT_UNDETERMINEDERRORTXT?></div>");
					return false;
			}
		});
	});

});
</script>

<div class="modal fade" data-modal-color="blue" id="modalRestoreConfigFile" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
					<h4 class="modal-title"><?=__COMMON_MODALMSG_MODALRESTORECONFIGFILEH4?></h4>
			</div>
			<div class="modal-body">
					<p><?=__COMMON_MODALMSG_MODALRESTORECONFIGFILENAMETXT?>: <span class="idTitleRef"></span></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect actDisplayConfigBackUpFile restoreDisp" data-next="restore" data-file="" data-dir="<?=$backUpsFolder?>"><?=__COMMON_BTN_MODALRESTORECONFIGDISPLAYFILETXT?></button>
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect actRestoreConfigBackUp" data-file="" data-dir="<?=$backUpsFolder?>"><?=__COMMON_GENERICTEXT_RESTORE?></button>
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" data-modal-color="red" id="modaldeleteConfigFile" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
					<h4 class="modal-title"><?=__COMMON_MODALMSG_MODALDELETECONFIGFILEH4?></h4>
			</div>
			<div class="modal-body">
					<p><?=__COMMON_MODALMSG_MODALDELETECONFIGFILETXT?>: <span class="idTitleRef"></span></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect actDisplayConfigBackUpFile" data-next="kill" data-file="" data-dir="<?=$backUpsFolder?>"><?=__COMMON_BTN_MODALRESTORECONFIGDISPLAYFILETXT?></button>
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect actDeleteConfigBackUp" data-file="" data-dir="<?=$backUpsFolder?>"><?=__COMMON_BTN_DELETEFILETXT?></button>
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
			</div>
		</div>
	</div>
</div>
