<?php
/**
 * DATE UPDATE: 2018-03-15 15:29 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * LANDING BASIC CONFIG
 *
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=__LEVEL_ACCESS_GOD;
$minRoleAdim=1;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>Configuraci&oacute;n b&aacute;sica de la landing</small>';

// MANDATORY PARAMS
$moduleSetting->landingsHistoryLegalTexts=__QUERY_TABLES_PREFIX.'lopd_landings_history_legal_texts';

$moduleSetting->mainTable='landings_site_config';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='id';
$moduleSetting->keyListLabel='id';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->isCanEnabled=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole ? true : false;
$moduleSetting->fieldNameStatusRow='is_enabled';
$moduleSetting->allowNewRecord=true;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->minEnableLevel=__LEVEL_ACCESS_MASTER;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[10,25,50]'; // EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;
$moduleSetting->defaultOrderField=['id'=>'DESC'];
$moduleSetting->allowCopyRecord=true;	// NEW 2018
$moduleSetting->minCopyRecordLevel=__LEVEL_ACCESS_MASTER;	// NEW 2018

/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array(
//		' && activo = '=>0
// EXAMPLE WITH MORE SENTENCES:
//		,'|| ('=>''
//		,'theme = '=>'\'123\''
//		,')'=>''
	)
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode($arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS

$order=0;
//echo $web_url.$adminFolder;
/**
 * LOS CAMPOS DEBEN APARECER POR EL ORDEN AQUI ABAJO
 * DUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id'=>array(
    'formType'=>'hidden'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>0
    ,'label'=>'ID'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
	,'client_id'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>2]
		,'tabularFilter'=>['mode'=>'BBDD','origin'=>__QUERY_TABLES_PREFIX.'landings_customers','tag'=>array('filterType'=>'select')]
		,'required'=>1
		,'fieldTableName'=>__QUERY_TABLES_PREFIX.'landings_customers'
		,'arrFieldsTagSelect'=>array('id_landing','prefix') // PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
		,'includeMainBBDD'=>true
		,'whereSelectField'=>null
		,'orderSelectField'=>'prefix'
    ,'label'=>' Cliente'
    ,'helpText'=>'Cliente al que pertenece la landing'
    ,'visible'=>1
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
	)
  ,'landing_name'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>3,'columnWidth'=>3]
    ,'required'=>1
    ,'label'=>'Nombre landing'
    ,'helpText'=>'Identificador landing'
    ,'helpTextExtend'=>'Indicar el nombre, solo caracteres ingleses, n&uacute;meros y guiones'
    ,'visible'=>1
    ,'placeHolder'=>'Identificador landing'
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'landing_identifier'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>1
    ,'label'=>'Identificador'
    ,'helpText'=>'Identificador landing'
    ,'helpTextExtend'=>'Necesario por compatibilidad, suele ser igual a `Nombre landing`'
    ,'visible'=>1
    ,'placeHolder'=>'Identificador interno'
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'txt_title_site'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>1
    ,'label'=>'T&iacute;tulo del navegador'
    ,'helpText'=>'T&iacute;tulo navegador'
		,'helpTextExtend'=>'texto que identifica la p&aacute;gina abierta en el navegador'
    ,'visible'=>1
    ,'placeHolder'=>'nombre archivo CSS'
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'script_body'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>4,'columnWidth'=>2]
    ,'required'=>0
    ,'label'=>'Script index'
    ,'helpText'=>'Ejemplo: body_001.php'
    ,'helpTextExtend'=>'Indica el nombre del archivo que debe cargarse al abrir la landing'
    ,'visible'=>1
    ,'placeHolder'=>'nombre archivo'
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'footer_cookies_css'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Script CSS cookies'
    ,'helpText'=>'Nombre script Cookies CSS'
		,'helpTextExtend'=>'Si no se especifica, autom&aacute;ticamente se cargar&aacute; `footer_cookies.css`'
    ,'visible'=>1
    ,'placeHolder'=>'archivo CSS para contenidos textos de cookies'
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'privacy_policy_file'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Pol&iacute;tica de privacidad'
    ,'rowsData'=>2
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>6
    ,'helpText'=>'Pol&iacute;tica de privacidad de la landing'
		,'helpTextExtend'=>'Recordar que para longitudes de texto inferiores a 100 caracteres, el sistema utilizar&aacute; este texto como nombre de archivo, ej.: privaci_policy.php'
    ,'visible'=>1
    ,'placeHolder'=>'Pol&iacute;tica de privacidad'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'landing_terms_file'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Aviso legal'
    ,'rowsData'=>2
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>6
    ,'helpText'=>'Aviso legal / condiciones'
		,'helpTextExtend'=>'Recordar que para longitudes de texto inferiores a 100 caracteres, el sistema utilizar&aacute; este texto como nombre de archivo, ej.: condicionescontratacion.php'
    ,'visible'=>1
    ,'placeHolder'=>'Aviso legal'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'customer_data_transfer_file'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Consentimiento env&iacute;o datos'
    ,'rowsData'=>2
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>6
    ,'helpText'=>'Consentimiento env&iacute;o datos a terceros'
		,'helpTextExtend'=>'Recordar que para longitudes de texto inferiores a 100 caracteres, el sistema utilizar&aacute; este texto como nombre de archivo, ej.: condicionescontratacion.php'
    ,'visible'=>1
    ,'placeHolder'=>'Consentimiento env&iacute;o datos a terceros'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'cookies_html_contents'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Texto legal cookies'
    ,'rowsData'=>2
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>6
    ,'helpText'=>'Texto de las cookies del sitio'
		,'helpTextExtend'=>'En el texto de las cookies, colocar el bloque [[[]]] para poder incluir autom&aacute;ticamente el listado din&aacute;mico de las cookies para esta landing'
    ,'visible'=>1
    ,'placeHolder'=>'Aviso legal'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'id_cookie_landing'=>array(
     'formType'=>'select'
    ,'dataType'=>'integer'
    ,'required'=>0
    ,'label'=>'Cookies disponibles para la landing'
    ,'fieldTableName'=>__QUERY_TABLES_PREFIX.'cookies_contents'
    ,'defaultSelectText'=>'Seleccionar'
    ,'excludeMainTable'=>true
		,'sourceTagBbddData'=>(object)['relatedTableName'=>__QUERY_TABLES_PREFIX.'cookies_landings','keyFieldName'=>'id_landing','relatedFieldName'=>'id_cookie_landing','arrFieldsTagSelect'=>array('id_cookie','cookie_name'),'arrRelatedFields'=>array('id_cookie','id_landing'),'whereSelectField'=>' && main.id_cookie != 0','orderSelectField'=>'cookie_name']
    ,'visible'=>1
    ,'selectSearchBox'=>true
    ,'multipleSelect'=>true
    ,'placeHolder'=>'Selecci&oacute;n de respuestas'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'helpText'=>'Seleccionar uno o varios elementos de la lista'
    ,'order'=>++$order
  )
 ,'skip_check_phone_number'=>array(
		'formType'=>'checkbox'
		,'dataType'=>'toggle'
		,'required'=>0
		,'toggleParams'=>array('tsColor'=>'green')
		,'label'=>'Skip check Tlf'
		,'arrValues'=>array(0=>'<span class=c-red>no</span>',1=>'SI')
		,'helpText'=>'Activar / desactivar'
		,'helpTextExtend'=>'Permite omitir el control del n&uacute;mero de tel&eacute;fono por CURL, si se activa se omitir&aacute; la verificaci&oacute;n de la existencia del tel&eacute;fono y ser&aacute; v&aacute;lido cualquier n&uacute;mero'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>2
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
  ,'url_local'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Url local'
    ,'helpText'=>'Campo obsoleto'
    ,'visible'=>1
    ,'placeHolder'=>'campo no m&aacute;s utilizado'
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'is_enabled'=>array(
		'formType'=>'checkbox'
		,'dataType'=>'toggle'
		,'tabularListData'=>['columnOrder'=>5,'columnWidth'=>1]	// PARA QUE NO VISUALICE EN EL LISTADO, PONER EL MISMO NUMERO DEL ULTIMO UTILIZADO
		,'required'=>0
		,'toggleParams'=>array('tsColor'=>'green')
		,'label'=>'Habilitada'
		,'arrValues'=>array(0=>'<span class=c-red>no</span>',1=>'SI')
		,'helpText'=>'Habilitar / deshabilitar'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>4
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  0=>array(
    'groupTextTitle'=>'Gesti&oacute;n de registros'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
	,1=>array(
    'groupTextTitle'=>'Enlaces/Contenidos HTML adicionales de la landing (formulario)'
    ,'additionalInfo'=>'Enlaces externos / HTML adicional que debe aparecer para la landing'
    ,'groupIco'=>'fa fa-file-text'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
  ,2=>array(
    'groupTextTitle'=>'Otras opciones generalmente no utilizadas'
    ,'additionalInfo'=>'Solo utilizadas antes del 2017'
    ,'groupIco'=>'zmdi zmdi-view-compact'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);

