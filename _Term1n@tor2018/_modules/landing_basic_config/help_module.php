<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">

		<ul class="tab-nav text-center fw-nav">
			<li class=""><a href="#tabU1" data-toggle="tab" aria-expanded="false">Este módulo</a></li>
			<li class=""><a href="#tabU2" data-toggle="tab" aria-expanded="false">Notas</a></li>
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tabU1">
					<p>Desglose de las principales funciones del módulo actual.</p>
					<p>Este módulo sirve para efectuar la configuración básica de las todas las landings. Aquí se deben configurar los parámetros más importantes para cargar correctamente las landings y algunos textos complementarios de HTML de textos legales que pueden aparecer en ventanas modales o externas.</p>
					<ol>
						<li class="m-b-10">El campo <b>"Cliente"</b> define a que cliente pertenece la landing actual.</li>
						<li class="m-b-10">El campo <b>"Nombre landing"</b> identifica la misma landing, este dato es el que aparece en otros formularios para este mismo back-office.</li>
						<li class="m-b-10">El campo <b>"Identificador"</b> debe contener solamente caracteres nglosajones y símbolos válidos.</li>
						<li class="m-b-10">El campo <b>"Título del navegador"</b> es el titular que aparece en el navegador cliente.</li>
						<li class="m-b-10">El campo <b>"Script index"</b> ydentifica el script PHP utilizado para dibujar gran parte del HTML de la landing (excluído el formulario de registro, que se define en el módulo <b>"HTML Landings"</b>, donde hay 2 ociones: <i>Standard</i> ó <i>Específico</i>).</li>
						<li class="m-b-10">En el campo <b>"Script CSS cookies"</b> es posible definir clases CSS específicas para los contenidos a mostrar para las cookies.</li>
						<li class="m-b-10">Los campos <b>"Política de privacidad"</b>, <b>"Aviso legal"</b>, <b>"Consentimiento envío datos"</b> y <b>"Texto legal cookies"</b>, permiten incluir contenidos HTML específicos para las modales o páginas externas adicionales. Importante notar que si algunos de estos campos tienen menos de 100 caracteres, se intentará abrir siempre una ventana externa tomando como URL el texto introducido <span class="c-bluegray"><i>(<a href="#tabU2" data-toggle="tab">ver las notas para más detalles</a>)</i></span>.</li>
						<li class="m-b-10">El campo <b>"Cookies disponibles para la landing"</b> permite añadir tantos elementos de este listado que será incluido en el contenido de las cookies donde aparezca la codificación <b>[[[]]]</b>.</li>
						<li class="m-b-10">El campo <b>"skip_check_phone_number"</b> permite omitir el control del número de teléfono por CURL, si se activa se omitirá la verificación de la existencia del teléfono y será válido cualquier número.</li>
						<li class="m-b-10">El campo <b>"url_local"</b> campo obsoleto.</li>
					</ol>
				</div>
				<div class="tab-pane fade f-16" id="tabU2">
						<p>En 2018, para aplicar las nuevas normativas de la LOPD, se han incluído unas clases CSS específicas que sirven para abrir automáticamente las <i>ventanas modales</i> que visualizarán los textos apropiados:
						<ol>
							<li>La clase CSS <b>legal_notices</b> sirve para abrir el contenido definido en el campo <i>Aviso legal</i>.</li>
							<li><b>link_pp</b> identifica la modal que deberá cargar las <i>políticas de privacidad</i> de la landing, definidas en el campo <i>Política de privacidad</i>.</li>
							<li><b>modal_atd</b> es el nombre de clase CSS para cargar la modal que con los textos para los <i>envío de datos a terceros</i> en la landing, en el campo <i>Consentimiento envío datos</i>.</li>
							<li><b>cookie_info</b> para la clase CSS que debe cargar la modal para los <i>contenidos definidos para las cookies</i>, contenido que se edita en el campo <i>Texto legal cookies</i>.</li>
						</ol>
						Muy importante saber que para que funcione correctamente la apertura de la ventana modal en la misma landing, los contenidos para los campos <i>"Aviso legal", "Consentimiento envío datos"</i>&nbsp;&nbsp;y/o <i>"Política de privacidad"</i> deben tener una longitud superior a 100 caracteres y en el parámetro <i>href</i> del TAG HTML del enlace, se debería escribir "#null".
						</p>
						<p>Así mismo, se puede colocar en cualquier lugar del contenido HTML el enlace con la clase CSS <b>"cookie_info"</b> para abrir la modal con los contenidos definidos para las cookies.</p>
				</div>
				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>

			</div>

	</div>
</div>
