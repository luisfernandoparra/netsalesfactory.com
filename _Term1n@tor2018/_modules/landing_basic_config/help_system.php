<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">
		<ul class="tab-nav text-center fw-nav" data-tab-color="green">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Notas específicas para este módulo</a></li>
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p></span>.</p>
					<ol>
						<li class="m-b-10">Si existe el dato, el campo <b>"Script index"</b> referencia el script que deberá cargar la landing para personalizar la disposición del HTML a dibujar en la página.</li>
						<li class="m-b-10">Importante recordar que el archivo CSS se puede cargar independientemente por landing, con la misma regla siempre: de la carpeta "css/" se buscará el archivo <b>"css_<i>XXX</i>.css"</b>, donde <i>XXX</i> es el ID de la landing, ejemplo:<br />&emsp;- <span class="c-bluegray">promociones/<i>carpeta_cliente</i>/css/<b>css_099.css</b></span><br />Por defecto, si no se define un archivo CSS específico para la landing, el sistema buscará el script definido en la variable php <b>"$defaultCSS"</b> existente en el script <b>"/includes/header_tmplt.php"</b>, ejemplo:<br />&emsp;- <span class="c-bluegray">$defaultCSS = '<b>000</b> ';</span><br />&emsp;- el sistema buscará: <span class="c-bluegray">promociones/<i>carpeta_cliente</i>/css/<b>css_000.css</b></span></li>
						<li class="m-b-10">Para el campo de tipo <i>textarea</i>&nbsp&nbsp;<b>"Texto legal cookies"</b> se debe recordar que se ha preparado un automatismo para que en el texto de la modal de las cookies se incluya automáticamente la lista con tabla de las cookies marcadas en el campo desplegable <i>"Cookies disponibles para la landing"</i> donde se aparezca en este campo la cadena <b>[[[]]]</b></li>
					</ol>
				</div>

<!--				<div class="tab-pane fade f-16" id="tab2">
						<p></p>
				</div>


				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>-->

			</div>
	</div>
</div>
