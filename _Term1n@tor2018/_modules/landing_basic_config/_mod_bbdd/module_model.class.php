<?php
/**
 *	CLASE LOCAL
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class ModelLocalModule extends CBBDD
{
	protected $database;
	private $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $defaultOrderField;
	public $keyListLabel;
	public $mainTable;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;


	public function __construct($db_type=null, $db_host='', $db_user='', $db_pass='', $db_name='', $localParams=null, $port='3306')
	{
		$this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
		$this->prefixGtp=__QUERY_TABLES_PREFIX;
		$this->query='';
		$this->mainTable=$this->prefixGtp.$localParams->mainTable;
		$this->listFieldsOutput=''; $this->mainTableFieldsNames=''; $this->defaultWhereSql='';
		$this->defaultOrderField='';
		return;
	}

	/**
	 * STORE LOPD LEGAL MODIFIED COPNTENTS
	 * 
	 * @param int $idRecord
	 * @param string $localTable
	 * @param string $db
	 * @param array $requestData
	 * @return object
	 */
	public function insertLopdLandingHistory($idRecord=0 ,$localTable=null, $db=null, $requestData=array())
	{
		$privacy_policy_txt=str_replace('\'','`',$requestData['privacy_policy_file']);
		$landing_terms_txt=str_replace('\'','`',$requestData['landing_terms_file']);
		$accept_transfer_data_txt=str_replace('\'','`',$requestData['customer_data_transfer_file']);
		$query='INSERT INTO `%s`.`%s` (id_landing, pp_contents, bl_contents, cdt_contents, effective_date) VALUES (%d, \'%s\', \'%s\', \'%s\', NOW()) ';
		$this->query=sprintf($query, $db, $localTable, (int)$idRecord, $privacy_policy_txt, $landing_terms_txt, $accept_transfer_data_txt);
		$res=$this->ejecuta_query($this->query);
		return $this;
	}


	/**
	 * DEVUELVE LA LISTA DE ELEMENTOS ENCONTRADOS PARA LA TABLA ESPECIFICADA (SI OMITIDA = mainTable)
	 *
	 * @param int $idRecord
	 * @param string $localTable
	 * @param array $arrFieldsTagSelect	DEBE CONTENER LOS NOMBRES DE LOS CAMPOS PARA OBTENER LOS DATOS, EL PRIMERO SIEMPRE EL ID DE LA TABLA
	 * @param type $sqlWhere
	 * @param type $fieldOrder
	 * @return array
	 */
	public function getSpecificRecordData($idRecord=0 ,$localTable=null, $arrFieldsTagSelect=null, $sqlWhere=null, $fieldOrder=null)
	{
		$sqlOrder=isset($fieldOrder) ? ' ORDER BY '.$fieldOrder : '';
		$sqlWhere=isset($sqlWhere) ? ' && '.$sqlWhere : '';
		$arrAliasNames=array('id_list', 'text_list');

		if(count($arrFieldsTagSelect))
		{
			$fieldsNames='';
			foreach($arrFieldsTagSelect as $key=>$value)
				$fieldsNames.=$value.' AS '.$arrAliasNames[$key].', ';

			$fieldsNames=substr($fieldsNames,0,-2);
		}

		$localTable=isset($localTable) ? $localTable : $this->commonDb->mainTable;
		$query='SELECT '.$fieldsNames.' FROM `%s`.`%s` WHERE 1 '.$sqlWhere.$sqlOrder;
		$this->query=sprintf($query, $this->commonDb->db, $localTable);
		$this->commonDb->getResultSelectArray($this->query);
    $res=$this->commonDb->tResultadoQuery;

		return $res;
	}

}