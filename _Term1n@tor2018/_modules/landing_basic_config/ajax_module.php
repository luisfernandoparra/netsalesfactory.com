<?php
/**
 * LOCAL AJAX MODULE
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');

ini_set('display_errors',1);
error_reporting($errorReportDefault);
$moduleParams='';

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
//$response['query']=array();

if($hashSecutityScript != __HASH_CONTROL_SCRIPT){
  die(__MSG_ACCESS_ERROR);
}


if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$tmpVars=json_decode(file_get_contents('php://input'), true);	// DECODE JSON DATA

if(count($tmpVars))
  $arraExtVars=$tmpVars;



/**
 * ALMACENA LOS TEXTOS SENSIBLES PARA EL HOISTORICO DE LOPD
 * EN FECHA DE MODIFICACION
 *
 * M.F. 2018.03.07
 */
if(isset($arraExtVars['action']) && $arraExtVars['action'] == 'updateLopdHistoyLegalTexts')
{
	$resAction=false;
	$response['msgTitle']='<h2 class="c-white">ERROR en histórico LOPD</h2>';
	include($dirModAdmin.'mod_bbdd.class.php');
	include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'_mod_bbdd/module_model.class.php');
	$dataLocalModel=new ModelLocalModule($db_type, $db_host, $db_user, $db_pass, $db_name, null, $port);
	$resConnexion=$dataLocalModel->connectDB();

  if(!$dataLocalModel->idConexion->sqlstate)
  {
		$response['msgTitle']='<h2 class="c-white">ERROR DE CONEXI&Oacute;N</h2>';
		$response['errorTxt']='<center><span class="c-white f-16">Ha ocurrido un error al intentar hacer la conexión a la BBDD</span><br /><br />No ha sido posible actualizar el historico LOPD landings.</center>';
		$response['timeWait']=20000;
		$response['success']=$resAction;
    $res=json_encode($response);
    die($res);
  }

	$resAction=$dataLocalModel->insertLopdLandingHistory($arraExtVars['landingId'], $arraExtVars['landingsHistoryLegalTexts'], $db_name, $arraExtVars['db'][key($arraExtVars['db'])]);//[$arraExtVars['moduleAdmin']]

	if(!$resAction)	// ERROR
	{
		$response['errorTxt']='Ha ocurrido un error al intentar actualizar el historico.<br />';
	}
	else
	{
		$response['success']=$resAction->Id ? $resAction->Id : $resAction;
		$response['msgTitle']=null;
		$response['msgText']='<span style=color:#eee;>+ LOPD update OK</span><br />(ID = '.$resAction->Id.')';
	}

	$response['success']=$resAction;
	$res=json_encode($response);
	die($res);
}
