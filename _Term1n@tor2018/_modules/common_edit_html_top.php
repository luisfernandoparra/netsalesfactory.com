<?php
@header('Content-Type: text/html; charset=utf-8');
@session_start();

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

// START LOCAL MODULE LANGUAGE TRANSLACTIONS
if(isset($_SESSION['userAdmin']['pref_']['ref_lang']) && isset($arraExtVars['from']))
{
	$dirLang=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'].'lang/';
	$filLang=$dirLang.'module_'.strtolower($_SESSION['userAdmin']['pref_']['ref_lang']).'.php';

	if(file_exists($filLang))
	{
		include($filLang);
		$wordSep='_'; $tmpWord=null; $tmpValue=null;

		foreach($langTxt as $kL=>$vL)
		{
			$tmpWord=$kL;

			foreach($vL as $nn=>$tmpValue1)
			{
				$tmpWord_2=null;
				if(is_array($tmpValue1))
				{
					$tmpWord_3=null;

					foreach($tmpValue1 as $nv=>$tmpValue2)
					{
						$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
						define('__'.strtoupper($tmpWord_3), $tmpValue2);
					}
				}
				else
				{
					$tmpWord_2=$tmpWord.$wordSep.$nn;
					define('__'.strtoupper($tmpWord_2), $tmpValue1);
				}
			}
		}
	}
}
// END LOCAL MODULE LANGUAGE TRANSLACTIONS

?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php
// START COMMON AND VENDOR CSS & JAVASCRIPT COMMON LIBRARIES
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_comm_vendor_CSS.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_javascript_libraries.php');
// END COMMON AND VENDOR CSS & JAVASCRIPT COMMON LIBRARIES


/**
 * SE RECORRE LA CONFIGURACION DE LOS CAMPOS A CARGAR
 * PARA DETERMINAR SI SON NECESARIAS OTRAS LIBRERIAS
 * CSS / JS ADICIONALES, SEGUN EL TIPO DE DATO A DIBUJAR
 * O EL PARAMETRO MANDATORIO CORRESPONDIENTE
 *
 * TAMBIEN SE DIBUJAN LOS JS ESPECIFICOS NECESARIOS
 * SEGUN LIBRERIAS COMUNES CARGADAS
 */
$htmlSpecificLibraries='';
$dateLoaded=0;
$jsForm=''; $jsFormOnLoad='';
$countTag=0; $htmlWysiwigEditor='';

foreach($moduleSetting->arrFieldsFormModal as $key=>$value)
{
	// LIBRERIAS Y ELEMENTOS NECESARIOS PARA EDITOR TINY WYSIWIG
	if(!$dateLoaded && $moduleSetting->arrFieldsFormModal[$key]['actionsForm']->wysiwigEditor['modal'])
	{
		include_once($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_plugins/whysiwyg_tiny_editor/wysiwig_editor_html.php');
	}

	// LIBRERIAS PARA CAMPOS DE TIPO FECHA /HORA
	if(!$dateLoaded && $moduleSetting->arrFieldsFormModal[$key]['formType'] == 'date')
	{
		$dateLoaded=true;
		$htmlSpecificLibraries.='<link href="'.$web_url.$adminFolder.'/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />';
		$htmlSpecificLibraries.='<script src="'.$web_url.$adminFolder.'/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
	}

  // LIBRERIAS PARA CAMPOS CON SELECT MULTIPLES
	if(isset($moduleSetting->arrFieldsFormModal[$key]['formType']) && $moduleSetting->arrFieldsFormModal[$key]['formType'] == 'select')
	{
		$htmlSpecificLibraries.='<link href="'.$web_url.$adminFolder.'/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />';
		$htmlSpecificLibraries.='<script src="'.$web_url.$adminFolder.'/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>';
		echo'<style>.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn){width:99%!important;}</style>';	// NECESARIO X v.1.11.2 (2016.09.13 M.F.)
	}

  // LIBRERIAS PARA CAMPOS INPUT TIPO FILE
	if(isset($moduleSetting->arrFieldsFormModal[$key]['formType']) && $moduleSetting->arrFieldsFormModal[$key]['formType'] == 'file')
	{
		$htmlSpecificLibraries.='<script src="'.$web_url.$adminFolder.'/vendors/fileinput/fileinput.min.js"></script>';
	}

  // LIBRERIAS PARA CAMPOS INPUT TIPO SLIDER
	if(isset($moduleSetting->arrFieldsFormModal[$key]['dataType']) && $moduleSetting->arrFieldsFormModal[$key]['dataType'] == 'slider')
	{
		$htmlSpecificLibraries.='<link href="'.$web_url.$adminFolder.'/vendors/bower_components/nouislider/distribute/nouislider.min.css" rel="stylesheet" />';
		$htmlSpecificLibraries.='<script src="'.$web_url.$adminFolder.'/vendors/bower_components/nouislider/distribute/nouislider.min.js"></script>';

    /**
     * PARAMETRO PARA DUBUJAR EL OBJETO SLIDER DEPENDIENDO DE UN RANGO DE VALORES
     * DEFINIDOS EN EL MODULO DE CONFIGURACION
     */
		if(isset($value['sliderConf']))	// TAG TIPO INPUT - SLIDER
		{
			$jsForm.='var slider'.$countTag.'=document.getElementById("slider-'.$key.'");';
			$jsForm.='noUiSlider.create(slider'.$countTag.',{';
			$jsForm.='start:0';
			$jsForm.=',range:{"min":['.(int)$value['sliderConf']['startValue'].'],"max":['.(int)$value['sliderConf']['endValue'].']}';
			$jsForm.=',animate:true, animationDuration:300';
			$jsForm.=',connect:"lower"';
			$jsForm.=',step:'.(int)$value['sliderConf']['stepValue'].'';
			$jsForm.=',tooltips:true';
			$jsForm.=',pips:{mode: "count",	values:1,	density:'.(int)($value['sliderConf']['endValue']).'}';
			$jsForm.='});';
			$jsForm.='slider'.$countTag.'.noUiSlider.set($("input[data-ref-name=\'slider-'.$key.'\']").val());';
			$jsForm.='var toSlider'.$countTag.'=$("input[data-ref-name=\'slider-'.$key.'\']");';
			$jsForm.='slider'.$countTag.'.noUiSlider.on("update", function(values, handle){$(toSlider'.$countTag.').val(values[handle])});';
		}
	}

	if(isset($moduleSetting->arrFieldsFormModal[$key]['dataType']) && $moduleSetting->arrFieldsFormModal[$key]['dataType'] == 'toggle')
	{
		if(!isset($moduleSetting->arrFieldsFormModal[$key]['arrValues'][0]))	// FORCE DEFAULT NEGATIVE VALUE IF THIS NOT EXIST
			$moduleSetting->arrFieldsFormModal[$key]['arrValues'][0]=0;

		if(!isset($moduleSetting->arrFieldsFormModal[$key]['arrValues'][1]))	// FORCE DEFAULT POSITIVE VALUE IF THIS NOT EXIST
			$moduleSetting->arrFieldsFormModal[$key]['arrValues'][1]=1;

	}
	$countTag++;
}

echo $htmlSpecificLibraries;
?>

<script type="text/javascript">
var root_path="<?=$web_url.$adminFolder?>/";

function commonRestoreFormElements()
{
	$('#bbddOperation').modal('toggle');
	return;
}

function commonRestoreErrorForm(res,ajaxNative)
{
	var	introText="<?=__COMMON_JSFN_COMMONRESTOREERRORFORMINTROTXT?>"+res.msgPainTxt+"\r\n";
	var moreErrInfo="";

	if(ajaxNative)
	{
		introText="<?=__COMMON_JSFN_COMMONRESTOREERRORAJAXNATIVE?>";
		moreErrInfo=res.status ? "\n (<?=__COMMON_GENERICTEXT_STATUS?> = "+res.status : "";
		moreErrInfo+=res.statusText ? ", "+res.statusText+")" : ")";
	}
	else
	{
		moreErrInfo="\n"+(res.errorText ? res.errorText : "<?=__COMMON_JSFN_UNKNOWNERROR?>");
	}

	swal({
		title: "<?=__COMMON_GENERICTEXT_ERROR?>",
		text: introText+moreErrInfo,
		type: "error",
		showCancelButton: false,
		confirmButtonText: "<?=__COMMON_GENERICTEXT_OKTXT?>",
		closeOnConfirm: false,
		closeOnCancel: false,
		timer:5000,
	},
	function(isConfirm){
      swal("<?=__COMMON_GENERICTEXT_TXTSAVEERROR?>");
      parent.$(".saveButton").attr("disabled",false);
			parent.$(".insertButton").attr("disabled",false);
      parent.$(".enableDisableButton").attr("disabled",false);
      parent.$(".actCancel").attr("disabled",false);
   });

  setTimeout("commonRestoreFormElements();",1500);
	return;
}

$(document).ready(function(){
<?php
if(isset($moduleSetting->disallowUpdate) && $moduleSetting->disallowUpdate)
	echo 'parent.$(".saveButton").attr("disabled",true);';
?>

	/**
	 * LLAMADA PARA EJECURTAR FUNCIONES PERSONALIZADAS
	 * CON POSIBLES PARAMETROS OPCIONALES OPCIONALES
	 */
  $('#moduleMainForm').on("dblclick","input, textarea",function(){
		var tmpArrFunc=$(this).attr("data-func-params");

		if(tmpArrFunc)
		{
			var tmpArrFunc=tmpArrFunc.split(",");

			if(tmpArrFunc.length)
			{
				var tmpFunc=""; // NOMBRE FUNCION PERSONALIZADA, PARAMETRIZADA POR extraFunc
				var tmpParams=new Array();

				$.each(tmpArrFunc, function(n, value){
					if(!n)
						tmpFunc=value;
					else
						tmpParams["param_"+n]=value;
				});

				tmpParams["obj"]=this;
				eval(tmpFunc)(tmpParams);
			}
		}
	});


	$(".actDelete").click(function(e){
		var dataSend={<?=@$outJsParamsModule?>};
		dataSend.idRef=$(this).attr("data-id");
		dataSend.action="deleteRecord";

		$.ajax({
		data:dataSend
		,url:root_path + "ajax/common_admin.php", method: "post", dataType: "json", cache: false, async: true//, contentType: false
		,success:function(response){
			if(!response.success)// && !response.msgTitle
			{
				commonDeleteAction(response,0);
				return false;
			}

			window.parent.commonNotify(response.msgTitle ? response.msgTitle : "<?=__COMMON_NOTIFY_CORRECTOPERATION?>", response.msgText ? "<br />"+response.msgText : "", undefined, "center", undefined, 'success', "animated flipInX", undefined);
			return;
		}
		,error:function(response){
				commonDeleteAction(response,1);
				return false;
			}
		});
	});

	// BUTTON ZOOM IMAGE ACTION
	$(".btn-zoom-image").click(function(x){
			$('.zoom-modal-image .modal-dialog .modal-content').html("<img class='modalZoomImage' src='"+$(this).attr("data-img")+"' width='40%' />");
	});

	// ACCION BOTON ELIMINAR ARCHIVO DE INPUTS TIPO file
	$(".btn-danger.fileinput-exists").click(function(x){
		$('#moduleMainForm').append("<input name='eraseFiles["+$(this).attr("data-table")+"]["+$(this).attr("data-field")+"]' type='hidden' value='"+$(this).attr("data-field")+"' />");
	});

	// CLASE CSS "singleCheck" = PARA ENVIAR SEGUN EL ESTADO DEL CHECK-BOX: 1 CUANDO MARCADO Y 0 CUANDO DESMARCADO
	$(".singleCheck").on("click",function(){
		var fieldToUpdate="db"+this.id.substring(10);

		if(this.checked)
			$("input[name='"+fieldToUpdate+"']").val(1);
		else
			$("input[name='"+fieldToUpdate+"']").val(0);

//console.log(this.checked,") fieldToUpdate = ",fieldToUpdate)
	});

	$(".hideFormGroup").click(function(){	// HIDE / DISPLAY GRUP FORM BLOCKS
		if($("[data-ref~='"+$(this).attr("data-ref")+"'].containerGroup").is(":visible"))
		{
			$("[data-ref~='"+$(this).attr("data-ref")+"'].containerGroup").slideUp();
			$(this).attr("title","<?=__COMMON_EDITMODULE_BTNEXPANDTEXT?>").children("i").removeClass("zmdi-window-minimize c-black").addClass("zmdi-window-maximize c-blue");
		}
		else
		{
			$(this).attr("title","<?=__COMMON_EDITMODULE_BTNCONTRACTTEXT?>").children("i").addClass("zmdi-window-minimize c-black").removeClass("zmdi-window-maximize c-blue");
			$("[data-ref~='"+$(this).attr("data-ref")+"'].containerGroup").slideDown();
		}
	});

	parent.$(".headerAdditionalInfo").html("");

	<?=$jsForm?>

	// ACCIONES COMUNES AL ACABAR DE CARGAR EL IFRAME DE EDICION DEL MODULO ABIERTO
	$(window).load(function(){
<?php
/**
 * START AVISO CUANDO HA OCURRIDO UN ERROR AL CARGAR UN REGISTRO EN EDICION
 * Y NO HA DEVUELTO EL IDENTIFICADOR BUSCADO
 *
 */
if(!isset($resDataForm[$moduleSetting->keyListLabel]) && $arraExtVars['action'] == 'edit')
{
?>
		parent.sweetAlert({
			title:'<?=__COMMON_SWEETALERT_RECORDLOADERRORTITLE?>',
			type:'error',
			html:true,
			text:'<div class="text-left p-20"><?=__COMMON_SWEETALERT_RECORDLOADERRORTXT?></div>',
			animation:true,
			customClass:''
		});
<?php
// END AVISO CUANDO HA OCURRIDO UN ERROR AL CARGAR UN REGISTRO EN EDICION
}

if($arraExtVars['action'] == 'edit') // DIBUJA EL TITULAR DE LA VENTANA MODAL DE EDICION
	echo 'window.parent.$(".idTitleRef").html("<span class=\'color-block p-5\'><i class=\'zmdi zmdi-edit\'>&nbsp;&nbsp;</i>'.__COMMON_EDITMODULE_HEADERTITLE.': <b>'.$arraExtVars['module']['name'].'</b> <span>");';

if($arraExtVars['action'] == 'new') // DIBUJA EL TITULAR PARA LA INSERCION DE UN NUEVO REGISTRO
	echo 'window.parent.$(".idTitleRef").html("<span class=\'color-block p-5\'><i class=\'zmdi zmdi-file\'>&nbsp;&nbsp;</i>'.__COMMON_INSERTMODULE_HEADERTITLE.': <b>'.$arraExtVars['module']['name'].'</b> <span>");';

?>
	});

  $('.viewWysiWig').on('click', function(e){
    e.stopPropagation();
    $("#htmlPlainBox").modal('hide');
    $('#HtmlWysiBox').modal('show');
  });

  $(".modalHtmlWysiBox").click(function(){
		var tmpContent=$("textarea[name='db[<?=$moduleSetting->mainTable?>]["+$(this).attr("data-field-name")+"]']").val();

		if(!tmpContent)
		{
			commonNotify("<b><?=__COMMON_NOTIFY_INFONOCONTENTSTITLE?></b>", "<br><br><?=__COMMON_NOTIFY_INFONOCONTENTS?>", undefined, "center", undefined, 'info', "animated headShake", "bounceOutDown", 3000);
			return false;
		}

		$(".idTitleWysiEdit").html("<?=__COMMON_MODALMSG_IDTITLEWYSIEDITH4?>");
		$(".headerAdditionalInfo").html('"<b>'+$(this).parent().children(0).html()+'</b>"');//$(el).attr("data-title-header")
    $("#tmpWysiView").html(tmpContent);
  });


  $(".modalHtmlPlainBox").click(function(el){
		$(".idTitleHtmlEdit").html("<?=__COMMON_MODALMSG_TITLEWYSIEDITSTDH4?>");//$(this).attr("data-field-name")
		$(".headerAdditionalInfo").html('"<b>'+$(this).parent().children(0).html()+'</b>"');//$(el).attr("data-title-header")
		$(".updateHtmlButton").attr("data-tmp-field-name", $(this).attr("data-field-name"))
		$("textarea[name='tmpDataEdit']").val($("textarea[name='db[<?=$moduleSetting->mainTable?>]["+$(this).attr("data-field-name")+"]']").val());
  });

  $(".updateHtmlButton").click(function(el){
		var tmpTxt=$("textarea[name='tmpDataEdit']").val();
		$("textarea[name='db[<?=$moduleSetting->mainTable?>]["+$(this).attr("data-tmp-field-name")+"]']").text(tmpTxt);
		$("textarea[name='db[<?=$moduleSetting->mainTable?>]["+$(this).attr("data-tmp-field-name")+"]']").val(tmpTxt);
		$("textarea[name='tmpDataEdit']").val("<?=__COMMON_UPDATING?>.....");
		$('#htmlPlainBox').modal('toggle');
  });

  $(".htmlEditNotes").click(function(el){
		swal({
			title: "<?=__COMMON_BTNSWALTITLE_HTMLEDITNOTES?>",
			text: "<?=__COMMON_BTNSWALCONTENT_HTMLEDITNOTES?>",
			type: "info",
			confirmButtonText: "cerrar"
		});
  });

});

/**
 * VENTANA MODAL PARA TRADUCCIONES (COMUN PARA TODOS LOS MODULOS)
 *
 *	CON arrParams["obj"] SE ACCEDE A CUALQUIER PROPIEDAD DEL CAMPO MANEJADO
 *
 * @param {array} arrParams
 * @returns {none}
 */
function modalFieldTransaltions(arrParams)
{
	var currFieldValue=null;
	var tmpTagType=arrParams["obj"].nodeName;
	var tmpParentNodeElement=arrParams["obj"].parentElement;
	var tmpParentLabel=tmpParentNodeElement.getElementsByTagName("label");
	var tmpParentLabelTxt=$(tmpParentLabel).text();
	var tmpPos=tmpParentLabelTxt.indexOf("(+)");
	tmpParentLabelTxt=tmpParentLabelTxt.substring(0,tmpPos);

	if(tmpTagType == "INPUT")
		currFieldValue=$("input[name='"+arrParams["obj"].name+"']").val();
	else
	{
		$(".currentIcon").html("");	// VACIA PREVIOS CONTENIDOS X EVITAR ERRORES

		tmpParentLabelTxt=$(tmpParentLabel).text();
		tmpPos=tmpParentLabelTxt.indexOf("(+)");
		tmpParentLabelTxt=tmpParentLabelTxt.substring(0,tmpPos);

		if(tmpParentLabelTxt)
			$(".currentIcon").html("<b>"+tmpParentLabelTxt+"</b>.");
	}


	$("#modalFieldTransaltions").modal();

	if(currFieldValue)
		$(".currentIcon").html((tmpParentLabelTxt ? "<i>"+tmpParentLabelTxt+"</i>" : "")+" (<?=__COMMON_AJAXMSG_CURRENTSELECTEDTXT?>: <b>"+currFieldValue+"</b>).");

	$("#ifrFieldTranslations").attr("src","../common_modal_transations.php?elementName="+arrParams["obj"].name+"&currentValue="+currFieldValue+"&idElement=<?=isset($_REQUEST['id']) ? $_REQUEST['id'] : ''?>");
  return;
}


</script>

<?php

//echo '<pre>modules_admin:<p>';print_r($resDataForm);echo '</pre>action='.$arraExtVars['action'];//die();
?>

<div class="modal fade zoom-modal-image" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content text-center modalZoomImage" style="background:transparent;" title="<?=__COMMON_MODALMSG_TITLEESCCLOSE?>"></div>
	</div>
</div>

<div class="modal fade" data-modal-color="red" id="modalSubmitError" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><i><?=__COMMON_MODALMSG_SAVETITLEERROR?></i></h4>
			</div>
			<div class="modal-body errorSaveMessage">
					<i><?=__COMMON_GENERICTEXT_PROCESSING?>...<span class="idTitleRef"></span></i>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="htmlPlainBox" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="dynamic" data-keyboard="true">
	<div class="modal-dialog modal-HTML-edit">
		<div class="modal-content">
			<div class="modal-header commonHeaderEdit">
				<h4 class="modal-title" style='color:#333;width:96%!important;'><span class="idTitleHtmlEdit"></span><span class="headerAdditionalInfo"></span></h4>
				<div class="fltBtnClose">
					<button class='btn bgm-gray btn-icon waves-effect waves-circle' data-dismiss='modal'><i class='zmdi zmdi-close'></i></button>
				</div>
			</div>

			<div class="modal-body" style="overflow:auto;max-height:200px;">
				<textarea name="tmpDataEdit" id="tmpDataEdit" class="directEditField p-10" style="max-height:100hv;width:100%;" rows="18"><?=__COMMON_GENERICTEXT_RETRIEVINGDATA?>...</textarea>
			</div>

			<div class="modal-footer">
					<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> htmlEditNotes" data-tmp-field-name=""><i class='zmdi zmdi-info-outline'></i> <?=__COMMON_GENERICTEXT_NOTES?></button>
					<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> updateHtmlButton" data-tmp-field-name=""><?=__COMMON_GENERICTEXT_UPDATE?></button>
					<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> " data-dismiss="modal"><?=__COMMON_BTN_CANCEL?></button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade p-t-10 p-l-10" id="htmlWysiBox" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="dynamic" data-keyboard="true">
	<div class="modal-lg " style="display:inline-table;width:auto!important;">
		<div class="modal-content">
			<div class="modal-header bgm-gray">
				<h4 class="modal-title"><span class="idTitleWysiEdit"></span><span class="headerAdditionalInfo"></span></h4>
				<div class="fltBtnClose">
					<button class='btn bgm-gray btn-icon waves-effect waves-circle' data-dismiss='modal'><i class='zmdi zmdi-close'></i></button>
				</div>
			</div>

			<div class="modal-body" style="overflow:auto;max-height:550px;min-width:90%!important;">
				<div id="tmpWysiView" class="p-10" style="max-height:350hv;min-width:90%!important;"><i><?=__COMMON_GENERICTEXT_HTMLRENDERING?>...</i></div>
			</div>

			<div class="modal-footer">
					<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> " data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modalFieldTransaltions" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-xs">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?=__COMMON_MODALMSG_MODALFIELDTRANSALTIONSH4?> <span class="currentIcon"></span></h4>
			</div>
			<div class="modal-body">
				<iframe style='height:60vh;width:101.3%;' class="" id='ifrFieldTranslations' name='ifrFieldTranslations' src='#null' frameborder='0' marginheight='0' marginwidth='0' scrolling='YES' width='100%'></iframe>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
			</div>
		</div>
	</div>
</div>

<?php
echo $htmlWysiwigEditor;