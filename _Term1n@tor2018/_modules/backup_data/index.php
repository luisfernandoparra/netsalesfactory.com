<?php
/**
 * MAIN MODULE - admin users
 * DEFAULT ACTION: list record data
 *
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
if($hashSecutityScript != __HASH_CONTROL_SCRIPT) die(__MSG_ACCESS_ERROR);

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$modeLoadConfig=1;
$folderCommonModules=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/';

$provenience=$_SERVER['HTTP_REFERER'];
$modNameFrom=strpos($provenience,'_modules/')+9;
$modName=substr($provenience, $modNameFrom);
$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));

$localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'_mod_bbdd/';
$localModuleFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'];

include('config_module.php');

if(!isset($moduleSetting))
{
  echo '<br /><br /><div class="p-20 m-20 f-16 alert alert-danger"><center><span class="zmdi zmdi-info f-20"> '.__COMMON_GENERICTEXT_ERROR.'</span><br />'.__COMMON_GENERICTEXT_ERROCONFIGMODULESTART.' <b>'.@$modName.'</b>.</center><br /><br />'.__COMMON_GENERICTEXT_ERROCONFIGMODULEEND.'.</div><br /><br />';
  $moduleSetting=array();
	die();
}

$msgModuleError=null; $bbddBBDD=null;

/**
 * RESTORE CONFIG FILES DISPLAY BLOCKS
 */
if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD )	// GOD ACCESS LEVEL
	include($folderCommonModules.'common_restore_modules.php');

include($dirModAdmin.'mod_common_lists.class.php');
$bbdd=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name, null, $port);
$resConnexion=$bbdd->connectDB();

$bbdd->current=1;
$bbdd->rowCount=1000;
$bbdd->mainTable=$moduleSetting->mainTable;
$bbdd->fieldNames=@$moduleSetting->fieldNames;
$bbdd->listFieldsOutput=@$moduleSetting->listFieldsOutput;
$bbdd->defaultWhereSql=$moduleSetting->defaultWhereSql;
$bbdd->skipBdDefault=$moduleSetting->skipBdDefault;
$bbdd->moduleFieldsStructure=[];	// NECESARIO INICIALIZAR ESTE ARRAY PARA COMPATIBILIZAR LA CLASE UTILIZADA NORMALMENTE PARA LOS LISTADOS
$objData=$bbdd->getRowsDataList();  // CARGA DE LOS DATOS CON TODAS LAS TABLAS DE LA BBDD ACTUAL

if(count($objData))
{
	include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'local_class.php');
	$localHtml=new MethodsLocalModule();
	$localHtml->buildHtml($objData, $moduleSetting);
}

?>

<script type="text/javascript">
<!--

$(document).ready(function(){
	var tmpHeight=$("#mCSB_1").height()-140;	// ALTO MODAL DE INSERCION NUEVO REGISTRO

	// MARCAR/DESMARCAR TODAS LAS TABLAS PRESENTES A GESTIONAR
	$("input[name='checkAll']").click(function(){
		var tmpStatus=this.checked;

		$('input[class*="checkTable"], input[data-paramCheck=1]').each(function(){
			if(tmpStatus)
				this.checked="checked";
			else
				this.checked=false;
		});
	});

	// MARCAR/DESMARCAR TODAS LAS TABLAS DE SISTEMA
	$("input[name='checkSystemTbl']").click(function(){
		var tmpStatus=this.checked;

		$('input[data-system="isSystem"]').each(function(){
			if(tmpStatus)
				this.checked="checked";
			else
				this.checked=false;
		});
	});

	// MARCAR/DESMARCAR TABLAS DEPRECATED
	$("input[name='deprecatedTables']").click(function(){
		var tmpStatus=this.checked;

		$('input[data-deprecated="isDeprecated"]').each(function(){
			if(tmpStatus)
				this.checked="checked";
			else
				this.checked=false;
		});
	});

	// MARCAR/DESMARCAR TABLAS MAS GRANDES DE N REGISTROS
	$("input[name='tooRows']").click(function(){
		var tmpStatus=this.checked;

		$('input[data-tooRows="1"]').each(function(){
			if(tmpStatus)
				this.checked="checked";
			else
				this.checked=false;
		});
	});

	$("body").on("click",".btnDownload",function(){
    window.open(root_path+"_backups/"+$(this).attr("data-file"), "_blank","menubar=1,toolbar=0,scrollbars=1")
	});

	$(".clearIframe").click(function(){
		$("iframe[name=ifrBackup]").attr("src","<?=$arraExtVars['moduleAdmin']?>iframe_module.php?action=clear");
	});

	// EJECUTAR EL BACK-UP PARA LAS TABLAS SELECCIONADAS
	$(".backupStart").click(function(){
		var numTablesChecked=0;
		var error=false; var swalTitle=""; var swalMsg=""; var swalType="";

		$('input[class*="checkTable"]').each(function(index, value){
			numTablesChecked+=value.checked ? 1 : 0;
		});

		if($("select[name=backUpMode]").val() == 0)
		{
			swalTitle="ATENCIÓN";
			swalMsg="Antes de proseguir, debes seleccionar un modo de back-up";
			swalType="warning";
			error=true;
		}

		if(!numTablesChecked)
		{
			swalTitle="ATENCIÓN";
			swalMsg="Debes marcar por lo menos una tabla, en caso contrario es inútil ejecutar esta tarea!";
			swalType="warning";
			error=true;
		}

		if(error)
		{
			swal({
				title:swalTitle,
				text:swalMsg,
				type:swalType,
				showCancelButton:false,
				confirmButtonText:"cerrar",
				closeOnConfirm:false,
				closeOnCancel:false,
				confirmButtonColor:"#F44336",
				timer:10000
			});
			return false;
		}

		$("input[name=num_tables]").val(numTablesChecked);
		var dataSend=$('#moduleMainForm').serialize();
//console.log("==>",dataSend);return false;

		$("#backUpExecuting").modal();
		$("iframe[name=ifrBackup]").attr("src","<?=$arraExtVars['moduleAdmin']?>iframe_module.php?"+dataSend);
		$("#tagTitleModal").html("<span class='c-red bgm-white p-l-10 p-r-10'><i class=\'zmdi zmdi-hourglass-alt\'></i> <em>Back-up en ejecución...</em></span>");
		$("#tagWaitText").html("Por favor, espera hasta que finalice el proceso");
		$("#tagLoader").show();
return false;

		$.ajax({
			data:dataSend
			,url:root_path+"<?=$arraExtVars['moduleAdmin']?>ajax_module.php?", method: "post", dataType: "json", cache: false, async: true
			,success:function(response){
				$("#backUpExecuting").modal('toggle');

				if(!response.success)// && !response.msgTitle
				{
					window.parent.commonNotify(response.msgTitle ? response.msgTitle : "Operación correcta", response.msgText ? "<br />"+response.msgText : "", undefined, "center", undefined, 'danger', "animated flipInX", "", response.timeWait);
				}
				else
					swal(response.msgTitle,response.msgText,"success");
				
				return false;
			}
			,error:function(response){
				$("#backUpExecuting").modal('toggle');
				swal("ERROR EN LA COPIA","Error grave, AVISAR A SISTEMAS","error");
				return false;
			}
		});
    return false;
	});

});

-->
</script>
<form action="" method='post' id='moduleMainForm' name='moduleMainForm' onsubmit='' enctype='multipart/form-data'>
	<input type="hidden" name="random" value="<?=rand(0,10000)?>" />
	<input type="hidden" name="action" value="goBackUp" />
	<input type="hidden" name="num_tables" value="0" />
	<input type="hidden" name="moduleAdmin" value="<?=$arraExtVars['moduleAdmin']?>" />
	<div class="card-header">
		<h2>Módulo de <b><?=$arraExtVars['modLabel']?></b>
			<?=@$moduleSetting->moduleCommenHeader?>
		</h2>
		Se realizará la copia de seguridad solamente a las tablas que estén marcadas (las cifras corresponden al número de registros de cada tabla).
	</div>

	<div class="card bs-item z-depth-1 p-15 m-l-25 m-r-25">
		<div class="row">
		<span class="p-l-25">Opciones disponibles:</span><br /><br />
		<div class="card-header">
			<div class="text-left pull-left checkbox m-5 c-black p-r-20">
				<label>Marcar todas
					<input type="checkbox" name="checkAll" />
					<i class="input-helper"></i>
				</label>
			</div>
			<div class="text-left pull-left checkbox m-5 c-black">
				<label class="c-purple">Tablas de sistema
					<input type="checkbox" name="checkSystemTbl" data-paramCheck="1" />
					<i class="input-helper"></i>
				</label>
			</div>

			<div class="text-left pull-left checkbox m-5 c-black">
				<label class="c-gray">Deprecated
					<input type="checkbox" name="deprecatedTables" data-paramCheck="1" />
					<i class="input-helper"></i>
				</label>
			</div>

			<div class="text-left pull-left checkbox m-5 c-black">
				<label class="c-gray">>  <?=$moduleSetting->tooRowsPerTable?> reg.
					<input type="checkbox" name="tooRows" data-paramCheck="1" />
					<i class="input-helper"></i>
				</label>
			</div>

			<div class="text-left pull-left checkbox m-5 c-black">
				<label class="c-gray">Descargar
					<input type="checkbox" name="fileDownload" />
					<i class="input-helper"></i>
				</label>
			</div>

			<div class="pull-left col-lg-2 checkbox-inline">
				<div class="select m-0 c-black">
					<select name="backUpMode" class="form-control">
						<option value="0">Modo de back-up:</option>
						<option value="bbdd">Datos+estructura</option>
						<option value="structure">Solo la estructura</option>
						<option value="data">Solo los datos</option>
					</select>
				</div>
			</div>

			<div class="pull-left col-lg-2 checkbox-inline">
				<button class="btn bgm-bluegray btn-sm waves-effect backupStart">Ejecutar el Back-up</button>
			</div>

		</div>
	</div>
</div>

<!-- START DATA CONTENTS -->
<div class="moduleMainContent card w-100 p-l-20 m-0">
	<div class="row">
<?php
/**
 * HTML DE TODAS LAS TABLAS DE LA BBDD ACTUAL
 */
echo $localHtml->html;

?>
	</div>
</div>
<!-- END DATA LISTING -->
</form>

<!-- START HTML LOCAL MODULE/TAGS/ELEMENTS --->
<div class="modal fade" data-modal-color="blue" data-keyboard="false" id="backUpExecuting" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="tagTitleModal"></h4>
			</div>
      <div class="modal-body contentProcess" style="height:50vh!important">
				<iframe name="ifrBackup" src="#null" class="w-full row" frameborder="0" marginheight="0" marginwidth="0" width="107%" height="100%"></iframe>
			</div>
			<div class="modal-footer">
				<div class="preloader pull-left" id="tagLoader">
					<svg class="pl-circular" viewBox="25 25 50 50">
						<circle class="plc-path" cx="50" cy="50" r="20"></circle>
					</svg>
				</div>
				<div class="pull-left c-white m-t-10 m-l-10" id="tagWaitText">...</div>
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect clearIframe" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<?php
// COMMON HTML CONTENTS
if($_SESSION['userAdmin']['roleId'] > 2 )	// MINIMUN ACCESS LEVEL
	@include($folderCommonModules.'common_html_contents_all.php');

if($_SESSION['userAdmin']['roleId'] > 7)	// INTERMEDIATE ACCESS LEVEL
	@include($folderCommonModules.'common_html_contents_role_8.php');

if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD )	// GOD ACCESS LEVEL
	@include($folderCommonModules.'common_html_contents_role_10.php');

?>
<!-- END HTML LOCAL MODULE/TAGS/ELEMENTS --->
