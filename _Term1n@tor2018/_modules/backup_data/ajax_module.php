<?php
/**
 * SPECIFIC SCRIPT ATCIONS
 *
 * AJAX RESPONSE ACTIONS ==> PARAMETRO ´action` (COMUN PARA TODAS LAS ACCIONES)
 *
 */
header('Content-Type: text/html; charset=utf-8');
@session_start();
$response=array();
$response['successStatus']=false;
$response['success']=false;
$response['timeWait']=0;

//	START prevent direct file access or lost work session
if(!isset($_SERVER['HTTP_REFERER']) && basename($_SERVER['SCRIPT_NAME']) != 'index.php' || !$_SESSION['userAdmin'])
{
  $response['msgPainTxt']="\nHUY !".@$arraExtVars['idRef'];
  $response['errorText']='<br />Parece que usted no deber&iacutea estar aqu&iacute';
	$response['msgTitle']='<h2 class="c-white">ERROR</h2>';
	$response['msgText']='Parece que <u>has perdido la sesi&oacute;n de trabajo</u> que ten&iacute;as abierta <span class="c-red bgm-white"></span>';

  if(@$_SESSION['userAdmin'] && isset($_SERVER['HTTP_REFERER'])) // SE HA PERDIDO O O NO SE TIENE SESION DE TRABAJO
  {
    $response['msgText']='<br />Parece que <u>has perdido la sesi&oacute;n de trabajo</u> que ten&iacute;as abierta.<br /><br /><p class="text-center p-5"><button class="btn bgm-orange waves-effect p-20 w-100 logoutAll">Reinicia ahora</button></p>';
    $response['timeWait']=15000;
  }
	$response['success']=0;

  $response['successStatus']=$response['successStatus'] ? $response['successStatus'] : 'danger';
  $response['timeWait']=$response['timeWait'] ? $response['timeWait'] : 20000;
  $response['success']=false;
	$res=json_encode($response);
	die($res);
}	//	END prevent direct file access or lost work session

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_REQUEST;

@include('../../../conf/config_web.php');
include($dirModulesAdmin.$arraExtVars['moduleAdmin'].'config_module.php');
//echo '-->'.$moduleSetting->prefixTbl;die();
if($arraExtVars['action'] == 'goBackUp')
{
	$prefijoNombreTabla='db_';
	$response['successStatus']=false;
	$response['success']=false;
	$response['msgTitle']='<h2 class="c-white">BACKUP ERROR</h2>';
	$response['timeWait']=$response['timeWait'] ? $response['timeWait'] : 10000;
	$response['msgText']='Ha ocurrido un <u>error en el proceso de back-up</u>&nbsp;&nbsp;<span class="c-red bgm-white">&nbsp;('.date('H:i:s').')&nbsp;</span>';

	if(isset($arraExtVars['num_tables']) && $arraExtVars['num_tables'] == 1)	// SOLO UNA TABLA
	{
		$prefijoNombreTabla=str_replace(' ','_',$arraExtVars['tables'][0]);
//		$prefijoNombreTabla=ucwords($prefijoNombreTabla);
//		$prefijoNombreTabla=str_replace(' ','',$prefijoNombreTabla);
		$prefijoNombreTabla=$prefijoNombreTabla.'_DB_';
	}

  $tmpPhpV=substr(PHP_VERSION,0,3);
	$backup_file=$prefijoNombreTabla.strtolower($db_name).'-'.strtoupper($arraExtVars['backUpMode']).'-'.$tmpPhpV.'-'.date('Y.m.d-Hi').'.sql';
	$fp=@fopen($pathBackupFiles.$backup_file,'w');

	if(empty($fp))	// NO EXISTE O NO SE ACCEDE A LA CARPETA PARA EL BACK-UP
	{
		$response['msgTitle']='<h2 class="c-white">BACKUP FILE ERROR</h2>';
		$response['timeWait']=30000;
		$response['msgText']='<br /><center><span class="p-25 f-12 c-yellow">&nbsp;--->&nbsp;ERROR AL EJECUTAR LA TAREA&nbsp;<---&nbsp;</span><br /><br />Parece que no se puede escribir en la carpeta de <br /><br /><h4>'.(substr($pathBackupFiles,strpos($pathBackupFiles,'/')+1,strrpos($pathBackupFiles,'/')-3)).'</h4><br /><br /><b>no existe o no tiene los permisos adecuados para crear el archivo "<u>'.$backup_file.'</u>"</b>.<br /><br />Para poder efectuar el back-up deseado, primero ha de solucionar este problema.</center>';
		fclose($fp);
//		unset($_SESSION['nuevaBBDD']);
		$res=json_encode($response);
		die($res);
	}

	include('../../../conf/config.misc.php');
	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModulesAdmin.$arraExtVars['moduleAdmin'].'_mod_bbdd/module_model.class.php');
	$data=new ModelLocalModule($db_type, $db_host, $db_user, $db_pass, $db_name,null,$port);
	$resConnexion=$data->connectDB();

  if(!$data->idConexion->sqlstate)
  {
		$response['msgTitle']='<h2 class="c-white">ERROR DE CONEXI&Oacute;N</h2>';
		$response['msgText']='<center><span class="c-white f-16">Ha ocurrido un error al intentar hacer la conexión a la BBDD</span><br /><br />Para poder efectuar el back-up deseado, primero ha de solucionar este problema.</center>';
		$response['timeWait']=10000;
    $res=json_encode($response);
    die($res);
  }


	$schema='# '.@$_SESSION['nombre_site']."\n" .
'#' . "\n" .
'# Database Backup para la BBDD: '.$db_name."\n" .
'# Copia realizada por: '.$_SESSION['userAdmin']['name'].' '.$_SESSION['userAdmin']['surname']."\n" .
'#' . "\n".
'# Sobre PHP v.'.PHP_VERSION."\n".
'#' . "\n".
'# Fecha de ejecucion: '.date('D-m-y H:i')."\n\n";
	fputs($fp,$schema);
  $resFile=0;

//	$res=$data->backUpSql($arraExtVars['tables'], $arraExtVars['backUpMode']);
  set_time_limit(200);
  foreach($arraExtVars['tables'] as $key=>$table)
  {
    $res=$data->backUpSql($table, $arraExtVars['backUpMode']);
    $resFile+=fputs($fp, $res);
    flush();
  }
  set_time_limit(30);
	
	fclose($fp);

  if($res)  // COPIA FINALIZADA CORRECTAMENTE
  {
    $response['successStatus']=true;
    $response['success']=true;
    $response['timeWait']=2000;
    $response['msgTitle']='COPIA CORRECTA';
    $response['msgText']='Archivo creado: '.$backup_file;
  }
	$res=json_encode($response);
	die($res);
}

echo "\n";print_r($arraExtVars);die('END AJAX FILE !!!!!!!!!!!!!!!!!!!');
