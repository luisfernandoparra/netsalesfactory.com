<?php
/**
 *	CLASE CON LAS FUNCIONES DE BBDD NECESARIAS PARA CREAR LAS LISTAS DE LA BBSDD STANDARD DE GTP6
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();
class ModelLocalModule extends CBBDD
{
	protected $database;
	private $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $defaultOrderField;
	public $keyListLabel;
	public $mainTable;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;

	//public function __construct($db_type, $db_host, $db_user, $db_pass, $db_name, $localParams=null, $port='')
	public function __construct($db_type=null, $db_host='', $db_user='', $db_pass='', $db_name='', $localParams=null, $port=1312)
	{
		$this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, null, $port);
//echo '<pre>';print_r($localParams);
		$this->prefixGtp=isset($localParams['prefixTbl']) ? $localParams['prefixTbl'] : __QUERY_TABLES_PREFIX;
		$this->query='';
		$this->skipBdDefault='';
		$this->listFieldsOutput=''; $this->mainTableFieldsNames=''; $this->defaultWhereSql='';
		$this->defaultOrderField=''; $this->keyListLabel=null;
		return;
	}

  private function tep_not_null($value)
	{
    if (is_array($value))
		{
      if (sizeof($value) > 0)
        return true;
			else
        return false;
    }
		else
		{
      if((is_string($value) || is_int($value)) && ($value != '') && ($value != 'NULL') && (strlen(trim($value)) > 0))
        return true;
			else
        return false;
    }
  }

	public function backUpSql($table=null, $backUpMode=null)
	{
		if(!$table)
			return false;

		$schema=''; $tmpRes=''; $tmpSchema='';
    $schema= 'DROP TABLE IF EXISTS `'.$table.'`;'."\n".'CREATE TABLE `'.$table.'` ('."\n";

    $table_list = array();
    $query='SHOW FIELDS FROM `'.$table.'`';

    $this->getResultSelectArray($query);
    $this->mainTableFieldsNames=$this->tResultadoQuery;

    foreach($this->mainTableFieldsNames as $fields)
    {
      $table_list[]=$fields['Field'];
      $schema.='  '.$fields['Field'].' '.$fields['Type'];

      $brakets="'";if($fields['Default']=='CURRENT_TIMESTAMP') $brakets='';	// <====== RATTOPPO!!!!

      if(strlen($fields['Default']) > 0) $schema.=' default '.$brakets.$fields['Default'].$brakets.'';
      if($fields['Null'] != 'YES') $schema.=' not null';
      if(isset($fields['Extra'])) $schema.=' '.$fields['Extra'];
      $schema.=','."\n";
    }

    //$schema=@ereg_replace(",\n$",'',$schema);
    $schema=preg_replace('/,\n$/i', '', $schema);
    $index=@array();

    $query='show keys from `'.$table.'`';
    $this->getResultSelectArray($query);
    $this->mainTableFieldsNames=$this->tResultadoQuery;

    foreach($this->mainTableFieldsNames as $keys)
    {
      $kname=$keys['Key_name'];

      if(!isset($index[$kname]))
        $index[$kname]=@array('unique' => !$keys['Non_unique'], 'fulltext' => ($keys['Index_type'] == 'FULLTEXT' ? '1' : '0'), 'columns' => array());

      $index[$kname]['columns'][]=$keys['Column_name'];
    }

    while(list($kname, $info) = each($index))
    {
      $schema.=','."\n";
      $columns=implode($info['columns'],', ');

      if($kname == 'PRIMARY')
        $schema.='  PRIMARY KEY ('.$columns.')';
      elseif($info['fulltext'] == '1')
        $schema.='  FULLTEXT '.$kname.' ('.$columns.')';
      elseif($info['unique'])
          $schema.='  UNIQUE '.$kname.' ('.$columns.')';
        else
          $schema.='  KEY '.$kname.' ('.$columns.')';
      }
    $schema.="\n".');'."\n\n";

		if(in_array($table,$_REQUEST['tables']) && ($backUpMode == 'bbdd' || $backUpMode == 'data'))	// SE EFECTUA EL BACK-UP DE DATOS PARA TODAS LAS TABLAS SELECCIONADAS PREVIAMENTE
    {
//try
//{
      $query="SELECT ".implode(',',$table_list)." FROM `".$table.'`';
      $this->getResultSelectArray($query);
      $this->mainTableFieldsNames=$this->tResultadoQuery;
      $tmpSchema='';

      foreach($this->mainTableFieldsNames as $rows)
      {
        $tmpSchema.='INSERT INTO `'.$table.'` ('.implode(', ',$table_list).') VALUES (';

        reset($table_list);
        while(list(,$i) = each($table_list))
        {
          if (!isset($rows[$i]))
            $tmpSchema.='NULL, ';
          elseif($this->tep_not_null($rows[$i]))
          {
            $row=addslashes($rows[$i]);
            $row=@preg_replace('/\n#/i', "\n".'\#', $row);
            //$row=@ereg_replace("\n#", "\n".'\#', $row);
            $tmpSchema.='\''.$row.'\', ';
          }
          else
            $tmpSchema.='\'\', ';
          }

        $tmpSchema=@preg_replace('/, $/i', '', $tmpSchema).');'."\n";
        //$tmpSchema=@ereg_replace(', $','',$tmpSchema).');'."\n";
        }
//} catch (TableAException $e){
//register_shutdown_function('errTimeOut');
////return 0;
//  throw $e;
//}catch(Exception $ex){
//register_shutdown_function('errTimeOut');
//echo'<hr>ERROR POR TIEMPO EXCESIVO EN CREACIÓN DE DATOS<hr>';
//die();
//}


      }

    $tmpRes.=(($backUpMode !== 'data') ? $schema : '') .$tmpSchema."\n";

//echo $query."\n";
//echo "\n";
//print_r($this->mainTableFieldsNames);
//echo $schema;die("\n---");
		return($tmpRes);
	}

}