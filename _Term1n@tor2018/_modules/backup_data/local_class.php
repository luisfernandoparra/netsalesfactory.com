<?php
/**
 * HTML PARA EL BODY DEL MODULO DE BACK-UP
 * CON TODAS LAS TABLAS DE LA BBDD ACTUAL
 *
 * M.F. v1.0
 * 2016.11.25
 *
 */
@session_start();
class MethodsLocalModule
{
	public $html;

	public function __construct()
	{
		$this->html='';
		return;
	}

	/**
	 * CONSTRUYE EL HTML REPRESENTANDO TODAS LAS TABLAS DE LA BBDD
	 * DISTINGUIENDO ENTRE "deprecated" y "system"
	 *
	 * @param array $data
	 * @param object $params
	 * @return none
	 */
	public function buildHtml($data=null, $params=null)
	{
		$this->minRowsAutoCheck=1000;
		if(!count($data))
		{
			$this->html='<div class="card-header card-padding"><div class="row f-500 c-black m-b-20 p-b-25 text-center"><div class="text-center"><div class="col-xs-6 p-25 bs-item z-depth-1">
				No se han encontrados datos para generar los contenidos esperados</div></div></div></div>';
			return false;
		}
		$this->html.='<div class="card-header card-padding row">';

		foreach($data as $key=>$value)
		{
			$systemTable=strstr(strtolower($value['TABLE_NAME']),'system_');
			$tooRowsPerTable=$value['TABLE_ROWS'] >= $params->tooRowsPerTable;
			$deprecatedTable=strstr(strtolower($value['TABLE_NAME']),'deprecated');
			$cssDeprecated=$deprecatedTable ? ' style="text-decoration:line-through!important;"' : '';

			$this->html.='<div class="col-lg-3 checkbox m-5">';
			$this->html.='<label class="'.($value['TABLE_ROWS'] > $this->minRowsAutoCheck ? 'c-red"' : ($systemTable ? 'c-purple' : 'c-blue')).'" '.$cssDeprecated.'>';
			$this->html.='<input type="checkbox" ';
			$this->html.=((!$systemTable && $value['TABLE_ROWS'] < $this->minRowsAutoCheck && !$tooRowsPerTable) ? ($deprecatedTable ? '' : 'checked="checked"') : '');
			$this->html.=' value="'.$value['TABLE_NAME'].'" name="tables[]" class="checkTable" data-system="'.($systemTable ? 'isSystem' : '').'" data-deprecated="'.($deprecatedTable ? 'isDeprecated' : '').'" data-tooRows="'.($tooRowsPerTable ? 1 : '').'"/> ';;
			$this->html.=$value['TABLE_NAME'].($value['TABLE_ROWS'] ? ' ('.number_format($value['TABLE_ROWS'],0,',','.').')' : '').'';
			$this->html.='<i class="input-helper"></i>';
			$this->html.='</label>';
			$this->html.='</div>';
		}
		$this->html.='';
		$this->html.='</div>';

		return;
	}
}