<?php
/**
 * SPECIFIC SCRIPT ATCIONS
 *
 * IFRAME MODULE
 *
 */
header('Content-Type: text/html; charset=utf-8');
@session_start();
$response=array(); $strOut='';
$defaulStyle='<style>*{font-family:roboto,arial,verdana;font-size:13px;color:#fff;}</style>';
ob_clean();

function errTimeOut($err='')
{
	ini_set('display_errors', E_ALL);     # muestra los errores sólo en el entorno de pruebas
	updateOpener('oh!','<span class=c-yellow>ERROR DE TIEMPO EXCEDIDO</span>','<span class=c-orange>no es posible continuar</span>');
	echo'<br><br><div style=background:red;color:#fff;margin:4px;padding:4px;>ERROR POR TIEMPO EXCESIVO EN CREACIÓN DE DATOS</div>';
	die();
}
//die();
function updateOpener($boxName='', $title='AVISO', $msg='proceso abortado')
{
	$str='<script>';
	$str.='window.parent.document.getElementById("tagTitleModal").innerHTML="'.$title.'";';
	$str.='window.parent.document.getElementById("tagWaitText").innerHTML="'.$msg.'";';
	$str.='window.parent.document.getElementById("tagLoader").style.display="none";';	//getElementsByClassName
	$str.='</script>';
	echo $str;
	return;
}

//	START prevent direct file access or lost work session
if(!isset($_SERVER['HTTP_REFERER']) && basename($_SERVER['SCRIPT_NAME']) != 'index.php' || !$_SESSION['userAdmin'])
{
	$strOut=$defaulStyle.'<span style="font-family:arial;color:#fff;"><p>No se puede acceder a este archivo<br><br><i>has intentado recargar este marco? ... no puedes hacerlo!</i></p>';
	$strOut.='O es posible que <u>has perdido la sesi&oacute;n de trabajo</u> que ten&iacute;as abierta';
	$strOut.='</span>';

	updateOpener('oh!','<span class=c-yellow>ERROR DE ACCESO</span>','<span class=c-orange>no es posible continuar</span>');
	die($strOut);
}	//	END prevent direct file access or lost work session

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_REQUEST;

@include('../../../conf/config_web.php');
@include($dirModulesAdmin.$arraExtVars['moduleAdmin'].'config_module.php');


if($arraExtVars['action'] == 'clear')
{
	echo '<script>document.write("");</script>';
//	echo $defaulStyle.'<script>document.body.innerHTML="";</script>';
	ob_flush();
	echo '';//print_r($arraExtVars);die('END AJAX FILE !!!!!!!!!!!!!!!!!!!');
	die('');
}

if($arraExtVars['action'] == 'goBackUp')
{
	$prefijoNombreTabla='db_';
	$strOut.='<h2 class="c-white">BACKUP ERROR</h2>';
	$strOut.='Ha ocurrido un <u>error en el proceso de back-up</u>&nbsp;&nbsp;<span class="c-red bgm-white">&nbsp;('.date('H:i:s').')&nbsp;</span>';

	if(isset($arraExtVars['num_tables']) && $arraExtVars['num_tables'] == 1)	// SOLO UNA TABLA
	{
		$prefijoNombreTabla=str_replace(' ','_',$arraExtVars['tables'][0]);
		$prefijoNombreTabla=$prefijoNombreTabla.'_DB_';
	}

  $tmpPhpV=substr(PHP_VERSION,0,3);
  $backup_file=$prefijoNombreTabla.strtolower($db_name).'-'.strtoupper($arraExtVars['backUpMode']).'-'.$tmpPhpV.'-'.date('Y.m.d-Hi').'.sql';
	//$backup_file=$prefijoNombreTabla.strtolower($db_name).'-'.strtoupper($arraExtVars['backUpMode']).'-'.date('Y.m.d-Hi').'.sql';
	$fp=@fopen($pathBackupFiles.$backup_file,'w');

	if(empty($fp))	// NO EXISTE O NO SE ACCEDE A LA CARPETA PARA EL BACK-UP
	{
		$strOut=$defaulStyle.'<center>Parece que no se puede escribir en la carpeta de:<h4>'.(substr($pathBackupFiles,strpos($pathBackupFiles,'/')+1,strrpos($pathBackupFiles,'/')-3)).'</h4>no existe o no tiene los permisos adecuados para crear el archivo<br><b><u>'.$backup_file.'</u></b>.<br /><br />Para poder efectuar el back-up deseado, primero has de solucionar este problema.</center>';
		fclose($fp);
		updateOpener('','<span class=c-yellow>ERROR AL CREAR EL ARCHIVO</span>','<span class=c-orange>no es posible continuar</span>');
		die($strOut);
	}

	include($folderRootFront.'conf/config.misc.php');
	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModulesAdmin.$arraExtVars['moduleAdmin'].'_mod_bbdd/module_model.class.php');
	$data=new ModelLocalModule($db_type, $db_host, $db_user, $db_pass, $db_name,null,$port);
//	include($dirModAdmin.'mod_common_lists.class.php');
//	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name);
	$resConnexion=$data->connectDB();
//echo '<pre>';print_r($resConnexion);print_r($data);die();

  if(!$data->idConexion->sqlstate)
  {
		$strOut=$defaulStyle.'<center><span class="c-white f-16">Ha ocurrido un error al intentar hacer la conexión a la BBDD</span><br /><br />Para poder efectuar el back-up deseado, se debe solucionar este problema.<br><br><br>Si el problema persiste, contacta con el departamento de informática.</center>';
    updateOpener('','<span class=c-yellow>ERROR DE CONEXI&Oacute;N</span>','<span class=c-orange>no es posible continuar</span>');
    die($strOut);
  }

	$schema='# '.@$_SESSION['nombre_site']."\n" .
'#' . "\n" .
'# Database Backup para la BBDD: '.$db_name."\n" .
'# Copia realizada por: '.$_SESSION['userAdmin']['name'].' '.$_SESSION['userAdmin']['surname']."\n" .
'#' . "\n".
'# Sobre PHP v.'.PHP_VERSION."\n".
'#' . "\n".
'# Fecha de ejecucion: '.date('D-m-y H:i')."\n\n";
	fputs($fp,$schema);
  $resFile=0;

	$strOut='';
	echo $defaulStyle;
	ob_flush();

	ini_set('display_errors', E_ERROR);     # muestra los errores sólo en el entorno de pruebas

	$momentStart=date('H:i:s');
////	echo 'Inicio del proceso: <b>'.$momentStart.'</b>.<hr />';
	set_time_limit(1000);
	$numZeros=strlen($arraExtVars['num_tables']);

	//@ini_set('max_execution_time', 2);
//register_shutdown_function('errTimeOut');
		foreach($arraExtVars['tables'] as $key=>$table)
		{
			echo 'procesando ('.str_pad(($key+1),$numZeros,'0',STR_PAD_LEFT).'/'.$arraExtVars['num_tables'].'): <b>'.$table.'</b> ...<a name="p_'.$key.'"></a>';
			echo '<script>document.location="#p_'.$key.'";</script>';
			flush();
			$res=$data->backUpSql($table, $arraExtVars['backUpMode']);
//			register_shutdown_function('errTimeOut', $table);

			$tmpRes=fputs($fp, $res);
			$resFile+=$tmpRes;
			ob_flush();
			echo ' <span style=color:yellow;font-weight:bold;>OK!</span><br />';
		}

	fclose($fp);
	set_time_limit(5);

  if($res)  // COPIA FINALIZADA CORRECTAMENTE
  {
		$strOut=$defaulStyle;
		ob_flush();
		$momentEnd=date('H:i:s');
		$strOut.=$defaulStyle.'<hr />';
////		$strOut.=$defaulStyle.'Fin del proceso: <b>'.$momentEnd.'</b>.';
		$strOut.='<br /><center><span class="c-white f-13">Archivo creado (<i>'.number_format($resFile,0,',','.').' bytes</i>):<br><b>'.$backup_file.'</b></center><br /><a name="bottomPage"></a>';
		$strOut.='<script>document.location="#bottomPage";</script>';
//&nbsp;&nbsp;
    $btnFileDownLoad=@$arraExtVars['fileDownload']? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class=\'pull-right\'><button data-file=\''.$backup_file.'\' class=\'btn btn-default '.$_SESSION['userAdmin']['pref_']['btn_default_forCol'].' '.$_SESSION['userAdmin']['pref_']['btn_default_bckCol'].' waves-effect btnDownload\' style=margin-top:-10px;><i class=\'zmdi zmdi-download\'></i> Descargar</button></div>' : '';
    updateOpener('','<span class=\'c-black\'><i class=\'zmdi zmdi-check\'></i></span> <span class=\'c-white f-700\'>COPIA CORRECTA</span>','<span class=c-yellow>proceso finalizado (inicio - fin: <b>'.$momentStart.'</b> - <b>'.$momentEnd.'</b>)</span>'.$btnFileDownLoad);
  }

	die($strOut);
}
