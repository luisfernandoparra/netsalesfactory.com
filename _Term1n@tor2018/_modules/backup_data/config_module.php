<?php
/**
 * DATE UPDATE: 2016-11-25 08:25 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * ADMIN USERS
 * 
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=20;
$minRoleAdim=1;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>Realizar copias de seguridad totasl o parcial de la base de datos actual</small>';
$moduleSetting->skipBdDefault=true;	// OMITIR LA BBDD POR DEFECTO
$moduleSetting->autoRowNumber=0;
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->keyListLabel='TABLE_NAME';
$moduleSetting->foreignKeys='TABLE_NAME';	// IMPORTANTE, SIN ESPACIOS
$moduleSetting->mainTable='INFORMATION_SCHEMA.TABLES';	// MASTER DATA TABLE
$moduleSetting->orderField='TABLE_NAME';
$moduleSetting->arrFieldsOutput=array('TABLE_NAME','ENGINE','CREATE_TIME','TABLE_ROWS');
$moduleSetting->defaultWhereSql=' && TABLE_SCHEMA = \''.$db_name.'\' && SUBSTRING(TABLE_NAME,1,8) != \''.$tbl_system.'\' && ENGINE !=\'MEMORY\''; // SE OMITEN DEL LISTADO LAS TABLAS DE SYSTEMA
$moduleSetting->numRecDisplay='[5,10,25,50]'; // array, EJ.: [5,10,20]
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=false;
$moduleSetting->isEditable=__LEVEL_ACCESS_GOD;
$moduleSetting->isCanEnabled=false;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->minEnableLevel=__LEVEL_ACCESS_MASTER;

$moduleSetting->tooRowsPerTable=1000;	// LIMITE MAXIMO DE FILAS POR TABLA (CHECK PAR MARCAR/DESCMARCAR)

/**
 * IMPORTANTT: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal
 * DEBEN DE ESTAR ORDENADOS Y AGRUPADOS POR formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$order=0;
$moduleSetting->arrFieldsFormModal=array(
	'TABLE_NAME'=>array(
    'formType'=>'text'
    ,'listHeader'=>'ID'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>1]
    ,'required'=>1
    ,'label'=>''
    ,'helpText'=>''
    ,'visible'=>1
    ,'formGroup'=>1
    ,'placeHolder'=>''
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
	,'TABLE_NAME'=>array(
    'formType'=>'text'
    ,'tabularListData'=>['columnOrder'=>3]
    ,'dataType'=>'varchar'
    ,'required'=>1
    ,'label'=>'Nombre tabla'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'ENGINE'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>4]
    ,'required'=>1
    ,'label'=>'Motor'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Titolo'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'CREATE_TIME'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>2]
    ,'required'=>false
    ,'label'=>'Fecha creaci&oacute;n'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Tecnica utilizzata'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'TABLE_ROWS'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>5]
    ,'required'=>false
    ,'label'=>'Num. filas'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'N. rows'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
);

