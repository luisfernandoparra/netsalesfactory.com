<?php
/**
 * LOCAL AJAX MODULE
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');

ini_set('display_errors', $displayErrors);
error_reporting($errorReportDefault);
$moduleParams='';

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
$response['query']=array();

if($hashSecutityScript != __HASH_CONTROL_SCRIPT){
  die(__MSG_ACCESS_ERROR);
}

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$tmpVars=json_decode(file_get_contents('php://input'), true);	// DECODE JSON DATA

if(count($tmpVars))
  $arraExtVars=$tmpVars;

if(isset($arraExtVars['from']) && $arraExtVars['from'])
{
  $localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'].'_mod_bbdd/';
  $localModuleFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'].'';
	$provenience=$_SERVER['HTTP_REFERER'];
	$modNameFrom=strpos($provenience,'_modules/')+9;
	$modName=substr($provenience, $modNameFrom);
	$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));

	if(file_exists($dirModulesAdmin.'_modules/'.$modName.'/config_module.php'))
		include($dirModulesAdmin.'_modules/'.$modName.'/config_module.php');
}

echo '<link href="'.$web_url.$adminFolder.'/css/app_1.min.css" rel="stylesheet" />';

if($arraExtVars['action'] == 'getLopdContent')
{
	include($dirModAdmin.'mod_bbdd.class.php');
  include($localModelsFolder.'module_model_ajax.class.php');

  $dataModuleBBDD=null; $msgModuleError=' Error en el script Ajax: <br>'.__FILE__;
  $dataModuleBBDD=new ModelLocalModuleAjax($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting, $port);
  $resConnexion=$dataModuleBBDD->connectDB();

  if(!$resConnexion)
	{
    $msgModuleError='<div class="text-center w-100  pull-left"><div class="alert alert-danger p-25 f-16">'.strtoupper(__COMMON_BBDD_TXTCONNECTERROR).'<br><br>'.__COMMON_BBDD_TXTNOCONNECTIONBBDD.'<br><br>'.__COMMON_GENERICTEXT_ERROCONFIGMODULEEND.'<br></div></div>';
		echo $msgModuleError;
		die();
	}

	switch($arraExtVars['detailToDisplay'])
	{
		case 'status_accept_legal_basis':
			$fieldLegalContentsHistory='bl_contents';
			$fieldLegalContentsLanding='privacy_policy_file';
			$headerTitleText=''.$moduleSetting->arrFieldsFormModal['status_accept_legal_basis']['helpTextExtend'];
			break;
		case 'status_accept_customer_data_transfer':
			$fieldLegalContentsHistory='cdt_contents';
			$fieldLegalContentsLanding='customer_data_transfer_file';
			$headerTitleText=''.$moduleSetting->arrFieldsFormModal['status_accept_privacy_policy']['helpTextExtend'];
			break;
		default:
			$fieldLegalContentsHistory='pp_contents';
			$fieldLegalContentsLanding='landing_terms_file';
			$headerTitleText=''.$moduleSetting->arrFieldsFormModal['status_accept_privacy_policy']['helpTextExtend'];
	}

  $cur_conn_id=$dataModuleBBDD->idConexion;

	$resLegalData=$dataModuleBBDD->getHistoryLegalContent(@$arraExtVars, 'lopd_landings_history_legal_texts', $fieldLegalContentsHistory, $fieldLegalContentsLanding);
//echo $dataModuleBBDD->query.'<hr>';print_r($arraExtVars);die();
	if($dataModuleBBDD->error)
	{
		foreach($dataModuleBBDD->errorTxt as $errorDisplay)
			echo $errorDisplay;

		die('<br /><br /><center>Si esta situación continúa, por favor, contactar con informática.</center>');
	}

	if(count($resLegalData))
	{
		if($resLegalData['legal_text'])
		{
			$tmpDate=$resLegalData['effective_date'];
			echo '<div class="bgm-lime p-5 p-l-10 c-black">Contenido de <b>'.$headerTitleText.'</b> del: '.($tmpDate).'</div>';
			echo '<div class="bgm-white p-20">';
			echo ''.$resLegalData['legal_text'];
			echo '</div>';
		}
		else
		{
			echo '<div>';
			echo '<center><h2>no se hallaron textos legales para esta landing!</h2></center>';
			echo '</div>';
		}
	}
//echo $moduleSetting->mainTable;
//echo "\n".'//////<pre>';print_r($dataModuleBBDD);
//echo "\n".'<pre>';print_r($moduleSetting->arrFieldsFormModal);
//echo "\n<PRE>".'';print_r($arraExtVars);die();

  die();
}
