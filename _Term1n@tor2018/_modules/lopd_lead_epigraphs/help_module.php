<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">

		<ul class="tab-nav text-center fw-nav">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Este módulo</a></li>
			<!--<li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">otro TAB</a></li>-->
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>Desglose de las principales funciones del módulo actual.</p>
					<p>La función de este módulo es la de poder visualizar que textos legales aprobados por el lead para cada determianda landing, pudiendo también visualizar el contenido legal vigente en el momento del registro para cada epígrafe aprobado.</p>
					<ol>
						<li class="m-b-10">El campo <b>"Nombre landing"</b> identifica landing del registro (+ el ID de la misma).</li>
						<li class="m-b-10">El campo <b>"Insertado"</b> visualiza la fecha de registro del lead.</li>
						<li class="m-b-10">El campo <b>"Modificado"</b> debería servir para mostrar la fecha de modificación, actualmente NO utilizada y corresponde a la misma fecha de registro.</li>
						<li class="m-b-10">El bloque <b>"Datos personales del lead registrado en la landing"</b> dibuja los elementos almacenados por el registro del lead en la landing visualizada, estos datos pueden cambiar en función de los elementos cargados según la landing y el cliente de la misma.</li>
						<li class="m-b-10">El campo <b>"P.P."</b> si activo, indica la aceptación de las políticas de privacidad de la landing.</li>
						<li class="m-b-10">El campo <b>"B.L."</b> si activo, indica la aceptación de los textos legales de la landing.</li>
						<li class="m-b-10">El campo <b>"C.D.T."</b> si activo, indica la aceptación de la transferencia de los datos personales a terceros.</li>
					</ol>
					<p>Para campos de los puntos <b>5</b>, <b>6</b> y <b>7</b> puede aparecer el botón <button type="button" class="btn btn-xs waves-effect p-r-10 c-black bgm-lightgray">&nbsp;<span class="fa fa-search"></span>&nbsp;ver texto legal</button> que permite visualizar el texto legal vigente aprobado por el lead en el momento del registro.</p>
					<p>Este módulo es <i>exclusivamente de visualización de datos</i>, no se puede efectuar modificación alguna sobre los mismos.</p>
				</div>

			</div>

	</div>
</div>
