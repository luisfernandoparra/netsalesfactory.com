<?php
/**
 * DATE UPDATE: 2018-03-13 13:01 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * HISTORICO DE LAS ACEPTACIONES/REHUSES TEXTOS LEGALES LOPD
 * 
 * IMPORTANTE, EL PARAMETRO orderTabularList PRESENTE EN LOS CAMPOS
 * INDICA LA POSICION Y DONDE DEBE APARECER EN EL LISTADO TABULAR DEL MODULO
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$moduleSetting=new stdClass();

// MANDATORY PARAMS
$moduleSetting->moduleBBDD=null;
$moduleSetting->mainTable='lopd_lead_company_legal_epigraphs';
$moduleSetting->keyListLabel='id';
$moduleSetting->orderField='id';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->isCanEnabled=false;
$moduleSetting->allowNewRecord=false;

// OPTIONAL PARAMS
//$moduleSetting->minEditLevel=99999;
$moduleSetting->moduleCommenHeader='<small>Historico de las aceptaciones/rehuses textos legales LOPD por leads registrados</small>';
$moduleSetting->numRecDisplay='[10,30,50]'; // EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;
$moduleSetting->defaultOrderField=['id'=>'DESC'];
$moduleSetting->disallowUpdate=true;

$order=0;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id'=>array(
    'formType'=>'hidden'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'label'=>''
    ,'helpText'=>''
    ,'visible'=>0
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'id_lead'=>array(
		'formType'=>'text'
		,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>2]
//		,'tabularFilter'=>['mode'=>'BBDD','origin'=>'registros','tag'=>array('filterType'=>'select')]
		,'fieldTableName'=>'registros'
		,'arrFieldsTagSelect'=>array('id_registro','nombre')
		,'includeMainBBDD'=>true
		,'orderSelectField'=>'nombre'
		,'label'=>' Nombre lead'
		,'excludeAutoForm'=>true
		,'visible'=>false
		,'order'=>++$order
  )
  ,'id_landing'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>3,'columnWidth'=>2]
		,'tabularFilter'=>['mode'=>'BBDD','origin'=>__QUERY_TABLES_PREFIX.'landings_site_config','tag'=>array('filterType'=>'select')]
		,'fieldTableName'=>__QUERY_TABLES_PREFIX.'landings_site_config'
		,'arrFieldsTagSelect'=>array('id','CONCAT("<span style=color:red;>",id,"</span> - ",landing_name)') // PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
		,'includeMainBBDD'=>true
		,'orderSelectField'=>'id'
		,'label'=>' ID - Nombre landing'
		,'helpText'=>'Id + Referecia de la landing'
		,'visible'=>1
		,'formGroup'=>0
		,'excludeAutoForm'=>false
		,'order'=>++$order
  )
  ,'__arr_edit_extra_data'=>array(
		'formType'=>'text'
		,'dataType'=>'hide'
		,'fieldTableName'=>'registros'
		,'arrFieldsTagSelect'=>array('id_registro','nombre','email','telefono','cp','sex','id_provincia')
		,'arrProperTagSelect'=>array(
				'id_registro'=>array('name'=>'id','type'=>'hide','cssClass'=>'')
				,'nombre'=>array('name'=>'Nombre y apellidos','type'=>'','cssClass'=>'c-black')
				,'email'=>array('name'=>'E-mail','type'=>'','cssClass'=>'')
				,'telefono'=>array('name'=>'Tel&eacute;fono','type'=>'','cssClass'=>'')
				,'cp'=>array('name'=>'C.P.','type'=>'','cssClass'=>'')
				,'sex'=>array('name'=>'G&eacute;nero','type'=>'','cssClass'=>'')
				,'id_provincia'=>array('name'=>'Id provincia','type'=>'','cssClass'=>'')
			)
		,'visible'=>true
		,'formGroup'=>1
		,'getLocalRecord'=>true
		,'order'=>++$order
  )
  ,'status_accept_privacy_policy'=>array(
		'formType'=>'checkbox'
		,'dataType'=>'toggle'
		,'tabularListData'=>['columnOrder'=>4,'columnWidth'=>1]
		,'toggleParams'=>array('tsColor'=>'green')
		,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')
		,'label'=>'P.P.'
		,'helpText'=>'Estado para las pol&iacute;ticas de privacidad'
		,'helpTextExtend'=>'Texto legal de las pol&iacute;ticas de privacidad'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>2
		,'excludeAutoForm'=>false
		,'order'=>++$order
  )
  ,'status_accept_legal_basis'=>array(
		'formType'=>'checkbox'
		,'dataType'=>'toggle'
		,'tabularListData'=>['columnOrder'=>5,'columnWidth'=>1]
		,'toggleParams'=>array('tsColor'=>'green')
		,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')
		,'label'=>'B.L.'
		,'helpText'=>'Estado para las bases legales'
		,'helpTextExtend'=>'Texto legal de las bases legales'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>2
		,'excludeAutoForm'=>false
		,'order'=>++$order
  )
  ,'status_accept_customer_data_transfer'=>array(
		'formType'=>'checkbox'
		,'dataType'=>'toggle'
		,'tabularListData'=>['columnOrder'=>6,'columnWidth'=>1]
		,'toggleParams'=>array('tsColor'=>'green')
		,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')
		,'label'=>'C.D.T.'
		,'helpText'=>'Estado para la aceptaci&oacute;n de transferencia de datos'
		,'helpTextExtend'=>'Texto legal de la transferencia de datos'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>2
		,'excludeAutoForm'=>false
		,'order'=>++$order
  )
  ,'insert_date'=>array(
		'formType'=>'date'
		,'dataType'=>'timestamp'
		,'tabularListData'=>['columnOrder'=>7,'columnWidth'=>2]
		,'tabularFilter'=>['mode'=>'STATIC','tag'=>array('filterType'=>'date')]
		,'label'=>' Insertado'
		,'helpText'=>''
		,'visible'=>1
		,'placeHolder'=>'Fecha de registro del lead'
		,'formGroup'=>0
		,'excludeAutoForm'=>false
		,'order'=>++$order
  )
  ,'update_date'=>array(
		'formType'=>'date'
		,'dataType'=>'date'
		,'tabularFilter'=>['mode'=>'STATIC','tag'=>array('filterType'=>'date')]
		,'label'=>' Modificado'
		,'helpText'=>''
		,'visible'=>1
		,'placeHolder'=>'Fecha de modificaci&oacute;n (NO UTILIZADO)'
		,'formGroup'=>0
		,'excludeAutoForm'=>false
		,'order'=>++$order
  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  0=>array(
    'groupTextTitle'=>'Datos gen&eacute;ricos del registro'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
  ,1=>array(
    'groupTextTitle'=>'Datos personales del lead registrado en la landing'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi zmdi-spellcheck'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
  ,2=>array(
    'groupTextTitle'=>'Estados aceptaciones / rechazos de textos legales'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi zmdi-calendar'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);
