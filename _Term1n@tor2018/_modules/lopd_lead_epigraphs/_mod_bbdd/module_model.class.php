<?php
/**
 *	CLASE CON LAS METODOS DE BBDD NECESARIOS PARA EL MODULO CARGADO
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

//class ModelLocalModule extends CommonEditingModel
class ModelLocalModule extends CBBDD
{
	protected $database;
	private $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $defaultOrderField;
	public $keyListLabel;
	public $mainTable;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;

	public function __construct($commonDb)
	{
		$this->commonDb=$commonDb;
		$this->arrFieldsTagSelect='';
		$this->query='';
		return;
	}


	/**
	 * DEVUELVE LA LISTA DE ELEMENTOS ENCONTRADOS PARA LA TABLA ESPECIFICADA (SI OMITIDA = mainTable)
	 *
	 * @param int $idRecord
	 * @param string $localTable
	 * @param array $arrFieldsTagSelect	DEBE CONTENER LOS NOMBRES DE LOS CAMPOS PARA OBTENER LOS DATOS, EL PRIMERO SIEMPRE EL ID DE LA TABLA
	 * @return array
	 */
	public function getSpecificLocalRecordData($idRecord=0 ,$localTable=null, $arrFieldsTagSelect=null, $sqlWhere=null, $fieldOrder=null)
	{
		$sqlOrder=isset($fieldOrder) ? ' ORDER BY '.$fieldOrder : '';
		$sqlWhere=isset($sqlWhere) ? ' && '.$sqlWhere : '&& '.$arrFieldsTagSelect[0].'='.(int)$idRecord;
		$arrAliasNames=array('id_only_read_list'); $fieldsNames='';

		if(count($arrFieldsTagSelect))
		{
			foreach($arrFieldsTagSelect as $key=>$value)
				$fieldsNames.=$value.' AS '.($arrAliasNames[$key] ? $arrAliasNames[$key] : ''.$value).', ';

			$fieldsNames=substr($fieldsNames,0,-2);
		}
		$localTable=isset($localTable) ? $localTable : $this->commonDb->mainTable;
		$query='SELECT '.$fieldsNames.' FROM `%s`.`%s` WHERE 1 '.$sqlWhere.$sqlOrder;
		$this->query=sprintf($query, $this->commonDb->db, $localTable);
		$this->commonDb->getResultSelectArray($this->query);
		$resData=$this->commonDb->tResultadoQuery[0];
//echo '<h1>OGGETTO CORRETTO</h1><hr><pre>';print_r($resData);echo'</pre>';return;
		return $resData;
	}

  public function __destruct(){}
}