<?php
/**
 *	CLASE CON LAS FUNCIONES DE BBDD NECESARIAS LOS AJAX DEL MODULO
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class ModelLocalModuleAjax extends CBBDD
{
	protected $database;
	private $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $defaultOrderField;
	public $keyListLabel;
	public $mainTable;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;

  public function __construct($db_type=null, $db_host='', $db_user='', $db_pass='', $db_name='', $localParams=null, $port=1312)
  {
    $this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
		$this->arrFieldsTagSelect='';
		$this->query='';
		$this->prefixGtp=(isset($localParams->prefixTbl) && $localParams->prefixTbl) ? $localParams->prefixTbl : __QUERY_TABLES_PREFIX;
		return;
	}


	/**
	 * OBTENCION DE LOS TEXTOS LEGALES ACEPTADOS POR EL LEAD EN FECHA DE REGISTRO
	 *
	 * @param array $requestParams
	 * @param string $tableName
	 * @param string $fieldLegalContentsHistory
	 * @param string $fieldLegalContentsLanding
	 * @return array
	 */
	public function getHistoryLegalContent($requestParams=null, $tableName='', $fieldLegalContentsHistory='', $fieldLegalContentsLanding)
	{
    if(!isset($fieldLegalContentsHistory)) // FALTA UN PARAMETRO ESENCIAL PARA EJECUTAR LA QUERY
    {
      $this->error=true;
      $this->errorTxt[]='<center><h2>Falta indicar la accion SQL</h2></center>';
      $this->errorTxt[]='<br /><br /><div class="f-20 p-20 m-20 bgm-red c-white">Es necesario conocer la acción para poder aplicar el control necesario del campo: <b>'.$fieldLegalContentsHistory.'</b></div>';
      return;
    }

		$leadRegistrationDate=strtotime($requestParams['insert_date']);
    $query='SELECT %s AS legal_text, effective_date FROM %s.%s WHERE id_landing=%d && YEAR(effective_date)=\'%s\' && MONTH(effective_date)=\'%s\' && DAY(effective_date)=\'%s\' && HOUR(effective_date)=\'%s\' ORDER BY effective_date DESC LIMIT 1';
		$this->query=sprintf($query, $fieldLegalContentsHistory, $this->db, $this->prefixGtp.$tableName, (int)$requestParams['id_landing'], date('Y',$leadRegistrationDate), date('m',$leadRegistrationDate), date('d',$leadRegistrationDate), date('H',$leadRegistrationDate));
    $this->getResultSelectArray($this->query);
    $resQuery=$this->tResultadoQuery[0];

		if(!$resQuery['legal_text'])	// NO HAY TEXTOS LEGALES PARA EL MOMENTO EXACTO DEL REGISTRO, SE BUSCAN LOS MAS CERCANOS DEL DIA
		{
	    $query='SELECT %s AS legal_text, effective_date FROM %s.%s WHERE id_landing=%d && YEAR(effective_date)=\'%s\' && MONTH(effective_date)=\'%s\' && DAY(effective_date)=\'%s\' ORDER BY effective_date DESC LIMIT 1';
			$this->query=sprintf($query, $fieldLegalContentsHistory, $this->db, $this->prefixGtp.$tableName, (int)$requestParams['id_landing'], date('Y',$leadRegistrationDate), date('m',$leadRegistrationDate), date('d',$leadRegistrationDate));
			$this->getResultSelectArray($this->query);
			$resQuery=$this->tResultadoQuery[0];
		}

		if(!$resQuery['legal_text'])	// NO HAY TEXTOS LEGALES PARA EL MOMENTO EXACTO DEL REGISTRO, SE BUSCAN LOS MAS CERCANOS DEL MES
		{
			$query='SELECT %s AS legal_text, effective_date FROM %s.%s WHERE id_landing=%d && YEAR(effective_date)="%s" && MONTH(effective_date)="%s" ORDER BY id_history DESC LIMIT 1';
			$this->query=sprintf($query, $fieldLegalContentsHistory, $this->db, $this->prefixGtp.$tableName, (int)$requestParams['id_landing'], date('Y',$leadRegistrationDate), date('m',$leadRegistrationDate));
			$this->getResultSelectArray($this->query);
			$resQuery=$this->tResultadoQuery[0];
		}

		if(!$resQuery['effective_date'] || strtotime($resQuery['effective_date']) > $leadRegistrationDate)	// GARANTIZA LA CARGA DE LOS TEXTOS LEGALES VIGENTES AL REGISTRO DEL LEAD
		{
			$query='SELECT %s AS legal_text, effective_date FROM %s.%s WHERE id_landing=%d && DATE(effective_date) <= "%s" ORDER BY id_history DESC LIMIT 1';
			$this->query=sprintf($query, $fieldLegalContentsHistory, $this->db, $this->prefixGtp.$tableName, (int)$requestParams['id_landing'], date('Y-m-d',$leadRegistrationDate));
			$this->getResultSelectArray($this->query);
			$resQuery=$this->tResultadoQuery[0];
		}
//echo ($this->query).'<hr> ['.strtotime($resQuery['effective_date']).']<hr> >>> '.$leadRegistrationDate.'<p>';//print_r($resQuery);


		if(!$resQuery['legal_text'])	// NO SE ENCONTRARON TEXTOS LEGALES EN EL HISTORICO, SE RECUPERAN LOS ACTUALES DE LA LANDING
		{
			$query='SELECT %s AS legal_text, \''.date('Y-m-d',$leadRegistrationDate).'\' AS effective_date FROM %s.%s WHERE id=%d ';
			$this->query=sprintf($query, $fieldLegalContentsLanding, $this->db, $this->prefixGtp.'landings_site_config', (int)$requestParams['id_landing']);
			$this->getResultSelectArray($this->query);
			$resQuery=$this->tResultadoQuery[0];
		}

		return $resQuery;
  }
}