<?php
/**
 * LOCAL EDIITING MODULE CONTENTS
 * 
 *	DEBEN EXISTIR LOS PARAMETROS EN EL CAMPO ESPECIAL __arr_edit_extra_data:
 * arrFieldsTagSelect
 * arrProperTagSelect
 */

$moduleHtml='<div class="w-100 m-b-20">';
$moduleHtml.='<div class="table-responsive"><table class="table table-striped table-hover table-condensed">';
$moduleHtml.=''; $moduleTblHeader=''; $moduleTblTitleCol=''; $moduleTblRow='';

if(count($moduleSetting->arrFieldsFormModal['__arr_edit_extra_data']['arrFieldsTagSelect']))
{
	$moduleTblHeader.='<thead class="c-red"><tr>';

	foreach($moduleSetting->arrFieldsFormModal['__arr_edit_extra_data']['arrFieldsTagSelect'] as $localElem)
	{
		if($moduleSetting->arrFieldsFormModal['__arr_edit_extra_data']['arrProperTagSelect'][$localElem]['type'] == 'hide' || !$resDataForm['__arr_edit_extra_data'][$localElem])
			continue;

		$moduleTblHeader.='<th>'.$moduleSetting->arrFieldsFormModal['__arr_edit_extra_data']['arrProperTagSelect'][$localElem]['name'].'</th>';
	}

	$moduleTblHeader.='</tr></thead>';
	$moduleTblRow.='<tr>';


	foreach($moduleSetting->arrFieldsFormModal['__arr_edit_extra_data']['arrFieldsTagSelect'] as $localElem)
	{
		if($moduleSetting->arrFieldsFormModal['__arr_edit_extra_data']['arrProperTagSelect'][$localElem]['type'] == 'hide' || !$resDataForm['__arr_edit_extra_data'][$localElem])
			continue;

		$moduleTblRow.='<td ';

		foreach($moduleSetting->arrFieldsFormModal['__arr_edit_extra_data']['arrProperTagSelect'][$localElem] as $localKey=>$localValue)
		{
			if($localKey == 'cssClass' && $localValue)
				$moduleTblRow.=' class="'.$localValue.'"';
		}

		$moduleTblRow.=' >'.$resDataForm['__arr_edit_extra_data'][$localElem].'</td>';
	}

	$moduleTblRow.='</tr>';

}

$moduleHtml.=$moduleTblHeader.$moduleTblRow;
$moduleHtml.='</table></div>';
$moduleHtml.='</div>';
$outFormBoxField.=$moduleHtml;
