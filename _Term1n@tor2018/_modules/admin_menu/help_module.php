<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">
		<ul class="tab-nav text-center fw-nav" data-tab-color="green">
			<li class=""><a href="#tab3" data-toggle="tab" aria-expanded="false">nota sobre este mismo módulo</a></li>
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>El principal elemento para colocar los módulos disponibles en el back-office es el control desplegable "<b>Menú hijo de...</b>"; con este control se establece el nivel donde debe aparecer el elemento de menú editado, siendo posible colocarlo en cualquiera de los <b>tres niveles disponibles</b>.</p>
					<ol>
						<li class="m-b-10"><b>Nombre carpeta</b> debe indicar el nombre de la carpeta que contiene los archivos del módulo al que se refiere la entrada del menú.</li>
						<li class="m-b-10"><b>Etiqueta</b> es el texto que aparece en el menú (y sucesivamente en varias partes del mismo módulo al que se accede).</li>
						<li class="m-b-10"><b>Link</b> es el nombre del archivo que abre el menú, usualmente <i>`index.php`</i>.</li>
						<li class="m-b-10"><b>Orden de aparici&oacute;n</b> afecta al orden de aparición respecto a los demás elementos del menú izquierdo.</li>
						<li class="m-b-10"><b>Icono relacionado</b> Es posible asociar un icono (que aparecerá en distintas partes del mismo módulo) para cada elemento de menú.</li>
						<li class="m-b-10"><b>Nivel mínimo de acceso</b> establece el nivel mínimo necesario del usuario accedido que debe tener para visualizar la entrada del mismo menú. Obviamente, si se establece a una entrada que tiene sub-menús un nivel menor que los mismos, y el usuario accedido no tiene un rol igual o mayor, estos NO aparecerán.</li>
						<li class="m-b-10"><b>Estado</b> posibilita habilitar o deshabilitar el menú editado.</li>
					</ol>
				</div>
				<div class="tab-pane fade f-16" id="tab2">
						<p>El archivo "config_module.php" contiene la gran parte de parámetros necesarios para el funcionamiento de los módulos del back-office.</p>
						<p>Debemos recordar que este archivo contiene también las definiciones para los campos a utilizar en el listado principal y en la edición del mismo, esto para todos los módulos con estructuras simples.</p>
				</div>


				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>

			</div>
	</div>
</div>
