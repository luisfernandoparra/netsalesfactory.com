<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">
		<ul class="tab-nav text-center fw-nav" data-tab-color="green">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Notas</a></li>
			<!--<li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">config_module.php</a></li>-->
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade f-16" id="tab3">
					<ol>
						<li class="m-b-10"><b>Funciones específicas para algunos campos de este módulo:</b>
							<br>campo "menu_s_modulename": 'extraFunc' => 'openModalModulesFolders'
							<br>campo "icon": 'extraFunc' => 'modalIcons'</li>
					</ol>
				</div>

				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>

			</div>
	</div>
</div>
