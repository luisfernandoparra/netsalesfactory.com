<?php
@header('Content-Type: text/html; charset=utf-8');
@session_start();

include('../../../conf/config_web.php');
include('../../../conf/config.misc.php');

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_comm_vendor_CSS.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_javascript_libraries.php');

$currentIcon=$arraExtVars['iconValue'] ? $arraExtVars['iconValue'] : '' ;
?>
<style>
.col-sm-3:hover{
background:#ddd;
color:#000;
cursor:pointer;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$("div[class=col-sm-3]").click(function(){
		var tmpValue=window.parent.$("input[name='<?=$arraExtVars['fieldName']?>']").val();
		var tmpNewClass=$(this).children("i").attr("class");
		tmpNewClass=tmpNewClass.substr(5);
		window.parent.$("input[name='<?=$arraExtVars['fieldName']?>']").val(tmpNewClass);
		window.parent.$("#modalIcons").modal('toggle');
	});

	$(".col-sm-3").addClass("p-10");

	$("i[class='zmdi <?=$currentIcon?>']").addClass("c-red");
	$("body").animate({scrollTop:$(".<?=$currentIcon?>").offset().top},600);
});
</script>

</head>
<body class="bgm-white">
<div class="card-body card-padding f-16">

<div class="col-sm-3"><i class="zmdi zmdi-3d-rotation"></i>&nbsp;&nbsp;<span>3d-rotation</span></div>
<div class="col-sm-3"><i class="zmdi zmdi-airplane-off"></i>&nbsp;&nbsp;<span>airplane-off</span></div>
<div class="col-sm-3"><i class="zmdi zmdi-airplane"></i>&nbsp;&nbsp;<span>airplane</span></div>
<div class="col-sm-3"><i class="zmdi zmdi-album"></i>&nbsp;&nbsp;<span>album</span></div>
<div class="col-sm-3"><i class="zmdi zmdi-archive"></i>&nbsp;&nbsp;<span>archive</span></div>
<div class="col-sm-3"><i class="zmdi zmdi-assignment-account"></i>&nbsp;&nbsp;<span>assignment-account</span></div>
<div class="col-sm-3"><i class="zmdi zmdi-assignment-alert"></i>&nbsp;&nbsp;<span>assignment-alert</span></div>
<div class="col-sm-3"><i class="zmdi zmdi-assignment-check"></i>&nbsp;&nbsp;<span>assignment-check</span></div>
<div class="col-sm-3"><i class="zmdi zmdi-assignment-o"></i>&nbsp;&nbsp;<span>assignment-o</span></div>
<div class="col-sm-3"><i class="zmdi zmdi-assignment-return"></i>&nbsp;&nbsp;<span>assignment-return</span></div>
<div class="col-sm-3" data-name="assignment-returned" data-code="f10b"><i class="zmdi zmdi-assignment-returned"></i>&nbsp;&nbsp;<span>assignment-returned</span></div>
<div class="col-sm-3" data-name="assignment" data-code="f10c"><i class="zmdi zmdi-assignment"></i>&nbsp;&nbsp;<span>assignment</span></div>
<div class="col-sm-3" data-name="attachment-alt" data-code="f10d"><i class="zmdi zmdi-attachment-alt"></i>&nbsp;&nbsp;<span>attachment-alt</span></div>
<div class="col-sm-3" data-name="attachment" data-code="f10e"><i class="zmdi zmdi-attachment"></i>&nbsp;&nbsp;<span>attachment</span></div>
<div class="col-sm-3" data-name="audio" data-code="f10f"><i class="zmdi zmdi-audio"></i>&nbsp;&nbsp;<span>audio</span></div>
<div class="col-sm-3" data-name="badge-check" data-code="f110"><i class="zmdi zmdi-badge-check"></i>&nbsp;&nbsp;<span>badge-check</span></div>
<div class="col-sm-3" data-name="balance-wallet" data-code="f111"><i class="zmdi zmdi-balance-wallet"></i>&nbsp;&nbsp;<span>balance-wallet</span></div>
<div class="col-sm-3" data-name="balance" data-code="f112"><i class="zmdi zmdi-balance"></i>&nbsp;&nbsp;<span>balance</span></div>
<div class="col-sm-3" data-name="battery-alert" data-code="f113"><i class="zmdi zmdi-battery-alert"></i>&nbsp;&nbsp;<span>battery-alert</span></div>
<div class="col-sm-3" data-name="battery-flash" data-code="f114"><i class="zmdi zmdi-battery-flash"></i>&nbsp;&nbsp;<span>battery-flash</span></div>
<div class="col-sm-3" data-name="battery-unknown" data-code="f115"><i class="zmdi zmdi-battery-unknown"></i>&nbsp;&nbsp;<span>battery-unknown</span></div>
<div class="col-sm-3" data-name="battery" data-code="f116"><i class="zmdi zmdi-battery"></i>&nbsp;&nbsp;<span>battery</span></div>
<div class="col-sm-3" data-name="bike" data-code="f117"><i class="zmdi zmdi-bike"></i>&nbsp;&nbsp;<span>bike</span></div>
<div class="col-sm-3" data-name="block-alt" data-code="f118"><i class="zmdi zmdi-block-alt"></i>&nbsp;&nbsp;<span>block-alt</span></div>
<div class="col-sm-3" data-name="block" data-code="f119"><i class="zmdi zmdi-block"></i>&nbsp;&nbsp;<span>block</span></div>
<div class="col-sm-3" data-name="boat" data-code="f11a"><i class="zmdi zmdi-boat"></i>&nbsp;&nbsp;<span>boat</span></div>
<div class="col-sm-3" data-name="book-image" data-code="f11b"><i class="zmdi zmdi-book-image"></i>&nbsp;&nbsp;<span>book-image</span></div>
<div class="col-sm-3" data-name="book" data-code="f11c"><i class="zmdi zmdi-book"></i>&nbsp;&nbsp;<span>book</span></div>
<div class="col-sm-3" data-name="bookmark-outline" data-code="f11d"><i class="zmdi zmdi-bookmark-outline"></i>&nbsp;&nbsp;<span>bookmark-outline</span></div>
<div class="col-sm-3" data-name="bookmark" data-code="f11e"><i class="zmdi zmdi-bookmark"></i>&nbsp;&nbsp;<span>bookmark</span></div>
<div class="col-sm-3" data-name="brush" data-code="f11f"><i class="zmdi zmdi-brush"></i>&nbsp;&nbsp;<span>brush</span></div>
<div class="col-sm-3" data-name="bug" data-code="f120"><i class="zmdi zmdi-bug"></i>&nbsp;&nbsp;<span>bug</span></div>
<div class="col-sm-3" data-name="bus" data-code="f121"><i class="zmdi zmdi-bus"></i>&nbsp;&nbsp;<span>bus</span></div>
<div class="col-sm-3" data-name="cake" data-code="f122"><i class="zmdi zmdi-cake"></i>&nbsp;&nbsp;<span>cake</span></div>
<div class="col-sm-3" data-name="car-taxi" data-code="f123"><i class="zmdi zmdi-car-taxi"></i>&nbsp;&nbsp;<span>car-taxi</span></div>
<div class="col-sm-3" data-name="car-wash" data-code="f124"><i class="zmdi zmdi-car-wash"></i>&nbsp;&nbsp;<span>car-wash</span></div>
<div class="col-sm-3" data-name="car" data-code="f125"><i class="zmdi zmdi-car"></i>&nbsp;&nbsp;<span>car</span></div>
<div class="col-sm-3" data-name="card-giftcard" data-code="f126"><i class="zmdi zmdi-card-giftcard"></i>&nbsp;&nbsp;<span>card-giftcard</span></div>
<div class="col-sm-3" data-name="card-membership" data-code="f127"><i class="zmdi zmdi-card-membership"></i>&nbsp;&nbsp;<span>card-membership</span></div>
<div class="col-sm-3" data-name="card-travel" data-code="f128"><i class="zmdi zmdi-card-travel"></i>&nbsp;&nbsp;<span>card-travel</span></div>
<div class="col-sm-3" data-name="card" data-code="f129"><i class="zmdi zmdi-card"></i>&nbsp;&nbsp;<span>card</span></div>
<div class="col-sm-3" data-name="case-check" data-code="f12a"><i class="zmdi zmdi-case-check"></i>&nbsp;&nbsp;<span>case-check</span></div>
<div class="col-sm-3" data-name="case-download" data-code="f12b"><i class="zmdi zmdi-case-download"></i>&nbsp;&nbsp;<span>case-download</span></div>
<div class="col-sm-3" data-name="case-play" data-code="f12c"><i class="zmdi zmdi-case-play"></i>&nbsp;&nbsp;<span>case-play</span></div>
<div class="col-sm-3" data-name="case" data-code="f12d"><i class="zmdi zmdi-case"></i>&nbsp;&nbsp;<span>case</span></div>
<div class="col-sm-3" data-name="cast-connected" data-code="f12e"><i class="zmdi zmdi-cast-connected"></i>&nbsp;&nbsp;<span>cast-connected</span></div>
<div class="col-sm-3" data-name="cast" data-code="f12f"><i class="zmdi zmdi-cast"></i>&nbsp;&nbsp;<span>cast</span></div>
<div class="col-sm-3" data-name="chart-donut" data-code="f130"><i class="zmdi zmdi-chart-donut"></i>&nbsp;&nbsp;<span>chart-donut</span></div>
<div class="col-sm-3" data-name="chart" data-code="f131"><i class="zmdi zmdi-chart"></i>&nbsp;&nbsp;<span>chart</span></div>
<div class="col-sm-3" data-name="city-alt" data-code="f132"><i class="zmdi zmdi-city-alt"></i>&nbsp;&nbsp;<span>city-alt</span></div>
<div class="col-sm-3" data-name="city" data-code="f133"><i class="zmdi zmdi-city"></i>&nbsp;&nbsp;<span>city</span></div>
<div class="col-sm-3" data-name="close-circle-o" data-code="f134"><i class="zmdi zmdi-close-circle-o"></i>&nbsp;&nbsp;<span>close-circle-o</span></div>
<div class="col-sm-3" data-name="close-circle" data-code="f135"><i class="zmdi zmdi-close-circle"></i>&nbsp;&nbsp;<span>close-circle</span></div>
<div class="col-sm-3" data-name="close" data-code="f136"><i class="zmdi zmdi-close"></i>&nbsp;&nbsp;<span>close</span></div>
<div class="col-sm-3" data-name="cocktail" data-code="f137"><i class="zmdi zmdi-cocktail"></i>&nbsp;&nbsp;<span>cocktail</span></div>
<div class="col-sm-3" data-name="code-setting" data-code="f138"><i class="zmdi zmdi-code-setting"></i>&nbsp;&nbsp;<span>code-setting</span></div>
<div class="col-sm-3" data-name="code-smartphone" data-code="f139"><i class="zmdi zmdi-code-smartphone"></i>&nbsp;&nbsp;<span>code-smartphone</span></div>
<div class="col-sm-3" data-name="code" data-code="f13a"><i class="zmdi zmdi-code"></i>&nbsp;&nbsp;<span>code</span></div>
<div class="col-sm-3" data-name="coffee" data-code="f13b"><i class="zmdi zmdi-coffee"></i>&nbsp;&nbsp;<span>coffee</span></div>
<div class="col-sm-3" data-name="collection-bookmark" data-code="f13c"><i class="zmdi zmdi-collection-bookmark"></i>&nbsp;&nbsp;<span>collection-bookmark</span></div>
<div class="col-sm-3" data-name="collection-case-play" data-code="f13d"><i class="zmdi zmdi-collection-case-play"></i>&nbsp;&nbsp;<span>collection-case-play</span></div>
<div class="col-sm-3" data-name="collection-folder-image" data-code="f13e"><i class="zmdi zmdi-collection-folder-image"></i>&nbsp;&nbsp;<span>collection-folder-image</span></div>
<div class="col-sm-3" data-name="collection-image-o" data-code="f13f"><i class="zmdi zmdi-collection-image-o"></i>&nbsp;&nbsp;<span>collection-image-o</span></div>
<div class="col-sm-3" data-name="collection-image" data-code="f140"><i class="zmdi zmdi-collection-image"></i>&nbsp;&nbsp;<span>collection-image</span></div>
<div class="col-sm-3" data-name="collection-item-1" data-code="f141"><i class="zmdi zmdi-collection-item-1"></i>&nbsp;&nbsp;<span>collection-item-1</span></div>
<div class="col-sm-3" data-name="collection-item-2" data-code="f142"><i class="zmdi zmdi-collection-item-2"></i>&nbsp;&nbsp;<span>collection-item-2</span></div>
<div class="col-sm-3" data-name="collection-item-3" data-code="f143"><i class="zmdi zmdi-collection-item-3"></i>&nbsp;&nbsp;<span>collection-item-3</span></div>
<div class="col-sm-3" data-name="collection-item-4" data-code="f144"><i class="zmdi zmdi-collection-item-4"></i>&nbsp;&nbsp;<span>collection-item-4</span></div>
<div class="col-sm-3" data-name="collection-item-5" data-code="f145"><i class="zmdi zmdi-collection-item-5"></i>&nbsp;&nbsp;<span>collection-item-5</span></div>
<div class="col-sm-3" data-name="collection-item-6" data-code="f146"><i class="zmdi zmdi-collection-item-6"></i>&nbsp;&nbsp;<span>collection-item-6</span></div>
<div class="col-sm-3" data-name="collection-item-7" data-code="f147"><i class="zmdi zmdi-collection-item-7"></i>&nbsp;&nbsp;<span>collection-item-7</span></div>
<div class="col-sm-3" data-name="collection-item-8" data-code="f148"><i class="zmdi zmdi-collection-item-8"></i>&nbsp;&nbsp;<span>collection-item-8</span></div>
<div class="col-sm-3" data-name="collection-item-9-plus" data-code="f149"><i class="zmdi zmdi-collection-item-9-plus"></i>&nbsp;&nbsp;<span>collection-item-9-plus</span></div>
<div class="col-sm-3" data-name="collection-item-9" data-code="f14a"><i class="zmdi zmdi-collection-item-9"></i>&nbsp;&nbsp;<span>collection-item-9</span></div>
<div class="col-sm-3" data-name="collection-item" data-code="f14b"><i class="zmdi zmdi-collection-item"></i>&nbsp;&nbsp;<span>collection-item</span></div>
<div class="col-sm-3" data-name="collection-music" data-code="f14c"><i class="zmdi zmdi-collection-music"></i>&nbsp;&nbsp;<span>collection-music</span></div>
<div class="col-sm-3" data-name="collection-pdf" data-code="f14d"><i class="zmdi zmdi-collection-pdf"></i>&nbsp;&nbsp;<span>collection-pdf</span></div>
<div class="col-sm-3" data-name="collection-plus" data-code="f14e"><i class="zmdi zmdi-collection-plus"></i>&nbsp;&nbsp;<span>collection-plus</span></div>
<div class="col-sm-3" data-name="collection-speaker" data-code="f14f"><i class="zmdi zmdi-collection-speaker"></i>&nbsp;&nbsp;<span>collection-speaker</span></div>
<div class="col-sm-3" data-name="collection-text" data-code="f150"><i class="zmdi zmdi-collection-text"></i>&nbsp;&nbsp;<span>collection-text</span></div>
<div class="col-sm-3" data-name="collection-video" data-code="f151"><i class="zmdi zmdi-collection-video"></i>&nbsp;&nbsp;<span>collection-video</span></div>
<div class="col-sm-3" data-name="compass" data-code="f152"><i class="zmdi zmdi-compass"></i>&nbsp;&nbsp;<span>compass</span></div>
<div class="col-sm-3" data-name="cutlery" data-code="f153"><i class="zmdi zmdi-cutlery"></i>&nbsp;&nbsp;<span>cutlery</span></div>
<div class="col-sm-3" data-name="delete" data-code="f154"><i class="zmdi zmdi-delete"></i>&nbsp;&nbsp;<span>delete</span></div>
<div class="col-sm-3" data-name="dialpad" data-code="f155"><i class="zmdi zmdi-dialpad"></i>&nbsp;&nbsp;<span>dialpad</span></div>
<div class="col-sm-3" data-name="dns" data-code="f156"><i class="zmdi zmdi-dns"></i>&nbsp;&nbsp;<span>dns</span></div>
<div class="col-sm-3" data-name="drink" data-code="f157"><i class="zmdi zmdi-drink"></i>&nbsp;&nbsp;<span>drink</span></div>
<div class="col-sm-3" data-name="edit" data-code="f158"><i class="zmdi zmdi-edit"></i>&nbsp;&nbsp;<span>edit</span></div>
<div class="col-sm-3" data-name="email-open" data-code="f159"><i class="zmdi zmdi-email-open"></i>&nbsp;&nbsp;<span>email-open</span></div>
<div class="col-sm-3" data-name="email" data-code="f15a"><i class="zmdi zmdi-email"></i>&nbsp;&nbsp;<span>email</span></div>
<div class="col-sm-3" data-name="eye-off" data-code="f15b"><i class="zmdi zmdi-eye-off"></i>&nbsp;&nbsp;<span>eye-off</span></div>
<div class="col-sm-3" data-name="eye" data-code="f15c"><i class="zmdi zmdi-eye"></i>&nbsp;&nbsp;<span>eye</span></div>
<div class="col-sm-3" data-name="eyedropper" data-code="f15d"><i class="zmdi zmdi-eyedropper"></i>&nbsp;&nbsp;<span>eyedropper</span></div>
<div class="col-sm-3" data-name="favorite-outline" data-code="f15e"><i class="zmdi zmdi-favorite-outline"></i>&nbsp;&nbsp;<span>favorite-outline</span></div>
<div class="col-sm-3" data-name="favorite" data-code="f15f"><i class="zmdi zmdi-favorite"></i>&nbsp;&nbsp;<span>favorite</span></div>
<div class="col-sm-3" data-name="filter-list" data-code="f160"><i class="zmdi zmdi-filter-list"></i>&nbsp;&nbsp;<span>filter-list</span></div>
<div class="col-sm-3" data-name="fire" data-code="f161"><i class="zmdi zmdi-fire"></i>&nbsp;&nbsp;<span>fire</span></div>
<div class="col-sm-3" data-name="flag" data-code="f162"><i class="zmdi zmdi-flag"></i>&nbsp;&nbsp;<span>flag</span></div>
<div class="col-sm-3" data-name="flare" data-code="f163"><i class="zmdi zmdi-flare"></i>&nbsp;&nbsp;<span>flare</span></div>
<div class="col-sm-3" data-name="flash-auto" data-code="f164"><i class="zmdi zmdi-flash-auto"></i>&nbsp;&nbsp;<span>flash-auto</span></div>
<div class="col-sm-3" data-name="flash-off" data-code="f165"><i class="zmdi zmdi-flash-off"></i>&nbsp;&nbsp;<span>flash-off</span></div>
<div class="col-sm-3" data-name="flash" data-code="f166"><i class="zmdi zmdi-flash"></i>&nbsp;&nbsp;<span>flash</span></div>
<div class="col-sm-3" data-name="flip" data-code="f167"><i class="zmdi zmdi-flip"></i>&nbsp;&nbsp;<span>flip</span></div>
<div class="col-sm-3" data-name="flower-alt" data-code="f168"><i class="zmdi zmdi-flower-alt"></i>&nbsp;&nbsp;<span>flower-alt</span></div>
<div class="col-sm-3" data-name="flower" data-code="f169"><i class="zmdi zmdi-flower"></i>&nbsp;&nbsp;<span>flower</span></div>
<div class="col-sm-3" data-name="font" data-code="f16a"><i class="zmdi zmdi-font"></i>&nbsp;&nbsp;<span>font</span></div>
<div class="col-sm-3" data-name="fullscreen-alt" data-code="f16b"><i class="zmdi zmdi-fullscreen-alt"></i>&nbsp;&nbsp;<span>fullscreen-alt</span></div>
<div class="col-sm-3" data-name="fullscreen-exit" data-code="f16c"><i class="zmdi zmdi-fullscreen-exit"></i>&nbsp;&nbsp;<span>fullscreen-exit</span></div>
<div class="col-sm-3" data-name="fullscreen" data-code="f16d"><i class="zmdi zmdi-fullscreen"></i>&nbsp;&nbsp;<span>fullscreen</span></div>
<div class="col-sm-3" data-name="functions" data-code="f16e"><i class="zmdi zmdi-functions"></i>&nbsp;&nbsp;<span>functions</span></div>
<div class="col-sm-3" data-name="gas-station" data-code="f16f"><i class="zmdi zmdi-gas-station"></i>&nbsp;&nbsp;<span>gas-station</span></div>
<div class="col-sm-3" data-name="gesture" data-code="f170"><i class="zmdi zmdi-gesture"></i>&nbsp;&nbsp;<span>gesture</span></div>
<div class="col-sm-3" data-name="globe-alt" data-code="f171"><i class="zmdi zmdi-globe-alt"></i>&nbsp;&nbsp;<span>globe-alt</span></div>
<div class="col-sm-3" data-name="globe-lock" data-code="f172"><i class="zmdi zmdi-globe-lock"></i>&nbsp;&nbsp;<span>globe-lock</span></div>
<div class="col-sm-3" data-name="globe" data-code="f173"><i class="zmdi zmdi-globe"></i>&nbsp;&nbsp;<span>globe</span></div>
<div class="col-sm-3" data-name="graduation-cap" data-code="f174"><i class="zmdi zmdi-graduation-cap"></i>&nbsp;&nbsp;<span>graduation-cap</span></div>
<div class="col-sm-3" data-name="group" data-code="f3e9"><i class="zmdi zmdi-group"></i>&nbsp;&nbsp;<span>group</span></div>
<div class="col-sm-3" data-name="home" data-code="f175"><i class="zmdi zmdi-home"></i>&nbsp;&nbsp;<span>home</span></div>
<div class="col-sm-3" data-name="hospital-alt" data-code="f176"><i class="zmdi zmdi-hospital-alt"></i>&nbsp;&nbsp;<span>hospital-alt</span></div>
<div class="col-sm-3" data-name="hospital" data-code="f177"><i class="zmdi zmdi-hospital"></i>&nbsp;&nbsp;<span>hospital</span></div>
<div class="col-sm-3" data-name="hotel" data-code="f178"><i class="zmdi zmdi-hotel"></i>&nbsp;&nbsp;<span>hotel</span></div>
<div class="col-sm-3" data-name="hourglass-alt" data-code="f179"><i class="zmdi zmdi-hourglass-alt"></i>&nbsp;&nbsp;<span>hourglass-alt</span></div>
<div class="col-sm-3" data-name="hourglass-outline" data-code="f17a"><i class="zmdi zmdi-hourglass-outline"></i>&nbsp;&nbsp;<span>hourglass-outline</span></div>
<div class="col-sm-3" data-name="hourglass" data-code="f17b"><i class="zmdi zmdi-hourglass"></i>&nbsp;&nbsp;<span>hourglass</span></div>
<div class="col-sm-3" data-name="http" data-code="f17c"><i class="zmdi zmdi-http"></i>&nbsp;&nbsp;<span>http</span></div>
<div class="col-sm-3" data-name="image-alt" data-code="f17d"><i class="zmdi zmdi-image-alt"></i>&nbsp;&nbsp;<span>image-alt</span></div>
<div class="col-sm-3" data-name="image-o" data-code="f17e"><i class="zmdi zmdi-image-o"></i>&nbsp;&nbsp;<span>image-o</span></div>
<div class="col-sm-3" data-name="image" data-code="f17f"><i class="zmdi zmdi-image"></i>&nbsp;&nbsp;<span>image</span></div>
<div class="col-sm-3" data-name="inbox" data-code="f180"><i class="zmdi zmdi-inbox"></i>&nbsp;&nbsp;<span>inbox</span></div>
<div class="col-sm-3" data-name="invert-colors-off" data-code="f181"><i class="zmdi zmdi-invert-colors-off"></i>&nbsp;&nbsp;<span>invert-colors-off</span></div>
<div class="col-sm-3" data-name="invert-colors" data-code="f182"><i class="zmdi zmdi-invert-colors"></i>&nbsp;&nbsp;<span>invert-colors</span></div>
<div class="col-sm-3" data-name="key" data-code="f183"><i class="zmdi zmdi-key"></i>&nbsp;&nbsp;<span>key</span></div>
<div class="col-sm-3" data-name="label-alt-outline" data-code="f184"><i class="zmdi zmdi-label-alt-outline"></i>&nbsp;&nbsp;<span>label-alt-outline</span></div>
<div class="col-sm-3" data-name="label-alt" data-code="f185"><i class="zmdi zmdi-label-alt"></i>&nbsp;&nbsp;<span>label-alt</span></div>
<div class="col-sm-3" data-name="label-heart" data-code="f186"><i class="zmdi zmdi-label-heart"></i>&nbsp;&nbsp;<span>label-heart</span></div>
<div class="col-sm-3" data-name="label" data-code="f187"><i class="zmdi zmdi-label"></i>&nbsp;&nbsp;<span>label</span></div>
<div class="col-sm-3" data-name="labels" data-code="f188"><i class="zmdi zmdi-labels"></i>&nbsp;&nbsp;<span>labels</span></div>
<div class="col-sm-3" data-name="lamp" data-code="f189"><i class="zmdi zmdi-lamp"></i>&nbsp;&nbsp;<span>lamp</span></div>
<div class="col-sm-3" data-name="landscape" data-code="f18a"><i class="zmdi zmdi-landscape"></i>&nbsp;&nbsp;<span>landscape</span></div>
<div class="col-sm-3" data-name="layers-off" data-code="f18b"><i class="zmdi zmdi-layers-off"></i>&nbsp;&nbsp;<span>layers-off</span></div>
<div class="col-sm-3" data-name="layers" data-code="f18c"><i class="zmdi zmdi-layers"></i>&nbsp;&nbsp;<span>layers</span></div>
<div class="col-sm-3" data-name="library" data-code="f18d"><i class="zmdi zmdi-library"></i>&nbsp;&nbsp;<span>library</span></div>
<div class="col-sm-3" data-name="link" data-code="f18e"><i class="zmdi zmdi-link"></i>&nbsp;&nbsp;<span>link</span></div>
<div class="col-sm-3" data-name="lock-open" data-code="f18f"><i class="zmdi zmdi-lock-open"></i>&nbsp;&nbsp;<span>lock-open</span></div>
<div class="col-sm-3" data-name="lock-outline" data-code="f190"><i class="zmdi zmdi-lock-outline"></i>&nbsp;&nbsp;<span>lock-outline</span></div>
<div class="col-sm-3" data-name="lock" data-code="f191"><i class="zmdi zmdi-lock"></i>&nbsp;&nbsp;<span>lock</span></div>
<div class="col-sm-3" data-name="mail-reply-all" data-code="f192"><i class="zmdi zmdi-mail-reply-all"></i>&nbsp;&nbsp;<span>mail-reply-all</span></div>
<div class="col-sm-3" data-name="mail-reply" data-code="f193"><i class="zmdi zmdi-mail-reply"></i>&nbsp;&nbsp;<span>mail-reply</span></div>
<div class="col-sm-3" data-name="mail-send" data-code="f194"><i class="zmdi zmdi-mail-send"></i>&nbsp;&nbsp;<span>mail-send</span></div>
<div class="col-sm-3" data-name="mall" data-code="f195"><i class="zmdi zmdi-mall"></i>&nbsp;&nbsp;<span>mall</span></div>
<div class="col-sm-3" data-name="map" data-code="f196"><i class="zmdi zmdi-map"></i>&nbsp;&nbsp;<span>map</span></div>
<div class="col-sm-3" data-name="menu" data-code="f197"><i class="zmdi zmdi-menu"></i>&nbsp;&nbsp;<span>menu</span></div>
<div class="col-sm-3" data-name="money-box" data-code="f198"><i class="zmdi zmdi-money-box"></i>&nbsp;&nbsp;<span>money-box</span></div>
<div class="col-sm-3" data-name="money-off" data-code="f199"><i class="zmdi zmdi-money-off"></i>&nbsp;&nbsp;<span>money-off</span></div>
<div class="col-sm-3" data-name="money" data-code="f19a"><i class="zmdi zmdi-money"></i>&nbsp;&nbsp;<span>money</span></div>
<div class="col-sm-3" data-name="more-vert" data-code="f19b"><i class="zmdi zmdi-more-vert"></i>&nbsp;&nbsp;<span>more-vert</span></div>
<div class="col-sm-3" data-name="more" data-code="f19c"><i class="zmdi zmdi-more"></i>&nbsp;&nbsp;<span>more</span></div>
<div class="col-sm-3" data-name="movie-alt" data-code="f19d"><i class="zmdi zmdi-movie-alt"></i>&nbsp;&nbsp;<span>movie-alt</span></div>
<div class="col-sm-3" data-name="movie" data-code="f19e"><i class="zmdi zmdi-movie"></i>&nbsp;&nbsp;<span>movie</span></div>
<div class="col-sm-3" data-name="nature-people" data-code="f19f"><i class="zmdi zmdi-nature-people"></i>&nbsp;&nbsp;<span>nature-people</span></div>
<div class="col-sm-3" data-name="nature" data-code="f1a0"><i class="zmdi zmdi-nature"></i>&nbsp;&nbsp;<span>nature</span></div>
<div class="col-sm-3" data-name="navigation" data-code="f1a1"><i class="zmdi zmdi-navigation"></i>&nbsp;&nbsp;<span>navigation</span></div>
<div class="col-sm-3" data-name="open-in-browser" data-code="f1a2"><i class="zmdi zmdi-open-in-browser"></i>&nbsp;&nbsp;<span>open-in-browser</span></div>
<div class="col-sm-3" data-name="open-in-new" data-code="f1a3"><i class="zmdi zmdi-open-in-new"></i>&nbsp;&nbsp;<span>open-in-new</span></div>
<div class="col-sm-3" data-name="palette" data-code="f1a4"><i class="zmdi zmdi-palette"></i>&nbsp;&nbsp;<span>palette</span></div>
<div class="col-sm-3" data-name="parking" data-code="f1a5"><i class="zmdi zmdi-parking"></i>&nbsp;&nbsp;<span>parking</span></div>
<div class="col-sm-3" data-name="pin-account" data-code="f1a6"><i class="zmdi zmdi-pin-account"></i>&nbsp;&nbsp;<span>pin-account</span></div>
<div class="col-sm-3" data-name="pin-assistant" data-code="f1a7"><i class="zmdi zmdi-pin-assistant"></i>&nbsp;&nbsp;<span>pin-assistant</span></div>
<div class="col-sm-3" data-name="pin-drop" data-code="f1a8"><i class="zmdi zmdi-pin-drop"></i>&nbsp;&nbsp;<span>pin-drop</span></div>
<div class="col-sm-3" data-name="pin-help" data-code="f1a9"><i class="zmdi zmdi-pin-help"></i>&nbsp;&nbsp;<span>pin-help</span></div>
<div class="col-sm-3" data-name="pin-off" data-code="f1aa"><i class="zmdi zmdi-pin-off"></i>&nbsp;&nbsp;<span>pin-off</span></div>
<div class="col-sm-3" data-name="pin" data-code="f1ab"><i class="zmdi zmdi-pin"></i>&nbsp;&nbsp;<span>pin</span></div>
<div class="col-sm-3" data-name="pizza" data-code="f1ac"><i class="zmdi zmdi-pizza"></i>&nbsp;&nbsp;<span>pizza</span></div>
<div class="col-sm-3" data-name="plaster" data-code="f1ad"><i class="zmdi zmdi-plaster"></i>&nbsp;&nbsp;<span>plaster</span></div>
<div class="col-sm-3" data-name="power-setting" data-code="f1ae"><i class="zmdi zmdi-power-setting"></i>&nbsp;&nbsp;<span>power-setting</span></div>
<div class="col-sm-3" data-name="power" data-code="f1af"><i class="zmdi zmdi-power"></i>&nbsp;&nbsp;<span>power</span></div>
<div class="col-sm-3" data-name="print" data-code="f1b0"><i class="zmdi zmdi-print"></i>&nbsp;&nbsp;<span>print</span></div>
<div class="col-sm-3" data-name="puzzle-piece" data-code="f1b1"><i class="zmdi zmdi-puzzle-piece"></i>&nbsp;&nbsp;<span>puzzle-piece</span></div>
<div class="col-sm-3" data-name="quote" data-code="f1b2"><i class="zmdi zmdi-quote"></i>&nbsp;&nbsp;<span>quote</span></div>
<div class="col-sm-3" data-name="railway" data-code="f1b3"><i class="zmdi zmdi-railway"></i>&nbsp;&nbsp;<span>railway</span></div>
<div class="col-sm-3" data-name="receipt" data-code="f1b4"><i class="zmdi zmdi-receipt"></i>&nbsp;&nbsp;<span>receipt</span></div>
<div class="col-sm-3" data-name="refresh-alt" data-code="f1b5"><i class="zmdi zmdi-refresh-alt"></i>&nbsp;&nbsp;<span>refresh-alt</span></div>
<div class="col-sm-3" data-name="refresh-sync-alert" data-code="f1b6"><i class="zmdi zmdi-refresh-sync-alert"></i>&nbsp;&nbsp;<span>refresh-sync-alert</span></div>
<div class="col-sm-3" data-name="refresh-sync-off" data-code="f1b7"><i class="zmdi zmdi-refresh-sync-off"></i>&nbsp;&nbsp;<span>refresh-sync-off</span></div>
<div class="col-sm-3" data-name="refresh-sync" data-code="f1b8"><i class="zmdi zmdi-refresh-sync"></i>&nbsp;&nbsp;<span>refresh-sync</span></div>
<div class="col-sm-3" data-name="refresh" data-code="f1b9"><i class="zmdi zmdi-refresh"></i>&nbsp;&nbsp;<span>refresh</span></div>
<div class="col-sm-3" data-name="roller" data-code="f1ba"><i class="zmdi zmdi-roller"></i>&nbsp;&nbsp;<span>roller</span></div>
<div class="col-sm-3" data-name="ruler" data-code="f1bb"><i class="zmdi zmdi-ruler"></i>&nbsp;&nbsp;<span>ruler</span></div>
<div class="col-sm-3" data-name="scissors" data-code="f1bc"><i class="zmdi zmdi-scissors"></i>&nbsp;&nbsp;<span>scissors</span></div>
<div class="col-sm-3" data-name="screen-rotation-lock" data-code="f1bd"><i class="zmdi zmdi-screen-rotation-lock"></i>&nbsp;&nbsp;<span>screen-rotation-lock</span></div>
<div class="col-sm-3" data-name="screen-rotation" data-code="f1be"><i class="zmdi zmdi-screen-rotation"></i>&nbsp;&nbsp;<span>screen-rotation</span></div>
<div class="col-sm-3" data-name="search-for" data-code="f1bf"><i class="zmdi zmdi-search-for"></i>&nbsp;&nbsp;<span>search-for</span></div>
<div class="col-sm-3" data-name="search-in-file" data-code="f1c0"><i class="zmdi zmdi-search-in-file"></i>&nbsp;&nbsp;<span>search-in-file</span></div>
<div class="col-sm-3" data-name="search-in-page" data-code="f1c1"><i class="zmdi zmdi-search-in-page"></i>&nbsp;&nbsp;<span>search-in-page</span></div>
<div class="col-sm-3" data-name="search-replace" data-code="f1c2"><i class="zmdi zmdi-search-replace"></i>&nbsp;&nbsp;<span>search-replace</span></div>
<div class="col-sm-3" data-name="search" data-code="f1c3"><i class="zmdi zmdi-search"></i>&nbsp;&nbsp;<span>search</span></div>
<div class="col-sm-3" data-name="seat" data-code="f1c4"><i class="zmdi zmdi-seat"></i>&nbsp;&nbsp;<span>seat</span></div>
<div class="col-sm-3" data-name="settings-square" data-code="f1c5"><i class="zmdi zmdi-settings-square"></i>&nbsp;&nbsp;<span>settings-square</span></div>
<div class="col-sm-3" data-name="settings" data-code="f1c6"><i class="zmdi zmdi-settings"></i>&nbsp;&nbsp;<span>settings</span></div>
<div class="col-sm-3" data-name="shape" data-code="f3eb"><i class="zmdi zmdi-shape"></i>&nbsp;&nbsp;<span>shape</span></div>
<div class="col-sm-3" data-name="shield-check" data-code="f1c7"><i class="zmdi zmdi-shield-check"></i>&nbsp;&nbsp;<span>shield-check</span></div>
<div class="col-sm-3" data-name="shield-security" data-code="f1c8"><i class="zmdi zmdi-shield-security"></i>&nbsp;&nbsp;<span>shield-security</span></div>
<div class="col-sm-3" data-name="shopping-basket" data-code="f1c9"><i class="zmdi zmdi-shopping-basket"></i>&nbsp;&nbsp;<span>shopping-basket</span></div>
<div class="col-sm-3" data-name="shopping-cart-plus" data-code="f1ca"><i class="zmdi zmdi-shopping-cart-plus"></i>&nbsp;&nbsp;<span>shopping-cart-plus</span></div>
<div class="col-sm-3" data-name="shopping-cart" data-code="f1cb"><i class="zmdi zmdi-shopping-cart"></i>&nbsp;&nbsp;<span>shopping-cart</span></div>
<div class="col-sm-3" data-name="sign-in" data-code="f1cc"><i class="zmdi zmdi-sign-in"></i>&nbsp;&nbsp;<span>sign-in</span></div>
<div class="col-sm-3" data-name="sort-amount-asc" data-code="f1cd"><i class="zmdi zmdi-sort-amount-asc"></i>&nbsp;&nbsp;<span>sort-amount-asc</span></div>
<div class="col-sm-3" data-name="sort-amount-desc" data-code="f1ce"><i class="zmdi zmdi-sort-amount-desc"></i>&nbsp;&nbsp;<span>sort-amount-desc</span></div>
<div class="col-sm-3" data-name="sort-asc" data-code="f1cf"><i class="zmdi zmdi-sort-asc"></i>&nbsp;&nbsp;<span>sort-asc</span></div>
<div class="col-sm-3" data-name="sort-desc" data-code="f1d0"><i class="zmdi zmdi-sort-desc"></i>&nbsp;&nbsp;<span>sort-desc</span></div>
<div class="col-sm-3" data-name="spellcheck" data-code="f1d1"><i class="zmdi zmdi-spellcheck"></i>&nbsp;&nbsp;<span>spellcheck</span></div>
<div class="col-sm-3" data-name="spinner" data-code="f3ec"><i class="zmdi zmdi-spinner"></i>&nbsp;&nbsp;<span>spinner</span></div>
<div class="col-sm-3" data-name="storage" data-code="f1d2"><i class="zmdi zmdi-storage"></i>&nbsp;&nbsp;<span>storage</span></div>
<div class="col-sm-3" data-name="store-24" data-code="f1d3"><i class="zmdi zmdi-store-24"></i>&nbsp;&nbsp;<span>store-24</span></div>
<div class="col-sm-3" data-name="store" data-code="f1d4"><i class="zmdi zmdi-store"></i>&nbsp;&nbsp;<span>store</span></div>
<div class="col-sm-3" data-name="subway" data-code="f1d5"><i class="zmdi zmdi-subway"></i>&nbsp;&nbsp;<span>subway</span></div>
<div class="col-sm-3" data-name="sun" data-code="f1d6"><i class="zmdi zmdi-sun"></i>&nbsp;&nbsp;<span>sun</span></div>
<div class="col-sm-3" data-name="tab-unselected" data-code="f1d7"><i class="zmdi zmdi-tab-unselected"></i>&nbsp;&nbsp;<span>tab-unselected</span></div>
<div class="col-sm-3" data-name="tab" data-code="f1d8"><i class="zmdi zmdi-tab"></i>&nbsp;&nbsp;<span>tab</span></div>
<div class="col-sm-3" data-name="tag-close" data-code="f1d9"><i class="zmdi zmdi-tag-close"></i>&nbsp;&nbsp;<span>tag-close</span></div>
<div class="col-sm-3" data-name="tag-more" data-code="f1da"><i class="zmdi zmdi-tag-more"></i>&nbsp;&nbsp;<span>tag-more</span></div>
<div class="col-sm-3" data-name="tag" data-code="f1db"><i class="zmdi zmdi-tag"></i>&nbsp;&nbsp;<span>tag</span></div>
<div class="col-sm-3" data-name="thumb-down" data-code="f1dc"><i class="zmdi zmdi-thumb-down"></i>&nbsp;&nbsp;<span>thumb-down</span></div>
<div class="col-sm-3" data-name="thumb-up-down" data-code="f1dd"><i class="zmdi zmdi-thumb-up-down"></i>&nbsp;&nbsp;<span>thumb-up-down</span></div>
<div class="col-sm-3" data-name="thumb-up" data-code="f1de"><i class="zmdi zmdi-thumb-up"></i>&nbsp;&nbsp;<span>thumb-up</span></div>
<div class="col-sm-3" data-name="ticket-star" data-code="f1df"><i class="zmdi zmdi-ticket-star"></i>&nbsp;&nbsp;<span>ticket-star</span></div>
<div class="col-sm-3" data-name="toll" data-code="f1e0"><i class="zmdi zmdi-toll"></i>&nbsp;&nbsp;<span>toll</span></div>
<div class="col-sm-3" data-name="toys" data-code="f1e1"><i class="zmdi zmdi-toys"></i>&nbsp;&nbsp;<span>toys</span></div>
<div class="col-sm-3" data-name="traffic" data-code="f1e2"><i class="zmdi zmdi-traffic"></i>&nbsp;&nbsp;<span>traffic</span></div>
<div class="col-sm-3" data-name="translate" data-code="f1e3"><i class="zmdi zmdi-translate"></i>&nbsp;&nbsp;<span>translate</span></div>
<div class="col-sm-3" data-name="triangle-down" data-code="f1e4"><i class="zmdi zmdi-triangle-down"></i>&nbsp;&nbsp;<span>triangle-down</span></div>
<div class="col-sm-3" data-name="triangle-up" data-code="f1e5"><i class="zmdi zmdi-triangle-up"></i>&nbsp;&nbsp;<span>triangle-up</span></div>
<div class="col-sm-3" data-name="truck" data-code="f1e6"><i class="zmdi zmdi-truck"></i>&nbsp;&nbsp;<span>truck</span></div>
<div class="col-sm-3" data-name="turning-sign" data-code="f1e7"><i class="zmdi zmdi-turning-sign"></i>&nbsp;&nbsp;<span>turning-sign</span></div>
<div class="col-sm-3" data-name="ungroup" data-code="f3ed"><i class="zmdi zmdi-ungroup"></i>&nbsp;&nbsp;<span>ungroup</span></div>
<div class="col-sm-3" data-name="wallpaper" data-code="f1e8"><i class="zmdi zmdi-wallpaper"></i>&nbsp;&nbsp;<span>wallpaper</span></div>
<div class="col-sm-3" data-name="washing-machine" data-code="f1e9"><i class="zmdi zmdi-washing-machine"></i>&nbsp;&nbsp;<span>washing-machine</span></div>
<div class="col-sm-3" data-name="window-maximize" data-code="f1ea"><i class="zmdi zmdi-window-maximize"></i>&nbsp;&nbsp;<span>window-maximize</span></div>
<div class="col-sm-3" data-name="window-minimize" data-code="f1eb"><i class="zmdi zmdi-window-minimize"></i>&nbsp;&nbsp;<span>window-minimize</span></div>
<div class="col-sm-3" data-name="window-restore" data-code="f1ec"><i class="zmdi zmdi-window-restore"></i>&nbsp;&nbsp;<span>window-restore</span></div>
<div class="col-sm-3" data-name="wrench" data-code="f1ed"><i class="zmdi zmdi-wrench"></i>&nbsp;&nbsp;<span>wrench</span></div>
<div class="col-sm-3" data-name="zoom-in" data-code="f1ee"><i class="zmdi zmdi-zoom-in"></i>&nbsp;&nbsp;<span>zoom-in</span></div>
<div class="col-sm-3" data-name="zoom-out" data-code="f1ef"><i class="zmdi zmdi-zoom-out"></i>&nbsp;&nbsp;<span>zoom-out</span></div>
<div class="col-sm-3" data-name="alert-circle-o" data-code="f1f0"><i class="zmdi zmdi-alert-circle-o"></i>&nbsp;&nbsp;<span>alert-circle-o</span></div>
<div class="col-sm-3" data-name="alert-circle" data-code="f1f1"><i class="zmdi zmdi-alert-circle"></i>&nbsp;&nbsp;<span>alert-circle</span></div>
<div class="col-sm-3" data-name="alert-octagon" data-code="f1f2"><i class="zmdi zmdi-alert-octagon"></i>&nbsp;&nbsp;<span>alert-octagon</span></div>
<div class="col-sm-3" data-name="alert-polygon" data-code="f1f3"><i class="zmdi zmdi-alert-polygon"></i>&nbsp;&nbsp;<span>alert-polygon</span></div>
<div class="col-sm-3" data-name="alert-triangle" data-code="f1f4"><i class="zmdi zmdi-alert-triangle"></i>&nbsp;&nbsp;<span>alert-triangle</span></div>
<div class="col-sm-3" data-name="help-outline" data-code="f1f5"><i class="zmdi zmdi-help-outline"></i>&nbsp;&nbsp;<span>help-outline</span></div>
<div class="col-sm-3" data-name="help" data-code="f1f6"><i class="zmdi zmdi-help"></i>&nbsp;&nbsp;<span>help</span></div>
<div class="col-sm-3" data-name="info-outline" data-code="f1f7"><i class="zmdi zmdi-info-outline"></i>&nbsp;&nbsp;<span>info-outline</span></div>
<div class="col-sm-3" data-name="info" data-code="f1f8"><i class="zmdi zmdi-info"></i>&nbsp;&nbsp;<span>info</span></div>
<div class="col-sm-3" data-name="notifications-active" data-code="f1f9"><i class="zmdi zmdi-notifications-active"></i>&nbsp;&nbsp;<span>notifications-active</span></div>
<div class="col-sm-3" data-name="notifications-add" data-code="f1fa"><i class="zmdi zmdi-notifications-add"></i>&nbsp;&nbsp;<span>notifications-add</span></div>
<div class="col-sm-3" data-name="notifications-none" data-code="f1fb"><i class="zmdi zmdi-notifications-none"></i>&nbsp;&nbsp;<span>notifications-none</span></div>
<div class="col-sm-3" data-name="notifications-off" data-code="f1fc"><i class="zmdi zmdi-notifications-off"></i>&nbsp;&nbsp;<span>notifications-off</span></div>
<div class="col-sm-3" data-name="notifications-paused" data-code="f1fd"><i class="zmdi zmdi-notifications-paused"></i>&nbsp;&nbsp;<span>notifications-paused</span></div>
<div class="col-sm-3" data-name="notifications" data-code="f1fe"><i class="zmdi zmdi-notifications"></i>&nbsp;&nbsp;<span>notifications</span></div>
<div class="col-sm-3" data-name="account-add" data-code="f1ff"><i class="zmdi zmdi-account-add"></i>&nbsp;&nbsp;<span>account-add</span></div>
<div class="col-sm-3" data-name="account-box-mail" data-code="f200"><i class="zmdi zmdi-account-box-mail"></i>&nbsp;&nbsp;<span>account-box-mail</span></div>
<div class="col-sm-3" data-name="account-box-o" data-code="f201"><i class="zmdi zmdi-account-box-o"></i>&nbsp;&nbsp;<span>account-box-o</span></div>
<div class="col-sm-3" data-name="account-box-phone" data-code="f202"><i class="zmdi zmdi-account-box-phone"></i>&nbsp;&nbsp;<span>account-box-phone</span></div>
<div class="col-sm-3" data-name="account-box" data-code="f203"><i class="zmdi zmdi-account-box"></i>&nbsp;&nbsp;<span>account-box</span></div>
<div class="col-sm-3" data-name="account-calendar" data-code="f204"><i class="zmdi zmdi-account-calendar"></i>&nbsp;&nbsp;<span>account-calendar</span></div>
<div class="col-sm-3" data-name="account-circle" data-code="f205"><i class="zmdi zmdi-account-circle"></i>&nbsp;&nbsp;<span>account-circle</span></div>
<div class="col-sm-3" data-name="account-o" data-code="f206"><i class="zmdi zmdi-account-o"></i>&nbsp;&nbsp;<span>account-o</span></div>
<div class="col-sm-3" data-name="account" data-code="f207"><i class="zmdi zmdi-account"></i>&nbsp;&nbsp;<span>account</span></div>
<div class="col-sm-3" data-name="accounts-add" data-code="f208"><i class="zmdi zmdi-accounts-add"></i>&nbsp;&nbsp;<span>accounts-add</span></div>
<div class="col-sm-3" data-name="accounts-alt" data-code="f209"><i class="zmdi zmdi-accounts-alt"></i>&nbsp;&nbsp;<span>accounts-alt</span></div>
<div class="col-sm-3" data-name="accounts-list-alt" data-code="f20a"><i class="zmdi zmdi-accounts-list-alt"></i>&nbsp;&nbsp;<span>accounts-list-alt</span></div>
<div class="col-sm-3" data-name="accounts-list" data-code="f20b"><i class="zmdi zmdi-accounts-list"></i>&nbsp;&nbsp;<span>accounts-list</span></div>
<div class="col-sm-3" data-name="accounts-outline" data-code="f20c"><i class="zmdi zmdi-accounts-outline"></i>&nbsp;&nbsp;<span>accounts-outline</span></div>
<div class="col-sm-3" data-name="accounts" data-code="f20d"><i class="zmdi zmdi-accounts"></i>&nbsp;&nbsp;<span>accounts</span></div>
<div class="col-sm-3" data-name="face" data-code="f20e"><i class="zmdi zmdi-face"></i>&nbsp;&nbsp;<span>face</span></div>
<div class="col-sm-3" data-name="female" data-code="f20f"><i class="zmdi zmdi-female"></i>&nbsp;&nbsp;<span>female</span></div>
<div class="col-sm-3" data-name="male-alt" data-code="f210"><i class="zmdi zmdi-male-alt"></i>&nbsp;&nbsp;<span>male-alt</span></div>
<div class="col-sm-3" data-name="male-female" data-code="f211"><i class="zmdi zmdi-male-female"></i>&nbsp;&nbsp;<span>male-female</span></div>
<div class="col-sm-3" data-name="male" data-code="f212"><i class="zmdi zmdi-male"></i>&nbsp;&nbsp;<span>male</span></div>
<div class="col-sm-3" data-name="mood-bad" data-code="f213"><i class="zmdi zmdi-mood-bad"></i>&nbsp;&nbsp;<span>mood-bad</span></div>
<div class="col-sm-3" data-name="mood" data-code="f214"><i class="zmdi zmdi-mood"></i>&nbsp;&nbsp;<span>mood</span></div>
<div class="col-sm-3" data-name="run" data-code="f215"><i class="zmdi zmdi-run"></i>&nbsp;&nbsp;<span>run</span></div>
<div class="col-sm-3" data-name="walk" data-code="f216"><i class="zmdi zmdi-walk"></i>&nbsp;&nbsp;<span>walk</span></div>
<div class="col-sm-3" data-name="cloud-box" data-code="f217"><i class="zmdi zmdi-cloud-box"></i>&nbsp;&nbsp;<span>cloud-box</span></div>
<div class="col-sm-3" data-name="cloud-circle" data-code="f218"><i class="zmdi zmdi-cloud-circle"></i>&nbsp;&nbsp;<span>cloud-circle</span></div>
<div class="col-sm-3" data-name="cloud-done" data-code="f219"><i class="zmdi zmdi-cloud-done"></i>&nbsp;&nbsp;<span>cloud-done</span></div>
<div class="col-sm-3" data-name="cloud-download" data-code="f21a"><i class="zmdi zmdi-cloud-download"></i>&nbsp;&nbsp;<span>cloud-download</span></div>
<div class="col-sm-3" data-name="cloud-off" data-code="f21b"><i class="zmdi zmdi-cloud-off"></i>&nbsp;&nbsp;<span>cloud-off</span></div>
<div class="col-sm-3" data-name="cloud-outline-alt" data-code="f21c"><i class="zmdi zmdi-cloud-outline-alt"></i>&nbsp;&nbsp;<span>cloud-outline-alt</span></div>
<div class="col-sm-3" data-name="cloud-outline" data-code="f21d"><i class="zmdi zmdi-cloud-outline"></i>&nbsp;&nbsp;<span>cloud-outline</span></div>
<div class="col-sm-3" data-name="cloud-upload" data-code="f21e"><i class="zmdi zmdi-cloud-upload"></i>&nbsp;&nbsp;<span>cloud-upload</span></div>
<div class="col-sm-3" data-name="cloud" data-code="f21f"><i class="zmdi zmdi-cloud"></i>&nbsp;&nbsp;<span>cloud</span></div>
<div class="col-sm-3" data-name="download" data-code="f220"><i class="zmdi zmdi-download"></i>&nbsp;&nbsp;<span>download</span></div>
<div class="col-sm-3" data-name="file-plus" data-code="f221"><i class="zmdi zmdi-file-plus"></i>&nbsp;&nbsp;<span>file-plus</span></div>
<div class="col-sm-3" data-name="file-text" data-code="f222"><i class="zmdi zmdi-file-text"></i>&nbsp;&nbsp;<span>file-text</span></div>
<div class="col-sm-3" data-name="file" data-code="f223"><i class="zmdi zmdi-file"></i>&nbsp;&nbsp;<span>file</span></div>
<div class="col-sm-3" data-name="folder-outline" data-code="f224"><i class="zmdi zmdi-folder-outline"></i>&nbsp;&nbsp;<span>folder-outline</span></div>
<div class="col-sm-3" data-name="folder-person" data-code="f225"><i class="zmdi zmdi-folder-person"></i>&nbsp;&nbsp;<span>folder-person</span></div>
<div class="col-sm-3" data-name="folder-star-alt" data-code="f226"><i class="zmdi zmdi-folder-star-alt"></i>&nbsp;&nbsp;<span>folder-star-alt</span></div>
<div class="col-sm-3" data-name="folder-star" data-code="f227"><i class="zmdi zmdi-folder-star"></i>&nbsp;&nbsp;<span>folder-star</span></div>
<div class="col-sm-3" data-name="folder" data-code="f228"><i class="zmdi zmdi-folder"></i>&nbsp;&nbsp;<span>folder</span></div>
<div class="col-sm-3" data-name="gif" data-code="f229"><i class="zmdi zmdi-gif"></i>&nbsp;&nbsp;<span>gif</span></div>
<div class="col-sm-3" data-name="upload" data-code="f22a"><i class="zmdi zmdi-upload"></i>&nbsp;&nbsp;<span>upload</span></div>
<div class="col-sm-3" data-name="border-all" data-code="f22b"><i class="zmdi zmdi-border-all"></i>&nbsp;&nbsp;<span>border-all</span></div>
<div class="col-sm-3" data-name="border-bottom" data-code="f22c"><i class="zmdi zmdi-border-bottom"></i>&nbsp;&nbsp;<span>border-bottom</span></div>
<div class="col-sm-3" data-name="border-clear" data-code="f22d"><i class="zmdi zmdi-border-clear"></i>&nbsp;&nbsp;<span>border-clear</span></div>
<div class="col-sm-3" data-name="border-color" data-code="f22e"><i class="zmdi zmdi-border-color"></i>&nbsp;&nbsp;<span>border-color</span></div>
<div class="col-sm-3" data-name="border-horizontal" data-code="f22f"><i class="zmdi zmdi-border-horizontal"></i>&nbsp;&nbsp;<span>border-horizontal</span></div>
<div class="col-sm-3" data-name="border-inner" data-code="f230"><i class="zmdi zmdi-border-inner"></i>&nbsp;&nbsp;<span>border-inner</span></div>
<div class="col-sm-3" data-name="border-left" data-code="f231"><i class="zmdi zmdi-border-left"></i>&nbsp;&nbsp;<span>border-left</span></div>
<div class="col-sm-3" data-name="border-outer" data-code="f232"><i class="zmdi zmdi-border-outer"></i>&nbsp;&nbsp;<span>border-outer</span></div>
<div class="col-sm-3" data-name="border-right" data-code="f233"><i class="zmdi zmdi-border-right"></i>&nbsp;&nbsp;<span>border-right</span></div>
<div class="col-sm-3" data-name="border-style" data-code="f234"><i class="zmdi zmdi-border-style"></i>&nbsp;&nbsp;<span>border-style</span></div>
<div class="col-sm-3" data-name="border-top" data-code="f235"><i class="zmdi zmdi-border-top"></i>&nbsp;&nbsp;<span>border-top</span></div>
<div class="col-sm-3" data-name="border-vertical" data-code="f236"><i class="zmdi zmdi-border-vertical"></i>&nbsp;&nbsp;<span>border-vertical</span></div>
<div class="col-sm-3" data-name="copy" data-code="f237"><i class="zmdi zmdi-copy"></i>&nbsp;&nbsp;<span>copy</span></div>
<div class="col-sm-3" data-name="crop" data-code="f238"><i class="zmdi zmdi-crop"></i>&nbsp;&nbsp;<span>crop</span></div>
<div class="col-sm-3" data-name="format-align-center" data-code="f239"><i class="zmdi zmdi-format-align-center"></i>&nbsp;&nbsp;<span>format-align-center</span></div>
<div class="col-sm-3" data-name="format-align-justify" data-code="f23a"><i class="zmdi zmdi-format-align-justify"></i>&nbsp;&nbsp;<span>format-align-justify</span></div>
<div class="col-sm-3" data-name="format-align-left" data-code="f23b"><i class="zmdi zmdi-format-align-left"></i>&nbsp;&nbsp;<span>format-align-left</span></div>
<div class="col-sm-3" data-name="format-align-right" data-code="f23c"><i class="zmdi zmdi-format-align-right"></i>&nbsp;&nbsp;<span>format-align-right</span></div>
<div class="col-sm-3" data-name="format-bold" data-code="f23d"><i class="zmdi zmdi-format-bold"></i>&nbsp;&nbsp;<span>format-bold</span></div>
<div class="col-sm-3" data-name="format-clear-all" data-code="f23e"><i class="zmdi zmdi-format-clear-all"></i>&nbsp;&nbsp;<span>format-clear-all</span></div>
<div class="col-sm-3" data-name="format-clear" data-code="f23f"><i class="zmdi zmdi-format-clear"></i>&nbsp;&nbsp;<span>format-clear</span></div>
<div class="col-sm-3" data-name="format-color-fill" data-code="f240"><i class="zmdi zmdi-format-color-fill"></i>&nbsp;&nbsp;<span>format-color-fill</span></div>
<div class="col-sm-3" data-name="format-color-reset" data-code="f241"><i class="zmdi zmdi-format-color-reset"></i>&nbsp;&nbsp;<span>format-color-reset</span></div>
<div class="col-sm-3" data-name="format-color-text" data-code="f242"><i class="zmdi zmdi-format-color-text"></i>&nbsp;&nbsp;<span>format-color-text</span></div>
<div class="col-sm-3" data-name="format-indent-decrease" data-code="f243"><i class="zmdi zmdi-format-indent-decrease"></i>&nbsp;&nbsp;<span>format-indent-decrease</span></div>
<div class="col-sm-3" data-name="format-indent-increase" data-code="f244"><i class="zmdi zmdi-format-indent-increase"></i>&nbsp;&nbsp;<span>format-indent-increase</span></div>
<div class="col-sm-3" data-name="format-italic" data-code="f245"><i class="zmdi zmdi-format-italic"></i>&nbsp;&nbsp;<span>format-italic</span></div>
<div class="col-sm-3" data-name="format-line-spacing" data-code="f246"><i class="zmdi zmdi-format-line-spacing"></i>&nbsp;&nbsp;<span>format-line-spacing</span></div>
<div class="col-sm-3" data-name="format-list-bulleted" data-code="f247"><i class="zmdi zmdi-format-list-bulleted"></i>&nbsp;&nbsp;<span>format-list-bulleted</span></div>
<div class="col-sm-3" data-name="format-list-numbered" data-code="f248"><i class="zmdi zmdi-format-list-numbered"></i>&nbsp;&nbsp;<span>format-list-numbered</span></div>
<div class="col-sm-3" data-name="format-ltr" data-code="f249"><i class="zmdi zmdi-format-ltr"></i>&nbsp;&nbsp;<span>format-ltr</span></div>
<div class="col-sm-3" data-name="format-rtl" data-code="f24a"><i class="zmdi zmdi-format-rtl"></i>&nbsp;&nbsp;<span>format-rtl</span></div>
<div class="col-sm-3" data-name="format-size" data-code="f24b"><i class="zmdi zmdi-format-size"></i>&nbsp;&nbsp;<span>format-size</span></div>
<div class="col-sm-3" data-name="format-strikethrough-s" data-code="f24c"><i class="zmdi zmdi-format-strikethrough-s"></i>&nbsp;&nbsp;<span>format-strikethrough-s</span></div>
<div class="col-sm-3" data-name="format-strikethrough" data-code="f24d"><i class="zmdi zmdi-format-strikethrough"></i>&nbsp;&nbsp;<span>format-strikethrough</span></div>
<div class="col-sm-3" data-name="format-subject" data-code="f24e"><i class="zmdi zmdi-format-subject"></i>&nbsp;&nbsp;<span>format-subject</span></div>
<div class="col-sm-3" data-name="format-underlined" data-code="f24f"><i class="zmdi zmdi-format-underlined"></i>&nbsp;&nbsp;<span>format-underlined</span></div>
<div class="col-sm-3" data-name="format-valign-bottom" data-code="f250"><i class="zmdi zmdi-format-valign-bottom"></i>&nbsp;&nbsp;<span>format-valign-bottom</span></div>
<div class="col-sm-3" data-name="format-valign-center" data-code="f251"><i class="zmdi zmdi-format-valign-center"></i>&nbsp;&nbsp;<span>format-valign-center</span></div>
<div class="col-sm-3" data-name="format-valign-top" data-code="f252"><i class="zmdi zmdi-format-valign-top"></i>&nbsp;&nbsp;<span>format-valign-top</span></div>
<div class="col-sm-3" data-name="redo" data-code="f253"><i class="zmdi zmdi-redo"></i>&nbsp;&nbsp;<span>redo</span></div>
<div class="col-sm-3" data-name="select-all" data-code="f254"><i class="zmdi zmdi-select-all"></i>&nbsp;&nbsp;<span>select-all</span></div>
<div class="col-sm-3" data-name="space-bar" data-code="f255"><i class="zmdi zmdi-space-bar"></i>&nbsp;&nbsp;<span>space-bar</span></div>
<div class="col-sm-3" data-name="text-format" data-code="f256"><i class="zmdi zmdi-text-format"></i>&nbsp;&nbsp;<span>text-format</span></div>
<div class="col-sm-3" data-name="transform" data-code="f257"><i class="zmdi zmdi-transform"></i>&nbsp;&nbsp;<span>transform</span></div>
<div class="col-sm-3" data-name="undo" data-code="f258"><i class="zmdi zmdi-undo"></i>&nbsp;&nbsp;<span>undo</span></div>
<div class="col-sm-3" data-name="wrap-text" data-code="f259"><i class="zmdi zmdi-wrap-text"></i>&nbsp;&nbsp;<span>wrap-text</span></div>
<div class="col-sm-3" data-name="comment-alert" data-code="f25a"><i class="zmdi zmdi-comment-alert"></i>&nbsp;&nbsp;<span>comment-alert</span></div>
<div class="col-sm-3" data-name="comment-alt-text" data-code="f25b"><i class="zmdi zmdi-comment-alt-text"></i>&nbsp;&nbsp;<span>comment-alt-text</span></div>
<div class="col-sm-3" data-name="comment-alt" data-code="f25c"><i class="zmdi zmdi-comment-alt"></i>&nbsp;&nbsp;<span>comment-alt</span></div>
<div class="col-sm-3" data-name="comment-edit" data-code="f25d"><i class="zmdi zmdi-comment-edit"></i>&nbsp;&nbsp;<span>comment-edit</span></div>
<div class="col-sm-3" data-name="comment-image" data-code="f25e"><i class="zmdi zmdi-comment-image"></i>&nbsp;&nbsp;<span>comment-image</span></div>
<div class="col-sm-3" data-name="comment-list" data-code="f25f"><i class="zmdi zmdi-comment-list"></i>&nbsp;&nbsp;<span>comment-list</span></div>
<div class="col-sm-3" data-name="comment-more" data-code="f260"><i class="zmdi zmdi-comment-more"></i>&nbsp;&nbsp;<span>comment-more</span></div>
<div class="col-sm-3" data-name="comment-outline" data-code="f261"><i class="zmdi zmdi-comment-outline"></i>&nbsp;&nbsp;<span>comment-outline</span></div>
<div class="col-sm-3" data-name="comment-text-alt" data-code="f262"><i class="zmdi zmdi-comment-text-alt"></i>&nbsp;&nbsp;<span>comment-text-alt</span></div>
<div class="col-sm-3" data-name="comment-text" data-code="f263"><i class="zmdi zmdi-comment-text"></i>&nbsp;&nbsp;<span>comment-text</span></div>
<div class="col-sm-3" data-name="comment-video" data-code="f264"><i class="zmdi zmdi-comment-video"></i>&nbsp;&nbsp;<span>comment-video</span></div>
<div class="col-sm-3" data-name="comment" data-code="f265"><i class="zmdi zmdi-comment"></i>&nbsp;&nbsp;<span>comment</span></div>
<div class="col-sm-3" data-name="comments" data-code="f266"><i class="zmdi zmdi-comments"></i>&nbsp;&nbsp;<span>comments</span></div>
<div class="col-sm-3" data-name="check-all" data-code="f267"><i class="zmdi zmdi-check-all"></i>&nbsp;&nbsp;<span>check-all</span></div>
<div class="col-sm-3" data-name="check-circle-u" data-code="f268"><i class="zmdi zmdi-check-circle-u"></i>&nbsp;&nbsp;<span>check-circle-u</span></div>
<div class="col-sm-3" data-name="check-circle" data-code="f269"><i class="zmdi zmdi-check-circle"></i>&nbsp;&nbsp;<span>check-circle</span></div>
<div class="col-sm-3" data-name="check-square" data-code="f26a"><i class="zmdi zmdi-check-square"></i>&nbsp;&nbsp;<span>check-square</span></div>
<div class="col-sm-3" data-name="check" data-code="f26b"><i class="zmdi zmdi-check"></i>&nbsp;&nbsp;<span>check</span></div>
<div class="col-sm-3" data-name="circle-o" data-code="f26c"><i class="zmdi zmdi-circle-o"></i>&nbsp;&nbsp;<span>circle-o</span></div>
<div class="col-sm-3" data-name="circle" data-code="f26d"><i class="zmdi zmdi-circle"></i>&nbsp;&nbsp;<span>circle</span></div>
<div class="col-sm-3" data-name="dot-circle-alt" data-code="f26e"><i class="zmdi zmdi-dot-circle-alt"></i>&nbsp;&nbsp;<span>dot-circle-alt</span></div>
<div class="col-sm-3" data-name="dot-circle" data-code="f26f"><i class="zmdi zmdi-dot-circle"></i>&nbsp;&nbsp;<span>dot-circle</span></div>
<div class="col-sm-3" data-name="minus-circle-outline" data-code="f270"><i class="zmdi zmdi-minus-circle-outline"></i>&nbsp;&nbsp;<span>minus-circle-outline</span></div>
<div class="col-sm-3" data-name="minus-circle" data-code="f271"><i class="zmdi zmdi-minus-circle"></i>&nbsp;&nbsp;<span>minus-circle</span></div>
<div class="col-sm-3" data-name="minus-square" data-code="f272"><i class="zmdi zmdi-minus-square"></i>&nbsp;&nbsp;<span>minus-square</span></div>
<div class="col-sm-3" data-name="minus" data-code="f273"><i class="zmdi zmdi-minus"></i>&nbsp;&nbsp;<span>minus</span></div>
<div class="col-sm-3" data-name="plus-circle-o-duplicate" data-code="f274"><i class="zmdi zmdi-plus-circle-o-duplicate"></i>&nbsp;&nbsp;<span>plus-circle-o-duplicate</span></div>
<div class="col-sm-3" data-name="plus-circle-o" data-code="f275"><i class="zmdi zmdi-plus-circle-o"></i>&nbsp;&nbsp;<span>plus-circle-o</span></div>
<div class="col-sm-3" data-name="plus-circle" data-code="f276"><i class="zmdi zmdi-plus-circle"></i>&nbsp;&nbsp;<span>plus-circle</span></div>
<div class="col-sm-3" data-name="plus-square" data-code="f277"><i class="zmdi zmdi-plus-square"></i>&nbsp;&nbsp;<span>plus-square</span></div>
<div class="col-sm-3" data-name="plus" data-code="f278"><i class="zmdi zmdi-plus"></i>&nbsp;&nbsp;<span>plus</span></div>
<div class="col-sm-3" data-name="square-o" data-code="f279"><i class="zmdi zmdi-square-o"></i>&nbsp;&nbsp;<span>square-o</span></div>
<div class="col-sm-3" data-name="star-circle" data-code="f27a"><i class="zmdi zmdi-star-circle"></i>&nbsp;&nbsp;<span>star-circle</span></div>
<div class="col-sm-3" data-name="star-half" data-code="f27b"><i class="zmdi zmdi-star-half"></i>&nbsp;&nbsp;<span>star-half</span></div>
<div class="col-sm-3" data-name="star-outline" data-code="f27c"><i class="zmdi zmdi-star-outline"></i>&nbsp;&nbsp;<span>star-outline</span></div>
<div class="col-sm-3" data-name="star" data-code="f27d"><i class="zmdi zmdi-star"></i>&nbsp;&nbsp;<span>star</span></div>
<div class="col-sm-3" data-name="bluetooth-connected" data-code="f27e"><i class="zmdi zmdi-bluetooth-connected"></i>&nbsp;&nbsp;<span>bluetooth-connected</span></div>
<div class="col-sm-3" data-name="bluetooth-off" data-code="f27f"><i class="zmdi zmdi-bluetooth-off"></i>&nbsp;&nbsp;<span>bluetooth-off</span></div>
<div class="col-sm-3" data-name="bluetooth-search" data-code="f280"><i class="zmdi zmdi-bluetooth-search"></i>&nbsp;&nbsp;<span>bluetooth-search</span></div>
<div class="col-sm-3" data-name="bluetooth-setting" data-code="f281"><i class="zmdi zmdi-bluetooth-setting"></i>&nbsp;&nbsp;<span>bluetooth-setting</span></div>
<div class="col-sm-3" data-name="bluetooth" data-code="f282"><i class="zmdi zmdi-bluetooth"></i>&nbsp;&nbsp;<span>bluetooth</span></div>
<div class="col-sm-3" data-name="camera-add" data-code="f283"><i class="zmdi zmdi-camera-add"></i>&nbsp;&nbsp;<span>camera-add</span></div>
<div class="col-sm-3" data-name="camera-alt" data-code="f284"><i class="zmdi zmdi-camera-alt"></i>&nbsp;&nbsp;<span>camera-alt</span></div>
<div class="col-sm-3" data-name="camera-bw" data-code="f285"><i class="zmdi zmdi-camera-bw"></i>&nbsp;&nbsp;<span>camera-bw</span></div>
<div class="col-sm-3" data-name="camera-front" data-code="f286"><i class="zmdi zmdi-camera-front"></i>&nbsp;&nbsp;<span>camera-front</span></div>
<div class="col-sm-3" data-name="camera-mic" data-code="f287"><i class="zmdi zmdi-camera-mic"></i>&nbsp;&nbsp;<span>camera-mic</span></div>
<div class="col-sm-3" data-name="camera-party-mode" data-code="f288"><i class="zmdi zmdi-camera-party-mode"></i>&nbsp;&nbsp;<span>camera-party-mode</span></div>
<div class="col-sm-3" data-name="camera-rear" data-code="f289"><i class="zmdi zmdi-camera-rear"></i>&nbsp;&nbsp;<span>camera-rear</span></div>
<div class="col-sm-3" data-name="camera-roll" data-code="f28a"><i class="zmdi zmdi-camera-roll"></i>&nbsp;&nbsp;<span>camera-roll</span></div>
<div class="col-sm-3" data-name="camera-switch" data-code="f28b"><i class="zmdi zmdi-camera-switch"></i>&nbsp;&nbsp;<span>camera-switch</span></div>
<div class="col-sm-3" data-name="camera" data-code="f28c"><i class="zmdi zmdi-camera"></i>&nbsp;&nbsp;<span>camera</span></div>
<div class="col-sm-3" data-name="card-alert" data-code="f28d"><i class="zmdi zmdi-card-alert"></i>&nbsp;&nbsp;<span>card-alert</span></div>
<div class="col-sm-3" data-name="card-off" data-code="f28e"><i class="zmdi zmdi-card-off"></i>&nbsp;&nbsp;<span>card-off</span></div>
<div class="col-sm-3" data-name="card-sd" data-code="f28f"><i class="zmdi zmdi-card-sd"></i>&nbsp;&nbsp;<span>card-sd</span></div>
<div class="col-sm-3" data-name="card-sim" data-code="f290"><i class="zmdi zmdi-card-sim"></i>&nbsp;&nbsp;<span>card-sim</span></div>
<div class="col-sm-3" data-name="desktop-mac" data-code="f291"><i class="zmdi zmdi-desktop-mac"></i>&nbsp;&nbsp;<span>desktop-mac</span></div>
<div class="col-sm-3" data-name="desktop-windows" data-code="f292"><i class="zmdi zmdi-desktop-windows"></i>&nbsp;&nbsp;<span>desktop-windows</span></div>
<div class="col-sm-3" data-name="device-hub" data-code="f293"><i class="zmdi zmdi-device-hub"></i>&nbsp;&nbsp;<span>device-hub</span></div>
<div class="col-sm-3" data-name="devices-off" data-code="f294"><i class="zmdi zmdi-devices-off"></i>&nbsp;&nbsp;<span>devices-off</span></div>
<div class="col-sm-3" data-name="devices" data-code="f295"><i class="zmdi zmdi-devices"></i>&nbsp;&nbsp;<span>devices</span></div>
<div class="col-sm-3" data-name="dock" data-code="f296"><i class="zmdi zmdi-dock"></i>&nbsp;&nbsp;<span>dock</span></div>
<div class="col-sm-3" data-name="floppy" data-code="f297"><i class="zmdi zmdi-floppy"></i>&nbsp;&nbsp;<span>floppy</span></div>
<div class="col-sm-3" data-name="gamepad" data-code="f298"><i class="zmdi zmdi-gamepad"></i>&nbsp;&nbsp;<span>gamepad</span></div>
<div class="col-sm-3" data-name="gps-dot" data-code="f299"><i class="zmdi zmdi-gps-dot"></i>&nbsp;&nbsp;<span>gps-dot</span></div>
<div class="col-sm-3" data-name="gps-off" data-code="f29a"><i class="zmdi zmdi-gps-off"></i>&nbsp;&nbsp;<span>gps-off</span></div>
<div class="col-sm-3" data-name="gps" data-code="f29b"><i class="zmdi zmdi-gps"></i>&nbsp;&nbsp;<span>gps</span></div>
<div class="col-sm-3" data-name="headset-mic" data-code="f29c"><i class="zmdi zmdi-headset-mic"></i>&nbsp;&nbsp;<span>headset-mic</span></div>
<div class="col-sm-3" data-name="headset" data-code="f29d"><i class="zmdi zmdi-headset"></i>&nbsp;&nbsp;<span>headset</span></div>
<div class="col-sm-3" data-name="input-antenna" data-code="f29e"><i class="zmdi zmdi-input-antenna"></i>&nbsp;&nbsp;<span>input-antenna</span></div>
<div class="col-sm-3" data-name="input-composite" data-code="f29f"><i class="zmdi zmdi-input-composite"></i>&nbsp;&nbsp;<span>input-composite</span></div>
<div class="col-sm-3" data-name="input-hdmi" data-code="f2a0"><i class="zmdi zmdi-input-hdmi"></i>&nbsp;&nbsp;<span>input-hdmi</span></div>
<div class="col-sm-3" data-name="input-power" data-code="f2a1"><i class="zmdi zmdi-input-power"></i>&nbsp;&nbsp;<span>input-power</span></div>
<div class="col-sm-3" data-name="input-svideo" data-code="f2a2"><i class="zmdi zmdi-input-svideo"></i>&nbsp;&nbsp;<span>input-svideo</span></div>
<div class="col-sm-3" data-name="keyboard-hide" data-code="f2a3"><i class="zmdi zmdi-keyboard-hide"></i>&nbsp;&nbsp;<span>keyboard-hide</span></div>
<div class="col-sm-3" data-name="keyboard" data-code="f2a4"><i class="zmdi zmdi-keyboard"></i>&nbsp;&nbsp;<span>keyboard</span></div>
<div class="col-sm-3" data-name="laptop-chromebook" data-code="f2a5"><i class="zmdi zmdi-laptop-chromebook"></i>&nbsp;&nbsp;<span>laptop-chromebook</span></div>
<div class="col-sm-3" data-name="laptop-mac" data-code="f2a6"><i class="zmdi zmdi-laptop-mac"></i>&nbsp;&nbsp;<span>laptop-mac</span></div>
<div class="col-sm-3" data-name="laptop" data-code="f2a7"><i class="zmdi zmdi-laptop"></i>&nbsp;&nbsp;<span>laptop</span></div>
<div class="col-sm-3" data-name="mic-off" data-code="f2a8"><i class="zmdi zmdi-mic-off"></i>&nbsp;&nbsp;<span>mic-off</span></div>
<div class="col-sm-3" data-name="mic-outline" data-code="f2a9"><i class="zmdi zmdi-mic-outline"></i>&nbsp;&nbsp;<span>mic-outline</span></div>
<div class="col-sm-3" data-name="mic-setting" data-code="f2aa"><i class="zmdi zmdi-mic-setting"></i>&nbsp;&nbsp;<span>mic-setting</span></div>
<div class="col-sm-3" data-name="mic" data-code="f2ab"><i class="zmdi zmdi-mic"></i>&nbsp;&nbsp;<span>mic</span></div>
<div class="col-sm-3" data-name="mouse" data-code="f2ac"><i class="zmdi zmdi-mouse"></i>&nbsp;&nbsp;<span>mouse</span></div>
<div class="col-sm-3" data-name="network-alert" data-code="f2ad"><i class="zmdi zmdi-network-alert"></i>&nbsp;&nbsp;<span>network-alert</span></div>
<div class="col-sm-3" data-name="network-locked" data-code="f2ae"><i class="zmdi zmdi-network-locked"></i>&nbsp;&nbsp;<span>network-locked</span></div>
<div class="col-sm-3" data-name="network-off" data-code="f2af"><i class="zmdi zmdi-network-off"></i>&nbsp;&nbsp;<span>network-off</span></div>
<div class="col-sm-3" data-name="network-outline" data-code="f2b0"><i class="zmdi zmdi-network-outline"></i>&nbsp;&nbsp;<span>network-outline</span></div>
<div class="col-sm-3" data-name="network-setting" data-code="f2b1"><i class="zmdi zmdi-network-setting"></i>&nbsp;&nbsp;<span>network-setting</span></div>
<div class="col-sm-3" data-name="network" data-code="f2b2"><i class="zmdi zmdi-network"></i>&nbsp;&nbsp;<span>network</span></div>
<div class="col-sm-3" data-name="phone-bluetooth" data-code="f2b3"><i class="zmdi zmdi-phone-bluetooth"></i>&nbsp;&nbsp;<span>phone-bluetooth</span></div>
<div class="col-sm-3" data-name="phone-end" data-code="f2b4"><i class="zmdi zmdi-phone-end"></i>&nbsp;&nbsp;<span>phone-end</span></div>
<div class="col-sm-3" data-name="phone-forwarded" data-code="f2b5"><i class="zmdi zmdi-phone-forwarded"></i>&nbsp;&nbsp;<span>phone-forwarded</span></div>
<div class="col-sm-3" data-name="phone-in-talk" data-code="f2b6"><i class="zmdi zmdi-phone-in-talk"></i>&nbsp;&nbsp;<span>phone-in-talk</span></div>
<div class="col-sm-3" data-name="phone-locked" data-code="f2b7"><i class="zmdi zmdi-phone-locked"></i>&nbsp;&nbsp;<span>phone-locked</span></div>
<div class="col-sm-3" data-name="phone-missed" data-code="f2b8"><i class="zmdi zmdi-phone-missed"></i>&nbsp;&nbsp;<span>phone-missed</span></div>
<div class="col-sm-3" data-name="phone-msg" data-code="f2b9"><i class="zmdi zmdi-phone-msg"></i>&nbsp;&nbsp;<span>phone-msg</span></div>
<div class="col-sm-3" data-name="phone-paused" data-code="f2ba"><i class="zmdi zmdi-phone-paused"></i>&nbsp;&nbsp;<span>phone-paused</span></div>
<div class="col-sm-3" data-name="phone-ring" data-code="f2bb"><i class="zmdi zmdi-phone-ring"></i>&nbsp;&nbsp;<span>phone-ring</span></div>
<div class="col-sm-3" data-name="phone-setting" data-code="f2bc"><i class="zmdi zmdi-phone-setting"></i>&nbsp;&nbsp;<span>phone-setting</span></div>
<div class="col-sm-3" data-name="phone-sip" data-code="f2bd"><i class="zmdi zmdi-phone-sip"></i>&nbsp;&nbsp;<span>phone-sip</span></div>
<div class="col-sm-3" data-name="phone" data-code="f2be"><i class="zmdi zmdi-phone"></i>&nbsp;&nbsp;<span>phone</span></div>
<div class="col-sm-3" data-name="portable-wifi-changes" data-code="f2bf"><i class="zmdi zmdi-portable-wifi-changes"></i>&nbsp;&nbsp;<span>portable-wifi-changes</span></div>
<div class="col-sm-3" data-name="portable-wifi-off" data-code="f2c0"><i class="zmdi zmdi-portable-wifi-off"></i>&nbsp;&nbsp;<span>portable-wifi-off</span></div>
<div class="col-sm-3" data-name="portable-wifi" data-code="f2c1"><i class="zmdi zmdi-portable-wifi"></i>&nbsp;&nbsp;<span>portable-wifi</span></div>
<div class="col-sm-3" data-name="radio" data-code="f2c2"><i class="zmdi zmdi-radio"></i>&nbsp;&nbsp;<span>radio</span></div>
<div class="col-sm-3" data-name="reader" data-code="f2c3"><i class="zmdi zmdi-reader"></i>&nbsp;&nbsp;<span>reader</span></div>
<div class="col-sm-3" data-name="remote-control-alt" data-code="f2c4"><i class="zmdi zmdi-remote-control-alt"></i>&nbsp;&nbsp;<span>remote-control-alt</span></div>
<div class="col-sm-3" data-name="remote-control" data-code="f2c5"><i class="zmdi zmdi-remote-control"></i>&nbsp;&nbsp;<span>remote-control</span></div>
<div class="col-sm-3" data-name="router" data-code="f2c6"><i class="zmdi zmdi-router"></i>&nbsp;&nbsp;<span>router</span></div>
<div class="col-sm-3" data-name="scanner" data-code="f2c7"><i class="zmdi zmdi-scanner"></i>&nbsp;&nbsp;<span>scanner</span></div>
<div class="col-sm-3" data-name="smartphone-android" data-code="f2c8"><i class="zmdi zmdi-smartphone-android"></i>&nbsp;&nbsp;<span>smartphone-android</span></div>
<div class="col-sm-3" data-name="smartphone-download" data-code="f2c9"><i class="zmdi zmdi-smartphone-download"></i>&nbsp;&nbsp;<span>smartphone-download</span></div>
<div class="col-sm-3" data-name="smartphone-erase" data-code="f2ca"><i class="zmdi zmdi-smartphone-erase"></i>&nbsp;&nbsp;<span>smartphone-erase</span></div>
<div class="col-sm-3" data-name="smartphone-info" data-code="f2cb"><i class="zmdi zmdi-smartphone-info"></i>&nbsp;&nbsp;<span>smartphone-info</span></div>
<div class="col-sm-3" data-name="smartphone-iphone" data-code="f2cc"><i class="zmdi zmdi-smartphone-iphone"></i>&nbsp;&nbsp;<span>smartphone-iphone</span></div>
<div class="col-sm-3" data-name="smartphone-landscape-lock" data-code="f2cd"><i class="zmdi zmdi-smartphone-landscape-lock"></i>&nbsp;&nbsp;<span>smartphone-landscape-lock</span></div>
<div class="col-sm-3" data-name="smartphone-landscape" data-code="f2ce"><i class="zmdi zmdi-smartphone-landscape"></i>&nbsp;&nbsp;<span>smartphone-landscape</span></div>
<div class="col-sm-3" data-name="smartphone-lock" data-code="f2cf"><i class="zmdi zmdi-smartphone-lock"></i>&nbsp;&nbsp;<span>smartphone-lock</span></div>
<div class="col-sm-3" data-name="smartphone-portrait-lock" data-code="f2d0"><i class="zmdi zmdi-smartphone-portrait-lock"></i>&nbsp;&nbsp;<span>smartphone-portrait-lock</span></div>
<div class="col-sm-3" data-name="smartphone-ring" data-code="f2d1"><i class="zmdi zmdi-smartphone-ring"></i>&nbsp;&nbsp;<span>smartphone-ring</span></div>
<div class="col-sm-3" data-name="smartphone-setting" data-code="f2d2"><i class="zmdi zmdi-smartphone-setting"></i>&nbsp;&nbsp;<span>smartphone-setting</span></div>
<div class="col-sm-3" data-name="smartphone-setup" data-code="f2d3"><i class="zmdi zmdi-smartphone-setup"></i>&nbsp;&nbsp;<span>smartphone-setup</span></div>
<div class="col-sm-3" data-name="smartphone" data-code="f2d4"><i class="zmdi zmdi-smartphone"></i>&nbsp;&nbsp;<span>smartphone</span></div>
<div class="col-sm-3" data-name="speaker" data-code="f2d5"><i class="zmdi zmdi-speaker"></i>&nbsp;&nbsp;<span>speaker</span></div>
<div class="col-sm-3" data-name="tablet-android" data-code="f2d6"><i class="zmdi zmdi-tablet-android"></i>&nbsp;&nbsp;<span>tablet-android</span></div>
<div class="col-sm-3" data-name="tablet-mac" data-code="f2d7"><i class="zmdi zmdi-tablet-mac"></i>&nbsp;&nbsp;<span>tablet-mac</span></div>
<div class="col-sm-3" data-name="tablet" data-code="f2d8"><i class="zmdi zmdi-tablet"></i>&nbsp;&nbsp;<span>tablet</span></div>
<div class="col-sm-3" data-name="tv-alt-play" data-code="f2d9"><i class="zmdi zmdi-tv-alt-play"></i>&nbsp;&nbsp;<span>tv-alt-play</span></div>
<div class="col-sm-3" data-name="tv-list" data-code="f2da"><i class="zmdi zmdi-tv-list"></i>&nbsp;&nbsp;<span>tv-list</span></div>
<div class="col-sm-3" data-name="tv-play" data-code="f2db"><i class="zmdi zmdi-tv-play"></i>&nbsp;&nbsp;<span>tv-play</span></div>
<div class="col-sm-3" data-name="tv" data-code="f2dc"><i class="zmdi zmdi-tv"></i>&nbsp;&nbsp;<span>tv</span></div>
<div class="col-sm-3" data-name="usb" data-code="f2dd"><i class="zmdi zmdi-usb"></i>&nbsp;&nbsp;<span>usb</span></div>
<div class="col-sm-3" data-name="videocam-off" data-code="f2de"><i class="zmdi zmdi-videocam-off"></i>&nbsp;&nbsp;<span>videocam-off</span></div>
<div class="col-sm-3" data-name="videocam-switch" data-code="f2df"><i class="zmdi zmdi-videocam-switch"></i>&nbsp;&nbsp;<span>videocam-switch</span></div>
<div class="col-sm-3" data-name="videocam" data-code="f2e0"><i class="zmdi zmdi-videocam"></i>&nbsp;&nbsp;<span>videocam</span></div>
<div class="col-sm-3" data-name="watch" data-code="f2e1"><i class="zmdi zmdi-watch"></i>&nbsp;&nbsp;<span>watch</span></div>
<div class="col-sm-3" data-name="wifi-alt-2" data-code="f2e2"><i class="zmdi zmdi-wifi-alt-2"></i>&nbsp;&nbsp;<span>wifi-alt-2</span></div>
<div class="col-sm-3" data-name="wifi-alt" data-code="f2e3"><i class="zmdi zmdi-wifi-alt"></i>&nbsp;&nbsp;<span>wifi-alt</span></div>
<div class="col-sm-3" data-name="wifi-info" data-code="f2e4"><i class="zmdi zmdi-wifi-info"></i>&nbsp;&nbsp;<span>wifi-info</span></div>
<div class="col-sm-3" data-name="wifi-lock" data-code="f2e5"><i class="zmdi zmdi-wifi-lock"></i>&nbsp;&nbsp;<span>wifi-lock</span></div>
<div class="col-sm-3" data-name="wifi-off" data-code="f2e6"><i class="zmdi zmdi-wifi-off"></i>&nbsp;&nbsp;<span>wifi-off</span></div>
<div class="col-sm-3" data-name="wifi-outline" data-code="f2e7"><i class="zmdi zmdi-wifi-outline"></i>&nbsp;&nbsp;<span>wifi-outline</span></div>
<div class="col-sm-3" data-name="wifi" data-code="f2e8"><i class="zmdi zmdi-wifi"></i>&nbsp;&nbsp;<span>wifi</span></div>
<div class="col-sm-3" data-name="arrow-left-bottom" data-code="f2e9"><i class="zmdi zmdi-arrow-left-bottom"></i>&nbsp;&nbsp;<span>arrow-left-bottom</span></div>
<div class="col-sm-3" data-name="arrow-left" data-code="f2ea"><i class="zmdi zmdi-arrow-left"></i>&nbsp;&nbsp;<span>arrow-left</span></div>
<div class="col-sm-3" data-name="arrow-merge" data-code="f2eb"><i class="zmdi zmdi-arrow-merge"></i>&nbsp;&nbsp;<span>arrow-merge</span></div>
<div class="col-sm-3" data-name="arrow-missed" data-code="f2ec"><i class="zmdi zmdi-arrow-missed"></i>&nbsp;&nbsp;<span>arrow-missed</span></div>
<div class="col-sm-3" data-name="arrow-right-top" data-code="f2ed"><i class="zmdi zmdi-arrow-right-top"></i>&nbsp;&nbsp;<span>arrow-right-top</span></div>
<div class="col-sm-3" data-name="arrow-right" data-code="f2ee"><i class="zmdi zmdi-arrow-right"></i>&nbsp;&nbsp;<span>arrow-right</span></div>
<div class="col-sm-3" data-name="arrow-split" data-code="f2ef"><i class="zmdi zmdi-arrow-split"></i>&nbsp;&nbsp;<span>arrow-split</span></div>
<div class="col-sm-3" data-name="arrows" data-code="f2f0"><i class="zmdi zmdi-arrows"></i>&nbsp;&nbsp;<span>arrows</span></div>
<div class="col-sm-3" data-name="caret-down-circle" data-code="f2f1"><i class="zmdi zmdi-caret-down-circle"></i>&nbsp;&nbsp;<span>caret-down-circle</span></div>
<div class="col-sm-3" data-name="caret-down" data-code="f2f2"><i class="zmdi zmdi-caret-down"></i>&nbsp;&nbsp;<span>caret-down</span></div>
<div class="col-sm-3" data-name="caret-left-circle" data-code="f2f3"><i class="zmdi zmdi-caret-left-circle"></i>&nbsp;&nbsp;<span>caret-left-circle</span></div>
<div class="col-sm-3" data-name="caret-left" data-code="f2f4"><i class="zmdi zmdi-caret-left"></i>&nbsp;&nbsp;<span>caret-left</span></div>
<div class="col-sm-3" data-name="caret-right-circle" data-code="f2f5"><i class="zmdi zmdi-caret-right-circle"></i>&nbsp;&nbsp;<span>caret-right-circle</span></div>
<div class="col-sm-3" data-name="caret-right" data-code="f2f6"><i class="zmdi zmdi-caret-right"></i>&nbsp;&nbsp;<span>caret-right</span></div>
<div class="col-sm-3" data-name="caret-up-circle" data-code="f2f7"><i class="zmdi zmdi-caret-up-circle"></i>&nbsp;&nbsp;<span>caret-up-circle</span></div>
<div class="col-sm-3" data-name="caret-up" data-code="f2f8"><i class="zmdi zmdi-caret-up"></i>&nbsp;&nbsp;<span>caret-up</span></div>
<div class="col-sm-3" data-name="chevron-down" data-code="f2f9"><i class="zmdi zmdi-chevron-down"></i>&nbsp;&nbsp;<span>chevron-down</span></div>
<div class="col-sm-3" data-name="chevron-left" data-code="f2fa"><i class="zmdi zmdi-chevron-left"></i>&nbsp;&nbsp;<span>chevron-left</span></div>
<div class="col-sm-3" data-name="chevron-right" data-code="f2fb"><i class="zmdi zmdi-chevron-right"></i>&nbsp;&nbsp;<span>chevron-right</span></div>
<div class="col-sm-3" data-name="chevron-up" data-code="f2fc"><i class="zmdi zmdi-chevron-up"></i>&nbsp;&nbsp;<span>chevron-up</span></div>
<div class="col-sm-3" data-name="forward" data-code="f2fd"><i class="zmdi zmdi-forward"></i>&nbsp;&nbsp;<span>forward</span></div>
<div class="col-sm-3" data-name="long-arrow-down" data-code="f2fe"><i class="zmdi zmdi-long-arrow-down"></i>&nbsp;&nbsp;<span>long-arrow-down</span></div>
<div class="col-sm-3" data-name="long-arrow-left" data-code="f2ff"><i class="zmdi zmdi-long-arrow-left"></i>&nbsp;&nbsp;<span>long-arrow-left</span></div>
<div class="col-sm-3" data-name="long-arrow-return" data-code="f300"><i class="zmdi zmdi-long-arrow-return"></i>&nbsp;&nbsp;<span>long-arrow-return</span></div>
<div class="col-sm-3" data-name="long-arrow-right" data-code="f301"><i class="zmdi zmdi-long-arrow-right"></i>&nbsp;&nbsp;<span>long-arrow-right</span></div>
<div class="col-sm-3" data-name="long-arrow-tab" data-code="f302"><i class="zmdi zmdi-long-arrow-tab"></i>&nbsp;&nbsp;<span>long-arrow-tab</span></div>
<div class="col-sm-3" data-name="long-arrow-up" data-code="f303"><i class="zmdi zmdi-long-arrow-up"></i>&nbsp;&nbsp;<span>long-arrow-up</span></div>
<div class="col-sm-3" data-name="rotate-ccw" data-code="f304"><i class="zmdi zmdi-rotate-ccw"></i>&nbsp;&nbsp;<span>rotate-ccw</span></div>
<div class="col-sm-3" data-name="rotate-cw" data-code="f305"><i class="zmdi zmdi-rotate-cw"></i>&nbsp;&nbsp;<span>rotate-cw</span></div>
<div class="col-sm-3" data-name="rotate-left" data-code="f306"><i class="zmdi zmdi-rotate-left"></i>&nbsp;&nbsp;<span>rotate-left</span></div>
<div class="col-sm-3" data-name="rotate-right" data-code="f307"><i class="zmdi zmdi-rotate-right"></i>&nbsp;&nbsp;<span>rotate-right</span></div>
<div class="col-sm-3" data-name="square-down" data-code="f308"><i class="zmdi zmdi-square-down"></i>&nbsp;&nbsp;<span>square-down</span></div>
<div class="col-sm-3" data-name="square-right" data-code="f309"><i class="zmdi zmdi-square-right"></i>&nbsp;&nbsp;<span>square-right</span></div>
<div class="col-sm-3" data-name="swap-alt" data-code="f30a"><i class="zmdi zmdi-swap-alt"></i>&nbsp;&nbsp;<span>swap-alt</span></div>
<div class="col-sm-3" data-name="swap-vertical-circle" data-code="f30b"><i class="zmdi zmdi-swap-vertical-circle"></i>&nbsp;&nbsp;<span>swap-vertical-circle</span></div>
<div class="col-sm-3" data-name="swap-vertical" data-code="f30c"><i class="zmdi zmdi-swap-vertical"></i>&nbsp;&nbsp;<span>swap-vertical</span></div>
<div class="col-sm-3" data-name="swap" data-code="f30d"><i class="zmdi zmdi-swap"></i>&nbsp;&nbsp;<span>swap</span></div>
<div class="col-sm-3" data-name="trending-down" data-code="f30e"><i class="zmdi zmdi-trending-down"></i>&nbsp;&nbsp;<span>trending-down</span></div>
<div class="col-sm-3" data-name="trending-flat" data-code="f30f"><i class="zmdi zmdi-trending-flat"></i>&nbsp;&nbsp;<span>trending-flat</span></div>
<div class="col-sm-3" data-name="trending-up" data-code="f310"><i class="zmdi zmdi-trending-up"></i>&nbsp;&nbsp;<span>trending-up</span></div>
<div class="col-sm-3" data-name="unfold-less" data-code="f311"><i class="zmdi zmdi-unfold-less"></i>&nbsp;&nbsp;<span>unfold-less</span></div>
<div class="col-sm-3" data-name="unfold-more" data-code="f312"><i class="zmdi zmdi-unfold-more"></i>&nbsp;&nbsp;<span>unfold-more</span></div>
<div class="col-sm-3" data-name="bike" data-code="f117"><i class="zmdi zmdi-directions-bike"></i> zmdi-directions-bike</div>
<div class="col-sm-3" data-name="boat" data-code="f11a"><i class="zmdi zmdi-directions-boat"></i> zmdi-directions-boat</div>
<div class="col-sm-3" data-name="bus" data-code="f121"><i class="zmdi zmdi-directions-bus"></i> zmdi-directions-bus</div>
<div class="col-sm-3" data-name="car" data-code="f125"><i class="zmdi zmdi-directions-car"></i> zmdi-directions-car</div>
<div class="col-sm-3" data-name="railway" data-code="f1b3"><i class="zmdi zmdi-directions-railway"></i> zmdi-directions-railway</div>
<div class="col-sm-3" data-name="run" data-code="f215"><i class="zmdi zmdi-directions-run"></i> zmdi-directions-run</div>
<div class="col-sm-3" data-name="subway" data-code="f1d5"><i class="zmdi zmdi-directions-subway"></i> zmdi-directions-subway</div>
<div class="col-sm-3" data-name="walk" data-code="f216"><i class="zmdi zmdi-directions-walk"></i> zmdi-directions-walk</div>
<div class="col-sm-3" data-name="turning-sign" data-code="f1e7"><i class="zmdi zmdi-directions"></i> zmdi-directions</div>
<div class="col-sm-3" data-name="layers-off" data-code="f18b"><i class="zmdi zmdi-layers-off"></i> zmdi-layers-off</div>
<div class="col-sm-3" data-name="layers" data-code="f18c"><i class="zmdi zmdi-layers"></i> zmdi-layers</div>
<div class="col-sm-3" data-name="ticket-star" data-code="f1df"><i class="zmdi zmdi-local-activity"></i> zmdi-local-activity</div>
<div class="col-sm-3" data-name="airplane" data-code="f103"><i class="zmdi zmdi-local-airport"></i> zmdi-local-airport</div>
<div class="col-sm-3" data-name="money-box" data-code="f198"><i class="zmdi zmdi-local-atm"></i> zmdi-local-atm</div>
<div class="col-sm-3" data-name="cocktail" data-code="f137"><i class="zmdi zmdi-local-bar"></i> zmdi-local-bar</div>
<div class="col-sm-3" data-name="coffee" data-code="f13b"><i class="zmdi zmdi-local-cafe"></i> zmdi-local-cafe</div>
<div class="col-sm-3" data-name="car-wash" data-code="f124"><i class="zmdi zmdi-local-car-wash"></i> zmdi-local-car-wash</div>
<div class="col-sm-3" data-name="store-24" data-code="f1d3"><i class="zmdi zmdi-local-convenience-store"></i> zmdi-local-convenience-store</div>
<div class="col-sm-3" data-name="cutlery" data-code="f153"><i class="zmdi zmdi-local-dining"></i> zmdi-local-dining</div>
<div class="col-sm-3" data-name="drink" data-code="f157"><i class="zmdi zmdi-local-drink"></i> zmdi-local-drink</div>
<div class="col-sm-3" data-name="flower-alt" data-code="f168"><i class="zmdi zmdi-local-florist"></i> zmdi-local-florist</div>
<div class="col-sm-3" data-name="gas-station" data-code="f16f"><i class="zmdi zmdi-local-gas-station"></i> zmdi-local-gas-station</div>
<div class="col-sm-3" data-name="shopping-cart" data-code="f1cb"><i class="zmdi zmdi-local-grocery-store"></i> zmdi-local-grocery-store</div>
<div class="col-sm-3" data-name="hospital" data-code="f177"><i class="zmdi zmdi-local-hospital"></i> zmdi-local-hospital</div>
<div class="col-sm-3" data-name="hotel" data-code="f178"><i class="zmdi zmdi-local-hotel"></i> zmdi-local-hotel</div>
<div class="col-sm-3" data-name="washing-machine" data-code="f1e9"><i class="zmdi zmdi-local-laundry-service"></i> zmdi-local-laundry-service</div>
<div class="col-sm-3" data-name="library" data-code="f18d"><i class="zmdi zmdi-local-library"></i> zmdi-local-library</div>
<div class="col-sm-3" data-name="mall" data-code="f195"><i class="zmdi zmdi-local-mall"></i> zmdi-local-mall</div>
<div class="col-sm-3" data-name="movie-alt" data-code="f19d"><i class="zmdi zmdi-local-movies"></i> zmdi-local-movies</div>
<div class="col-sm-3" data-name="label" data-code="f187"><i class="zmdi zmdi-local-offer"></i> zmdi-local-offer</div>
<div class="col-sm-3" data-name="parking" data-code="f1a5"><i class="zmdi zmdi-local-parking"></i> zmdi-local-parking</div>
<div class="col-sm-3" data-name="hospital-alt" data-code="f176"><i class="zmdi zmdi-local-pharmacy"></i> zmdi-local-pharmacy</div>
<div class="col-sm-3" data-name="phone" data-code="f2be"><i class="zmdi zmdi-local-phone"></i> zmdi-local-phone</div>
<div class="col-sm-3" data-name="pizza" data-code="f1ac"><i class="zmdi zmdi-local-pizza"></i> zmdi-local-pizza</div>
<div class="col-sm-3" data-name="ticket-star" data-code="f1df"><i class="zmdi zmdi-local-activity"></i> zmdi-local-activity</div>
<div class="col-sm-3" data-name="email" data-code="f15a"><i class="zmdi zmdi-local-post-office"></i> zmdi-local-post-office</div>
<div class="col-sm-3" data-name="print" data-code="f1b0"><i class="zmdi zmdi-local-printshop"></i> zmdi-local-printshop</div>
<div class="col-sm-3" data-name="camera" data-code="f28c"><i class="zmdi zmdi-local-see"></i> zmdi-local-see</div>
<div class="col-sm-3" data-name="truck" data-code="f1e6"><i class="zmdi zmdi-local-shipping"></i> zmdi-local-shipping</div>
<div class="col-sm-3" data-name="store" data-code="f1d4"><i class="zmdi zmdi-local-store"></i> zmdi-local-store</div>
<div class="col-sm-3" data-name="car-taxi" data-code="f123"><i class="zmdi zmdi-local-taxi"></i> zmdi-local-taxi</div>
<div class="col-sm-3" data-name="male-female" data-code="f211"><i class="zmdi zmdi-local-wc"></i> zmdi-local-wc</div>
<div class="col-sm-3" data-name="map" data-code="f196"><i class="zmdi zmdi-map"></i> zmdi-map</div>
<div class="col-sm-3" data-name="gps-dot" data-code="f299"><i class="zmdi zmdi-my-location"></i> zmdi-my-location</div>
<div class="col-sm-3" data-name="nature-people" data-code="f19f"><i class="zmdi zmdi-nature-people"></i> zmdi-nature-people</div>
<div class="col-sm-3" data-name="nature" data-code="f1a0"><i class="zmdi zmdi-nature"></i> zmdi-nature</div>
<div class="col-sm-3" data-name="navigation" data-code="f1a1"><i class="zmdi zmdi-navigation"></i> zmdi-navigation</div>
<div class="col-sm-3" data-name="pin-account" data-code="f1a6"><i class="zmdi zmdi-pin-account"></i> zmdi-pin-account</div>
<div class="col-sm-3" data-name="pin-assistant" data-code="f1a7"><i class="zmdi zmdi-pin-assistant"></i> zmdi-pin-assistant</div>
<div class="col-sm-3" data-name="pin-drop" data-code="f1a8"><i class="zmdi zmdi-pin-drop"></i> zmdi-pin-drop</div>
<div class="col-sm-3" data-name="pin-help" data-code="f1a9"><i class="zmdi zmdi-pin-help"></i> zmdi-pin-help</div>
<div class="col-sm-3" data-name="pin-off" data-code="f1aa"><i class="zmdi zmdi-pin-off"></i> zmdi-pin-off</div>
<div class="col-sm-3" data-name="pin" data-code="f1ab"><i class="zmdi zmdi-pin"></i> zmdi-pin</div>
<div class="col-sm-3" data-name="traffic" data-code="f1e2"><i class="zmdi zmdi-traffic"></i> zmdi-traffic</div>
<div class="col-sm-3" data-name="apps" data-code="f313"><i class="zmdi zmdi-apps"></i>&nbsp;&nbsp;<span>apps</span></div>
<div class="col-sm-3" data-name="grid-off" data-code="f314"><i class="zmdi zmdi-grid-off"></i>&nbsp;&nbsp;<span>grid-off</span></div>
<div class="col-sm-3" data-name="grid" data-code="f315"><i class="zmdi zmdi-grid"></i>&nbsp;&nbsp;<span>grid</span></div>
<div class="col-sm-3" data-name="view-agenda" data-code="f316"><i class="zmdi zmdi-view-agenda"></i>&nbsp;&nbsp;<span>view-agenda</span></div>
<div class="col-sm-3" data-name="view-array" data-code="f317"><i class="zmdi zmdi-view-array"></i>&nbsp;&nbsp;<span>view-array</span></div>
<div class="col-sm-3" data-name="view-carousel" data-code="f318"><i class="zmdi zmdi-view-carousel"></i>&nbsp;&nbsp;<span>view-carousel</span></div>
<div class="col-sm-3" data-name="view-column" data-code="f319"><i class="zmdi zmdi-view-column"></i>&nbsp;&nbsp;<span>view-column</span></div>
<div class="col-sm-3" data-name="view-comfy" data-code="f31a"><i class="zmdi zmdi-view-comfy"></i>&nbsp;&nbsp;<span>view-comfy</span></div>
<div class="col-sm-3" data-name="view-compact" data-code="f31b"><i class="zmdi zmdi-view-compact"></i>&nbsp;&nbsp;<span>view-compact</span></div>
<div class="col-sm-3" data-name="view-dashboard" data-code="f31c"><i class="zmdi zmdi-view-dashboard"></i>&nbsp;&nbsp;<span>view-dashboard</span></div>
<div class="col-sm-3" data-name="view-day" data-code="f31d"><i class="zmdi zmdi-view-day"></i>&nbsp;&nbsp;<span>view-day</span></div>
<div class="col-sm-3" data-name="view-headline" data-code="f31e"><i class="zmdi zmdi-view-headline"></i>&nbsp;&nbsp;<span>view-headline</span></div>
<div class="col-sm-3" data-name="view-list-alt" data-code="f31f"><i class="zmdi zmdi-view-list-alt"></i>&nbsp;&nbsp;<span>view-list-alt</span></div>
<div class="col-sm-3" data-name="view-list" data-code="f320"><i class="zmdi zmdi-view-list"></i>&nbsp;&nbsp;<span>view-list</span></div>
<div class="col-sm-3" data-name="view-module" data-code="f321"><i class="zmdi zmdi-view-module"></i>&nbsp;&nbsp;<span>view-module</span></div>
<div class="col-sm-3" data-name="view-quilt" data-code="f322"><i class="zmdi zmdi-view-quilt"></i>&nbsp;&nbsp;<span>view-quilt</span></div>
<div class="col-sm-3" data-name="view-stream" data-code="f323"><i class="zmdi zmdi-view-stream"></i>&nbsp;&nbsp;<span>view-stream</span></div>
<div class="col-sm-3" data-name="view-subtitles" data-code="f324"><i class="zmdi zmdi-view-subtitles"></i>&nbsp;&nbsp;<span>view-subtitles</span></div>
<div class="col-sm-3" data-name="view-toc" data-code="f325"><i class="zmdi zmdi-view-toc"></i>&nbsp;&nbsp;<span>view-toc</span></div>
<div class="col-sm-3" data-name="view-web" data-code="f326"><i class="zmdi zmdi-view-web"></i>&nbsp;&nbsp;<span>view-web</span></div>
<div class="col-sm-3" data-name="view-week" data-code="f327"><i class="zmdi zmdi-view-week"></i>&nbsp;&nbsp;<span>view-week</span></div>
<div class="col-sm-3" data-name="widgets" data-code="f328"><i class="zmdi zmdi-widgets"></i>&nbsp;&nbsp;<span>widgets</span></div>
<div class="col-sm-3" data-name="alarm-check" data-code="f329"><i class="zmdi zmdi-alarm-check"></i>&nbsp;&nbsp;<span>alarm-check</span></div>
<div class="col-sm-3" data-name="alarm-off" data-code="f32a"><i class="zmdi zmdi-alarm-off"></i>&nbsp;&nbsp;<span>alarm-off</span></div>
<div class="col-sm-3" data-name="alarm-plus" data-code="f32b"><i class="zmdi zmdi-alarm-plus"></i>&nbsp;&nbsp;<span>alarm-plus</span></div>
<div class="col-sm-3" data-name="alarm-snooze" data-code="f32c"><i class="zmdi zmdi-alarm-snooze"></i>&nbsp;&nbsp;<span>alarm-snooze</span></div>
<div class="col-sm-3" data-name="alarm" data-code="f32d"><i class="zmdi zmdi-alarm"></i>&nbsp;&nbsp;<span>alarm</span></div>
<div class="col-sm-3" data-name="calendar-alt" data-code="f32e"><i class="zmdi zmdi-calendar-alt"></i>&nbsp;&nbsp;<span>calendar-alt</span></div>
<div class="col-sm-3" data-name="calendar-check" data-code="f32f"><i class="zmdi zmdi-calendar-check"></i>&nbsp;&nbsp;<span>calendar-check</span></div>
<div class="col-sm-3" data-name="calendar-close" data-code="f330"><i class="zmdi zmdi-calendar-close"></i>&nbsp;&nbsp;<span>calendar-close</span></div>
<div class="col-sm-3" data-name="calendar-note" data-code="f331"><i class="zmdi zmdi-calendar-note"></i>&nbsp;&nbsp;<span>calendar-note</span></div>
<div class="col-sm-3" data-name="calendar" data-code="f332"><i class="zmdi zmdi-calendar"></i>&nbsp;&nbsp;<span>calendar</span></div>
<div class="col-sm-3" data-name="time-countdown" data-code="f333"><i class="zmdi zmdi-time-countdown"></i>&nbsp;&nbsp;<span>time-countdown</span></div>
<div class="col-sm-3" data-name="time-interval" data-code="f334"><i class="zmdi zmdi-time-interval"></i>&nbsp;&nbsp;<span>time-interval</span></div>
<div class="col-sm-3" data-name="time-restore-setting" data-code="f335"><i class="zmdi zmdi-time-restore-setting"></i>&nbsp;&nbsp;<span>time-restore-setting</span></div>
<div class="col-sm-3" data-name="time-restore" data-code="f336"><i class="zmdi zmdi-time-restore"></i>&nbsp;&nbsp;<span>time-restore</span></div>
<div class="col-sm-3" data-name="time" data-code="f337"><i class="zmdi zmdi-time"></i>&nbsp;&nbsp;<span>time</span></div>
<div class="col-sm-3" data-name="timer-off" data-code="f338"><i class="zmdi zmdi-timer-off"></i>&nbsp;&nbsp;<span>timer-off</span></div>
<div class="col-sm-3" data-name="timer" data-code="f339"><i class="zmdi zmdi-timer"></i>&nbsp;&nbsp;<span>timer</span></div>
<div class="col-sm-3" data-name="android-alt" data-code="f33a"><i class="zmdi zmdi-android-alt"></i>&nbsp;&nbsp;<span>android-alt</span></div>
<div class="col-sm-3" data-name="android" data-code="f33b"><i class="zmdi zmdi-android"></i>&nbsp;&nbsp;<span>android</span></div>
<div class="col-sm-3" data-name="apple" data-code="f33c"><i class="zmdi zmdi-apple"></i>&nbsp;&nbsp;<span>apple</span></div>
<div class="col-sm-3" data-name="behance" data-code="f33d"><i class="zmdi zmdi-behance"></i>&nbsp;&nbsp;<span>behance</span></div>
<div class="col-sm-3" data-name="codepen" data-code="f33e"><i class="zmdi zmdi-codepen"></i>&nbsp;&nbsp;<span>codepen</span></div>
<div class="col-sm-3" data-name="dribbble" data-code="f33f"><i class="zmdi zmdi-dribbble"></i>&nbsp;&nbsp;<span>dribbble</span></div>
<div class="col-sm-3" data-name="dropbox" data-code="f340"><i class="zmdi zmdi-dropbox"></i>&nbsp;&nbsp;<span>dropbox</span></div>
<div class="col-sm-3" data-name="evernote" data-code="f341"><i class="zmdi zmdi-evernote"></i>&nbsp;&nbsp;<span>evernote</span></div>
<div class="col-sm-3" data-name="facebook-box" data-code="f342"><i class="zmdi zmdi-facebook-box"></i>&nbsp;&nbsp;<span>facebook-box</span></div>
<div class="col-sm-3" data-name="facebook" data-code="f343"><i class="zmdi zmdi-facebook"></i>&nbsp;&nbsp;<span>facebook</span></div>
<div class="col-sm-3" data-name="github-box" data-code="f344"><i class="zmdi zmdi-github-box"></i>&nbsp;&nbsp;<span>github-box</span></div>
<div class="col-sm-3" data-name="github" data-code="f345"><i class="zmdi zmdi-github"></i>&nbsp;&nbsp;<span>github</span></div>
<div class="col-sm-3" data-name="google-drive" data-code="f346"><i class="zmdi zmdi-google-drive"></i>&nbsp;&nbsp;<span>google-drive</span></div>
<div class="col-sm-3" data-name="google-earth" data-code="f347"><i class="zmdi zmdi-google-earth"></i>&nbsp;&nbsp;<span>google-earth</span></div>
<div class="col-sm-3" data-name="google-glass" data-code="f348"><i class="zmdi zmdi-google-glass"></i>&nbsp;&nbsp;<span>google-glass</span></div>
<div class="col-sm-3" data-name="google-maps" data-code="f349"><i class="zmdi zmdi-google-maps"></i>&nbsp;&nbsp;<span>google-maps</span></div>
<div class="col-sm-3" data-name="google-pages" data-code="f34a"><i class="zmdi zmdi-google-pages"></i>&nbsp;&nbsp;<span>google-pages</span></div>
<div class="col-sm-3" data-name="google-play" data-code="f34b"><i class="zmdi zmdi-google-play"></i>&nbsp;&nbsp;<span>google-play</span></div>
<div class="col-sm-3" data-name="google-plus-box" data-code="f34c"><i class="zmdi zmdi-google-plus-box"></i>&nbsp;&nbsp;<span>google-plus-box</span></div>
<div class="col-sm-3" data-name="google-plus" data-code="f34d"><i class="zmdi zmdi-google-plus"></i>&nbsp;&nbsp;<span>google-plus</span></div>
<div class="col-sm-3" data-name="google" data-code="f34e"><i class="zmdi zmdi-google"></i>&nbsp;&nbsp;<span>google</span></div>
<div class="col-sm-3" data-name="instagram" data-code="f34f"><i class="zmdi zmdi-instagram"></i>&nbsp;&nbsp;<span>instagram</span></div>
<div class="col-sm-3" data-name="language-css3" data-code="f350"><i class="zmdi zmdi-language-css3"></i>&nbsp;&nbsp;<span>language-css3</span></div>
<div class="col-sm-3" data-name="language-html5" data-code="f351"><i class="zmdi zmdi-language-html5"></i>&nbsp;&nbsp;<span>language-html5</span></div>
<div class="col-sm-3" data-name="language-javascript" data-code="f352"><i class="zmdi zmdi-language-javascript"></i>&nbsp;&nbsp;<span>language-javascript</span></div>
<div class="col-sm-3" data-name="language-python-alt" data-code="f353"><i class="zmdi zmdi-language-python-alt"></i>&nbsp;&nbsp;<span>language-python-alt</span></div>
<div class="col-sm-3" data-name="language-python" data-code="f354"><i class="zmdi zmdi-language-python"></i>&nbsp;&nbsp;<span>language-python</span></div>
<div class="col-sm-3" data-name="lastfm" data-code="f355"><i class="zmdi zmdi-lastfm"></i>&nbsp;&nbsp;<span>lastfm</span></div>
<div class="col-sm-3" data-name="linkedin-box" data-code="f356"><i class="zmdi zmdi-linkedin-box"></i>&nbsp;&nbsp;<span>linkedin-box</span></div>
<div class="col-sm-3" data-name="paypal" data-code="f357"><i class="zmdi zmdi-paypal"></i>&nbsp;&nbsp;<span>paypal</span></div>
<div class="col-sm-3" data-name="pinterest-box" data-code="f358"><i class="zmdi zmdi-pinterest-box"></i>&nbsp;&nbsp;<span>pinterest-box</span></div>
<div class="col-sm-3" data-name="pocket" data-code="f359"><i class="zmdi zmdi-pocket"></i>&nbsp;&nbsp;<span>pocket</span></div>
<div class="col-sm-3" data-name="polymer" data-code="f35a"><i class="zmdi zmdi-polymer"></i>&nbsp;&nbsp;<span>polymer</span></div>
<div class="col-sm-3" data-name="rss" data-code="f3ea"><i class="zmdi zmdi-rss"></i>&nbsp;&nbsp;<span>rss</span></div>
<div class="col-sm-3" data-name="share" data-code="f35b"><i class="zmdi zmdi-share"></i>&nbsp;&nbsp;<span>share</span></div>
<div class="col-sm-3" data-name="stackoverflow" data-code="f35c"><i class="zmdi zmdi-stackoverflow"></i>&nbsp;&nbsp;<span>stackoverflow</span></div>
<div class="col-sm-3" data-name="steam-square" data-code="f35d"><i class="zmdi zmdi-steam-square"></i>&nbsp;&nbsp;<span>steam-square</span></div>
<div class="col-sm-3" data-name="steam" data-code="f35e"><i class="zmdi zmdi-steam"></i>&nbsp;&nbsp;<span>steam</span></div>
<div class="col-sm-3" data-name="twitter-box" data-code="f35f"><i class="zmdi zmdi-twitter-box"></i>&nbsp;&nbsp;<span>twitter-box</span></div>
<div class="col-sm-3" data-name="twitter" data-code="f360"><i class="zmdi zmdi-twitter"></i>&nbsp;&nbsp;<span>twitter</span></div>
<div class="col-sm-3" data-name="vk" data-code="f361"><i class="zmdi zmdi-vk"></i>&nbsp;&nbsp;<span>vk</span></div>
<div class="col-sm-3" data-name="wikipedia" data-code="f362"><i class="zmdi zmdi-wikipedia"></i>&nbsp;&nbsp;<span>wikipedia</span></div>
<div class="col-sm-3" data-name="windows" data-code="f363"><i class="zmdi zmdi-windows"></i>&nbsp;&nbsp;<span>windows</span></div>
<div class="col-sm-3" data-name="500px" data-code="f3ee"><i class="zmdi zmdi-500px"></i>&nbsp;&nbsp;<span>500px</span></div>
<div class="col-sm-3" data-name="8tracks" data-code="f3ef"><i class="zmdi zmdi-8tracks"></i>&nbsp;&nbsp;<span>8tracks</span></div>
<div class="col-sm-3" data-name="amazon" data-code="f3f0"><i class="zmdi zmdi-amazon"></i>&nbsp;&nbsp;<span>amazon</span></div>
<div class="col-sm-3" data-name="blogger" data-code="f3f1"><i class="zmdi zmdi-blogger"></i>&nbsp;&nbsp;<span>blogger</span></div>
<div class="col-sm-3" data-name="delicious" data-code="f3f2"><i class="zmdi zmdi-delicious"></i>&nbsp;&nbsp;<span>delicious</span></div>
<div class="col-sm-3" data-name="disqus" data-code="f3f3"><i class="zmdi zmdi-disqus"></i>&nbsp;&nbsp;<span>disqus</span></div>
<div class="col-sm-3" data-name="flattr" data-code="f3f4"><i class="zmdi zmdi-flattr"></i>&nbsp;&nbsp;<span>flattr</span></div>
<div class="col-sm-3" data-name="flickr" data-code="f3f5"><i class="zmdi zmdi-flickr"></i>&nbsp;&nbsp;<span>flickr</span></div>
<div class="col-sm-3" data-name="github-alt" data-code="f3f6"><i class="zmdi zmdi-github-alt"></i>&nbsp;&nbsp;<span>github-alt</span></div>
<div class="col-sm-3" data-name="google-old" data-code="f3f7"><i class="zmdi zmdi-google-old"></i>&nbsp;&nbsp;<span>google-old</span></div>
<div class="col-sm-3" data-name="linkedin" data-code="f3f8"><i class="zmdi zmdi-linkedin"></i>&nbsp;&nbsp;<span>linkedin</span></div>
<div class="col-sm-3" data-name="odnoklassniki" data-code="f3f9"><i class="zmdi zmdi-odnoklassniki"></i>&nbsp;&nbsp;<span>odnoklassniki</span></div>
<div class="col-sm-3" data-name="outlook" data-code="f3fa"><i class="zmdi zmdi-outlook"></i>&nbsp;&nbsp;<span>outlook</span></div>
<div class="col-sm-3" data-name="paypal-alt" data-code="f3fb"><i class="zmdi zmdi-paypal-alt"></i>&nbsp;&nbsp;<span>paypal-alt</span></div>
<div class="col-sm-3" data-name="pinterest" data-code="f3fc"><i class="zmdi zmdi-pinterest"></i>&nbsp;&nbsp;<span>pinterest</span></div>
<div class="col-sm-3" data-name="playstation" data-code="f3fd"><i class="zmdi zmdi-playstation"></i>&nbsp;&nbsp;<span>playstation</span></div>
<div class="col-sm-3" data-name="reddit" data-code="f3fe"><i class="zmdi zmdi-reddit"></i>&nbsp;&nbsp;<span>reddit</span></div>
<div class="col-sm-3" data-name="skype" data-code="f3ff"><i class="zmdi zmdi-skype"></i>&nbsp;&nbsp;<span>skype</span></div>
<div class="col-sm-3" data-name="slideshare" data-code="f400"><i class="zmdi zmdi-slideshare"></i>&nbsp;&nbsp;<span>slideshare</span></div>
<div class="col-sm-3" data-name="soundcloud" data-code="f401"><i class="zmdi zmdi-soundcloud"></i>&nbsp;&nbsp;<span>soundcloud</span></div>
<div class="col-sm-3" data-name="tumblr" data-code="f402"><i class="zmdi zmdi-tumblr"></i>&nbsp;&nbsp;<span>tumblr</span></div>
<div class="col-sm-3" data-name="twitch" data-code="f403"><i class="zmdi zmdi-twitch"></i>&nbsp;&nbsp;<span>twitch</span></div>
<div class="col-sm-3" data-name="vimeo" data-code="f404"><i class="zmdi zmdi-vimeo"></i>&nbsp;&nbsp;<span>vimeo</span></div>
<div class="col-sm-3" data-name="whatsapp" data-code="f405"><i class="zmdi zmdi-whatsapp"></i>&nbsp;&nbsp;<span>whatsapp</span></div>
<div class="col-sm-3" data-name="xbox" data-code="f406"><i class="zmdi zmdi-xbox"></i>&nbsp;&nbsp;<span>xbox</span></div>
<div class="col-sm-3" data-name="yahoo" data-code="f407"><i class="zmdi zmdi-yahoo"></i>&nbsp;&nbsp;<span>yahoo</span></div>
<div class="col-sm-3" data-name="youtube-play" data-code="f408"><i class="zmdi zmdi-youtube-play"></i>&nbsp;&nbsp;<span>youtube-play</span></div>
<div class="col-sm-3" data-name="youtube" data-code="f409"><i class="zmdi zmdi-youtube"></i>&nbsp;&nbsp;<span>youtube</span></div>
<div class="col-sm-3" data-name="aspect-ratio-alt" data-code="f364"><i class="zmdi zmdi-aspect-ratio-alt"></i>&nbsp;&nbsp;<span>aspect-ratio-alt</span></div>
<div class="col-sm-3" data-name="aspect-ratio" data-code="f365"><i class="zmdi zmdi-aspect-ratio"></i>&nbsp;&nbsp;<span>aspect-ratio</span></div>
<div class="col-sm-3" data-name="blur-circular" data-code="f366"><i class="zmdi zmdi-blur-circular"></i>&nbsp;&nbsp;<span>blur-circular</span></div>
<div class="col-sm-3" data-name="blur-linear" data-code="f367"><i class="zmdi zmdi-blur-linear"></i>&nbsp;&nbsp;<span>blur-linear</span></div>
<div class="col-sm-3" data-name="blur-off" data-code="f368"><i class="zmdi zmdi-blur-off"></i>&nbsp;&nbsp;<span>blur-off</span></div>
<div class="col-sm-3" data-name="blur" data-code="f369"><i class="zmdi zmdi-blur"></i>&nbsp;&nbsp;<span>blur</span></div>
<div class="col-sm-3" data-name="brightness-2" data-code="f36a"><i class="zmdi zmdi-brightness-2"></i>&nbsp;&nbsp;<span>brightness-2</span></div>
<div class="col-sm-3" data-name="brightness-3" data-code="f36b"><i class="zmdi zmdi-brightness-3"></i>&nbsp;&nbsp;<span>brightness-3</span></div>
<div class="col-sm-3" data-name="brightness-4" data-code="f36c"><i class="zmdi zmdi-brightness-4"></i>&nbsp;&nbsp;<span>brightness-4</span></div>
<div class="col-sm-3" data-name="brightness-5" data-code="f36d"><i class="zmdi zmdi-brightness-5"></i>&nbsp;&nbsp;<span>brightness-5</span></div>
<div class="col-sm-3" data-name="brightness-6" data-code="f36e"><i class="zmdi zmdi-brightness-6"></i>&nbsp;&nbsp;<span>brightness-6</span></div>
<div class="col-sm-3" data-name="brightness-7" data-code="f36f"><i class="zmdi zmdi-brightness-7"></i>&nbsp;&nbsp;<span>brightness-7</span></div>
<div class="col-sm-3" data-name="brightness-auto" data-code="f370"><i class="zmdi zmdi-brightness-auto"></i>&nbsp;&nbsp;<span>brightness-auto</span></div>
<div class="col-sm-3" data-name="brightness-setting" data-code="f371"><i class="zmdi zmdi-brightness-setting"></i>&nbsp;&nbsp;<span>brightness-setting</span></div>
<div class="col-sm-3" data-name="broken-image" data-code="f372"><i class="zmdi zmdi-broken-image"></i>&nbsp;&nbsp;<span>broken-image</span></div>
<div class="col-sm-3" data-name="center-focus-strong" data-code="f373"><i class="zmdi zmdi-center-focus-strong"></i>&nbsp;&nbsp;<span>center-focus-strong</span></div>
<div class="col-sm-3" data-name="center-focus-weak" data-code="f374"><i class="zmdi zmdi-center-focus-weak"></i>&nbsp;&nbsp;<span>center-focus-weak</span></div>
<div class="col-sm-3" data-name="compare" data-code="f375"><i class="zmdi zmdi-compare"></i>&nbsp;&nbsp;<span>compare</span></div>
<div class="col-sm-3" data-name="crop-16-9" data-code="f376"><i class="zmdi zmdi-crop-16-9"></i>&nbsp;&nbsp;<span>crop-16-9</span></div>
<div class="col-sm-3" data-name="crop-3-2" data-code="f377"><i class="zmdi zmdi-crop-3-2"></i>&nbsp;&nbsp;<span>crop-3-2</span></div>
<div class="col-sm-3" data-name="crop-5-4" data-code="f378"><i class="zmdi zmdi-crop-5-4"></i>&nbsp;&nbsp;<span>crop-5-4</span></div>
<div class="col-sm-3" data-name="crop-7-5" data-code="f379"><i class="zmdi zmdi-crop-7-5"></i>&nbsp;&nbsp;<span>crop-7-5</span></div>
<div class="col-sm-3" data-name="crop-din" data-code="f37a"><i class="zmdi zmdi-crop-din"></i>&nbsp;&nbsp;<span>crop-din</span></div>
<div class="col-sm-3" data-name="crop-free" data-code="f37b"><i class="zmdi zmdi-crop-free"></i>&nbsp;&nbsp;<span>crop-free</span></div>
<div class="col-sm-3" data-name="crop-landscape" data-code="f37c"><i class="zmdi zmdi-crop-landscape"></i>&nbsp;&nbsp;<span>crop-landscape</span></div>
<div class="col-sm-3" data-name="crop-portrait" data-code="f37d"><i class="zmdi zmdi-crop-portrait"></i>&nbsp;&nbsp;<span>crop-portrait</span></div>
<div class="col-sm-3" data-name="crop-square" data-code="f37e"><i class="zmdi zmdi-crop-square"></i>&nbsp;&nbsp;<span>crop-square</span></div>
<div class="col-sm-3" data-name="exposure-alt" data-code="f37f"><i class="zmdi zmdi-exposure-alt"></i>&nbsp;&nbsp;<span>exposure-alt</span></div>
<div class="col-sm-3" data-name="exposure" data-code="f380"><i class="zmdi zmdi-exposure"></i>&nbsp;&nbsp;<span>exposure</span></div>
<div class="col-sm-3" data-name="filter-b-and-w" data-code="f381"><i class="zmdi zmdi-filter-b-and-w"></i>&nbsp;&nbsp;<span>filter-b-and-w</span></div>
<div class="col-sm-3" data-name="filter-center-focus" data-code="f382"><i class="zmdi zmdi-filter-center-focus"></i>&nbsp;&nbsp;<span>filter-center-focus</span></div>
<div class="col-sm-3" data-name="filter-frames" data-code="f383"><i class="zmdi zmdi-filter-frames"></i>&nbsp;&nbsp;<span>filter-frames</span></div>
<div class="col-sm-3" data-name="filter-tilt-shift" data-code="f384"><i class="zmdi zmdi-filter-tilt-shift"></i>&nbsp;&nbsp;<span>filter-tilt-shift</span></div>
<div class="col-sm-3" data-name="gradient" data-code="f385"><i class="zmdi zmdi-gradient"></i>&nbsp;&nbsp;<span>gradient</span></div>
<div class="col-sm-3" data-name="grain" data-code="f386"><i class="zmdi zmdi-grain"></i>&nbsp;&nbsp;<span>grain</span></div>
<div class="col-sm-3" data-name="graphic-eq" data-code="f387"><i class="zmdi zmdi-graphic-eq"></i>&nbsp;&nbsp;<span>graphic-eq</span></div>
<div class="col-sm-3" data-name="hdr-off" data-code="f388"><i class="zmdi zmdi-hdr-off"></i>&nbsp;&nbsp;<span>hdr-off</span></div>
<div class="col-sm-3" data-name="hdr-strong" data-code="f389"><i class="zmdi zmdi-hdr-strong"></i>&nbsp;&nbsp;<span>hdr-strong</span></div>
<div class="col-sm-3" data-name="hdr-weak" data-code="f38a"><i class="zmdi zmdi-hdr-weak"></i>&nbsp;&nbsp;<span>hdr-weak</span></div>
<div class="col-sm-3" data-name="hdr" data-code="f38b"><i class="zmdi zmdi-hdr"></i>&nbsp;&nbsp;<span>hdr</span></div>
<div class="col-sm-3" data-name="iridescent" data-code="f38c"><i class="zmdi zmdi-iridescent"></i>&nbsp;&nbsp;<span>iridescent</span></div>
<div class="col-sm-3" data-name="leak-off" data-code="f38d"><i class="zmdi zmdi-leak-off"></i>&nbsp;&nbsp;<span>leak-off</span></div>
<div class="col-sm-3" data-name="leak" data-code="f38e"><i class="zmdi zmdi-leak"></i>&nbsp;&nbsp;<span>leak</span></div>
<div class="col-sm-3" data-name="looks" data-code="f38f"><i class="zmdi zmdi-looks"></i>&nbsp;&nbsp;<span>looks</span></div>
<div class="col-sm-3" data-name="loupe" data-code="f390"><i class="zmdi zmdi-loupe"></i>&nbsp;&nbsp;<span>loupe</span></div>
<div class="col-sm-3" data-name="panorama-horizontal" data-code="f391"><i class="zmdi zmdi-panorama-horizontal"></i>&nbsp;&nbsp;<span>panorama-horizontal</span></div>
<div class="col-sm-3" data-name="panorama-vertical" data-code="f392"><i class="zmdi zmdi-panorama-vertical"></i>&nbsp;&nbsp;<span>panorama-vertical</span></div>
<div class="col-sm-3" data-name="panorama-wide-angle" data-code="f393"><i class="zmdi zmdi-panorama-wide-angle"></i>&nbsp;&nbsp;<span>panorama-wide-angle</span></div>
<div class="col-sm-3" data-name="photo-size-select-large" data-code="f394"><i class="zmdi zmdi-photo-size-select-large"></i>&nbsp;&nbsp;<span>photo-size-select-large</span></div>
<div class="col-sm-3" data-name="photo-size-select-small" data-code="f395"><i class="zmdi zmdi-photo-size-select-small"></i>&nbsp;&nbsp;<span>photo-size-select-small</span></div>
<div class="col-sm-3" data-name="picture-in-picture" data-code="f396"><i class="zmdi zmdi-picture-in-picture"></i>&nbsp;&nbsp;<span>picture-in-picture</span></div>
<div class="col-sm-3" data-name="slideshow" data-code="f397"><i class="zmdi zmdi-slideshow"></i>&nbsp;&nbsp;<span>slideshow</span></div>
<div class="col-sm-3" data-name="texture" data-code="f398"><i class="zmdi zmdi-texture"></i>&nbsp;&nbsp;<span>texture</span></div>
<div class="col-sm-3" data-name="tonality" data-code="f399"><i class="zmdi zmdi-tonality"></i>&nbsp;&nbsp;<span>tonality</span></div>
<div class="col-sm-3" data-name="vignette" data-code="f39a"><i class="zmdi zmdi-vignette"></i>&nbsp;&nbsp;<span>vignette</span></div>
<div class="col-sm-3" data-name="wb-auto" data-code="f39b"><i class="zmdi zmdi-wb-auto"></i>&nbsp;&nbsp;<span>wb-auto</span></div>
<div class="col-sm-3" data-name="eject-alt" data-code="f39c"><i class="zmdi zmdi-eject-alt"></i>&nbsp;&nbsp;<span>eject-alt</span></div>
<div class="col-sm-3" data-name="eject" data-code="f39d"><i class="zmdi zmdi-eject"></i>&nbsp;&nbsp;<span>eject</span></div>
<div class="col-sm-3" data-name="equalizer" data-code="f39e"><i class="zmdi zmdi-equalizer"></i>&nbsp;&nbsp;<span>equalizer</span></div>
<div class="col-sm-3" data-name="fast-forward" data-code="f39f"><i class="zmdi zmdi-fast-forward"></i>&nbsp;&nbsp;<span>fast-forward</span></div>
<div class="col-sm-3" data-name="fast-rewind" data-code="f3a0"><i class="zmdi zmdi-fast-rewind"></i>&nbsp;&nbsp;<span>fast-rewind</span></div>
<div class="col-sm-3" data-name="forward-10" data-code="f3a1"><i class="zmdi zmdi-forward-10"></i>&nbsp;&nbsp;<span>forward-10</span></div>
<div class="col-sm-3" data-name="forward-30" data-code="f3a2"><i class="zmdi zmdi-forward-30"></i>&nbsp;&nbsp;<span>forward-30</span></div>
<div class="col-sm-3" data-name="forward-5" data-code="f3a3"><i class="zmdi zmdi-forward-5"></i>&nbsp;&nbsp;<span>forward-5</span></div>
<div class="col-sm-3" data-name="hearing" data-code="f3a4"><i class="zmdi zmdi-hearing"></i>&nbsp;&nbsp;<span>hearing</span></div>
<div class="col-sm-3" data-name="pause-circle-outline" data-code="f3a5"><i class="zmdi zmdi-pause-circle-outline"></i>&nbsp;&nbsp;<span>pause-circle-outline</span></div>
<div class="col-sm-3" data-name="pause-circle" data-code="f3a6"><i class="zmdi zmdi-pause-circle"></i>&nbsp;&nbsp;<span>pause-circle</span></div>
<div class="col-sm-3" data-name="pause" data-code="f3a7"><i class="zmdi zmdi-pause"></i>&nbsp;&nbsp;<span>pause</span></div>
<div class="col-sm-3" data-name="play-circle-outline" data-code="f3a8"><i class="zmdi zmdi-play-circle-outline"></i>&nbsp;&nbsp;<span>play-circle-outline</span></div>
<div class="col-sm-3" data-name="play-circle" data-code="f3a9"><i class="zmdi zmdi-play-circle"></i>&nbsp;&nbsp;<span>play-circle</span></div>
<div class="col-sm-3" data-name="play" data-code="f3aa"><i class="zmdi zmdi-play"></i>&nbsp;&nbsp;<span>play</span></div>
<div class="col-sm-3" data-name="playlist-audio" data-code="f3ab"><i class="zmdi zmdi-playlist-audio"></i>&nbsp;&nbsp;<span>playlist-audio</span></div>
<div class="col-sm-3" data-name="playlist-plus" data-code="f3ac"><i class="zmdi zmdi-playlist-plus"></i>&nbsp;&nbsp;<span>playlist-plus</span></div>
<div class="col-sm-3" data-name="repeat-one" data-code="f3ad"><i class="zmdi zmdi-repeat-one"></i>&nbsp;&nbsp;<span>repeat-one</span></div>
<div class="col-sm-3" data-name="repeat" data-code="f3ae"><i class="zmdi zmdi-repeat"></i>&nbsp;&nbsp;<span>repeat</span></div>
<div class="col-sm-3" data-name="replay-10" data-code="f3af"><i class="zmdi zmdi-replay-10"></i>&nbsp;&nbsp;<span>replay-10</span></div>
<div class="col-sm-3" data-name="replay-30" data-code="f3b0"><i class="zmdi zmdi-replay-30"></i>&nbsp;&nbsp;<span>replay-30</span></div>
<div class="col-sm-3" data-name="replay-5" data-code="f3b1"><i class="zmdi zmdi-replay-5"></i>&nbsp;&nbsp;<span>replay-5</span></div>
<div class="col-sm-3" data-name="replay" data-code="f3b2"><i class="zmdi zmdi-replay"></i>&nbsp;&nbsp;<span>replay</span></div>
<div class="col-sm-3" data-name="shuffle" data-code="f3b3"><i class="zmdi zmdi-shuffle"></i>&nbsp;&nbsp;<span>shuffle</span></div>
<div class="col-sm-3" data-name="skip-next" data-code="f3b4"><i class="zmdi zmdi-skip-next"></i>&nbsp;&nbsp;<span>skip-next</span></div>
<div class="col-sm-3" data-name="skip-previous" data-code="f3b5"><i class="zmdi zmdi-skip-previous"></i>&nbsp;&nbsp;<span>skip-previous</span></div>
<div class="col-sm-3" data-name="stop" data-code="f3b6"><i class="zmdi zmdi-stop"></i>&nbsp;&nbsp;<span>stop</span></div>
<div class="col-sm-3" data-name="surround-sound" data-code="f3b7"><i class="zmdi zmdi-surround-sound"></i>&nbsp;&nbsp;<span>surround-sound</span></div>
<div class="col-sm-3" data-name="tune" data-code="f3b8"><i class="zmdi zmdi-tune"></i>&nbsp;&nbsp;<span>tune</span></div>
<div class="col-sm-3" data-name="volume-down" data-code="f3b9"><i class="zmdi zmdi-volume-down"></i>&nbsp;&nbsp;<span>volume-down</span></div>
<div class="col-sm-3" data-name="volume-mute" data-code="f3ba"><i class="zmdi zmdi-volume-mute"></i>&nbsp;&nbsp;<span>volume-mute</span></div>
<div class="col-sm-3" data-name="volume-off" data-code="f3bb"><i class="zmdi zmdi-volume-off"></i>&nbsp;&nbsp;<span>volume-off</span></div>
<div class="col-sm-3" data-name="volume-up" data-code="f3bc"><i class="zmdi zmdi-volume-up"></i>&nbsp;&nbsp;<span>volume-up</span></div>
<div class="col-sm-3" data-name="n-1-square" data-code="f3bd"><i class="zmdi zmdi-n-1-square"></i>&nbsp;&nbsp;<span>n-1-square</span></div>
<div class="col-sm-3" data-name="n-2-square" data-code="f3be"><i class="zmdi zmdi-n-2-square"></i>&nbsp;&nbsp;<span>n-2-square</span></div>
<div class="col-sm-3" data-name="n-3-square" data-code="f3bf"><i class="zmdi zmdi-n-3-square"></i>&nbsp;&nbsp;<span>n-3-square</span></div>
<div class="col-sm-3" data-name="n-4-square" data-code="f3c0"><i class="zmdi zmdi-n-4-square"></i>&nbsp;&nbsp;<span>n-4-square</span></div>
<div class="col-sm-3" data-name="n-5-square" data-code="f3c1"><i class="zmdi zmdi-n-5-square"></i>&nbsp;&nbsp;<span>n-5-square</span></div>
<div class="col-sm-3" data-name="n-6-square" data-code="f3c2"><i class="zmdi zmdi-n-6-square"></i>&nbsp;&nbsp;<span>n-6-square</span></div>
<div class="col-sm-3" data-name="neg-1" data-code="f3c3"><i class="zmdi zmdi-neg-1"></i>&nbsp;&nbsp;<span>neg-1</span></div>
<div class="col-sm-3" data-name="neg-2" data-code="f3c4"><i class="zmdi zmdi-neg-2"></i>&nbsp;&nbsp;<span>neg-2</span></div>
<div class="col-sm-3" data-name="plus-1" data-code="f3c5"><i class="zmdi zmdi-plus-1"></i>&nbsp;&nbsp;<span>plus-1</span></div>
<div class="col-sm-3" data-name="plus-2" data-code="f3c6"><i class="zmdi zmdi-plus-2"></i>&nbsp;&nbsp;<span>plus-2</span></div>
<div class="col-sm-3" data-name="sec-10" data-code="f3c7"><i class="zmdi zmdi-sec-10"></i>&nbsp;&nbsp;<span>sec-10</span></div>
<div class="col-sm-3" data-name="sec-3" data-code="f3c8"><i class="zmdi zmdi-sec-3"></i>&nbsp;&nbsp;<span>sec-3</span></div>
<div class="col-sm-3" data-name="zero" data-code="f3c9"><i class="zmdi zmdi-zero"></i>&nbsp;&nbsp;<span>zero</span></div>
<div class="col-sm-3" data-name="airline-seat-flat-angled" data-code="f3ca"><i class="zmdi zmdi-airline-seat-flat-angled"></i>&nbsp;&nbsp;<span>airline-seat-flat-angled</span></div>
<div class="col-sm-3" data-name="airline-seat-flat" data-code="f3cb"><i class="zmdi zmdi-airline-seat-flat"></i>&nbsp;&nbsp;<span>airline-seat-flat</span></div>
<div class="col-sm-3" data-name="airline-seat-individual-suite" data-code="f3cc"><i class="zmdi zmdi-airline-seat-individual-suite"></i>&nbsp;&nbsp;<span>airline-seat-individual-suite</span></div>
<div class="col-sm-3" data-name="airline-seat-legroom-extra" data-code="f3cd"><i class="zmdi zmdi-airline-seat-legroom-extra"></i>&nbsp;&nbsp;<span>airline-seat-legroom-extra</span></div>
<div class="col-sm-3" data-name="airline-seat-legroom-normal" data-code="f3ce"><i class="zmdi zmdi-airline-seat-legroom-normal"></i>&nbsp;&nbsp;<span>airline-seat-legroom-normal</span></div>
<div class="col-sm-3" data-name="airline-seat-legroom-reduced" data-code="f3cf"><i class="zmdi zmdi-airline-seat-legroom-reduced"></i>&nbsp;&nbsp;<span>airline-seat-legroom-reduced</span></div>
<div class="col-sm-3" data-name="airline-seat-recline-extra" data-code="f3d0"><i class="zmdi zmdi-airline-seat-recline-extra"></i>&nbsp;&nbsp;<span>airline-seat-recline-extra</span></div>
<div class="col-sm-3" data-name="airline-seat-recline-normal" data-code="f3d1"><i class="zmdi zmdi-airline-seat-recline-normal"></i>&nbsp;&nbsp;<span>airline-seat-recline-normal</span></div>
<div class="col-sm-3" data-name="airplay" data-code="f3d2"><i class="zmdi zmdi-airplay"></i>&nbsp;&nbsp;<span>airplay</span></div>
<div class="col-sm-3" data-name="closed-caption" data-code="f3d3"><i class="zmdi zmdi-closed-caption"></i>&nbsp;&nbsp;<span>closed-caption</span></div>
<div class="col-sm-3" data-name="confirmation-number" data-code="f3d4"><i class="zmdi zmdi-confirmation-number"></i>&nbsp;&nbsp;<span>confirmation-number</span></div>
<div class="col-sm-3" data-name="developer-board" data-code="f3d5"><i class="zmdi zmdi-developer-board"></i>&nbsp;&nbsp;<span>developer-board</span></div>
<div class="col-sm-3" data-name="disc-full" data-code="f3d6"><i class="zmdi zmdi-disc-full"></i>&nbsp;&nbsp;<span>disc-full</span></div>
<div class="col-sm-3" data-name="explicit" data-code="f3d7"><i class="zmdi zmdi-explicit"></i>&nbsp;&nbsp;<span>explicit</span></div>
<div class="col-sm-3" data-name="flight-land" data-code="f3d8"><i class="zmdi zmdi-flight-land"></i>&nbsp;&nbsp;<span>flight-land</span></div>
<div class="col-sm-3" data-name="flight-takeoff" data-code="f3d9"><i class="zmdi zmdi-flight-takeoff"></i>&nbsp;&nbsp;<span>flight-takeoff</span></div>
<div class="col-sm-3" data-name="flip-to-back" data-code="f3da"><i class="zmdi zmdi-flip-to-back"></i>&nbsp;&nbsp;<span>flip-to-back</span></div>
<div class="col-sm-3" data-name="flip-to-front" data-code="f3db"><i class="zmdi zmdi-flip-to-front"></i>&nbsp;&nbsp;<span>flip-to-front</span></div>
<div class="col-sm-3" data-name="group-work" data-code="f3dc"><i class="zmdi zmdi-group-work"></i>&nbsp;&nbsp;<span>group-work</span></div>
<div class="col-sm-3" data-name="hd" data-code="f3dd"><i class="zmdi zmdi-hd"></i>&nbsp;&nbsp;<span>hd</span></div>
<div class="col-sm-3" data-name="hq" data-code="f3de"><i class="zmdi zmdi-hq"></i>&nbsp;&nbsp;<span>hq</span></div>
<div class="col-sm-3" data-name="markunread-mailbox" data-code="f3df"><i class="zmdi zmdi-markunread-mailbox"></i>&nbsp;&nbsp;<span>markunread-mailbox</span></div>
<div class="col-sm-3" data-name="memory" data-code="f3e0"><i class="zmdi zmdi-memory"></i>&nbsp;&nbsp;<span>memory</span></div>
<div class="col-sm-3" data-name="nfc" data-code="f3e1"><i class="zmdi zmdi-nfc"></i>&nbsp;&nbsp;<span>nfc</span></div>
<div class="col-sm-3" data-name="play-for-work" data-code="f3e2"><i class="zmdi zmdi-play-for-work"></i>&nbsp;&nbsp;<span>play-for-work</span></div>
<div class="col-sm-3" data-name="power-input" data-code="f3e3"><i class="zmdi zmdi-power-input"></i>&nbsp;&nbsp;<span>power-input</span></div>
<div class="col-sm-3" data-name="present-to-all" data-code="f3e4"><i class="zmdi zmdi-present-to-all"></i>&nbsp;&nbsp;<span>present-to-all</span></div>
<div class="col-sm-3" data-name="satellite" data-code="f3e5"><i class="zmdi zmdi-satellite"></i>&nbsp;&nbsp;<span>satellite</span></div>
<div class="col-sm-3" data-name="tap-and-play" data-code="f3e6"><i class="zmdi zmdi-tap-and-play"></i>&nbsp;&nbsp;<span>tap-and-play</span></div>
<div class="col-sm-3" data-name="vibration" data-code="f3e7"><i class="zmdi zmdi-vibration"></i>&nbsp;&nbsp;<span>vibration</span></div>
<div class="col-sm-3" data-name="voicemail" data-code="f3e8"><i class="zmdi zmdi-voicemail"></i>&nbsp;&nbsp;<span>voicemail</span></div>

</div>

</body>