<?php
/**
 * DATE UPDATE: 2017-05-23 08:51 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE BACK-OFFICE-MENU
 *
 * ADMIN MENU
 *
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=$_SESSION['userAdmin']['roleId'];
$minRoleAdim=1;

$moduleSetting=new stdClass();
//$moduleSetting->moduleCommenHeader='<small>comentarios espec&iacute;ficos del m&oacute;dulo actual....</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='system_menu_admin';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='i_option_sort';
$moduleSetting->keyListLabel='id_option_menu';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->isCanEnabled=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole ? true : false;
$moduleSetting->fieldNameStatusRow='b_enabled';
$moduleSetting->allowNewRecord=true;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->minEnableLevel=__LEVEL_ACCESS_MASTER;

// OPTIONAL PARAMS
$moduleSetting->defaultOrderField=['id_related_option'=>'ASC','i_option_sort'=>'ASC'];
$moduleSetting->numRecDisplay='[10,25,50]'; // EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;

/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array(
//		' && activo = '=>0
// EXAMPLE WITH MORE SENTENCES:
//		,'|| ('=>''
//		,'theme = '=>'\'123\''
//		,')'=>''
	)
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode($arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS

$order=0;
//echo $web_url.$adminFolder;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id_option_menu'=>array(
    'formType'=>'hidden'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>0
    ,'label'=>'ID'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'id_related_option'=>array(
    'formType'=>'select'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>3]
    ,'tabularFilter'=>['mode'=>'BBDD','tag'=>array('filterType'=>'select'),'dataFields'=>array('keyOptions'=>'id_option_menu','textOptions'=>'label_menu')] // parametro para premitir filtro del listado especifico sobre el campo actual, array con parametros
    ,'required'=>1
    ,'label'=>'Pertenece a'
    ,'fieldTableName'=>__QUERY_TABLES_PREFIX.'system_menu_admin'  // NOMBRE DE LA TABLA ORIGEN A UTILIZAR PARA LA EXTRACCION DE DATOS PARA OBTENER LOS VALORES DE UN CAMPO "SELECT"
    ,'arrFieldsTagSelect'=>array('id_option_menu','label_menu')	// PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
    ,'defaultSelectText'=>'<span class=c-black>raiz</span>'
		,'orderSelectField'=>'label_menu'
    ,'whereSelectField'=>null
    ,'allowZeroValue'=>true
    ,'visible'=>1
    ,'selectSearchBox'=>false
    ,'multipleSelect'=>false
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'helpText'=>'Selecciona donde ha de colocarse este men&uacute; (en color de fondo oscuro, los men&uacute;s de nivel 2'
    ,'order'=>++$order
  )
  ,'module_name'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>4,'columnWidth'=>2]
    ,'required'=>2
    ,'label'=>'Nombre carpeta'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
		,'extraFunc'=>'openModalModulesFolders'
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'label_menu'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>3,'columnWidth'=>2]
    ,'required'=>1
    ,'label'=>'Etiqueta'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Etiqueta del men&uacute;'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'filelink'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>1
		,'colsClass'=>2
    ,'label'=>'Link'
    ,'helpText'=>'Link que abre el elemento del men&uacute;, habitualmente `index.php`'
    ,'visible'=>1
    ,'placeHolder'=>'Link del elemento'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'i_option_sort'=>array(
    'formType'=>'text'
    ,'dataType'=>'integer'
    ,'required'=>1
		,'colsClass'=>2
    ,'label'=>'Orden de aparici&oacute;n'
    ,'helpText'=>'(utilizar solo n&uacute;meros)'
    ,'tagPattern'=>'[0-9]*$'
    ,'visible'=>1
    ,'placeHolder'=>'Posici&oacute;n en el men&uacute;'
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'icon'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Icono relacionado'
    ,'visible'=>1
    ,'placeHolder'=>'Icono relacionado'
    ,'helpText'=>'Doble click en el campo para abrir la ventana con todos los iconos disponibles'
    ,'formGroup'=>2
    ,'extraFunc'=>'modalIcons,menu_s_icon'	// FUNCION PERSONALIZADA DEL MODULO, CON ELEMENTOS: 1=nombre funcion, DEMAS=parametros separados por comas (ej.: myFuncNombre,hola)
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'i_accesslevel'=>array(
    'formType'=>'text'
    ,'dataType'=>'slider'
    ,'tabularListData'=>['columnOrder'=>5,'columnWidth'=>1]
    ,'sliderConf'=>array('startValue'=>0,'endValue'=>$_SESSION['userAdmin']['roleId'],'stepValue'=>1) // ESTE PARAMETRO SE GESTIONA EN _modules\common_edit_html_top.php
    ,'required'=>0
    ,'label'=>'Nivel de acceso'
    ,'helpText'=>'Niveles para los permisos de acceso'
    ,'helpTextExtend'=>'Definir los niveles m&iacute;nimos para los permisos de los elementos, '.$minRoleAdim.' = m&aacute;s bajo, '.$maxRoleAdim.' = m&aacute;s alto'
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>3
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'b_enabled'=>array(
    'formType'=>'checkbox'
    ,'dataType'=>'toggle'
    ,'tabularListData'=>['columnOrder'=>6,'columnWidth'=>1]
    ,'tabularFilter'=>['mode'=>'STATIC','tag'=>array('filterType'=>'radio'),'dataFields'=>array('keyOptions'=>'b_enabled','textOptions'=>'descripcion_estado','keyDefault'=>'0'),'dataValues'=>array(0=>'Bloqueado',1=>'Habilitado')]
    ,'required'=>0
    ,'toggleParams'=>array('tsColor'=>'green')
    ,'arrValues'=>array(0=>'<span class=c-red>bloqueado</span>',1=>'habilitado')	// PARA ESTE CONTROL, SIEMPRE EL PRIMER VALOR ES EL NEGATIVO Y EL SEGUNDO EL POSITIVO
    ,'label'=>'Estado'
    ,'helpText'=>'Habilitar / deshabilitar este registro'
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>3
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  1=>array(
    'groupTextTitle'=>'Gesti&oacute;n de las entradas de men&uacute; para el back-office'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
  ,2=>array(
    'groupTextTitle'=>'Otras opciones'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi zmdi-view-compact'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
  ,3=>array(
    'groupTextTitle'=>'Opciones de sistema'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi zmdi-view-compact'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);

