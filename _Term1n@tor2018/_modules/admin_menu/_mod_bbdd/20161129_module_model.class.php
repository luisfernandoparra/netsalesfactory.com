<?php
/**
 *	CLASE CON LAS FUNCIONES DE BBDD NECESARIAS PARA CREAR LAS LISTAS DE LA BBSDD STANDARD DE GTP6
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class ModelLocalModule extends CBBDD
{
	protected $database;
	private $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $defaultOrderField;
	public $keyListLabel;
	public $mainTable;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;

	public function __construct($commonDb)
	{
		$this->commonDb=$commonDb;
		$this->arrFieldsTagSelect='';
		$this->query='';
    $this->prefixGtp=__QUERY_TABLES_PREFIX;
		return;
	}

	public function testXXX($mainTable='', $skipBdDefault=false)
	{
/*
		$sqlSelectFields=$this->listFieldsOutput ? $this->listFieldsOutput : '*';
		$sqlSelectFields=$sqlSelectFields ? ($this->keyListLabel ? $this->keyListLabel.' AS keyListModule, '.$sqlSelectFields : $sqlSelectFields ) : '';
		$sqlOrderBy=$this->defaultOrderField ? ' ORDER BY '.$this->defaultOrderField : '';
		$sqlWhere=$this->defaultWhereSql ? ' '.$this->defaultWhereSql.' ' : '';

		$this->query='SET @rownr=0;';
		$this->ejecuta_query($this->query);
		$this->query='SELECT '.$sqlSelectFields.', @rownr:=@rownr+1 AS autoRowNumber FROM %s%s WHERE 1 '.$sqlWhere.$sqlOrderBy;
		// ORDER BY LIMIT 1
		$this->query=sprintf($this->query, ($skipBdDefault ? '' : '`'.$this->db.'`.'), ($skipBdDefault ? '' : '`'.$this->prefixGtp).$mainTable.($skipBdDefault ? '' : '`'));
		$this->getResultSelectArray($this->query, $this->keyListLabel);
		$resSql=$this->tResultadoQuery;
*/
		$queryFiledNames='DESCRIBE %s';
		$queryFiledNames=sprintf($queryFiledNames, $this->db.'.'.$this->prefixGtp.$mainTable);
		$this->getResultSelectArray($queryFiledNames);
		$this->mainTableFieldsNames=$this->tResultadoQuery;
		return;
//		return $resSql;
	}

	public function updateStatusRowXXX($mainTable='', $id=0, $skipBdDefault=false, $statusFieldName='b_enabled', $newStatusValue=0)
	{
    // OBTENER EL NOMBRE DEL CAMPO id PRIMARIO DE LA TABLA A GESTIONAR
    $query='SELECT `COLUMN_NAME` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = "'.$this->db.'" && `TABLE_NAME` = "'.$this->prefixGtp.$mainTable.'"
&& `COLUMN_KEY` = "PRI";';
    $this->getResultSelectArray($query);
    $primaryColumnName=$this->tResultadoQuery;
    $primaryColumnName=$primaryColumnName[0]['COLUMN_NAME'];
//echo $newStatusValue;die();
    if(!trim($statusFieldName))
      $statusFieldName='b_enabled';

		$this->query='UPDATE %s%s SET %s = %d WHERE `%s` = %d ';
		$this->query=sprintf($this->query, ($skipBdDefault ? '' : '`'.$this->db.'`.'), ($skipBdDefault ? '' : '`'.$this->prefixGtp).$mainTable.($skipBdDefault ? '' : '`'), $statusFieldName, (int)$newStatusValue, $primaryColumnName, (int)$id);

		$res=$this->ejecuta_query($this->query);
		return $res;
	}


	public function getRecordData_DEPRECATED($idRecord=0)
	{
    $queryFiledNames='EXPLAIN %s.%s';
    $queryFiledNames=sprintf($queryFiledNames, $this->db, $this->mainTable);
		$this->getResultSelectArray($queryFiledNames);
		$this->mainTableFieldsNames=$this->tResultadoQuery;

		$sqlSelect='';
		foreach($this->mainTableFieldsNames as $key=>$value)
		{
			$sqlSelect.=$value['Field'].', ';
		}
		$sqlSelect=substr($sqlSelect,0,-2);
//echo '<hr>['.$idRecord.']<hr> ';return;

    $query='SELECT `COLUMN_NAME` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = "'.$this->db.'" && `TABLE_NAME` = "'.$this->mainTable.'" && `COLUMN_KEY` = "PRI";';
    $this->getResultSelectArray($query);
    $primaryColumnName=$this->tResultadoQuery;
    $primaryColumnName=$primaryColumnName[0]['COLUMN_NAME'];

		$query='SELECT %s FROM `%s`.`%s` WHERE `%s`=%d';
		$this->query=sprintf($query, $sqlSelect, $this->db, $this->mainTable, $primaryColumnName, (int)$idRecord);
//echo '<hr>'.$this->query.'<hr> ';return;

    $this->getResultSelectArray($this->query);
    $rowData=$this->tResultadoQuery;
//echo $this->query."\r\n".'--->'.$primaryColumnName;
//print_r($rowData);
		return $rowData[0];
	}


	/**
	 * DEVUELVE LA LISTA DE ELEMENTOS ENCONTRADOS PARA LA TABLA ESPECIFICADA (SI OMITIDA = mainTable)
	 *
	 * @param int $idRecord
	 * @param string $localTable
	 * @param array $arrFieldsTagSelect	DEBE CONTENER LOS NOMBRES DE LOS CAMPOS PARA OBTENER LOS DATOS, EL PRIMERO SIEMPRE EL ID DE LA TABLA
	 * @return array
	 */
	public function getSpecificRecordData($idRecord=0 ,$localTable=null, $arrFieldsTagSelect=null, $sqlWhere=null, $fieldOrder=null)
	{
		$sqlOrder=isset($fieldOrder) ? ' ORDER BY '.$fieldOrder : '';
		$sqlWhere=isset($sqlWhere) ? ' && '.$sqlWhere : '';
		$arrAliasNames=array('id_list', 'text_list');

		if(count($arrFieldsTagSelect))
		{
			$fieldsNames='';
			foreach($arrFieldsTagSelect as $key=>$value)
				$fieldsNames.=$value.' AS '.$arrAliasNames[$key].', ';

			$fieldsNames=substr($fieldsNames,0,-2);
		}

		$localTable=isset($localTable) ? $localTable : $this->commonDb->mainTable;
		$query='SELECT '.$fieldsNames.' FROM `%s`.`%s` WHERE 1 '.$sqlWhere.$sqlOrder;
		$this->query=sprintf($query, $this->commonDb->db, $localTable);
//echo '<hr>'.$this->query.' <hr><pre>';print_r($arrFieldsTagSelect);return;
		$this->commonDb->getResultSelectArray($this->query);
//    $rowData['select=id_father']=$this->commonDb->tResultadoQuery;
    $res=$this->commonDb->tResultadoQuery;

		return $res;
	}

}