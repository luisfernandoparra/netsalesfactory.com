<?php
/**
 *	CLASE CON LAS FUNCIONES DE BBDD NECESARIAS PARA CREAR LAS LISTAS DE LA BBSDD STANDARD DE GTP6
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class ModelLocalModule extends CBBDD
{
	protected $database;
	private $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $defaultOrderField;
	public $keyListLabel;
	public $mainTable;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;

	public function __construct($commonDb)
	{
		$this->commonDb=$commonDb;
		$this->arrFieldsTagSelect='';
		$this->query='';
    $this->prefixGtp=QUERY_TABLES_PREFIX;
		return;
	}

	/**
	 * DEVUELVE LA LISTA DE ELEMENTOS ENCONTRADOS PARA LA TABLA ESPECIFICADA (SI OMITIDA = mainTable)
	 *
	 * @param int $idRecord
	 * @param string $localTable
	 * @param array $arrFieldsTagSelect	DEBE CONTENER LOS NOMBRES DE LOS CAMPOS PARA OBTENER LOS DATOS, EL PRIMERO SIEMPRE EL ID DE LA TABLA
	 * @return array
	 */
	public function getSpecificRecordData($idRecord=0 ,$localTable=null, $arrFieldsTagSelect=null, $sqlWhere=null, $fieldOrder=null)
	{
		$sqlOrder=isset($fieldOrder) ? ' ORDER BY '.$fieldOrder : '';
		$sqlWhere=isset($sqlWhere) ? ' && '.$sqlWhere : '';
		$arrAliasNames=array('id_list', 'text_list');

		if(count($arrFieldsTagSelect))
		{
			$fieldsNames='';
			foreach($arrFieldsTagSelect as $key=>$value)
				$fieldsNames.=$value.' AS '.$arrAliasNames[$key].', ';

			$fieldsNames=substr($fieldsNames,0,-2);
		}

		$localTable=isset($localTable) ? $localTable : $this->commonDb->mainTable;
		$query='SELECT '.$fieldsNames.', IF(mnu.menu_id_related_option, IF((SELECT tmp.menu_id_related_option FROM '.$this->commonDb->db.'.'.$this->commonDb->mainTable.' AS tmp WHERE tmp.id_option_menu=mnu.menu_id_related_option),\'bgm-indigo c-white p-l-25\',\'bgm-lightblue c-white p-l-25\'), null) AS markSpecial
			FROM `%s`.`%s` AS mnu
			WHERE 1 '.$sqlWhere.$sqlOrder;
		$this->query=sprintf($query, $this->commonDb->db, $localTable);
//echo '<hr>'.$this->query.' <hr><pre>';print_r($arrFieldsTagSelect);return;
/**
			LEFT JOIN(SELECT tmp.menu_id_related_option, COUNT(*) AS qty FROM '.$this->commonDb->db.'.'.$this->commonDb->mainTable.' AS tmp GROUP BY tmp.menu_id_related_option) AS tmp
			ON mnu.id_option_menu = tmp.menu_id_related_option
(SELECT tmp.menu_id_related_option FROM '.$this->commonDb->db.'.'.$this->commonDb->mainTable.' AS tmp WHERE tmp.id_option_menu=mnu.menu_id_related_option) AS topFather

 */
		$this->commonDb->getResultSelectArray($this->query);
//    $rowData['select=id_father']=$this->commonDb->tResultadoQuery;
    $res=$this->commonDb->tResultadoQuery;

		return $res;
	}

}