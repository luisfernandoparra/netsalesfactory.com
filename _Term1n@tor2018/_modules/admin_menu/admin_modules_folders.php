<?php
@header('Content-Type: text/html; charset=utf-8');
@session_start();
include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$modulesFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/';

if($tmpDir=opendir($modulesFolder))
{
	$arrFolders=[];
	while(false !== ($entrada = readdir($tmpDir)))
		if($entrada != '.' && $entrada != '..' && is_dir($modulesFolder.$entrada))
			$arrFolders[]=$entrada;

	asort($arrFolders);
	closedir($tmpDir);
}

?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_comm_vendor_CSS.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_javascript_libraries.php');

$currentValue=$arraExtVars['currValue'] ? $arraExtVars['currValue'] : '' ;
?>
<style>
*{font-size:1.1em;}
.col-sm-3:hover{
background:#ddd;
color:#000;
cursor:pointer;
}
.col-sm-3{
margin-right:0px;
}
@media(max-width:1200px){
	*{font-size:1.02em;}
}
@media(max-width:768px){
	*{font-size:1em;}
}
</style>

<script type="text/javascript">
$(document).ready(function(){
	$("div[class=col-sm-3]").click(function(){
		var tmpValue=window.parent.$("input[name='<?=$arraExtVars['fieldName']?>']").val();
		var tmpNewClass=$(this).children("span").html();
//console.log(tmpValue,"-",tmpNewClass)
		window.parent.$("input[name='<?=$arraExtVars['fieldName']?>']").val(tmpNewClass);
		window.parent.$("#modalModulesFolder").modal('toggle');
	});

	$(".col-sm-3").addClass("p-5");

	$("i[class='zmdi <?=$currentValue?>']").addClass("c-red");

<?php
if(isset($_REQUEST['reload']) && $_REQUEST['reload'])
{
	echo 'parent.$(".currentAction").html("");';
}
?>
});
</script>

</head>
<body class="bgm-white">
<div class="card-body">

<?php
	$outHtml='<div class="m-5">';
//	$outHtml.='<div class="col-sm-3" data-name="" title="Utilizar esta entrada para elementos con sub-menús"><i class="zmdi zmdi-folder-outline"></i>&nbsp;&nbsp;<span>-</span></div>';
	$outHtml.='<div class="col-sm-3" data-name="" title="Utilizar esta entrada para elementos con sub-menús"><i class="zmdi zmdi-folder-outline"></i>&nbsp;&nbsp;<span>(submenu)</span></div>';
	$outHtml.='</div>';
	$outHtml.='<div class="row"></div>';

if(count($arrFolders))
{
	$outHtml.='<div class="m-5">';
	$arrSkipFolders=array('_deprecated','_system_backoffice');

	foreach($arrFolders as $key=>$value)
	{
		if(in_array($value, $arrSkipFolders))
			continue;

		if($_SESSION['userAdmin']['roleId'] < __LEVEL_ACCESS_GOD)
			continue;

		$outHtml.='<div class="col-sm-3" data-name="" data-code=""><i class="zmdi zmdi-folder"></i>&nbsp;&nbsp;<span>'.$value.'</span></div>';
	}
	$outHtml.='</div>';
}
else
	$outHtml='<center>No se encontraron carpetas de módulos en la ruta especificada</center>';

echo $outHtml;
?>

</div>
</body>