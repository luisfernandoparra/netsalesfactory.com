<?php
/**
 * DATE UPDATE: 2016-11-02 16:36 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * ADMIN USERS
 * 
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=$_SESSION['userAdmin']['roleId'];  // AJUSTA AUTOMATICAMENTE EL MAXIMO ROL PARA EL USUARIO ACCEDIDO
$minRoleAdim=1;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>Canales de captación (alias tipos de extracción o productos de GTP.</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='services';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='description';
$moduleSetting->keyListLabel='id_service';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->isCanEnabled=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole ? true : false;
$moduleSetting->fieldNameStatusRow='b_enabled';
//$moduleSetting->denyOtherUsers=[$_SESSION['userAdmin']['roleId'] => 'roleId'];
$moduleSetting->allowNewRecord=true;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->minEnableLevel=__LEVEL_ACCESS_MASTER;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[10,25,50]'; // array, EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;

/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array(
		' && '.$moduleSetting->fieldNameStatusRow.' = '=>0
		,'&& roleId <= '=>$_SESSION['userAdmin']['roleId']
// EXAMPLE WITH MORE SENTENCES:
//		,'|| ('=>''
//		,'theme = '=>'\'123\''
//		,')'=>''
	)
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode( $arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS



//echo $moduleSetting->moduleIcon.'<pre>';print_r($arrEraseOptions);echo'</pre>';
$order=0;

//echo $web_url.$adminFolder;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id_service'=>array(
    'formType'=>'text'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1]
    ,'required'=>1
    ,'label'=>'Id service'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>true
    ,'order'=>++$order
  )
  ,'description'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>2]
    ,'required'=>1
    ,'label'=>'Servicio'
    ,'helpText'=>'Nombre del servicio'
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'price'=>array(
    'formType'=>'text'
    ,'dataType'=>'float'
    ,'required'=>1
    ,'label'=>'Precio'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>'Precio'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'b_enabled'=>array(
    'formType'=>'checkbox'
    ,'dataType'=>'toggle'
    ,'tabularListData'=>['columnOrder'=>4]
    ,'required'=>0
    ,'toggleParams'=>array('tsColor'=>'green')
    ,'arrValues'=>array(0=>'<span class=c-red>bloqueado</span>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')
    ,'label'=>'Estado'
    ,'helpText'=>'Habilitar / deshabilitar este registro'
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
    )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  1=>array(
    'groupTextTitle'=>'Definición de los canales de captación'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);
