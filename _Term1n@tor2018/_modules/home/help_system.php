<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">
		<ul class="tab-nav text-center fw-nav" data-tab-color="green">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Crear un módulo</a></li>
			<li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">config_module.php</a></li>
			<li class=""><a href="#tabs3" data-toggle="tab" aria-expanded="false">buscar ayuda</a></li>
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>Pasos básicos de cómo proceder para crear un nuevo módulo desde 0; en este apartado se indica como crear un nuevo módulo para el back-office <span class="c-gray">(hasta que no esté listo el instalador...)</span>.</p>
					<ol>
						<li class="m-b-10">Situarse en la carpeta de <b>"_modules"</b> y seleccionar una carpeta existente que contenga el módulo que se crea más similar o adapto para las nuevas necesidades requeridas; copiar y pegar la carpeta al mismo nivel que la original, de este modo, si por ejemplo hemos seleccionado <b>"users"</b>, obtendremos <b>"users - copia"</b>.</li>
						<li class="m-b-10">Renombraremos la copia obtenida, en este ejemplo la llamaremos <b>"nuevo_modulo"</b>.</li>
						<li class="m-b-10">El nuevo módulo podrá ser accedido una vez hayamos insertado en la tabla de <b>"system_menu_admin"</b> los datos necesarios para su gestión.</li>
						<li class="m-b-10">Lo comentado aqu&iacute; arriba (<b>punto 3</b>), se puede conseguir de modo muy f&aacute;cil insertando un nuevo registro desde el m&oacute;dulo de <i>gesti&oacute;n de men&uacute;s</i> de este mismo back-office .</li>
						<li class="m-b-10">Llegados a este punto, es necesario personalizar/configurar el módulo que acabamos de crear; para ello se debe editar el archivo específico que controla la mayoría de las funcionalidades del mismo módulo: <a href="#null" class="gotoHelpTag" data-target-tag=".mainHelConfigModule" data-no-scroll="true">config_module.php</a> <span class="c-gray">(este script se encuentra siempre en la misma carpeta del módulo)</span>. Es posible editar este archivo directamente desde el mismo back-office, haciendo click en <b>"Configuración del módulo"</b>.<br /><u>Importante:</u> si se comete algún error "crítico" en este archivo, seguramente el módulo no será más acesible desde el back-office, por lo que deberemos editar externamente <a href="#null" class="gotoHelpTag" data-target-tag=".mainHelConfigModule" data-no-scroll="true">config_module.php</a>.</li>
						<li class="m-b-10">Nota: cada vez que efectuemos un cambio en el archivo de configuración (esto SOLO utilizando el editor integrado en el back-office), se crea siempre una copia de back-up que se listará automáticamente para su posible recuperación.</li>
					</ol>
				</div>
				<div class="tab-pane fade f-16" id="tab2">
						<p>El archivo "config_module.php" contiene la gran parte de parámetros necesarios para el funcionamiento de los módulos del back-office.</p>
						<p>Debemos recordar que este archivo contiene también las definiciones para los campos a utilizar en el listado principal y en la edición del mismo, esto para todos los módulos con estructuras simples.</p>
						<p>Para más información y muchos más detalles, accede a la sección específica de <a href="#null" class="gotoHelpTag" data-target-tag=".mainHelConfigModule" data-no-scroll="true">config-module</a>
				</div>

				<div class="tab-pane fade f-16" id="tabs3">
						<p>La construcción de las ayudas se consigue con la carga del ajax "<b>ajax/common_admin.php</b>" (<i>$arraExtVars['action']</i> == '<i>getHelpModule</i>') desde (<i>js/dujok.js</i>) con cualquier control que tenga definida la clase CSS "<i>basicHelpOpen</i>" o para abrir una ventana externa la clase CSS "<i>openNewWindow</i>"</p>
				</div>


				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>

			</div>
	</div>
</div>
