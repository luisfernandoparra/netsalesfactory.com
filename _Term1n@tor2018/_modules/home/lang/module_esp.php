<?php
//	local language
//	SPANISH

$langTxt['mod']['ajaxError']['queryTxtFail']='<u>Ha ocurrido un error</u> para la consulta efectuada';
$langTxt['mod']['ajaxError']['accessViolation']='Parece que usted no deber&iacutea estar aqu&iacute';
$langTxt['mod']['ajaxInfo']['queryNoData']='sin datos';
$langTxt['mod']['ajaxInfo']['NoDataDateRangeRecords']='<u>No se encontraron datos</u> para el rango de fechas deseado';

$langTxt['mod']['notify']['jsErrorStatsLoad']='No se ha procedido con la carga correcta de los datos';

$langTxt['mod']['bodyTxt']['welcomeTxt']='Bienvenido a la administración de Back-Office, v';

$langTxt['mod']['btnAlt']['title']='NOTA IMPORTANTE';
$langTxt['mod']['btnAlt']['content']='Recuerda que la actualizaci&oacute;n de las estad&iacute;sticas y los datos mostrados es un porceso que puede ser muy lento y pesado para el sistema';
$langTxt['mod']['btn']['txt']='Actualizar estad&iacute;sticas';

$langTxt['mod']['modalMsg']['txtExtractionResponse']='para los tipo de extración con respuesta = ';
$langTxt['mod']['modalMsg']['txtGraphDetailsContent']='AQUÍ LAS OPCIONES PARA GENERAR LOS GRÁFICOS....';
$langTxt['mod']['modalMsg']['txtGraphDetailsTitle']='Selector de opciones para detalles de gráficos';

