<?php
//	local language
//	ITALIAN

$langTxt['mod']['ajaxError']['queryTxtFail']='<u>Si &egrave; riscontrato un problema</u> per la query effettuata';
$langTxt['mod']['ajaxError']['accessViolation']='Sembra que lei non si dovrebbe ttrovare qu&grave;!';
$langTxt['mod']['ajaxInfo']['queryNoData']='senza dati';
$langTxt['mod']['ajaxInfo']['NoDataDateRangeRecords']='<u>Non abbiamo trovato dati</u> per le date specificate';

$langTxt['mod']['notify']['jsErrorStatsLoad']='Non &egrave; stato possibile procedere con il corretto caricamento dei dati';

$langTxt['mod']['bodyTxt']['welcomeTxt']='Benvenuto all` amministrazione del Back-Office, v';

$langTxt['mod']['btnAlt']['title']='NOTA IMPORTANTE';
$langTxt['mod']['btnAlt']['content']='Ricorda che l`aggiornamento delle statistiche ed i dati visualizzati &egrave; un processo che pu&ograve; risultare moto oneroso per il sistema e potrebbe risultare piuttosto lento';
$langTxt['mod']['btn']['txt']='Aggiornare le statistiche';

$langTxt['mod']['modalMsg']['txtExtractionResponse']='per le estrazioni con risposta = ';
$langTxt['mod']['modalMsg']['txtGraphDetailsContent']='AQUÍ LAS OPCIONES PARA GENERAR LOS GRÁFICOS....';
$langTxt['mod']['modalMsg']['txtGraphDetailsTitle']='Selettore delle opzioni per i dettagli';

