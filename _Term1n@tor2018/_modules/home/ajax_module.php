<?php
/**
 * SPECIFIC SCRIPT ATCIONS
 *
 * AJAX RESPONSE ACTIONS ==> PARAMETRO ´action` (COMUN PARA TODAS LAS ACCIONES)
 *
 * NOMBRE-ACCION: BREVE COMENTARIO DESCRIPTIVO
 *
 * M.F. 2018.02.08
 */

header('Content-Type: text/html; charset=utf-8');
@session_start();
$response['successStatus']='';
//echo '<pre>';print_r($_REQUEST);print_r($_SESSION);echo '</pre>';//die();
if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_REQUEST;

// START LOCAL MODULE LANGUAGE TRANSLACTIONS
if(isset($_SESSION['userAdmin']['pref_']['ref_lang']) && isset($arraExtVars['from']))
{
	include('../../../conf/config_web.php');
	include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');
	//include('../../../conf/config.misc.php');

	$dirLang=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'].'lang/';
	$filLang=$dirLang.'module_'.strtolower($_SESSION['userAdmin']['pref_']['ref_lang']).'.php';

	if(file_exists($filLang))
	{
		include($filLang);
		$wordSep='_'; $tmpWord=null; $tmpValue=null;

		foreach($langTxt as $kL=>$vL)
		{
			$tmpWord=$kL;

			foreach($vL as $nn=>$tmpValue1)
			{
				$tmpWord_2=null;
				if(is_array($tmpValue1))
				{
					$tmpWord_3=null;

					foreach($tmpValue1 as $nv=>$tmpValue2)
					{
						$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
						define('__'.strtoupper($tmpWord_3), $tmpValue2);
					}
				}
				else
				{
					$tmpWord_2=$tmpWord.$wordSep.$nn;
					define('__'.strtoupper($tmpWord_2), $tmpValue1);
				}
			}
		}
	}
}
// END LOCAL MODULE LANGUAGE TRANSLACTIONS



//	START prevent direct file access or lost work session
if(!isset($_SERVER['HTTP_REFERER']) && basename($_SERVER['SCRIPT_NAME']) != 'index.php' || !$_SESSION['userAdmin'])
{
  @include_once('../../../conf/config_web.php');
  $response['msgPainTxt']="\nARGGG !".@$arraExtVars['idRef'];
  $response['errorText']='<br />'.__MOD_AJAXERROR_ACCESSVIOLATION;

  if(!@$_SESSION['userAdmin'] && isset($_SERVER['HTTP_REFERER'])) // SE HA PERDIDO O O NO SE TIENE SESION DE TRABAJO
  {
    $response['errorText']='<br />'.(defined('__MOD_AJAXINFO_LOSTSESSIONMSG') ? __MOD_AJAXINFO_LOSTSESSIONMSG : 'Error in your session').'<br /><br /><p class="text-center p-5"><button class="btn bgm-orange waves-effect p-20 w-100 logoutCssHome">'.(defined('__MOD_AJAXINFO_LOSTSESSIONBTNTXT') ? __MOD_AJAXINFO_LOSTSESSIONBTNTXT : 'PLEASE, restart your session work').'</button></p>';
    $response['timeWait']=10000;
  }

  $response['successStatus']=$response['successStatus'] ? $response['successStatus'] : 'danger';
  $response['timeWait']=$response['timeWait'] ? $response['timeWait'] : 1000;
  $response['success']=false;
	$res=json_encode($response);
	die($res);
}	//	END prevent direct file access or lost work session




echo "\n";print_r($arraExtVars);die('END AJAX FILE !!!!!!!!!!!!!!!!!!!');
