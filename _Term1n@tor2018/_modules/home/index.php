<?php
/**
 * MAIN MODULE - HOME
 * DEFAULT ACTION: list record data (PART 1/3)
 *
 */
//@putenv('TZ=Europe/Amsterdam');
@header('Content-Type: text/html; charset=utf-8');
@session_start();

include('config_module.php');
if($_SESSION['userAdmin']['firstHomeLoad'])
	$_SESSION['userAdmin']['momentLastStatsUpdate']=date('H:i:s');

$dataModuleBBDD=new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
//$dataModuleBBDD=new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting, null, 'utf8', $port);
$resConnexion=$dataModuleBBDD->connectDB();

if(!$resConnexion)
{
	$msgModuleError='<div class="alert alert-danger m-t-20 m-t-20 f-700 f-20 text-center">'.__COMMON_GENERICTEXT_WARNING.'<br /><br /><br />'.__COMMON_BBDD_TXTCONNECTERROR.'</div>';
	echo $msgModuleError;
	die();
}

$dataModuleBBDD->logBackOffice(null,null,$arraExtVars['modLabel']);//null,null,$arraExtVars['moduleAdmin']

?>
<style>
.btn[class*="bgm-"]:not(.bgm-white){right:11px;}
.divOacityLow{opacity:.3;box-shadow:inset 0px 0px 100px rgba(254,254,254,1);}
.divOacityLow div div{color:#000!important;;}
.divOacityNormal{opacity:1}
</style>
<script type="text/javascript">
/*
function statisticsUpdate()
{
	$.ajax({
		url: "<?=$web_url.$adminFolder.'/'.$arraExtVars['moduleAdmin']?>ajax_module.php", method: "post", dataType: "json",
		data: {
			action: "getHomeStatistics"
			,from: "<?=$arraExtVars['moduleAdmin']?>"
		},
		cache: false,
		async: true,
		success: function(response)
		{
      if(response.errorText)
      {
        $(".additionalInfoUpdate").fadeIn().html(" <div class='p-0 p-r-25 alert alert-"+(response.successStatus ? response.successStatus : "danger")+" alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>&nbsp;("+(response.errorShortText ? response.errorShortText : "error en la actualización de los datos visualizados")+")&nbsp;</div>");
  			commonNotify(response.msgPainTxt, response.errorText ? response.errorText : "", undefined, "center", undefined, response.successStatus, "animated flipInX", "zoomOut", response.timeWait ? response.timeWait : 2000);
    		$(".momentLastStatsUpdate").html(response.momentLastStatsUpdate);
        $(".percent").html("<i class='zmdi zmdi-grid-off'></i>");
  			return false;
      }

      $(".additionalInfoUpdate").html("").hide();
      // VALOR ABSOLUTO DEL TIPO DE EXTRACCION EVALUADA
			$.each(response.data,function(extractionResponseInt,labelResponse){
				$(".displayExtraction_"+extractionResponseInt+"Tot").html(" <b>"+labelResponse["displayExtraction_"+extractionResponseInt]+"</b>");
				$(".percentExtraction_"+extractionResponseInt).html(labelResponse["percentExtraction_"+extractionResponseInt]);
				var zeroString = "00";
				var paddedNum=zeroString.substring((extractionResponseInt + "").length, 4) + extractionResponseInt;

				$(".responseType_"+paddedNum).removeClass("divOacityLow");
				$(".responseType_"+paddedNum).removeClass("divOacityNormal");
//console.log(labelResponse,extractionResponseInt," --->",labelResponse["percentExtraction_"+extractionResponseInt],": ",(labelResponse["integerExtraction_"+extractionResponseInt]) > 0)
				if(labelResponse["percentExtraction_"+extractionResponseInt] != undefined)
				{
					$(".percentExtraction_"+extractionResponseInt+"Graph").data('easyPieChart').update(labelResponse["percentExtraction_"+extractionResponseInt]);
					if(labelResponse["integerExtraction_"+extractionResponseInt] > 0)
						$(".responseType_"+paddedNum).addClass("divOacityNormal");
					else
						$(".responseType_"+paddedNum).addClass("divOacityLow");
				}
				else
					$(".responseType_"+paddedNum).addClass("divOacityLow");
			});

		
			commonNotify(response.msgPainTxt, response.errorText ? response.errorText : "", undefined, "center", undefined, response.successStatus, "animated flipInX", "zoomOut", response.timeWait ? response.timeWait : 2000);
			$(".momentLastStatsUpdate").html(response.momentLastStatsUpdate);
      return true;
		},
		error: function (response) {
			$(".additionalInfoUpdate").fadeIn().html(" <div class='p-0 p-r-25 alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button>&nbsp;("+(response.errorShortText ? response.errorShortText : "error en la actualización de los datos visualizados")+")&nbsp;</div>");
			var today = new Date();
			today=today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			commonNotify("<b><?=__COMMON_GENERICTEXT_ERROR?></b>", "<br /><br /><?=__MOD_NOTIFY_JSERRORSTATSLOAD?>", undefined, "center", undefined, 'danger', "animated flipInY", "rotateOut",5000);	// MENSAJE NO INVASIVO
			return false;
		}
	});
	return;
}
*/
$(document).ready(function(){
//  $(".modalGetGraphDetails").click(function(){
//    var extracionType=$(this).parents().attr("class");
//    extracionType=extracionType.substr(extracionType.length - 2);
//    $("#modalGraphDetails").modal();
//    $(".idTitleRef").html("<br /><br /><b><?=__MOD_MODALMSG_TXTEXTRACTIONRESPONSE?>"+extracionType+"</b>")
//  })

	$("body").on("click",".logoutCssHome",function(){
		$.ajax({
			url:root_path + "ajax/common_admin.php", method: "post", dataType: "json", data:{action:10}, cache:false
			,success: function (response)
			{
//				if(response.success)
					document.location.href=".";
			},
			error:function(response){
				document.location.href=".";
				return false;
			}
		});
	})

//	$(".updateStatisticsButton").click(function(){
//		statisticsUpdate();
//	});

  $(window).load(function(){

<?php
if(!$_SESSION['userAdmin']['firstHomeLoad'])
{
//	echo 'statisticsUpdate();';	// EJECUTA LA CARGA DE LAS ESTAISTICAS AUTOMATICAMENTE SOLO EN LA PRIMERA CARGA DE LA HOME DEL USUARIO ACTIVO
}
?>
		setTimeout('$(".blockTWelcome").parent().parent().parent().parent().parent().slideUp(\'slow\');',4000);

  });
<?php
if($_SESSION['userAdmin']['firstHomeLoad'])
{
	echo '$(".blockHomeWelcome").removeClass("hidden");';
}
?>
});

</script>
<?php
//if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD)
//	echo '<div class="">&nbsp;<a href="#modalEmailSend" data-toggle="modal" ><i class="p-5 zmdi zmdi-mail-send c-bluegray waves-effect" title="test email" ></i></a></div>';
?>

<div class="card-header">
		<div class="pull-right">
<!--			<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect updateStatisticsButton" data-trigger="hover" data-toggle="popover" data-original-title="<?=__MOD_BTNALT_TITLE?>" data-content="<?=__MOD_BTNALT_CONTENT?>" data-placement="left" ><?=__MOD_BTN_TXT?></button>-->
		</div>
		<h2><?=__MOD_BODYTXT_WELCOMETXT?><?=$versionBackOffice?>  / <?=$arraExtVars['modLabel']?>
<!--			<small class="f-12 c-gray">Momento de la &uacute;ltima actualizaci&oacute;n de las estad&iacute;sticas: <span class="c-bluegray momentLastStatsUpdate"><?=@$_SESSION['userAdmin']['momentLastStatsUpdate']?></span><span class="c-bluegray additionalInfoUpdate"></span></small>-->
		</h2>
</div>

<div class="row">
<?php
//echo '<pre>';print_r($_SESSION['userAdmin']['name']);echo '</pre>';
// MENSAJE AL NO TENER CONFIGURADO CORRECTAMENTE EL IDIOMA PARA EL USUARIO
if(!isset($_SESSION['userAdmin']['pref_']['ref_lang']) || !@$_SESSION['userAdmin']['pref_']['ref_lang'])
	echo '	<div class="card-body card-padding">
    <div class="col-sm-12 text-left">
      <div class="p-t-20 p-b-15 text-center alert alert-warning f-16">
        <div class="p-b-10"><b>ERROR INFO:</b><br />You don\'t have languages defined !<br />No tienes idiomas definidos!<br />No hai definito il linguaggio da adoperare!
        </div>
      </div>
    </div>
	</div>
';

?>
	<div class="card-body blockHomeWelcome hidden" >
    <div class="w-100 text-center">
			<center>
				<div class="bgm-white p-t-5 p-b-25 brd-10 modal-dialog" style="border:2px solid #666;">
					<div class="c-black p-20">
						<span class="blockTWelcome"><br /><h1>Bienvenido <?=strtoupper($_SESSION['userAdmin']['name'])?></h1><br />a la gesti&oacute;n de la versi&oacute;n back-office 2018</span>
					</div>
				</div>
			</center>
    </div>
	</div>
</div>

<div class="card-body card-padding" >
    <div class="w-100">
			<div class="bgm-lightgray ">
				<div class="c-black p-5">
					<span class="">
						<h4>Breve resumen de los pasos a seguir en la creación de una nueva landing</h4>
						<ol class="OlSubLevels">
							<li>Acceder al grupo de módulos <b>Configuración landings</b></li>
							<li>En el módulo <b>Clientes ( CRM )</b> verificar la existencia del cliente para la landing a crear; si no existiera, primero ha de comprobarse que en la tabla <b>clientes</b> ya esté creado, en caso afirmativo aprarecerá en el listado de <b>Ref-Cliente-CRM</b> y podrá ser utilizado para crear la landing.</li>
							<li>
								Continuar con el módulo <b>Config. básica landings</b> para definir los parámetros básicos que identificarán la landing.
								<ol class="OlSubLevels">
									<li>Seleccionar el cliente de la landing.</li>
									<li>Utilizar para <i>"Nombre landing"</i> e <i>Identificador</i> la misma referencia, solo con caracteres ingleses, guiones y sin espacios.</li>
									<li>Si no se indica nada en <i>"Script index"</i>, automáticamente se cargará <i>body_common.php</i>; en caso contrario, el script indicado se deberá hallar en <i>includes/</i>, que es la ruta donde se busca automáticamente, ejemplo: <b>body_0001.php</b>.</li>
									<li>Para los campos <i>"Política de privacidad"</i> y <i>"Aviso legal"</i> (esto vale para configuraciones standard, recordar que los scrpts pueden ser cambiados individualmente en cada landing), cuando la longitud del texto es inferior a <b>100 caracteres</b>, se utiliza automáticamente este contenido de nombre de archivo, el sistema lo buscará y lo abrirá, si contrariamente supera los 100 caracteres, se dibujará su contenido cargando respectivamente <b>includes/privacy_policies.php?cr=<i>ID_LANDING</i></b> para la "política de privacidad" y <b>includes/landing_conditions.php?cr=<i>ID_LANDING</i></b></li>
								</ol>
							</li>
							<li>
								Último módulo a configurar es ya el de los contenidos HTML que deben aparecer en la landing
								<ol class="OlSubLevels">
									<li>Seleccionar la landing creada en el paso anterior buscando el identificador en el desplegable <i>"Nombre landing"</i>.</li>
									<li>Con el control <i>"Tipo formulario registro"</i> se define el script que se cargará para dibujar el formulario de la landing; si se designa <i>"standard"</i>, automaticamente se cargará el contenido de <b>"includes/form_script_common.php"</b>, en caso contrario, el sistema buscará en includes <i>"form_data_<b>ID_LANDING</b>"</i>, ejemplo: <i>form_data_001</i>.</li>
								</ol>
							</li>
						</ol>
					</span>
				</div>
			</div>
    </div>
	</div>
</div>


<div class="modal fade" data-modal-color="blue" id="modalGraphDetails" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?=__MOD_MODALMSG_TXTGRAPHDETAILSTITLE?></h4>
			</div>
			<div class="modal-body">
        <p><i><?=__MOD_MODALMSG_TXTGRAPHDETAILSCONTENT?></i><span class="idTitleRef"></span></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect actAbortDelete" data-dismiss="modal"><?=__COMMON_BTN_CANCEL?></button>
			</div>
		</div>
	</div>
</div>



<?php

//echo '--->'.$_SESSION['userAdmin']['firstHomeLoad'];
//unset($_SESSION['userAdmin']['firstHomeLoad']);
//$_SESSION['userAdmin']['firstHomeLoad']=1;
$_SESSION['userAdmin']['firstHomeLoad']=false;	// HABILITAR ESTA LINEA CUANDO TODO OK


//echo'<div class="row"><pre>';
//print_r($arraExtVars);
//echo'<hr>session:';
//echo'b_display_sql=';print_r($_SESSION['userAdmin']['pref_']);echo'</div>';
// NECESARIO PARA ESTABLECER ACCIONES AUTOMÁTICAS
