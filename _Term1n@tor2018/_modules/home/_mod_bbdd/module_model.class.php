<?php
/**
 *	CLASE CON LAS FUNCIONES DE BBDD NECESARIAS PARA CREAR LAS LISTAS DE LA BBSDD STANDARD DE GTP6
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class ModelLocalModule extends CBBDD
{
	protected $database;
	private $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $mainTable;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;

	public function __construct($db_type=null, $db_host='', $db_user='', $db_pass='', $db_name='', $localParams=null, $port='3306')
	{
		$this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
		$this->prefixGtp=__QUERY_TABLES_PREFIX;
		$this->query='';
		$this->mainTable=$this->prefixGtp.$localParams->mainTable;
		$this->listFieldsOutput=''; $this->mainTableFieldsNames=''; $this->defaultWhereSql='';
		$this->defaultOrderField='';
		return;
	}


	/**
	 *
	 * @param string $localTable
	 * @param string $getExtractionsFrom
	 * @param string $getExtractionsTo
	 * @param integer $response
	 * @return object
	 */
	public function getHomeStatistics($localTable=null, $getExtractionsFrom=null, $getExtractionsTo=null, $response=0)
	{
//		$sqlOrder=isset($fieldOrder) ? ' ORDER BY '.$fieldOrder : '';
		$sqlWhere=' && DATE(fecha_envio) BETWEEN ';
		$sqlWhere.=isset($getExtractionsFrom) ? '\''.$getExtractionsFrom.'\'' : 'CURDATE()';
		$sqlWhere.=' AND ';
		$sqlWhere.=isset($getExtractionsTo) ? '\''.$getExtractionsTo.'\'' : 'CURDATE()';
		$sqlWhere.=' || (DATE(fecha_entrega) BETWEEN ';
		$sqlWhere.=isset($getExtractionsFrom) ? '\''.$getExtractionsFrom.'\'' : 'CURDATE()';
		$sqlWhere.=' AND ';
		$sqlWhere.=isset($getExtractionsTo) ? '\''.$getExtractionsTo.'\' && estado=0)' : 'CURDATE() && estado=0)';

		if($response)
			$sqlWhere.=' && respuesta = '.(int)$response;

		$sqlWhere.=' GROUP BY respuesta, estado';

		$localTable=isset($localTable) ? $this->prefixGtp.$localTable : $this->mainTable;
		$query='SELECT IF(respuesta = 5 || respuesta = 3, respuesta, estado) AS response_eval, estado, COUNT(id_extraccion) AS num_records FROM `%s`.`%s` WHERE 1 '.$sqlWhere;
		$this->query=sprintf($query, $this->db, $localTable);
		$this->getResultSelectArray($this->query);
    $res=$this->tResultadoQuery;
//echo	$this->mError.'<hr>'.$this->query.' <hr><pre>=====>';print_r($this);echo '</pre>';return;
//    $rowData['select=id_father']=$this->tResultadoQuery;

		return $res;
	}
}