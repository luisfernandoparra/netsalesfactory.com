<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">
		<ul class="tab-nav text-center fw-nav" data-tab-color="green">
			<li class="active"><a href="#tabu1" data-toggle="tab" aria-expanded="false">nota sobre este mismo módulo</a></li>
			<li><a href="#tabu3" data-toggle="tab" aria-expanded="false">Buscar ayudas del back-office</a></li>
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade f-16 in active" id="tabu1">
          <p class="bgm-white p-20">Página inicial (en definición de contenidos y funcionalidades).</p>
				</div>
				<div class="tab-pane fade f-16 in active" id="tabu3">
						<p>Es posible efectuar búsquedas en los contenidos de las ayudas indicando el texto a encontrar en el panel deslizante derecho del back-office.</p>
						<p>Recordar quer para iniciar la búsqueda, se debe primero escribir el texto y después pulsar la tecla "<i>enter</i>"</p>
						<p>Si existe el texto que se busca, aparece una pequeña ventana modal (verde) con un botón(o botones) y el nombre de la sección (o secciones) donde este aparece (indicando además el número de las coincidencias halladas): presionándolo se abre el módulo de la ayuda que se está buscando, el sistema selecciona automáticamente el primer nivel de ayuda.... pero hay que recordar que si la coincidencia se encuentra en algún sub-nivel, estos no se abrirán automáticamente.</p>
				</div>

				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>

			</div>
	</div>
</div>
