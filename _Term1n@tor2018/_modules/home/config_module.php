<?php
/**
 * DATE UPDATE: 2017-01-19 17:32 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * NOTA IMPORTANTE:
 *
 * CAMBIAR A 5 EL VALOR DEL CAMPO estado CUANDO OCURRE UN ERROR INTERNO, (ACTALMENTE ES 2)
 * PARA LOS DATOS FUTUROS DE LA TABLA DE EXTRACCIONES PENDIENTES
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$moduleSetting=new stdClass();
$moduleSetting->mainTable='';

// CONFIGURACION DE LOS ESTADOS PARA LAS EXTRACCIONES PENDIANTES DIARIAS A VISUSALIZAR
//$moduleSetting->arrExtractionsRespuestasDataTypes=array(
//	0=>['Extraction_0','bgm-gray','Extracciones en espera']	// EN ESPERA
//	,1=>['Extraction_1','bgm-green','Extracciones correctas']	// OK
//	,3=>['Extraction_3','bgm-orange','Extracciones duplicadas']	// DUPLICADO
//	,2=>['Extraction_2','bgm-red','Extracciones en ERROR']	// ERROR
//	,5=>['Extraction_5','bgm-indigo','Paradas por filtros']	// FILTRO
//	,7=>['Extraction_7','bgm-purple','Capping']
//	,11=>['Extraction_11','bgm-brown','Cuarentena teléfono']
//	,12=>['Extraction_12','bgm-deeporange','Cuarentena email']
//	,15=>['Extraction_15','bgm-pink','Volúmen global']
//	,23=>['Extraction_23','bgm-black','Cuarentena email interno']
//);


//emailbidding