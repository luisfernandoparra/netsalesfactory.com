<?php
/**
 * DATE UPDATE: 2016-11-02 07:16 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * ADMIN USERS
 *
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=__LEVEL_ACCESS_GOD;
$minRoleAdim=1;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>comentarios espec&iacute;ficos del m&oacute;dulo actual....</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='MY_TABLE_NAME';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='MY_FIELD';
$moduleSetting->keyListLabel='id';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->isCanEnabled=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole ? true : false;
$moduleSetting->fieldNameStatusRow='MY_FIELD';
$moduleSetting->allowNewRecord=true;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->minEnableLevel=__LEVEL_ACCESS_MASTER;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[10,25,50]'; // EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;

/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array(
//		' && activo = '=>0
// EXAMPLE WITH MORE SENTENCES:
//		,'|| ('=>''
//		,'theme = '=>'\'123\''
//		,')'=>''
	)
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode($arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS

$order=0;
//echo $web_url.$adminFolder;
/**
 * LOS CAMPOS DEBEN APARECER POR EL ORDEN AQUI ABAJO
 * DUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id'=>array(
    'formType'=>'hidden'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1]
    ,'required'=>0
    ,'label'=>'ID'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'usuario'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>2]
    ,'required'=>2
    ,'label'=>''
    ,'helpText'=>'??'
    ,'visible'=>1
    ,'placeHolder'=>'El nombre de usuario para acceder al back-office'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'e_mail'=>array(
    'formType'=>'email'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>3]
    ,'required'=>1
    ,'label'=>'E-mail'
    ,'helpText'=>''
    ,'tagPattern'=>'[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$'
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'profile_image'=>array(
    'formType'=>'file'
    ,'dataType'=>'imageFile'  // allFile / imageFile
    ,'required'=>false
    ,'label'=>'LABEL X'
    ,'helpText'=>''
    ,'visible'=>true
    ,'placeHolder'=>''
    ,'htmlFolder'=>$web_url.$adminFolder.'/profile_images/'
    ,'targetFolder'=>$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/profile_images/'
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'nivel_acceso'=>array(
    'formType'=>'text'
    ,'dataType'=>'slider'
    ,'sliderConf'=>array('startValue'=>0,'endValue'=>20,'stepValue'=>1) // ESTE PARAMETRO SE GESTIONA EN _modules\common_edit_html_top.php
    ,'required'=>0
    ,'label'=>'Nivel  del role'
    ,'helpText'=>'Niveles de permisos para el admin, '.$minRoleAdim.' = m&aacute;s bajo, '.$maxRoleAdim.' = m&aacute;s alto'
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
 ,'b_enabled'=>array(
    'formType'=>'checkbox'
    ,'dataType'=>'toggle'
    ,'tabularListData'=>['columnOrder'=>4]
    ,'required'=>0
    ,'toggleParams'=>array('tsColor'=>'green')
//    ,'arrValues'=>array(0,1)	// PARA ESTE CONTROL, SIEMPRE EL PRIMER VALOR ES EL NEGATIVO Y EL SEGUNDO EL POSITIVO
    ,'label'=>'LABEL X'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  1=>array(
    'groupTextTitle'=>'Gesti&oacute;n de registros'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
  ,2=>array(
    'groupTextTitle'=>'Otras opciones del usuario'
    ,'additionalInfo'=>'Par&aacute;metros funcionales'
    ,'groupIco'=>'zmdi zmdi-view-compact'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);

