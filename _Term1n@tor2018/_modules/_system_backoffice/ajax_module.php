<?php
/**
 * LOCAL AJAX MODULE
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');

ini_set('display_errors',1);
error_reporting($errorReportDefault);
$moduleParams='';

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
//$response['query']=array();
$response['success']=false;

if($hashSecutityScript != __HASH_CONTROL_SCRIPT){
  die(__MSG_ACCESS_ERROR);
}

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$tmpVars=json_decode(file_get_contents('php://input'), true);	// DECODE JSON DATA

if(count($tmpVars))
  $arraExtVars=$tmpVars;

//include($dirModAdmin.'mod_bbdd.class.php');
////	include($dirModulesAdmin.$arraExtVars['moduleAdmin'].'_mod_bbdd/module_model.class.php');
////	$data=new ModelLocalModule($db_type, $db_host, $db_user, $db_pass, $db_name);
////	$resConnexion=$data->connectDB();
//$data=new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
//$resConnexion=$data->connectDB();
//
//if($hashSecutityScript !== __HASH_CONTROL_SCRIPT)
//{
//	$data->logBackOffice(0,null,''.__MSG_ACCESS_ERROR);
//	$response['txtError']=strip_tags(__MSG_ACCESS_ERROR);
//	$res=json_encode($response);
//	die($res);
//}

/*
if(isset($arraExtVars['moduleAdmin']) && $arraExtVars['moduleAdmin'])
{
  $localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'_mod_bbdd/';
  $localModuleFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'';
	$provenience=$_SERVER['HTTP_REFERER'];
	$modNameFrom=strpos($provenience,'_modules/')+9;
	$modName=substr($provenience, $modNameFrom);
	$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));

	if(file_exists($localModelsFolder.'/module_model.class.php'))
	{
		include($dirModAdmin.'mod_bbdd.class.php');
		include($localModelsFolder.'module_model_ajax.class.php');
	}

	$dataModuleBBDD=null; $msgModuleError=' Error en el script Ajax: <br>'.__FILE__;
	$dataModuleBBDD=new ModelLocalModuleAjax($db_type, $db_host, $db_user, $db_pass, $db_name, null, $port);
	$resConnexion=$dataModuleBBDD->connectDB();
	$cur_conn_id=$dataModuleBBDD->idConexion;
}
*/


if(isset($arraExtVars['action']) && $arraExtVars['action'] == 'changeHtpass')
{
	$response['success']=true;
	include($dirModAdmin.'mod_bbdd.class.php');
//	include($dirModulesAdmin.$arraExtVars['moduleAdmin'].'_mod_bbdd/module_model.class.php');
//	$data=new ModelLocalModule($db_type, $db_host, $db_user, $db_pass, $db_name);
//	$resConnexion=$data->connectDB();

	$data=new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
	$resConnexion=$data->connectDB();
//echo $port;
//  $localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'_mod_bbdd/';

//	$response['success']=true;
//echo __HASH_CONTROL_SCRIPT;
//  $localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'_mod_bbdd/';

  if(!$data->idConexion->sqlstate)
  {
		$response['success']=false;
		$response['msgTitle']='<h2 class="c-white">ERROR DE CONEXI&Oacute;N</h2>';
		$response['msgText']='<center><span class="c-white f-16">Ha ocurrido un error al intentar hacer la conexión a la BBDD</span><br /><br />si el error persiste, contactar con informática.</center>';
		$response['timeWait']=10000;
    $res=json_encode($response);
    die($res);
  }

	include($dirModulesAdmin.$arraExtVars['moduleAdmin'].'config_module.php');
	$fileDetailsTmp=array();
	$fileDetailsTmp[]=$charEnabled.'AuthType Basic';
	$fileDetailsTmp[]=$charEnabled.'AuthName "Area restringida"';
	$fileDetailsTmp[]=$charEnabled.'AuthUserFile '.$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/.htpasswd';
	$fileDetailsTmp[]=$charEnabled.'Require valid-user';
	$charEnabled=$arraExtVars['db'][$moduleSetting->mainTable]['htaccess_enabled'][0] ? '' : '#';

	// SI NO EXISTE .htaccess, SE CREA EL NUEVO FILE
	if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/.htaccess'))	// SI NO EXISTE .htaccess, SE CREA
	{
//$fileDetailsTmp[]='#TEST';
		foreach($fileDetailsTmp as $data)
			$strContent.=$data."\n";

		$fp=@fopen($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/.htaccess','w+');
		$resFile=fputs($fp, $strContent);
		fclose($fp);

		if($resFile)
		{
			$response['timeWait']=15000;
			$response['msgTitle']='<h3 class="c-white">CREADO CORRECTAMENTE htaccess </h3>';
			$response['msgText']='<span class="f-18 text-left"><span class="c-yellow">ha sido creado el archivo en la carpeta del back-office</span><br />desde ahora se puede habilitar una capa adicional de protección al back-office.<br><b>Si no consigue acceder</b>, debe solicitar que le habiliten el acceso a los <b>gestores del sitio Web</b>.</span>';
			$response['success']=true;
		}
		else
		{
			$response['timeWait']=10000;
			$response['msgTitle']='<h2 class="c-white">ERROR en cración de archivo</h2>';
			$response['msgText']='<center><span class="c-white f-16">imposible crear el archivo en la carpeta del back-office</span><br />Contactar con informática.</center>';
			$response['success']=false;
		}
	}
	else
	{
		$theFile=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/.htaccess';
		$fileDetails=explode("\n",file_get_contents($theFile));
		$outNewData='';
		$response['msgTitle']='<h4 class="c-yellow">actualizado</h4>';
		$response['msgText']='<center><span class="c-white f-16">proceso correcto</span></center>';
		$response['timeWait']=2000;

		foreach($fileDetails as $key=>$data)
		{
			$tmp=strpos($data, '#'); //  OJO: es la posicion del POUND

			if(strlen($tmp))
				$tmpStr=substr($data,1);
			else
				$tmpStr=$data;

			if(in_array($tmpStr, $fileDetailsTmp))
				$outNewData.=$charEnabled.$tmpStr."\n";
			else
				$outNewData.=$data."\n";
		}

		$outNewData=substr($outNewData,0,-1);
		$fp=@fopen($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/.htaccess','w');
		$resFile=fputs($fp, $outNewData);
		$resFile=fclose($fp);
//$resFile=0;
		$response['success']=$resFile ? true : false;

		if(!$response['success'])
		{
			$response['timeWait']=10000;
			$response['msgTitle']='<h2 class="c-white">ERROR al actualizar el archivo</h2>';
			$response['msgText']='<center><span class="c-white f-16">imposible actualizar el archivo HTACCESS en la carpeta del back-office</span><br />Contactar con informática.</center>';
//			$data->logBackOffice(0,null,'HTACCESS UPDATE FAILED - '.basename($arraExtVars['fileUrl']));
		}
	}

//echo "\nres=".$res;
	//$fp=@fopen($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/.htaccess','w+');
//	$xx=file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/');
//echo"\n==>".(file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/.htaccess'));die();

//echo"\n)->1:".$folderRootApp."\n->2:".$dirModAdmin;
//echo "\n htaccess_enabled: ".$arraExtVars['db'][$moduleSetting->mainTable]['htaccess_enabled'][0];
//echo "\n salida:\n".$outNewData;
//echo "\n";print_r($fileDetailsTmp);
//print_r($arraExtVars);
//die("\nFIN");
}


$res=json_encode($response);
die($res);