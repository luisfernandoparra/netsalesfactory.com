<?php
/**
 * MAIN MODULE - admin users
 * DEFAULT ACTION: list record data
 *
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
if($hashSecutityScript != __HASH_CONTROL_SCRIPT) die(__MSG_ACCESS_ERROR);

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$modeLoadConfig=1;
$folderCommonModules=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/';
$folderModule=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'];

$provenience=$_SERVER['HTTP_REFERER'];
$modNameFrom=strpos($provenience,'_modules/')+9;
$modName=substr($provenience, $modNameFrom);
$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));

@include('config_module.php');

if(!isset($moduleSetting))
{
  echo '<br /><br /><div class="p-20 m-20 f-16 alert alert-danger"><center><span class="zmdi zmdi-info f-20"> '.__COMMON_GENERICTEXT_ERROR.'</span><br />'.__COMMON_GENERICTEXT_ERROCONFIGMODULESTART.' <b>'.@$modName.'</b>.</center><br /><br />'.__COMMON_GENERICTEXT_ERROCONFIGMODULEEND.'.</div><br /><br />';
  $moduleSetting=array();
}
/**
 * RESTORE CONFIG FILES DISPLAY BLOCKS
 */
if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD )	// GOD ACCESS LEVEL
	include($folderCommonModules.'common_restore_modules.php');


/**
 * SERIALIZACION DE LOS PARAMETROS ESPECIFICOS
 * DEL MODULO ACTUAL NECESARIOS PARA EL IFRAME
 * DEL LISTADO DEL MISMO MODULO
 *
 * $outJsParamsModule = TODOS LOS PARAMETROS PARA SER SERIALIZADOS EN LA CARGA DEL IFRAME CORRESPONDIENTE
 */
$outJsParamsModule='';

foreach($moduleSetting as $field => $value)
{
	$valueJs='';

	if(is_array($value) && count($value) > 1)
	{
		if($field == 'arrFieldsFormModal' || $field == 'arrGoupsForm')	// SE OMITEN LOS ELEMENTOS INNECESARIOS
			continue;

		$valueJs='[';

		foreach($value as $arrValues)
		{
			if(!is_array($arrValues))
				$valueJs.=''.(str_replace(' ','_',$arrValues)).'="'.$arrValues.'",';
		}

		$valueJs=substr($valueJs,0,-1);
		$valueJs.=']';
 	}
	else
		@$valueJs='"'.$value.'"';

	$outJsParamsModule.=''.$field.':'.$valueJs.',';
}

$outJsParamsModule=substr($outJsParamsModule,0,-1);

?>


<script type="text/javascript">
<!--
// EDICION DE DATOS DEL REGISTRO SELECCIONADO
function openModalEditRecord(recordId, dataRow)	// SE CARGA EL SCRIPT CON EL ALTO MAXIMO DE LA CAJA "MODAL" A DIBUJAR PARA LA DIMENSION ACTUAL DE LA PANTALLA
{
	var additionalParams="";

	if(dataRow != undefined)
		$.each(dataRow[0].dataset,function(dataName, dataValue){
			additionalParams+="&"+dataName+"="+encodeURIComponent(dataValue);
		});

	var tmpHeight=$("body").prop("clientHeight");
	$("#modalWiderRefEdit").click();
  $("#ifrmEditRecord").attr("src","<?=$arraExtVars['moduleAdmin']?>ifr_edit.php?action=edit&ref_mod=<?=$hashSecutityScript?>&from=<?=$arraExtVars['moduleAdmin']?>&id="+recordId+"&module[name]=<?=$arraExtVars['modLabel']?>&module[modIcon]=<?=$moduleSetting->moduleIcon?>&editHeight="+tmpHeight+$.trim(additionalParams)+"").css("height",tmpHeight+"px");
	return true;
}

$(document).ready(function(){
	var tmpHeight=$("#mCSB_1").height()-140;	// ALTO MODAL DE INSERCION NUEVO REGISTRO

	// ACCION PARA ELIMINAR EL REGISTRO SELECCIONADO
	$(".actDelete").click(function(e){
		var dataSend={<?=@$outJsParamsModule?>};
		dataSend.idRef=$(this).attr("data-id");
		dataSend.action="deleteRecord";
		dataSend.module="<?=@$arraExtVars['moduleAdmin']?>";
		commonDeleteRecord(this, dataSend);
	});

	// BTN GUARDAR DEL FORMULARIO DE EDICION
	$(".saveButton").click(function(){
console.log("ACCION:: btn actualizar registro")
    document.getElementById("ifrmEditRecord").contentWindow.document.getElementById("tmpSubmit").click();
	});

	// BTN INSERTAR DEL FORMULARIO DE EDICION
	// EL COMPLETAMIENTO DE LA ACCION SE EFECTUA EN EL SUBMIT DEL FORMULARIO "moduleMainForm"
	$(".insertButton").click(function(){
console.log("ACCION:: btn insertar nuevo registro")
			document.getElementById("ifrmInsertRecord").contentWindow.document.getElementById("tmpSubmit").click()
	});


	// BTN ACTIVAR/DESACTIVAR REGISTRO EDITADO
	$(".enableDisableButton").click(function(){
//console.log("this.dataset.fieldName=",this.dataset)
		enableDisableRowId(this.dataset.id, "<?=$moduleSetting->mainTable?>", this.dataset.status, this.dataset.fieldName)
		$(this).hide();
		$(".actCancel ").click();
	});

  // AL FINALIZAR COMPLETAMENTE DE DIBUJAR EL MODULO, CARGA EL IFRAME DEL LISTADO CORRESPONDIENTE
  $(window).load(function(){
		var configDataList={};	// OBJETO CON LOS DATOS DE CONFIGURACION PARA LA BBDD DEL MODULO ACTUIAL
		var configDataList=serializeDataArray({<?=$outJsParamsModule?>}, "configDataList");
		$("#ifrListBlock").attr("src","ifr_common_listing.php?action=list&modulePath=<?=$arraExtVars['moduleAdmin']?>&"+configDataList).removeClass("hidden");
  });

	// NEW RECORD MODAL
	$(".modalNewRecord").click(function(){
		$("#modalWiderRefNewRecord").click();

		window.parent.$(".idTitleRef").html("<span style=font-style:italic;><div class='preloader pls-pink pl-xs'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div> <?=__COMMON_JSTITLE_TXTNEWRECORDLOADING?>...<span>");
		$("#ifrmInsertRecord").attr("src","<?=@$arraExtVars['moduleAdmin']?>ifr_edit.php?action=new&ref_mod=<?=@$hashSecutityScript?>&from=<?=@$arraExtVars['moduleAdmin']?>&module[name]=<?=@$arraExtVars['modLabel']?>&module[moment]=miValor2&editHeight="+(tmpHeight)+"").css("height",(tmpHeight)+"px");
		return true;
  });
});

-->
</script>

	<div class="card-header">
		<h2><?=__COMMON_GENERICTEXT_HEADERTITLETABULARDATA?> <b><?=$arraExtVars['modLabel']?></b>
			<?=@$moduleSetting->moduleCommenHeader?>
		</h2>
	</div>

<!-- START DATA LISTING -->
	<iframe style='height:30vh;' class="hidden" id='ifrListBlock' name='ifrListBlock' src='_blank.html' frameborder='0' marginheight='0' marginwidth='0' scrolling='NO' width='100%'></iframe>
<!-- END DATA LISTING -->

<!-- START HTML LOCAL MODULE/TAGS/ELEMENTS --->
<?php
// COMMON HTML CONTENTS
include($folderCommonModules.'common_html_contents_edit_form.php');

if($_SESSION['userAdmin']['roleId'] > 2 )	// MINIMUN ACCESS LEVEL
	@include($folderCommonModules.'common_html_contents_all.php');

if($_SESSION['userAdmin']['roleId'] > 7)	// INTERMEDIATE ACCESS LEVEL
	@include($folderCommonModules.'common_html_contents_role_8.php');

if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD )	// GOD ACCESS LEVEL
	@include($folderCommonModules.'common_html_contents_role_10.php');

if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/.htpasswd'))
{
	echo '<div class="p-20"><center><h1 class="c-white bgm-orange m-25 p-t-10 p-b-10">ATENCIÓN</h1></center><b>LEER ATENTAMENTE:</b><br>Antes de proteger el acceso al back-office, debe crear el archivo de contraseñas modificando/insertando la de un administrador de sistema.<br>Si no se efectúa antes esta operaci&oacute;n, es muy posible que <b>no pueda acceder más</b> a este administrador del sitio.<br><br>Recordar que si había ya contraseñas ANTES de la creación de este archivo, debe cambiarla por lo menos temporalmente para que esta se almacene correctamente la primera vez; sucesivamente podrá restablecerla a su conveniencia. </div>';
}
//echo $_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/.htpasswd';
//echo'<hr>'.file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/.htpasswd');

