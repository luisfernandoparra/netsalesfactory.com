<?php
/**
 * DATE UPDATE: 2017-10-18
 * LOCAL MODULE SYSTEM CONFIGURATION SETTINGS
 *
 *
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=__LEVEL_ACCESS_GOD;
$minRoleAdim=__LEVEL_ACCESS_GOD;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>Configuraci&oacute;n b&aacute;sica de sistema para el back-office</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='system_admin2016';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='';
$moduleSetting->keyListLabel='id';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD ? true : false;
$moduleSetting->isCanEnabled=false;
$moduleSetting->fieldNameStatusRow='';
$moduleSetting->allowNewRecord=false;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->minEnableLevel=__LEVEL_ACCESS_GOD;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[1]'; // EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;

/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array()
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode($arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS

$order=0;
/**
 * LOS CAMPOS DEBEN APARECER POR EL ORDEN AQUI ABAJO
 * DUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id'=>array(
    'formType'=>'hidden'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>0
    ,'label'=>'ID'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'htaccess_enabled'=>array(
	'formType'=>'checkbox'
	,'dataType'=>'toggle'
	,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>2]
	  ,'toggleParams'=>array('tsColor'=>'green')
    ,'arrValues'=>array(0=>'<span class=c-red>no</span>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')
    ,'required'=>0
    ,'label'=>'Proteger acceso al back-office'
    ,'helpText'=>'Proteger el acceso al back-office'
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'forced_admin_lang'=>array(
	  'formType'=>'select'
	  ,'dataType'=>'text'
    ,'tabularListData'=>['columnOrder'=>3]
	  ,'required'=>1
	  ,'label'=>'Lingua'
	  ,'fieldTableName'=>__QUERY_TABLES_PREFIX.'languages'  // NOMBRE DE LA TABLA ORIGEN A UTILIZAR PARA LA EXTRACCION DE DATOS PARA OBTENER LOS VALORES DE UN CAMPO "SELECT"
	  ,'arrFieldsTagSelect'=>array('id','iso3')	// PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
 	  ,'defaultSelectText'=>'Seleccionar'
	  ,'orderSelectField'=>'iso3'
	  ,'whereSelectField'=>'b_enabled'
    ,'label'=>'Idioma por defecto'
    ,'helpText'=>''
    ,'tagPattern'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
//  ,'img_logo'=>array(
//    'formType'=>'file'
//    ,'dataType'=>'imageFile'  // allFile / imageFile
//    ,'required'=>false
//    ,'label'=>'Logo admin'
//    ,'helpText'=>''
//    ,'visible'=>true
//    ,'placeHolder'=>''
//    ,'htmlFolder'=>$web_url.$adminFolder.'/profile_images/'
//    ,'targetFolder'=>$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/profile_images/'
//    ,'formGroup'=>2
//    ,'excludeAutoForm'=>false
//    ,'order'=>++$order
//  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  1=>array(
    'groupTextTitle'=>'Par&aacute;metros b&aacute;sicos de sistema para el back-office'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);

