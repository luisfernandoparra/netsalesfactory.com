<?php
/**
 *	CLASE CON LAS METODOS DE BBDD NECESARIOS PARA EL MODULO CARGADO
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class ModelLocalModule extends CommonEditingModel
{
	public $query;
	public $commonDb;
	public $arrFieldsTagSelect;

	public function __construct($db_type=null, $db_host='', $db_user='', $db_pass='', $db_name='', $localParams=null, $port='3306')
	{
		$this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
		$this->prefixGtp=__QUERY_TABLES_PREFIX;
		$this->query='';
		$this->mainTable=$this->prefixGtp.$localParams->mainTable;
		$this->listFieldsOutput=''; $this->mainTableFieldsNames=''; $this->defaultWhereSql='';
		$this->defaultOrderField='';
		return;
	}

  public function __destruct(){}
}