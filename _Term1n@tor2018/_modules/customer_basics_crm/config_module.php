<?php
/**
 * DATE UPDATE: 2016-11-04 07:46 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * CUSTOMER BASICS CONFIG
 * 
 * IMPORTANTE, EL PARAMETRO orderTabularList PRESENTE EN LOS CAMPOS
 * INDICA LA POSICION Y DONDE DEBE APARECER EN EL LISTADO TABULAR DEL MODULO
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$moduleSetting=new stdClass();

// MANDATORY PARAMS
$moduleSetting->moduleBBDD=null;
$moduleSetting->mainTable='landings_customers';
//$moduleSetting->foreignKeys='id,id_lang';	// IMPORTANTE, SIN ESPACIOS
$moduleSetting->keyListLabel='id';
$moduleSetting->orderField='prefix';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->fieldNameStatusRow='is_enabled';
$moduleSetting->isCanEnabled=true;
$moduleSetting->allowNewRecord=true;


// OPTIONAL PARAMS
$moduleSetting->moduleCommenHeader='<small>Categor&iacute;as disponibles para todos los sorteos</small>';
$moduleSetting->numRecDisplay='[10,30,50]'; // EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;

$order=0;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id'=>array(
    'formType'=>'hidden'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>0
    ,'label'=>''
    ,'helpText'=>''
    ,'visible'=>0
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'prefix'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>2]
    ,'required'=>1
    ,'label'=>'Nombre cliente landing'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
	,'id_landing'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>3,'columnWidth'=>3]
		,'tabularFilter'=>['mode'=>'BBDD','origin'=>__QUERY_TABLES_PREFIX.'landings_customers','tag'=>array('filterType'=>'select')]
		,'required'=>1
		,'fieldTableName'=>'clientes'
		,'arrFieldsTagSelect'=>array('id_cliente','cliente') // PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
		,'includeMainBBDD'=>true
		,'whereSelectField'=>null
		,'orderSelectField'=>'cliente'
    ,'defaultSelectText'=>'Nombre cliente CRM'
    ,'label'=>' Cliente'
    ,'helpText'=>'Cliente al que pertenece la landing (CRM)'
    ,'visible'=>1
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
	)
  ,'is_enabled'=>array(
		'formType'=>'checkbox'
		,'dataType'=>'toggle'
		,'tabularListData'=>['columnOrder'=>4]	// PARA QUE NO VISUALICE EN EL LISTADO, PONER EL MISMO NUMERO DEL ULTIMO UTILIZADO
		,'required'=>0
		,'toggleParams'=>array('tsColor'=>'green')
		,'label'=>'Estado'
		,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')	// PARA ESTE CONTROL, SIEMPRE EL PRIMER VALOR ES EL NEGATIVO Y EL SEGUNDO EL POSITIVO
		,'helpText'=>'Habilitar / deshabilitar'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>4
		,'excludeAutoForm'=>false
		,'order'=>++$order
  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  1=>array(
    'groupTextTitle'=>'Clientes landings'
    ,'additionalInfo'=>'Todos los clientes de las landings'
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);
