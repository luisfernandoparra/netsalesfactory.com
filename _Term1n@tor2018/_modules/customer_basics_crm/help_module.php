<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">

		<ul class="tab-nav text-center fw-nav">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Intro</a></li>
			<!--<li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">otro TAB</a></li>-->
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>Breve introducción para el módulo actual.</p>
					<ol>
						<li class="m-b-10">Asignación inicial de los clientes disponibles para las landings según establecido el la tabla fuente del <b>CRM "clientes"</b></li>
					</ol>
				</div>
				<div class="tab-pane fade f-16" id="tab2">
						<p>El archivo "config_module.php" contiene la gran parte de parámetros necesarios para el funcionamiento de los módulos del back-office.</p>
						<p>Debemos recordar que este archivo contiene también las definiciones para los campos a utilizar en el listado principal y en la edición del mismo, esto para todos los módulos con estructuras simples.</p>
				</div>

<!-- PAGINACION SI NECESARIA
				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>
-->
			</div>

	</div>
</div>
