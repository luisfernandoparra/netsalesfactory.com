<?php
/**
 * HTML COMMON MODULE/TAGS/ELEMENTS
 *
 * OF ALL ADMIN MODULES
 * WIDTH ACCESS ADMIN LEVEL >= __LEVEL_ACCESS_VISITOR
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

// DISPLAY SPECIFIC BUTTON IF HELP FILE EXIST
$displayBtnHelpModule=file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'help_module.php') ? 1 : 0;
?>
<a id="modalWiderRefEdit" data-toggle="modal" href="#editRowModal" class="hide" title="<?=__COMMON_LISTBTNTITLE_EDITRECORD?>" >editRow</a>
<a id="modalWiderRefDelete" data-toggle="modal" href="#modaldeleteRow" class="hide" title="<?=__COMMON_LISTBTNTITLE_DELETERECORD?>" >deleteRow</a>
<a id="modalWiderRefNewRecord" data-toggle="modal" href="#modalNewRecord" class="hide" title="<?=__COMMON_LISTBTNTITLE_NEWRECORD?>" >newRec</a>

<div class="modal fade" id="modalNewRecord" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-record-insert">
			<div class="modal-content">

				<div class="modal-header <?=$_SESSION['userAdmin']['pref_']['top_header_theme_bckCol']?>">
					<div class="fltBtnClose">
						<button class='btn btn-icon bg-black-trp <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect waves-circle actCancel' data-dismiss='modal'><i class='zmdi zmdi-close <?=$foreAutoColor?>'></i></button>
					</div>
					<h4 class="modal-title <?=$foreAutoColor?>" style='width:96%!important;'><span class="idTitleRef"></span><span class="headerAdditionalInfo hide <?=$foreAutoColor?>"></span></h4>
				</div>

				<div class="modal-body" id="boxContentNewRecord" style="overflow:hidden">
					<iframe style='width:100%!important;' id='ifrmInsertRecord' name='ifrmInsertRecord' src='#null' frameborder='0' marginheight='0' marginwidth='0' scrolling='auto' width='100%' height='100%'></iframe>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect helpUserModule" style="display:<?=$displayBtnHelpModule ? 'inline' : 'none'?>;">?</button>
					<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> insertButton"><?=__COMMON_BTN_INSERT?></button>
					<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> actCancel" data-dismiss="modal"><?=__COMMON_BTN_CANCEL?></button>
				</div>

			</div>
		</div>
</div>

<div class="modal fade" id="editRowModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-record-edit brd-10">
				<div class="modal-content">
					<div class="modal-header <?=$_SESSION['userAdmin']['pref_']['top_header_theme_bckCol']?>">
						<h4 class="modal-title <?=$foreAutoColor?>" style='width:96%!important;'><span class="idTitleRef"></span><span class="headerAdditionalInfo hide  <?=$foreAutoColor?>"></span></h4>
            <div class="fltBtnClose">
              <button class='btn btn-icon bg-black-trp <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect waves-circle actCancel' data-dismiss='modal'><i class='zmdi zmdi-close <?=$foreAutoColor?>'></i></button>
            </div>
					</div>

					<div class="modal-body" id="boxContentEditRecord" style="overflow:hidden">
            <iframe style='width:100%!important;' id='ifrmEditRecord' name='ifrmEditRecord' src='#null' frameborder='0' marginheight='0' marginwidth='0' scrolling='auto' width='100%' height='100%'></iframe>
          </div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect helpUserModule" style="display:<?=$displayBtnHelpModule ? 'inline' : 'none'?>;">?</button>
						<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect enableDisableButton" data-id="0" data-field-name="" data-status="0" style="display:none;">...</button>
						<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> saveButton" style="display:none;"><?=__COMMON_BTN_SAVE?></button>
						<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> actCancel" data-dismiss="modal"><?=__COMMON_BTN_CANCEL?></button>
					</div>
				</div>
		</div>
</div>

<div class="modal fade" data-modal-color="red" id="modaldeleteRow" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
					<h4 class="modal-title"><?=__COMMON_MODALMSG_CONFIRMDELETEH4?></h4>
			</div>
			<div class="modal-body">
					<p><?=__COMMON_MODALMSG_CONFIRMDELETETEXT?> id: <span class="idTitleRef"></span></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect actDelete" data-id="0"><?=__COMMON_BTN_DELETE?></button>
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect actAbortDelete" data-dismiss="modal"><?=__COMMON_BTN_CANCEL?></button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" data-modal-color="cyan" id="modalCopyRow" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
					<h4 class="modal-title"><?=__COMMON_JSBOOTGRID_MODULESETTINGCOPYRECORDSTART?> :: <i><?=$arraExtVars['modName']?></i></h4>
			</div>
			<div class="modal-body">
					<p><?=__COMMON_MODALMSG_CONFIRMCOPYRECORDTEXT?>: <span class="idTitleRef"></span></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect actCopyRecord" data-id="0" data-module=""><?=__COMMON_BTN_COPYRECORD?></button>
				<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect" data-dismiss="modal"><?=__COMMON_BTN_CANCEL?></button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modalUserModuleHelp" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="true">
	<div class="modal-dialog brd-10 m-r-30 w-50 o-auto bs-item z-depth-4-top m-t-20" style="max-height:95vh;">
				<div class="modal-content">
					<div class="modal-header <?=$_SESSION['userAdmin']['pref_']['top_header_theme_bckCol']?>" style="opacity:.95;">
						<h4 class="modal-title w-100 <?=$foreAutoColor?>" style='width:90%!important;'>
							<span class=""><?=__COMMON_MODALMSG_MODALUSERMODULEHELPH4?> </span>
							<span class="text-uppercase <?=$foreAutoColor?>"><b><?=$arraExtVars['modName']?></b></span>
						</h4>
            <div class="fltBtnClose">
              <button class='btn btn-icon bg-black-trp <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect waves-circle' onclick="$('#modalUserModuleHelp').modal('toggle')" data-dismiss="modal" ><i class='zmdi zmdi-close <?=$foreAutoColor?>'></i></button>
            </div>
					</div>
					<div class="modal-body o-auto" id="boxContentEditRecord" style="">
<?php
if($displayBtnHelpModule)
{
	include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'help_module.php');
	echo'<script>$(".fw-footer").hide();</script>';
}
?>
          </div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> " onclick="$('#modalUserModuleHelp').modal('toggle')"><?=__COMMON_BTN_CLOSE?></button>
					</div>
				</div>
		</div>
</div>
