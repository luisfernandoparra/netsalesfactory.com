<?php
/**
 * DATE UPDATE: 2018-03-20 09:48 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * LANDING-CONTENTS
 * 
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=$_SESSION['userAdmin']['roleId'];  // AJUSTA AUTOMATICAMENTE EL MAXIMO ROL PARA EL USUARIO ACCEDIDO
$minRoleAdim=1;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>Contenidos HTML de las landings</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='landings_data_contents';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='id';
$moduleSetting->keyListLabel='id';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->isCanEnabled=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole ? true : false;
$moduleSetting->fieldNameStatusRow='is_enabled';
//$moduleSetting->denyOtherUsers=[$_SESSION['userAdmin']['roleId'] => 'roleId'];
$moduleSetting->allowNewRecord=true;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->minEnableLevel=__LEVEL_ACCESS_MASTER;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[10,25,50]'; // array, EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;
$moduleSetting->defaultOrderField=['id'=>'DESC'];
$moduleSetting->allowCopyRecord=true;	// NEW 2018
$moduleSetting->minCopyRecordLevel=__LEVEL_ACCESS_MASTER;	// NEW 2018
$moduleSetting->arrCopyExcludeFields = array('site_landing_id');

/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array(
//		' && '.$moduleSetting->fieldNameStatusRow.' = '=>0
//		,'&& roleId <= '=>$_SESSION['userAdmin']['roleId']
// EXAMPLE WITH MORE SENTENCES:
//		,'|| ('=>''
//		,'theme = '=>'\'123\''
//		,')'=>''
	)
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode( $arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS

$arrScriptFormName=array(
  0=>'Standard',
  1=>'Espec&iacute;fico',
);

//echo $moduleSetting->moduleIcon.'<pre>';print_r($arrEraseOptions);echo'</pre>';
$order=0;

//echo $web_url.$adminFolder;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id'=>array(
    'formType'=>'text'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>1
    ,'label'=>'ref.'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>true
    ,'order'=>++$order
  )
	,'site_landing_id'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>3]
		,'tabularFilter'=>['mode'=>'BBDD','origin'=>__QUERY_TABLES_PREFIX.'landings_site_config','tag'=>array('filterType'=>'select')]
		,'required'=>1
		,'fieldTableName'=>__QUERY_TABLES_PREFIX.'landings_site_config'
		,'arrFieldsTagSelect'=>array('id','CONCAT("<span style=color:red;>",id,"</span> - ",landing_name)') // PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
		,'includeMainBBDD'=>true
		, 'whereSelectField'=> (isset($_REQUEST['id'])) ? null : ' id NOT IN (SELECT site_landing_id FROM '.$moduleSetting->prefixTbl.$moduleSetting->mainTable.')'
		,'orderSelectField'=>'id'
    ,'label'=>' ID - Nombre landing'
    ,'helpText'=>'Id + Referecia de la landing'
    ,'visible'=>1
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
	)
	,'script_form_name'=>array(
    'formType'=>'radio'
    ,'dataType'=>'integer'
		,'tabularListData'=>['columnOrder'=>3,'columnWidth'=>2]
    ,'required'=>0
    ,'label'=>'Tipo formulario registro'
    ,'arrValues'=>$arrScriptFormName
		,'arrValuesRadioTag'=>$arrScriptFormName
    ,'visible'=>1
    ,'selectSearchBox'=>false
    ,'multipleSelect'=>false
    ,'placeHolder'=>''
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'helpText'=>'Archivo a utilizar para los campos del formulario'
//    ,'helpTextExtend'=>'Si se utiliza la opci&oacute;n `Espec&iacute;fico`, se incluir&aacute; el script terminado con el ID de la landing actual (ej.: form_script_001), si no `form_script_common.php`'
    ,'helpTextExtend'=>'Con la opci&oacute;n Espec&iacute;fico, en esta landing se incluir&aacute; el archivo form_script_'.(isset($_REQUEST['id']) && $_REQUEST['id'] ? str_pad($_REQUEST['id'],3,'0',STR_PAD_LEFT) : '+ ID').', con la opci&oacute;n Standard ser&aacute; `form_script_common.php`'
//    ,'helpTextExtend'=>'Si se utiliza la opci&oacute;n `Espec&iacute;fico`, se incluir&aacute; el script terminado con el ID de la landing actual (form_data_'.(isset($_REQUEST['id']) && $_REQUEST['id'] ? str_pad($_REQUEST['id'],3,'0',STR_PAD_LEFT) : 'XXX').'), si no `form_script_common.php`'
    ,'order'=>++$order
	)
	,'mobile_auto_modal'=>array(
    'formType'=>'checkbox'
    ,'dataType'=>'toggle'
    ,'tabularListData'=>['columnOrder'=>4,'columnWidth'=>1]
    ,'required'=>0
    ,'toggleParams'=>array('tsColor'=>'green')
    ,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')
    ,'label'=>'Auto-modal'
    ,'helpText'=>'Especificar modo (solo si procede)'
		,'helpTextExtend'=>'Se debe cargar la modal autom&aacute;ticamente al abrir la landing? (sin seleccionar=NO/seleccionado=SI)'
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>0
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
	)
  ,'header_top_content'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Contenido cabecera landing'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>6
    ,'helpText'=>'HTML del bloque superior landing'
    ,'visible'=>1
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'box_top_left'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Contenido HTML cuerpo landing'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>6
    ,'helpText'=>'Bloque del contenido central'
		,'helpTextExtend'=>'Se excluye el formulario'
    ,'visible'=>1
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'box_bottom_form'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Contenido HTML inferior'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>6
    ,'helpText'=>''
		,'helpTextExtend'=>'Este bloque aparece despu&eacute;s del bloque central de la landing'
    ,'visible'=>1
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'box_footer_text'=>array(
    'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Contenido del footer'
    ,'rowsData'=>4
		,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'colsClass'=>6
    ,'helpText'=>'HTML del footer de la landing'
		,'helpTextExtend'=>''
    ,'visible'=>1
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
	,'is_enabled'=>array(
    'formType'=>'checkbox'
    ,'dataType'=>'toggle'
    ,'tabularListData'=>['columnOrder'=>5,'columnWidth'=>1]
    ,'required'=>0
    ,'toggleParams'=>array('tsColor'=>'green')
    ,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')
    ,'label'=>'Estado'
    ,'helpText'=>'Habilitar / deshabilitar este registro'
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
	)
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  0=>array(
    'groupTextTitle'=>'Configuraciones de la landing'
    ,'additionalInfo'=>'Par&aacute;metros imprescindibles'
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
	,1=>array(
    'groupTextTitle'=>'Contenidos HTML de la landing'
    ,'additionalInfo'=>'HTML que aparece en la p&aacute;gina'
    ,'groupIco'=>'fa fa-vcard-o'
    ,'groupHeaderClass'=>'bg-black-trp'
  )
//	,2=>array(
//    'groupTextTitle'=>'Sistema'
//    ,'additionalInfo'=>''
//    ,'groupIco'=>'fa fa-cog'
//    ,'groupHeaderClass'=>'bg-black-trp'
//  )
);
