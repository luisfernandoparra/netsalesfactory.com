<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">

		<ul class="tab-nav text-center fw-nav">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Principales funciones</a></li>
			<li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">Notas</a></li>
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>Desglose de las principales funciones del módulo actual.</p>
					<p>En este módulo se atribuyen los principales contenidos HTML para la landing actual. Notar que todas las landings, de forma standard, están preparadas para contener cuatro posiciones de código HTML: <i>bloque superior</i><sup>(1)</sup>, <i>bloque medio</i> (habitualmente a la izquierda del formulario de registro)<sup>(2)</sup>, <i>bloque inferior</i> (debajo del formulario<sup>(3)</sup> y el &uacute;ltimo como <i>bloque footer</i><sup>(4)</sup> (de la misma página).</p>
					<ol>
						<li class="m-b-10">Lo primero es seleccionar el <b>ID / nombre landing</b> para referenciar los contenidos que se definen en este módulo. Recordar que el ID del record de esta tabla (aquí identificado como <i>ref.</i>) no es el ID de la landing, por ese motivo se ha incluido junto al <i>nombre</i> el valor del <i>ID</i> que si referencia específicamente la landing editada.</li>
						<li class="m-b-10">En la selección del <b>Tipo formulario registro</b> se determina como se incluye el bloque de los campos del formulario por medio del archivo específico que puede ser el <b>`form_script_common.php`</b> predefinido para todas las landings de un mismo cliente o `<b>form_script_<i>XXX</i>.php</b>`, donde <b><i>XXX</i></b> es el ID de la misma landing, completo de ceros a la izquierda si fuera necesario para números inferiores a 100.</li>
						<li class="m-b-10">Con el selector <b>Auto-modal</b> se define si se debe cargar automáticamente una <i>modal</i> al abrir la landing (esto no se utiliza ya desde hace tiempo).</li>
						<li class="m-b-10">Bloque 1: <b>Contenido cabecera landing</b>, generalmente con el logo de la landing y un titular.</li>
						<li class="m-b-10">Bloque 2: <b>Contenido HTML cuerpo landing</b>, usualmente con textos e imágenes describiendo la oferta.</li>
						<li class="m-b-10">Bloque 3: <b>Contenido HTML inferior</b>, con textos o elementos HTML activos para completar la información necesaria a presentar.</li>
						<li class="m-b-10">Bloque 4: <b>Contenido del footer</b>, por lo general con una barra de links a textos legales de la landing.</li>
						<li class="m-b-10">El campo <b>Estado</b> define si se deben visualizar los contenidos definidos en este nmódulo.</li>
					</ol>
				</div>
				<div class="tab-pane fade f-16" id="tab2">
					<p class="c-bluegray">Es importante recordar que las URL`s que se deban definir en estos contenidos, si hacen referencia a la misma landing, generalmente deben ser construidas a partir del dominio donde se aloja la misma; esta información en algunas ocasiones suele llegar a posteriori de la creación de la landing, por lo que hay que prestar atención cuando se active en real.</p>
						<p>En 2018, para aplicar las nuevas normativas de la LOPD, se han incluído unas clases CSS específicas que sirven para abrir automáticamente las <i>ventanas modales</i> que visualizarán los textos apropiados:
						<ol>
							<li>La clase CSS <b>legal_notices</b> sirve para abrir el contenido definido en el módulo de <i>Config. básica landings</i> con el campo <i>Aviso legal</i>.</li>
							<li><b>link_pp</b> identifica la modal que deberá cargar las <i>políticas de privacidad</i> de la landing, definidas siempre en <i>Config. básica landings</i> con el campo <i>Política de privacidad</i>.</li>
							<li><b>modal_atd</b> es el nombre de clase CSS para cargar la modal que con los textos para los <i>envío de datos a terceros</i> en la landing, definidas en <i>Config. básica landings</i> con el campo <i>Consentimiento envío datos</i>.</li>
							<li><b>cookie_info</b> para la clase CSS que debe cargar la modal para los <i>contenidos definidos para las cookies</i>, contenido que se edita siempre en <i>Config. básica landings</i> con el campo <i>Texto legal cookies</i>.</li>
						</ol>
						Muy importante saber que para que funcione correctamente la apertura de la ventana modal en la misma landing, los contenidos del módulo <i>"Config. básica landings"</i>, para los campos <i>"Aviso legal", "Consentimiento envío datos"</i>&nbsp;&nbsp;y/o <i>"Política de privacidad"</i> deben tener una longitud superior a 100 caracteres y en el parámetro <i>href</i> del TAG HTML del enlace, se debería escribir "#null".
						</p>
						<p>Así mismo, se puede colocar en cualquier lugar del contenido HTML el enlace con la clase CSS <b>"cookie_info"</b> para abrir la modal con los contenidos definidos para las cookies.</p>
				</div>

				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>

			</div>

	</div>
</div>
