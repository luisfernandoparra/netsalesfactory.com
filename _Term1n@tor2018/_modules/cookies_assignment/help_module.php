<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">

		<ul class="tab-nav text-center fw-nav">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Intro</a></li>
			<!--<li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">otro TAB</a></li>-->
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>Breve introducción para el módulo de configuraión de dominios.</p>
					<ol>
            <li class="m-b-10">El campo <strong>"Empresa"</strong>, especificar la empresa de la cookie</li>
						<li class="m-b-10">El campo <strong>"Nombre cookie"</strong>, es el nombre técnico de la cookie.</li>
            <li class="m-b-10">El campo <strong>"Dominio"</strong>, se debe especificar la url del dominio sin incluir "http" o "https"</li>
						<li class="m-b-10">El campo <strong>"Descripción"</strong>, breve descripción que aparece en el recuadro de las cookies para el público.</li>
						<li class="m-b-10">El campo <strong>"Duración"</strong>, tiempo de vida de la cookie.</li>
						<li class="m-b-10">El campo <strong>"Información extra"</strong>, descripción/información detallada que aparece en el recuadro de las cookies para el público.</li>

						<li class="m-b-10">El campo <strong>"Es de Terceros?"</strong>, hay dos grupos de cookies, la interna de la empresa<i>(NO)</i>, y la denominada de terceros, reservada para empresas externas a Netsales <i>(SI)</i>.</li>
						<li class="m-b-10">El campo <strong>"Estado"</strong>, habilita/deshabilita el registro.</li>
					</ol>
				</div>
				<div class="tab-pane fade f-16" id="tab2">
						<p>El archivo "config_module.php" contiene la gran parte de parámetros necesarios para el funcionamiento de los módulos del back-office.</p>
						<p>Debemos recordar que este archivo contiene también las definiciones para los campos a utilizar en el listado principal y en la edición del mismo, esto para todos los módulos con estructuras simples.</p>
				</div>
			</div>

	</div>
</div>
