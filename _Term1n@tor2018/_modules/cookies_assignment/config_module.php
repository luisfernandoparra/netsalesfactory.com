<?php
/**
 * DATE UPDATE: 2018-03-20 09:28 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * ADMIN USERS
 * 
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=$_SESSION['userAdmin']['roleId'];  // AJUSTA AUTOMATICAMENTE EL MAXIMO ROL PARA EL USUARIO ACCEDIDO
$minRoleAdim=1;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>Definici&oacute;n de coookies disponibles para las landings</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='cookies_contents';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='cookie_name';
$moduleSetting->keyListLabel='id_cookie';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_MASTER ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->isCanEnabled=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole ? true : false;
$moduleSetting->fieldNameStatusRow='b_enabled';
//$moduleSetting->denyOtherUsers=[$_SESSION['userAdmin']['roleId'] => 'roleId'];
$moduleSetting->allowNewRecord=true;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_GOD;
$moduleSetting->minEnableLevel=__LEVEL_ACCESS_MASTER;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[10,25,50]'; // array, EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;
$moduleSetting->allowCopyRecord=true;
$moduleSetting->minCopyRecordLevel=__LEVEL_ACCESS_MASTER;
$moduleSetting->arrCopyExcludeFields=array('business_name');

/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array(
		' && !'.$moduleSetting->fieldNameStatusRow=>''
		,' && !(SELECT COUNT(*) FROM `'.__QUERY_TABLES_PREFIX.'cookies_landings` WHERE id_cookie = '=>'[[idRef]])'	// EVITAR BORRAR REGISTROS EN TABLAS RELACIONALAS
	)
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode( $arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS

//echo $moduleSetting->moduleIcon.'<pre>';print_r($arrEraseOptions);echo'</pre>';
$order=0;

//echo $web_url.$adminFolder;
/**
 * IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
 * LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
 * Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
 * PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
 */
$moduleSetting->arrFieldsFormModal=array(
  'id_cookie'=>array(
    'formType'=>'text'
    ,'dataType'=>'integer'
    ,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
    ,'required'=>1
    ,'label'=>'Id'
    ,'helpText'=>''
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>true
    ,'order'=>++$order
  )
  ,'business_name'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>2,'columnWidth'=>1]
    ,'required'=>1
    ,'label'=>'Empresa'
    ,'helpText'=>'Nombre de empresa'
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'cookie_name'=>array(
     'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>3,'columnWidth'=>2]
    ,'required'=>1
    ,'label'=>'Nombre cookie'
    ,'helpText'=>'Nombre de cookie'
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'cookie_domain'=>array(
     'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>4,'columnWidth'=>2]
    ,'required'=>1
    ,'label'=>'Dominio'
    ,'helpText'=>'Nombre del dominio'
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'description'=>array(
    'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'tabularListData'=>['columnOrder'=>5,'columnWidth'=>3]
    ,'required'=>1
    ,'label'=>'Descripci&oacute;n'
    ,'helpText'=>'Descripci&oacute;n de la cookie'
    ,'visible'=>1
    ,'placeHolder'=>'Descripci&oacute;n'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'live_time'=>array(
     'formType'=>'text'
    ,'dataType'=>'varchar'
    ,'required'=>1
    ,'label'=>'Duraci&oacute;n'
    ,'helpText'=>'Duraci&oacute;n de la cookie en a&ntilde;os'
    ,'visible'=>1
    ,'placeHolder'=>''
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,'extra_info'=>array(
     'formType'=>'textarea'
    ,'dataType'=>'varchar'
    ,'required'=>0
    ,'label'=>'Informaci&oacute;n extra'
    ,'helpText'=>''
    ,'visible'=>1
    ,'actionsForm'=>(object)['isHtmlBtn'=>true,'isWysiBtn'=>true,'wysiwigEditor'=>['modal'=>true,'script'=>'test','toolBar'=>'default_toolbar']]
    ,'rowsData'=>4
    ,'placeHolder'=>'Informaci&oacute;n extra'
    ,'formGroup'=>1
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )

  ,'is_third_partie'=>array(
    'formType'=>'checkbox'
    ,'dataType'=>'toggle'
    ,'tabularListData'=>['columnOrder'=>6,'columnWidth'=>1]
    ,'tabularFilter'=>['mode'=>'STATIC','tag'=>array('filterType'=>'radio'),'dataFields'=>array('keyOptions'=>'https_enabled','textOptions'=>'descripcion','keyDefault'=>'0'),'dataValues'=>array(0=>'NO',1=>'SI')]
    ,'required'=>0
    ,'toggleParams'=>array('tsColor'=>'green')
    ,'arrValues'=>array(0=>'propia',1=>'<i class="c-green f-700">terceros</i>')
    ,'label'=>'Es de Terceros?'
    ,'helpText'=>'Marcar si es de terceros'
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
  ,$moduleSetting->fieldNameStatusRow=>array(
    'formType'=>'checkbox'
    ,'dataType'=>'toggle'
    ,'tabularListData'=>['columnOrder'=>6]
    ,'tabularFilter'=>['mode'=>'STATIC','tag'=>array('filterType'=>'radio'),'dataFields'=>array('keyOptions'=>'https_enabled','textOptions'=>'descripcion','keyDefault'=>'0'),'dataValues'=>array(0=>'NO',1=>'SI')]
    ,'required'=>0
    ,'toggleParams'=>array('tsColor'=>'green')
    ,'arrValues'=>array(0=>'<i class="zmdi zmdi-minus f-20 c-red f-700"></i>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')
    ,'label'=>'Estado'
    ,'helpText'=>'Activar / Desactivar'
    ,'visible'=>1
    ,'placeHolder'=>null
    ,'formGroup'=>2
    ,'excludeAutoForm'=>false
    ,'order'=>++$order
  )
);

//	HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
  1=>array(
    'groupTextTitle'=>'Caracteristicas de cookies'
    ,'additionalInfo'=>'Este m&oacute;dulo proporciona los datos necesarios para alimentar las especificaciones de cada cookie asociada a un dominio en la tabla del texto de politica de cookies'
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  ),
  2=>array(
    'groupTextTitle'=>'Otros par&aacute;metros'
    ,'additionalInfo'=>''
    ,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
    ,'groupHeaderClass'=>'bg-black-trp'
  )
);
