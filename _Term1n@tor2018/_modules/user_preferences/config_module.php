<?php
/**
 * DATE UPDATE: 2018-03-16 08:07 (Mario Francescutto) - updated from admin editor
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * ADMIN Configuraciones personales para los administradores
 * 
*/
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$maxRoleAdim=$_SESSION['userAdmin']['roleId'];  // AJUSTA AUTOMATICAMENTE EL MAXIMO ROL PARA EL USUARIO ACCEDIDO
$minRoleAdim=1;

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>comentarios espec&iacute;ficos del m&oacute;dulo actual....</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='adminusers_preferences';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='user_id';
$moduleSetting->keyListLabel='id';
$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD ? true : false;
$moduleSetting->isEditable=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEditRole ? true : false;
$moduleSetting->isCanEnabled=$_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole ? true : false;
$moduleSetting->fieldNameStatusRow='b_enabled';
$moduleSetting->allowNewRecord=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD ? true : false;
$moduleSetting->minEditLevel=__LEVEL_ACCESS_OPERATOR;
$moduleSetting->minEnableLevel=$_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD ? $_SESSION['userAdmin']['roleId'] : 1000;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay='[10,25,50]'; // EJ.: [5,10,20]
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;
$moduleSetting->allowCopyRecord=true;	// NEW 2018
$moduleSetting->minCopyRecordLevel=__LEVEL_ACCESS_MASTER;	// NEW 2018


/**
 * START RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS
 *
 * EL ARRAY $arrEraseOptions CONLLEVA TODAS LAS CONDICIONES ADICIONALES
 * PARA UTILIZAR EN LA ELIMINACION DE UN REGISTRO DEL MODULO ACTUAL
 */
$arrEraseOptions=array(
	'sqlWhere'=>array(
		' && !'.$moduleSetting->fieldNameStatusRow => ''
	)
);
$moduleSetting->eraseOptions=str_replace('"','\"',json_encode($arrEraseOptions,JSON_HEX_QUOT));
//	END RESTRICCIONES SQL PARA LA ELIMINACION DE REGISTROS



$order=0;

/**
* IMPORTANTE: MIENTRAS NO SE OBTENGAN LOS PARAMETROS DE arrFieldsFormModal DESDE BBDD
* LOS CAMPOS DEBEN APARECERAN POR EL ORDEN AQUI ABAJO
* Y OUEDEN SER AGRUPADOS POR EL PARAMETRO formGroup
* PARA PERMITIR COLOCARLOS CORRECTAMENTE EN EL FORMULARIO
*/
$moduleSetting->arrFieldsFormModal=array(
	'id'=>array(
		'formType'=>'hidden'
		,'dataType'=>'integer'
		,'tabularListData'=>['columnOrder'=>1,'columnWidth'=>1]
		,'required'=>0
		,'label'=>'ID'
		,'helpText'=>''
		,'visible'=>1
		,'placeHolder'=>''
		,'formGroup'=>0
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'user_id'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>2]
		,'uniqueRecord'=>1  // NO PUEDE EXISTIR OTRO REGISTRO CON EL MISMO ID DE USUARIO
		,'tabularFilter'=>['mode'=>'BBDD','origin'=>'self','tag'=>array('filterType'=>'select')]
		,'required'=>1
		,'label'=>' Usuario'
		,'isDisabled'=>($_SESSION['userAdmin']['roleId'] < __LEVEL_ACCESS_GOD ? true : false)
		,'selectSearchBox'=>($_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD ? true : false)
		,'fieldTableName'=>__QUERY_TABLES_PREFIX.'adminusers'  // NOMBRE DE LA TABLA ORIGEN A UTILIZAR PARA LA EXTRACCION DE DATOS PARA OBTENER LOS VALORES DEL CAMPO "SELECT"
		,'arrFieldsTagSelect'=>array('userId','user') // PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
		,'whereSelectField'=>null
		,'orderSelectField'=>'user'
		,'helpText'=>'Usuario back-office'
		,'helpTextExtend'=>'identificador del usuario al que se le asignan las propiedades personalizadas'
		,'visible'=>1
		,'placeHolder'=>''
		,'formGroup'=>1
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_i_button_list_type'=>array(
		'formType'=>'select'
		,'dataType'=>'integer'
		,'tabularListData'=>['columnOrder'=>3]
		,'required'=>1
		,'multipleSelect'=>false
		,'arrStaticValues'=>array(1=>'Circular',2=>'Cuadrado') // OPCIONES ESTATICAS PARA EL TAG
		,'arrValues'=>array(1=>'<i class="zmdi zmdi-circle-o bgm-lime c-black" data-trigger="hover" data-toggle="popover" data-placement="left" data-content="estilo circular"></i>',2=>'<i class="zmdi zmdi-square-o bgm-yellow c-black" data-trigger="hover" data-toggle="popover" data-placement="left" data-content="estilo cuadrado"></i>')
		,'label'=>'Tipo de boton en el listado'
		,'helpText'=>'Tipo de botones en los listados'
		,'visible'=>1
		,'placeHolder'=>''
		,'formGroup'=>1
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_auto_update_time'=>array(
		'formType'=>'text'
		,'dataType'=>'slider'
		,'sliderConf'=>array('startValue'=>2,'endValue'=>60,'stepValue'=>1)
		,'required'=>false
		,'label'=>'Auo-update'
		,'helpText'=>'Intervalo de tiempo'
		,'helpTextExtend'=>'Intervalo de tiempo utilizado en el auto-update para los listados (solo donde est&eacute; disponible)'
		,'visible'=>true
		,'placeHolder'=>'Tiempo en segundos.'
		,'formGroup'=>1
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_top_header_theme_bckCol'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'tabularListData'=>['columnOrder'=>4]
		,'required'=>1
		,'multipleSelect'=>false
		,'arrStaticValues'=>$arrBackAdminColors
		,'defaultSelectText'=>'DEFAULT_TEXT'
		,'defaultValue'=>''
		,'allowZeroValue'=>false
		,'label'=>'Color de fondo header/men&uacute; izquierdo'
		// ,'cssControl'=>'autoSaveTheme' // CLASE PARA CONTROLAR ACCIONES DEL ELEMENTO POR JS ADEMAS DE UN ESTILO ESPECIAL
		,'helpText'=>''
		,'visible'=>1
		,'formGroup'=>2
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_btn_default_forCol'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'required'=>1
		,'multipleSelect'=>false
		,'arrStaticValues'=>$arrForeAdminColors
		,'defaultSelectText'=>'DEFAULT_TEXT'
		,'defaultValue'=>''
		,'allowZeroValue'=>false
		,'label'=>'Color primer plano en botones'
		,'helpText'=>''
		,'helpTextExtend'=>'Color de los textos de botones'
		,'visible'=>1
		,'formGroup'=>2
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_btn_default_bckCol'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'required'=>1
		,'multipleSelect'=>false
		,'arrStaticValues'=>$arrBackAdminColors
		,'defaultSelectText'=>'DEFAULT_TEXT'
		,'defaultValue'=>''
		,'allowZeroValue'=>false
		,'label'=>'Color de fondo de botones'
		,'helpText'=>''
		,'helpTextExtend'=>'Color de fondo de los botones stabdard del back-office'
		,'visible'=>1
		,'formGroup'=>2
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_background_bckCol'=>array(
		'formType'=>'select'
		,'dataType'=>'varchar'
		,'required'=>0
		,'multipleSelect'=>false
		,'arrStaticValues'=>$arrBackAdminColors
		,'defaultSelectText'=>'DEFAULT_TEXT'
		,'defaultValue'=>''
		,'allowZeroValue'=>false
		,'label'=>'Color de fondo de pantallas'
		,'helpText'=>''
		,'helpTextExtend'=>'Color de fondo para algunos elementos del back-office (en beta)'
		,'visible'=>1
		,'formGroup'=>2
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_b_display_sql'=>array(
		'formType'=>'checkbox'
		,'dataType'=>'toggle'
		,'tabularListData'=>['columnOrder'=>5,'columnWidth'=>2]
		,'tabularFilter'=>['mode'=>'STATIC','origin'=>'self','tag'=>array('filterType'=>'radio'),'dataFields'=>array('keyOptions'=>'descripcion_estado','textOptions'=>'descripcion_estado','keyDefault'=>'0'),'dataValues'=>array(0=>'NO',1=>'SI')]
		,'required'=>0
		,'toggleParams'=>array('tsColor'=>'green')
		,'label'=>'Ver Sql'
		,'arrValues'=>array(0=>'<span class=c-red>no</span>',1=>'SI')
		,'helpText'=>'Visualizar sentencias Sql'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>3
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_view_line_numbers'=>array(
		'formType'=>'checkbox'
		,'dataType'=>'toggle'
		,'required'=>0
		,'toggleParams'=>array('tsColor'=>'green')
		,'label'=>'Ver n&uacute;meros de l&iacute;nea'
		,'arrValues'=>array(0=>'<span class=c-red>no</span>',1=>'SI')
		,'helpText'=>'Ver los n&uacute;meros de l&iacute;nea en los documentos editables con Geshi'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>3
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_b_debugSession'=>array(
		'formType'=>'checkbox'
		,'dataType'=>'toggle'
		,'tabularListData'=>['columnOrder'=>6]
		,'tabularFilter'=>['mode'=>'STATIC','origin'=>'self','tag'=>array('filterType'=>'radio'),'dataFields'=>array('keyOptions'=>'descripcion_estado','textOptions'=>'descripcion_estado','keyDefault'=>'0'),'dataValues'=>array(0=>'NO',1=>'SI')]
		,'required'=>0
		,'toggleParams'=>array('tsColor'=>'green')
		,'label'=>'Ver errores'
		,'arrValues'=>array(0=>'<span class=c-red>no</span>',1=>'<i class="zmdi zmdi-check f-20 c-green f-700"></i>')
		,'helpText'=>'Visualizar mensajes de error'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>3
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_b_on_filters_start'=>array(
		'formType'=>'radio'
		,'dataType'=>'integer'
		,'tabularListData'=>['columnOrder'=>7,'columnWidth'=>2]
		,'required'=>0
		,'arrValues'=>array(0=>'<i class="fa fa-retweet f-16"></i> movimiento',1=>'<i class="fa fa-hand-o-up f-16"></i> click derecho')
		,'arrValuesRadioTag'=>array(0=>'Movimento', 1=>'Click derecho')
		,'label'=>'Como abrir filtros'
		,'helpText'=>'Modo di apparizione dei filtri'
		,'helpTextExtend'=>'per i filtri di ricerca delle colonne del listato, per mezzo del movimento del mouse oppure facendo CLICK'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>3
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_b_filters_location'=>array(
		'formType'=>'radio'
		,'dataType'=>'integer'
		,'required'=>0
		,'arrValuesRadioTag'=>array(0=>'Top', 1=>'Left')
		,'label'=>'Posic&oacute;n de los filtros'
		,'helpText'=>'Lugar donde deben aparicer de los filtros'
		,'helpTextExtend'=>'para las columnas de los listados tabulares, a la izquierda de la pantalla o en la zona superior del listado'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>3
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
	,'pref_ref_lang'=>array(
		'formType'=>'select'
		,'dataType'=>'text'
		,'required'=>1
		,'label'=>'Pa&iacute;s del Idioma'
		,'fieldTableName'=>__QUERY_TABLES_PREFIX.'langs'  // NOMBRE DE LA TABLA ORIGEN A UTILIZAR PARA LA EXTRACCION DE DATOS PARA OBTENER LOS VALORES DE UN CAMPO "SELECT"
		,'arrFieldsTagSelect'=>array('iso3','spanish_name')	// PRIMERO: REFERENCIA A GUARDAR EN BBDD / SEGUNDO: TEXTO A MOSTRAR EN EL LISTADO
		,'defaultSelectText'=>'Seleccionar'
		,'orderSelectField'=>'iso3'
		,'whereSelectField'=>'b_enabled'
		,'visible'=>1
		,'selectSearchBox'=>true
		,'multipleSelect'=>false
		,'placeHolder'=>''
		,'formGroup'=>4
		,'excludeAutoForm'=>false
		,'helpText'=>'Selecciona un pa&iacute;s para el idioma que prefieres en tu back-office'
		,'order'=>++$order
	)
	,'b_enabled'=>array(
		'formType'=>'checkbox'
		,'dataType'=>'toggle'
		,'tabularListData'=>['columnOrder'=>7]	// PARA QUE NO VISUALICE EN EL LISTADO, PONER EL MISMO NUMERO DEL ULTIMO UTILIZADO
		,'required'=>0
		,'toggleParams'=>array('tsColor'=>'green')
		,'label'=>'Habilitar las perferencias'
		,'arrValues'=>array(0=>'<span class=c-red>no</span>',1=>'SI')
		,'helpText'=>'Habilitar / deshabilitar las preferencias del usuario'
		,'visible'=>1
		,'placeHolder'=>null
		,'formGroup'=>4
		,'excludeAutoForm'=>false
		,'order'=>++$order
	)
);

	// HEADER GROUP PROPERTIES
$moduleSetting->arrGoupsForm=array(
	1=>array(
		'groupTextTitle'=>'Gesti&oacute;n de las preferencias para el back-office'
		,'additionalInfo'=>''
		,'groupIco'=>'zmdi '.$moduleSetting->moduleIcon
		,'groupHeaderClass'=>'bg-black-trp'
	)
	,2=>array(
		'groupTextTitle'=>'Colores'
		,'additionalInfo'=>''
		,'groupIco'=>'zmdi zmdi-format-color-fill'
		,'groupHeaderClass'=>'bg-black-trp'
	)
	,3=>array(
		'groupTextTitle'=>'Otras opciones del usuario'
		,'additionalInfo'=>'Par&aacute;metros funcionales'
		,'groupIco'=>'zmdi zmdi-view-compact'
		,'groupHeaderClass'=>'bg-black-trp'
	)
	,4=>array(
		'groupTextTitle'=>'Opciones de sistema'
		,'additionalInfo'=>''
		,'groupIco'=>'zmdi zmdi-view-compact'
		,'groupHeaderClass'=>'bg-black-trp'
	)
);

