<?php
/**
 * LOCAL AJAX MODULE
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

include('../../../conf/config_web.php');
include('../../../conf/config.misc.php');

ini_set('display_errors',1);
error_reporting($errorReportDefault);
$moduleParams='';

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
$response['query']=array();

if($hashSecutityScript != __HASH_CONTROL_SCRIPT){
  die(__MSG_ACCESS_ERROR);
}

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$tmpVars=json_decode(file_get_contents('php://input'), true);	// DECODE JSON DATA

if(count($tmpVars))
  $arraExtVars=$tmpVars;

if(isset($arraExtVars['loginParams']))
	$moduleParams=json_decode($arraExtVars['loginParams']);

$modeLoadConfig=1;
//echo "\n".'1. ';print_r($arraExtVars);
if(isset($arraExtVars['from']) && $arraExtVars['from'])
{
  $localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'].'_mod_bbdd/';
  $localModuleFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'].'';
	$provenience=$_SERVER['HTTP_REFERER'];
	$modNameFrom=strpos($provenience,'_modules/')+9;
	$modName=substr($provenience, $modNameFrom);
	$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));

	if(file_exists($dirModulesAdmin.'_modules/'.$modName.'/config_module.php'))
	{
		include($dirModulesAdmin.'_modules/'.$modName.'/config_module.php');
//echo $moduleSetting->mainTable;//die();
	}
}


//$folderCommonModules=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/';
//$folderModule=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'];
//include('config_module.php');

if($arraExtVars['action'] == 'chekFormSubmitValidation')
{
	include($dirModAdmin.'mod_bbdd.class.php');
  include($localModelsFolder.'module_model_ajax.class.php');

  $dataModuleBBDD=null; $msgModuleError=' Error en el script Ajax: <br>'.__FILE__;
  $dataModuleBBDD=new ModelLocalModuleAjax($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting, $port);
  $resConnexion=$dataModuleBBDD->connectDB();

  if(!$resConnexion)
    $msgModuleError='<div class="text-center w-100  pull-left"><div class="alert alert-danger p-25 f-16">'.strtoupper(__COMMON_BBDD_TXTCONNECTERROR).'<br><br>'.__COMMON_BBDD_TXTNOCONNECTIONBBDD.'<br><br>'.__COMMON_GENERICTEXT_ERROCONFIGMODULEEND.'<br></div></div>';

  $cur_conn_id=$dataModuleBBDD->idConexion;

	if($arraExtVars['sqlAction'] == 'checkUnique')	// * SE IMPIDE CAMBIAR EL KEY ID SI YA EXISTE UN REGISTRO ALMACENADO EN LA BBDD PARA EL NUEVO ID A SETEAR
		$resValidation=$dataModuleBBDD->chekOtherKeyExistent(@$arraExtVars['sql']);

  if($_SESSION['userAdmin']['pref_']['b_display_sql'])
  {
    $response['query']['sql'][]=$dataModuleBBDD->query;
    $response['query']['res'][]=$resValidation;
  }

  if(@$dataModuleBBDD->error || !$cur_conn_id)
  {
    $response['errorTxt']=isset($dataModuleBBDD->errorTxt) ? $dataModuleBBDD->errorTxt : $msgModuleError;
    $response['success']=false;
    $res=json_encode($response);
    die($res);
  }

  //  NO SE PUEDE CAMBIAR EL ID DEL REGISTRO EXISTENTE, MENSAJES DE ERROR
  if(isset($resValidation) && $resValidation['numRecords'] && $arraExtVars['sqlAction'] == 'checkUnique')
  {
    $response['errorTxt'][]='No es posible proseguir, el valor para "'.@$arraExtVars['sql']['fieldNameToCheck'].'" ya existe en la base de datos';
    $response['errorTxt'][]='Estás intentando guardar esta configuración para un usuario que ya tiene una definida ('.$resValidation['idKeyExistent'].')';
    $response['tmp']=$dataModuleBBDD->tResultadoQuery;
    $response['success']=false;
  }
  else
    $response['success']=true;

  $res=json_encode($response);
  die($res);
//echo "\n".$localModelsFolder;
//echo "\n".'1. ';print_r($cur_conn_id);
}


/**
 * CAMBIA INSTANTANEAMENTE ALGUNAS CONFIGURACIONES DEL USUARIO ACTIVO
 * PARA LA SESION ABIERTA
 */
if($arraExtVars['action'] == 'updateUserSessionPreferences' && $arraExtVars['tmp_ID'] == $_SESSION['userAdmin']['userId'])
{
  $localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/user_preferences/_mod_bbdd/';
  $localModuleFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/user_preferences/';
	$provenience=$_SERVER['HTTP_REFERER'];
	$modNameFrom=strpos($provenience,'_modules/')+9;
	$modName=substr($provenience, $modNameFrom);
	$modName=substr($provenience, $modNameFrom,strpos($modName,'/'));

	if(file_exists($dirModulesAdmin.'_modules/'.$modName.'/config_module.php'))
		include($dirModulesAdmin.'_modules/'.$modName.'/config_module.php');

	if(count($arraExtVars['db'][$moduleSetting->mainTable]) && $_SESSION['userAdmin']['userId'][0] == (@$arraExtVars['db'][$moduleSetting->mainTable]['user_id'][0] || @$arraExtVars['db'][$moduleSetting->mainTable]['id'][0]) && $arraExtVars['db'][$moduleSetting->mainTable]['b_enabled'][0] != 0)
	{
    foreach($arraExtVars['db'][$moduleSetting->mainTable] as $key=>$value)
    {
      $colorType=substr(substr($key,5),-6);
			$tmpVal=null;

      if(substr($key,0,5) != 'pref_')
        continue;

      if($colorType == 'forCol' || $colorType == 'bckCol')  // SOLO PARA ASIGNACION DE COLORES
      {
        $colorType=$colorType == 'forCol' ? 'c-' : 'bgm-';
				$tmpVal=(isset($value) && $value !='') ? $colorType.$value[0] : $colorType.$arrDefaultColor[substr($key,5)];
        $_SESSION['userAdmin']['pref_'][substr($key,5)]=$tmpVal;
      }
      else
			{
				$tmpVal=$value[0];
//        $_SESSION['userAdmin']['pref_'][substr($key,5)]=$tmpVal;
        $_SESSION['userAdmin']['pref_'][substr($key,5)]=is_array($value) ? $tmpVal : $value;
			}

			if($_SESSION['userAdmin']['pref_']['b_display_sql'])
				$response['userPreferencesUpdated'][substr($key,5)]=$tmpVal;
    }
//    if($colorType != 'forCol' && $colorType != 'bckCol')  // SOLO PARA ASIGNACION DE COLORES
//      continue;
//
//    $colorType=$colorType == 'forCol' ? 'c-' : 'bgm-';
//    $_SESSION['userAdmin']['pref_'][substr($key,5)]=(isset($value) && $value !='') ? $colorType.$value[0] : $colorType.$arrDefaultColor[substr($key,5)];
//    }
	}
  else
  {
    unset($_SESSION['userAdmin']['pref_']);
  }
  $response['success']=true;
  $res=json_encode($response);
  die($res);
//	die(0);
}

die(0);


echo "\n--FIN--";print_r($arraExtVars);
die('end script');
include($localModelsFolder.'module_model.class.php');

$dataModuleBBDD=null;
$dataModuleBBDD=new ModelLocalModule($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting, $port);
$resConnexion=$dataModuleBBDD->connectDB();

if(!$resConnexion)
	$msgModuleError='<div class="text-center w-100  pull-left"><div class="alert alert-danger p-25 f-16">'.strtoupper(__COMMON_BBDD_TXTCONNECTERROR).'<br><br>'.__COMMON_BBDD_TXTNOCONNECTIONBBDD.'<br><br>'.__COMMON_GENERICTEXT_ERROCONFIGMODULEEND.'<br></div></div>';

$cur_conn_id=$dataModuleBBDD->idConexion;

$response['res']=true;
$res=json_encode($response);
die($res);