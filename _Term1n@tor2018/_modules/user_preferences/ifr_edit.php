<?php
/**
 * EDIT MODULE users preferences
 *
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');

ini_set('display_errors',$displayErrors);
error_reporting($errorReportDefault);
if(empty($_SESSION['id'])){echo '<script>parent.document.location="'.$web_url.$adminFolder.'/_modules/login/";</script>';}

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
if($hashSecutityScript != __HASH_CONTROL_SCRIPT) die(__MSG_ACCESS_ERROR);

$localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'].'_mod_bbdd/';
$localModuleFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'];

include($localModuleFolder.'config_module.php');	// LOCAL MODULE CONFIGURATION PARAMS

$msgModuleError=null; $dataModuleBBDD=null;
$iframeAction=ucfirst($arraExtVars['action']);  // NORMALIZA EL NOMBRE DE LA ACCION PARA IDENTIFICAR EL ELEMENTO A MANEJAR

/**
 *	START CARGA DE DATOS PROPIOS DEL REGISTRO A EDITAR/INSERTAR
 */
include($dirModAdmin.'mod_bbdd.class.php');
include($dirModAdmin.'mod_common_edit.class.php');
include($localModelsFolder.'module_model.class.php');

$dataModuleBBDD=new CommonEditingModel($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting, $port);
$resConnexion=$dataModuleBBDD->connectDB();

if(!$resConnexion)
{
	$msgModuleError='<div class="text-center w-100  pull-left"><div class="alert alert-danger p-25 f-16">'.strtoupper(__COMMON_BBDD_TXTCONNECTERROR).'<br><br>'.__COMMON_BBDD_TXTNOCONNECTIONBBDD.'<br><br>'.__COMMON_GENERICTEXT_ERROCONFIGMODULEEND.'<br></div></div>';
}

/**
 * SE OBTIENEN LOS DATOS RELATIVOS AL REGISTRO EDITADO (campos standard)
 *
 * $resDataForm ES EL ARRAY CON LOS DATOS DEL ACTUAL REGISTRO
 *
 */
$resDataForm=[];
if(isset($arraExtVars['id']))
	$resDataForm=$dataModuleBBDD->getCommonRecordData((int)$arraExtVars['id'], (isset($arraExtVars['foreignKey']) ? $arraExtVars['foreignKey'] : ''));

$dataLocalModel=new ModelLocalModule($dataModuleBBDD);
//	END CARGA DE DATOS PROPIOS DEL REGISTRO A EDITAR/INSERTAR


/**
 * FUNCIONES COMUNES PARA LOS FORMULARIOS DE LOS MODULOS DEL BACK-OFFICE
 */
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_classes/common_forms_methods.class.php');
$objForm=new CommonModulesMethods();

// HTML (HEADER/TOP) + ACCIONES COMUNES JS PARA LA EDICION DEL MODULO ACTUAL
include('../common_edit_html_top.php');
$outFormBoxField='';

$isAuthorized=true;
//echo '<pre>';print_r($resDataForm);
if(isset($resDataForm['user_id']) && ($resDataForm['user_id'] != $_SESSION['userAdmin']['userId']))
{
  if($_SESSION['userAdmin']['roleId'] != __LEVEL_ACCESS_GOD)
  {
    echo '<br /><br /><div class="alert alert-danger w-100 p-20 f-16"><center>'.__MOD_ROLE_NOROLEACCESSTXT.'</center></div><br />';
    $isAuthorized=false;
  }
}

if($resConnexion && $isAuthorized)
{
  $arrGroups=array();
	$countPos=0;

  foreach($moduleSetting->arrFieldsFormModal as $fieldname=>$value)  // SE RECORREN TODOS LOS CAMPOS A DIBUJAR
  {
    $tmpFieldGroupValue=isset($moduleSetting->arrFieldsFormModal[$fieldname]['formGroup']) ? $moduleSetting->arrFieldsFormModal[$fieldname]['formGroup'] : $tmpFieldGroupValue;

		foreach($value as $ff=>$vv) // SE AGRUPAN SEGUN EL "formGroup" QUE TIENEN ASIGNADO
			$arrGroups[$tmpFieldGroupValue][$fieldname][$ff]=$vv;

    if(@$moduleSetting->arrGoupsForm[$countPos])
    {
			$arrGroups[$countPos]['sys_configGroupForm']=$moduleSetting->arrGoupsForm[$countPos];
			unset($moduleSetting->arrGoupsForm[$countPos]);
    }

		$countPos++;
  }

  /**
   * RECORRER TODOS LOS CAMPOS A COLOCAR EN EL FORMULARIO
   * PARA CREAR EL BOX CORRESPONDIENTE
   */
  $outFormBoxField="\r\n\t".'<div class="card-body card-padding">';

	$count=0;
	$jsForm='';	// JS QUE SE CREA DINAMICAMENTE EN LA CARGA DE LOS CAMPOS QUE FORMAN EL FORMULARIO DE EDICION / INSERCION
	$jsFormSubmitCheckParams='';	// CONTROLES PARA EL SUBMIT DEL MODULO ACTUAL

	foreach($arrGroups as $refGroup=>$groupFields)
	{
		if($refGroup)	// SE OMITE COMPONER ESTOS TAGS A LOS ELEMENTOS PERTENECIENTES AL GRUPO 0
		{
			$outFormBoxField.="\r\n\t".'<div class="row">';
			$outFormBoxField.="\r\n\t\t".'<div class="boxField form-group bs-item z-depth-1 col-sm-12 p-20">';
		}

		if(isset($groupFields['sys_configGroupForm']) && count($groupFields['sys_configGroupForm']))	// CABECERA DEL GRUPO DEL FORMULARIO
		{
			$outFormBoxField.=$objForm->buildGroupHeader($groupFields['sys_configGroupForm'], $count);
		}

		if($refGroup)
			$outFormBoxField.="\r\n\t\t".'<div class="containerGroup" data-ref="'.$count.'">';

		foreach($groupFields as $fieldName=>$paramField)
		{
			$resHTML='';

			if(in_array($fieldName, $arrEnabledFieldStatus) && ($_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole) && isset($arraExtVars['id']))	// SOLO PARA LA EDICION DE REGISTROS
			{
				$jsForm.='setButtonEnableDisable('.(int)@!$resDataForm[$fieldName].', '.$arraExtVars['id'].', "'.$fieldName.'");';
			}

			if(isset($paramField['excludeAutoForm']) && $paramField['excludeAutoForm'])
				continue;

			$defaultFormType=isset($paramField['formType']) ? $paramField['formType'] : 'text';
			$defaultLabel=isset($paramField['label']) ? $paramField['label'] : '';
			$defaultPlaceHolder=isset($paramField['placeHolder']) ? $paramField['placeHolder'] : '';
			$methodName=str_replace('-', ' ', $defaultFormType);
			$methodName=ucfirst($methodName);
			$methodName=str_replace(' ', '', $methodName);
			$methodName='buildForm'.$methodName;

			if($defaultFormType != 'hidden')
			{
				if(method_exists($objForm, $methodName) && isset($paramField['visible']) && $paramField['visible'])	// CONSTRUCCION HTML DEL ELEMENTO DEL FORMULARIO
				{
					if(isset($dataLocalModel) && isset($paramField['sourceTagBbddData']))	// OBTENCION DATOS DESDE TABLAS RELACIONADAS INDEPENDIENTES A LA MAIN TABLE
						$resDataForm['select='.$fieldName]=$dataModuleBBDD->getRelatedDataField((int)@$arraExtVars['id'], @$paramField['fieldTableName'], $paramField['sourceTagBbddData'], $paramField['whereSelectField'], @$paramField['orderSelectField'], @$paramField['onlyFieldValueName']);

					if(isset($dataLocalModel) && count(@$paramField['arrFieldsTagSelect']))
						$resDataForm['select='.$fieldName]=$dataModuleBBDD->getSpecificRecordData((int)@$arraExtVars['id'], @$paramField['fieldTableName'], $paramField['arrFieldsTagSelect'], $paramField['whereSelectField'], $paramField['orderSelectField']);

					$resHTML=$objForm->$methodName($fieldName, $paramField, @$resDataForm, $moduleSetting->mainTable);
				}

				$outFormBoxField.=$resHTML;
			}
			else	// HIDDEN FIELDS
			{
				$outFormBoxField.="\r\n\t\t\t\t".'<input type="'.$defaultFormType.'" name="db['.$moduleSetting->mainTable.']['.$fieldName.']" value="'.@$resDataForm[$fieldName].'" />';
			}
			$count++;


      // 
      /**
       * SE CONSTRUYE EL CONTROL PARA EVITAR DUPLICADOS POR EL PARAMETRO DEL CAMPO EVALUADO "uniqueRecord"
       * ESTRUCTURA de fieldToCheck:
       * action = accion que debe hacer el PHP
       * from = mobre de la carpeta del modulo actual
       * sqlAction = 
       * sql[type] = tipo de datos (int, array, varchar
       * sql[table] = nombre de la tabla a utilizar
       * sql[value] = nombre del campo tal como aparece en el formulario de edicion
       * sql[fieldNameToCheck] = nombre del campo (de la tabla de la BBDD) a testear 
       * sql[currentKeyValue] = valor del campo sql[fieldNameToCheck] existente en la BBDD
       * sql[valueTocheck] = dinamico, por js, nuevo valor del campo editado
			 * idKeyMainTable = nombre del campo clave de la tabla a verificar
			 * valueKeyMainTable = valor de campo clave de la tabla a verificar
       */
      if(isset($paramField['uniqueRecord']) && $paramField['uniqueRecord'])
      {
        $jsFormSubmitCheckParams.='fieldToCheck={action:"chekFormSubmitValidation",from:"'.$arraExtVars['from'].'",sqlAction:"checkUnique",sql:{type:"varchar", table:"'.$moduleSetting->mainTable.'", value:"db['.$moduleSetting->mainTable.']['.$fieldName.'][]",fieldNameToCheck:"'.$fieldName.'",currentKeyValue:"'.@$resDataForm[$fieldName].'",valueTocheck: document.querySelector(\'[name="db['.$moduleSetting->mainTable.']['.$fieldName.'][]"]\').value}};';//$paramField
      }
// CON ESTOS DATOS IMPIDE EL CAMBIO DE ID DE USUARIO A REGISTRO EXISTENTE
//        $jsFormSubmitCheckParams.='fieldToCheck={action:"chekFormSubmitValidation",from:"'.$arraExtVars['from'].'",sql:{sqlAction:"checkUnique", type:"varchar", table:"'.$moduleSetting->mainTable.'", value:"db['.$moduleSetting->mainTable.']['.$fieldName.'][]",fieldNameToCheck:"'.$fieldName.'",valueTocheck: document.querySelector(\'[name="db['.$moduleSetting->mainTable.']['.$fieldName.'][]"]\').value, idKeyMainTable: "'.@$moduleSetting->keyListLabel.'", valueKeyMainTable: "'.@$arraExtVars['id'].'", tableSourceDataCheck: "'.@$paramField['fieldTableName'].'"}};';//$paramField
		}

		if($refGroup)	// SE OMITE COMPONER ESTOS TAGS A LOS ELEMENTOS PERTENECIENTES AL GRUPO 0
		{
			$outFormBoxField.="\r\n\t\t\t".'</div>';
			$outFormBoxField.="\r\n\t\t".'</div>';
			$outFormBoxField.="\r\n\t".'</div>';
		}
	}	// END FOREACH $arrGroups

  $outFormBoxField.="\r\n".'</div>';
}

?>

<script type="text/javascript">
<!--
/**
 * ***** initializeModule() :: MUY IMPORTANTE *****
 * para la visualizacion correctamente el MODULO DE EDICION/INSERCION:
 *
 * REDIMENSIONA EL ALTO DEL IFRAME CONTENEDOR DEL FORMULARIO DE INSERCION DE DATOS
 * SEGUN LA ALTYRA ACTUAL DE LA CAJA CONTENEDORA
 */
var txtTmp="";

function initializeModule()
{
  $(".contentForm").show();
  parent.$("#ifrm<?=$iframeAction?>Record").css("height",parent.$("#boxContent<?=$iframeAction?>Record").css("height"));parent.$("#boxContent<?=$iframeAction?>Record").show();
	return;
}

/**
 * CHECK PARA TODAS LAS EXCEPCIONES POSIBLES A LOS VALORES "required" ESTABLECIDOS EN EL FORMULARIO
*/
function checkValidFormData(dataCheck)
{
  var resCheck=false;

  $.ajax({
    data: dataCheck,
    url: root_path+"<?=$arraExtVars['from']?>ajax_module.php", method: "post", dataType: "json", cache: false, async: false, contentType: false, processData:false
    ,success:function(response){
			var tmpSql=response.query.sql ? response.query.sql : 0;
			if(tmpSql) window.parent.commonNotify("","<br />"+tmpSql+"<br />", undefined, undefined, undefined,( response.success ? "info" : "danger"), undefined, undefined, ( response.success ? 6000 : 20000));

      if(!response.success)
      {
				txtTmp="";
        if(response.errorTxt.constructor === Array) // HAY VARIOS ELEMENTOS EN EL ERROR
        {
          $(response.errorTxt).each(function (index, value){
            txtTmp+="<li>"+value+"</li>";
          });
        }
        else
        {
          txtTmp=response.errorTxt ? "<li><b>"+response.errorTxt+"</b></li>" : "<?=__COMMON_GENERICTEXT_UNDETERMINEDERRORTXT?>";
        }
        txtTmp=response.errorTxt ? "<?=__COMMON_GENERICTEXT_FOLLOWINGERROROCCURREDTXT?>:<br /><br /><ul>"+txtTmp+"</ul><br /><br />" : "<?=__COMMON_GENERICTEXT_UNDETERMINEDERRORTXT?>";
        $(".errorSaveMessage").html("<hr />"+txtTmp);
        $("#modalSubmitError").modal();	// OPEN MODAL INFO OPERATION
        resCheck=false;
        return resCheck;
      }

      resCheck=true;
      return resCheck;
    }
    ,error:function(response){
      $(".errorSaveMessage").html("<?=__COMMON_JSCHECKVALIDFORMDATA_DATACONTROLFAILHTMLTEXT?>");
      $("#modalSubmitError").modal();	// OPEN MODAL INFO OPERATION
      resCheck=false;
      return resCheck;
    }
  });

  return resCheck;
}

$(document).ready(function(){
	/**
	 * ACCION DE EDICION / INSERCION DE REGISTRO
	 */
	$('#moduleMainForm').on('submit', function(e){
		e.preventDefault();

		if(("mainKeyId=",$("input[name=mainKeyId]").val() > 0))
			$(".waitMessage").addClass("bgm-lightblue");

		$(".waitMessage").html(("mainKeyId=",$("input[name=mainKeyId]").val() > 0) ? "<br /><br /><p><?=__COMMON_JSSUBMITEDITFORM_UPDATEMESSAGERECORDREFTXT?>: <span class='c-black c-back bgm-white p-r-5 p-l-5'>"+$("input[name=mainKeyId]").val()+"</span><br /><br /></p><center><div class='preloader pls-yellow'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div></center><br />" : "<?=__COMMON_JSSUBMITEDITFORM_INSERTMESSAGERECORDTXT?><br /><br /><center><div class='preloader pls-white'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div></center><br />");

		var dataSend=new FormData($('#moduleMainForm')[0]);

		$('input[type=file]').each(function (index, value)
    {
			dataSend.append($(this).attr('name')+"[fileName]", $(this).attr('data-image-src'));
			dataSend.append("previousImage["+$(this).attr('data-image-delete')+"]", $(this).attr('data-previous-image'));	// NECESARIO PARA ELIMINAR FISICAMENTE LAS IMAGENES NO UTILIZADAS

			if(this.attributes.value)
				dataSend.append($(this).attr('data-image-delete'), this.attributes.value.value);

			var tmpName="db[<?=$moduleSetting->mainTable?>]["+$(this).attr('data-image-delete')+"]";

			if(this.attributes.value && $(this).attr('data-previous-image'))
			{
				if($(this).val())
					dataSend.append(tmpName, $(this).val());
				else
					dataSend.append(tmpName, this.attributes.value.value);
			}
			else
			{
				dataSend.append(tmpName, $(this).val());
			}
		});

    fieldToCheck=undefined;
    <?=$jsFormSubmitCheckParams?>

    if(typeof fieldToCheck !== 'undefined') // CHECK DUPLICATE FIELDS VALUES
    {
      var dataCheck=JSON.stringify(fieldToCheck);
      var resAjax=checkValidFormData(dataCheck);

      if(!resAjax)
      {
        var displayError="";
        if(txtTmp)
          displayError=txtTmp;

        $(".errorSaveMessage").html(""+displayError);
        $("#modalSubmitError").modal();	// OPEN MODAL INFO OPERATION
        return false;
      }
    }

		parent.$(".actCancel").attr("disabled","disabled");

		if($("input[name='action']").val() == "update")
		{
			parent.$(".saveButton").attr("disabled","disabled");
			parent.$(".enableDisableButton").attr("disabled","disabled");
		}
		else
		{
			parent.$(".insertButton").attr("disabled","disabled");
		}
  
    $("#bbddOperation").modal();	// OPEN MODAL INFO OPERATION

		$.ajax({
      data: dataSend
      ,url: root_path + "ajax/common_admin.php", method: "post", dataType: "json", cache: false, async: true, contentType: false, processData:false
      ,success:function(response){
				var tmpSql=response.query.sql ? response.query.sql : 0;
				if(tmpSql) window.parent.commonNotify("","<br />"+tmpSql+"<br />", undefined, undefined, undefined,( response.success ? "info" : "danger"), undefined, undefined, ( response.success ? 6000 : 20000));

        if(!response.success)// && !response.msgTitle
        {
          commonRestoreErrorForm(response,0);
          return false;
        }
        else
        {
          window.parent.commonNotify(response.msgTitle ? response.msgTitle : "<?=__COMMON_NOTIFY_CORRECTOPERATION?>", response.msgText ? "<br />"+response.msgText : "", undefined, "center", undefined, 'success', "animated flipInX", undefined);
        }

				var dataSend=new FormData($('#moduleMainForm')[0]);
        dataSend.append("action", "updateUserSessionPreferences");	// ACTUALIZA EN LA SESION ACTUAL LA CONFIGURACION DEL USUARIO ACTIVO
        dataSend.append("tmp_ID", "<?=@$resDataForm['user_id']?>");

        $.ajax({data: dataSend,url: "ajax_module.php", method: "post", dataType: "json", cache: false, async: true, contentType: false, processData:false});

        parent.$(".saveButton").attr("disabled",'true');
        parent.$(".insertButton").attr("disabled",false);
        parent.$(".enableDisableButton").attr("disabled",false);
        parent.$(".actCancel").attr("disabled",false);
        parent.$(".actCancel").click();
        parent.window.ifrListBlock.gridObj.bootgrid('reload');
        return;
      }
      ,error:function(response){
				commonRestoreErrorForm(response,1);
				return false;
			}
		});

		return false;
	});

	parent.$(".headerAdditionalInfo").html("");

	$(window).load(function(){
    <?=($moduleSetting->isEditable && $isAuthorized) ? 'parent.setTimeout(\'$(".saveButton").fadeIn();\',300);' : 'parent.$(".saveButton").hide();' ?>
    parent.setTimeout('$(".insertButton").fadeIn();',300);
		first_input=$('input[type=text], select')[0];	// SELECCIONA EL PRIMER INPUT DE TIPO TEXTO DEL FORMULARIO

		if(first_input != undefined){setTimeout("setFieldFocus();",400)};
<?php

if($arraExtVars['action'] == 'edit') // DIBUJA EL TITULAR DE LA VENTANA MODAL DE EDICION
{
  echo 'parent.$(".headerAdditionalInfo").removeClass("hide").html("- ID: <span class=\'text-uppercase\'>'.@$arraExtVars['id'].'</span>");';
}

if(strlen($jsForm) && $moduleSetting->isCanEnabled)	// SOLO SI SE HAN CREADO INSTRUCCIONES JS, SE PINTAN EN EL FORMULARIO
{
	echo '
parent.setTimeout(\'$(".enableDisableButton").fadeIn();\',600);
';
	echo $jsForm;
}

?>
    setTimeout("initializeModule();vAlignImages();",100);

	}); // END DOCUMENT LOAD
}); // END DOCUMENT READY

function vAlignImages()
{
  $('.fileinput-new').each(function (index, value){$(value).click();return false;});
  return;
}

function setButtonEnableDisable(statusToSet, idRecord, fieldName)
{
	parent.$(".enableDisableButton").html(statusToSet ? "<i class='zmdi zmdi-check'></i>&nbsp;&nbsp;<?=__COMMON_BTN_ENABLERECORD?>" : "<span class='ORANGE'>&nbsp;<i class='zmdi zmdi-block'></i>&nbsp;<?=__COMMON_BTN_DISABLERECORD?>&nbsp;</span>");
	parent.$(".enableDisableButton").attr("data-id", idRecord);
	parent.$(".enableDisableButton").attr("data-field-name", fieldName);
	parent.$(".enableDisableButton").attr("data-status", statusToSet);

	if(statusToSet = 0)
		parent.$(".enableDisableButton").addClass("bgm-orange");

	return;
}
-->
</script>
</head>

<body>

<div class="contentForm card" style="display:none;">
  <form action="" method='post' id='moduleMainForm' name='moduleMainForm' onsubmit='//return false;' enctype='multipart/form-data'>
		<input name="mainKeyId" type="hidden" value="<?=(int)@$arraExtVars['id']?>" />
		<input name="mainKeyName" type="hidden" value="<?=$moduleSetting->keyListLabel?>" />
		<input name="action" type="hidden" value="<?=isset($arraExtVars['id']) ? 'update' : 'insert'?>" />

<?php
if($msgModuleError)
{
  /**
   * ERROR EN LA CARGA DE LOS DATOS
   */
	die($msgModuleError);
}
else
{
	// SE DIBUJAN LOS ELEMENTOS DEL FORMULARIO A MOSTRAR
	echo $outFormBoxField;
?>
		<input id="tmpSubmit" name="tmpSubmit" type="submit" class="hide" />
  </form>

<?php
//echo '<pre>';
//print_r($_SESSION);
//print_r($moduleSetting->arrFieldsFormModal);
//print_r(@$arraExtVars);
//print_r($dataModuleBBDD);
//print_r($moduleSetting->arrGoupsForm);
//print_r($arrGroups);
//print_r($moduleSetting->arrFieldsFormModal);
//echo '</pre>';
}

// TESTS...
//for($n=0;$n<rand(1,10);$n++){echo '<pre>block-test:'.$n.'<p>';print_r($arraExtVars);echo '</pre>';}
?>
</div>
<div class="modal fade" data-modal-color="blue" id="bbddOperation" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><i><?=__COMMON_MODALMSG_UPDATINGOPERATIONTITLEH4?></i></h4>
			</div>
			<div class="modal-body waitMessage">
				<p><?=__COMMON_MODALMSG_WAITMESSAGE?><span class="idTitleRef"></span></p>
			</div>
		</div>
	</div>
</div>

</body>
</html>