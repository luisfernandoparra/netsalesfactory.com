<?php
//	local language
//	ITALIAN

$langTxt['mod']['role']['noRoleAccessTxt']='Non sei autorizzato a visualizzare il contenuto per questo utente';
$langTxt['mod']['jsCheckValidFormData']['dataControlFailHtmlText']='La verifica dei dati <u>è statta incorretta</u><br /><br />Se l`errore persiste, si prega di contattare ufficio tecnico<br /><br /><br />';