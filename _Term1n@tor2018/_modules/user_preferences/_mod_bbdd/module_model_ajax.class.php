<?php
/**
 *	CLASE CON LAS FUNCIONES DE BBDD NECESARIAS LOS AJAX DEL MODULO
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class ModelLocalModuleAjax extends CBBDD
{
	protected $database;
	private $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $defaultOrderField;
	public $keyListLabel;
	public $mainTable;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;

  public function __construct($db_type=null, $db_host='', $db_user='', $db_pass='', $db_name='', $localParams=null, $port=1312)
  {
    $this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, null, $port);
		$this->arrFieldsTagSelect='';
		$this->query='';
		$this->prefixGtp=(isset($localParams->prefixTbl) && $localParams->prefixTbl) ? $localParams->prefixTbl : __QUERY_TABLES_PREFIX;
		return;
	}


  /**
   * CONTROL PARA IMPEDIR CAMBIAR EL VALOR DEL ID DE USUARIO
   * SI ESTE YA EXISTE EN LA TABLA DE CONFIGURACIONES
   * 
   * @param integer $sqlParams
   * @return integer
   */
	public function chekOtherKeyExistent($sqlParams=null)
	{
    if(!isset($sqlParams['table'])) // FALTA EL PARAMETRO ESENCIAL PARA EJECUTAR LA QUERY
    {
      $this->error=true;
      $this->errorTxt[]='Falta indicar la accion SQL';
      $this->errorTxt[]='Es necesario conocer la acción para poder aplicar el control necesario del campo: <b>'.$sqlParams['value'].'</b>';
      return;
    }
    $query='SELECT COUNT(*) AS numRecords, user_id AS idKeyExistent FROM %s.%s WHERE %s!=%s && %s=%s';
		$this->query=sprintf($query, $this->db, $this->prefixGtp.$sqlParams['table'], isset($sqlParams['fieldNameToCheck']) ? $sqlParams['fieldNameToCheck'] : '1 ', (isset($sqlParams['type']) && $sqlParams['type'] == 'int') ? (int)$sqlParams['currentKeyValue'] : '\''.$sqlParams['currentKeyValue'].'\'', isset($sqlParams['fieldNameToCheck']) ? $sqlParams['fieldNameToCheck'] : '1 ', (isset($sqlParams['type']) && $sqlParams['type'] == 'int') ? (int)$sqlParams['valueTocheck'] : '\''.$sqlParams['valueTocheck'].'\'');

//echo 'chekValidationFormData :: '.$this->query."\nSQL params:";print_r($sqlParams);die();
    $this->getResultSelectArray($this->query);
    $resQuery=$this->tResultadoQuery;
    $resQuery=$resQuery[0];
		return $resQuery;
  }


  /**
   * QUERY QUE IMPIDE CAMBIAR EL KEY ID A UNA REGISTRO EXISTENTE
   * SI ESTE YA EXISTE EN LA TABLA DE CONFIGURACIONES
   *
   * FUNCION NO ACTUALMENTE UTILIZADA
   * 
   * @param integer $sqlParams
   * @return integer
   */
	public function chekValidationDuplicateRecord($sqlParams=null)
	{
    if(!isset($sqlParams['table'])) // FALTA EL PARAMETRO ESENCIAL PARA EJECUTAR LA QUERY
    {
      $this->error=true;
      $this->errorTxt[]='Falta indicar la accion SQL';
      $this->errorTxt[]='Es necesario conocer la acción para poder aplicar el control necesario del campo: <b>'.$sqlParams['value'].'</b>';
      return;
    }

    $query='SELECT COUNT(*) AS numRecords FROM %s.%s WHERE %s!=%s && %s=%s';
		$this->query=sprintf($query, $this->db, $this->prefixGtp.$sqlParams['table'], isset($sqlParams['fieldNameToCheck']) ? $sqlParams['fieldNameToCheck'] : '1 ', (isset($sqlParams['type']) && $sqlParams['type'] == 'int') ? (int)$sqlParams['valueTocheck'] : '\''.$sqlParams['valueTocheck'].'\'', isset($sqlParams['idKeyMainTable']) ? $sqlParams['idKeyMainTable'] : '1 ', (isset($sqlParams['type']) && $sqlParams['type'] == 'int') ? (int)$sqlParams['valueKeyMainTable'] : '\''.$sqlParams['valueKeyMainTable'].'\'');
//echo 'chekValidationFormData :: '.$this->query."\nSQL params:";print_r($sqlParams);die();
    $this->getResultSelectArray($this->query);
    $resQuery=$this->tResultadoQuery;
    $resQuery=$resQuery[0]['numRecords'];
		return (int)$resQuery;
  }

}