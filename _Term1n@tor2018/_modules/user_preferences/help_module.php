<div class="card-body card-padding">
	<div class="help-basic-blocks fw-container">

		<ul class="tab-nav text-center fw-nav">
			<li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Intro</a></li>
			<!--<li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">otro TAB</a></li>-->
		</ul>

		<div class="tab-content">
				<div class="tab-pane fade active in f-16" id="tab1">
					<p>Módulo de configuraciones personales para los administradores de esta herramienta.</p>
					<ol>
						<li class="m-b-10">Las configuraciones de los usuarios son personales, y solamente a los administradores de máximo nivel se les está permitido modificar a otros usuarios. Normalmente cada usuario puede editar solamente sus preferencias.</li>
						<li class="m-b-10">Entre otras posibilidades, se pueden personalizar algunos estilos (colores) de esta misma herramienta.</li>
						<li class="m-b-10">También, si disponible, existe la posibilidad de cambiar de idioma.</li>
						<li class="m-b-10">Algunos de los parámetros puede que no sean operativos si no se posee el rol de acceso requerido.</li>
					</ol>
				</div>
				<div class="tab-pane fade f-16" id="tab2">
				</div>

<!-- PAGINACION SOLO SI NECESARIA
				<ul class="fw-footer pagination wizard">
						<li class="previous first"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
						<li class="previous"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-left"></i></a></li>
						<li class="next"><a class="a-prevent" href="#null"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li class="next last"><a class="a-prevent" href="#null"><i class="zmdi zmdi-more-horiz"></i></a></li>
				</ul>
-->
			</div>

	</div>
</div>
