<?php
/**
 * LOCAL MODULE CONFIGURATION SETTINGS
 *
 * ADMIN USERS
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

$moduleSetting=new stdClass();
$moduleSetting->moduleCommenHeader='<small>comentarios espec&iacute;ficos del m&oacute;dulo actual....</small>';

// MANDATORY PARAMS
$moduleSetting->mainTable='adminusers';
$moduleSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$moduleSetting->orderField='userId';
$moduleSetting->keyListLabel='userId';
$moduleSetting->keyInnerLeft='user_id';
$moduleSetting->userFieldName='user';
$moduleSetting->passFieldName='password';
$moduleSetting->additionalTables=['usu'=>$moduleSetting->mainTable, 'upref'=>'adminusers_preferences']; // SQL TABLES
$moduleSetting->arrFieldsOutput=[
  'usu'=>['userId','roleId','user','password','b_enabled','name','surname','email','theme','profile_image']
  ,'upref'=>['user_id','pref_ref_lang','pref_background_bckCol','pref_btn_default_bckCol','pref_btn_default_forCol','pref_i_button_list_type','pref_top_header_theme_bckCol','pref_b_debugSession', 'pref_b_on_filters_start', 'pref_b_filters_location','pref_auto_update_time', 'pref_b_display_sql','b_enabled AS preferencesEnabled']
];


$moduleSetting->offsetStart=0;
$moduleSetting->isErasable=false;
$moduleSetting->isEditable=true;
$moduleSetting->isCanEnabled=false;
$moduleSetting->fieldNameStatusRow='b_enabled';
$moduleSetting->allowNewRecord=true;

// OPTIONAL PARAMS
$moduleSetting->numRecDisplay=1;
$moduleSetting->moduleIcon=isset($arraExtVars['modIcon']) ? $arraExtVars['modIcon'] : 'zmdi-assignment-o';
$moduleSetting->moduleIcon=isset($arraExtVars['module']['modIcon']) ? $arraExtVars['module']['modIcon'] : $moduleSetting->moduleIcon;
//echo $moduleSetting->moduleIcon.'<pre>';print_r($arraExtVars);echo'</pre>';
$order=0;
