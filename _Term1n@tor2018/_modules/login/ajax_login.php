<?php
/**
 * LOGIN BACK-OFFICE CONTROLLER
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();
include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');
include($dirModAdmin.'mod_bbdd.class.php');
include($dirClassesAdmin.'encryption.class.php');

ini_set('display_errors',0);
error_reporting(E_ALL);
$moduleParams='';

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));

if($hashSecutityScript != __HASH_CONTROL_SCRIPT){
  die(__MSG_ACCESS_ERROR);
}

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

if(isset($arraExtVars['loginParams']))
	$moduleParams=json_decode($arraExtVars['loginParams']);

$modeLoadConfig=1;
$localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/login/_mod_bbdd/';
$localModuleFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/login';

$folderCommonModules=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/';
$folderModule=$folderCommonModules.'login'; 
$dataModuleBBDD=null;

include($folderModule.'/config_module.php');
include($localModelsFolder.'module_model.class.php');

$dataModuleBBDD=new ModelLocalModule($db_type, $db_host, $db_user, $db_pass, $db_name, $moduleSetting, $port);
$resConnexion=$dataModuleBBDD->connectDB();

if(!$resConnexion)
	$msgModuleError='<div class="text-center w-100  pull-left"><div class="alert alert-danger p-25 f-16">'.strtoupper(__COMMON_BBDD_TXTCONNECTERROR).'<br><br>'.__COMMON_BBDD_TXTNOCONNECTIONBBDD.'<br><br>'.__COMMON_GENERICTEXT_ERROCONFIGMODULEEND.'<br></div></div>';

$cur_conn_id=$dataModuleBBDD->idConexion;

$tmpUser=mysqli_real_escape_string($cur_conn_id, $arraExtVars['user']);
$tmpPass=mysqli_real_escape_string($cur_conn_id, trim($arraExtVars['password']));
$tmpPass=md5($tmpPass.$clave_acceso);

$resDataForm=@$dataModuleBBDD->loginBackOffice($tmpUser, $tmpPass, @$moduleParams);

if(@$dataModuleBBDD->mError)
{
	$response['msgPainTxt']='<b>ERROR</b><br />';
	$response['errorText']='Ha ocurrido un error en la BBDD al efectuar el login.<br />Por favor, si este error persiste, notifique este error al dep. de informática:<br /><br /><b>'.$dataModuleBBDD->mError.'</b>';
	$response['errorShortText']='Parece que <u>no hay gr&aacute;ficos seleccionados</u> para mostrar';
	$response['successStatus']='danger';
  $response['timeWait']=10000;
	$response['success']=false;
	$res=json_encode($response);
	die($res);
}

if(count($resDataForm))
{
	$str=$arraExtVars['password'];
	$converter=new Encryption;
	$encoded=$converter->encode($str);
	$decoded=$converter->decode($encoded);

	$response['msgPainTxt']='<p class="p-10 f-20 f-700 text-center">BIENVENIDO</p>';
	$response['errorText']='<i>accediendo al panel de control...</i>';
	$response['successStatus']='success';
	$response['success']=true;
  $_SESSION['id']=$resDataForm['idUser'];
  $_SESSION['userAdmin']['userId']=$resDataForm['idUser'];
  $_SESSION['userAdmin']['currPass']=$encoded;
  $_SESSION['userAdmin']['name']=$resDataForm['name'];
  $_SESSION['userAdmin']['userName']=$arraExtVars['user'];
  $_SESSION['userAdmin']['surname']=$resDataForm['surname'];
  $_SESSION['userAdmin']['roleId']=$resDataForm['roleId'];
  $_SESSION['userAdmin']['email']=$resDataForm['email'];
  $_SESSION['userAdmin']['theme']=$resDataForm['theme'];
  $_SESSION['userAdmin']['pref_']['iconUser']=@$resDataForm['profile_image'];
	$_SESSION['userAdmin']['firstHomeLoad']=true;

  if(!$resDataForm['preferencesEnabled'])
    unset($_SESSION['userAdmin']['pref_']);

  foreach($resDataForm as $key=>$value) // STORE ALL USER PREFERENCES INTO CURRENT SESSION
  {
    if(substr($key,0,5) != 'pref_')
     continue;

    $_SESSION['userAdmin']['pref_'][substr($key,5)]=$value;
  }

  if(isset($_SESSION['userAdmin']['pref_']))
  {
    foreach($_SESSION['userAdmin']['pref_'] as $key=>$value) // FIND COLORS USER PREFERENCES
    {
      $colorType=substr($key,-6);

      if($colorType != 'forCol' && $colorType != 'bckCol')  // SOLO PARA ASIGNACION DE COLORES
        continue;

      $colorType=$colorType == 'forCol' ? 'c-' : 'bgm-';
      $_SESSION['userAdmin']['pref_'][$key]=(isset($value) && $value !='') ? ($resDataForm['preferencesEnabled'] ? $colorType.$value : $colorType.$arrDefaultColor[$key]) : $colorType.$arrDefaultColor[$key];
    }
  }

  unset($_SESSION['admUser']);

  $_SESSION['encrypted_user_id']=md5($_SESSION['userAdmin']['name']);

  if($_POST['keepmelogin'] === 'true')
  {
    setcookie('cookname', $_SESSION['userAdmin']['name'], time()+60*60*24*100, '/');
    setcookie('cookid', $_SESSION['encrypted_user_id'], time()+60*60*24*100, '/');
    setcookie('cooktype', $_SESSION['userAdmin']['roleId'], time()+60*60*24*100, '/');
  }
  $dataModuleBBDD->disconnectDB();
}
else
{
  unset($_SESSION['id']);
  unset($_SESSION['user']);
  session_destroy();
  header('Location:../index.php');
}

$response['res']=true;
$res=json_encode($response);
die($res);