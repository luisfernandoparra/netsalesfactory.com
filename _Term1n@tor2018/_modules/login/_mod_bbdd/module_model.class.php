<?php
/**
 *	LOGIN MODULE - SQL DATA
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class ModelLocalModule extends CBBDD
{
  public $database;
  public $query;
  public $mainTable;
  public $mainTableFieldsNames;
  public $defaultWhereSql;

  public function __construct($db_type=null, $db_host='', $db_user='', $db_pass='', $db_name='', $localParams=null, $port=1312)
  {
//echo $port;
    $this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
    $this->prefix=$localParams->prefixTbl;
    $this->query='';
    $this->mainTable=$localParams->prefixTbl.$localParams->mainTable;
    $this->listFieldsOutput=''; 
    $this->mainTableFieldsNames=''; 
    $this->defaultWhereSql='';
    $this->defaultOrderField='';
    return;
  }

  public function loginBackOffice($user='', $password='', $moduleParams=null)
  {
    $onlyEnabledUsers='';

    if(isset($moduleParams->arrFieldsOutput) && count($moduleParams->arrFieldsOutput))  // ALL FIELDS TO LOAD DATA
    {
      $fieldNames='';
      $count=0;
      foreach($moduleParams->arrFieldsOutput as $prefix=>$tblPrefix)
      {
        if(!$count)
          $onlyEnabledUsers=' && '.$prefix.'.'.$moduleParams->fieldNameStatusRow;

        foreach($tblPrefix as $fieldNameTbl)
        {
          $fieldNames.=$prefix.'.'.$fieldNameTbl.(!$count ? ' AS idUser' : '').',';
          $count++;
        }
      }
      $fieldNames=substr($fieldNames,0,-1);
    }

    if(isset($moduleParams->additionalTables) && count($moduleParams->additionalTables))  // ALL SQL TABLES
    {
      $tables=array();
      foreach($moduleParams->additionalTables as $tblPrefix=>$tableName)
        $tables[]=$tableName.' AS '.$tblPrefix;
    }

    $arrTablesPrefix=(array)$moduleParams->additionalTables;
    $prefixMaster=array_slice($arrTablesPrefix, 0, 1, true);
    $prefixInner=array_slice($arrTablesPrefix, 1, 1, true);
    $prefixMaster=array_keys($prefixMaster);
    $prefixInner=array_keys($prefixInner);

    $query='SELECT %s FROM `%s`.%s LEFT JOIN `%s`.%s ON %s.%s=%s.%s WHERE %s.%s = \'%s\' && %s.%s = \'%s\' %s';
    $this->query=sprintf($query, $fieldNames, $this->db, $this->prefix.$tables[0], $this->db, $this->prefix.$tables[1], $prefixMaster[0], $moduleParams->keyListLabel, $prefixInner[0], $moduleParams->keyInnerLeft, $prefixMaster[0], $moduleParams->userFieldName, $user, $prefixMaster[0], $moduleParams->passFieldName, $password, $onlyEnabledUsers);
//echo $this->query;die();
		$this->getResultSelectArray($this->query);
    $this->rowData=$this->tResultadoQuery;
		@$this->rowData=$this->rowData[0];
		$resLog=$this->rowData['idUser'] ? $this->rowData['idUser'] : -1;
		$this->logBackOffice($resLog);
    return $this->rowData;
  }
}