<?php
@header('Content-Type: text/html; charset=utf-8');
@session_start();
require('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');
$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));

if($hashSecutityScript != __HASH_CONTROL_SCRIPT){die(__MSG_ACCESS_ERROR);}

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$cookieId = $_SERVER['SERVER_NAME'] . $adminFolder;

if(!empty($_COOKIE[$cookieId]) && !empty($_COOKIE[$cookieId . '-data']))
{
	$data = json_decode($_COOKIE[$cookieId . '-data'], true);
	foreach ($data as $key => $value){
		$_SESSION[$key] = $value;
	}
	header('Location: ../../index.php');
}

$modeLoadConfig=1;
$folderCommonModules=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/';
$folderModule=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/login';

@include('config_module.php');

if(!isset($moduleSetting))
{
  echo '<br /><br /><div class="p-20 m-20 f-16 alert alert-danger"><center><span class="zmdi zmdi-info f-20"></span> No existe o no se puede abrir el archivo de configuración para el modulo <b>LOGIN</b>.</center><br /><br />Por favor, contacta con el departamento de inform&aacute;tica.</div><br /><br />';//die();
  $moduleSetting=array();
}

$moduleParams=json_encode($moduleSetting);
//echo '<pre>';die();;print_r($moduleSetting);
$folderCommonModules=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_modules/';
$folderModule=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/';//.$arraExtVars['moduleAdmin'];
$folderWebAdmin=$web_url.$adminFolder.'/';
//echo $folderWebAdmin.'<hr>LOGIN: = '.$hashSecutityScript.'<hr>'.__HASH_CONTROL_SCRIPT.'<hr>ROOT::/_Term1n@tor2018/index.php ---->'.$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php';die();
?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Back-office <?=($sitesPruebas ? ' '.$siteTitle.$siteVersion : $siteTitle.$siteVersion)?></title>

<!-- Vendor CSS -->
<!--Del login-->
<link href="<?=$folderWebAdmin?>vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
<link href="<?=$folderWebAdmin?>vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
<!-- CSS -->
<link href="<?=$folderWebAdmin?>css/app_1.min.css" rel="stylesheet">
<link href="<?=$folderWebAdmin?>css/app_2.min.css" rel="stylesheet">

<!--De los errores-->
<!--<link href="<?=$folderWebAdmin?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">-->
<link href="<?=$folderWebAdmin?>vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">   

<script src="<?=$folderWebAdmin?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=$folderWebAdmin?>js/app.min.js"></script>
<script src="<?=$folderWebAdmin?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?=$folderWebAdmin?>vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?=$folderWebAdmin?>vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?=$folderWebAdmin?>vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
<script src="<?=$folderWebAdmin?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?=$folderWebAdmin?>vendors/bower_components/autosize/dist/autosize.min.js"></script>

<script src="<?=$folderWebAdmin?>js/lib/jquery.validation.min.js"></script>
<script src="<?=$folderWebAdmin?>js/lib/jquery.additionalMethods.min.js"></script>
<script src="<?=$folderWebAdmin?>vendors/bower_components/Waves/dist/waves.min.js"></script>

<style>
</style>
<script type="text/javascript">
var root_path="<?=$web_url.$adminFolder?>/";
</script>
</head>
<body>
<div class="login-content <?=$sitesPruebas ? 'bgm-black' : 'bgm-teal'?>">
		<!-- Login -->
		<div class="lc-block toggled" id="l-login">
				<h2 class="col-xs-12 color-block bgm-<?=$sitesPruebas ? 'deeppurple' : 'blue'?>  bs-item z-depth-2-top" style="padding:20px;">Back-office Landings Netsales<?=$sitesPruebas ? ' (DEV)' : ''?></h2><br />
				<form  class="floating-label"  role="form"  id='formValidate'>
						<div class="lcb-form">
								<div class="input-group m-b-20d">
										<span class="input-group-addon "><i class="zmdi zmdi-account"></i></span>
										<div id='user-div' class="fg-line control-group">
												<input type="text" id='user' name='user' class="form-control" placeholder="User" required="required" value="<?=(isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '')?>" />
										</div>
								</div>
								<div class="input-group m-b-20 form-group">
										<span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
										<div id='password-div' class="fg-line control-group">
											<input type="password" id='password' name='password' class="form-control" placeholder="Password" required="required" value="<?=(isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '')?>" />
										</div>
								</div>
								<div class="input-group m-b-20 form-group">
								<!--<span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
										 <div class="fg-line control-group">
												 <input type="text" id="email2" name="email2" class="form-control" placeholder="Email Address" required>-->
										 <!--<small class="help-block with-errors"></small>-->
										 <!--</div>-->
								</div>
<!--                    <div class="checkbox">
										<label>
												<input type="checkbox" value="1" id='keepmelogin' name='keepmelogin'>
												<i class="input-helper"></i>
												Mantenme Conectado
										</label>
								</div>-->
								<div class="">
									<p><i class="input-helper"></i>
										Por favor, indique sus datos de acceso
									</p>
									<p>ver. <?=$versionBackOffice?></p>
								</div>
								<a href="" class="btn btn-login btn-success btn-float" onclick="javascript: validateLogin();return false;">
										<!--onclick="javascript: validateLogin();return false;"-->
										<i class="zmdi zmdi-arrow-forward"></i>
								</a>
						</div>
				</form>
<!--            <div class="lcb-navigation">
					<a href="" data-ma-action="login-switch" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>
						<a href="" data-ma-action="login-switch" data-ma-block="#l-forget-password"><i>?</i> <span>Recordar Password</span></a>
				</div>-->
		</div>
		<!-- Register -->
		<div class="lc-block" id="l-register">
				<div class="lcb-form">
						<div class="input-group m-b-20">
								<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
								<div class="fg-line">
										<input type="text" class="form-control" placeholder="Usuario">
								</div>
						</div>
						<div class="input-group m-b-20">
								<span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
								<div class="fg-line">
										<input type="text" class="form-control" placeholder="Email Address">
								</div>
						</div>
						<div class="input-group m-b-20">
								<span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
								<div class="fg-line">
										<input type="password" class="form-control" placeholder="Password">
								</div>
						</div>
						<a href="" class="btn btn-login btn-success btn-float"><i class="zmdi zmdi-check"></i></a>
				</div>
				<div class="lcb-navigation">
						<a href="" data-ma-action="login-switch" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign in</span></a>
						<a href="" data-ma-action="login-switch" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
				</div>
		</div>
		<!-- Forgot Password -->
		<div class="lc-block" id="l-forget-password">
				<div class="lcb-form">
						<p class="text-left">Contacta con Sistemas para Regenerarte la Password.<br /> Gracias</p>
						<div class="input-group m-b-20">
						<!--<span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
								<div class="fg-line">
										<input type="text" class="form-control" placeholder="Email Address">
								</div>-->
						</div>
						<a href="" class="btn btn-login btn-success btn-float"><i class="zmdi zmdi-check"></i></a>
				</div>
				<div class="lcb-navigation">
						<a href="" data-ma-action="login-switch" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign in</span></a>
						<!--<a href="" data-ma-action="login-switch" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>-->
				</div>
		</div>
</div>
<?php
// OLDER I-EXPLORER HTML
include($folderModule.'_inc/index_older_iexplorer.php');
?>
<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
<script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
<![endif]-->

<script type="text/javascript">
/**
 * Elementos obligatorios a rellenar
 * @type Array
 */
var val_elements = ['user', 'password'];

$(document).ready(function(){
	clearError();
	$("#user").focus();
	$('#formValidate').validate({
		rules: {
			user: {
				minlength: 3,
			},
			password:{
				minlength: 5
			}
		},
		messages: {
			user: "Introducir el nombre de usuario",
			password: "La contraseña es obligatoria",
		},
		errorElement: 'small',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
//console.log('error',error,element);
			id = element.attr('id');
			$(element).closest('.control-group').removeClass('has-success').addClass('has-error');
			error.insertAfter($('#'+id));
		},
		highlight: function (element) {
//console.log('hightligt',element);
			$(element).closest('.control-group').removeClass('has-success').addClass('has-error');
		},
		success: function (element) {
//console.log('success',element);
			element.addClass('has-success').closest('.control-group').removeClass('has-error').addClass('has-success');
		}
	});
});

function clearElement(elem){
    $('#' + elem + '-div').removeClass("has-error");
    $('#' + elem + '-msgerror').hide();
}

function clearError(){
  for (i = 0; i < val_elements.length; i++) {
		clearElement(val_elements[i]);
  }
}

$(document).keypress(function(e){
	if(e.which == 13) {
			validateLogin();
	}
});

function validateLogin(){
	var error = false;
	clearError();
  for (i = 0; i < val_elements.length; i++) {
   	elem = val_elements[i];
		data = $('#' + elem).val();
		if (data == '') {
			error = true;
			$('#' + elem + '-div').addClass("has-error");
			$('#' + elem + '-msgerror').show();
		}
  }
  if(!error)
	{
		var otherParams=JSON.stringify(<?=$moduleParams?>);
		$.ajax({
		url:"<?=$web_url.$adminFolder?>/_modules/login/ajax_login.php", method: "post", dataType: "json",
		data:{
				user: $('#user').val(),
				password: $('#password').val(),
				keepmelogin: $("#keepmelogin").is(':checked'),
				action: "login",
				loginParams:otherParams
			},
			cache: false,
			async: true,
			success: function(response)
			{
				if(!response.success)
				{
					commonNotify(response.msgPainTxt, response.errorText ? response.errorText : "", undefined, "center", undefined, response.successStatus, "animated flipInX", "zoomOut", response.timeWait ? response.timeWait : 2000);
					return false;
				}

				var uno = $('#nada').attr('nada');
				commonNotify(response.msgPainTxt, response.errorText ? response.errorText : "accediendo...", undefined, "center", undefined, response.successStatus, "animated flipInX", "zoomOut", response.timeWait ? response.timeWait : 2000);
				setTimeout(function(){document.location='<?=$folderWebAdmin?>index.php';},1500);
					return false;
			},
			error: function (response) {
				commonNotify("<p class='p-10 f-20 f-700 text-center'>ERROR</p>", "Usuario o password Incorrectos", undefined, "center", undefined, 'danger', "animated flipInY", "rotateOut");	// MENSAJE NO INVASIVO
				return false;
			}
		});
	}
	return false;
}
<?php
if(isset($_SESSION['currentModuleUrl']) && $_SESSION['currentModuleUrl'])
{
	echo '
$("#user").val("");$("#password").val("");
setTimeout(\'validateLogin();\',0);
';
}
else if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
{
	echo '
validateLogin();//setTimeout(\'$("#user").val("");$("#password").val("");\',200);
';
}
?>

</script>
</body>
</html>
<?php
//echo'<pre>';print_r($_SESSION);print_r($_SERVER);echo'</pre>';//die();