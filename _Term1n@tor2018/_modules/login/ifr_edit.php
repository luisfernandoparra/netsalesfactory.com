<?php
/**
 * EDIT MODULE login
 *
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

include('../../../conf/config_web.php');
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'conf/config.misc.php');

ini_set('display_errors',1);
error_reporting(E_ALL);
if(empty($_SESSION['id'])){echo '<script>parent.document.location="'.$web_url.$adminFolder.'/_modules/login/";</script>';}

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
if($hashSecutityScript != __HASH_CONTROL_SCRIPT) die(__MSG_ACCESS_ERROR);

$localModelsFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'].'_mod_bbdd/';
$localModuleFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['from'];

include($localModuleFolder.'config_module.php');	// LOCAL MODULE CONFIGURATION PARAMS

$msgModuleError=null; $dataModuleBBDD=null;
$iframeAction=ucfirst($arraExtVars['action']);  // NORMALIZA EL NOMBRE DE LA ACCION PARA IDENTIFICAR EL ELEMENTO A MANEJAR

/**
 *	START CARGA DE DATOS PROPIOS DEL REGISTRO A EDITAR/INSERTAR
 */
include($dirModAdmin.'mod_bbdd.class.php');
include($dirModAdmin.'mod_common_edit.class.php');
include($localModelsFolder.'module_model.class.php');

$dataModuleBBDD=new CommonEditingModel($db_type, $db_host, $db_user, $db_pass, $db_name, $port, $moduleSetting);
$resConnexion=$dataModuleBBDD->connectDB();

if(!$resConnexion)
{
	$msgModuleError='<div class="text-center w-100  pull-left"><div class="alert alert-danger p-25 f-16">'.strtoupper(__COMMON_BBDD_TXTCONNECTERROR).'<br><br>'.__COMMON_BBDD_TXTNOCONNECTIONBBDD.'<br><br>'.__COMMON_GENERICTEXT_ERROCONFIGMODULEEND.'<br></div></div>';
}

/**
 * SE OBTIENEN LOS DATOS RELATIVOS AL REGISTRO EDITADO (campos standard)
 *
 * $resDataForm ES EL ARRAY CON LOS DATOS DEL ACTUAL REGISTRO
 *
 */
if(isset($arraExtVars['id']))
	$resDataForm=$dataModuleBBDD->getCommonRecordData((int)$arraExtVars['id'], (isset($arraExtVars['foreignKey']) ? $arraExtVars['foreignKey'] : ''));

$dataLocalModel=new ModelLocalModule($dataModuleBBDD);
//	END CARGA DE DATOS PROPIOS DEL REGISTRO A EDITAR/INSERTAR


/**
 * FUNCIONES COMUNES PARA LOS FORMULARIOS DE LOS MODULOS DEL BACK-OFFICE
 */
include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_classes/common_forms_methods.class.php');
$objForm=new CommonModulesMethods();

// HTML (HEADER/TOP) + ACCIONES COMUNES JS PARA LA EDICION DEL MODULO ACTUAL
include('../common_edit_html_top.php');

if($resConnexion)
{
  $arrGroups=array();
	$countPos=0;

  foreach($moduleSetting->arrFieldsFormModal as $fieldname=>$value)  // SE RECORREN TODOS LOS CAMPOS A DIBUJAR
  {
    $tmpFieldGroupValue=isset($moduleSetting->arrFieldsFormModal[$fieldname]['formGroup']) ? $moduleSetting->arrFieldsFormModal[$fieldname]['formGroup'] : $tmpFieldGroupValue;

		foreach($value as $ff=>$vv) // SE AGRUPAN SEGUN EL "formGroup" QUE TIENEN ASIGNADO
			$arrGroups[$tmpFieldGroupValue][$fieldname][$ff]=$vv;

    if(@$moduleSetting->arrGoupsForm[$countPos])
    {
//echo'<hr><pre>';print_r($moduleSetting->arrGoupsForm);echo'</pre>';//return;
			$arrGroups[$countPos]['sys_configGroupForm']=$moduleSetting->arrGoupsForm[$countPos];
			unset($moduleSetting->arrGoupsForm[$countPos]);
    }

		$countPos++;
  }
//echo'<hr><pre>';print_r($arrGroups);echo'</pre>';//return;


  /**
   * RECORRER TODOS LOS CAMPOS A COLOCAR EN EL FORMULARIO
   * PARA CREAR EL BOX CORRESPONDIENTE
   */
  $outFormBoxField="\r\n\t".'<div class="card-body card-padding">';

	$count=0;
	$jsForm='';	// JS QUE SE CREA DINAMICAMENTE EN LA CARGA DE LOS CAMPOS QUE FORMAN EL FORMULARIO DE EDICION / INSERCION

	foreach($arrGroups as $refGroup=>$groupFields)
	{
		if($refGroup)	// SE OMITE COMPONER ESTOS TAGS A LOS ELEMENTOS PERTENECIENTES AL GRUPO 0
		{
			$outFormBoxField.="\r\n\t".'<div class="row">';
			$outFormBoxField.="\r\n\t\t".'<div class="boxField form-group bs-item z-depth-1 col-sm-12 p-20">';
		}

		if(isset($groupFields['sys_configGroupForm']) && count($groupFields['sys_configGroupForm']))	// CABECERA DEL GRUPO DEL FORMULARIO
		{
			$outFormBoxField.=$objForm->buildGroupHeader($groupFields['sys_configGroupForm'], $count);
		}

		if($refGroup)
			$outFormBoxField.="\r\n\t\t".'<div class="containerGroup" data-ref="'.$count.'">';

		foreach($groupFields as $fieldName=>$paramField)
		{
			$resHTML='';

			if(in_array($fieldName, $arrEnabledFieldStatus) && ($_SESSION['userAdmin']['roleId'] >= $systemData->minStdEnableRole) && isset($arraExtVars['id']))	// SOLO PARA LA EDICION DE REGISTROS
			{
				$jsForm.='setButtonEnableDisable('.(int)@!$resDataForm[$fieldName].', '.$arraExtVars['id'].', "'.$fieldName.'");';
			}

			if(isset($paramField['excludeAutoForm']) && $paramField['excludeAutoForm'])
				continue;

			$defaultFormType=isset($paramField['formType']) ? $paramField['formType'] : 'text';
			$defaultLabel=isset($paramField['label']) ? $paramField['label'] : '';
			$defaultPlaceHolder=isset($paramField['placeHolder']) ? $paramField['placeHolder'] : '';
			$methodName=str_replace('-', ' ', $defaultFormType);
			$methodName=ucfirst($methodName);
			$methodName=str_replace(' ', '', $methodName);
			$methodName='buildForm'.$methodName;

			if($defaultFormType != 'hidden')
			{
				if(method_exists($objForm, $methodName) && isset($paramField['visible']) && $paramField['visible'])	// CONSTRUCCION HTML DEL ELEMENTO DEL FORMULARIO
				{
					if(isset($dataLocalModel) && count(@$paramField['arrFieldsTagSelect']))
						$resDataForm['select='.$fieldName]=$dataLocalModel->getSpecificRecordData((int)@$arraExtVars['id'], @$paramField['fieldTableName'], $paramField['arrFieldsTagSelect'], $paramField['whereSelectField'], $paramField['orderSelectField']);

					$resHTML=$objForm->$methodName($fieldName, $paramField, @$resDataForm, $moduleSetting->mainTable);
				}

				$outFormBoxField.=$resHTML;
			}
			else	// HIDDEN FIELDS
			{
				$outFormBoxField.="\r\n\t\t\t\t".'<input type="'.$defaultFormType.'" name="db['.$moduleSetting->mainTable.']['.$fieldName.']" value="'.@$resDataForm[$fieldName].'" />';
			}
			$count++;
		}

		if($refGroup)	// SE OMITE COMPONER ESTOS TAGS A LOS ELEMENTOS PERTENECIENTES AL GRUPO 0
		{
			$outFormBoxField.="\r\n\t\t\t".'</div>';
			$outFormBoxField.="\r\n\t\t".'</div>';
			$outFormBoxField.="\r\n\t".'</div>';
		}
	}

  $outFormBoxField.="\r\n".'</div>';
}

?>

<script type="text/javascript">
<!--
/**
 * ***** initializeModule() :: MUY IMPORTANTE *****
 * para la visualizacion correctamente el MODULO DE EDICION/INSERCION:
 *
 * REDIMENSIONA EL ALTO DEL IFRAME CONTENEDOR DEL FORMULARIO DE INSERCION DE DATOS
 * SEGUN LA ALTYRA ACTUAL DE LA CAJA CONTENEDORA
 */
function initializeModule()
{
  $(".contentForm").show();
  parent.$("#ifrm<?=$iframeAction?>Record").css("height",parent.$("#boxContent<?=$iframeAction?>Record").css("height"));parent.$("#boxContent<?=$iframeAction?>Record").show();
	return;
}

$(document).ready(function(){
	/**
	 * ACCION DE EDICION / INSERCION DE REGISTRO
	 */
	$('#moduleMainForm').on('submit', function(e){
		e.preventDefault();
		parent.$(".actCancel").attr("disabled","disabled");
		parent.$(".saveButton").attr("disabled","disabled");
		parent.$(".enableDisableButton").attr("disabled","disabled");
		$("#modalWiderUpdating").click();	// OPEN MODAL INFO OPERATION

		if(("mainKeyId=",$("input[name=mainKeyId]").val() > 0))
			$(".waitMessage").addClass("bgm-lightblue");

		$(".waitMessage").html(("mainKeyId=",$("input[name=mainKeyId]").val() > 0) ? "<br /><br /><p>Actualizando el registro, referencia: <span class='c-black c-back bgm-white p-r-5 p-l-5'>"+$("input[name=mainKeyId]").val()+"</span><br /><br /></p><center><div class='preloader pls-yellow'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div></center><br />" : "Insertanto el nuevo registro....<br /><br /><center><div class='preloader pls-white'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div></center><br />");
		var moduleFields=$(this).serialize();

		$.ajax({
		data: moduleFields ,url: root_path + "ajax/common_admin.php", method: "post", dataType: "json", cache: false, async: true
		,success:function(response){
			var tmpSql=response.query.sql ? response.query.sql : 0;
			if(tmpSql) window.parent.commonNotify("","<br />"+tmpSql+"<br />", undefined, undefined, undefined,( response.success ? "info" : "danger"), undefined, undefined, ( response.success ? 6000 : 20000));
			processStatus=response.success;

			if(!processStatus)
			{
				commonRestoreErrorForm(response,0);
				return false;
			}

			window.parent.commonNotify(response.msgTitle ? response.msgTitle : "<?=__COMMON_NOTIFY_CORRECTOPERATION?>", response.msgText ? "<br />"+response.msgText : "", undefined, "center", undefined, 'success', "animated flipInX", undefined);
			parent.$(".saveButton").attr("disabled",false);
			parent.$(".enableDisableButton").attr("disabled",false);
			parent.$(".actCancel").attr("disabled",false);
			parent.$(".actCancel").click();
//			$('#bbddOperation').modal('toggle');

//			// MESSEGE NEW RECORD STATUS
//			window.parent.parent.commonNotify('OK!', "", undefined, undefined, undefined, 'success', undefined, undefined);
			return;
		}
		,error:function(response){
				commonRestoreErrorForm(response,1);
				return false;
			}
		});



		// START PRUEBAS
//		var res=confirm("continuar?")
//		if(res)
//		{
//			var moduleFields=$(this).serialize();
//console.log("ACCION:: submit del formulario abierto....");console.log(moduleFields);
//			parent.$(".actCancel").click();
////			this.submit();
//		}
		// END PRUEBAS

		return false;
	});

	parent.$(".headerAdditionalInfo").html("");

	$(window).load(function(){
    parent.setTimeout('$(".saveButton").fadeIn();',300);
		first_input=$('input[type=text], select')[0];
		if(first_input != undefined){setTimeout("setFieldFocus();",400)};
<?php

if($arraExtVars['action'] == 'edit') // DIBUJA EL TITULAR DE LA VENTANA MODAL DE EDICION
{
  echo 'parent.$(".headerAdditionalInfo").removeClass("hide").html("- ID: <span class=\'text-uppercase\'>'.@$arraExtVars['id'].'</span>");';
//    echo 'parent.$("#ifrm'.$iframeAction.'Record").css("height",parent.$("#boxContent'.$iframeAction.'Record").css("height"));parent.$("#boxContent'.$iframeAction.'Record").show();';
}

if($arraExtVars['action'] == 'new')
{
//    echo 'parent.$("#ifrm'.$iframeAction.'Record").css("height",parent.$("#boxContent'.$iframeAction.'Record").css("height"));parent.$("#boxContent'.$iframeAction.'Record").show();';
}

if(strlen($jsForm))	// SOLO SI SE HAN CREADO INSTRUCCIONES JS, SE PINTAN EN EL FORMULARIO
{
echo '
parent.setTimeout(\'$(".enableDisableButton").fadeIn();\',600);
';

echo $jsForm;
}

?>
    setTimeout("initializeModule()",100);
	});

});

function setButtonEnableDisable(statusToSet, idRecord, fieldName)
{
	parent.$(".enableDisableButton").html(statusToSet ? "<i class='zmdi zmdi-check'></i>&nbsp;&nbsp;<?=__COMMON_BTN_ENABLERECORD?>" : "<span class='ORANGE'>&nbsp;<i class='zmdi zmdi-block'></i>&nbsp;<?=__COMMON_BTN_DISABLERECORD?>&nbsp;</span>");
	parent.$(".enableDisableButton").attr("data-id", idRecord);
	parent.$(".enableDisableButton").attr("data-field-name", fieldName);
	parent.$(".enableDisableButton").attr("data-status", statusToSet);

	if(statusToSet = 0)
		parent.$(".enableDisableButton").addClass("bgm-orange");

	return;
}
-->
</script>
</head>

<body>

<div class="contentForm card" style="display:none;">
  <form action="" method='post' id='moduleMainForm' name='moduleMainForm' onsubmit='//return false;' enctype='multipart/form-data'>
		<input name="mainKeyId" type="hidden" value="<?=(int)@$arraExtVars['id']?>" />
		<input name="mainKeyName" type="hidden" value="<?=$moduleSetting->keyListLabel?>" />
		<input name="action" type="hidden" value="<?=isset($arraExtVars['id']) ? 'update' : 'insert'?>" />

<?php
if($msgModuleError)
{
  /**
   * ERROR EN LA CARGA DE LOS DATOS
   */
	die($msgModuleError);
}
else
{
	// SE DIBUJAN LOS ELEMENTOS DEL FORMULARIO A MOSTRAR
	echo $outFormBoxField;
?>
		<input id="tmpSubmit" name="tmpSubmit" type="submit" class="hide" />
  </form>

<?php
//echo '<pre>';
//print_r($objForm);
//print_r($moduleSetting->arrFieldsFormModal);
//print_r(@$arraExtVars);
//print_r($dataModuleBBDD);
//print_r($moduleSetting->arrGoupsForm);
//print_r($arrGroups);
//print_r($moduleSetting->arrFieldsFormModal);
//echo '</pre>';
}

// TESTS...
//for($n=0;$n<rand(1,10);$n++){echo '<pre>block-test:'.$n.'<p>';print_r($arraExtVars);echo '</pre>';}
?>
</div>
<a id="modalWiderUpdating" data-toggle="modal" href="#bbddOperation" class="hide" title="" >BBDD-operation</a>

<div class="modal fade" data-modal-color="blue" id="bbddOperation" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><i><?=__COMMON_MODALMSG_UPDATINGOPERATIONTITLEH4?></i></h4>
			</div>
			<div class="modal-body waitMessage">
					<p><?=__COMMON_MODALMSG_WAITMESSAGE?><span class="idTitleRef"></span></p>
			</div>
		</div>
	</div>
</div>

</body>
</html>