<?php
/**
 * LISTING MODULE IFRAME
 * LIST RECORD DATA (PART 3/3)
 *
 * OBJETIVOS
 * 1. OBTENICION DE LOS DATOS A VISUALIZAR DESDE LA BBDD
 * 2. GENERAR EL LISTADO PARA EL MODULO ABIERTO
 * 3. DIBUJAR LOS CONTROLES PARA EDITAR - ACTIVAR/DESACTIVAR - BORRAR REGISTROS
 *
 * @version 1.2
 * @date 2016.10.05
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();
include('../conf/config_web.php');
include('../conf/config.misc.php');
$minExtension= $sitesPruebas ? '' : '';
$filterSearchListIdentifier='(+)';	// STRING IDENTIFICADORA UTILIZADA PARA IDENTIFICAR LOS CAMPOS QUE DEBEN TENER FILTROS DE BUSQUEDA
$filterSearchListIdentifierLen=strlen($filterSearchListIdentifier);

ini_set('display_errors',$displayErrors);
error_reporting($errorReportDefault);

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
if($hashSecutityScript != __HASH_CONTROL_SCRIPT) die(__MSG_ACCESS_ERROR);

ini_set('display_errors', 1);

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

/**
 * START CODE FOR MANIPULATION MODE ON EVENTS COLUNMS GRID FILTERS
 * AND POSITION ON SCREEN
 *
 * 0 = MOUSE MOVE
 * 1 = RIGHT CLICK
 */
$locationColumFilters=(isset($_SESSION['userAdmin']['pref_']['b_filters_location']) && $_SESSION['userAdmin']['pref_']['b_filters_location']) ? '.gridFilters' : '.card-header';
$modeOnColumFiltersStart='$("#grid-data-api th").on("mousemove", "span", function(e){';
$modeOnColumFiltersEnd='},1200);';
$rightClickFilterEnabled=false;

if(isset($_SESSION['userAdmin']['pref_']['b_on_filters_start']) && $_SESSION['userAdmin']['pref_']['b_on_filters_start'])
{
	$modeOnColumFiltersStart='	$("#grid-data-api th").on("mousedown", "span", function(e){
		e.preventDefault();
		if(e.which !== 3){return;}
';
	$modeOnColumFiltersEnd='return false;},200);';
	$rightClickFilterEnabled=true;
}
// END CODE FOR MANIPULATION MODE ON EVENTS COLUNMS GRID FILTERS
?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php
// START COMMON AND VENDOR CSS & JAVASCRIPT COMMON LIBRARIES
include('_inc/index_comm_vendor_CSS.php');
include('_inc/index_javascript_libraries.php');
// END COMMON AND VENDOR CSS & JAVASCRIPT COMMON LIBRARIES
?>

<link href="vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
<script src="vendors/bootgrid/jquery.bootgrid.updated<?=$minExtension?>.js"></script>

<?php
if(!count(@$arraExtVars['configDataList']))	// SOLO SI SE RECIBEN LOS PARAMETROS PARA CONFIGURAR EL MODELO DE DATOS A CARGAR
{
	echo '<div class="alert alert-danger alert-dismissible"><center><p class="">'.__COMMON_BBDD_TXTNODATAERROR.'</div></p><hr></head><body></body>';
	die();
}

$configDataList=@$arraExtVars['configDataList'];
include($dirModAdmin.'mod_bbdd.class.php');
include($dirModAdmin.'mod_common_lists.class.php');
$dataList=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name, $configDataList,$port);
$resConnexion=$dataList->connectDB();

if(!$resConnexion)
	die('<center><h2>'.__COMMON_BBDD_TXTCONNECTERROR.'</h2></center>');

include($dirClassesAdmin.'table_list.class.php');
$tableObj=new ListTableElements();

$localModuleFolder=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['modulePath'];
include($localModuleFolder.'config_module.php');	// LOCAL MODULE CONFIGURATION PARAMS
//echo '$tmpArr=<pre style="display:inline-block;width:100%;height:100%;">';print_r($arrJsonTableFields);echo'</pre>';

// DEFINICION DE LAS VARIABLES NECESARIAS PARA OBTENER EL LISTADO EN EL MODULO ACTIVO
$dataList->defaultOrderField=@$moduleSetting->orderField;	// CAMPO POR DEFECTO PARA LA ORDENACION DEL LISTADO
$dataList->arrFieldsOutput=@$moduleSetting->arrFieldsOutput;	// ARRAY CON LOS NOMBRES DE LOS CAMPOS A UTILIZAR COMO HEADER LIST EN EL LISTADO
$dataList->defaultWhereSql=@$moduleSetting->defaultWhereSql;	// CLAUSULA SQL PARA EL "WHERE" DEL MODULO SIEMPRE PRESENTE
$dataList->fieldNameStatusRow=@$moduleSetting->fieldNameStatusRow;	// NOMBRE DEL CAMPO QUE HABIL./DESHABI. EL REGISTRO DE LA TABLA MAESTRA
$dataList->columIndexTable=@$moduleSetting->columIndexTable;
$skipBdDefault=(isset($moduleSetting->skipBdDefault) && $moduleSetting->skipBdDefault) ? 1 : (int)@$dataList->skipBdDefault;	// OMITIR EL PREFIJO DE LAS TABLAS + EL NOKMBRE DE LA BBDD EN LA QUERIE

$objModuleFields=json_decode(json_encode($moduleSetting->arrFieldsFormModal), false);
$dataList->jsonFields=json_encode($moduleSetting->arrFieldsFormModal);

$arrheaderTable=array();
$arrUnsortedheaderTable=array();
$arrTabularColumnWidths=array();	// ANCHO DE COLUMNAS, VALOR PARA LA CLASE A APLICAR (col-sm-*)
$listFieldsOutput='';

if(isset($moduleSetting->mainTable))
{
	$dataList->arrFieldsOutput=null;
	$objModuleData=$dataList->listDataAndHeaders($moduleSetting->mainTable, $skipBdDefault);	// CABECERAS DEL LISTADO + REGISTROS A LISTAR

	if(count($objModuleFields))
	{
		$arrFieldsOutput='';
		$jsArrFieldFilter='';

		foreach($objModuleFields as $key=>$value)
		{
      // ESTE PARAMETRO DETERMINA LOS CAMPOS (Y EL ORDEN) DE APARICION PARA EL LISTADO TABULAR
			if(isset($value->tabularListData->columnOrder) && $value->tabularListData->columnOrder)
			{
				$arrFieldsOutput.=$key.',';	// NOMBRES DE CAMPOS DEL LISTADO
        // RECOLOCACION DE LAS ETIQUETAS DE LA CABECERA DEL LISTADO
				$arrUnsortedheaderTable[$value->tabularListData->columnOrder][$key]=$value->label ? $value->label : $key;
			}

			if(isset($value->tabularListData->columnWidth) && $value->tabularListData->columnWidth)
				$arrTabularColumnWidths[$key]=$value->tabularListData->columnWidth;

      if(isset($value->tabularFilter))  //  SI EXISTE EL PARAMETRO PARA CRAR UN FILTRO DE BUSQUEDA, SE INICIALIZA EL ARRAY CORRESPONDIENTE
      {
        $tmpArr=(array)$value->tabularFilter;
        $jsArrFieldFilter.='arrFieldFilter["'.$key.'"]=\''.json_encode($moduleSetting->arrFieldsFormModal[$key]['tabularFilter']).'\';';
      }
		}

		if(count($arrUnsortedheaderTable)) // REORDENA LAS COLUMNAS A MOSTRAR SEGUN EL ORDEN ESPECIFICADO EN "orderTabularList"
			ksort($arrUnsortedheaderTable);

    foreach($arrUnsortedheaderTable as $key=>$value)
    {
      $arrheaderTable[key($value)]=$value[key($value)];
    }

		$dataList->arrFieldsOutput=substr($arrFieldsOutput,0,-1);
		$listFieldsOutput=$dataList->arrFieldsOutput;
    unset($arrUnsortedheaderTable);
	}
  else
  {
		if(count($dataList->mainTableFieldsNames))
		{
			foreach($dataList->mainTableFieldsNames as $value)
			{
				$arrheaderTable[$value['Field']]=$value['Field'];
				$listFieldsOutput.=$value['Field'].',';
			}
			$listFieldsOutput=substr($listFieldsOutput,0,-1);
		}
  }

	$tmpArr=$tableObj->getfieldHeaderNames($listFieldsOutput);

// START BLOCK AUTONUMBER
	$objTableHeaderFields=[];

  // SI SE DESEA VISUALIZAR EL NUMERO INCREMENTAL DE LAS FILAS, SE DEBEN CUMPLIR LOS REQUISITOS
  if(@$moduleSetting->columIndexTable && @$moduleSetting->autoRowNumber)
  {
		$objTableHeaderFields['autoRowNumber']='int';
  }

	$objTableHeaderFields=@array_merge($objTableHeaderFields, $tmpArr);
  unset($tmpArr);
// END BLOCK AUTONUMBER
}


if(is_array($listFieldsOutput))
{
	$jsFieldsSelect='"';

	foreach($listFieldsOutput as $key=>$value)
	{
		$jsFieldsSelect.=''.$value['Field'].',';
	}

	$jsFieldsSelect=substr($jsFieldsSelect,0,-1);
	$jsFieldsSelect.='"';
}
else
	$jsFieldsSelect='"'.$listFieldsOutput.'"';

if(count($objTableHeaderFields))
{
	$jsFieldNames='{';

	foreach($objTableHeaderFields as $key=>$value)
	{
		$jsFieldNames.='"'.$key.'":"'.$value.'",';
	}

	$jsFieldNames=substr($jsFieldNames,0,-1);
	$jsFieldNames.='}';
}

$listButtonType=[1=>'waves-circle btn-icon btn-default', 2=>'btn-sm m-r-5 f-16'];
$listButtonTypeLast=[1=>'waves-circle btn-icon btn-default', 2=>'btn-sm c-deeporange  m-r-5 f-16'];

$classesBtnType='waves-circle btn-icon btn-default';
$classesBtnTypeLast='waves-circle btn-icon btn-default';
if(isset($_SESSION['userAdmin']['pref_']['i_button_list_type']))
{
	$classesBtnType=$listButtonType[$_SESSION['userAdmin']['pref_']['i_button_list_type']];
	$classesBtnTypeLast=$listButtonTypeLast[$_SESSION['userAdmin']['pref_']['i_button_list_type']];
}
$foPaginateColor=isset($_SESSION['userAdmin']['pref_']['btn_default_forCol']) ? substr($_SESSION['userAdmin']['pref_']['btn_default_forCol'],2) : '#fff ';
$bgPaginateColor=isset($_SESSION['userAdmin']['pref_']['btn_default_bckCol']) ? substr($_SESSION['userAdmin']['pref_']['btn_default_bckCol'],4) : '#00BCD4';
$bdPaginateColor=isset($_SESSION['userAdmin']['pref_']['top_header_theme_bckCol']) ? substr($_SESSION['userAdmin']['pref_']['top_header_theme_bckCol'],4) : '#fff';

?>
<style>
.pagination>.active>a,.pagination>.active>a:focus,.pagination>.active>a:hover,.pagination>.active>span,.pagination>.active>span:focus,.pagination>.active>span:hover {z-index:3;color:<?=$foPaginateColor?>;background-color:<?=$bgPaginateColor?>;border-color:<?=$bdPaginateColor?>;cursor:default}

</style>
<script type="text/javascript">
var arrFieldFilter=new Array();
var root_path="<?=$web_url.$adminFolder?>/";
var fatherJs=window.parent.document;
var ifrFather=fatherJs.getElementById("ifrListBlock");
var allowNewRecord=<?=isset($moduleSetting->allowNewRecord) && $moduleSetting->allowNewRecord ? 'true' : 'false'?>;
var timeoutIdHeadGrid;
var modalHeadGrid=false;
var tmpFieldName="";
var objAdditionalFilters=new Object();
var objTmpValuesFilters=new Object();
<?=$jsArrFieldFilter?>

/**
 * REDIMENSIONA LA ALTURA DEL IFRAME ABRIDOR DE ESTE MISMO SCRIPT
 * @returns {none}
 */
function redimListingIframe(tmpHeight)
{
	$(ifrFather).css("height",tmpHeight+"px");
	return;
}

//	ACCION PARA QUITAR FILTROS Y LAS CONDICIONES APLICADAS A LA LISTA VISUALIZADA
parent.$("body").on("click",".removeListFilter",function(e){
	var tmpElem=e.currentTarget;//data-dismiss
  delete objAdditionalFilters[$(tmpElem).attr("data-filter-name").substr(9)];
  $(gridObj).bootgrid('reload');
  $(tmpElem).attr("data-dismiss","alert");

	$.ajax({
		url:root_path+"ajax/common_listing.php",
		method:"post", dataType:"json", cache: false,
		data:{action:"removeListFieldFilter",mainTableName:"<?=$moduleSetting->mainTable?>",fieldName:$(tmpElem).attr("data-filter-name").substr(9),moduleFolder:"<?=$arraExtVars['modulePath']?>",filterFieldName:"sys_find_"+tmpFieldName}
		,success:function(response)
		{
			if(!response.success)
			{
				parent.commonNotify("<center><i class=\'f-700 f-20 zmdi zmdi-info-outline\'></i>"+response.msgPainTxt+"</center>", response.errorText, undefined, "center", undefined, 'danger', "animated flipInY", "flipOutY", 6500);
				return false;
			}

			if(response.success)
			{
				parent.commonNotify("", "<?=__COMMON_NOTIFY_JSREMOVELISTFILTERSUCCESSSTART?> <span class=\'c-yellow f-700\'><u>"+tmpFieldLabel+"</u></span> <?=__COMMON_NOTIFY_JSREMOVELISTFILTERSUCCESSEND?>", undefined, "right", undefined, 'info', "animated flipInX", "flipOutX", 2500);
			}
		},error:function(response){
				setTimeout(function(){
parent.$(".headerContentFilter").html("<span class=\'f-700 f-20 c-yellow\'><?=__COMMON_GENERICTEXT_WARNING?></span><br><br /><?=__COMMON_NOTIFY_JSREMOVELISTAJAXFILTERERRORSTART?> <span class='c-white bgm-indigo p-r-10 p-l-10'>"+tmpFieldLabel+"</span> <?=__COMMON_NOTIFY_JSREMOVELISTAJAXFILTERERROREND?>.<br /><br /><center><?=__COMMON_GENERICTEXT_SORRYFORTHEINCONVENIENCE?>.</center>");
				},600);
		}
	});
  return;
})

function newEvents(tmpHeight)	// NUEVOS EVENTOS CREADOS DINAMICAMENTE
{
//	$('.selectpicker').selectpicker({size:4});

	$("#grid-data-api th").on("mouseout", "span", function(e)
	{
		if(timeoutIdHeadGrid)	// RESET TIMEOUT FOR HEADER MODALS
		{
			window.clearTimeout(timeoutIdHeadGrid);
			timeoutIdHeadGrid=null;
			modalHeadGrid=null;
		}
	});

	$("#grid-data-api th a").each(function(){
		if((!this.text.substring(0,<?=$filterSearchListIdentifierLen?>) == "<?=$filterSearchListIdentifier?>"))
			return false;

		if("<?=!$rightClickFilterEnabled?>")
			return false;

		$(this).attr('title', '<?=__COMMON_JSTITLE_TXTHELPOPENRIGHTCLICK?>');	// ONLY FOR RIGHT CLICK ENABLED
		$(this).attr('oncontextmenu', 'return false;');	// DISABLE CONTEXTUAL MENUS FOR COLUMNS LIST HEADERS
	});

<?php
/**
 * START EJECUCION DE "$("#grid-data-api th")" SEGUN SE TENGA CONFIGURADO EL MODO
 * DE LA APERTURA DE LOS FILTROS, PERSONALIZADA PARA CADA USUARIO
 */
echo $modeOnColumFiltersStart;
?>
		e.preventDefault();
		if(!timeoutIdHeadGrid) // GESTION DE LOS FILTROS PARA LA COLUMNA SELECCIONADA
		{
			timeoutIdHeadGrid = window.setTimeout(function(){
				timeoutIdHeadGrid = null;
				var tmpFielHeader=e.delegateTarget;

				if($(tmpFielHeader).text().substring(0,<?=(int)$filterSearchListIdentifierLen?>) != "<?=$filterSearchListIdentifier?>")
					return false;	// SOLO APLICABLE A COLUMNAS CON FILTROS ESTABLECIDOS

        tmpFieldName=$(tmpFielHeader).attr("data-column-id");
        tmpFieldLabel=$(tmpFielHeader).text().substring(3);

        if(objAdditionalFilters[tmpFieldName] != undefined) // EL ELEMENTO YA EXISTE
        {
					parent.commonNotify("<?=__COMMON_NOTIFY_JSINFOALREADYFILTEROPEN?>:", "<br /><span class=\'c-yellow f-700\'>"+tmpFieldLabel+"</span>", undefined, "right", undefined, 'info', "animated flipInY", "flipOutY", 2500);
          return false;
        }

        if(arrFieldFilter[tmpFieldName])
        {
          $.ajax({
            url:root_path+"ajax/common_listing.php",
            method:"post", dataType:"json", cache: false,
            data:{action:"createListFieldFilter",mainTableName:"<?=$moduleSetting->mainTable?>",keyId:"<?=$moduleSetting->keyListLabel?>",fieldName:tmpFieldName,moduleFolder:"<?=$arraExtVars['modulePath']?>",settingsField:arrFieldFilter[tmpFieldName],filterFieldName:"sys_find_"+tmpFieldName}
            ,success:function(response)
            {
              objAdditionalFilters[tmpFieldName]={};

							if(response.errorModule)
							{
								parent.commonNotify("<center><i class=\'f-700 f-20 zmdi zmdi-info-outline\'></i></center>", response.textMsg, undefined, "center", undefined, 'danger', "animated flipInY", "flipOutY", 6500);
								return false;
							}

              if(response.success)
              {
                setTimeout(function(){
                  parent.$(".gridFilters").css("z-index","20");
									parent.commonNotify("<center><i class=\'f-700 f-20 zmdi zmdi-check\'></i></center>", response.textMsg, undefined, "right", undefined, 'success', "animated flipInX", "flipOutX", 2000);

									if(parent.$(".card-header"))
									{
										parent.$("<?=$locationColumFilters?>").append(response.htmlObjectStart+response.htmlObject+response.htmlObjectEnd);
										setTimeout("parent.$('[data-toggle=\"popover\"]').popover();",80);

                    parent.newEventsMainIndex();
//console.log("SE DEBE ACTUALIZAR EL INDEX CON parent.newEvents JUNTO A LA CONSTRUCCION DEL FILTRO!!! .......... tmpValue.... ");

                    // UPDATE DATA TABULAR LIST ON "apply filter" BUTTON
										parent.$(".btnApplyFieldList").click(function(el){

											var buttonPressed=$(this).attr("data-filter-name");
											var tagType=parent.$("#"+buttonPressed).prop('tagName').toLowerCase();
//console.log("click para btnApplyFieldList..... tagType = ",tagType)
											var tmpValue=parent.$("#"+$(this).attr("data-filter-name")).val();
											tmpFieldName=$(tmpFielHeader).attr("data-column-id");	// OBTENCION DEL NOMBRE DEL CAMPO POR EL`PARAMETRO DE LA CABECERA DE LA COLUMNA PRESENTE
											tmpSerachValue="";

                      if(parent.$("input[name='"+$(this).attr("data-filter-name")+"[]']:checked").val() != null && tagType == 'input' && response.filterFieldNameResponse == buttonPressed)  // FOR RADIO TAGS
											{
                        objAdditionalFilters[tmpFieldName]={valueToFind:parent.$("input[name='"+$(this).attr("data-filter-name")+"[]']:checked").val()};
												tmpSerachValue=parent.$("input[name='"+$(this).attr("data-filter-name")+"[]']:checked").val();
											}

//                      if(tmpValue != null && (tagType == 'select' || tagType == 'input') && response.filterFieldNameResponse == buttonPressed)
                      if(tmpValue != null && (tagType == 'select' || tagType == 'input') && response.filterFieldNameResponse == buttonPressed && !tmpSerachValue)
											{
												var objInputsTmp=parent.$("[data-name~='"+tmpFieldName+"']"); // PARA DISCRIMINAR CON LOS CAMPOS DE FECHA Y CONSTRUIR f.inicio + f.fin

												if(objInputsTmp.size() == 2)	// HAY DOS CAMPOS CON EL MISMO NOMBRE
												{
													$.each(objInputsTmp, function(x, tmpElem){
//														objAdditionalFilters[tmpFieldName]['from']=null;
//														objAdditionalFilters[tmpFieldName]['settingsField']=arrFieldFilter[tmpFieldName];
														if(!x)
															objAdditionalFilters[tmpFieldName]['from']=tmpElem.value;
														else
															objAdditionalFilters[tmpFieldName]['to']=tmpElem.value;

														tmpValue+=tmpElem.value ? 1 : 0;
													})
												}
												else
													objAdditionalFilters[tmpFieldName]={valueToFind:tmpValue};  // ONLY FOR SELECT TAGS

												tmpSerachValue=tmpValue;
											}

                      if(tmpSerachValue == "")
                        return false;

											objAdditionalFilters[tmpFieldName]['settingsField']=arrFieldFilter[tmpFieldName];

											$(gridObj).bootgrid('reload');
											$.ajax({
												url:root_path+"ajax/common_listing.php",
												method:"post", dataType:"json", cache: false,
												data:{action:"applyListFieldFilter",mainTableName:"<?=$moduleSetting->mainTable?>",fieldName:tmpFieldName,moduleFolder:"<?=$arraExtVars['modulePath']?>",filterFieldName:"sys_find_"+tmpFieldName,realValueFind:tmpSerachValue}
												,success:function(response)
												{
													if(!response.success)
													{
														parent.commonNotify("<center><i class=\'f-700 f-20 zmdi zmdi-info-outline\'></i>"+response.msgPainTxt+"</center>", response.errorText, undefined, "center", undefined, 'danger', "animated flipInY", "flipOutY", 6500);
														return false;
													}

													if(response.success)
													{
														parent.commonNotify("<?=__COMMON_NOTIFY_JSSEARCHFILTERCORRECTLY?>", "", undefined, "right", undefined, 'success', "animated flipInY", "flipOutY", 2000);
														return false;
													}
												},error:function(response){}
											});
										})
									}
                },600);
              }
              else
              {
                setTimeout(function(){  // SE HA PRODUCIDO ALGUN ERROR ESTRUCTURAL
									parent.$('#modDataGridFilters').modal('toggle');
                  var responseMsg=response.errorText ? response.errorText : '<?=__COMMON_MODALMSG_JSAJAXERRORTXTCODE?> '+(response.status == 200 ? response.status+"<br /><br /><h4 class='c-yellow'><?=__COMMON_MODALMSG_JSAJAXERRORTXTCODE?></h4>" : response.status);
                  parent.$(".headerContentFilter").html("<div class='card-body card-padding'></div><center><div class='m-10 m-b-0 alert alert-warning bgm-red z-depth-2'>"+responseMsg+"<br /></div></center><br /><div class='text-right fg-float m-r-20'><button type='button' class='btn bgm-gray waves-effect' data-dismiss='modal'><?=__COMMON_GENERICTEXT_CLOSE?></button></div><br />");
                },1000);
              }
            },error:function(response){
              setTimeout(function(){  // ERROR GRAVE
                parent.$('#modDataGridFilters').modal('toggle');
                parent.$(".headerContentFilter").html("<span class=\'f-700 f-20 c-yellow\'><?=__COMMON_GENERICTEXT_WARNING?></span><br><br /><?=__COMMON_NOTIFY_JSREMOVELISTAJAXFILTERERRORSTART?> <span class='c-white bgm-indigo p-r-10 p-l-10'>"+tmpFieldLabel+"</span> <?=__COMMON_NOTIFY_JSREMOVELISTAJAXFILTERERROREND?>.<br /><br /><center><?=__COMMON_GENERICTEXT_SORRYFORTHEINCONVENIENCE?>.</center>");
              },600);
            }
          });
        }
				modalHeadGrid=true;
<?php
/**
 * START EJECUCION DE "$("#grid-data-api th")" SEGUN SE TENGA CONFIGURADO EL MODO
 * DE LA APERTURA DE LOS FILTROS, PERSONALIZADA PARA CADA USUARIO
 */
echo $modeOnColumFiltersEnd;
?>

		 // TIEMPO DE ESPERA PARA MOSTRAR EL FILTRO DE LA COLUMNA SELECCIONADA
		}
		return false;
	});

	/**
	 * COMMON METHOD EDITING CALL MODAL DATA WINDOW
	 *
	 * @param {object} e
	 * @returns {undefined}
	 */
	$("#grid-data-api td").on("click", ".editRow", function(e){
		window.parent.openModalEditRecord($(this).parent().parent("tr").attr("data-row-id"), $(this));
		window.parent.$(".idTitleRef").html("<span style=font-style:italic;><div class='preloader pls-yellow pl-xs'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div> <?=__COMMON_JSTITLE_TXTEDITLOADINGDATA?><span>")
		return false;
	});


	/**
	 * CALL COMMON METHOD DELETE ROW - ACTION OPEN CONFIRM MODAL WINDOW
	 */
	$("#grid-data-api td").on("click", ".deleteRow", function(e){
		window.parent.$(".idTitleRef").html("<b>"+$(this).parent().parent("tr").attr("data-row-id")+"</b>")
		window.parent.$(".actDelete").attr('data-id', $(this).parent().parent("tr").attr("data-row-id"));
		window.parent.$("#modalWiderRefDelete").click();
		return false;
	});

	/**
	 * METHOD COMMON CLICK ENABLE/DISABLE ROW RERCORD
	 */
	$("#grid-data-api td").on("click", ".enableDisableRow", function(e){
    var setNewStatus=$(this).parent().parent("tr").attr("class") == "warning" ? 1 : 0;
		enableDisableRowId($(this).parent().parent("tr").attr("data-row-id"), "<?=$moduleSetting->mainTable?>", setNewStatus, <?=isset($dataList->fieldNameStatusRow) ? '"'.$dataList->fieldNameStatusRow.'"' : 'null'?>, e);
		return false;
	});

	/**
	 * COPY RECORD (2018)
	 */
	$("#grid-data-api td").on("click", ".copyRecordRow", function(e){
		window.parent.$('#modalCopyRow').modal('toggle');
		copyRowId($(this).parent().parent("tr").attr("data-row-id"), "<?=$moduleSetting->mainTable?>", "<?=substr($arraExtVars['modulePath'],9,-1)?>", e);
<?php
if(isset($moduleSetting->arrCopyExcludeFields) && count($moduleSetting->arrCopyExcludeFields))
{
	$strTmpCopyInfo='<br /><br />'.__COMMON_MODALMSG_JSEXTRACOPYRECORDTEXT.':';
	foreach($moduleSetting->arrCopyExcludeFields as $tnmValParam)
		$strTmpCopyInfo.='<br />- <b>'.$tnmValParam.'</b>';
	echo 'window.parent.$(".idTitleRef").append("'.$strTmpCopyInfo.'");';
}

?>
		return false;
	});

		redimListingIframe(tmpHeight);  // REDIMENSIONA EL ALTO DEL IFRAME PADRE
}
var gridObj=null;

$(document).ready(function(){

	$(window).load(function(){
		parent.$(".preloadData").hide();
    if(allowNewRecord)	// DIBUJAR LOS ACCESOS PARA PERMITIR INSERTAR NUEVOS REGISTROS
    {
      parent.$(".modalNewRecord").show();
    }
	});

	gridObj=$("#grid-data-api").bootgrid({
		rowCount:<?=(isset($moduleSetting->numRecDisplay) && $moduleSetting->numRecDisplay != '[5,10,25,50]') ? $moduleSetting->numRecDisplay : '[5,10,25]' ?>
    ,searchSettings:{
      delay:250,
      characters:2
    }
		,templates:{
			header: "<div id=\"{{ctx.id}}\" class=\"{{css.header}}\"><div class=\"row\"><div class=\"col-sm-12 actionBar\"><div class=\"topPagination col-xs-4\"><p class=\"{{css.pagination}}\"></p></div> <span class=\"checkINcludeNumbers\"><label data-trigger=\"hover\" data-toggle=\"popover\" data-placement=\"bottom\" data-content=\"<?=__COMMON_POPOVER_JSMSGCONTENTINCLUDENUMERICDATA?>\" title=\"<?=__COMMON_POPOVER_JSMSGTITLEINCLUDENUMERICDATA?>\" class=\"checkbox checkbox-inline m-r-20\"><input name=\"includeNumbers\" value=\"1\" type=\"checkbox\" class=\"\"  /><i class=\"input-helper\"></i><?=__COMMON_POPOVER_JSTEXTINCLUDENUMERICDATA?></label></span> <span class=\"containerAutoUpdate hide\"><label data-trigger=\"hover\" data-toggle=\"popover\" data-placement=\"bottom\" data-content=\"Si está marcado, ejecuta la actualización automática del listado cada <?=($autoUpdateTime ? ($autoUpdateTime) : 5)?> segudos\" title=\"Auto-referesh del listado\" class=\"checkbox checkbox-inline m-r-20\"><input id=\"isAutoUpdate\" name=\"isAutoUpdate\" value=1 type=\"checkbox\" /><i class=\"input-helper\"></i>Auto-update</label></span> <p class=\"{{css.search}}\"></p><p class=\"{{css.actions}}\"></p></div></div></div>",
			search: " <div class=\"{{css.search}}\"><div class=\"input-group\"><span class=\"{{css.icon}} input-group-addon {{css.iconSearch}}\"></span> <input type=\"text\" class=\"{{css.searchField}}\" placeholder=\"{{lbl.search}}\" /></div></div>"
		}
		,labels:{
			noResults:"<br /><br /><h4 class='c-gray'><?=__COMMON_BBDD_TXTNODATAERROR?></h4><br /><br />",
			loading: "<br /><?=__COMMON_GENERICTEXT_LOADINGWAIT?><br /><br /><br />",
			infos: "<?=__COMMON_JSBOOTGRID_INFOSLABELS?>",
			refresh: "<?=__COMMON_JSBOOTGRID_REFRESHLABELS?>",
      search: "<?=__COMMON_JSBOOTGRID_SEARCHLABELS?>"
		}
		,css:{
			icon: 'zmdi icon',
			iconColumns: 'zmdi-view-module',
			iconRefresh: 'zmdi-refresh',
			iconDown: 'zmdi-sort-amount-desc',
			iconUp: 'zmdi-sort-amount-asc',
      paginationButton:"<?=$foPaginateColor?> f-12 waves-effect",
      pagination: 'pagination col-xs-pull-10'
		}
		,ajax:true
		,requestHandler:function(request){	// ADD PARAMS TO AJAX REQUEST
<?php
/**
 * LAS FILAS A DIBUJAR EN EL LISTADO SE OBTIENEN EN EL SCRIPT _mod_bbdd\mod_common_lists.class.php
 */
?>
			request.action="getTableListData";
			request.includeNumbers=($("input[name='includeNumbers']").prop('checked') == true) ? 1 : 0;
			request.moduleFolder="<?=$arraExtVars['modulePath']?>";
			request.mainTable="<?=$moduleSetting->mainTable?>";
			request.prefixTbl="<?=__QUERY_TABLES_PREFIX?>";
			request.fieldNames=<?=$jsFieldNames?>;
			request.listFieldsOutput=<?=$jsFieldsSelect?>;
			request.defaultWhereSql="<?=$dataList->defaultWhereSql?>";
			request.skipBdDefault=<?=$skipBdDefault?>;
			request.additionalFilters=objAdditionalFilters;
			request.id="b0df282a-0d67-40e5-8558-c9e93b7befed";
			return request;
		}
		,formatters:{	// SE EJECUTA POR CADA FILA OBTENIDA
		commands:function(column, row){
	      var elem="";
				var stringKeys="";
<?php
/**
 * SOLO SI EXISTE LA VARIABLE $moduleSetting->foreignKeys
 * SE CONSTRUYE EL HTML PARA ALMACENAR LOS DATOS
 */
if(isset($moduleSetting->foreignKeys) && $moduleSetting->foreignKeys)
{
?>
				var arrForeignKeys="<?=$moduleSetting->foreignKeys?>";
				arrForeignKeys=arrForeignKeys.split(',');

				$.each(row,function(dataNameField, dataValue){
					$.each(arrForeignKeys,function(n,keyStore){
						if(keyStore == dataNameField)
							stringKeys+=" data-foreign-key["+keyStore+"]=\""+dataValue+"\"";
					});
				})
<?php
}
/**
 * START BOTONES ACCIONES DEL LISTADO
 */

// SE DIBUJA EL BOTON DE EDITAR REGISTRO SOLO CUANDO PERMITIDO EN LA CONFIGURACION DEL MODULO
if(@$moduleSetting->isEditable && $_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_RESPONSIBLE)
{
?>
				elem+="<a data-toggle=\"modal\" href=\"#null\" class=\"btn waves-effect <?=$classesBtnType?> command-edit editRow\" "+stringKeys+" data-row-id=\"" + row[this.identifier] + "\" title=\"<?=__COMMON_JSBOOTGRID_MODULESETTINGISEDITABLE?>" + row[this.identifier] + "\"><span class=\"zmdi zmdi-edit\"></span></a>";
<?php
}

// SE DIBUJA EL BOTON DE ELIMINAR REGISTRO SOLO CUANDO CONSENTIDO EN LA CONFIGURACION DEL MODULO
if(isset($moduleSetting->isErasable) && $moduleSetting->isErasable)
{
?>
				elem+="<button type=\"button\" class=\"btn waves-effect <?=$classesBtnType?> command-delete deleteRow\" data-row-id=\"" + row[this.identifier] + "\" title=\"<?=__COMMON_JSBOOTGRID_MODULESETTINGDELETERECORD?>" + row[this.identifier] + "\"><span class=\"zmdi zmdi-delete\"></span></button>";
<?php
}

// SE DIBUJA EL BOTON DE HABILITAR / DESHABILITAR REGISTRO SOLO CUANDO PERMITIDO EN LA CONFIGURACION DEL MODULO
if(@$moduleSetting->isCanEnabled && ($_SESSION['userAdmin']['roleId'] >= @(int)$moduleSetting->minEnableLevel))
{
?>
				elem+="<a data-toggle=\"modal\" href=\"#null\" class=\"btn waves-effect <?=$classesBtnTypeLast?> command-edit enableDisableRow\" data-row-id=\"" + row[this.identifier] + "\" title=\"<?=__COMMON_JSBOOTGRID_MODULESETTINGENABLEDISABLESTART?>" + (Math.ceil(row[this.identifier]) == row[this.identifier] ? "\n<?=__COMMON_JSBOOTGRID_MODULESETTINGENABLEDISABLEEND?>" : "")+row[this.identifier]+ "\"><span class=\"zmdi zmdi-play\"></span></a>";
<?php
}

// 2018 COPY RECORD BUTTON
if(@$moduleSetting->allowCopyRecord && ($_SESSION['userAdmin']['roleId'] >= @(int)$moduleSetting->minCopyRecordLevel))
{
?>
				elem+="<a data-toggle=\"modal\" href=\"#null\" class=\"btn waves-effect <?=$classesBtnType?> command-edit copyRecordRow bgm-cyan\" data-row-id=\"" + row[this.identifier] + "\" title=\"<?=__COMMON_JSBOOTGRID_MODULESETTINGCOPYRECORDSTART?>" + (Math.ceil(row[this.identifier]) == row[this.identifier] ? "\n<?=__COMMON_JSBOOTGRID_MODULESETTINGENABLEDISABLEEND?>" : "")+row[this.identifier]+ "\"><span class=\"zmdi zmdi-copy\"></span></a>";
<?php
}
// END BOTONES ACCIONES DEL LISTADO

?>
				return elem
			}
		}
	}).on("loaded.rs.jquery.bootgrid",function(e, rows){
		$("input[name='includeNumbers']").on("click", function(e){	// PARA INCLUIR LOS CAMPOS NUMERICOS EN LAS BUSQUEDAS DE REGISTROS
			if(!$(".search-field").val())
				return;

			$(gridObj).bootgrid('reload');
		})

		$(this).attr("ajax",true);

    if(<?=(int)$skipBdDefault?>) // SE OMITEN CIERTOS CONTROLES AL NO SER LAS TABLAS STANDARD A LISTAR
    {
      setTimeout('$(".search, .checkINcludeNumbers").hide();',80);
    }

		var tmpHeight=e.currentTarget.clientHeight+200;
		setTimeout("newEvents('"+tmpHeight+"');$('[data-toggle=\"tooltip\"]').tooltip();$('[data-toggle=\"popover\"]').popover();",80);

		if(window.top.setAutoUpdateBootGrid == true){$('.containerAutoUpdate').show();}	// PERMITE CARGAR CORRECTAMENTE EL CONTROL DEL PLUGIN "AUTO UPDATE BOOT GRID"
	});

//	$(".sortable").click(function(){	// UPDATE EVENTES ON REORDER ACTION
//console.log("reorder action...")
//	})

	$("#isAutoUpdate").click(function(e)
  {
		if(window.top.setAutoUpdateBootGrid != null)
		{
			if(this.checked)
			{
				window.top.setAutoUpdateBootGrid=true;
				window.top.autoReloadBootGrid();
			}
			else
				window.top.setAutoUpdateBootGrid=false;
		}
  });

//  gridObj.bootgrid().on(".dropdown.open load.rs.jquery.bootgrid",function(e)
//  {
//console.log("2.bootgrid =====--->",e)
//  });

	$("#grid-data-api").fadeIn("slow",function(){	// NOTA: DEJAR ESTA ACCION AL FINAL DEL DOCUMENT READY
		$(".preloadData").hide();
	});

	setTimeout("SetWidthGridColumns();",200);
});

function SetWidthGridColumns()
{
<?php
/**
 * $arrTabularColumnWidths ES EL ARRAY CON LAS DEFINICIONES DEL ANCHO DE LAS COLUMNAS
 * PARA EL LISTADO A VISUALIZAR, DEFINIDO EN EL CONFIG DEL MODULO CON "columnWidth"
 */
if(count($arrTabularColumnWidths))
{
	foreach($arrTabularColumnWidths as $kk=>$vv)
		echo '$("th[data-column-id=\''.$kk.'\']").addClass("clearfix").addClass("col-xs-'.$vv.'");';
}
else
	echo'$(\'#grid-data-api>thead>tr>th:first-child\').addClass(\'classFirstColum\');';
?>
}
</script>

</head>
<body>


	<div class="card-data-liting">
		<center class="preloadData">
			<div class="preloader">
					<svg class="pl-circular" viewBox="25 25 50 50">
						<circle class="plc-path" cx="50" cy="50" r="20"></circle>
						<p><?=__COMMON_GENERICTEXT_LOADINGWAIT?></p>
					</svg>
			</div>
		</center>

		<table id="grid-data-api" class="table table-condensed table-hover table-striped" data-toggle="bootgrid" data-ajax="true" data-url="<?=$web_url.$adminFolder?>/ajax/common_admin.php">

<!-- START LISTA HEADER -->
			<thead>
				<tr>
<?php
//echo $web_url.$adminFolder.'<pre>';print_r($objTableHeaderFields);echo'</pre>';

/**
 * NOTAS
 * LA PRIMERA COLUMNA, SIEMPRE HA DE TENER "data-column-id" = "id"
 */
$outHeaderList='';
$posCol=0;

if(isset($objTableHeaderFields) && count($objTableHeaderFields))
{
  $setColumIndex=isset($moduleSetting->columIndexTable) ? (int)$moduleSetting->columIndexTable : null;

	// SE DIBUJAN LAS CABECERAS DEL LISTADO TABULAR
	foreach($arrheaderTable as $key=>$value)
	{
    $dataIdentifier='';
		$dataType=!$posCol && !isset($setColumIndex) ? 'numeric' : '0';
    $dataIdentifier=' data-column-id="'.$key.'"';
    $dataFieldFilterIcon=isset($moduleSetting->arrFieldsFormModal[$key]['tabularFilter']) ? $filterSearchListIdentifier : '';

    // CON LA COLUMNA IDENTIFICADORA DE ID PARA TABLAS STANDARD CON EL PRIMER CAMPO NUMERICO ID
    // O SI LA COLUMNA IDENTIFICADORA TIENE UNA REFERENCIA DE TIPO STRING O NO ES LA PRIMERA COLUMNA
    if(!$setColumIndex && !$posCol || isset($setColumIndex) && ($posCol == $setColumIndex))
      $dataIdentifier.=' data-identifier="true" ';

		@$outHeaderList.='<th '.$dataIdentifier.'  >'.$dataFieldFilterIcon.$value.'</th>';
//		@$outHeaderList.='<th '.$dataIdentifier.' data-type="'.(!isset($setColumIndex) ? $dataType : '').'" field-type="'.$dataType.'" '.$dataFieldFilter.' >'.$value.'</th>';

		$posCol++;
	}

	$outHeaderList.='<th data-column-id="commands" data-formatter="commands" data-sortable="false">'.__COMMON_THBOOTGRID_COMMANDSTEXTHEADER.'</th>';
}
else
{
	$outHeaderList='<th data-column-id="">'.__COMMON_GENERICTEXT_NODATA.'</th>';
}

echo $outHeaderList;
?>
				</tr>
			</thead>
<!-- END LISTA HEADER -->

			<tbody>
<?php
$outTable='';

if(!count(@$tableObj->tableRow))
{
  $outTable='<tr><td>'.__COMMON_THBOOTGRID_NOLISTDATA.'</td></tr>';
}
//echo '<pre style="display:inline-block;width:100%;height:100%;">';print_r($_SESSION['userAdmin']['pref_']);echo'</pre>';
?>
			</tbody>
		</table>
	</div>

</body>
</html>
