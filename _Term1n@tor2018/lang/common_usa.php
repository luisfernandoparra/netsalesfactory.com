<?php
//	common translations
//	ITALIAN

$langTxt['common']['generic_text']='Il mio testo!';
$langTxt['common']['generic_help']='Questo è l`aiuto';

$langTxt['ajax']['errorBbdd']['lostConnectionTxt']='Sembra che il tempo della tua sessione sia scaduto!';
$langTxt['ajax']['errorBbdd']['lostConnectionTxtBottom']='È necessario ri-convalidare l`accesso per continuare...';
$langTxt['ajax']['common']['fileNotSentTxt']='Non è stato possible inviare il file';
$langTxt['ajax']['common']['fileChangedErrorTxt']='È cambiato il file';
$langTxt['ajax']['common']['missingIdRefTxt']='it seems that the reference is missing';
$langTxt['ajax']['common']['noDataFoundTxt']='it was not possible to read the source record';

$langTxt['common']['genericText']['userConfigTxt']='Preferenze degli utenti';
$langTxt['common']['genericText']['systemConfigTxt']='Back-office configuration';
$langTxt['common']['genericText']['processing']='processando';
$langTxt['common']['genericText']['retrievingData']='recuperando i dati.';
$langTxt['common']['genericText']['notes']='Annotazioni';
$langTxt['common']['genericText']['only']='only';
$langTxt['common']['genericText']['selectTxt']='Selezionare';
$langTxt['common']['genericText']['systemTxt']='Sistema';
$langTxt['common']['genericText']['userTxt']='Utente';
$langTxt['common']['genericText']['okTxt']='OK';
$langTxt['common']['genericText']['update']='Aggiornare';
$langTxt['common']['genericText']['htmlRendering']='rendendo l` HTML';
$langTxt['common']['genericText']['error']='ERRORE';
$langTxt['common']['genericText']['undeterminedError']='Errore indeterminato';
$langTxt['common']['genericText']['undeterminedErrorTxt']='Errore indeterminato';
$langTxt['common']['genericText']['followingErrorOccurredTxt']='Non è possibile continuare; Si è verificato il seguente errore';
$langTxt['common']['genericText']['noData']='SENZA DATI';
$langTxt['common']['genericText']['warning']='ATTENZIONE';
$langTxt['common']['genericText']['loadContentsTxt']='carica di contenuti';
$langTxt['common']['genericText']['loadingWait']='Caricando, per piacere, attendere...';
$langTxt['common']['genericText']['pleaseWait']='si prega di attendere un momento';
$langTxt['common']['genericText']['status']='stato';
$langTxt['common']['genericText']['txtSaveError']='ATTENZIONE, ERRORE NEL SALVARE I DATI';
$langTxt['common']['genericText']['sorryForTheInconvenience']='Scusate il disagio';
$langTxt['common']['genericText']['erroConfigModuleStart']='Non esiste o non pu&ograve; aprire il file di configurazione per il modulo corrente';
$langTxt['common']['genericText']['erroConfigModuleend']='Si prega di contattare il reparto di informatica.';
$langTxt['common']['genericText']['headerTitleTabularData']='Elenco del modulo';
$langTxt['common']['genericText']['optionsOfYserTitle']='Opzioni di ';
$langTxt['common']['genericText']['optionsTxt']='Opzioni';
$langTxt['common']['genericText']['topChartLinkTxt']='Charts';
$langTxt['common']['genericText']['topChartTitleTxt']='View related charts';
$langTxt['common']['genericText']['reloadTxt']='Ricaricare';
$langTxt['common']['genericText']['forTxt']='per';
$langTxt['common']['genericText']['theFileTxt']='il file';
$langTxt['common']['genericText']['alreadyExistInFolderTxt']='esiste già nella directory di destinazione';
$langTxt['common']['genericText']['nothingSelectedTxt']='nessuna selezione';
$langTxt['common']['genericText']['moduleNotReadyTxt']='Non &egrave; pronto modulo richiesto';
$langTxt['common']['genericText']['inputHelpSearch']='Cercare aiuto';
$langTxt['common']['genericText']['searchingHelpTxt']='Cercando l`aiuto richiesto';
$langTxt['common']['genericText']['configScriptTxt']='Script di configurazione per il modulo attivo (<b>modo edizione</b>)';
$langTxt['common']['genericText']['backUpConfigScriptTxt']='File di back-up della configurazione per il modulo attuale (<b>modo visualizzazione</b>)';
$langTxt['common']['genericText']['backUpTitleTxt']='Back up del modulo';
$langTxt['common']['genericText']['newRecordTxt']='Nuovo record';
$langTxt['common']['genericText']['restore']='Restaurare';
$langTxt['common']['genericText']['restoring']='restaurando';
$langTxt['common']['genericText']['deleting']='cancellando';
$langTxt['common']['genericText']['filterByH4']='Filtra per';

$langTxt['common']['jsTitle']['TxtHelpOpenRightClick']='Fare click con il bottone destro del mouse per aprire il filtro per questa colonna';
$langTxt['common']['jsTitle']['TxtEditLoadingData']='caricamento in corso per l` edizione della riga selezionata...';
$langTxt['common']['jsTitle']['TxtNewRecordLoading']='caricamento dello schermo per la creazione d un nuovo record';
$langTxt['common']['jsSubmitEditForm']['updateMessageRecordRefTxt']='Aggiornamento il record, con riferimento';
$langTxt['common']['jsSubmitEditForm']['insertMessageRecordTxt']='Insertanto il nuovo record ....';
$langTxt['common']['jsGeneric']['UnexpectedError']='Errore inatteso';
$langTxt['common']['jsGeneric']['DeleteErrorTxt']='Impossibile eliminare';

$langTxt['common']['jsBootgrid']['infosLabels']='Vedendo dala fila <b>{{ctx.start}}</b> alla <b>{{ctx.end}}</b> (di <b>{{ctx.total}}</b> file)';
$langTxt['common']['jsBootgrid']['refreshLabels']='List update';
$langTxt['common']['jsBootgrid']['searchLabels']='Find (+2 characters)';
$langTxt['common']['jsBootgrid']['ModuleSettingisEditable']='Edit record ID: ';
$langTxt['common']['jsBootgrid']['ModuleSettingDeleteRecord']='Delete record ID: ';
$langTxt['common']['jsBootgrid']['ModuleSettingEnableDisableStart']='Enable/Disable ';
$langTxt['common']['jsBootgrid']['ModuleSettingCopyRecordStart']='Copy record ';
$langTxt['common']['jsBootgrid']['ModuleSettingEnableDisableEnd']=' ID: ';
$langTxt['common']['thBootgrid']['commandsTextHeader']='Commands';
$langTxt['common']['thBootgrid']['noListData']='Non si sono potuti ottenere dati per questo modulo';

$langTxt['common']['bbdd']['txtNoDataError']='NO SI SONO TROVATI DATI';
$langTxt['common']['bbdd']['txtNoResultError']='Non abbiamo trovato dati da visualizzare';
$langTxt['common']['bbdd']['txtConnectError']='Errore nella connessione con la BBDD';
$langTxt['common']['bbdd']['txtNoConnectionBbdd']='<b>Impossibile</b> connettersi al database';

$langTxt['common']['editModule']['headerTitle']='Edizione del modulo';
$langTxt['common']['editModule']['altExtraFuncText']=' (campo con modale)';
$langTxt['common']['editModule']['btnContractText']='ripiegare';
$langTxt['common']['editModule']['btnExpandText']='Mostrare';
$langTxt['common']['editModule']['mandatoryAltTxt']='obbligatorio';
$langTxt['common']['editModule']['buildFormFileNoExistErrorTxt']=' ERRORE: Non esiste il file sul server!!!';
$langTxt['common']['editModule']['buildFormFileNoFileStartTitle']='Senza un immagine ';
$langTxt['common']['editModule']['buildFormFileNoFileEndTitle']='per';

$langTxt['common']['insertModule']['headerTitle']='Inserimento di un nuovo record per il modulo';

$langTxt['common']['notify']['CorrectOperation']='Operazione corretta';
$langTxt['common']['notify']['infoNoContentsTitle']='INFORMAZIONE';
$langTxt['common']['notify']['infoNoContents']='Non ci sono contenuti da visualizzare';
$langTxt['common']['notify']['jsRemoveListFilterSuccessStart']='Il filtro di ricerca';
$langTxt['common']['notify']['jsRemoveListFilterSuccessEnd']='&egrave; stato rimosso';
$langTxt['common']['notify']['jsTitleErrorFilterRemove']='&egrave; stato rimosso';
$langTxt['common']['notify']['jsRemoveListAjaxFilterErrorStart']='Sembra che si &egrave; verificato un errore durante il tentativo di creare il filtro di ricerca per il campo';
$langTxt['common']['notify']['jsRemoveListAjaxFilterErrorEnd']='nella costruzione del filtro di ricerca.<br /><br />Se dovesse ripetersi questo errore, per piacere, contatta gli informatici';
$langTxt['common']['notify']['jsInfoAlreadyFilterOpen']='&Egrave; gi&agrave; stato aperto il filtro di ricerca';
$langTxt['common']['notify']['jsSearchFilterCorrectly']='Filtro d i ricerca<br><b>APPLICATO correctamente</b>';
$langTxt['common']['notify']['infoActionExecuting']='Esecuzione in corso...';
$langTxt['common']['notify']['restoreError']='Errore nella restaurazione';
$langTxt['common']['notify']['restoreSuccess']='File ripristinato';
$langTxt['common']['notify']['deleteError']='Errore nella cancellazione';
$langTxt['common']['notify']['deleteSuccess']='File eliminato';

$langTxt['common']['popover']['jsMsgTitleIncludeNumericData']='Dati numerici';
$langTxt['common']['popover']['jsMsgContentIncludeNumericData']='Inoltre includere nella ricerca tutti i campi numerici in questo modulo. Ricorda che a volte i dati ottenuti non corrispondono con quelli memorizzati nel DB.';
$langTxt['common']['popover']['jsTextIncludeNumericData']='Includere numerici';

$langTxt['common']['sweetAlert']['recordLoadErrorTitle']='ERRORE DURANTE IL CARICAMENTO DEL REGISTRO';
$langTxt['common']['sweetAlert']['recordLoadErrorTxt']='Errore durante il recupero dei dati, si prega di <b> prima controlla la configurazione di questo modulo è verificato </ b> <br />. <br /> Se l`errore persiste, contattare il reparto di programmazione.';

$langTxt['common']['modalMsg']['modalUserModuleHelpH4']='Specific help for module ';

$langTxt['common']['modalMsg']['confirmDeleteH4']='Eliminare il record';
$langTxt['common']['modalMsg']['idTitleWysiEditH4']='Wysi di ';
$langTxt['common']['modalMsg']['TitleWysiEditStdH4']='contenuti HTML di ';
$langTxt['common']['modalMsg']['updating']='AGGIORNANDO';
$langTxt['common']['modalMsg']['waitMessage']='Attuando l`operazione richiesta...';
$langTxt['common']['modalMsg']['confirmDeleteText']='Realmente vuoi cancellare il record';
$langTxt['common']['modalMsg']['titleEscClose']='Fare click o `ESC` per chiudere';
$langTxt['common']['modalMsg']['SaveTitleError']='ERROR NEL SALVARE I DATI';
$langTxt['common']['modalMsg']['jsAjaxErrorTxtCode']='Errore ajax, codice';
$langTxt['common']['modalMsg']['jsAjaxErrorTxtCodeH4']='(si deve rivedere l`ajax)';
$langTxt['common']['modalMsg']['UpdatingOperationTitleH4']='AGGIORNANDO....';
$langTxt['common']['modalMsg']['modalRestoreConfigFileH4']='Ripristinare questo file di configurazione di backup?';
$langTxt['common']['modalMsg']['modalRestoreConfigFileNameTxt']='Nome del file';
$langTxt['common']['modalMsg']['modaldeleteConfigFileH4']='(Si tratta di un file di backup) Si desidera eliminare questo file di configurazione?';
$langTxt['common']['modalMsg']['modaldeleteConfigFileTxt']='Sei veramente sicuro di voler eliminare il backup';
$langTxt['common']['modalMsg']['modalFieldTransaltionsH4']='Traduzioni per il campo';
$langTxt['common']['modalMsg']['confirmCopyRecordText']='Confirm duplicate record id';
$langTxt['common']['modalMsg']['jsExtraCopyRecordText']='<b>NOTE:</b> the fields listed below will not be duplicated, and must be filled in manually';

$langTxt['common']['ajaxMsg']['genericErrorResSql']='&egrave; necessario controllare questo modulo';
$langTxt['common']['ajaxMsg']['txtNumRecordsFound']='Num. record trovati';
$langTxt['common']['ajaxMsg']['updateConfigFileErrorTxt']='Si è verificato un errore durante il tentativo di aggiornare il file';
$langTxt['common']['ajaxMsg']['UnableExecuteBackupErrorTxt']='Impossibile eseguire il back-up';
$langTxt['common']['ajaxMsg']['unableCreateFolderErrorTxt']='Impossibile creare la directory per il file di back-up';
$langTxt['common']['ajaxMsg']['fileBackupErrorH3']='ERRORE nel back-up';
$langTxt['common']['ajaxMsg']['fileBackupErrorTxt']='Fallito il tentativo di creare il file';
$langTxt['common']['ajaxMsg']['fileBackupErrorFolderTxt']='nella directory';
$langTxt['common']['ajaxMsg']['highLightContentFileErrorTxt']='Impossibile applicare la funzione di <b>evidenziazione della sintassi</b> per il codice sorgente che è stato visualizzato per';
$langTxt['common']['ajaxMsg']['getConfigModuleFileFileErrorTxt']='Non è stato trovato o non è stato possibile aprire<br>il file di configurazione:';
$langTxt['common']['ajaxMsg']['getConfigModuleFileContentsErrorTxt']='Non è stato possibile caricare il contenuto oppure era vuoto<br>per il file di configurazione:';
$langTxt['common']['ajaxMsg']['unableRestoreFileTxt']='Impossibile ripristinare il file';
$langTxt['common']['ajaxMsg']['successRestoreFileTxt']='file <b>ripristinato correttamente</b>';
$langTxt['common']['ajaxMsg']['successDeleteFileTxt']='File <b>eliminato correttamente</b>';
$langTxt['common']['ajaxMsg']['infoReloadPageAfterRestoreTxt']='&Egrave; necessario ricaricare il modulo per implementare le eventuali modifiche';
$langTxt['common']['ajaxMsg']['unbelReadTxt']='Impossible leggere';
$langTxt['common']['ajaxMsg']['tryingToEraseErrorTxt']='ERRORE NEL TENTATIVO DI ELIMINARE';
$langTxt['common']['ajaxMsg']['currentSelectedTxt']='attualmente selezionato';

$langTxt['common']['listBtnTitle']['editRecord']='Editare il record';
$langTxt['common']['listBtnTitle']['deleteRecord']='Eliminare il record';

$langTxt['common']['btn']['resetMenuLeft']='Resetea tu sesión actual';
$langTxt['common']['btn']['SendEmailMenuLeft']='Send email';
$langTxt['common']['btn']['logoutMenuLeft']='Logout';
$langTxt['common']['btnSwalTitle']['HtmlEditNotes']='Annotazioni per l`edizione';
$langTxt['common']['btnSwalContent']['HtmlEditNotes']="IMPORTANTE:\\n\\r- Ricorda che non puoi utilizzare la virgoletta semplice (')\\n\\r- Nemmeno si salveranno i dati appartenenti a campi non attivi";
$langTxt['common']['btn']['save']='Salvare';
$langTxt['common']['btn']['cancel']='Cancellare';
$langTxt['common']['btn']['disableRecord']='Disattivare il record';
$langTxt['common']['btn']['enableRecord']='Attivare il record';
$langTxt['common']['btn']['insert']='Inserire';
$langTxt['common']['btn']['delete']='Eliminare';
$langTxt['common']['btn']['copyRecord']='Proceed with duplication';
$langTxt['common']['btn']['change']='Cambiare';
$langTxt['common']['btn']['close']='Chiudere';
$langTxt['common']['btn']['zoom']='Zoom';
$langTxt['common']['btn']['HTMLEditor']='Editor HTML';
$langTxt['common']['btn']['wysiwygViewTitle']='Semplice visore Wysiwyg';
$langTxt['common']['btn']['wysiwygViewTxt']='Vedere Wysiwyg';
$langTxt['common']['btn']['backOfficeHelp']='Aiuti specifici del modulo';
$langTxt['common']['btn']['backOfficeHelpSamll']='+ Descrizioni di Back-office';
$langTxt['common']['btn']['moduleConfigTxt']='Configurazione del modulo';
$langTxt['common']['btn']['onenInNewWindowTxt']='Aprire in una nuova finestra';
$langTxt['common']['btn']['backOfficeTopVersionTitle']='Ricaricare la pagina HOME';
$langTxt['common']['btn']['backOfficeTopVersionTxt']='Back-Office v.';
$langTxt['common']['btn']['reloadCurrentPageTitle']='Ricaricare questa stessa pagina';
$langTxt['common']['btn']['modalRestoreConfigDisplayFileTxt']='Vedere il contenuto';
$langTxt['common']['btn']['deleteFileTxt']='Eliminare il file';

$langTxt['top']['btn']['fullScreenTxt']='Schermo completo';
$langTxt['top']['btn']['goToUserConfigTxt']='Vai alla pagina delle impostazioni personali';
$langTxt['top']['btn']['openRightPanelTitle']='Aiuti + configurazione del modulo corrente';
$langTxt['top']['btn']['preLogOutTitle']='Se si desidera chiudere la sessione ...';
$langTxt['top']['btn']['logOutTitle']='Clicca ora per chiudere immediatamente la tua SESSIONE di lavoro';

$langTxt['topHeader']['btn']['newRecordTitle']='Inserire un nuovo record';
$langTxt['topHeader']['btn']['maximizeAreaTitle']='Hide/display left menu (if possible)';

$langTxt['rightSideBar']['btn']['backUpDeleteTitle']='Eliminare il file di back-up';
$langTxt['rightSideBar']['btn']['viewFileContentsTitle']='Vedere il contenuto del back-up effettuato';
$langTxt['rightSideBar']['label']['previousRestoreTxt']='Ripristinare precedenti impostazioni';

$langTxt['common']['jsFn']['commonRestoreErrorFormIntroTxt']='ERRORE quando si cercava di salvare i dati, ';
$langTxt['common']['jsFn']['commonRestoreErrorAjaxNative']='Si è; verificato un errore imprevisto';
$langTxt['common']['jsFn']['unknownError']='Errore sconosciuto';

$langTxt['common']['dateFormat']['ymd']='YYYY-MM-DD';
$langTxt['common']['dateFormat']['hm']='HH:mm';
$langTxt['common']['dateShortMonths']['list']='Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec';

$langTxt['plugInChart']['btnSettingsTitle']['txt']='Current chart configuration';
$langTxt['plugInChart']['hideDetailsFields']['placeholder']='Details exclusion fields';

$contL=0;
$wordSep='_';
$tmpWord=null; $tmpValue=null;

foreach($langTxt as $kL=>$vL)
{
	$tmpWord=$kL;

	foreach($vL as $nn=>$tmpValue1)
	{
		$tmpWord_2=null;
		if(is_array($tmpValue1))
		{
			$tmpWord_3=null;

			foreach($tmpValue1 as $nv=>$tmpValue2)
			{
				$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
				define('__'.strtoupper($tmpWord_3), $tmpValue2);
			}
		}
		else
		{
			$tmpWord_2=$tmpWord.$wordSep.$nn;
			define('__'.strtoupper($tmpWord_2), $tmpValue1);
		}
		$contL++;
	}
}
