<?php
//	common translations
//	SPANISH

$langTxt['common']['generic_text']='este es el texto';
$langTxt['common']['generic_help']='mi ayuda';

$langTxt['ajax']['errorBbdd']['lostConnectionTxt']='Parece que tu tiempo de sesi&oacute;n ha finalizado!';
$langTxt['ajax']['errorBbdd']['lostConnectionTxtBottom']='Debes volver a validar tu acceso para proseguir...';
$langTxt['ajax']['common']['fileNotSentTxt']='No se ha enviado el archivo';
$langTxt['ajax']['common']['fileChangedErrorTxt']='Se ha cambiado el archivo';

$langTxt['common']['genericText']['userConfigTxt']='Configuracioes personales';
$langTxt['common']['genericText']['processing']='procesando';
$langTxt['common']['genericText']['retrievingData']='recuperando datos.';
$langTxt['common']['genericText']['notes']='Notas';
$langTxt['common']['genericText']['selectTxt']='Seleccionar';
$langTxt['common']['genericText']['systemTxt']='Sistema';
$langTxt['common']['genericText']['userTxt']='Usuario';
$langTxt['common']['genericText']['okTxt']='OK';
$langTxt['common']['genericText']['update']='Actualizar';
$langTxt['common']['genericText']['htmlRendering']='renderizando el HTML';
$langTxt['common']['genericText']['error']='ERROR';
$langTxt['common']['genericText']['undeterminedError']='Error indeterminado';
$langTxt['common']['genericText']['undeterminedErrorTxt']='Errore indeterminato';
$langTxt['common']['genericText']['followingErrorOccurredTxt']='No es posible proseguir; ha ocurrido el siguiente error';
$langTxt['common']['genericText']['noData']='SIN DATOS';
$langTxt['common']['genericText']['warning']='ATENCI&Oacute;N';
$langTxt['common']['genericText']['loadContentsTxt']='cargando contenidos';
$langTxt['common']['genericText']['loadingWait']='Cargando, por favor, espera...';
$langTxt['common']['genericText']['pleaseWait']='por favor, espera unos instantes';
$langTxt['common']['genericText']['status']='estado';
$langTxt['common']['genericText']['sorryForTheInconvenience']='Perdona el inconveniente';
$langTxt['common']['genericText']['txtSaveError']='ATENCION, ERROR AL GUARDAR';
$langTxt['common']['genericText']['erroConfigModuleStart']='No existe o no se puede abrir el archivo de configuraci&oacute;n para el m&oacute;dulo actual';
$langTxt['common']['genericText']['erroConfigModuleend']='Por favor, contacta con el departamento de inform&aacute;tica';
$langTxt['common']['genericText']['headerTitleTabularData']='Listado del m&oacute;dulo';
$langTxt['common']['genericText']['optionsOfYserTitle']='Opciones de ';
$langTxt['common']['genericText']['optionsTxt']='Opciones de ';
$langTxt['common']['genericText']['reloadTxt']='Recargar';
$langTxt['common']['genericText']['forTxt']='per';
$langTxt['common']['genericText']['theFileTxt']='el archivo';
$langTxt['common']['genericText']['alreadyExistInFolderTxt']='ya existe en la carpeta de destino';
$langTxt['common']['genericText']['nothingSelectedTxt']='Sin nada seleccionado';
$langTxt['common']['genericText']['moduleNotReadyTxt']='No está listo el módulo solicitado';
$langTxt['common']['genericText']['inputHelpSearch']='Buscar ayuda';
$langTxt['common']['genericText']['searchingHelpTxt']='Buscando la ayuda solicitada';
$langTxt['common']['genericText']['configScriptTxt']='Script de configuración del módulo actual (<b>modo edición</b>)';
$langTxt['common']['genericText']['backUpConfigScriptTxt']='Archivo de back-up de configuración del módulo actual (<b>modo visualizaci&oacute;n</b>)';
$langTxt['common']['genericText']['backUpTitleTxt']='Back up del m&oacute;dulo';
$langTxt['common']['genericText']['newRecordTxt']='Nuevo registro';
$langTxt['common']['genericText']['restore']='Restaurar';
$langTxt['common']['genericText']['restoring']='restaurando';
$langTxt['common']['genericText']['deleting']='borrando';
$langTxt['common']['genericText']['filterByH4']='Filter by';

$langTxt['common']['jsTitle']['TxtHelpOpenRightClick']='Click derecho abre el filtro para esta columna';
$langTxt['common']['jsTitle']['TxtEditLoadingData']='cargando la edici&oacute;n de la fila seleccionada...';
$langTxt['common']['jsTitle']['TxtNewRecordLoading']='cargando la pantalla de creaci&oacute;n de nuevo registro';
$langTxt['common']['jsSubmitEditForm']['updateMessageRecordRefTxt']='Actualizando le mofifiche effettuate, referencia';
$langTxt['common']['jsSubmitEditForm']['insertMessageRecordTxt']='Insertanto el nuevo registro....';
$langTxt['common']['jsGeneric']['UnexpectedError']='Error inesperado';
$langTxt['common']['jsGeneric']['DeleteErrorTxt']='Imposible borrar';

$langTxt['common']['jsBootgrid']['infosLabels']='Visualizando desde la fila <b>{{ctx.start}}</b> hasta la <b>{{ctx.end}}</b> (de <b>{{ctx.total}}</b> filas)';
$langTxt['common']['jsBootgrid']['refreshLabels']='Actualizar el listado';
$langTxt['common']['jsBootgrid']['searchLabels']='Buscar (+2 chars)';
$langTxt['common']['jsBootgrid']['ModuleSettingisEditable']='Editar registro con referencia = ';
$langTxt['common']['jsBootgrid']['ModuleSettingDeleteRecord']='Eliminar registro ID: ';
$langTxt['common']['jsBootgrid']['ModuleSettingEnableDisableStart']='Habilitar/deshabilitar ';
$langTxt['common']['jsBootgrid']['ModuleSettingEnableDisableEnd']=' ID: ';
$langTxt['common']['thBootgrid']['commandsTextHeader']='Acciones';
$langTxt['common']['thBootgrid']['noListData']='no se han podido obtener datos para este m&oacute;dulo';

$langTxt['common']['bbdd']['txtNoDataError']='NO SE HAN ENCONTRADO DATOS';
$langTxt['common']['bbdd']['txtNoResultError']='No se han encontrado resultados';
$langTxt['common']['bbdd']['txtConnectError']='Error en la conexion de la BBDD';
$langTxt['common']['bbdd']['txtNoConnectionBbdd']='<b>No</b> se ha podido establecer la <b>conexi&oacute;n a la base de datos</b>';

$langTxt['common']['editModule']['headerTitle']='Edici&oacute;n del m&oacute;dulo';
$langTxt['common']['editModule']['altExtraFuncText']=' (campo con modal)';
$langTxt['common']['editModule']['btnContractText']='contraer';
$langTxt['common']['editModule']['btnExpandText']='Mostrar';
$langTxt['common']['editModule']['mandatoryAltTxt']='obligatorio';
$langTxt['common']['editModule']['buildFormFileNoExistErrorTxt']=' ERROR: no existe el archivo en el servidor!!!';
$langTxt['common']['editModule']['buildFormFileNoFileStartTitle']='Sin imagen para ';
$langTxt['common']['editModule']['buildFormFileNoFileEndTitle']='el campo';

$langTxt['common']['insertModule']['headerTitle']='Inserci&oacute;n de nuevo registro para el m&oacute;dulo';

$langTxt['common']['notify']['CorrectOperation']='Operación correcta';
$langTxt['common']['notify']['infoNoContentsTitle']='INFORMACI&Oacute;N';
$langTxt['common']['notify']['infoNoContents']='No hay contenidos a mostrar';
$langTxt['common']['notify']['jsRemoveListFilterSuccessStart']='El filtro de b&uacute;squeda';
$langTxt['common']['notify']['jsRemoveListFilterSuccessEnd']='ha sido eliminado';
$langTxt['common']['notify']['jsTitleErrorFilterRemove']='ha sido eliminado';
$langTxt['common']['notify']['jsRemoveListAjaxFilterErrorStart']='Parece que ha ocurrido un error al intentar crear el filtro de b&uacute;squeda para el campo';
$langTxt['common']['notify']['jsRemoveListAjaxFilterErrorEnd']='al intentar construir el filtro de b&uacute;squeda.<br /><br />Si el problema persiste, por favor, contacta con inform&aacute;tica';
$langTxt['common']['notify']['jsInfoAlreadyFilterOpen']='Ya tienes abiero el filtro de b&uacute;squeda';
$langTxt['common']['notify']['jsSearchFilterCorrectly']='Filtro de b&uacute;squeda<br><b>APLICADO correctamente</b>';
$langTxt['common']['notify']['infoActionExecuting']='Ejecutando la acción...';
$langTxt['common']['notify']['restoreError']='Error al restaurar';
$langTxt['common']['notify']['restoreSuccess']='Archivo restaurado';
$langTxt['common']['notify']['deleteError']='Error al borrar';
$langTxt['common']['notify']['deleteSuccess']='Archivo borrado';

$langTxt['common']['popover']['jsMsgTitleIncludeNumericData']='Datos num&eacute;ricos';
$langTxt['common']['popover']['jsMsgContentIncludeNumericData']='Incluir tambi&eacute;n en la b&uacute;squeda para todos los campos num&eacute;ricos de este m&oacute;dulo. Recuerda que a veces los datos representados no corresponden con los almacenados en la BBDD.';
$langTxt['common']['popover']['jsTextIncludeNumericData']='Incluir num&eacute;ricos';

$langTxt['common']['sweetAlert']['recordLoadError']='ERROR AL CARGAR EL REGISTRO SOLICITADO';
$langTxt['common']['sweetAlert']['recordLoadErrorTxt']='Ha ocurrido un error al recuperar los datos, por favor, <b>revisa primero la configuración de este módulo</b>.<br /><br />Si este error persiste, contacta con el departamento de programación.';

$langTxt['common']['modalMsg']['confirmDeleteH4']='Eliminar el registro';
$langTxt['common']['modalMsg']['idTitleWysiEditH4']='Wysi de ';
$langTxt['common']['modalMsg']['TitleWysiEditStdH4']='contenido HTML de ';
$langTxt['common']['modalMsg']['updating']='ACTUALIZANDO';
$langTxt['common']['modalMsg']['waitMessage']='Procediendo con la operación solicitada...';
$langTxt['common']['modalMsg']['confirmDeleteText']='Desea realmente eliminar el registro';
$langTxt['common']['modalMsg']['titleEscClose']='Hacer click o `ESC` para cerrar';
$langTxt['common']['modalMsg']['SaveTitleError']='ERROR AL GUARDAR';
$langTxt['common']['modalMsg']['jsAjaxErrorTxtCode']='Error ajax, c&oacute;digo';
$langTxt['common']['modalMsg']['jsAjaxErrorTxtCodeH4']='(se debe revisar el ajax)';
$langTxt['common']['modalMsg']['UpdatingOperationTitleH4']='ACTUALIZANDO....';
$langTxt['common']['modalMsg']['modalRestoreConfigFileH4']='Restaurar la siguiente copia de seguridad del archivo de configuración?';
$langTxt['common']['modalMsg']['modalRestoreConfigFileNameTxt']='Nombre del archivo';
$langTxt['common']['modalMsg']['modaldeleteConfigFileH4']='(esta es una copia de seguridad) Desea eliminar archivo de configuración?';
$langTxt['common']['modalMsg']['modaldeleteConfigFileTxt']='Desea realmente eliminar la copia de seguridad';

$langTxt['common']['ajaxMsg']['genericErrorResSql']='se debe revisar este m&oacute;dulo';
$langTxt['common']['ajaxMsg']['txtNumRecordsFound']='N&uacute;m registros encontados';
$langTxt['common']['ajaxMsg']['updateConfigFileErrorTxt']='Ha ocurrido un error al intentar actualizar el archivo';
$langTxt['common']['ajaxMsg']['UnableExecuteBackupErrorTxt']='Imposible realizar la copia de seguridad';
$langTxt['common']['ajaxMsg']['unableCreateFolderErrorTxt']='Imposible crear la carpeta para realizar la copia de seguridad';
$langTxt['common']['ajaxMsg']['fileBackupErrorH3']='ERROR en la copia de seguridad';
$langTxt['common']['ajaxMsg']['fileBackupErrorTxt']='Fallo al intentar crear el archivo';
$langTxt['common']['ajaxMsg']['fileBackupErrorFolderTxt']='en la carpeta';
$langTxt['common']['ajaxMsg']['highLightContentFileErrorTxt']='No se ha podido aplicar la función para <b>resaltar la sintaxis</b> del codigo fuente que se debía mostrar para';
$langTxt['common']['ajaxMsg']['getConfigModuleFileFileErrorTxt']='No se ha encontrado o no se ha podido abrir<br>el archivo de configuraci&oacute;n de';
$langTxt['common']['ajaxMsg']['getConfigModuleFileContentsErrorTxt']='No se podido cargar o estaba vac&iacute;o<br>el archivo de configuraci&oacute;n de';
$langTxt['common']['ajaxMsg']['unableRestoreFileTxt']='Imposible restaurar el archivo';
$langTxt['common']['ajaxMsg']['successRestoreFileTxt']='archivo <b>restaurado correctamente</b>';
$langTxt['common']['ajaxMsg']['successDeleteFileTxt']='archivo <b>eliminado correctamente</b>';
$langTxt['common']['ajaxMsg']['infoReloadPageAfterRestoreTxt']='debe recargar el modulo para aplicar los eventuales cambios';
$langTxt['common']['ajaxMsg']['unbelReadTxt']='Imposible leer';
$langTxt['common']['ajaxMsg']['tryingToEraseErrorTxt']='ERROR INTENTANDO BORRAR';

$langTxt['common']['listBtnTitle']['editRecord']='Editar el registro';
$langTxt['common']['listBtnTitle']['deleteRecord']='Eliminar el registro';

$langTxt['common']['btn']['resetMenuLeft']='Resetea tu sesión actual';
$langTxt['common']['btn']['logoutMenuLeft']='Cerrar la sesi&oacute;n';
$langTxt['common']['btnSwalTitle']['HtmlEditNotes']='Notas para la edición';
$langTxt['common']['btnSwalContent']['HtmlEditNotes']="IMPORTANTE:\\n\\r- Recuerda que no puedes utilizar la comilla simple (')\\n\\r- Tampoco se guardarán los datos pertenecientes a campos deshabilitados";
$langTxt['common']['btn']['save']='Guardar';
$langTxt['common']['btn']['cancel']='Cancelar';
$langTxt['common']['btn']['disableRecord']='Deshabilitar el registro';
$langTxt['common']['btn']['enableRecord']='Habilitar el registro';
$langTxt['common']['btn']['insert']='Insertar';
$langTxt['common']['btn']['delete']='Eliminar';
$langTxt['common']['btn']['change']='Cambiar';
$langTxt['common']['btn']['close']='Cerrar';
$langTxt['common']['btn']['zoom']='Ampliar';
$langTxt['common']['btn']['HTMLEditor']='Editor HTML';
$langTxt['common']['btn']['wysiwygViewTitle']='Simple visor Wysiwyg';
$langTxt['common']['btn']['wysiwygViewTxt']='ver Wysiwyg';
$langTxt['common']['btn']['backOfficeHelp']='Ayudas de esta herramienta';
$langTxt['common']['btn']['backOfficeHelpSamll']='+ breves descripciones del B.O.';
$langTxt['common']['btn']['moduleConfigTxt']='Configuraci&oacute;n del m&oacute;dulo';
$langTxt['common']['btn']['onenInNewWindowTxt']='Abrir en nueva ventana';
$langTxt['common']['btn']['backOfficeTopVersionTitle']='Recargar la home del administrador';
$langTxt['common']['btn']['backOfficeTopVersionTxt']='Back-Office v.';
$langTxt['common']['btn']['reloadCurrentPageTitle']='Recargar la p&aacute;gina actual';
$langTxt['common']['btn']['modalRestoreConfigDisplayFileTxt']='Visualizar el contenido';
$langTxt['common']['btn']['deleteFileTxt']='Borrar el archivo';

$langTxt['top']['btn']['fullScreenTxt']='Pantalla completa';
$langTxt['top']['btn']['goToUserConfigTxt']='Ir a la p&aacute;gina de las configuraciones personales';
$langTxt['top']['btn']['openRightPanelTitle']='Ayudas + configuraci&oacute;n del m&oacute;dulo actual';
$langTxt['top']['btn']['preLogOutTitle']='Si deseas cerrar tu sesi&oacute;n...';
$langTxt['top']['btn']['logOutTitle']='Click ahora para CERRAR LA SESI&Oacute;N inmediatamente';

$langTxt['topHeader']['btn']['newRecordTitle']='Insertar un nuevo registro';

$langTxt['rightSideBar']['btn']['backUpDeleteTitle']='Eliminar el archivo de BACK-UP';
$langTxt['rightSideBar']['btn']['viewFileContentsTitle']='Ver los contenios de la copia de seguridad efectuada';
$langTxt['rightSideBar']['label']['previousRestoreTxt']='Restaurar configuraciones anteriores';

$langTxt['common']['jsFn']['commonRestoreErrorFormIntroTxt']='ERROR al intentar almacenar los datos, ';
$langTxt['common']['jsFn']['commonRestoreErrorAjaxNative']='Ha ocurrido un error INESPERADO';
$langTxt['common']['jsFn']['unknownError']='ERROR DESCONOCIDO';

$langTxt['common']['dateFormat']['ymd']='YYYY-MM-DD';
$langTxt['common']['dateFormat']['hm']='HH:mm';

$contL=0;
$wordSep='_';
$tmpWord=null; $tmpValue=null;

foreach($langTxt as $kL=>$vL)
{
	$tmpWord=$kL;

	foreach($vL as $nn=>$tmpValue1)
	{
		$tmpWord_2=null;
		if(is_array($tmpValue1))
		{
			$tmpWord_3=null;

			foreach($tmpValue1 as $nv=>$tmpValue2)
			{
				$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
				define('__'.strtoupper($tmpWord_3), $tmpValue2);
//echo'<br>3- DEFINE=([<b>`'. strtoupper($tmpCurrLang.$wordSep.$tmpWord_3).'`</b>], val = [<b>`'.$tmpValue2.'`</b>]);';
			}
		}
		else
		{
			$tmpWord_2=$tmpWord.$wordSep.$nn;
			define('__'.strtoupper($tmpWord_2), $tmpValue1);
//echo'<br>2- DEFINE=([<b>`'.$tmpWord_2.'`</b>], val = [<b>`'.$tmpValue1.'`</b>]);';
		}
		$contL++;
	}
}

//echo'<br><pre style=color:red;>';print_r($langTxt[$tmpCurrLang]);
//echo'<br><pre style=color:red;>';print_r(get_defined_constants());
//echo'</pre>';
