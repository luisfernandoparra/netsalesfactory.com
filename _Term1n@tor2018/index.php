<?php
/**
 * INDEX PRINCIPAL DEL BACK-OFFICE
 * (DONDE SE CARGARÁ TODO)
 * 
 * @version 1.4
 * @date 2017.11.14
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

if(!isset($_REQUEST['moduleAdmin']))
  header('Location:index.php?moduleAdmin=_modules/home/&modName=Inicio&scName=index.php&modLabel=Home page&modIcon=zmdi-home');

include('../conf/config_web.php');
include('../conf/config.misc.php');
include($dirModAdmin.'mod_bbdd.class.php');

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
$pageTitle='';

if($hashSecutityScript != __HASH_CONTROL_SCRIPT){die(__MSG_ACCESS_ERROR);}

if(empty($_SESSION['id'])){header("Location: _modules/login/");}

if(isset($_REQUEST['clearSession']))
{
	unset($_SESSION['id']);
	unset($_SESSION['user']);
	unset($_COOKIE['cookname']);
	unset($_COOKIE['cooktype']);
	unset($_COOKIE['cookid']);
	session_destroy();
	header('Location:index.php');
	die();
}

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

$_SESSION['currentModuleUrl']=($_SERVER['REQUEST_SCHEME'] ? $_SERVER['REQUEST_SCHEME'] : 'http').'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Back-office <?=($sitesPruebas ? ' '.$siteTitle.$siteVersion : $siteTitle.$siteVersion)?></title>

<?php
// START COMMON AND VENDOR CSS
include('_inc/index_comm_vendor_CSS.php');
$headerTheme=(isset($_SESSION['userAdmin']['pref_']['top_header_theme_bckCol'])) ? substr($_SESSION['userAdmin']['pref_']['top_header_theme_bckCol'],4) : 'blue';
// END COMMON AND VENDOR CSS
?>
<style>
</style>

<script type="text/javascript">
var root_path="<?=$web_url.$adminFolder?>/";

function newEventsMainIndex(tmpHeight)	// NUEVOS EVENTOS CREADOS DINAMICAMENTE QUE DEBEN APPLICAR ELEMENTOS JS DEL INDEX
{
	$('.date-picker-filter').datetimepicker({
		format: 'DD/MM/YYYY'
		,locale: '<?=$adminIso2Lang?>'
	});

	$('.clearDataDate').click(function(){
		var tmpTag=$(this).parents()[0];
		var tmpTag=$(tmpTag).children()[0];
		$(tmpTag).val("")
	});
}
</script>

<?php
// JAVASCRIPT COMMON LIBRARIES
include('_inc/index_javascript_libraries.php');
?>

</head>
<body>

<header id="header" class="clearfix bgm-<?=$headerTheme?> <?=$foreAutoColor?>" data-ma-theme="<?=$headerTheme?>">
<?php
// START BLOCK TOP BLOCKS ELEMENTS
include('_inc/index_top_elements.php');
// END BLOCK TOP BLOCKS ELEMENTS
?>

  <!-- Top Search Content -->
  <div class="h-search-wrap">
    <div class="hsw-inner">
      <i class="hsw-close zmdi zmdi-arrow-left" data-ma-action="search-close"></i>
      <input type="text">
    </div>
  </div>
</header>

<div class="w-100 pull-left p-absolute p-5 text-right" style="margin-top:64px;max-width:92%;z-index:1;">

<?php
/**
 * MOSTRAR PREFERENCIAS DEL USUARIO ACTUAL
 */
if(isset($_SESSION['userAdmin']['pref_']) && $_SESSION['userAdmin']['pref_'])
{
	$btnPreferences='';
	ksort($_SESSION['userAdmin']['pref_']);

	foreach($_SESSION['userAdmin']['pref_'] as $btpPre=>$valBtn)
	{
		$altTxtIcon=str_replace('_', ' ', $btpPre);
		$altTxtIcon=preg_replace('@\\b[a-z]\\b ?@i','',$altTxtIcon);	// REMOVE SINGLE CHAR
		$altTxtIcon=ucwords($altTxtIcon);

		if($btpPre == 'b_filters_location')
		{
//			$iconFiltersLocation=$_SESSION['userAdmin']['pref_']['b_filters_location'] ? 'chevron-left' : 'chevron-up';
			$iconFiltersLocation=$_SESSION['userAdmin']['pref_']['b_filters_location'] ? 'flip-to-back' : 'flip-to-front';
			$btnPreferences.='<i data-toggle="tooltip" data-trigger="hover" data-placement="bottom" data-original-title="'.$altTxtIcon.': '.($_SESSION['userAdmin']['pref_']['b_filters_location'] ? 'LEFT' : 'TOP').'" class="hsw-close zmdi zmdi-'.$iconFiltersLocation.' m-l-5 m-r-5 c-black"></i>';
			continue;
		}
		if($btpPre == 'b_display_sql')
		{
			$btnPreferences.='<i data-toggle="tooltip" data-trigger="hover" data-placement="bottom" data-original-title="'.$altTxtIcon.': '.($_SESSION['userAdmin']['pref_']['b_display_sql'] ? 'YES' : 'NO').'" class="hsw-close zmdi zmdi-apps m-l-5 m-r-5 '.($_SESSION['userAdmin']['pref_']['b_display_sql'] ? 'c-black' : 'c-gray').'"></i>';
			continue;
		}
		if($btpPre == 'ref_lang')
		{
			$btnPreferences.='<span data-toggle="tooltip" data-trigger="hover" data-placement="bottom" data-original-title="Lang: '.($valBtn).'" class="c-black f-12 m-l-5" style="background:url('.$web_url.$adminFolder.'/flags/'.strtolower($valBtn).'.gif)no-repeat center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> ';
			continue;
		}
		if($btpPre == 'background_bckCol')
		{
			$btnPreferences.='<i data-toggle="tooltip" data-trigger="hover" data-placement="bottom" data-original-title="'.$altTxtIcon.': '.$_SESSION['userAdmin']['pref_']['background_bckCol'].'" class="hsw-close zmdi zmdi-tablet-android m-l-5 m-r-5 '.$_SESSION['userAdmin']['pref_']['background_bckCol'].'"></i>';
			continue;
		}
		if($btpPre == 'btn_default_forCol')
		{
			$btnPreferences.='<span class="btn btn-xs '.$_SESSION['userAdmin']['pref_']['btn_default_bckCol'].'" style="padding:0;margin-left:2px;"><i data-toggle="tooltip" data-trigger="hover" data-placement="bottom" data-original-title="'.($altTxtIcon.'')."\n(background button color: ".$_SESSION['userAdmin']['pref_']['btn_default_bckCol'].')" class="hsw-close zmdi zmdi-format-color-text m-l-5 m-r-5 '.$_SESSION['userAdmin']['pref_']['btn_default_forCol'].'"></i></span>';
			continue;
		}
		if($btpPre == 'b_debugSession')
		{
			$btnPreferences.='<i data-toggle="tooltip" data-trigger="hover" data-placement="bottom" data-original-title="'.$altTxtIcon.': '.($_SESSION['userAdmin']['pref_']['b_debugSession'] ? 'YES' : 'NO').'" class="hsw-close zmdi zmdi-developer-board m-l-5 m-r-5 '.($_SESSION['userAdmin']['pref_']['b_debugSession'] ? 'c-black' : 'c-gray').'"></i>';
			continue;
		}
//continue;
//		$btnPreferences.='<i title="'.($altTxtIcon.'_TXT').'" class="hsw-close zmdi zmdi-format-color-text m-l-5 m-r-5"></i> ';
//		$btnPreferences.='<i title="'.($altTxtIcon.'').'" class="hsw-close zmdi zmdi-block m-l-5 m-r-5"></i> ';
//		$btnPreferences.='<button class="btn btn-xs waves-effect" value="1" title="'.($altTxtIcon.'_TXT').'"><i class="hsw-close zmdi zmdi-block m-l-5 m-r-5"></i></button> ';
	}

	echo $btnPreferences;
}
?>
		<!--zmdi-arrow-right-top-->
		<!--<button class="btn btn-xs waves-effect" value="1"><i class="hsw-close zmdi zmdi-block"></i></button>-->
		<!--<i class="hsw-close zmdi zmdi-arrow-in m-l-20" data-ma-action=""></i>-->
	</div>
  
<div class="pull-left p-absolute gridFilters">
<!--<button type='button' class='btn bgm-gray btn-xs waves-effect pull-right'>x</button>-->
<!--ojo: hacer que el z-index sea 10 solo cuando no haya mas elementos DIV con la clase "alert"ahr>y hacer que la x desactive y quite todos los filtros presentes antes de cerrar la columna-->
</div>

<section id="main">
  <aside id="sidebar" class="sidebar c-overflow">
<?php
// START BLOCK LEFT USER SETTINGS
include('_inc/index_left_user_config.php');
// END BLOCK LEFT USER SETTINGS
?>

<?php
// START BLOCK LEFT MENU
include('_inc/index_left_menu.php');
// END BLOCK LEFT MENU
?>
  </aside>

<?php
// START RIGHT SIDEBAR
include('_inc/index_right_sidebar.php');
// END RIGHT SIDEBAR
?>

  <section id="content">

		<div class="modal fade" data-modal-color="white" id="modalEmailSend" tabindex="-1" >
<script>
$(document).ready(function(){

	$("#awdSendForm").submit(function(){
		var dataSend=new FormData($("#awdSendForm")[0]);
		dataSend.append("urlWs", "<?=$urlAmazonAwsApiSes?>");
//console.log("---->",this);

		$.ajax({
			data:dataSend
			,url:"<?=$web_url?>ajax/ajax_vtp_awd_ws.php"
			,method:"post",dataType:"json",cache:false,async:true,contentType:false,processData:false
			,success:function(response){

				if(!response.success)
				{
					commonNotify("<b><?=__COMMON_GENERICTEXT_ERROR?></b>", "<br /><br />No ha sido posible enviar el email:<br>"+(response.txt ? response.txt : ""), undefined, "left", undefined, 'danger', "animated flipInY", "rotateOut",5000);	// MENSAJE NO INVASIVO
					return false;
				}

				$("#modalEmailSend").modal('toggle');
				commonNotify("Mensaje enviado correctamente","", undefined, "center", undefined, 'success', "animated flipInX", "zoomOut", response.timeWait ? response.timeWait : 3000);
				return false;
			}
			,error:function(response){
				commonNotify("<b><?=__COMMON_GENERICTEXT_ERROR?></b>", "<br /><br />Error en el AJAX!!", undefined, "left", undefined, 'danger', "animated flipInX", "rotateOut",9000);	// MENSAJE NO INVASIVO
				return false;
			}
		});

		return false;
	});
});
</script>

			<div class="modal-dialog">
				<div class="modal-content c-black">
					<div class="modal-header">
						<h4 class="modal-title c-black">Env&iacute;o email</h4>
					</div>
					<form action="" method='post' id='awdSendForm' name='awdSendForm' onsubmit='//return false;' enctype='multipart/form-data'>
					<div class="modal-body c-black">
						<p><input type="text" name="awd_address" value="" class="form-control" placeholder="Destinatario" required="required" /></p>
						<p><input type="text" name="awd_subject" value="" class="form-control" placeholder="Sujeto" required="required" /></p>
						<p>Contenido: <textarea type="text" name="awd_body" class="directEditField p-10" style="max-height:100hv;width:100%;" rows="4"></textarea>
		<!--<a href="#htmlWysiBox" class="btn btn-xs bgm-cyan waves-effect modalHtmlWysiBox" title="" data-toggle="modal" data-field-name="title_article" data-original-title="Semplice visore Wysiwyg">Vedere Wysiwyg</a>-->
						</p>
					</div>
					<div class="modal-footer c-black">
						<button type="submit" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect actAbortDelete" >Enviar</button>
						<button type="button" class="btn btn-default <?=$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> waves-effect actAbortDelete" data-dismiss="modal"><?=__COMMON_BTN_CANCEL?></button>
					</div>

					</form>
				</div>
			</div>
		</div>

    <div class="container">

<!-- START TOP HTML HEADER -->
      <div class="block-header">
      <h2><?=$pageTitle?></h2>
<?php
include('_inc/index_top_html_header.php');
?>
      </div>
<!-- END TOP HTML HEADER -->

			<div class="card">

<?php
/**
 * START BLOCK BODY CONTENTS
 */

// START LOCAL MODULE LANGUAGE TRANSLACTIONS
if(isset($_SESSION['userAdmin']['pref_']['ref_lang']) && isset($_REQUEST['moduleAdmin']))
{
	$dirLang=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$_REQUEST['moduleAdmin'].'lang/';
	$filLang=$dirLang.'module_'.strtolower($_SESSION['userAdmin']['pref_']['ref_lang']).'.php';

	if(file_exists($filLang))
	{
		include($filLang);
		$wordSep='_'; $tmpWord=null; $tmpValue=null;

		foreach($langTxt as $kL=>$vL)
		{
			$tmpWord=$kL;

			foreach($vL as $nn=>$tmpValue1)
			{
				$tmpWord_2=null;
				if(is_array($tmpValue1))
				{
					$tmpWord_3=null;

					foreach($tmpValue1 as $nv=>$tmpValue2)
					{
						$tmpWord_3=$tmpWord.$wordSep.$nn.$wordSep.$nv;
						define('__'.strtoupper($tmpWord_3), $tmpValue2);
					}
				}
				else
				{
					$tmpWord_2=$tmpWord.$wordSep.$nn;
					define('__'.strtoupper($tmpWord_2), $tmpValue1);
				}
			}
		}
	}
}
// END LOCAL MODULE LANGUAGE TRANSLACTIONS

include('_inc/index_body_contents.php');
/**
 * END BLOCK BODY CONTENTS
 */
?>
			</div>
    </div>
  </section>
</section>
<?php
include('_inc/index_footer.php');
?>

<!-- Page Loader -->
<!--
<div class="page-loader">
  <div class="preloader pls-green">
    <svg class="pl-circular" viewBox="25 25 50 50">
      <circle class="plc-path" cx="50" cy="50" r="20" />
    </svg>
    <p>Cargando...</p>
  </div>
</div>
-->
<?php
// OLDER I-EXPLORER HTML
//include('_inc/index_older_iexplorer.php');
//echo '<section id="content"><div class="container"><pre>';
//print_r($_SESSION['userAdmin']['pref_']);
//print_r($_SESSION);
////print_r(@$arraExtVars);
//echo '</pre></div></section>';

if(isset($htmlWysiwigEditor))
	echo $htmlWysiwigEditor;

?>
<div class="modal fade" data-modal-color="bluegray" id="modDataGridFilters" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
      <button type="button" class="close f-20 m-r-15 m-t-5 c-white" data-dismiss="modal">&times;</button>
			<div class="modal-header">
        <h4 class="modal-title"><?=__COMMON_GENERICTEXT_FILTERBYH4?><span class="f-700 headerTitField"></span></h4>
			</div>
			<div class="modal-body waitMessage">
				<p><span class="headerContentFilter"></span></p>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" data-modal-color="white" id="userModuleNotes" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" style="background:#ECF2B4;">
      <button type="button" class="close f-20 m-r-15 m-t-5 c-red" data-dismiss="modal">&times;</button>
			<div class="modal-header">
        <h4 class="modal-title c-green personalNotes m-b-10"><?=__PLUGINNOTES_MODALTITLE_TXT?><span class="f-700 headerTitField"></span></h4>
			</div>
			<div class="modal-body o-auto contentUserNote" style="max-height:80vh;"></div>
		</div>
	</div>
</div>

<div class="modal fade " data-modal-color="white" id="phpMyAdminMini" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
<?php
//unset($_SESSION['XSS']);
//if(!isset($_SESSION['XSS']))
//{
//	$result='';
//	$chars=preg_split('//','ABCDEFabcdef0123456789');
//	for($i=0;$i<16;$i++)$result.=$chars[rand(0,count($chars)-1)];
//	$_SESSION['XSS']=$result;//get_rand_str(16);
//}
//$xurl='&XSS='.$_SESSION['XSS'];
?>
	<div class="modal-dialog modal-xs">
		<div class="modal-content bgm-<?=$headerTheme?>">
      <button type="button" class="close f-22 m-r-15 m-t-5 c-white" data-dismiss="modal">&times;</button>
			<div class="modal-header p-10">
        <h5 class="modal-title c-white headerPhpMyAdminMini"><i class="him-icon fa fa-database"></i> Mini phpMyAdmin<span class="f-700 headerTitField"></span></h5>
			</div>
			<div class="modal-body o-auto contentPhpMyAdminMini p-t-5" style="max-height:87vh;background:#fff;	">
				<iframe style='height:86vh;width:100%;' class="" id='ifrPhpMyAdminMini' name='ifrPhpMyAdminMini' src='#null' frameborder='0' marginheight='0' marginwidth='0' scrolling='YES' width='100%'></iframe>
			</div>
		</div>
	</div>
</div>

</body>
</html>
