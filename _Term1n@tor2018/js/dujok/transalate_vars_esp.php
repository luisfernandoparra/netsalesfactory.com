<?php
/**
 * LANG SPANISH
 *
 * ELEMENTOS PARA CONSTRUIR EL OBJETO "objLang"
 * que contiene los textos del idioma activo
 * para el script "dujok.js"
 *
 * UTILIZAR: objLang.myPropertie
 *
 * M.F. 2017.01.18
 */
$strOut='';
$arrJsDujokLang['notify']['errorTxtMsg']='Errore, non è possibile disattivare il record selezionato<br />!';//Errore, non è possibile disattivare il record selezionato!
$arrJsDujokLang['notify']['errorTxtTit']='questo il titolare';
$arrJsDujokLang['notify']['disabledRecordTxt']='Deshabilitado';
$arrJsDujokLang['notify']['enabledRecordTxt']='Habilitado';
$arrJsDujokLang['notify']['recordIdTxt']='registro ID: <b>';

$arrJsDujokLang['ajax']['introErrorCodeTxt']='Error ajax, c&oacute;digo ';
$arrJsDujokLang['ajax']['succesHtmlMsg']='<b>Operación correcta</b>';
$arrJsDujokLang['ajax']['errorWarningTitle']='ERROR';
$arrJsDujokLang['ajax']['statusTxt']='estado';
$arrJsDujokLang['ajax']['commonAjaxFailTxt']='(se debe revisar el AJAX)';
$arrJsDujokLang['ajax']['loadErrorTitle']='ERROR EN LA CARGA';
$arrJsDujokLang['ajax']['errorWarningTxt']='Ha ocurrido un error grave en el AJAX, por favor, resisa primero la configuración de este módulo.';
$arrJsDujokLang['ajax']['unexpectedErrorTxt']='Ha ocurrido un error INESPERADO';
$arrJsDujokLang['ajax']['unknownErrorTxt']='ERROR DESCONOCIDO';
$arrJsDujokLang['ajax']['errorDeleteRecordId']='Fallo al intentar eliminar el record ID';
$arrJsDujokLang['ajax']['errorCopyRecordId']='Imposibile copiar el registro ID';

$arrJsDujokLang['basicHelp']['errorMsgHeaderTxt']='Lo sentimos, pero ha ocurrido el siguiente error:';
$arrJsDujokLang['basicHelp']['savingBottomTxt']='Guardando, por favor, espera...';

$arrJsDujokLang['enableDisableRowId']['errorNoDisableRow']='Error, no se puede deshabilitar el registro indicado!';

$arrJsDujokLang['commonMsg']['errorMsgBottomTxt']='Disculpa las molestias.';
$arrJsDujokLang['commonMsg']['closeTxt']='Cerrar';
$arrJsDujokLang['commonMsg']['loadingTxt']='Cargando...';
$arrJsDujokLang['commonMsg']['forFileTxt']='Para el archivo';

foreach($arrJsDujokLang as $index=>$value)
{
	if(!is_array($value))	// PARA ARRAYS CON UN SOLO INDICES
		$strOut.=$index.':\''.str_replace('\'','`',$value).'\',';
	else
		foreach($value as $ind=>$val)	// PARA ARRAYS CON 2 INDICES
			$strOut.=$ind.':\''.str_replace('\'','`',$val).'\',';
}

if(isset($strOut) && count($strOut))
	$strOut=substr($strOut,0,-1);
?>

<script type="application/javascript">
var objLang={<?=$strOut?>};
</script>
