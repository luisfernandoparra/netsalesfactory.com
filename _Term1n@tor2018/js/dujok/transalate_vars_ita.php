<?php
/**
 * LANG ITALIAN
 *
 * ELEMENTOS PARA CONSTRUIR EL OBJETO "objLang"
 *
 * EL PRIMER ELEMENTO DEL OBJETO SIRVE SOLO PARA
 * INDICAR EL ÁMBITO DE DONDE SE DEBE UTILIZAR LA VARIABLE
 * que contiene los textos del idioma activo
 * para el script "dujok.js"
 *
 * UTILIZAR: objLang.myPropertie
 *
 * M.F. 2017.01.18
 */
$strOut='';
$arrJsDujokLang['notify']['errorTxtMsg']='Errore, non è possibile disattivare il record selezionato<br />!';//Errore, non è possibile disattivare il record selezionato!
$arrJsDujokLang['notify']['errorTxtTit']='questo il titolare';
$arrJsDujokLang['notify']['disabledRecordTxt']='Disattivato';
$arrJsDujokLang['notify']['enabledRecordTxt']='Attivato';
$arrJsDujokLang['notify']['recordIdTxt']='record ID: <b>';

$arrJsDujokLang['ajax']['introErrorCodeTxt']='Errore AJAX, codice ';
$arrJsDujokLang['ajax']['succesHtmlMsg']='<b>Operazione corretta</b>';
$arrJsDujokLang['ajax']['errorWarningTitle']='ERRORE';
$arrJsDujokLang['ajax']['statusTxt']='stato';
$arrJsDujokLang['ajax']['commonAjaxFailTxt']='(dovrebbe rivedere l`AJAX)';
$arrJsDujokLang['ajax']['loadErrorTitle']='ERRORE CARICANDO I DATI';
$arrJsDujokLang['ajax']['errorWarningTxt']='Un errore grave si è verificato nell `AJAX`, si prega di controllare la configurazione di questo modulo.';
$arrJsDujokLang['ajax']['unexpectedErrorTxt']='Si è verificato un errore IMPREVISTO';
$arrJsDujokLang['ajax']['unknownErrorTxt']='ERRORE SCONOSCIUTO';
$arrJsDujokLang['ajax']['errorDeleteRecordId']='Impossibile eliminare il record ID';
$arrJsDujokLang['ajax']['errorCopyRecordId']='Impossibile copiare il record ID';

$arrJsDujokLang['basicHelp']['errorMsgHeaderTxt']='Ci dispiace, ma si è verificato il seguente errore:';
$arrJsDujokLang['basicHelp']['savingBottomTxt']='Salvando il contenuto, attendere un attimo...';

$arrJsDujokLang['enableDisableRowId']['errorNoDisableRow']='Errore, non è possibile disattivare questo record!';

$arrJsDujokLang['commonMsg']['errorMsgBottomTxt']='Scusa per l`inconveniente.';
$arrJsDujokLang['commonMsg']['closeTxt']='Chiudere';
$arrJsDujokLang['commonMsg']['loadingTxt']='Caricando...';
$arrJsDujokLang['commonMsg']['forFileTxt']='Per il file';

foreach($arrJsDujokLang as $index=>$value)
{
	if(!is_array($value))	// PARA ARRAYS CON UN SOLO INDICES
		$strOut.=$index.':\''.str_replace('\'','`',$value).'\',';
	else
		foreach($value as $ind=>$val)	// PARA ARRAYS CON 2 INDICES
			$strOut.=$ind.':\''.str_replace('\'','`',$val).'\',';
}

if(isset($strOut) && count($strOut))
	$strOut=substr($strOut,0,-1);
?>

<script type="application/javascript">
var objLang={<?=$strOut?>};
//console.log(objLang);
</script>
