/**
 * SCRIPT PROPIO DEL PROYECTO
 */

/**
 * SERIALIZA LOS ELEMENTOS DEL OBJETO RECIBIDO
 * LISTO PARA ENVIO POR URL
 * 
 * @param  obj
 * @returns {string}
 */
function serializeDataArray(obj, arrName){
  return Object.keys(obj).reduce(function(a,k){a.push(arrName+'['+k+']='+encodeURIComponent(obj[k]));return a},[]).join('&');
}

/**
 * HABILITA / DESHABILTAR REGISTRO DE UNA TABLA
 * @param {type} tmpId
 * @param {type} tmpTable
 * @returns {undefined}
 */
function enableDisableRowId(tmpId, tmpTable, newStatus, fieldNameStatusRow, e)
{
	if(tmpTable == "information_schema.TABLES")
	{
		notify(objLang.errorNoDisableRow+"<br ><h3 style=color:#fff;>"+tmpId+"</h3>", "danger");
		return false;
	}

	$.ajax({
	url: "ajax/common_admin.php", method: "post", dataType: "json",
	data:{
		action: "newStatusRecord",
		id: tmpId,
		newStatusValue: newStatus,
		tblName: tmpTable,
		fieldNameToSet: fieldNameStatusRow
	}, cache: false, async: true,
	success:function(response){
		newStatus=!response.newStatus;

    if(!response.success)
    {
      swal(objLang.errorWarningTitle, objLang.errorWarningTxt, "error");
      return false;
    }

		if(newStatus)	// UPDATE STATUS LIST ROW
		{
			if(e == undefined)
				document.getElementById("ifrListBlock").contentWindow.$("tr[data-row-id='"+tmpId+"']").removeClass("warning");
			else
				$("tr[data-row-id='"+tmpId+"']").removeClass("warning");
		}
		else
		{
			if(e == undefined)
				document.getElementById("ifrListBlock").contentWindow.$("tr[data-row-id='"+tmpId+"']").addClass("warning");
			else
				$("tr[data-row-id='"+tmpId+"']").addClass("warning");
		}

		// MESSEGE NEW RECORD STATUS
		window.parent.commonNotify('<i class=\''+(newStatus ? 'f-20 zmdi zmdi-check' : 'f-20 c-yellow zmdi zmdi-block' )+'\'> </i>&nbsp;&nbsp;'+(newStatus ? "<span class=\'p-r-5 p-l-5 c-green bgm-white\'>"+objLang.enabledRecordTxt+"</span>" : "<span class=\'c-yellow\'>"+objLang.disabledRecordTxt+"</span>")+' '+objLang.recordIdTxt+tmpId+'</b>', "", undefined, undefined, undefined, 'success', undefined, undefined);
		return;
	}
	, error:function(response){
			swal(objLang.errorWarningTitle, objLang.unknownErrorTxt, "error");
			return false;
		}
	});
	return true;
}


$(document).ready(function(){
	var tmpHeight=$("#mCSB_1").height();

	$(".preLogoutCss").click(function(){
		$(".logoutCssTop").removeClass("hidden");
		$(this).addClass("hidden");
	})

	$(".openNewWindow").click(function(){
		var helpWidth=900;
		var helpHeight=screen.height-50;
		var posLeft=(screen.width-helpWidth)/2;
		var posTop=(screen.height-helpHeight)/3;
		posLeft=posLeft>0 ? posLeft : 0;
		posTop=posTop>0 ? posTop : 0;
    window.open(root_path+"_inc/external_modal.php?renderHtml=1&action=getHelpModule&modulePath="+$(this).attr("data-module-path")+"&moduleTitle="+$(this).attr("data-module-caption"), "_blank", "location=no,menubar=yes,scrollbars=yes,top="+posTop+",left="+posLeft+",resizable=yes,width=900,height=900");
	})

	$(".logoutCssTop").mouseleave(function(){
		$(".preLogoutCss").removeClass("hidden");
		$(this).addClass("hidden");
	})

	$(".logoutCss, .logoutCssTop").click(function(){
		$.ajax({
			url:root_path + "ajax/common_admin.php",
			method: "post",
			dataType: "json",
			data:{action:"logout"},
			cache: false
			,success: function (response)
			{
				if(response.success)
					document.location=".";
			}
		});
	});

//  $(".hideLeftSidebar").click(function(){
//console.log(this)
//    if($(this).children().hasClass("zmdi-arrow-back"))
//    {
//      $(this).children().removeClass("zmdi-arrow-back");
//      $(this).children().addClass("zmdi-arrow-forward")
////      $("#sidebar").fadeOut().css("width",0);
//      return;
//    }
//    else
//    {
//      $(this).children().addClass("zmdi-arrow-back");
//      $(this).children().removeClass("zmdi-arrow-forward")
//    }
//  });


	/**
	 * COMMON ACTIONS FOR MODAL EDIT BLOCK ON BOTTOM "CANCEL" BUTTON
	 */
	$(".actCancel").click(function(){
		$(".headerAdditionalInfo").addClass("hide");
		$("#ifrmEditBlock").attr("src","#null");
		$("#ifrmNewBlock").attr("src","#null");
		$("#boxContentEditRecord").hide();
		$("#boxContentNewRecord").hide();
		$(".contentForm").hide();
    $(".saveButton").hide();
    $(".saveButton").attr("disabled",false);
    $(".insertButton").hide();
    $(".insertButton").attr("disabled",false);
    $(".enableDisableButton").attr("disabled",false);
    $(".actCancel").attr("disabled",false);
		$(".enableDisableButton").hide();
	});


	$('#enableDisableRow').click(function(e){
		e.preventDefault();
		var nFrom=$(this).attr('data-from');
		var nAlign=$(this).attr('data-align');
		var nIcons=$(this).attr('data-icon');
		var nType=$(this).attr('data-type');
		var nAnimIn=$(this).attr('data-animation-in');
		var nAnimOut=$(this).attr('data-animation-out');
		var nTitle=$(this).attr('data-title');
		//PARAMS:  title, message, from, align, icon, type, animIn, animOut
		commonNotify(nTitle, "", nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut);
	 });


   $(".basicHelpOpen").click(function(){	// MODAL DE AUDAS DEL MODULO ACTIVO
		$.ajax({
			url:root_path + "ajax/common_admin.php",
			method: "post",
			dataType: "json",
			data:{action:'getHelpModule','modulePath':$(this).attr("data-module-path"),'moduleTitle':$(this).attr("data-module-caption")},
			cache: false
			,success: function (response)
			{
				if(response.success)
        {
					// AQUI SE DIBUJA TODO EL CAONTENIDO CARGADO DESDE EL AJAX PARA LA VENTANA MODAL DE ASL AYUDAS (TODOS LOS CONTENIDOS VISUALIZADOS)
					if(response.txtContents)
						$(".helpModuleBody").html(response.txtContents);

					$(".configModuleBody").css("max-height",(tmpHeight-100)+"px");
        }
        else
        {
          $(".helpModuleBody").html("<div class='card-body card-padding'><div class='alert alert-dismissible alert-danger' role='alert'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span></button>"+objLang.errorMsgHeaderTxt+"</div></div><center><div class='m-10 m-b-0 alert alert-warning bgm-gray z-depth-2'>"+response.errorText+"</div><br /><br />"+objLang.errorMsgBottomTxt+"<br /></center><br /><div class='text-right fg-float m-r-20'><button type='button' class='btn bgm-gray waves-effect' data-dismiss='modal'>"+objLang.closeTxt+"</button></div><br />");
        }
			}
		});
   })


   // MODAL EDICION ARCHIVO DE CONFIGURACION DEL MODULO ABIERTO
   $(".configFileOpen").click(function(){	// MODAL PARA VISUALIZAR EL ARCHIVO DE CONFIGURACION DEL MODULO ABIERTO
		$(".configModuleBody").html("<div class='' style='overflow:hidden;width:90%:auto;max-height:200px;'><center><i>"+objLang.loadingTxt+"</i><br /><br /><div class='preloader pl-xxl'><svg class='pl-circular' viewBox='25 25 50 50'><circle class='plc-path' cx='50' cy='50' r='20'></circle></svg></div></center></div>");
		var tmpPath=$(this).attr("data-module-path");

		$.ajax({
			url:root_path + "ajax/common_admin.php",
			method: "post",
			dataType: "json",
			data:{action:'getConfigData','modulePath':tmpPath,'moduleTitle':$(this).attr("data-module-caption")},
			cache: false
			,success: function (response)
			{
				if(response.success)
        {
					if(response.txtContents)
						$(".configModuleBody").html("<div>"+response.txtContents+"</div>");
					else
					{
						$(".configModuleBody").html("<div>"+objLang.loadErrorTitle+"</div>");
						return false;
					}

					$(".configModuleBody").css("max-height",(tmpHeight-100)+"px");
					$(".scrContents").fadeIn();

					// SAVE CONFIG MODULE EDITING
					$(".hec-save").click(function(){
            $(this).hide().parent().prepend("<br /><br /><div class='p-10 p-b-0 c-black bgm-cyan '><em>"+objLang.savingBottomTxt+"</em><br /></div>");
						$('.configModuleBody').animate({scrollTop:9999},2000);

            $.ajax({
              url:root_path + "ajax/common_admin.php"
              ,method:"post" ,dataType:"json", cache: false
              ,data:{action:'updateConfigModule', fileContent:$("#note-editor-1").html(),fileName:"config_module.php",pathFile:tmpPath}
              ,success: function (response)
              {
                if(response.success)
                {
									$('#configFile').modal('toggle');
									var tmpTitle=(response.msgTitle ? response.msgTitle : objLang.succesHtmlMsg);
									var tmpMsg=(response.msgText ? "<br />"+response.msgText : "");
                  commonNotify(tmpTitle, tmpMsg, undefined, "center", undefined, 'success', "animated flipInX", "flipOutX", 7000);
                }
                else
                {
									var responseMsg=response.errorText ? response.errorText : objLang.introErrorCodeTxt+(response.status == 200 ? response.status+"<br /><br /><h4 class='c-yellow'>"+objLang.commonAjaxFailTxt+"</h4>" : response.status);
									$(".configModuleBody").html("<div class='card-body card-padding'><div class='alert alert-dismissible alert-danger' role='alert'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span></button>"+objLang.errorMsgHeaderTxt+"</div></div><center><div class='m-10 m-b-0 alert alert-warning bgm-gray z-depth-2'>"+responseMsg+"<br /><h2>"+objLang.forFileTxt+" `<i>config_module.php</i>`</h2></div><br /><br />"+objLang.errorMsgBottomTxt+"<br /></center><br /><div class='text-right fg-float m-r-20'><button type='button' class='btn bgm-gray waves-effect' data-dismiss='modal'>"+objLang.closeTxt+"</button></div><br />");
                }
              }
							,error:function(response){	//AJAX ERROR
								var responseMsg=response.responseText ? response.responseText : objLang.introErrorCodeTxt+(response.status == 200 ? response.status+"<br /><br /><h4 class='c-yellow'>"+objLang.commonAjaxFailTxt+"</h4>" : response.status);
								$(".configModuleBody").html("<div class='card-body card-padding'><div class='alert alert-dismissible alert-danger' role='alert'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span></button>"+objLang.errorMsgHeaderTxt+"</div></div><center><div class='m-10 m-b-0 alert alert-warning bgm-gray z-depth-2'>"+responseMsg+"</div><br /><br />"+objLang.errorMsgBottomTxt+"<br /></center><br /><div class='text-right fg-float m-r-20'><button type='button' class='btn bgm-gray waves-effect' data-dismiss='modal'>"+objLang.closeTxt+"</button></div><br />");
								return false;
							}
            });
					});
        }
        else
        {
          $(".configModuleBody").html("<div class='card-body card-padding'><div class='alert alert-dismissible alert-danger' role='alert'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span></button>"+objLang.errorMsgHeaderTxt+"</div></div><center><div class='m-10 m-b-0 alert alert-warning bgm-gray z-depth-2'>"+response.errorText+"</div><br /><br />"+objLang.errorMsgBottomTxt+"<br /></center><br /><div class='text-right fg-float m-r-20'><button type='button' class='btn bgm-gray waves-effect' data-dismiss='modal'>"+objLang.closeTxt+"</button></div><br />");
        }
			}
			,error:function(response){	//AJAX ERROR
				var responseMsg=response.responseText ? response.responseText : objLang.introErrorCodeTxt+(response.status == 200 ? response.status+"<br /><br /><h4 class='c-yellow'>"+objLang.commonAjaxFailTxt+"</h4>" : response.status);
				$(".configModuleBody").html("<div class='card-body card-padding'><div class='alert alert-dismissible alert-danger' role='alert'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span></button>"+objLang.errorMsgHeaderTxt+"</div></div><center><div class='m-10 m-b-0 alert alert-warning bgm-gray z-depth-2'>"+responseMsg+"</div><br /><br />"+objLang.errorMsgBottomTxt+"<br /></center><br /><div class='text-right fg-float m-r-20'><button type='button' class='btn bgm-gray waves-effect' data-dismiss='modal'>"+objLang.closeTxt+"</button></div><br />");
				return false;
			}
		});
   });

	$("body").on("click",".logoutAll",function(){
		$.ajax({
			url:root_path + "ajax/common_admin.php",
			method: "post",
			dataType: "json",
			data:{action:10},
			cache: false
			,success: function (response)
			{
				if(response.success)
					document.location=".";
			}
		});
	});

	setInterval('updateClock()', 60000);
	updateClock();
});

var first_input="";

function commonDeleteAction(res, ajaxNative, success)
{
	if(success)
	{
		window.parent.commonNotify(res.msgTitle ? res.msgTitle : objLang.succesHtmlMsg, res.msgText ? "<br />"+res.msgText : "", undefined, "center", undefined, 'success', "animated flipInX", undefined);
			$('#modaldeleteRow').modal('toggle');
		return;
	}

	var	introText=res.introText ? res.introText : objLang.errorDeleteRecordId+res.msgPainTxt;
	var moreErrInfo="";

	if(ajaxNative)
	{
		introText=objLang.unexpectedErrorTxt;
		moreErrInfo=res.status ? "\n ("+objLang.statusTxt+"="+res.status : "";
		moreErrInfo+=res.statusText ? ", "+res.statusText+")" : ")";
	}
	else
	{
		moreErrInfo="\n"+(res.errorText ? res.errorText : objLang.unknownErrorTxt);
	}

	swal({
		title: objLang.errorWarningTitle,
		text: introText+moreErrInfo,
		type: "error",
		showCancelButton: false,
		confirmButtonText: objLang.closeTxt,
		closeOnConfirm: false,
		closeOnCancel: false,
		timer:10000,
	});

//  setTimeout("commonRestoreFormElements();",1500);
	return;
}


function setFieldFocus()
{
	first_input.focus();
}

function commonDeleteRecord(e, localData)
{
	$.ajax({
		data:localData
		,url:root_path + "ajax/common_admin.php", method: "post", dataType: "json", cache: false, async: true
		,success:function(response){
			if(!response.success)// && !response.msgTitle
			{
				commonDeleteAction(response, 0, 0);
				return false;
			}
			else
				commonDeleteAction(response, 0, 1);

			document.getElementById("ifrListBlock").contentWindow.$("#grid-data-api").bootgrid('reload');
			return false;
		}
		,error:function(response){
				commonDeleteAction(response, 1, 0);
				return false;
		}
	});
};

function updateClock()
{
	var currentTime=new Date();
	var currentHours=currentTime.getHours();
	var currentMinutes=currentTime.getMinutes();

	// Pad the minutes and seconds with leading zeros, if required
	currentMinutes=(currentMinutes < 10 ? "0" : "") + currentMinutes;

	// Choose either "AM" or "PM" as appropriate
	var timeOfDay=(currentHours < 12) ? "AM" : "PM";

	// Convert the hours component to 12-hour format if needed
	currentHours=(currentHours > 12) ? currentHours - 12 : currentHours;

	// Convert an hours component of "0" to "12"
	currentHours=(currentHours == 0) ? 12 : currentHours;

	// Compose the string for display
	var currentTimeString=currentHours+":"+currentMinutes+" "+timeOfDay;
	$(".topClock").html(currentTimeString);
}