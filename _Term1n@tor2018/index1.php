<?php
/**
 * INDEX SCRIPT -- INDEX HOME
 * 
 */
//@session_start();

session_start();
if(empty($_SESSION['id']))
    header("Location: login.php");

$arrCommonMenus=array(
  'label' => ['home' => 'Página inicial', 'calendar' => 'Calendario'],
  'link' => ['home' => '.', 'calendar' => 'calendar.php']
);

//echo'<pre>';print_r($_SERVER['SCRIPT_NAME']);die();
?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Back-office 2016-1</title>

<?php
// START COMMON AND VENDOR CSS
include('_inc/index_comm_vendor_CSS.php');
// END COMMON AND VENDOR CSS
?>

</head>
<body>

<header id="header" class="clearfix" data-ma-theme="blue">
<?php
// START BLOCK TOP BLOCKS ELEMENTS
include('_inc/index_top_elements.php');
// END BLOCK TOP BLOCKS ELEMENTS
?>

  <!-- Top Search Content -->
  <div class="h-search-wrap">
    <div class="hsw-inner">
      <i class="hsw-close zmdi zmdi-arrow-left" data-ma-action="search-close"></i>
      <input type="text">
    </div>
  </div>
</header>

<section id="main">
  <aside id="sidebar" class="sidebar c-overflow">
<?php
// START BLOCK LEFT USER SETTINGS
include('_inc/index_left_user_config.php');
// END BLOCK LEFT USER SETTINGS
?>

<?php
// START BLOCK LEFT MENU
include('_inc/index_left_menu.php');
// END BLOCK LEFT MENU
?>
  </aside>

<?php
// START RIGHT SIDEBAR
include('_inc/index_right_sidebar.php');
// END RIGHT SIDEBAR
?>

  <section id="content">
    <div class="container">

<!-- START TOP HTML HEADER -->
      <div class="block-header">
      <h2>Dashboard</h2>
<?php
include('_inc/index_top_html_header.php');
?>
      </div>
<!-- END TOP HTML HEADER -->

<?php
// BLOCK BODY CONTENTS
include('_inc/index_body_contents.php');
?>
    </div>
  </section>
</section>
<?php
include('_inc/index_footer.php');
?>

<!-- Page Loader -->
<div class="page-loader">
  <div class="preloader pls-green">
    <svg class="pl-circular" viewBox="25 25 50 50">
      <circle class="plc-path" cx="50" cy="50" r="20" />
    </svg>
    <p>Cargando...</p>
  </div>
</div>

<?php
// OLDER I-EXPLORER HTML
include('_inc/index_older_iexplorer.php');

// JAVASCRIPT LIBRARIES
include('_inc/index_javascript_libraries.php');
?>
</body>
</html>
