<?php
/**
 * INDEX SCRIPT -- OLDER I-EXPLORER HTML
 * 
 */
@session_start();
?>
<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
  <h1 class="c-white">atención!!</h1>
  <p>Usted está utilizando una versión antigua de Internet Explorer,<br>por favor, actualice
a cualquiera de los navegadores web para acceder a este sitio web.</p>
  <div class="iew-container">
    <ul class="iew-download">
      <li>
        <a href="http://www.google.com/chrome/">
          <img src="img/browsers/chrome.png" alt="">
          <div>Chrome</div>
        </a>
      </li>
      <li>
        <a href="https://www.mozilla.org/en-US/firefox/new/">
          <img src="img/browsers/firefox.png" alt="">
          <div>Firefox</div>
        </a>
      </li>
      <li>
        <a href="http://www.opera.com">
          <img src="img/browsers/opera.png" alt="">
          <div>Opera</div>
        </a>
      </li>
      <li>
        <a href="https://www.apple.com/safari/">
          <img src="img/browsers/safari.png" alt="">
          <div>Safari</div>
        </a>
      </li>
      <li>
        <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
          <img src="img/browsers/ie.png" alt="">
          <div>IE (Nuevo)</div>
        </a>
      </li>
    </ul>
  </div>
  <p>Lamentamos las molestias!</p>
</div>
<![endif]-->
