<?php
/**
 * COMMON SCRIPT -- BLOCK LEFT MENU
 *
 * FROM index.php
 * 
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$outMenuLeft='';
$contPos=0;
?>

<ul class="main-menu bgm-bluegray">
<?php
include($dirModAdmin.'mod_common_menu_left.class.php');	// MODULO PARA TODOS LOS METODOS LOGICOS (BBDD) DEL MODULO DEL MENU IZQUIERSO
$frameworkData=new CommonDataMenuLeft($db_type, $db_host, $db_user, $db_pass, $db_name);
$resConnexion=$frameworkData->connectDB();
//$arrDataMenuLeft=$frameworkData->getDataMenuLeft();

include($dirClassesAdmin.'menu_left.class.php');	//	CLASE PARA CONSTRUIR RL HTML DEL MENU IZQUIERDO
$menuleftHtml=new HtmlContents($frameworkData->tResultadoQuery);

//echo'<pre style=display:inline-block;max-height:300px;width:100%;>';
//$htmlOutput=$menuleftHtml->buildLeftMenu(0,$arrDataMenuLeft);
//print_r($frameworkData);
//print_r($arrDataMenuLeft);
//echo'</pre>'.__LEVEL_ACCESS_GOD;
$arrCommonMenus=array(
	'home' => array('level' => 0, 'link' => 'index.php', 'label' => 'Home page', 'icon_menu' => 'zmdi-home', 'modName' => 'Inicio', 'mnuRoleId' => 1),
	'modules_admin' => array('level' => 0, 'link' => 'index.php', 'label' => 'Administrador de los m&oacute;dulos', 'icon_menu' => 'zmdi-flip-to-back', 'modName' => 'modules_admin', 'mnuRoleId' => 9),
	'users' => array('level' => 0, 'link' => 'index.php', 'label' => 'Utenti (back-office)', 'icon_menu' => 'zmdi-account', 'modName' => 'Usuarios', 'mnuRoleId' => 9),
	'lang_elements_values' => array('level' => 0, 'link' => 'index.php', 'label' => 'Testi delle lingue', 'icon_menu' => 'zmdi-labels', 'modName' => 'lang_elements_values', 'mnuRoleId' => 8),
	'lang_elements' => array('level' => 0, 'link' => 'index.php', 'label' => 'Lang elements', 'icon_menu' => 'zmdi-labels', 'modName' => 'lang_elements', 'mnuRoleId' => 8),
	'pictures' => array('level' => 0, 'link' => 'index.php', 'label' => 'Dibujos', 'icon_menu' => 'zmdi-codepen', 'modName' => 'Dipinti', 'mnuRoleId' => 10),
	'_base_module' => array('level' => 0, 'link' => 'index.php', 'label' => 'Módulo base', 'icon_menu' => 'zmdi-photo-size-select-large', 'modName' => 'nuevo_modulo', 'mnuRoleId' => 8),
	'pictures_sections' => array('level' => 0, 'link' => 'index.php', 'label' => 'Secciones dibujos', 'icon_menu' => 'zmdi-photo-size-select-large', 'modName' => 'Pictures_Sections', 'mnuRoleId' => 8),
	'sistema' => array('level' => 0, 'link' => '#null', 'label' => 'Sistema', 'icon_menu' => 'zmdi-desktop-mac', 'modName' => 'Sistema', 'mnuRoleId' => __LEVEL_ACCESS_GOD, 'haveSubMenu' => true),
	'backup_data' => array('level' => 0, 'link' => 'index.php', 'label' => 'Backup', 'icon_menu' => 'zmdi-voicemail', 'modName' => 'Backup', 'mnuRoleId' => __LEVEL_ACCESS_GOD, 'subMenu' => true),
	'restore' => array('level' => 0, 'link' => 'index.php', 'label' => 'Restore', 'icon_menu' => 'zmdi-input-composite', 'modName' => 'Restore', 'mnuRoleId' => __LEVEL_ACCESS_GOD, 'subMenu' => true),
	'extracciones_pendientes_test' => array('level' => 0, 'link' => 'index.php', 'label' => 'Extracciones', 'icon_menu' => 'zmdi-globe', 'modName' => 'extracciones_pendientes_test', 'mnuRoleId' => 8),
	'countries' => array('level' => 0, 'link' => 'index.php', 'label' => 'Paesi', 'icon_menu' => 'zmdi-globe', 'modName' => 'Paises', 'mnuRoleId' => 8),
);
//asort($arrCommonMenus, SORT_NATURAL, ASC);
//ksort($arrCommonMenus);
//array_multisort($arrCommonMenus, SORT_NATURAL, DESC);


$tmpSubmenu=false;
/**
 * HTML PARA LOS ELEMENTOS DEL MENU IZQUIERDO
 * 
 */
foreach($arrCommonMenus as $moduleFolder => $module)
{
	$liClassActive='';

	if($tmpSubmenu && !isset($module['subMenu']) && !isset($module['haveSubMenu']))
	{
		$outMenuLeft.='</ul>';
		$outMenuLeft.='</li>';
		$tmpSubmenu=false;
	}

  if(!isset($module['modName']))
    continue;

  if($_SESSION['userAdmin']['roleId'] < $module['mnuRoleId'] )	// SI EL ROLEID ES MENOR DEL NIVEL DE ACCESO AL ELEMENTO DE MENU SE OMITE DIBUJAR EL MENU
    continue;

	if(isset($arraExtVars['modName']) && $module['modName'] == $arraExtVars['modName'] || (isset($arraExtVars['modName']) && !$arraExtVars['modName']))
	{
		$liClassActive='bgm-blue';
		$pageTitle=$module['label'];
	}
//echo'<br>'.$moduleFolder.' ->'.(@$arraExtVars['modName']);
//echo'<br>'.isset($module['haveSubMenu']);

  if(!isset($arraExtVars['modName']) && !$contPos)
  {
		$liClassActive='bgm-blue';
		$pageTitle=$module['label'];
    $linkMenu='index.php';
    $paramMenuLeft=!$contPos ? '?moduleAdmin=_modules/home/&modName='.$module['modName'].'&scName='.$module['link'].'&modLabel='.$module['label'].'&modIcon='.$module['icon_menu'] : '';

    $outMenuLeft.='<li class="'.$liClassActive.'">';
    $outMenuLeft.='<a href="'.$linkMenu.$paramMenuLeft.'" data-ma-action="submenu-toggle" class="c-white">';
    $outMenuLeft.='<i class="zmdi '.$module['icon_menu'].'"></i> ';
    $outMenuLeft.=''.$module['label'].'';
    $outMenuLeft.='</a>';
    $outMenuLeft.='</li>';
    $contPos++;
    continue;
  }
//echo '<hr>===>'.$module['icon_menu'];//die();

	$outMenuLeft.='<li class="'.$liClassActive.(isset($module['haveSubMenu']) ? ' sub-menu' : '').'">';
	$linkMenu=isset($module['link']) ? $module['link'] : 'index.php';
//	$paramMenuLeft=$contPos ? '?moduleAdmin=_modules/'.$moduleFolder.'/&modName='.$module['modName'].'&scName='.$module['link'].'&modLabel='.$module['label'] : '';
	$paramMenuLeft='?moduleAdmin=_modules/'.$moduleFolder.'/&modName='.$module['modName'].'&scName='.$module['link'].'&modLabel='.$module['label'].'&modIcon='.$module['icon_menu'];
	$outMenuLeft.='<a href="'.$linkMenu.$paramMenuLeft.'" class="c-white" '.($linkMenu == '#null' ? ' data-ma-action="submenu-toggle"' : '').'>';

	$outMenuLeft.='<i class="zmdi '.$module['icon_menu'].'"></i> ';
	$outMenuLeft.=''.$module['label'].'';
	$outMenuLeft.='</a>';

	if(isset($module['haveSubMenu']))
	{
		$outMenuLeft.='<ul>';
		$tmpSubmenu=true;
	}
	else
	{

	}
		$outMenuLeft.='</li>';

//	$outMenuLeft.='</li>';

	$contPos++;
//print_r($arrCommonMenus['home']);
}
echo $outMenuLeft;


/**
 *	ESTRUCTURA DE EJEMPLO x SUBMENUS
 */
?>
<!--
	<li>
		<a href="#null"><i class="zmdi zmdi-calendar"></i> TEST</a>
	</li>
  <li class="sub-menu">
    <a href="#null" data-ma-action="submenu-toggle"><i class="zmdi zmdi-menu"></i> Manú multinivel ejemplo</a>

    <ul>
      <li><a href="#null">Level 2</a></li>
      <li><a href="#null">Entrada de nivel 2</a></li>
      <li class="sub-menu">
        <a href="#null" data-ma-action="submenu-toggle">I have children too...</a>

        <ul>
          <li><a href="#null">Level 3 link</a></li>
          <li><a href="#null">Another Level 3 link</a></li>
        </ul>
      </li>
    </ul>
  </li>
</ul>
-->