<?php
/**
 * INDEX SCRIPT -- COMMON AND VENDOR CSS
 * 
 * (IN HEADER HTML)
 * 
 */
@session_start();
?>
<!-- Vendor CSS -->
<!--<link href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">-->
<link href="<?=$web_url.$adminFolder?>/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
<link href="<?=$web_url.$adminFolder?>/vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
<link href="<?=$web_url.$adminFolder?>/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
<link href="<?=$web_url.$adminFolder?>/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
<link href="<?=$web_url.$adminFolder?>/vendors/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<!--<link href="<?=$web_url.$adminFolder?>/vendors/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet">-->
<!--<link href="<?=$web_url.$adminFolder?>/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">-->

<!-- CSS -->
<link href="<?=$web_url.$adminFolder?>/css/app_1.min.css" rel="stylesheet">
<link href="<?=$web_url.$adminFolder?>/css/app_2.min.css" rel="stylesheet">

<link href="<?=$web_url.$adminFolder?>/css/dujok.css" rel="stylesheet">
