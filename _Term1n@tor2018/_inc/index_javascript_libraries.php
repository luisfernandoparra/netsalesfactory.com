<?php
/**
 * COMMON SCRIPT -- JAVASCRIPT LIBRARIES
 *
 * FROM index.php AND OTHERS
 * 
 */
@session_start();
@header('Content-Type: text/html; charset=utf-8');
$minExtension= $sitesPruebas ? '' : '.min';

//	if(0)	// PROVA PER VEDERFE SE SI PUO APPLICARE LA RICERCA NEI FILTRI DI TIPO SELECT
//	{
//		echo '<link href="'.$web_url.$adminFolder.'/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" />';
//		echo ' <script language="javascript" type="text/javascript" src="'.$web_url.$adminFolder.'/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>';
//		echo'<style>.bootstrap-select:not([class*="col-"]):not([class*="Xform-control"]):not(.input-group-btn){width:99%!important;}
//			.alert .select{display:block;height:46px;float:none;;}
//			</style>';	// NECESARIO X v.1.11.2 (2016.09.13 M.F.)
//	}

//$moduleSetting->arrFieldsFormModal[$key]['formType']

// START LOCAL LANGUAGE TRANSLACTIONS
if(isset($_SESSION['userAdmin']['pref_']['ref_lang']))
{
	$adminIso3Lang=$_SESSION['userAdmin']['pref_']['ref_lang'] ? strtolower($_SESSION['userAdmin']['pref_']['ref_lang']) : 'usa';
	$adminIso2Lang=isset($arrIso3ToIso2[$adminIso3Lang]) ? $arrIso3ToIso2[$adminIso3Lang] : 'us';
	$dirLang=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/js/dujok/';
  $filLang=$dirLang.'transalate_vars_'.$adminIso3Lang.'.php';	// INCLUDE TRANSLATED JS TEXTS OBJ

	if(file_exists($filLang))
		include($filLang);
}
// END LOCAL LANGUAGE TRANSLACTIONS


$htmlSpecificLibraries='';

if(isset($arraExtVars['moduleAdmin'])  && isset($arraExtVars['scName']))	// SOLO PARA EL INDEX GENERAL DEL ADMIN
{
	function findKey($array, $keySearch)
	{
		if (!is_array($array)) return false;
		if (array_key_exists($keySearch, $array)) return true;
		foreach($array as $key => $val)
			if (findKey($val, $keySearch)) return true;
		return false;
	}

	$localTmpModule=$web_url.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'config_module.php';

	$folderModule=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/'.$arraExtVars['moduleAdmin'];

	if(file_exists($folderModule.'config_module.php'))
	{
		@include($folderModule.'config_module.php');

		if(isset($moduleSetting) && $isDateFilter=findKey($moduleSetting->arrFieldsFormModal, 'filterType'))
		{
			$htmlSpecificLibraries.='<link href="'.$web_url.$adminFolder.'/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />';
			$htmlSpecificLibraries.='<script src="'.$web_url.$adminFolder.'/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
//echo ']<pre>';
//print_r($_REQUEST);
//print_r($moduleSetting->arrFieldsFormModal);
//echo '</pre>';
//die();
		}
	}
	//@include($web_url.$adminFolder.'/'.$arraExtVars['moduleAdmin'].'config_module.php');
}

?>

 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/jquery/dist/jquery.min.js"></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/flot/jquery.flot.js"></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/flot/jquery.flot.resize.js"></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/sparklines/jquery.sparkline<?=$minExtension?>.js"></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/moment/min/moment.min.js"></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/fullcalendar/dist/fullcalendar<?=$minExtension?>.js "></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/simpleWeather/jquery.simpleWeather<?=$minExtension?>.js"></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/Waves/dist/waves<?=$minExtension?>.js"></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bootstrap-growl/bootstrap-growl<?=$minExtension?>.js"></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<?php
echo $htmlSpecificLibraries;
$jsLocaleDateFile='<script language="javascript" type="text/javascript" src="'.$web_url.$adminFolder.'/vendors/bower_components/moment/locale/'.$adminIso2Lang.'.js"></script>';

echo $jsLocaleDateFile;
?>

<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
     <script language="javascript" type="text/javascript" src="vendors/bower_components/jquery-placeholder/jquery.placeholder<?=$minExtension?>.js"></script>
<![endif]-->

 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/js/app<?=$minExtension?>.js"></script>
 <script language="javascript" type="text/javascript" src="<?=$web_url.$adminFolder?>/js/dujok.js"></script>
