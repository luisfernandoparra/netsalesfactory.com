<?php
/**
 * (INDEX SCRIPT) -- RIGHT SIDEBAR
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$iframAwesomeFontIcons='';

?>
<aside id="chat" class="sidebar">

<!-- HELP FINDER -->
  <div class="chat-search">
    <div class="fg-line">
      <input name="help_text_find" type="text" class="form-control" placeholder="<?=__COMMON_GENERICTEXT_INPUTHELPSEARCH?>">
      <i class="zmdi zmdi-search"></i>
    </div>
  </div>

<!-- RIGHT SIDE-BAR ELEMENTS -->

<!--- CONTENIDOS DEL MENU LATERAL DERECHO --->
  <div class="lg-body c-overflow">
    <div class="list-group">
      <a class="list-group-item media basicHelpOpen" data-toggle="modal" href="#basicHelp" title="Ayuda básica" data-module-path="<?=@$_REQUEST['moduleAdmin']?>" data-module-caption="<?=@$_REQUEST['modLabel']?>">
        <div class="media-body">
          <div class="lgi-heading c-green"><i class="zmdi zmdi-help-outline">&nbsp;&nbsp;</i> <?=__COMMON_BTN_BACKOFFICEHELP?><br /></div>
          <div class="lgi-heading f-700 p-t-5 p-b-10"><?=@$_REQUEST['modLabel']?></div>
          <small class=""><?=__COMMON_BTN_BACKOFFICEHELPSAMLL?></small>
        </div>
      </a>
    </div>

<?php
// SOLO PARA SUPER ADMINISTRADORES
if($_SESSION['userAdmin']['roleId'] == __LEVEL_ACCESS_GOD)
{
?>
    <div class="list-group configDiv">
      <a class="list-group-item media configFileOpen" data-toggle="modal" href="#configFile" title="Configuración del módulo actual" data-module-path="<?=@$_REQUEST['moduleAdmin']?>" data-module-caption="<?=@$_REQUEST['modLabel']?>">
        <div class="media-body">
          <div class="lgi-heading c-blue"><i class="zmdi zmdi-tune">&nbsp;&nbsp;</i> <?=__COMMON_BTN_MODULECONFIGTXT?><br /></div>
          <div class="lgi-heading f-700 p-t-5 p-b-10"><i class="zmdi <?=@$_REQUEST['modIcon']?>">&nbsp;&nbsp;</i> <?=@$_REQUEST['modLabel']?></div>
        </div>
      </a>
    </div>

<?php
}

$iframAwesomeFontIcons=$web_url.$adminFolder.'/vendors/font-awesome-icons.php';
?>

    <div class="list-group">
      <a class="list-group-item media fontAwesomeIconsOpen" data-toggle="modal" href="#fontAwesomeIcons" title="Iconos awesome" data-module-path="<?=@$_REQUEST['moduleAdmin']?>" data-module-caption="<?=@$_REQUEST['modLabel']?>">
        <div class="media-body">
          <div class="lgi-heading c-blue"><i class="zmdi zmdi-font" aria-hidden="true">&nbsp;&nbsp;</i> Font Awesome Icons<br /></div>
          <!--<div class="lgi-heading f-700 p-t-5 p-b-10"><i class="zmdi <?=@$_REQUEST['modIcon']?>">&nbsp;&nbsp;</i> 222</div>-->
        </div>
      </a>
    </div>

  </div>
</aside>



<!-- MODAL MODULE HELP -->
<div class="modal c-black fade" data-modal-color="white" id="basicHelp" tabindex="-1" aria-hidden="true" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

      <div class="pull-right p-static m-r-20 m-t-10">
        <button class="btn btn-xs" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
      </div>

      <div class="pull-right p-static m-r-20 m-t-10">
        <button class="btn btn-xs openNewWindow" data-module-path="<?=@$_REQUEST['moduleAdmin']?>" data-module-caption="<?=@$_REQUEST['modLabel']?>"><?=__COMMON_BTN_ONENINNEWWINDOWTXT?></button>
      </div>

			<div class="modal-header p-15">
				<h4 class="modal-title c-green"><i class="zmdi zmdi-help-outline"></i>&nbsp;Ayudas para:&nbsp;<span class="f-700 p-t-10 p-b-20 c-black"><?=@$_REQUEST['modLabel']?></span>
				</h4>
			</div>
			<div class="modal-body helpModuleBody" style="overflow:auto;max-height:80vh;">
				<center><i><?=__COMMON_BTN_SEARCHINGHELPTXT?></i>
					<br /><br />
					<div class="preloader pl-xxl">
						<svg class="pl-circular" viewBox="25 25 50 50">
							<circle class="plc-path" cx="50" cy="50" r="20"></circle>
						</svg>
					</div>
				</center>
				<br /><br />
			</div>
		</div>
	</div>
</div>


<!-- MODAL EDIT CURRENT CONFIG MODULE  -->
<div class="modal c-black fade" data-modal-color="white" id="configFile" tabindex="-1" aria-hidden="true" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
      <div class="pull-right p-static m-r-20 m-t-10">
        <button class="btn btn-xs" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
      </div>
			<div class="modal-header p-15">
        <h4 class="modal-title c-blue"><i class="zmdi zmdi-tune"></i> <?=__COMMON_GENERICTEXT_CONFIGSCRIPTTXT?><br />
					<div class="f-700 p-t-10 p-b-20 c-black"><i class="zmdi <?=@$_REQUEST['modIcon']?>"></i> <?=@$_REQUEST['modLabel']?></div>
				</h4>
			</div>
			<div class="modal-body configModuleBody" style="overflow:auto;max-height:80vh;">
				<center><i><?=__COMMON_GENERICTEXT_LOADINGWAIT?></i>
					<br /><br />
					<div class="preloader pl-xxl">
						<svg class="pl-circular" viewBox="25 25 50 50">
							<circle class="plc-path" cx="50" cy="50" r="20"></circle>
						</svg>
					</div>
				</center>
				<br /><br />
			</div>
		</div>
	</div>
</div>



<!-- MODAL DISPLAY BACK-UP FILE  -->
<div class="modal c-black fade" data-modal-color="white" id="displayBakUpFile" tabindex="-1" aria-hidden="true" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
      <div class="pull-right p-static m-r-20 m-t-10">
        <button class="btn btn-xs" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
      </div>
			<div class="modal-header p-15">
        <h4 class="modal-title c-blue"><?=__COMMON_GENERICTEXT_BACKUPCONFIGSCRIPTTXT?><br />
          <div class="f-700 p-t-10 p-b-20 c-black"><i class="zmdi zmdi-file">&nbsp;&nbsp;</i> <?=__COMMON_GENERICTEXT_BACKUPTITLETXT?> <?=$_REQUEST['modLabel']?></div>
				</h4>
			</div>
			<div class="modal-body bakUpFileBody" style="overflow:auto;max-height:80vh;">
				<center><i><?=__COMMON_GENERICTEXT_LOADINGWAIT?></i>
					<br /><br />
					<div class="preloader pl-xxl">
						<svg class="pl-circular" viewBox="25 25 50 50">
							<circle class="plc-path" cx="50" cy="50" r="20"></circle>
						</svg>
					</div>
				</center>
				<br /><br />
			</div>
		</div>
	</div>
</div>


<!-- START FONT AWESOME ICONS  -->
<div class="modal c-black fade" data-modal-color="white" id="fontAwesomeIcons" tabindex="-1" aria-hidden="true" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
      <div class="pull-right p-static m-r-20 m-t-10">
        <button class="btn btn-xs" data-dismiss="modal"><?=__COMMON_BTN_CLOSE?></button>
      </div>
			<div class="modal-header p-15">
        <h4 class="modal-title c-blue"><i class="zmdi zmdi-font" aria-hidden="true"></i> Font Awesome Icons<br />
					<div class="f-700 p-t-10 p-b-20 c-black"><i class="zmdi zmdi-info-outline"></i> Set de <b>iconos</b> utilizados en la Web</div>
				</h4>
			</div>
			<div class="modal-body fontAwesomeIconsBody" style="overflow:auto;max-height:84vh;">
				<iframe style='height:13200px;' class="hidden" id='ifrFontAwesomeIcons' name='ifrFontAwesomeIcons' src='_blank.html' frameborder='0' marginheight='0' marginwidth='0' scrolling='no' width='100%'></iframe>
			</div>
		</div>
	</div>
</div>

<div class="modal fade m-t-20" id="faZoomIco" tabindex="-1" role="dialog" aria-hidden="true" role="dialog">
	<div class="vertical-alignment-helper text-center">
		<div class="modal-dialog vertical-align-center" >
			<div class="modal-content f-20 p-b-20 text-center"><br />...<br /></div>
		</div>
	</div>
</div>


<script type="text/javascript">
var iconsAwesomeLoaded=false;
$(document).ready(function(){
	$(".fontAwesomeIconsOpen").click(function(){
		if(!iconsAwesomeLoaded)
			$("#ifrFontAwesomeIcons").attr("src","<?=$iframAwesomeFontIcons?>").removeClass("hidden");
		iconsAwesomeLoaded=true;
	});
});
</script>
<!-- END FONT AWESOME ICONS  -->
