<?php
/**
 * COMMON SCRIPT -- BLOCK LEFT MENU
 *
 * FROM index.php
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

include($dirModAdmin.'mod_common_menu_left.class.php');	// MODULO PARA TODOS LOS METODOS LOGICOS (BBDD) DEL MODULO DEL MENU IZQUIERSO
$frameworkData=new CommonDataMenuLeft($db_type, $db_host, $db_user, $db_pass, $db_name, $port);
$resConnexion=$frameworkData->connectDB();
$response=$frameworkData->getDataLeft();
include($dirClassesAdmin.'menu_left.class.php');	//	CLASE PARA CONSTRUIR RL HTML DEL MENU IZQUIERDO
$menuleftHtml=new HtmlContents();
$menu=$menuleftHtml->buildLeftMenu($response, $foreAutoColor);

echo $menu;
