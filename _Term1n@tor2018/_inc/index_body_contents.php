<?php
/**
 * INDEX SCRIPT -- BLOCK BODY CONTENTS
 *
 * SCRIPT PUENTE PARA INCLUIR LOS CONTENIDOS
 * PRINCIPALES DE LA PAGINA ABIERTA
 */
@session_start();
?>
<div class="row">
  <div class="col-sm-12">
<?php
if(isset($arraExtVars['moduleAdmin']) && file_exists($arraExtVars['moduleAdmin']))
{
	include($arraExtVars['moduleAdmin'].$arraExtVars['scName']);
}
else
{
	echo '<center><h1 class="alert alert-danger alert-dismissible f-20">'.__COMMON_GENERICTEXT_MODULENOTREADYTXT.'</h1></center>';
}
?>
  </div>
</div>
