<?php
/**
 * INDEX SCRIPT -- GENERAL SETTINGS
 * 
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$contPos=0;

$hashSecutityScript=md5(date('Ymd')).'DUJOK'.md5(date('mmm'));
if($hashSecutityScript != __HASH_CONTROL_SCRIPT) die(__MSG_ACCESS_ERROR);

$pageTitle=(isset($arraExtVars['modIcon']) && $arraExtVars['modIcon'] ? '<span class="f-20 c-black pull-left p-relative m-r-10"><i class="zmdi '.$arraExtVars['modIcon'].'"> </i></span> ' : '').@$arraExtVars['modLabel'].'';

$currentUserParams=' data-action="edit" data-ref_mod="'.$hashSecutityScript.'" data-from="_modules/user_preferences" data-id="'.$_SESSION['userAdmin']['userId'].'" data-module-name="TEST1" data-module-modIcon="" data-editHeight="1000"';

$arrUserMenus=array(
	'user_preferences' => array('level' => 0, 'link' => 'index.php', 'label' => __COMMON_GENERICTEXT_USERCONFIGTXT, 'icon' => 'zmdi-face', 'modName' => 'user_preferences', 'mnuRoleId' => 4, 'classLink'=>'userCfg', 'menuPatsams'=>$currentUserParams),
	'_system_backoffice' => array('level' => 0, 'link' => 'index.php', 'label' => __COMMON_GENERICTEXT_SYSTEMCONFIGTXT, 'icon' => 'zmdi-settings', 'modName' => '_system_backoffice', 'mnuRoleId' => __LEVEL_ACCESS_GOD, 'classLink'=>'userCfg', 'menuPatsams'=>$currentUserParams)
);
?>
<div class="s-profile">
  <!--<button class="btn btn-default waves-effect hideLeftSidebar"><i class="zmdi zmdi-arrow-back"></i></button>-->
  <a href="#null" data-ma-action="profile-menu-toggle">
    <div class="sp-pic">
<?php
$imgDisplayProfile=(isset($_SESSION['userAdmin']['pref_']['iconUser']) && $_SESSION['userAdmin']['pref_']['iconUser']) ? 'profile_images/'.$_SESSION['userAdmin']['pref_']['iconUser'] : 'img/profile-pics/default.png' ;
	echo '<img src="'.$web_url.$adminFolder.'/'.$imgDisplayProfile.'" title="'.__COMMON_GENERICTEXT_OPTIONSOFYSERTITLE.$_SESSION['userAdmin']['name'].'">';
?>
    </div>

    <div class="sp-info">
      <?=$_SESSION['userAdmin']['name'].' '.$_SESSION['userAdmin']['surname']?>
      <i class="zmdi zmdi-caret-down"></i>
    </div>
  </a>

  <ul class="main-menu bgm-gray">
<?php
if($_SESSION['userAdmin']['roleId'] > __LEVEL_ACCESS_OPERATOR )
{
	/**
	 * HTML PARA LOS ELEMENTOS DEL MENU IZQUIERDO
	 *
	 */
	$outMenuLeft='';

	foreach($arrUserMenus as $moduleFolder => $module)
	{
		$liClassActive=$module['classLink'] ? $module['classLink'] : $_SESSION['userAdmin']['pref_']['top_header_theme_bckCol'];

		if(!isset($module['modName']))
			continue;

		if($_SESSION['userAdmin']['roleId'] < $module['mnuRoleId'] )	// SI EL ROLEID ES MENOR DEL NIVEL DE ACCESO AL ELEMENTO DE MENU SE OMITE DIBUJAR EL MENU
			continue;

		if(isset($arraExtVars['modName']) && $module['modName'] == $arraExtVars['modName'] || (isset($arraExtVars['modName']) && !$arraExtVars['modName']))
		{
      if(isset($_SESSION['userAdmin']['pref_']['top_header_theme_bckCol']))
        $liClassActive=$_SESSION['userAdmin']['pref_']['top_header_theme_bckCol'];
      else
        $liClassActive='bgm-blue';
		}

		$outMenuLeft.='<li class="">';
		$paramMenuLeft='';

		$linkMenu= $module['link'] ? 'index.php' : '#';

		$paramMenuLeft='?moduleAdmin=_modules/'.$moduleFolder.'/&modName='.$module['modName'].'&scName='.$module['link'].'&modLabel='.$module['label'].'&modIcon='.$module['icon'];
		$outMenuLeft.='<a href="'.$linkMenu.$paramMenuLeft.'" class="sub-menu '.$liClassActive.'" '.$module['menuPatsams'].'>';
		$outMenuLeft.='<i class="zmdi '.$module['icon'].'"> </i>';
		$outMenuLeft.='<span class=p-l-5>'.$module['label'].'</span>';
		$outMenuLeft.='</a>';
		$outMenuLeft.='</li>';
		$contPos++;
	//print_r($arrUserMenus['home']);
	}
	echo $outMenuLeft;
	$outMenuLeft='';

}

if($_SESSION['userAdmin']['roleId'] > 99 )
{
?>
    <li class="logoutCss">
      <a href="index.php?clearSession"><i class="zmdi zmdi-pin-off"></i><span class=p-l-5><?=__COMMON_BTN_RESETMENULEFT?></span></a>
    </li>
<?php
}
?>
<!--    <li class="sendEmailLeft">
      <a href="#modalEmailSend" data-toggle="modal" ><i class="zmdi zmdi-email"></i><span class=p-l-5><?=__COMMON_BTN_SENDEMAILMENULEFT?></span></a>
    </li>-->
    <li class="logoutCss">
      <a href="#null"><i class="zmdi zmdi-power"></i><span class=p-l-5><?=__COMMON_BTN_LOGOUTMENULEFT?></span></a>
    </li>
  </ul>

</div>
