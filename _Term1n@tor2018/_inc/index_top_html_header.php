<?php
/**
 * INDEX SCRIPT -- TOP HTML HEADER
 *
 * NOMBRE DEL MODULO ACTIVO +
 * + ACCIONES COMUNES AL MISMO
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$htmlExtraHeaderLiTag='';
$htmlExtraHeaderButtonTag='';

if(isset($moduleSetting->pluginsTopIndex) && count($moduleSetting->pluginsTopIndex) && $moduleSetting->pluginsTopIndex['plugInName'] == 'highCharts')
{
/*
	$htmlExtraHeaderLiTag.='
						<li>
								<a data-toggle="tooltip" data-original-title="" data-placement="left" href="#null" title="'.__COMMON_GENERICTEXT_TOPCHARTTITLETXT.'" class="modalHighCharts">
									'.__COMMON_GENERICTEXT_TOPCHARTLINKTXT.'
								</a>
						</li>';
*/
	$htmlExtraHeaderButtonTag.='
						<button type="button" class="btn bgm-white waves-effect modalHighCharts fa fa-bar-chart" data-trigger="hover" data-toggle="popover" data-content="'.__COMMON_GENERICTEXT_TOPCHARTTITLETXT.'" data-placement="bottom"></button>
<script>$(".modalHighCharts").click(function(){$(".headerH3Plugin").html("<div class=\'preloader pls-white c-res\'><svg class=\'pl-circular\' viewBox=\'25 25 50 50\'><circle class=\'plc-path\' cx=\'50\' cy=\'50\' r=\'20\'></circle></svg></div>");$("#contentHighCharts").modal("toggle");$("#ifrmHighChrarts").attr("src",$("#ifrmHighChrarts").attr("src")+"&tmp="+Math.random());});</script>
';

if(!$dateLoaded && $moduleSetting->pluginsTopIndex)
	include_once($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_plugins/'.$moduleSetting->pluginsTopIndex['customPath'].'/container_module_chart.php');

}
// constant('__PLUGINCHART_FORMUSERCONFIG_LABEL'.strtoupper($miVariableDinamica
?>
<script type="text/javascript">
var arrTextContent=new Array();	// ARRAY PARA UTILIZAR LAS TRADUCCIONES ESTATICAS EN FILES JS
arrTextContent["hlp_noData"]="<?=__COMMON_GENERICTEXT_NODATA?>";
arrTextContent["hlp_searchedText"]="<?=__COMMON_JSHELP_SEARCHEDTEXT?>";
arrTextContent["hlp_searching"]="<?=__COMMON_GENERICTEXT_SEARCHINGHELPTXT?>";
arrTextContent["hlp_preMatched"]="<?=__COMMON_JSHELP_AJAXMATCHESPRETXT?>";
arrTextContent["hlp_postMatched"]="<?=__COMMON_JSHELP_AJAXMATCHESPOSTTXT?>";
</script>

<ul class="actions btn-group">
		<li>
      <!--<a data-toggle="modal" href="#null" title="Insertar un nuevo registro" class="modalNewRecord" style="display:none;"><i class="zmdi zmdi-file openNewRecordModule"></i></a>-->
      <button type="button" class="btn bgm-white waves-effect modalNewRecord zmdi zmdi-file" data-trigger="hover" data-toggle="popover" data-content="<?=__TOPHEADER_BTN_NEWRECORDTITLE?>" data-placement="bottom" style="display:none;"></button>
			<?=$htmlExtraHeaderButtonTag?>
		</li>

		<li>
      <button type="button" class="btn bgm-white waves-effect fa fa-arrows-h f-14 p-5 fullWidthData" data-trigger="hover" data-toggle="tooltip" title="<?=__TOPHEADER_BTN_MAXIMIZEAREATITLE?>" data-placement="bottom" style=""></button>
		</li>

		<li class="dropdown">
				<button aria-expanded="false" type="button" class="btn <?=@$_SESSION['userAdmin']['pref_']['btn_default_forCol']?> <?=@$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> btn-default dropdown-toggle waves-effect" data-toggle="dropdown" title="<?=__COMMON_GENERICTEXT_OPTIONSTXT?>">
					<i class="zmdi zmdi-more-vert"></i>
				</button>
				<ul class="dropdown-menu dropdown-menu-right">
						<li>
								<a class="refreshCurrentModule" href="<?=$_SESSION['currentModuleUrl']?>"><?=__COMMON_GENERICTEXT_RELOADTXT?></a>
						</li>
						<?=$htmlExtraHeaderLiTag?>
						<li>
								<a data-toggle="tooltip" data-original-title="Tooltip on left" data-placement="left" href="#null" title="<?=__TOPHEADER_BTN_NEWRECORDTITLE?>" class="modalNewRecord" style="display:none;">
									<?=__COMMON_GENERICTEXT_NEWRECORDTXT?>
								</a>
						</li>
				</ul>
		</li>
</ul>
