<?php
/**
 * INDEX SCRIPT -- TOP BLOCKS ELEMENTS
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();
?>
<ul class="h-inner" style="color:#333;">
  <li class="hi-trigger ma-trigger" data-ma-action="sidebar-open" data-ma-target="#sidebar">
    <div class="line-wrap">
      <div class="line top"></div>
      <div class="line center"></div>
      <div class="line bottom"></div>
    </div>
  </li>

  <li class="hi-logo hidden-xs">
    <a href="index.php" title="<?=__COMMON_BTN_BACKOFFICETOPVERSIONTITLE?>" class="<?=$foreAutoColor?>" data-placement="bottom"><?=__COMMON_BTN_BACKOFFICETOPVERSIONTXT?><?=$versionBackOffice?></a>
  </li>

   <li class="hi-logoXX hidden-xs m-l-15">
		<a class="bg-black-trp m-t-5 btn btn-sm <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> <?=$foreAutoColor?>" href="index.php" title="<?=__COMMON_BTN_BACKOFFICETOPVERSIONTITLE?>" data-placement="bottom"> <?=$_SESSION['userAdmin']['name']?> (<?=$_SESSION['userAdmin']['roleId']?>)</a>
  </li>

  <li class="m-l-10 hi-logo hidden-xs <?=($_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD && $_SERVER['SERVER_ADDR'] === '192.168.2.102') ? '' : 'hide' ?>">
    <a class="bg-black-trp f-10 m-t-5 btn btn-sm <?=$_SESSION['userAdmin']['pref_']['btn_default_bckCol']?> <?=$foreAutoColor?>" href="#phpMyAdminMini" data-toggle="modal" title="mini admin mySql" class="f-12" data-placement="bottom" onclick="<?=($_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD && $_SERVER['SERVER_ADDR'] === '192.168.2.102') ? 'console.log(document.getElementById(\'ifrPhpMyAdminMini\').src);document.getElementById(\'ifrPhpMyAdminMini\').src=\'_common_bd_gestor_auxiliar.php?firstOpen=1&db='.$db_name.'\';' : '#null' ?>" ><i class="him-icon fa fa-database"></i></a>
  </li>
<!--
if(document.getElementById(\'ifrPhpMyAdminMini\').src == \'#null\')
<?=($_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_GOD && $_SERVER['SERVER_ADDR'] === '192.168.2.102') ? '_common_bd_gestor_auxiliar.php?firstOpen=1db='.$db_name : '#null' ?>
-->
  <li class="pull-right">
    <ul class="hi-menu">

			<li class="hi-logo <?=$foreAutoColor?> m-t-5 f-16">
				<?=date('d-m-Y')?>
				<span class="topClock"></span>
			</li>

			<li>
				<a class="refreshCurrentModule <?=$foreAutoColor?>" href="<?=@$_SESSION['currentModuleUrl']?>" data-content="Recargar la p&aacute;gina actual" data-placement="bottom"><i class="him-icon zmdi zmdi-refresh-alt"></i></a>
			</li>
<!--
      <li data-ma-action="search-open" data-content="(actualmente NO funcional)" data-original-title="Buscar" data-trigger="hover" data-toggle="popover" data-placement="bottom">
        <a href="#null"><i class="him-icon zmdi zmdi-search"></i></a>
      </li>
-->
<?php
if($_SESSION['userAdmin']['roleId'] >= __LEVEL_ACCESS_OPERATOR)	// BETA SETTING
{
?>
<!-- START BLOCK TOP MESSAGES --->
<!--
      <li class="dropdown">
        <a data-toggle="dropdown" href="#null">
          <i class="him-icon zmdi zmdi-email"></i>
          <i class="him-counts">6</i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg pull-right">
          <div class="list-group">
            <div class="lg-header">
              Mensajes
            </div>
            <div class="lg-body">
              <a class="list-group-item media" href="#null">
                <div class="pull-left">
                  <img class="lgi-img" src="img/profile-pics/<?=rand(1,9)?>.jpg" alt="">
                </div>
                <div class="media-body">
                  <div class="lgi-heading">David Belle (EXAMPLE))</div>
                  <small class="lgi-text">Cum sociis natoque penatibus et magnis dis parturient montes</small>
                </div>
              </a>
            </div>
            <a class="view-more" href="#null">View All</a>
          </div>
        </div>
      </li>
-->
<!-- END BLOCK TOP MESSAGES --->



<?php
/**
 * START PLUG-INS TOP WINDOW
 */
	if(isset($arrPluginAddOns))
	{
		foreach($arrPluginAddOns as $plugInNm=>$plugInObj)
		{
			if(file_exists($arrPluginAddOns[$plugInNm]['folderPlugin'].$arrPluginAddOns[$plugInNm]['fileName']) && $_SESSION['userAdmin']['roleId'] >= $arrPluginAddOns[$plugInNm]['minAccessLevel'])
			{
				include($arrPluginAddOns[$plugInNm]['folderPlugin'].$arrPluginAddOns[$plugInNm]['fileName']);
			}
		}
	}
// END PLUG-INS TOP WINDOW
?>



<!--
      <li class="dropdown hidden-xs">
        <a data-toggle="dropdown" href="#null">
          <i class="him-icon zmdi zmdi-view-list-alt"></i>
          <i class="him-counts">2</i>
        </a>
        <div class="dropdown-menu pull-right dropdown-menu-lg">
          <div class="list-group">
            <div class="lg-header">
              Tasks
            </div>
            <div class="lg-body">
              <div class="list-group-item">
                <div class="lgi-heading m-b-5">HTML5 Validation Report</div>

                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                    <span class="sr-only">95% Complete (success)</span>
                  </div>
                </div>
              </div>
              <div class="list-group-item">
                <div class="lgi-heading m-b-5">Google Chrome Extension</div>

                <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                    <span class="sr-only">80% Complete (success)</span>
                  </div>
                </div>
              </div>
              <div class="list-group-item">
                <div class="lgi-heading m-b-5">Social Intranet Projects</div>

                <div class="progress">
                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                    <span class="sr-only">20% Complete</span>
                  </div>
                </div>
              </div>
              <div class="list-group-item">
                <div class="lgi-heading m-b-5">Bootstrap Admin Template</div>

                <div class="progress">
                  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                    <span class="sr-only">60% Complete (warning)</span>
                  </div>
                </div>
              </div>
              <div class="list-group-item">
                <div class="lgi-heading m-b-5">Youtube Client App</div>

                <div class="progress">
                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                    <span class="sr-only">80% Complete (danger)</span>
                  </div>
                </div>
              </div>
            </div>

            <a class="view-more" href="#null">View All</a>
          </div>
        </div>
      </li>
-->
      <li class="dropdown">
        <a data-toggle="dropdown" href="#null" class="<?=$foreAutoColor?>"><i class="him-icon zmdi zmdi-more-vert"></i></a>
        <ul class="dropdown-menu dm-icon pull-right">
          <li class="hidden-xs">
            <a data-ma-action="fullscreen" href="#null"><i class="zmdi zmdi-fullscreen"></i> <?=__TOP_BTN_FULLSCREENTXT?></a>
          </li>
          <li class="divider hidden-xs"></li>
          <li>
						<a href="index.php?moduleAdmin=_modules/user_preferences/&modName=user_preferences&scName=index.php&modLabel=<?=__COMMON_GENERICTEXT_USERCONFIGTXT?>&modIcon=zmdi-face" data-placement="left" title="<?=__TOP_BTN_GOTOUSERCONFIGTXT?>"><i class="zmdi zmdi-face"></i> <?=__COMMON_GENERICTEXT_USERCONFIGTXT?></a>
          </li>
        </ul>
      </li>

      <li class="hidden-xs ma-trigger" data-ma-action="sidebar-open" data-ma-target="#chat">
        <a href="#null" title="<?=__TOP_BTN_OPENRIGHTPANELTITLE?>" data-placement="bottom" class="<?=$foreAutoColor?>" data-trigger="hover"><i class="him-icon zmdi zmdi-menu"></i></a>
      </li>

<?php
}
?>

      <li class="logoutCssTop hidden bgm-deeporange waves-effect" data-trigger="hover" data-toggle="popover" title="<?=__TOP_BTN_LOGOUTTITLE?>" data-placement="left">
        <a href="#null"><i class="him-icon zmdi zmdi-power"></i></a>
      </li>

      <li class="preLogoutCss waves-effect">
        <a href="#null" class="<?=$foreAutoColor?>" title="<?=__TOP_BTN_PRELOGOUTTITLE?>" data-trigger="hover" data-placement="left"><i class="him-icon zmdi zmdi-square-right"></i></a>
      </li>

    </ul>
  </li>
</ul>
