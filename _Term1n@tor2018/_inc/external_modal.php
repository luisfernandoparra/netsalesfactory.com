<?php
/**
 * (EXTERNAL WINDOW)
 *
 * ACTIONS ==> PARAMETRO ´action` (COMUN PARA TODAS LAS ACCIONES)
 * getHelpModule: DISPLAY HELP HTML SCRIPTS
 * 
 */

@header('Content-Type: text/html; charset=utf-8');
@session_start();

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_REQUEST;

$tmpTitleModule=isset($arraExtVars['moduleTitle']) ? $arraExtVars['moduleTitle'] : 'Back-office';

if(!isset($_SESSION['userAdmin']))	// SE HA PERDIDO LA SESION
{
	include('../../conf/config_web.php');
	include_once('../../conf/config.misc.php');
	include($dirModAdmin.'mod_bbdd.class.php');
	include($dirModAdmin.'mod_common_lists.class.php');
	$data=new CommonAdminLists($db_type, $db_host, $db_user, $db_pass, $db_name, null, $port);
	$resConnexion=$data->connectDB();
	$data->logBackOffice(0,null,'LOST-SESSION / '.$tmpTitleModule);
	echo '<header style="color:#000;font-family:arial,verdana;text-align:center;padding:10%;padding-top:6px;font-size:1.2em;">';
	echo '<div style="margin-bottom:10%;">Ayuda / <span style="font-weight:bold;">'.$tmpTitleModule.'</span></div>';
	echo '<div style="font-weight:normal;color:red;">';
	echo 'Parece que has estado inactivo con esta herramienta demasiado tiempo<br />o has dejado abierta esta pagina saliendo de tu sesión de usuario.';
	echo '<br /><br />Sembra che sei stato inattivo con questo strumento troppo a lungo<br />o che hanno lasciato aperta questa pagina dalla vostra sessione utente';
	echo '<br /><br />It seems that you`ve been inactive with this tool too long<br />or who have left open this page from your user session';
	echo '</div>';
	echo '<div style="font-weight:normal;color:#333;margin-top:2%;">';
	echo 'Para volver a visualizar las ayudas, debes identificarte nuevamente.';
	echo '<br /><br />Per visualizzare nuovamente aiuto, è necessario accedere di nuovo.';
	echo '<br /><br />To view help again, you need to log in again.';
	echo '</div>';
	echo '</header>';
	die();
}

if(isset($arraExtVars['action']))
{
  switch($arraExtVars['action'])
  {
    case 'getHelpModule';	// OBTENCION
      include('../ajax/common_admin.php');
      include_once($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.'/conf/config.misc.php');
?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Ayuda / <?=$tmpTitleModule?></title>

<script type="text/javascript">
var root_path="<?=$web_url.$adminFolder?>/";
</script>

<?php
      include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_comm_vendor_CSS.php');
      include($_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_inc/index_javascript_libraries.php');
      echo '<header class="clearfix '.@$_SESSION['userAdmin']['pref_']['btn_default_forCol'].' '.$_SESSION['userAdmin']['pref_']['btn_default_bckCol'].'">';
      echo '<div class="'.$_SESSION['userAdmin']['pref_']['background_bckCol'].'">';
      echo '<div class="c-gray p-10 text-center f-600 f-20">Ayuda / <span class="c-black text-center f-700 f-20 activePage">'.$tmpTitleModule.'</span></div>';
      echo '<div class="'.@$_SESSION['userAdmin']['pref_']['pref_background_bckCol'].' p-20">'.$htmlOut.'</div>';
      echo '</div>';
      echo '</header>';
      break;
  }
  die();
}
?>
