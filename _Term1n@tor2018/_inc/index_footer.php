<?php
@header('Content-Type: text/html; charset=utf-8');
@session_start();

/**
 * SI EXISTEN ARCHIVOS JS DE PLUGINS, SE CARGAN DESPUÉS DE LOS DEMÁS
 */
//echo '--->'.date('H:i:s').'--->'.count($arrJsAdditionalPlugins);
if(isset($arrJsAdditionalPlugins) && count($arrJsAdditionalPlugins))
{
	foreach($arrJsAdditionalPlugins as $jsFile)
		include($jsFile);
}
?>

<footer id="footer">
	Copyright &copy; <?=date('Y')?> M.F. soft (<span class="c-green">M.F.</span>)

    <ul class="f-menu">
        <li><a href="<?=@$arrCommonMenus['link']['home']?>"><?=@$arrCommonMenus['label']['home']?></a></li>
<!--        <li><a href="#null">Dashboard</a></li>
        <li><a href="#null">Reports</a></li>
        <li><a href="#null">Support</a></li>
        <li><a href="#null">Contact</a></li>-->
    </ul>
</footer>

<script type="text/javascript">

$(document).ready(function(){

	$(".userCfg").click(function(){
    this.style.backgroundColor="#009688";
    this.style.color="#ffffff";
/*
    var zz=this.dataset;
    var arrUrlParam=[];

    for(var i in zz)
    {
      arrUrlParam.push(encodeURI(i) + "=" + encodeURI(zz[i]));
    }

    var jsonParams=JSON.stringify(zz);
*/
	});

	// ACCION PARA COPIAR EL REGISTRO SELECCIONADO
	$(".actCopyRecord").click(function(e){
		var dataSend={<?=@$outJsParamsModule?>};
		dataSend.idRef=$(this).attr("data-id");
		dataSend.action="copyModuleRecord";
		dataSend.moduleName="<?=substr($arraExtVars['moduleAdmin'],9,-1)?>";
		commonCopyModuleRecord(this, dataSend);
	});
});
</script>
