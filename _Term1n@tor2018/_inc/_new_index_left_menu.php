<?php
/**
 * COMMON SCRIPT -- BLOCK LEFT MENU
 *
 * FROM index.php
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$outMenuLeft='';
$contPos=0;
die();
?>


<?php
include($dirModAdmin.'mod_common_menu_left.class.php');	// MODULO PARA TODOS LOS METODOS LOGICOS (BBDD) DEL MODULO DEL MENU IZQUIERSO
$frameworkData=new CommonDataMenuLeft($db_type, $db_host, $db_user, $db_pass, $db_name);
$resConnexion=$frameworkData->connectDB();
//$arrDataMenuLeft=$frameworkData->getDataMenuLeft();

$response = $frameworkData->getDataLeft();
include($dirClassesAdmin.'menu_left.class.php');	//	CLASE PARA CONSTRUIR RL HTML DEL MENU IZQUIERDO
//$menuleftHtml=new HtmlContents($frameworkData->tResultadoQuery);
$menuleftHtml = new HtmlContents();
$menu = $menuleftHtml->buildLeftMenuNew($response);

echo $menu;
//echo '</ul>';
/*

$arrCommonMenus=array(
	'home' => array('level' => 0, 'link' => 'index.php', 'label' => 'P&aacute;gina inicial', 'icon_menu' => 'zmdi-home', 'modName' => 'Inicio', 'mnuRoleId' => 1),
	'modules_admin' => array('level' => 0, 'link' => 'index.php', 'label' => 'Administrador de los m&oacute;dulos', 'icon_menu' => 'zmdi-flip-to-back', 'modName' => 'modules_admin', 'mnuRoleId' => 9),
	'users' => array('level' => 0, 'link' => 'index.php', 'label' => 'Usuarios (back-office)', 'icon_menu' => 'zmdi-account', 'modName' => 'Usuarios', 'mnuRoleId' => 9),
	'raffles_categories' => array('level' => 0, 'link' => 'index.php', 'label' => 'Categor&iacute;as de sorteos', 'icon_menu' => 'zmdi-labels', 'modName' => 'Categorias', 'mnuRoleId' => 8),
	'products_lang' => array('level' => 0, 'link' => 'index.php', 'label' => 'Nombres de productos', 'icon_menu' => 'zmdi-codepen', 'modName' => 'Productos_Lang', 'mnuRoleId' => 10),
	'products_photos' => array('level' => 0, 'link' => 'index.php', 'label' => 'Fotos de productos', 'icon_menu' => 'zmdi-photo-size-select-large', 'modName' => 'Productos_Photos', 'mnuRoleId' => 8),
	'countries' => array('level' => 0, 'link' => 'index.php', 'label' => 'Paises', 'icon_menu' => 'zmdi-globe', 'modName' => 'Paises', 'mnuRoleId' => 8),
	'extracciones_pendientes_test' => array('level' => 0, 'link' => 'index.php', 'label' => 'Extracciones', 'icon_menu' => 'zmdi-globe', 'modName' => 'extracciones_pendientes_test', 'mnuRoleId' => 8),

//	'listado' => array('level' => 0, 'link' => 'index.php', 'label' => 'Listado', 'icon_menu' => 'zmdi-layers', 'modName' => 'Test', 'mnuRoleId' => 0)
);
*/


//asort($arrCommonMenus, SORT_NATURAL, ASC);
//ksort($arrCommonMenus);
//array_multisort($arrCommonMenus, SORT_NATURAL, DESC);
//$tmpSubmenu=false;
/**
 * HTML PARA LOS ELEMENTOS DEL MENU IZQUIERDO
 *
 */

/*
foreach($arrCommonMenus as $moduleFolder => $module)
{
	$liClassActive='';

	if($tmpSubmenu && !isset($module['subMenu']) && !isset($module['haveSubMenu']))
	{
		$outMenuLeft.='</ul>';
		$outMenuLeft.='</li>';
		$tmpSubmenu=false;
	}

  if(!isset($module['modName']))
    continue;

  if($_SESSION['userAdmin']['roleId'] < $module['mnuRoleId'] )	// SI EL ROLEID ES MENOR DEL NIVEL DE ACCESO AL ELEMENTO DE MENU SE OMITE DIBUJAR EL MENU
    continue;

	if(isset($arraExtVars['modName']) && $module['modName'] == $arraExtVars['modName'] || (isset($arraExtVars['modName']) && !$arraExtVars['modName']))
	{
		$liClassActive='bgm-blue';
		$pageTitle=$module['label'];
	}
//echo'<br>'.$moduleFolder.' ->'.(@$arraExtVars['modName']);
//echo'<br>'.isset($module['haveSubMenu']);

  if(!isset($arraExtVars['modName']) && !$contPos)
  {
		$liClassActive='bgm-blue';
		$pageTitle=$module['label'];
    $linkMenu='index.php';
    $paramMenuLeft=!$contPos ? '?moduleAdmin=_modules/home/&modName='.$module['modName'].'&scName='.$module['link'].'&modLabel='.$module['label'].'&modIcon='.$module['icon_menu'] : '';

    $outMenuLeft.='<li class="'.$liClassActive.'">';
    $outMenuLeft.='<a href="'.$linkMenu.$paramMenuLeft.'" data-ma-action="submenu-toggle" class="c-white">';
    $outMenuLeft.='<i class="zmdi '.$module['icon_menu'].'"></i> ';
    $outMenuLeft.=''.$module['label'].'';
    $outMenuLeft.='</a>';
    $outMenuLeft.='</li>';
    $contPos++;
    continue;
  }
//echo '<hr>===>'.$module['icon_menu'];//die();

	$outMenuLeft.='<li class="'.$liClassActive.(isset($module['haveSubMenu']) ? ' sub-menu' : '').'">';
	$linkMenu=isset($module['link']) ? $module['link'] : 'index.php';
//	$paramMenuLeft=$contPos ? '?moduleAdmin=_modules/'.$moduleFolder.'/&modName='.$module['modName'].'&scName='.$module['link'].'&modLabel='.$module['label'] : '';
	$paramMenuLeft='?moduleAdmin=_modules/'.$moduleFolder.'/&modName='.$module['modName'].'&scName='.$module['link'].'&modLabel='.$module['label'].'&modIcon='.$module['icon_menu'];
	$outMenuLeft.='<a href="'.$linkMenu.$paramMenuLeft.'" class="c-white" '.($linkMenu == '#null' ? ' data-ma-action="submenu-toggle"' : '').'>';

	$outMenuLeft.='<i class="zmdi '.$module['icon_menu'].'"></i> ';
	$outMenuLeft.=''.$module['label'].'';
	$outMenuLeft.='</a>';

	if(isset($module['haveSubMenu']))
	{
		$outMenuLeft.='<ul>';
		$tmpSubmenu=true;
	}
	else
	{

	}
		$outMenuLeft.='</li>';

//	$outMenuLeft.='</li>';

	$contPos++;
//print_r($arrCommonMenus['home']);
}
echo $outMenuLeft;


/**
 *	ESTRUCTURA DE EJEMPLO x SUBMENUS
 */
?>