<?php
/**
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class HtmlContents
{
  public $textForeColor;

	public function __construct()
	{
    $this->textForeColor='';
		return;
	}

  private function setLiState($url,$id){
    if(strpos($url,$id))
		{
      $state='active';
    }
		else
		{
      $state='';
    }

    return $state;
  }

  private function setLiClass($num_hijos,$state)
	{
    if($num_hijos != 0 && $state == 'active')
		{
      $class=' sub-menu toggled';
    }
		elseif($num_hijos != 0 && $state != 'active')
		{
      $class=' sub-menu toggle';
    }
		else
		{
      $class=''.$state;
    }
    return $class;
  }

  private function setALink($num_hijos, $mod_name, $label, $file, $icon, $id)
	{
    if($num_hijos != 0)
      $link='#null';
		else
      $link='?moduleAdmin=_modules/'.$mod_name.'/&modName='.$label .'&scName='.$file.'&modLabel='.$label.'&modIcon='.utf8_decode($icon).'&optId='.$id;

		return $link;
  }

  private function setAClass($num_element)
	{
    if($num_element != 0)
		{
      $class='data-ma-action="submenu-toggle"';
    }
		else
		{
      $class='';
    }

    return $class;
  }

  private function getStructure($li_class='', $option_icon='', $option_label='DESC!!!', $option_id=0, $a_link='#null', $a_class='', $children=0)
	{
    $menu_p='<li class="%s" id="%s"><a %s href="%s" class="'.(strrchr($li_class, 'active') ? 'c-black' : $this->textForeColor).'"><i class="zmdi %s"></i>&nbsp;&nbsp;%s</a></li>'."\n";
    $structure=sprintf($menu_p, $li_class, $option_id, $a_class, $a_link, $option_icon, $option_label);

    if($children != 0)
		{
      $rewrite=str_replace('</a></li>','</a>%s</li>',$structure);
      $structure=$rewrite;
    }

    return $structure;
  }

  public function buildLeftMenu($resultado, $foreAutoColor='c-white')
	{
    $this->textForeColor=$foreAutoColor;
    $url_active=$_SERVER['REQUEST_URI'];
    $html='<ul class="main-menu %s "></ul>';
    $theme='bgm-red';

    if(isset($_SESSION['userAdmin']['pref_']['top_header_theme_bckCol']))
      $theme=$_SESSION['userAdmin']['pref_']['top_header_theme_bckCol'].' '.$foreAutoColor;
    else
      $theme='bgm-blue c-white';

    $html=sprintf($html, $theme);
    $rewrite_html=str_replace('><','>%s<', $html);
    $html=$rewrite_html;
    $result_menu='';

    foreach($resultado as $nivel => $father)
    {
      if($father['id_related_option'] != 0)
				continue;

			$liId=$father['module_name'].'_'.$father['id_option_menu'].'_';
			$liState=$this->setLiState($url_active, $liId);
			$liClass=$this->setLiClass($father['cantidad'], $liState);
			$iIcon=$father['icon'];
			$aLabel=$father['label_menu'];
			$linkOpt=$this->setALink($father['cantidad'], $father['module_name'], $father['label_menu'], $father['filelink'], $father['icon'], $liId);
			$aClass=$this->setAClass($father['cantidad']);
			$options=$this->getStructure($liClass, $iIcon, $aLabel, $liId, $linkOpt, $aClass, $father['cantidad']);
			$result_menu.=$options;

			if($father['cantidad'])	// EXISTEN SUB-MANES (NIVEL 2)
			{
				if($liState != '')
						$result_sub_menu='<ul style="display: block">';
				else
						$result_sub_menu='<ul>';

				foreach($resultado as $subnivel => $children)
				{
					if($children['id_related_option'] != $father['id_option_menu'])
						continue;

					$liId.='|'.$children['module_name'].'_'.$children['id_option_menu'].'_';
					$c_liState=$this->setLiState($url_active, $liId);
					$c_liClass=$this->setLiClass($children['cantidad'], $c_liState);
					$c_iIcon=$children['icon'];
					$c_aLabel=$children['label_menu'];
					$c_linkOpt=$this->setALink($children['cantidad'], $children['module_name'], $children['label_menu'], $children['filelink'], $children['icon'], $liId);
					$c_aClass=$this->setAClass($children['cantidad']);
					$c_options=$this->getStructure($c_liClass, $c_iIcon, $c_aLabel, $liId, $c_linkOpt, $c_aClass, $children['cantidad']);
					$result_sub_menu.=$c_options;

					if($children['cantidad'])		// EXISTEN SUB-MENUS (NIVEL 3)
					{
						if($liState != '' && $c_liState != '')
							$result_sub_sub_menu='<ul style="display: block">';
						else
							$result_sub_sub_menu='<ul>';

						foreach($resultado as $sub_nivel => $grandson)
						{
							if($grandson['id_related_option'] != $children['id_option_menu'])
								continue;

							$liId.='|'.$grandson['module_name'].'_'.$grandson['id_option_menu'].'_';
							$g_liState=$this->setLiState($url_active, $liId);
							$g_liClass=$this->setLiClass($grandson['cantidad'], $g_liState);
							$g_iIcon=$grandson['icon'];
							$g_aLabel=$grandson['label_menu'];
							$g_linkOpt=$this->setALink($grandson['cantidad'], $grandson['module_name'], $grandson['label_menu'], $grandson['filelink'], $grandson['icon'], $liId);
							$g_aClass=$this->setAClass($grandson['cantidad']);
							$g_options=$this->getStructure($g_liClass, $g_iIcon, $g_aLabel.'', $liId, $g_linkOpt, $g_aClass, $grandson['cantidad']);
							$result_sub_sub_menu.=$g_options;
							$liId=str_replace('|'.$grandson['module_name'].'_'.$grandson['id_option_menu'].'_', '', $liId);
//echo'<br>'.$resultado[$sub_nivel]['id_option_menu'];
							unset($resultado[$sub_nivel]['id_option_menu']);
						}
						$result_sub_sub_menu.='</ul>';
					}
					$liId=str_replace('|'.$children['module_name'].'_'.$children['id_option_menu'].'_', '', $liId);
					unset($resultado[$subnivel]['id_option_menu']);
				}
				$result_sub_menu.='</ul>';
				$result_menu=sprintf($result_menu, $result_sub_menu);
				$result_menu=sprintf($result_menu, @$result_sub_sub_menu);
			}
			unset($resultado[$nivel]['id_option_menu']);
    }

    $html=sprintf($html, $result_menu);
    return $html;
  }
}
