<?php
/**
 * COMMON AJAX METHODS
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class CommonAjaxMethods
{
	public $backUpFilename;

	public function __construct()
	{
		$this->backUpFilename='';
		return;
	}

	public function test($param=0)
	{
		return;
	}

	/**
	 * UPDATE CONFIG MODULE FILES
	 *
	 * @param string $fileName
	 * @param string $pathFile
	 * @param string $txtContent
	 * @return integer
	 */
	public function updateConfigModuleFile($fileName=null, $pathFile=null, $txtContent='')
	{
		$txtContent="<?php\r\n".$txtContent;
//		$asciiChars='a-zA-Z0-9\s`~!@#$%^&*()_+-={}|:;<>?,.\/\"\'\\\[\]';
//		$txtContent=preg_replace("/[^$asciiChars]/", '', $txtContent);
		$txtContent=str_replace(array('á','é','í','ó','ú','ñ','Á','É','Í','Ó','Ú','Ñ'),array('&aacute;','&eacute;','&iacute;','&oacute;','&uacute;','&ntilde;','&Aacute;','&Eacute;','&Iacute;','&Oacute;','&Uacute;','&Ntilde;'), $txtContent);

    $posTopCommentUpdate=strpos($txtContent, '/**');
    $startText=substr($txtContent, $posTopCommentUpdate);
    $startPoint='/**';
    $endPoint='- updated from admin editor';
    $newText='/**
 * DATE UPDATE: '.date('Y-m-d H:i').' ('.ucfirst($_SESSION['userAdmin']['name']).' '.ucfirst($_SESSION['userAdmin']['surname']).') - updated from admin editor';

    $resultText=preg_replace('#('.preg_quote($startPoint).')(.*)('.preg_quote($endPoint).')#si', $newText, $txtContent, 1);

		$asciiChars='a-zA-Z0-9\s`~!@#$%^&*()_+-={}|:;<>?,.\/\"\'\\\[\]';
		$resultText=preg_replace("/[^$asciiChars]/", null, $resultText);

    $resultText=str_replace("\xa0",'',$resultText);	// EN VIA EXPERIMENTAL (NO HAY QUE QUITAR HASTA COMPROBADO!!!!)

//    $resultText=utf8_decode($resultText);
//echo "\n posTopCommentUpdate= ".$posTopCommentUpdate;
//echo "\n".$result;//preg_replace("/\/**[\s\S]*?/\(updated from admin editor)/",'XXXXXXX',$startText,1);
//echo "\n newLine= ".$newLine;
//echo "\n posEndAutoComment= ".$posEndAutoComment;
//echo "\n PORCION= ".substr($startText, 0, $posEndAutoComment);
//echo "\n\n\n startText=
//".$startText;
//echo "\n\n\n";print_r($_SESSION['name']);
//die();
		$file=@fopen($pathFile.$fileName,'wb');
		$res=@fwrite($file, $resultText);
//		$res=@fwrite($file, "\xEF\xBB\xBF".$resultText);
//		$res=@fwrite($file, "\0x00".$resultText);
		@fclose($file);
//		$res=@file_put_contents($pathFile.$fileName, $txtContent, LOCK_EX);
		if(!$res)	// FILE ERROR
		{
			$response['success']=false;
			$response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-card-alert"></i></h1> <h3>'.__COMMON_AJAXMSG_UPDATECONFIGFILEERRORTXT.' <b>'.$fileName.'</b></h3>error code F0003</span><br />';
			$res=json_encode($response);
			die($res);
		}
		return $res;
	}

	/**
	 * BACK-UP CREATION FILE
	 *
	 * @param string $fileName
	 * @param string $pathFile
	 * @param string $newFileName
	 * @param string $targetPath
	 * @return boolean
	 */
	public function backUpFile($fileName=null, $pathFile=null, $newFileName=null, $targetPath=null)
	{
		if(!$newFileName)
			$newFileName=str_pad(@$_SESSION['userAdmin']['userId'],3,'0',STR_PAD_LEFT).'-'.date('Ymd').'-'.date('His').'_'.$fileName;

		$targetPath=$targetPath ? $targetPath : $pathFile;	/// PARA $targetPath SE ASIGNA LA RUTA INICIAL SI NO EXISTE UNA DE DESTINO

		if(!$fileName || !$pathFile)
		{
			$response['success']=false;
			$response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-block-alt"></i></h1><h2>'.__COMMON_AJAXMSG_UNABLEEXECUTEBACKUPERRORTXT.' </h2></span><br />';
			$res=json_encode($response);
			die($res);
		}

		if(!is_dir($targetPath))	// IF TARGET DIRECTORY NOT EXISTS, TRY TO CREATE IT
		{
			$res=@mkdir($targetPath, 0755, true);

			if(!$res)	// ON CREATION PATH ERROR
			{
				$response['success']=false;
				$response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-block-alt"></i></h1><h2>'.__COMMON_AJAXMSG_UNABLECREATEFOLDERERRORTXT.'</h2></span><br />';
				$res=json_encode($response);
				die($res);
			}
		}

		$res=@copy($pathFile.$fileName, $targetPath.$newFileName);
		$this->backUpFilename=$newFileName;

		if(!$res)	// FALLO EN LA CREACION DEL ARCHIVO
		{
				$response['success']=false;
				$response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-minus-circle"></i></h1><h3>'.__COMMON_AJAXMSG_FILEBACKUPERRORH3.'</h3><span class="f-14">'.__COMMON_AJAXMSG_FILEBACKUPERRORTXT.' `'.$newFileName.'`<br />'.__COMMON_AJAXMSG_FILEBACKUPERRORFOLDERTXT.' `'.$targetPath.'`</span></span><br />';
				$res=json_encode($response);
				die($res);
		}
		return true;
	}

	/**
	 * RESALTA (ESTILIZA) EL CONTENIDO DEL TEXTO RECIBIDO
	 * SEGUN EL LENGUAJE DE PROGRAMACION DEFINIDO EN  lang
	 *
	 * string type $lang
	 * string string $txtContent
	 * string string $dirModulesAdmin
	 * string string $referenceElement
	 * string bool $enableRowNumber
	 * @return string
	 */
	public function highLightContentFile($lang='', $txtContent='', $dirModulesAdmin, $referenceElement='UNKNOWN!!!!!', $enableRowNumber=false)
	{
		include($dirModulesAdmin.'vendors/geshi/geshi.php');
		$geshi=new GeSHi($txtContent, $lang);
		$geshi->set_tab_width(2);
		if($enableRowNumber)
			$geshi->enable_line_numbers(GESHI_FANCY_LINE_NUMBERS);
		$geshi->enable_strict_mode(0);
//echo'<pre class=text-left>';print_r($geshi);
		$res=$geshi->parse_code();	// HIGLIGHT PHP CODE

		if(!$res)	// PROCESS SYNTAX HIGHLIGHT ERROR
		{
			$response['success']=false;
			$response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-block-alt"></i></h1> '.__COMMON_AJAXMSG_HIGHLIGHTCONTENTFILEERRORTXT.' <h1>'.$referenceElement.'</h1></span><br />';
			$res=json_encode($response);
			die($res);
		}

		return $res;
	}

	/**
	 * LEE EL ARCHIVO DE CONFIGURACION DEL MODULO ABIERTO
	 * Y DEVUELVE EL CONTENIDO COMPLETO
	 *
	 * @param string $dirModulesAdmin
	 * @param array $arraExtVars
	 * @return type
	 */
	public function getConfigModuleFile($dirModulesAdmin, $arraExtVars)
	{
		$res=is_file($dirModulesAdmin.$arraExtVars['modulePath'].'config_module.php');

		if(!$res)	// FILE ERROR
		{
			$response['success']=false;
			$response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-card-alert"></i></h1> <h3>'.__COMMON_AJAXMSG_GETCONFIGMODULEFILEFILEERRORTXT.' <b>'.$arraExtVars['moduleTitle'].'</b></h3>error code F0001</span><br />';
			$res=json_encode($response);
			die($res);
		}

		$res=file_get_contents($dirModulesAdmin.$arraExtVars['modulePath'].'config_module.php');
		$res=str_replace(array('á','é','í','ó','ú','ñ','Á','É','Í','Ó','Ú','Ñ'),array('&aacute;','&eacute;','&iacute;','&oacute;','&uacute;','&ntilde;','&Aacute;','&Eacute;','&Iacute;','&Oacute;','&Uacute;','&Ntilde;'), $res);
		$res=utf8_encode($res);

		if(!$res)	// FILE CONTENTS ERROR OR NULL
		{
			$response['success']=false;
			$response['errorText']='<span class="c-black "><h1><i class="zmdi zmdi-card-alert"></i></h1> <h3>'.__COMMON_AJAXMSG_GETCONFIGMODULEFILECONTENTSERRORTXT.' <b>'.$arraExtVars['moduleTitle'].'</b></h3>error code F0002</span><br />';
			$res=json_encode($response);
			die($res);
		}

		// START BUSCAR TEXTO FIJO PARA INCLUIR POR DEFECTO LA FECHA + AUTOR DOCUMENTO
		$stringToFind='- updated from admin editor';
		$resFind=strpos($res, $stringToFind);
		$startCommentToFind='/**';
		$resFind2=strpos($res, $startCommentToFind);

		if(!$resFind && $resFind2)
		{
			$startPoint='/**';
			$endPoint='* LOCAL MODULE';
			$newText='/**'.PHP_EOL.' * DATE UPDATE: '.date('Y-m-d H:i').' ('.ucfirst($_SESSION['userAdmin']['name']).' '.ucfirst($_SESSION['userAdmin']['surname']).') '.$stringToFind.''.PHP_EOL.' * LOCAL MODULE';
			$tmpContent=preg_replace('#('.preg_quote($startPoint).')(.*)('.preg_quote($endPoint).')#si', $newText, $res, 1);
			$res=$tmpContent;
		}
		// END BUSCAR TEXTO FIJO PARA INCLUIR POR DEFECTO LA FECHA + AUTOR DOCUMENTO

		$res=str_replace('<?php','',$res);	// SE SUPRIME LA ETIQUETA IDENTIFICADORA DE PHP INICIAL
		$res=str_replace('?>','',$res);	// SE SUPRIME LA ETIQUETA IDENTIFICADORA DE PHP FINAL
		$res=implode("\n", array_slice(explode("\n", $res), 1));	// SE ELIMINA LA PRIMERA LINEA VACIA
		return $res;
	}


	public function __destruct(){}
}