<?php
/**
 * COMMON ADMIN FUNCTIONS
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class ListTableElements
{
//	public $test;

	public function __construct()
	{
		return;
	}


	/**
	 * NECESARIO PARA CONSTRUIR LA CABECERA DEL LISTADO
	 * SI ES UN ARRAY $mainTableFieldsNames, SE RECORREN LOS ELEMENTOS X OBTENER EL NOMBRE + EL TIPO DE DATO
	 * @param array $mainTableFieldsNames
	 * @return array
	 */
	public function getfieldHeaderNames($mainTableFieldsNames=null)
	{
		if(!is_array($mainTableFieldsNames))	// STRING OF VALUES, COMMA SEPARATED
		{
			$arrName=explode(',', $mainTableFieldsNames);

			foreach($arrName as $value)
			{
				$resData[$value]='string';
			}
			return $resData;
		}

		foreach($mainTableFieldsNames as $key=>$value)
		{
			$resData[$value['Field']]=substr($value['Type'],0,7) == 'varchar' ? 'string': 'int';
		}

//echo'<pre>';print_r($mainTableFieldsNames);
		return @$resData;
	}


  /**
   * DEVUELVE LAS FILAS PARA EL LISTADO A DIBUJAR
   * @param varchar $tableHeaderNames
   * @param array $tableRowsData
   * @return null
   */
	public function getTableRows($tableHeaderNames=null, $tableRowsData=null)
	{
		if(!count($tableHeaderNames))
			return false;

		foreach($tableRowsData as $key=>$value)
		{
			foreach($tableHeaderNames as $k=>$v)
			{
				$this->tableRow[$key][$k]=$value[$k];
				$this->rowOptions[$key]['a']=$key;
			}
		}
		return;
	}
}