<?php
/**
 * COMMON EDITING FORMS METHODS
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class CommonModulesMethods
{
	public $formElements;
	public $fieldParams;
	private $altExtraFuncText;
	private $moduleErrorTxt;

	public function __construct()
	{
    $this->fieldParams=null;
		$this->moduleErrorTxt='';
    $this->altExtraFuncText=__COMMON_EDITMODULE_ALTEXTRAFUNCTEXT;
		return;
	}

	/**
	 * PERMITE OMITIR LA RENDERIZACION POR COMPLETO DE UN CAMPO
	 * EN EL FORMULARIO DE INSERCION /EDICION DE DATOS
	 * ANTES DE EVALUAR LOS PARAMETROS DEL CAMPO Y CONSTRUIR EL TAG CORRESPONDIENTE
	 * M.F. 2017.09.13
	 *
	 * @param string $fieldName
	 * @param string $data
	 * @param array $fieldParams
	 * @return boolean
	 */
	public function displayFieldOnForm($fieldName=null, $data='', $fieldParams=[])
	{
		$returnValue=false;

		if($fieldParams['displayFieldOn'])	// POSIBILIDADES DINAMICAS PARA LA VISUALIZACION DEL CAMPO
		{
			$paramsIsDisplay=json_decode($fieldParams['displayFieldOn']);

			if(count($paramsIsDisplay))
			{
				$isNotEqual=false;

				foreach($paramsIsDisplay as $objectName=>$objectContents)
				{
					if(is_object($objectContents))
					{
						switch($objectName)
						{
							case 'notEqual':	// EL VALOR DEBE SER DISTINTO A
								foreach($objectContents as $objectContentDetails=>$detail)
								{

									if($detail == $data && $objectContentDetails === 'value')
									{
										$isNotEqual=true;
									}

									if($objectContentDetails === 'action' && $detail === 'return' && $isNotEqual === true)
									{
										$returnValue=true;
									}
								}
								break;
						}
					}
				}
			}
		}
//echo '<hr>$fieldName='.$fieldName.' : '.$data;
		return $returnValue;
	}


	/**
	 * TAG PARA LA CONSTRUCCION DE LOS GRUPOS O BLOQUES DE CAMPOS
	 * QUE APARECEN EN EL FORMULARIO
	 * A BLOQUE SE LE PUEDE APLICAR UN TITULAR, UNA DESCRIPCION Y UN ICONO
	 * TODO ELLO EN "$headerGroupElem"
	 *
	 * @param array $headerGroupElem
	 * @param int $refBlock
	 * @return string
	 */
	public function buildGroupHeader($headerGroupElem, $refBlock=0)
	{
		$outHTML="\r\n\t\t".'<div class="bs-item z-depth-1">';
		$outHTML.="\r\n\t\t".'<div class="card-header ch-alt '.$headerGroupElem['groupHeaderClass'].'"><h2>';
		$outHTML.="\r\n\t\t\t".'<span><i class="c-gray '.$headerGroupElem['groupIco'].'"></i>&nbsp;</span>';
		$outHTML.="\r\n\t\t\t".'<span>'.$headerGroupElem['groupTextTitle'].'</span>';
		$outHTML.="\r\n\t\t\t".'<div class="fltBtnClose"><button type="button" class="btn waves-effect hideFormGroup" data-ref="'.$refBlock.'" title="'.__COMMON_EDITMODULE_BTNCONTRACTTEXT.'"><i class="zmdi zmdi-window-minimize"> </i> </button></div>';
		$outHTML.="\r\n\t\t\t".'</h2>';
		$outHTML.="\r\n\t\t\t".'<span>'.$headerGroupElem['additionalInfo'].'</span>';
		$outHTML.="\r\n\t\t".'</div>';
		$outHTML.="\r\n\t\t".'</div><br />';
		return $outHTML;
	}


	/**
	 * TAG input TYPE text
	 * DEVUELVE EL HTML DE LOS CAMPOS TIPO TEXT
	 *
	 * @param string $fieldName
	 * @param array $fieldParams
	 * @param array $dataForm
	 * @param string $mainTableName
	 * @return string
	 */
	public function buildFormText($fieldName='', $fieldParams=[], $dataForm=null, $mainTableName='')
	{
    $bbddValue=isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : null;
		$isRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? ' required="required"' : '';
		$isDisabled=(isset($fieldParams['isDisabled']) && $fieldParams['isDisabled']) ? ' disabled="disabled"' : '';
		$colsClass=(isset($fieldParams['colsClass']) && $fieldParams['colsClass']) ? (int)$fieldParams['colsClass'] : 3;
		$highlightRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? '  *' : '';
		$isLocalJsFunc=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? 1 : '';
		$highlightRequired.=($isLocalJsFunc) ? ' (+)' : '';
		$additionalAlt=(isset($fieldParams['required']) && $fieldParams['required'] ? ' ('.__COMMON_EDITMODULE_MANDATORYALTTXT.')' : '');
		$additionalAlt.=($isLocalJsFunc && strstr(strtolower($fieldParams['extraFunc']), 'modal')) ? $this->altExtraFuncText : '';

		$tipElement=(isset($fieldParams['helpText']) || isset($fieldParams['helpTextExtend']) || isset($fieldParams['required'])) ? ' data-trigger="hover" data-toggle="popover" data-placement="top"' : '';
		$tipElement.=' data-original-title="'.(isset($fieldParams['helpText']) && $fieldParams['helpText'] ? $fieldParams['helpText'] : '').'" ';
		$tipElement.=' data-content="'.(isset($fieldParams['helpTextExtend']) ? ''.$fieldParams['helpTextExtend'] : '').$additionalAlt.'" ';
		$tipElement.=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? ' data-func-params="'.$fieldParams['extraFunc'].'"' : '';
		$tagPattern=(isset($fieldParams['tagPattern']) && $fieldParams['tagPattern']) ? 'pattern="'.$fieldParams['tagPattern'].'"' : '';

		// SI EXISTE EL VALOR, SE SUSTITUYE EL ID X EL CONTENIDO DE LA TABLA DE ORIGEN
		if(isset($dataForm['displayValue='.$fieldName]) && isset($dataForm['displayValue='.$fieldName][0]['id_list']) && $bbddValue == $dataForm['displayValue='.$fieldName][0]['id_list'])
			$bbddValue=$dataForm['displayValue='.$fieldName][0]['text_list'];

		$bbddValue=isset($dataForm['select='.$fieldName]) && count($dataForm['select='.$fieldName]) && isset($fieldParams['onlyFieldValueName']) && $fieldParams['onlyFieldValueName'] ? $dataForm['select='.$fieldName][0][$fieldParams['onlyFieldValueName']] : $bbddValue;

		if(!count($fieldParams))
			return;

    // HIDE TEXT INPUT -- EN PRUEBA
    if($fieldParams['dataType'] == 'hide')
    {
			$outHTML="\r\n\t\t".'<div class="hide">';
			$outHTML.="\r\n\t\t\t".'<input type="hidden" name="db['.$mainTableName.']['.$fieldName.']" value="'.$bbddValue.'"  />';
      $outHTML.="\r\n\t\t".'</div>';
      return $outHTML;
    }

    // SLIDER OUTPUT
    if($fieldParams['dataType'] == 'slider')
    {
			$outHTML="\r\n\t\t".'<div class="col-sm-'.$colsClass.' m-b-20">';
      $outHTML.="\r\n\t\t\t".'<label for="'.$fieldName.'">'.$fieldParams['label'].$highlightRequired.'</label>';
      $outHTML.="\r\n\t\t".'<div id="slider-'.$fieldName.'" class="input-slider m-b-15 " '.$tipElement.'>';
			$outHTML.="\r\n\t\t\t".'<input type="hidden" name="db['.$mainTableName.']['.$fieldName.']" value="'.$bbddValue.'" data-ref-name="slider-'.$fieldName.'" />';
      $outHTML.="\r\n\t\t".'</div></div>';
      return $outHTML;
    }

		$outHTML="\r\n\t\t".'<div class="col-sm-'.$colsClass.' m-b-20">';
		$outHTML.="\r\n\t\t".'<span class="fg-line">';
		$outHTML.="\r\n\t\t\t".'<label for="'.$fieldName.'">'.$fieldParams['label'].$highlightRequired.'</label>';
		$outHTML.="\r\n\t\t\t\t".'<input class="form-control" placeholder="'.$fieldParams['placeHolder'].'" type="'.$fieldParams['formType'].'" name="db['.$mainTableName.']['.$fieldName.']" value="'.$bbddValue.'" '.$isRequired.' '.$isDisabled.' '.$tipElement.' '.$tagPattern.' />';
		$outHTML.="\r\n\t\t\t".'';
		$outHTML.="\r\n\t\t".'</span>';
		$outHTML.="\r\n\t\t".'</div>';
		return $outHTML;
	}


	/**
	 * TAG input TYPE email
	 * DEVUELVE EL HTML DE LOS CAMPOS TIPO EMAIL
	 *
	 * @param string $fieldName
	 * @param array $fieldParams
	 * @param array $dataForm
	 * @param string $mainTableName
	 * @return string
	 */
	public function buildFormEmail($fieldName='', $fieldParams=[], $dataForm=null, $mainTableName='')
	{
    $bbddValue=isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : null;
		$isRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? ' required="required"' : '';
		$isDisabled=(isset($fieldParams['isDisabled']) && $fieldParams['isDisabled']) ? ' disabled="disabled"' : '';
		$colsClass=(isset($fieldParams['colsClass']) && $fieldParams['colsClass']) ? (int)$fieldParams['colsClass'] : 3;
		$highlightRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? '  *' : '';
		$isLocalJsFunc=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? 1 : '';
		$highlightRequired.=($isLocalJsFunc) ? ' (+)' : '';
		$additionalAlt=(isset($fieldParams['required']) && $fieldParams['required'] ? ' ('.__COMMON_EDITMODULE_MANDATORYALTTXT.')' : '');
		$additionalAlt.=($isLocalJsFunc && strstr(strtolower($fieldParams['extraFunc']), 'modal')) ? $this->altExtraFuncText : '';

		$tipElement=(isset($fieldParams['helpText']) || isset($fieldParams['helpTextExtend']) || isset($fieldParams['required'])) ? ' data-trigger="hover" data-toggle="popover" data-placement="top"' : '';
		$tipElement.=' data-original-title="'.(isset($fieldParams['helpText']) && $fieldParams['helpText'] ? $fieldParams['helpText'] : '').'" ';
		$tipElement.=' data-content="'.(isset($fieldParams['helpTextExtend']) ? ''.$fieldParams['helpTextExtend'] : '').$additionalAlt.'" ';
		$tagPattern=(isset($fieldParams['tagPattern']) && $fieldParams['tagPattern']) ? 'pattern="'.$fieldParams['tagPattern'].'"' : '';

		// SI EXISTE EL VALOR, SE SUSTITUYE EL ID X EL CONTENIDO DE LA TABLA DE ORIGEN
		if(isset($dataForm['displayValue='.$fieldName]) && isset($dataForm['displayValue='.$fieldName][0]['id_list']) && $bbddValue == $dataForm['displayValue='.$fieldName][0]['id_list'])
			$bbddValue=$dataForm['displayValue='.$fieldName][0]['text_list'];

		if(!count($fieldParams))
			return;

    // SLIDER OUTPUT
    if($fieldParams['dataType'] == 'slider')
    {
			$outHTML="\r\n\t\t".'<div class="col-sm-'.$colsClass.' m-b-20">';
      $outHTML.="\r\n\t\t\t".'<label for="'.$fieldName.'">'.$fieldParams['label'].$highlightRequired.'</label>';
      $outHTML.="\r\n\t\t".'<div id="slider-'.$fieldName.'" class="input-slider m-b-15 " '.$tipElement.'>';
			$outHTML.="\r\n\t\t\t".'<input type="hidden" name="db['.$mainTableName.']['.$fieldName.']" value="'.$bbddValue.'" data-ref-name="slider-'.$fieldName.'" />';
      $outHTML.="\r\n\t\t".'</div></div>';
      return $outHTML;
    }

		$outHTML="\r\n\t\t".'<div class="col-sm-'.$colsClass.' m-b-20">';
		$outHTML.="\r\n\t\t".'<span class="fg-line">';
		$outHTML.="\r\n\t\t\t".'<label for="'.$fieldName.'">'.$fieldParams['label'].$highlightRequired.'</label>';
		$outHTML.="\r\n\t\t\t\t".'<input class="form-control '.@$fieldParams['cssControl'].'" placeholder="'.$fieldParams['placeHolder'].'" type="'.$fieldParams['formType'].'" name="db['.$mainTableName.']['.$fieldName.']" value="'.$bbddValue.'" '.$isRequired.' '.$isDisabled.' '.$tipElement.' '.$tagPattern.' />';
		$outHTML.="\r\n\t\t\t".'';
		$outHTML.="\r\n\t\t".'</span>';
		$outHTML.="\r\n\t\t".'</div>';
		return $outHTML;
	}


	/**
	 * TAG input TYPE check-box
	 * DEVUELVE EL HTML DE LOS CAMPOS TIPO CHECK-BOX
	 *
	 * @param string $fieldName
	 * @param array $fieldParams
	 * @param array $dataForm
	 * @param string $mainTableName
	 * @return string
	 */
	public function buildFormCheckBox($fieldName='', $fieldParams=[], $dataForm=null, $mainTableName='')
	{
    $bbddValue=isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : 0; $tsColor='';
		$isRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? ' required="required"' : '';
		$isDisabled=(isset($fieldParams['isDisabled']) && $fieldParams['isDisabled']) ? ' disabled="disabled"' : '';
		$highlightRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? '  *' : '';
		$isLocalJsFunc=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? 1 : '';
		$highlightRequired.=($isLocalJsFunc) ? ' (+)' : '';
		$additionalAlt=(isset($fieldParams['required']) && $fieldParams['required'] ? ' ('.__COMMON_EDITMODULE_MANDATORYALTTXT.')' : '');
		$additionalAlt.=($isLocalJsFunc && strstr(strtolower($fieldParams['extraFunc']), 'modal')) ? $this->altExtraFuncText : '';

		$tipElement=(isset($fieldParams['helpText']) || isset($fieldParams['helpTextExtend']) || isset($fieldParams['required'])) ? ' data-trigger="hover" data-toggle="popover" data-placement="top"' : '';
		$tipElement.=' data-original-title="'.(isset($fieldParams['helpText']) && $fieldParams['helpText'] ? $fieldParams['helpText'] : '').'" ';
		$tipElement.=' data-content="'.(isset($fieldParams['helpTextExtend']) ? ''.$fieldParams['helpTextExtend'] : '').$additionalAlt.'" ';
		$tipElement.=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? ' data-func-params="'.$fieldParams['extraFunc'].'"' : '';

		if(!count($fieldParams))
			return;

		$paramChecked=$bbddValue ? ' checked="checked" ' : '';
		$outHTML="\r\n\t\t".'<div class="col-sm-3 m-b-20" '.$tipElement.'>';
		$outHTML.="\r\n\t\t".'<span class="fg-line">';

		if(isset($fieldParams['arrValuesRadioTag']) && count($fieldParams['arrValuesRadioTag']))
		{
			$outHTML.="\r\n\t\t\t".'<p class="m-r-10">'.$fieldParams['label'].$highlightRequired.'<i class="input-helper"></i></p>';
			foreach($fieldParams['arrValuesRadioTag'] as $key=>$value)
			{
				$isSelected=($dataForm[$fieldName] == $key) ? ' checked="checked"' : '';
				$outHTML.="\r\n\t\t\t".'<label class="checkbox checkbox-inline m-r-10">';
				$outHTML.="\r\n\t\t\t\t".'<input type="'.$fieldParams['formType'].'" name="db['.$mainTableName.']['.$fieldName.'][]" value="'.$key.'" '.$isSelected.' '.$isRequired.' '.$isDisabled.' />';
				$outHTML.="\r\n\t\t\t".$value.'<i class="input-helper '.@$fieldParams['cssControl'].'"></i></label>';
			}
		}
		else
		{
			if($fieldParams['dataType'] == 'toggle')	// CONTROL DESLIZANTE PARA SELECCIONAR SI / NO
			{
				$dataForm[$fieldName]=isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : 0; // FORCED TO 0 FOR NEW RECORDS
				$tsColor=$fieldParams['toggleParams']['tsColor'] ? $fieldParams['toggleParams']['tsColor'] : 'black';
				$isSelected=((int)$dataForm[$fieldName] > 0) ? ' checked="checked"' : '';
				$outHTML.="\r\n\t\t".'<div class="toggle-switch toggle-switch-demo" data-ts-color="'.$tsColor.'">';
				$outHTML.="\r\n\t\t\t".'<label for="tmp_check_['.$mainTableName.']['.$fieldName.'][]" class="ts-label">'.$fieldParams['label'].$highlightRequired.'</label>';
				$outHTML.="\r\n\t\t\t\t".'<input class="singleCheck '.@$fieldParams['cssControl'].'" hidden="hidden" type="'.$fieldParams['formType'].'" id="tmp_check_['.$mainTableName.']['.$fieldName.'][]" name="tmp_check_['.$mainTableName.']['.$fieldName.'][]" value="'.$dataForm[$fieldName].'" '.$isSelected.'  '.$isRequired.' '.$isDisabled.' />';
				$outHTML.="\r\n\t\t".'<label class="ts-helper" for="tmp_check_['.$mainTableName.']['.$fieldName.'][]" ></label>';
				$outHTML.="\r\n\t\t".'</div>';
				$outHTML.="\r\n\t\t\t".'<input type="hidden" name="db['.$mainTableName.']['.$fieldName.'][]" value="'.(isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : 0).'" />';
			}
			else
			{
				$outHTML.="\r\n\t\t\t".'<label class="checkbox checkbox-inline m-r-20">';
				$outHTML.="\r\n\t\t\t\t".'<input class="singleCheck '.@$fieldParams['cssControl'].'" type="'.$fieldParams['formType'].'" data-name="db['.$mainTableName.']['.$fieldName.']" id="tmp_check_['.$mainTableName.']['.$fieldName.']" value="'.$dataForm[$fieldName].'" '.$isRequired.' '.$isDisabled.' '.$paramChecked.'/>';
				$outHTML.="\r\n\t\t\t".$fieldParams['label'].$highlightRequired.'<i class="input-helper"></i></label>';
				$outHTML.="\r\n\t\t\t".'<input type="hidden" name="db['.$mainTableName.']['.$fieldName.']" value="'.(isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : 0).'" />';
			}
		}

		$outHTML.="\r\n\t\t".'</span>';
		$outHTML.="\r\n\t\t".'</div>';
		return $outHTML;
	}


	/**
	 *
	 * @param string $fieldName
	 * @param array $fieldParams
	 * @param array $dataForm
	 * @param string $mainTableName
	 * @return string
	 */
	public function buildFormRadio($fieldName='', $fieldParams=[], $dataForm=null, $mainTableName='')
	{
    $bbddValue=isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : null;
		$isRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? ' required="required"' : '';
		$isDisabled=(isset($fieldParams['isDisabled']) && $fieldParams['isDisabled']) ? ' disabled="disabled"' : '';
		$highlightRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? '  *' : '';
		$isLocalJsFunc=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? 1 : '';
		$highlightRequired.=($isLocalJsFunc) ? ' (+)' : '';
		$additionalAlt=(isset($fieldParams['required']) && $fieldParams['required'] ? ' ('.__COMMON_EDITMODULE_MANDATORYALTTXT.')' : '');
		$additionalAlt.=($isLocalJsFunc && strstr(strtolower($fieldParams['extraFunc']), 'modal')) ? $this->altExtraFuncText : '';

		$tipElement=(isset($fieldParams['helpText']) || isset($fieldParams['helpTextExtend']) || isset($fieldParams['required'])) ? ' data-trigger="hover" data-toggle="popover" data-placement="top"' : '';
		$tipElement.=' data-original-title="'.(isset($fieldParams['helpText']) && $fieldParams['helpText'] ? $fieldParams['helpText'] : '').'" ';
		$tipElement.=' data-content="'.(isset($fieldParams['helpTextExtend']) ? ''.$fieldParams['helpTextExtend'] : '').$additionalAlt.'" ';
		$tipElement.=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? ' data-func-params="'.$fieldParams['extraFunc'].'"' : '';

		if(!count($fieldParams))
			return;

		$outHTML="\r\n\t\t".'<div class="col-sm-3 m-b-20" '.$tipElement.'>';
		$outHTML.="\r\n\t\t".'<span class="fg-line">';
		$outHTML.="\r\n\t\t\t".'<label class="m-r-10">'.$fieldParams['label'].$highlightRequired.'<i class="input-helper"></i></label>';

		if(isset($fieldParams['arrValuesRadioTag']) && count($fieldParams['arrValuesRadioTag']))
		{
			foreach($fieldParams['arrValuesRadioTag'] as $key=>$value)
			{
				$isSelected=($dataForm[$fieldName] == $key) ? ' checked="checked"' : '';
				$outHTML.="\r\n\t\t\t".'<label class="radio radio-inline m-r-10">';
				$outHTML.="\r\n\t\t\t\t".'<input class="'.@$fieldParams['cssControl'].'" type="'.$fieldParams['formType'].'" name="db['.$mainTableName.']['.$fieldName.'][]" value="'.$key.'" '.$isSelected.' '.$isRequired.' '.$isDisabled.' />';
				$outHTML.="\r\n\t\t\t".$value.'<i class="input-helper"></i></label>';
			}
		}

		$outHTML.="\r\n\t\t".'</span>';
		$outHTML.="\r\n\t\t".'</div>';
		return $outHTML;
	}


	/**
	 *	TAG input  TYPE file
	 * (two output modes:  1 = only images, 2 = for all files)
	 *
	 * @param string $fieldName
	 * @param array $fieldParams
	 * @param array $dataForm
	 * @param string $mainTableName
	 * @return string
	 */
	public function buildFormFile($fieldName='', $fieldParams=[], $dataForm=null, $mainTableName='')
	{
    $bbddValue=isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : null;
		$isRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? ' required="required"' : '';
		$isDisabled=(isset($fieldParams['isDisabled']) && $fieldParams['isDisabled']) ? ' disabled="disabled"' : '';
		$highlightRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? '  *' : '';
		$isLocalJsFunc=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? 1 : '';
		$highlightRequired.=($isLocalJsFunc) ? ' (+)' : '';
		$additionalAlt=(isset($fieldParams['required']) && $fieldParams['required'] ? ' ('.__COMMON_EDITMODULE_MANDATORYALTTXT.')' : '');
		$additionalAlt.=($isLocalJsFunc && strstr(strtolower($fieldParams['extraFunc']), 'modal')) ? $this->altExtraFuncText : '';

		$tipElement=(isset($fieldParams['helpText']) || isset($fieldParams['helpTextExtend']) || isset($fieldParams['required'])) ? ' data-trigger="hover" data-toggle="popover" data-placement="top"' : '';
		$tipElement.=' data-original-title="'.(isset($fieldParams['helpText']) && $fieldParams['helpText'] ? $fieldParams['helpText'] : '').'" ';
		$tipElement.=' data-content="'.(isset($fieldParams['helpTextExtend']) ? ''.$fieldParams['helpTextExtend'] : '').$additionalAlt.'" ';
		$tipElement.=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? ' data-func-params="'.$fieldParams['extraFunc'].'"' : '';
		$tagPattern=(isset($fieldParams['tagPattern']) && $fieldParams['tagPattern']) ? 'pattern="'.$fieldParams['tagPattern'].'"' : '';

		// SI EXISTE EL VALOR, SE SUSTITUYE EL ID X EL CONTENIDO DE LA TABLA DE ORIGEN
		if(isset($dataForm['displayValue='.$fieldName]) && isset($dataForm['displayValue='.$fieldName][0]['id_list']) && $bbddValue == $dataForm['displayValue='.$fieldName][0]['id_list'])
			$bbddValue=$dataForm['displayValue='.$fieldName][0]['text_list'];

		if(!count($fieldParams))
			return;

		$outHTML="\r\n\t\t".'<div class="col-sm-4 m-b-20">';
		$htmlFileFolder=(isset($fieldParams['htmlFolder']) && $fieldParams['htmlFolder']) ? ''.$fieldParams['htmlFolder'].'' : '';
		$highLightCheck=''; $txtMsgError='';

		if((isset($fieldParams['targetFolder']) && isset($dataForm[$fieldName]) && trim($dataForm[$fieldName])))	// CHECK EXISTENCIA DEL ARCHIVO EN EL ESERVIDOR
		{
			$flagFileExist=file_exists($fieldParams['targetFolder'].$dataForm[$fieldName]);
			$highLightCheck=$flagFileExist ? '' : 'alert alert-danger';
			$txtMsgError=$flagFileExist ? '' : __COMMON_EDITMODULE_BUILDFORMFILENOEXISTERRORTXT;
		}

    if($fieldParams['dataType'] == 'imageFile')  // 1. ONLY FOR FILES TYPE image
    {
      $dataForm[$fieldName]=isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : '';
      $outHTML.="\r\n\t\t\t".'<label for="'.$fieldName.'">'.$fieldParams['label'].$highlightRequired.'</label><br />';
      $outHTML.="\r\n\t\t".'<div class="fileinput fileinput-new text-center" data-provides="fileinput">';
			$outHTML.="\r\n\t\t".'<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="max-width:200px;max-height:150px;">';

			if($dataForm[$fieldName])
				$outHTML.="\r\n\t\t".'<img class="'.$highLightCheck.'" src="'.$htmlFileFolder.$dataForm[$fieldName].'" alt="'.($txtMsgError ? $txtMsgError : $fieldParams['label']).'" title="'.$dataForm[$fieldName].'" />';
			else
				$outHTML.="\r\n\t\t".'<img class="'.$highLightCheck.'" src="../../img/file_not_found.png" title="'.__COMMON_EDITMODULE_BUILDFORMFILENOFILESTARTTITLE.($dataForm[$fieldName] ? $dataForm[$fieldName] : __COMMON_EDITMODULE_BUILDFORMFILENOFILEENDTITLE.' `'.$fieldParams['label']).'`" />';

			$outHTML.="\r\n\t\t".'</div>';
			$outHTML.="\r\n\t\t".'<br />
			<span class="btn btn-info btn-file">
				<span class="fileinput-exists">'.__COMMON_BTN_CHANGE.'</span>';
			$outHTML.="\r\n\t\t\t\t".'<input type="'.$fieldParams['formType'].'" value="'.$dataForm[$fieldName].'" name="arrFiles['.$mainTableName.']['.$fieldName.']" '.$isRequired.' '.$isDisabled.' '.$tipElement.' '.$tagPattern.' data-previous-image="'.$dataForm[$fieldName].'" data-image-delete="'.$fieldName.'" data-image-src="'.$dataForm[$fieldName].'" class="'.@$fieldParams['cssControl'].'" />';
			$outHTML.="\r\n\t\t\t".'</span>';

      if($dataForm[$fieldName])
      {
			  $outHTML.="\r\n\t\t\t".'<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput" title="'.__COMMON_BTN_DELETE.'" data-field="'.$fieldName.'" data-table="'.$mainTableName.'">&times;</a>';
        $outHTML.="\r\n\t\t\t".'<a href="#" class="btn btn-zoom-image fileinput-exists zmdi zmdi-zoom-in bgm-green" title="'.__COMMON_BTN_ZOOM.'" data-toggle="modal" data-target=".zoom-modal-image" data-img="'.$htmlFileFolder.$dataForm[$fieldName].'" data-table="'.$mainTableName.'"></a>';
      }
      $outHTML.="\r\n\t\t".'</div>';
    }
    else  // 2. ALL OTHER FILES
    {
      $outHTML.="\r\n\t\t\t".'<p class="m-b-20">'.$fieldParams['label'].$highlightRequired.'</p>';
      $outHTML.="\r\n\t\t".'<div class="fileinput fileinput-new" data-provides="fileinput">';
			$outHTML.='
			<span class="btn btn-primary btn-file m-r-10">
				<span class="fileinput-new">'.__COMMON_GENERICTEXT_SELECTTXT.'</span>
				<span class="fileinput-exists"> / Cambiar</span>';
      $outHTML.="\r\n\t\t\t\t".'<input type="'.$fieldParams['formType'].'" value="'.$dataForm[$fieldName].'" name="arrFiles['.$mainTableName.']['.$fieldName.']" '.$isRequired.' '.$isDisabled.' '.$tipElement.' '.$tagPattern.' data-previous-image="'.$dataForm[$fieldName].'" data-image-delete="'.$fieldName.'" data-image-src="'.$dataForm[$fieldName].'" class="'.@$fieldParams['cssControl'].'" />';
      $outHTML.="\r\n\t\t\t".'</span>';
      $outHTML.="\r\n\t\t\t".'<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput" title="'.__COMMON_BTN_DELETE.'" data-field="'.$fieldName.'" data-table="'.$mainTableName.'">&times;</a>';
      $outHTML.="\r\n\t\t".'<span class="fileinput-filename"><span class="'.$highLightCheck.'">'.(isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : $txtMsgError).'</span></span>';
      $outHTML.="\r\n\t\t".'</div>';
    }
		$outHTML.="\r\n\t\t".'</div>';
		return $outHTML;
	}


	/**
	 * TAG input TYPE text --- ONLY FOR DATES) ---
	 *
	 * @param string $fieldName
	 * @param array $fieldParams
	 * @param array $dataForm
	 * @param string $mainTableName
	 * @return string
	 */
	public function buildFormDate($fieldName='', $fieldParams=[], $dataForm=null, $mainTableName='')
	{
		error_reporting(E_ERROR);
		$outHTML='';
		$dateDisplay=''; $hourDisplay=''; $dateFormat='';
		$classDate=array('timestamp'=>'date-time-picker','date'=>'date-picker','time'=>'time-picker');
    $bbddValue=isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : null;

		if(isset($bbddValue) && $bbddValue && isset($fieldParams['dataType']))
		{
			if(strtolower($fieldParams['dataType']) == 'timestamp' || strtolower($fieldParams['dataType']) == 'date')
			{
				$dateDisplay=date('Y-m-d', strtotime($bbddValue));
				$dateFormat.=__COMMON_DATEFORMAT_YMD;
			}

			if(strtolower($fieldParams['dataType']) == 'timestamp' || strtolower($fieldParams['dataType']) == 'time')
			{
				$hourDisplay=date(' H:i', strtotime($bbddValue));
				$dateFormat.=' '.__COMMON_DATEFORMAT_HM;
			}
		}

//		if($fieldParams['displayFieldOn'])	// POSIBILIDADES DINAMICAS PARA LA VISUALIZACION DEL CAMPO
//		{
//			$paramsIsDisplay=json_decode($fieldParams['displayFieldOn']);
//
//			if(count($paramsIsDisplay))
//			{
//				$isNotEqual=false;
//
//				foreach($paramsIsDisplay as $objectName=>$objectContents)
//				{
//					if(is_object($objectContents))
//					{
//						switch($objectName)
//						{
//							case 'notEqual':	// EL VALOR DEBE SER DISTINTO A
//								foreach($objectContents as $objectContentDetails=>$detail)
//								{
//									if($detail === $bbddValue && $objectContentDetails === 'value')
//										$isNotEqual=true;
//
//									if($objectContentDetails === 'action' && $detail === 'return' && $isNotEqual == true)
//									{
//										return;
//									}
//								}
//								break;
//						}
//					}
//				}
//			}
////			return;
//		}

		$isRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? ' required="required"' : '';
		$isDisabled=(isset($fieldParams['isDisabled']) && $fieldParams['isDisabled']) ? ' disabled="disabled"' : '';
		$colsClass=(isset($fieldParams['colsClass']) && $fieldParams['colsClass']) ? (int)$fieldParams['colsClass'] : 3;
		$highlightRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? '  *' : '';
		$isLocalJsFunc=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? 1 : '';
		$highlightRequired.=($isLocalJsFunc) ? ' (+)' : '';
		$additionalAlt=(isset($fieldParams['required']) && $fieldParams['required'] ? ' ('.__COMMON_EDITMODULE_MANDATORYALTTXT.')' : '');
		$additionalAlt.=($isLocalJsFunc && strstr(strtolower($fieldParams['extraFunc']), 'modal')) ? $this->altExtraFuncText : '';

		$tipElement=(isset($fieldParams['helpText']) || isset($fieldParams['helpTextExtend']) || isset($fieldParams['required'])) ? ' data-trigger="hover" data-toggle="popover" data-placement="top"' : '';
		$tipElement.=' data-original-title="'.(isset($fieldParams['helpText']) && $fieldParams['helpText'] ? $fieldParams['helpText'] : '').'" ';
		$tipElement.=' data-content="'.(isset($fieldParams['helpTextExtend']) ? ''.$fieldParams['helpTextExtend'] : '').$additionalAlt.'" ';
		$tipElement.=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? ' data-func-params="'.$fieldParams['extraFunc'].'"' : '';

		if(!count($fieldParams))
			return;

		$outHTML.="\r\n\t\t".'<div class="col-sm-'.$colsClass.' m-b-20">';
		$outHTML.=$this->moduleErrorTxt;
		$outHTML.="\r\n\t\t".'<div class="dtp-container">';
		$outHTML.="\r\n\t\t\t".'<label for="'.$fieldName.'">'.$fieldParams['label'].$highlightRequired.'</label>';
		$outHTML.="\r\n\t\t\t\t".'<input data-date-format="'.$dateFormat.'" class="form-control '.$classDate[$fieldParams['dataType']].' '.@$fieldParams['cssControl'].'" placeholder="'.$fieldParams['placeHolder'].'" type="text" name="db['.$mainTableName.']['.$fieldName.']" value="'.$dateDisplay.$hourDisplay.'" '.$isDisabled.' '.$isRequired.' '.$tipElement.' />';
		$outHTML.="\r\n\t\t\t".'';
		$outHTML.="\r\n\t\t".'</div>';
		$outHTML.="\r\n\t\t".'</div>';
		return $outHTML;
	}


	/**
	 * TAG TEXTAREA
	 *
	 * @param string $fieldName
	 * @param array $fieldParams
	 * @param string $dataForm
	 * @param type $mainTableName
	 * @return string
	 */
	public function buildFormTextarea($fieldName='', $fieldParams=[], $dataForm=null, $mainTableName='')
	{
		$editHtmlModal=(isset($fieldParams['actionsForm']->isHtmlBtn) && $fieldParams['actionsForm']->isHtmlBtn ? '&nbsp;&nbsp;<a href="#htmlPlainBox" class="btn btn-xs bgm-gray waves-effect modalHtmlPlainBox" title="'.__COMMON_BTN_HTMLEDITOR.'" data-toggle="modal" data-field-name="'.$fieldName.'">HTML</a>' : '');
		$editHtmlModal.=(isset($fieldParams['actionsForm']->cssExternalFile) && $fieldParams['actionsForm']->cssExternalFile ? '<link href="'.$fieldParams['actionsForm']->cssExternalFile.'" rel="stylesheet" />' : '');
		$editHtmlModal.=(isset($fieldParams['actionsForm']->isWysiBtn) && $fieldParams['actionsForm']->isWysiBtn ? '&nbsp;&nbsp;<a href="#htmlWysiBox" class="btn btn-xs bgm-cyan waves-effect modalHtmlWysiBox" title="'.__COMMON_BTN_WYSIWYGVIEWTITLE.'" data-toggle="modal" data-field-name="'.$fieldName.'">'.__COMMON_BTN_WYSIWYGVIEWTXT.'</a>' : '');
		$editHtmlModal.=(isset($fieldParams['actionsForm']->wysiwigEditor['modal']) && $fieldParams['actionsForm']->wysiwigEditor['modal'] ? '&nbsp;&nbsp;<a href="#htmlWysiEditBox" class="btn btn-xs bgm-cyan waves-effect modalHtmlWysiEditBox" title="'.__COMMON_BTN_WYSIWYGEDITTITLE.'" data-toggle="modal" data-field-name="'.$fieldName.'">'.__COMMON_BTN_WYSIWYGEDITTXT.'</a>' : '');
    $bbddValue=isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : null;
		$isRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? ' required="required"' : '';
		$isDisabled=(isset($fieldParams['isDisabled']) && $fieldParams['isDisabled']) ? ' disabled="disabled"' : '';
		$rowsTag=(isset($fieldParams['rowsData']) && $fieldParams['rowsData']) ? (int)$fieldParams['rowsData'] : 3;
		$colsClass=(isset($fieldParams['colsClass']) && $fieldParams['colsClass']) ? (int)$fieldParams['colsClass'] : 6;
		$highlightRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? '  *' : '';
		$isLocalJsFunc=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? 1 : '';
		$highlightRequired.=($isLocalJsFunc) ? ' (+)' : '';
		$additionalAlt=(isset($fieldParams['required']) && $fieldParams['required'] ? ' ('.__COMMON_EDITMODULE_MANDATORYALTTXT.')' : '');
		$additionalAlt.=($isLocalJsFunc && strstr(strtolower($fieldParams['extraFunc']), 'modal')) ? $this->altExtraFuncText : '';

		$tipElement=(isset($fieldParams['helpText']) || isset($fieldParams['helpTextExtend']) || isset($fieldParams['required'])) ? ' data-trigger="hover" data-toggle="popover" data-placement="top"' : '';
		$tipElement.=' data-original-title="'.(isset($fieldParams['helpText']) && $fieldParams['helpText'] ? $fieldParams['helpText'] : '').'" ';
		$tipElement.=' data-content="'.(isset($fieldParams['helpTextExtend']) ? ''.$fieldParams['helpTextExtend'] : '').$additionalAlt.'" ';
		$tipElement.=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? ' data-func-params="'.$fieldParams['extraFunc'].'"' : '';

		if(!count($fieldParams))
			return;

		$outHTML="\r\n\t\t".'<div class="col-sm-'.$colsClass.' m-b-20">';
		$outHTML.="\r\n\t\t".'<span class="fg-line">';
		$outHTML.="\r\n\t\t\t".'<label for="'.$fieldName.'">'.$fieldParams['label'].$highlightRequired.'</label>'.$editHtmlModal;
		$outHTML.="\r\n\t\t\t\t".'<'.$fieldParams['formType'].' style="word-wrap:break-word;" class="form-control '.@$fieldParams['cssControl'].'" name="db['.$mainTableName.']['.$fieldName.']" '.$isRequired.' '.$isDisabled.' '.$tipElement.' rows='.$rowsTag.' >'.$bbddValue.'</'.$fieldParams['formType'].'>';
		$outHTML.="\r\n\t\t\t".'';
		$outHTML.="\r\n\t\t".'</span>';
		$outHTML.="\r\n\t\t".'</div>';
		return $outHTML;
	}


	/**
	 * TAG input TYPE text password
	 * DEVUELVE EL HTML DE LOS CAMPOS TIPO TEXT (password)
	 *
	 * @param string $fieldName
	 * @param array $fieldParams
	 * @param array $dataForm
	 * @param string $mainTableName
	 * @return string
	 */
	public function buildFormPassword($fieldName='', $fieldParams=[], $dataForm=null, $mainTableName='')
	{
    $bbddValue=isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : null;
		$isRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? ' required="required"' : '';
		$isDisabled=(isset($fieldParams['isDisabled']) && $fieldParams['isDisabled']) ? ' disabled="disabled"' : '';
		$colsClass=(isset($fieldParams['colsClass']) && $fieldParams['colsClass']) ? (int)$fieldParams['colsClass'] : 3;
		$highlightRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? '  *' : '';
		$isLocalJsFunc=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? 1 : '';
		$highlightRequired.=($isLocalJsFunc) ? ' (+)' : '';
		$additionalAlt=(isset($fieldParams['required']) && $fieldParams['required'] ? ' ('.__COMMON_EDITMODULE_MANDATORYALTTXT.')' : '');
		$additionalAlt.=($isLocalJsFunc && strstr(strtolower($fieldParams['extraFunc']), 'modal')) ? $this->altExtraFuncText : '';

		$tipElement=(isset($fieldParams['helpText']) || isset($fieldParams['helpTextExtend']) || isset($fieldParams['required'])) ? ' data-trigger="hover" data-toggle="popover" data-placement="top"' : '';
		$tipElement.=' data-original-title="'.(isset($fieldParams['helpText']) && $fieldParams['helpText'] ? $fieldParams['helpText'] : '').'" ';
		$tipElement.=' data-content="'.(isset($fieldParams['helpTextExtend']) ? ''.$fieldParams['helpTextExtend'] : '').$additionalAlt.'" ';
		$tipElement.=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? ' data-func-params="'.$fieldParams['extraFunc'].'"' : '';

		if(!count($fieldParams))
			return;

		$outHTML="\r\n\t\t".'<div class="col-sm-'.$colsClass.' m-b-20">';
		$outHTML.="\r\n\t\t".'<span class="fg-line">';
		$outHTML.="\r\n\t\t\t".'<label for="'.$fieldName.'">'.$fieldParams['label'].$highlightRequired.'</label>';
		$outHTML.="\r\n\t\t\t\t".'<input class="form-control '.@$fieldParams['cssControl'].'" placeholder="'.$fieldParams['placeHolder'].'" type="'.$fieldParams['formType'].'" name="db['.$mainTableName.']['.$fieldName.']" value="'.$bbddValue.'" '.$isRequired.' '.$isDisabled.' '.$tipElement.' />';
		$outHTML.="\r\n\t\t\t".'';
		$outHTML.="\r\n\t\t".'</span>';
		$outHTML.="\r\n\t\t".'</div>';

		return $outHTML;
	}


	/**
	 * TAG select
	 * DEVUELVE EL HTML DE LOS CAMPOS TIPO SELECT
	 *
	 * @param string $fieldName
	 * @param array $fieldParams
	 * @param array $dataForm
	 * @param string $mainTableName
	 * @return string
	 */
	public function buildFormSelect($fieldName='', $fieldParams=[], $dataForm=null, $mainTableName='')
	{
		$isRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? ' required="required"' : '';
		$isDisabled=(isset($fieldParams['isDisabled']) && $fieldParams['isDisabled']) ? ' disabled="disabled"' : '';
		$colsClass=(isset($fieldParams['colsClass']) && $fieldParams['colsClass']) ? (int)$fieldParams['colsClass'] : 3;
		$searchParam=(isset($fieldParams['selectSearchBox']) && $fieldParams['selectSearchBox']) ? ' data-live-search="true"' : '';
		$isMultiselect=(isset($fieldParams['multipleSelect']) && $fieldParams['multipleSelect']) ? 1 : null;
		$highlightRequired=(isset($fieldParams['required']) && $fieldParams['required']) ? '  *' : '';
		$isLocalJsFunc=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? 1 : '';
		$highlightRequired.=($isLocalJsFunc) ? ' (+)' : '';
		$additionalAlt=(isset($fieldParams['required']) && $fieldParams['required'] ? ' ('.__COMMON_EDITMODULE_MANDATORYALTTXT.')' : '');
		$additionalAlt.=($isLocalJsFunc && strstr(strtolower($fieldParams['extraFunc']), 'modal')) ? $this->altExtraFuncText : '';
		$options='';
		$tipElement=(isset($fieldParams['helpText']) || isset($fieldParams['helpTextExtend']) || isset($fieldParams['required'])) ? ' data-trigger="hover" data-toggle="popover" data-placement="top"' : '';
		$tipElement.=' data-original-title="'.(isset($fieldParams['helpText']) && $fieldParams['helpText'] ? $fieldParams['helpText'] : '').'" ';
		$tipElement.=' data-content="'.(isset($fieldParams['helpTextExtend']) ? ''.$fieldParams['helpTextExtend'] : '').$additionalAlt.'" ';
		$tipElement.=(isset($fieldParams['extraFunc']) && $fieldParams['extraFunc']) ? ' data-func-params="'.$fieldParams['extraFunc'].'"' : '';

		if(!count($fieldParams))
			return;

		$firstSelectValue=isset($fieldParams['defaultValue']) ? $fieldParams['defaultValue'] : ($fieldParams['allowZeroValue'] ? 0 : null);
//		$firstSelectValue=isset($fieldParams['defaultValue']) ? $fieldParams['defaultValue'] : ($fieldParams['dataType'] == 'integer') ? null : '';
//		$firstSelectValue='';
    $bbddValue=isset($dataForm[$fieldName]) ? $dataForm[$fieldName] : null;

		// SELECT CON ORIGEN DE DATOS POR BBDD, DEFINIDO EN "fieldTableName" PRESENTE EN LA CONFIGURACION DEL MODULO ABIERTO
		if(isset($dataForm['select='.$fieldName]) && count($dataForm['select='.$fieldName]) && !$isMultiselect)
		{
			if(isset($fieldParams['defaultSelectText']) && $fieldParams['defaultSelectText'] != '_SKIPDEFAULT_')
				$options='<option value="'.$firstSelectValue.'" '.($bbddValue == $firstSelectValue ? ' selected="selected"' : '').'>'.(isset($fieldParams['defaultSelectText']) ? $fieldParams['defaultSelectText'] : __COMMON_GENERICTEXT_SELECTTXT).'</option>';

			foreach($dataForm['select='.$fieldName] as $key=>$value)
			{
				$classSpecial=isset($value['markSpecial']) && $value['markSpecial'] ? ' '.$value['markSpecial'] : '';
//				$classSpecial=isset($value['markSpecial']) && $value['markSpecial'] ? ' f-700' : '';

        if(!isset($value['id_list']))
          continue;

        $tmpId=isset($value['id_list']) ? $value['id_list'] : 0;
				$tmpId=($fieldParams['dataType'] == 'integer') ? (int)$tmpId : $tmpId;
				$currentSelectedValue=$bbddValue == $tmpId ? ' selected="selected"' : '';
				$options.='<option value="'.$tmpId.'" '.$currentSelectedValue.' class="'.$classSpecial.'">'.$value['text_list'].'</option>';
			}
		}


		if(isset($fieldParams['sourceTagBbddData']) && isset($dataForm['select='.$fieldName]) && count($dataForm['select='.$fieldName]) && $isMultiselect)
		{
			$options='';

			foreach($dataForm['select='.$fieldName] as $key=>$value)
			{
				$currentSelectedValue='';

				if($value['selectedField'])
					$currentSelectedValue=' selected="selected"';

				$options.='<option value="'.$value['id_list'].'" '.$currentSelectedValue.'>'.$value['text_list'].'</option>';
			}
		}

		// MULTISELECT CON ORIGEN DE DATOS DEFINIDO EN LA BBDD DE LA TABLA "fieldTableName"
		if(!isset($fieldParams['sourceTagBbddData']) && isset($fieldParams['fieldTableName']) && isset($dataForm['select='.$fieldName]) && count($dataForm['select='.$fieldName]) && $isMultiselect)
		{
			$arrKeysBbddValue=explode(',', $bbddValue);	// OBTENCION DEL ARRAY PARA BUSCAR LOS VALORES ALMACENADOS EN LA BBDD, SEPARADOS POR "," (COMA)
			$options='';

			foreach($dataForm['select='.$fieldName] as $key=>$value)
			{
				$currentSelectedValue='';
				if(in_array($value['id_list'], $arrKeysBbddValue))
					$currentSelectedValue=' selected="selected"';

				$options.='<option value="'.$value['id_list'].'" '.$currentSelectedValue.'>'.$value['text_list'].'</option>';
			}
		}

    // SELECT (MULTIPLE) CON UN ARRAY "ESTATICO" DE VALORES PREDEFINIDOS
    if(isset($fieldParams['arrStaticValues']) && count($fieldParams['arrStaticValues']))
    {
			$arrKeysBbddValue=explode(',', $bbddValue);	// OBTENCION DEL ARRAY PARA BUSCAR LOS VALORES ALMACENADOS EN LA BBDD, SEPARADOS POR "," (COMA)
//echo'<p>-'.$bbddValue;
			$options='';
      $tmpClassOption=''; // array con clases css unicas para cada option de la select

			if($fieldParams['defaultSelectText'])
				$options='<option value="'.$firstSelectValue.'" >'.(isset($fieldParams['defaultSelectText']) ? ($fieldParams['defaultSelectText'] != 'DEFAULT_TEXT' ? $fieldParams['defaultSelectText'] : __COMMON_GENERICTEXT_SELECTTXT) : __COMMON_GENERICTEXT_SELECTTXT).'</option>';

      foreach($fieldParams['arrStaticValues'] as $key=>$value)	// SI EXISTE EL ARRAY "ESTATICO", SE RECORREN TODOS LOS ELEMENTOS PARA ENCONTRAR VALORES ALMACENADO EN BBDD
			{
				$currentSelectedValue='';
				if(in_array($key, $arrKeysBbddValue))
					$currentSelectedValue=' selected="selected"';

				if(isset($fieldParams['arrClassOptions']) && $fieldParams['arrClassOptions'][$key])
					$tmpClassOption=$fieldParams['arrClassOptions'][$key];

				$options.='<option class="'.$tmpClassOption.'" value="'.$key.'" '.$currentSelectedValue.'>'.$value.'</option>';
        $tmpClassOption='';
      }
    }

    $tagMultiple=$isMultiselect ? ' multiple="multiple"' : '';

//		$outHTML="\r\n\t\t".'<div class="col-sm-3 m-b-20">';
		$outHTML="\r\n\t\t".'<div class="col-sm-'.$colsClass.' m-b-20">';
		$outHTML.="\r\n\t\t".'<span class="fg-line ">';
		$outHTML.="\r\n\t\t\t".'<div class="select" '.$tipElement.'>'.$fieldParams['label'].$highlightRequired.'';
		$outHTML.="\r\n\t\t\t\t".'<select class="selectpicker '.@$fieldParams['cssControl'].'" placeholder="'.@$fieldParams['placeHolder'].' '.@$fieldParams['cssControl'].'" name="db['.$mainTableName.']['.$fieldName.'][]" '.$isRequired.' '.$isDisabled.' '.$tagMultiple.''.$searchParam.' data-none-selected-text="'.__COMMON_GENERICTEXT_NOTHINGSELECTEDTXT.'" >';
		$outHTML.="\r\n\t\t\t".''.$options;
		$outHTML.="\r\n\t\t".'</select></div>';
		$outHTML.="\r\n\t\t".'</span>';
		$outHTML.="\r\n\t\t".'</div>';
		return $outHTML;
	}

	public function __destruct() {}
}