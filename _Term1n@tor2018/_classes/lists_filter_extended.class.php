<?php
/**
 * COMMON LISTING EXTENDED FILTERS FUNCTIONS
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class filterExtendedSupport
{
	public $fieldName;
	public $fieldParams;
	public $html;
	public $classDate;
	private $classesContainerFilters;
	private $positionBlocksFilters;
	private $classesBgndFilters;
	private $tipPlacement;
	private $txtContents;

	public function __construct($fieldName='', $fieldParams=null)
	{
		$this->html='';
		$this->positionBlocksFilters=(isset($_SESSION['userAdmin']['pref_']['b_filters_location']) && $_SESSION['userAdmin']['pref_']['b_filters_location']) ? 1 : 0;
		$this->fieldName=$fieldName;
		$this->fieldParams=$fieldParams;
		$this->html=null;
		$this->classesContainerFilters=$this->positionBlocksFilters ? ' w-full  p-0  m-t-5' : 'col-sm-4 p-5 m-t-10';
		$this->classesBgndFilters=$this->positionBlocksFilters ? 'bgm-white  z-depth-1' : ' z-depth-1';
		$this->tipPlacement=$this->positionBlocksFilters ? 'left' : 'top';
		$this->classDate=array('timestamp'=>'date-time-picker-filter','date'=>'date-picker-filter','time'=>'time-picker-filter');
		return;
	}

	/**
	 * FILTRO DE TIPO select
	 *
	 * @param ARRAY $arrSqlData
	 * @return NULL
	 */
	public function getSelectFilter($arrSqlData=null)
	{
		$this->htmlBoxStart='<div class=\'alert c-black '.$this->classesContainerFilters.'\' role=\'alert\' style="min-width:270px!important;">
			<div class=\'p-10 bs-item '.$this->classesBgndFilters.'\'><button type=\'button\' class=\'btn bgm-red btn-xs waves-effect pull-right m-r-0 removeListFilter\' style=z-index:6; data-trigger="hover" data-toggle="popover" data-content="'.__LOCALAJAX_BTN_REMOVESEARCHFILTERTXT.'" data-placement="'.$this->tipPlacement.'" data-filter-name="sys_find_'.$this->fieldName.'"><i class="zmdi zmdi-grid-off p-l-5 p-r-5 f-14"></i></button>
			<button type=\'button\' class=\'btn bgm-lightgreen btn-xs waves-effect pull-right m-r-5 btnApplyFieldList\' style=z-index:6; data-trigger="hover" data-toggle="popover" data-content="'.__LOCALAJAX_BTN_APPLYSEARCHFILTERTXT.'" data-placement="'.$this->tipPlacement.'" data-filter-name="sys_find_'.$this->fieldName.'"><i class="zmdi zmdi-check p-l-5 p-r-5 f-700 f-14"></i></button>';
		$this->html.='<span>'.__LOCALAJAX_DATELABEL_SEARCH.': <b>'.$this->fieldParams['label'].'</b><br /></span>';
		$this->html.='<div class="select">
			<select id="sys_find_'.$this->fieldName.'" name="sys_find_'.$this->fieldName.'" class="form-control selectpicker" title="Todos" >';// data-live-search-style="startsWith" data-live-search="true"

		foreach($arrSqlData as $key=>$value)
		{
			$this->html.='<option value="'.$value['id_list'].'">';
			$this->html.=''.$value['text_list'];
			$this->html.='</option>';
		}

		$this->html.='</select></div>';
		$this->htmlBoxEnd='</div></div>';
		return;
	}


	/**
	 * FILTRO DE TIPO date
	 *
	 * @param string $data
	 * @return NULL
	 */
	public function getDateFilter($data=null)
	{
		$classTypeDate='';
		if(isset($this->fieldParams['tabularFilter']['tag']['filterType']))
		{
			$classTypeDate=$this->classDate[$this->fieldParams['tabularFilter']['tag']['filterType']];
			$dateFormat.=__COMMON_DATEFORMAT_YMD;
		}
		$this->htmlBoxStart='<div class=\'alert c-black col-sm-12 '.$this->classesContainerFilters.'\' role=\'alert\' style="min-width:270px!important;">
			<div class=\'p-10 bs-item '.$this->classesBgndFilters.'\'><button type=\'button\' class=\'btn bgm-red btn-xs waves-effect pull-right m-r-0 removeListFilter\' style=z-index:6; data-trigger="hover" data-toggle="popover" data-content="'.__LOCALAJAX_BTN_REMOVESEARCHFILTERTXT.'" data-placement="'.$this->tipPlacement.'" data-filter-name="sys_find_'.$this->fieldName.'"><i class="zmdi zmdi-grid-off p-l-5 p-r-5 f-14"></i></button>
			<button type=\'button\' class=\'btn bgm-lightgreen btn-xs waves-effect pull-right m-r-5 btnApplyFieldList\' style=z-index:6; data-trigger="hover" data-toggle="popover" data-content="'.__LOCALAJAX_BTN_APPLYSEARCHFILTERTXT.'" data-placement="'.$this->tipPlacement.'" data-filter-name="sys_find_'.$this->fieldName.'"><i class="zmdi zmdi-check p-l-5 p-r-5 f-700 f-14"></i></button>';
		$this->html.='<span>'.__LOCALAJAX_DATELABEL_SEARCH.': <b>'.$this->fieldParams['label'].'</b><br /></span>';
		$this->html.='<div class="fg-line">';
		$this->html.='<span class="c-bluegray pull-left" >'.__LOCALAJAX_DATELABEL_FROM.'</span><span class="row" >';
		$this->html.='<input type="text" id="sys_find_'.$this->fieldName.'" data-name="'.$this->fieldName.'" name="sys_find_'.$this->fieldName.'" value="'.$value['id_list'].'" data-date-format="'.$dateFormat.'" class="form-control input-append col-lg-1 '.$classTypeDate.'" data-trigger="hover" placeholder="'.__LOCALAJAX_DATELABEL_PLACEHOLDERFROM.'" /><button class="btn pull-right bgm-lime btn-xs waves-effect clearDataDate" data-trigger="hover" data-toggle="popover" data-content="'.__LOCALAJAX_BTN_REMOVETHISDATE.'" data-placement="'.$this->tipPlacement.'"><i class="zmdi zmdi-close c-black"></i></button></span>';
		$this->html.='<span class="c-bluegray pull-left" >'.__LOCALAJAX_DATELABEL_TO.'</span><span class="row" >';
		$this->html.='<input type="text" id="sys_find_'.$this->fieldName.'" data-name="'.$this->fieldName.'" name="sys_find_'.$this->fieldName.'" value="'.$value['id_list'].'" data-date-format="'.$dateFormat.'" class="form-control input-append col-lg-1 '.$classTypeDate.'" data-trigger="hover" placeholder="'.__LOCALAJAX_DATELABEL_PLACEHOLDERTO.'" /><button class="btn pull-right bgm-lime btn-xs waves-effect clearDataDate" data-trigger="hover" data-toggle="popover" data-content="'.__LOCALAJAX_BTN_REMOVETHISDATE.'" data-placement="'.$this->tipPlacement.'"><i class="zmdi zmdi-close c-black"></i></button></span>';
		$this->html.='</div>';
		$this->htmlBoxEnd='</div></div>';
		return;
	}


	/**
	 * FILTRO DE TIPO radio
	 *
	 * @param ARRAY $arrSqlData
	 * @return NULL
	 */
	public function getRadioFilter($arrSqlData=null)
	{
		$this->htmlBoxStart='<div class=\'alert c-black '.$this->classesContainerFilters.'\' role=\'alert\' style="min-width:270px!important;">
			<div class=\'p-10 bs-item '.$this->classesBgndFilters.'\'><button type=\'button\' class=\'btn bgm-red btn-xs waves-effect pull-right m-r-0 removeListFilter\' style=z-index:6; data-trigger="hover" data-toggle="popover" data-content="'.__LOCALAJAX_BTN_REMOVESEARCHFILTERTXT.'" data-placement="'.$this->tipPlacement.'" data-filter-name="sys_find_'.$this->fieldName.'"><i class="zmdi zmdi-grid-off p-l-5 p-r-5 f-14"></i></button>
			<button type=\'button\' class=\'btn bgm-lightgreen btn-xs waves-effect pull-right m-r-5 btnApplyFieldList\' style=z-index:6; data-trigger="hover" data-toggle="popover" data-content="'.__LOCALAJAX_BTN_APPLYSEARCHFILTERTXT.'" data-placement="'.$this->tipPlacement.'" data-filter-name="sys_find_'.$this->fieldName.'"><i class="zmdi zmdi-check p-l-5 p-r-5 f-700 f-14"></i></button>';
		$this->html.='<span>'.__LOCALAJAX_DATELABEL_SEARCH.': <b>'.$this->fieldParams['label'].'</b><br /></span>';
		$this->html.='<div class="fg-line">';

		foreach($arrSqlData as $key=>$value)
		{
			$this->html.='<label class="checkbox checkbox-inline m-r-10">';
			$this->html.='<input type="'.$this->fieldParams['tabularFilter']['tag']['filterType'].'" id="sys_find_'.$this->fieldName.'" name="sys_find_'.$this->fieldName.'[]" value="'.$value['id_list'].'" class="hide" />';
			$this->html.=''.$value['text_list'];
			$this->html.='<i class="input-helper '.@$fieldParams['cssControl'].'"></i></label>';
//echo"\n".$value['text_list'];
		}

		$this->html.='</div>';
		$this->htmlBoxEnd='</div></div>';
//echo'<pre>';print_r($arrSqlData);
		return;
	}


}