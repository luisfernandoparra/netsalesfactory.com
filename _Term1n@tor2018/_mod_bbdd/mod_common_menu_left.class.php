<?php
/**
 * MODELO PARA LOS METODOS DEL MENU IZQUIERDO COMUN A TODO EL ADMIN
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class CommonDataMenuLeft extends CBBDD
{
	protected $database;
	public $query;
	public $data = array();

	public function __construct($db_type, $db_host, $db_user, $db_pass, $db_name, $port='3306')
	{
		$this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
		$this->mainTable='system_menu_admin';
		$this->query='';
		return;
	}

	public function getDataLeft()
	{
		$this->query='SELECT menu.*, IFNULL(tmp.cantidad, 0) AS cantidad
FROM '.$this->db.'.'.__QUERY_TABLES_PREFIX.$this->mainTable.' AS menu LEFT JOIN (
SELECT tmp.id_related_option, COUNT(*) AS cantidad FROM '.$this->db.'.'.__QUERY_TABLES_PREFIX.$this->mainTable.' AS tmp
WHERE tmp.b_enabled = 1
GROUP BY tmp.id_related_option) AS tmp
ON menu.id_option_menu = tmp.id_related_option
WHERE menu.b_enabled && menu.i_accesslevel <= '.$_SESSION['userAdmin']['roleId'].'
ORDER BY menu.i_option_sort';
//echo $this->query;echo'<pre>';print_r($_SESSION);//return;
		$this->getResultSelectArray($this->query);
		$response=$this->tResultadoQuery;

		$data = array();
		$this->data = $response;
		return $this->data;
    }

}