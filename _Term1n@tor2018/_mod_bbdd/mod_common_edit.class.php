<?php
/**
 * COMMON EDITING MODEL METHODS
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class CommonEditingModel extends CBBDD
{
	public $database;
	public $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $resQuery;
	public $defaultOrderField;
	public $keyListLabel;
	public $mainTable;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;

	public function __construct($db_type=null, $db_host='', $db_user='', $db_pass='', $db_name='', $localParams=null, $port='3306')
	{
		$this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, 'utf8', $port);
		$this->prefixGtp=isset($localParams->prefixTbl) && $localParams->prefixTbl ? $localParams->prefixTbl : ($localParams->moduleBBDD ? '' : __QUERY_TABLES_PREFIX);
		$this->query='';
		$this->resQuery=[];
		$this->mainTable=isset($localParams->mainTable) ? $this->prefixGtp.$localParams->mainTable : '';
		$this->listFieldsOutput=''; $this->mainTableFieldsNames=''; $this->defaultWhereSql='';
		$this->skipBdDefault='';
		$this->defaultOrderField=''; $this->keyListLabel=null;
	return;
	}


	/**
	 * CARGA DE DATOS PARA EL REGISTRO A DIBUJAR EN EL FORMULARIO
	 * DEL MODULO ABIERTO
	 * (SE BUSCARA POR ID UNICO O SEGUN LAS CLAVES DEFINIDAS EN $foreignKeys)
	 *
	 * @param integer $idRecord
	 * @param array $foreignKeys
	 * @return array
	 */
	public function getCommonRecordData($idRecord=0, $foreignKeys=null)
	{
		$moduleDb=$this->moduleBBDD ? $this->moduleBBDD : $this->db;
    $queryFiledNames='EXPLAIN %s.%s';
    $queryFiledNames=sprintf($queryFiledNames, $moduleDb, $this->mainTable);
		$this->getResultSelectArray($queryFiledNames);
		$this->mainTableFieldsNames=$this->tResultadoQuery;
		$sqlSelect='';

    // PASO 1: OBTENCION DE LOS CAMPOS DE LA TABLA MAESTRA DEL MODULO ACTIVO
    $query='SELECT `COLUMN_NAME` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = "'.$moduleDb.'" && `TABLE_NAME` = "'.$this->mainTable.'" && `COLUMN_KEY` = "PRI";';
//echo '<hr>'.$query.' <hr><pre>';print_r($this);return;
    $this->getResultSelectArray($query);
    $primaryColumnName=$this->tResultadoQuery;
    $primaryColumnName=@$primaryColumnName[0]['COLUMN_NAME'];  // NOMBRE DEL CAMPO INDICE PRIMARIO DE LA TABLA MAESTRA

		// OBTENCION DE LOS CAMPOS A CARGAR SEGUN EL ESQUEMA DE LA MISMA TABLA
		if(isset($this->mainTableFieldsNames) && $this->mainTableFieldsNames)
		{
			foreach($this->mainTableFieldsNames as $key=>$value)
				$sqlSelect.=$value['Field'].', ';

			$sqlSelect=substr($sqlSelect,0,-2);
		}

		/**
		 * PASO 2: OBTENCION DE LOS DATOS DEL REGISTRO A EDITAR SEGUN LOS CAMPOS CONSEGUIDOS EN EL PASO 1
		 * Y DEPENDIENDO TAAMBIÉN DE LOS INDICES RECIBIDOS PARA BUSCAR EL REGIISTRO A EDITAR
		 */

		// SELECCION DE LOS CAMPOS CLAVE SI EL PARAMETRO $foreignKeys EXISTE
		if($foreignKeys)
		{
			$sqlWhere='';
			foreach($foreignKeys as $key=>$value)
				$sqlWhere.=' && '.$key.'=\''.$value.'\'';

			$query='SELECT %s FROM `%s`.`%s` WHERE 1 %s';
			$this->query=sprintf($query, $sqlSelect, $moduleDb, $this->mainTable, $sqlWhere);
		}
		else
		{
			$query='SELECT %s FROM `%s`.`%s` WHERE `%s`=%d';
			$this->query=sprintf($query, $sqlSelect, $moduleDb, $this->mainTable, $primaryColumnName, (int)$idRecord);
		}

		$this->getResultSelectArray($this->query);
    $rowData=$this->tResultadoQuery;
		@$rowData=$rowData[0];
		$this->logBackOffice($idRecord);
		return $rowData;
	}


	/**
	 * METODO COMUN DE MODIFICACION DE DATOS
	 *
	 * @param string $mainTable
	 * @param string $mainKeyFieldName
	 * @param integer $mainKeyRecordId
	 * @param aray $arrFieldsdToUpdate
	 * @return integer
   * M.F. 2016.12.06
	 */
	public function commonUpdate($mainTable='', $mainKeyFieldName='', $mainKeyRecordId=0, $arrFieldsdToUpdate=null)
	{
		if(!count($arrFieldsdToUpdate))
			return $res;

		$sqlFields='';
		foreach($arrFieldsdToUpdate as $fieldName=>$value)
		{
			$tmpValue=$value;

			if(is_array($value))
			{
				$tmpValue='';
				foreach($value as $key=>$v)
				{
					$tmpValue.=$v.',';
				}
				$tmpValue=substr($tmpValue,0,-1);
			}

			$sqlFields.=$fieldName.'=\''.str_replace('\'','`',$tmpValue).'\', ';
		}

		$sqlFields=substr($sqlFields,0,-2);

		$query='UPDATE `%s`.`%s` SET %s WHERE `%s`=%d' ;
		$this->query=sprintf($query, $this->db, $this->prefixGtp.$mainTable, $sqlFields, $mainKeyFieldName, $mainKeyRecordId);
		$res=$this->ejecuta_query($this->query);
		$this->logBackOffice($mainKeyRecordId);
		return $res;

echo "\r\n".$this->query."\r\n";
echo "\r\n".$mainTable."\r\n";
echo $mainKeyFieldName."\r\n";
echo $mainKeyRecordId."\r\n";
echo "\r\n"."\r\n".'vars--->';print_r($_REQUEST);
echo "\r\n"."\r\n".'$arrFieldsdToUpdate --->';print_r($arrFieldsdToUpdate);
echo '[--->files: ';print_r($_FILES);echo'</pre>';die(0);
print_r($arrFieldsdToUpdate);return;
	}


	/**
	 *	ACTUALIZA TABLAS RELACIONADAS INDEPENDIENTES DE LA TABLA PRINCIPAL DEL MODULO ABIERTO
	 *
	 * @param string $relatedTable (TABLA RELACIONADA)
	 * @param string $mainKeyFieldName (NOMBRE DEL CAMPO CLAVE DE LA RELACION)
	 * @param string $relatedFieldName (NOMBRE DEL CAMPO RELACIONADO AL CAMPO CLAVE)
	 * @param string $mainKeyFieldId (VALOR DEL CAMPO CLAVE)
	 * @param array $arrNewValues (ARRAY CON VALORES RELACIONADOS AL CAMPO CLAVE DE LA MISMA)
	 * @return boolean
	 */
	public function updateRelatedTable($relatedTable='', $mainKeyFieldName='', $relatedFieldName='', $mainKeyFieldId=0, $arrNewValues=null)
	{
		if(!isset($relatedTable) || !isset($mainKeyFieldName) || !isset($relatedFieldName))
			return false;

    $inSqlValues='(';
		$refQuery=0;
		$this->query=[]; $this->resQuery=[];

		foreach($arrNewValues as $value)
		{
			$query='SELECT COUNT(%s) AS cant FROM `%s`.`%s` WHERE `%s` = %s AND `%s` = %s';
			$this->query[$refQuery]=sprintf($query, $mainKeyFieldName, $this->db, $relatedTable, $mainKeyFieldName, $mainKeyFieldId, $relatedFieldName, $value);
			$this->resQuery[$refQuery]=$this->getResultSelectArray($this->query[$refQuery]);
			$cant=$this->tResultadoQuery;

			if((int)$cant[0]['cant'] == 0)
			{
				$refQuery++;
				$queryTmp='INSERT INTO `%s`.`%s` (%s, %s) VALUES (%d,%d)';
				$this->query[$refQuery]=sprintf($queryTmp, $this->db, $relatedTable, $mainKeyFieldName, $relatedFieldName, $mainKeyFieldId, $value);
				$res=$this->ejecuta_query($this->query[$refQuery]);
				$this->resQuery[$refQuery]=$res;
			}

			$inSqlValues.=(int)$value.', ';
    }

		$inSqlValues=substr($inSqlValues, 0, -2);
		$inSqlValues.= $inSqlValues ? ')' : '("")';
		$refQuery++;
		$query='DELETE FROM `%s`.`%s` WHERE `%s`= %d AND `%s` NOT IN %s' ;
		$this->query[$refQuery]=sprintf($query, $this->db, $relatedTable, $mainKeyFieldName, $mainKeyFieldId, $relatedFieldName, $inSqlValues);
		$res=$this->ejecuta_query($this->query[$refQuery]);
		$this->resQuery[$refQuery]=$res;
		return $res;
	}

	/*
	 * FRANK BRICEÑO 05/06/2017
	 * ACTUALIZA EL ORDEN EN QUE SE HA REORDENADO LOS CAMPOS QUE APARECEN DE UNA TABLA RELACIONAL
	 */
    public function updateRelatedOrder($relatedTable='', $mainKeyFieldName='', $reorderFieldName='', $relatedFieldName=0, $arrNewValues=null)
    {
			if(!isset($relatedTable) || !isset($relatedFieldName) || !isset($reorderFieldName))
				return false;

			$inSqlValues='(';
			$refQuery=0;
			$this->query=[];
			$this->resQuery=[];
			$arrNewValues=explode(',',$arrNewValues);

			foreach($arrNewValues as $update)
			{
				$qvalues=explode('-',$update);

				if($qvalues[0] != '' && $qvalues[1] != '')
				{
					$query = "UPDATE %s.%s SET %s = %s where %s = %s";
					$this->query[$refQuery] = sprintf($query,$this->db, $relatedTable, $reorderFieldName, $qvalues[1], $relatedFieldName, $qvalues[0]);;
					$res = $this->ejecuta_query($this->query[$refQuery]);
					$this->resQuery[1] = $res;
				}
			}
			//die();
			return $res;
    }

	/**
	 * OBTENCION DE DATOS PARA TABLAS RELACIONADAS
	 *
	 * @param integer $mainId
	 * @param string $localTable
	 * @param object $relatedData (ELEMENTOS: relatedTableName / keyFieldName / relatedFieldName / arrFieldsTagSelect / arrRelatedFields / whereSelectField / orderSelectField)
	 * @return type
	 *
	 * M.F. 2017.02.07
	 */
	public function getRelatedDataField($mainId=0, $localTable='', $relatedData=null)
	{
		$sqlOrder=isset($relatedData->orderSelectField) ? ' ORDER BY main.'.$relatedData->orderSelectField : '';
		$fieldsNames='main.'.$relatedData->arrFieldsTagSelect[0].' AS id_list, main.'.$relatedData->arrFieldsTagSelect[1].' AS text_list, IF(related.'.$relatedData->arrFieldsTagSelect[0].' is null, 0, 1) AS selectedField';
		$sqlWhere='';
		$sqlWhere.=$relatedData->whereSelectField ? $relatedData->whereSelectField : '';
		$sqlOn=($relatedData->keyFieldName && $relatedData->keyFieldName) ? 'main.'.$relatedData->arrFieldsTagSelect[0].'=related.'.$relatedData->arrRelatedFields[0].' && related.'.$relatedData->keyFieldName.'='.(int)$mainId : '1';
		$query='SELECT '.$fieldsNames.' FROM `%s`.`%s` AS main LEFT JOIN `%s`.`%s` AS related ON %s WHERE 1 '.$sqlWhere.$sqlOrder;
		$this->query=sprintf($query, $this->db, $localTable, $this->db, $relatedData->relatedTableName, $sqlOn);
		$this->getResultSelectArray($this->query);
    $res=$this->tResultadoQuery;
		return $res;
	}


	/**
	 * DEVUELVE EL ELEMENTO O LA LISTA DE ELEMENTOS ENCONTRADOS PARA LA TABLA ESPECIFICADA (SI OMITIDA = mainTable)
	 *
	 * @param int $idRecord
	 * @param string $localTable
	 * @param array $arrFieldsTagSelect	DEBE CONTENER LOS NOMBRES DE LOS CAMPOS PARA OBTENER LOS DATOS, EL PRIMERO SIEMPRE EL ID DE LA TABLA
	 * @param string $sqlWhere
	 * @param boolean $fieldOrder
	 * @param boolean $onlyOneFieldValue
	 * @return array
	 *
	 * M.F. 2017.02.08
	 */
	public function getSpecificRecordData($idRecord=0 ,$localTable=null, $arrFieldsTagSelect=null, $sqlWhere=null, $fieldOrder=false, $onlyOneFieldValue=false)
	{
		$sqlOrder=isset($fieldOrder) ? ' ORDER BY '.$fieldOrder : '';
		$sqlOnlyOneRecord=(isset($idRecord) && $idRecord && isset($onlyOneFieldValue) && $onlyOneFieldValue) ? ' && '.$arrFieldsTagSelect[0].'=\''.$idRecord.'\'' : null;
		$sqlWhere=$sqlOnlyOneRecord.(isset($sqlWhere) ? ' && '.$sqlWhere : '');
		$fieldsNames=$onlyOneFieldValue ? $onlyOneFieldValue : null;

		if(count($arrFieldsTagSelect) && !$onlyOneFieldValue)
		{
			$fieldsNames='';
			$arrAliasNames=array('id_list', 'text_list');
			foreach($arrFieldsTagSelect as $key=>$value)
				$fieldsNames.=$value.' AS '.$arrAliasNames[$key].', ';

			$fieldsNames=substr($fieldsNames,0,-2);
		}

		$localTable=isset($localTable) ? $localTable : $this->mainTable;
		$query='SELECT '.$fieldsNames.' FROM `%s`.`%s` WHERE 1 '.$sqlWhere.$sqlOrder;
		$this->query=sprintf($query, $this->db, $localTable);
		$this->getResultSelectArray($this->query);
    $res=$this->tResultadoQuery;
		return $res;
	}


	/**
	 * METODO QUE RECUPERA EL VALOR DEL CAMPO $fieldName DE LA TABLA $mainTable
	 *
	 * @param string $mainTable
	 * @param string $mainKeyFieldName
	 * @param integer $mainKeyRecordId
	 * @param array $arrFieldsdToUpdate
	 * @param string $fieldName
	 * @return string
	 */
	public function getCurrentFieldValue($mainTable='', $mainKeyFieldName='', $mainKeyRecordId=0, $arrFieldsdToUpdate=null, $fieldName='')
	{
		if(!trim($fieldName))
			return $res;

    $this->query='SELECT %s FROM %s%s WHERE %s=\'%s\'';
    $this->query=sprintf($this->query, $fieldName, ($this->skipBdDefault ? '' : '`'.$this->db.'`.'), ($this->skipBdDefault ? '' : '`'.$this->prefixGtp).$mainTable.($this->skipBdDefault ? '' : '`'), $mainKeyFieldName, $mainKeyRecordId);
    $this->getResultSelectArray($this->query);
    $resSql=$this->tResultadoQuery;
    $resQuery=$resSql[0][$fieldName];
		return $resQuery;
	}


	/**
	 * OBTENER EL REGISTRO COMPLETO DEL ELEMENTO A COPIAR
	 * 
	 * @param string $mainTable
	 * @param integer $mainKeyRecordId
	 * @param string $foreignKeys
	 * @return array
	 */
	public function getSingleRowData($mainTable='', $mainKeyRecordId=0, $foreignKeys=null)
	{
		if(!trim($mainTable))
			return false;

		$moduleDb=$this->moduleBBDD ? $this->moduleBBDD : $this->db;
    $queryFiledNames='EXPLAIN %s.%s';
    $queryFiledNames=sprintf($queryFiledNames, $moduleDb, $mainTable);
		$this->getResultSelectArray($queryFiledNames);
		$this->mainTableFieldsNames=$this->tResultadoQuery;
		$sqlSelect='';

		$moduleDb=$this->moduleBBDD ? $this->moduleBBDD : $this->db;
    $query='SELECT `COLUMN_NAME` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = "'.$moduleDb.'" && `TABLE_NAME` = "'.$mainTable.'" && `COLUMN_KEY` = "PRI";';
    $this->getResultSelectArray($query);
    $primaryColumnName=$this->tResultadoQuery;
    $primaryColumnName=@$primaryColumnName[0]['COLUMN_NAME'];  // NOMBRE DEL CAMPO INDICE PRIMARIO DE LA TABLA MAESTRA

		// OBTENCION DE LOS CAMPOS A CARGAR SEGUN EL ESQUEMA DE LA MISMA TABLA
		if(isset($this->mainTableFieldsNames) && $this->mainTableFieldsNames)
		{
			foreach($this->mainTableFieldsNames as $key=>$value)
				$sqlSelect.=($primaryColumnName == $value['Field']) ? '' : $value['Field'].', ';	// SKIP ONLY PRIMARY KEY FIELD

			$sqlSelect=substr($sqlSelect,0,-2);
		}

		if($foreignKeys)
		{
			$sqlWhere='';
			foreach($foreignKeys as $key=>$value)
				$sqlWhere.=' && '.$key.'=\''.$value.'\'';

			$query='SELECT %s FROM `%s`.`%s` WHERE 1 %s';
			$this->query=sprintf($query, $sqlSelect, $moduleDb, $mainTable, $sqlWhere);
		}
		else
		{
			$query='SELECT %s FROM `%s`.`%s` WHERE `%s`=%d';
			$this->query=sprintf($query, $sqlSelect, $moduleDb, $mainTable, $primaryColumnName, (int)$mainKeyRecordId);
		}

		$this->getResultSelectArray($this->query);
    $rowData=$this->tResultadoQuery;
		@$rowData=$rowData[0];
		$this->logBackOffice($mainKeyRecordId);
		return $rowData;
	}

	/**
	 * DUPLICACION DE REGISTRO DEL MODULO ACTIVO
	 *
	 * @param string $paramNameTable
	 * @param array $rowData
	 * @param byte $paramStatusRow
	 * @param array $arrCopyExcludeFields
	 * @return boolean
	 */
	public function copyRowRecord($paramNameTable='', $rowData=array(), $paramStatusRow=null, $arrCopyExcludeFields=array())
	{
		if(!trim($paramNameTable))
			return false;

		$strFieldNames=''; $strValues='';

		foreach($rowData as $field=>$value)
		{
			$tmpValue=str_replace('\'', '"', $value);

			if(in_array($field, $arrCopyExcludeFields))	// CAMPOS CON EL VALOR A EXCLUIR EN LA COPIA
				$tmpValue=0;

			if($paramStatusRow === $field)	// FORZAR EL NUEVO RECORD DESHABILITADO
				$tmpValue=0;

			$strFieldNames.=$field.',';
			$strValues.='\''.$tmpValue.'\',';
		}

		$strFieldNames=substr($strFieldNames,0,-1);
		$strValues=substr($strValues,0,-1);
		$moduleDb=$this->moduleBBDD ? $this->moduleBBDD : $this->db;
		$query='INSERT INTO %s.%s (%s) VALUES (%s)';
		$this->query=sprintf($query, $moduleDb, $paramNameTable, $strFieldNames, $strValues);
		$res=$this->ejecuta_query($this->query);
		$this->logBackOffice($this->Id);
		return $res;
	}
}
