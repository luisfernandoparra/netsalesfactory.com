<?php
/**
 *	CLASE CON LAS FUNCIONES DE BBDD NECESARIAS PARA CREAR LAS LISTAS DE LA BBSDD STANDARD DE GTP6
 * 
 * Modificado 2017.05.22 (M.F.)
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();

class CommonAdminLists extends CBBDD
{
	protected $database;
	private $prefixGtp;	// PREFIJO DE LAS TABLAS NATIVAS DE GTP6
	public $query;
	public $defaultOrderField;
	public $sortList;
	public $keyListLabel;
	public $mainTableFieldsNames;
	public $listFieldsOutput;
	public $defaultWhereSql;
	public $skipBdDefault;

	public function __construct($db_type, $db_host, $db_user, $db_pass, $db_name, $localParams=null, $port='1312')
	{
		$this->database=$this->CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, null, null,$port);
		$this->prefixGtp=isset($localParams->prefixTbl) && $localParams->prefixTbl ? $localParams->prefixTbl : ($localParams->moduleBBDD ? '' : __QUERY_TABLES_PREFIX);
//echo $bbdd_community_logs."]\n localParams ==== [".$this->prefixGtp.']';print_r($_REQUEST['mainTable']);print_r($localParams);
		$this->query='';
		$this->skipBdDefault='';
		$this->listFieldsOutput=''; $this->mainTableFieldsNames=''; $this->defaultWhereSql='';
		$this->defaultOrderField=''; $this->sortList=''; $this->keyListLabel=null;
		return;
	}

	/**
	 * 1. DEVUELVE LAS CABECERAS DEL LISTADO
	 * 2. DEVUELVE LOS REGISTROS A LISTAR
	 *
	 * @param varchar $mainTable
	 * @param byte $skipBdDefault
	 * @return object (this->mainTableFieldsNames), object ($resSql)
	 */
	public function listDataAndHeaders($mainTable='', $skipBdDefault=false)
	{
		$queryFiledNames='DESCRIBE %s';
		$queryFiledNames=sprintf($queryFiledNames, $this->db.'.'.$this->prefixGtp.$mainTable);
		$this->getResultSelectArray($queryFiledNames);
		$this->mainTableFieldsNames=$this->tResultadoQuery;
		return;
	}


	public function commonDeleteRecord($mainTable='', $mainFieldKey='', $mainKey=0, $mainOptions='')
	{
		$res=false; $sqlWhere='';

		if(!$mainTable || !$mainKey || !$mainFieldKey)
			return false;

    // SE BUSCAN SENTECIAS SQL ESPECIFICAS DEL ARCHIVO DE CONFIGURACION DEL MODULO UTIULIZADO EN ESTE PROCESO
		foreach($mainOptions as $key=>$option)
		{
			if($key == 'sqlWhere' && count($option))
			{
				foreach($option as $sqlCommand=>$sqlValue)
					$sqlWhere.=' '.$sqlCommand.$sqlValue;
			}
		}

		$query='DELETE FROM `%s`.`%s` WHERE %s = \'%s\' '.$sqlWhere;
		$this->query=sprintf($query, $this->db, $this->prefixGtp.$mainTable, $mainFieldKey, $mainKey);
//echo $this->query;print_r($_REQUEST);return;
		$res=$this->ejecuta_query($this->query);
		$this->logBackOffice($mainKey,null,(int)$this->mNumRegs);
		return $res;
	}

	public function commonInsert($mainTable='', $mainKeyFieldName='', $mainKeyRecordId=0, $arrFieldsdToUpdate=null)
	{
    $statusFieldName=isset($arrFieldsdToUpdate['b_enabled']) ? $arrFieldsdToUpdate['b_enabled'] : 0;

		if(!count($arrFieldsdToUpdate))
			return $res;

    // OBTENER EL NOMBRE DEL CAMPO id PRIMARIO DE LA TABLA A GESTIONAR
    $query='SELECT `COLUMN_NAME` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = "'.$this->db.'" && `TABLE_NAME` = "'.$this->prefixGtp.$mainTable.'"
&& `COLUMN_KEY` = "PRI";';
    $this->getResultSelectArray($query);
    $primaryColumnName=$this->tResultadoQuery;
    $primaryColumnName=$primaryColumnName[0]['COLUMN_NAME'];

    if(!(int)$statusFieldName)
      $statusFieldName='b_enabled';

    $query='SELECT `COLUMN_NAME`, `DATA_TYPE` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = "'.$this->db.'" && `TABLE_NAME` = "'.$this->prefixGtp.$mainTable.'" && `COLUMN_KEY` != "PRI";';
    $this->getResultSelectArray($query);
    $arraTableFields=$this->tResultadoQuery;
		$fieldNames=''; $fieldValues='';

		foreach($arraTableFields as $key=>$value)
		{
			if(!isset($value['COLUMN_NAME']))
				continue;

			if(@is_array($arrFieldsdToUpdate[$value['COLUMN_NAME']]))
			{
				$tmpValues='\'';
				foreach($arrFieldsdToUpdate[$value['COLUMN_NAME']] as $kk=>$vv)
					$tmpValues.=(($value['DATA_TYPE'] == 'int' || $value['DATA_TYPE'] == 'tinyint') ? (int)$vv : $vv).',';

				$tmpValues=substr($tmpValues,0,-1);
				$fieldValues.=$tmpValues.'\', ';
			}
			else
			{
        // SE EVITAN ERRORES AL INTENTAR LEER COLUMNAS INEXISTENTES O NO INICIALIZADAS
				if(!isset($arrFieldsdToUpdate[$value['COLUMN_NAME']]))
					continue;

				$fieldValues.='\''.(($value['DATA_TYPE'] == 'int' || $value['DATA_TYPE'] == 'tinyint') ? (int)$arrFieldsdToUpdate[$value['COLUMN_NAME']] : $arrFieldsdToUpdate[$value['COLUMN_NAME']]).'\', ';
			}

			$fieldNames.=$value['COLUMN_NAME'].', ';

		}

		$fieldNames=substr($fieldNames,0,-2);
		$fieldValues=substr($fieldValues,0,-2);
		$query='INSERT INTO `%s`.`%s` (%s) values (%s)' ;
		$this->query=sprintf($query, $this->db, $this->prefixGtp.$mainTable, $fieldNames, $fieldValues);
		$res=$this->ejecuta_query($this->query);
		$this->logBackOffice($this->Id);
		return $res;

echo "\r\n".$this->query."\r\n";
echo "\r\n".$mainTable."\r\n";
echo $mainKeyFieldName."\r\n";
echo $this->Id."\r\n";
echo "\r\n"."\r\n".'arrFieldsdToUpdate : ';print_r($arrFieldsdToUpdate);
echo "\r\n"."\r\n".'$arraTableFields: ';print_r($arraTableFields);
echo '[--->files: ';print_r($_FILES);echo'</pre>';die(0);
print_r($arrFieldsdToUpdate);return;
	}


	/**
	 * METODO QUE RECUPERA EL VALOR DEL CAMPO $fieldName DE LA TABLA $mainTable
	 *
	 * @param string $mainTable
	 * @param string $mainKeyFieldName
	 * @param integer $mainKeyRecordId
	 * @param array $arrFieldsdToUpdate
	 * @param string $fieldName
	 * @return string
	 */
	public function getCurrentFieldValue($mainTable='', $mainKeyFieldName='', $mainKeyRecordId=0, $arrFieldsdToUpdate=null, $fieldName='')
	{
		if(!trim($fieldName))
			return $res;

    $this->query='SELECT %s FROM %s%s WHERE %s=\'%s\'';
    $this->query=sprintf($this->query, $fieldName, ($this->skipBdDefault ? '' : '`'.$this->db.'`.'), ($this->skipBdDefault ? '' : '`'.$this->prefixGtp).$mainTable.($this->skipBdDefault ? '' : '`'), $mainKeyFieldName, $mainKeyRecordId);
    $this->getResultSelectArray($this->query);
    $resSql=$this->tResultadoQuery;
    $resQuery=$resSql[0][$fieldName];
		return $resQuery;
	}


	public function updateStatusRow($mainTable='', $id=0, $skipBdDefault=false, $statusFieldName='b_enabled', $newStatusValue=0)
	{
    // OBTENER EL NOMBRE DEL CAMPO id PRIMARIO DE LA TABLA A GESTIONAR
    $query='SELECT `COLUMN_NAME` FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA` = "'.$this->db.'" && `TABLE_NAME` = "'.$this->prefixGtp.$mainTable.'"
&& `COLUMN_KEY` = "PRI";';
    $this->getResultSelectArray($query);
    $primaryColumnName=$this->tResultadoQuery;
    $primaryColumnName=$primaryColumnName[0]['COLUMN_NAME'];

    if(!trim($statusFieldName))
      $statusFieldName='b_enabled';

		$this->query='UPDATE %s%s SET %s = %d WHERE `%s` = %d ';
		$this->query=sprintf($this->query, ($skipBdDefault ? '' : '`'.$this->db.'`.'), ($skipBdDefault ? '' : '`'.$this->prefixGtp).$mainTable.($skipBdDefault ? '' : '`'), $statusFieldName, (int)$newStatusValue, $primaryColumnName, (int)$id);
		$res=$this->ejecuta_query($this->query);
		$this->logBackOffice($id);
		return $res;
	}


  /**
   * GET DATA ROWS ON LISTING EVENTS
	 * @version 1.2
	 * @author M.F.
   *
   * @return array
   */
	public function getRowsDataList()
	{
		$moduleDb=$this->moduleBBDD ? $this->moduleBBDD : $this->db;
		$sqlBaseStructure=($this->mainTable == 'INFORMATION_SCHEMA.TABLES') ? 'WHERE 1 '.$this->defaultWhereSql :'';
		$this->totalNumRows=0; $sqlOrderBy='';
    $sqlFindString='';
    $searchString=trim(@$this->searchValue);
		$this->logBackOffice();


    if($searchString && !$this->skipBdDefault)	// PARA LAS TABLAS STANDARD, SE OBTIENEN LOS NOMBRES DE LOS CAMPOS DONDE BUSCAR
    {
      $queryFiledNames='DESCRIBE %s';
      $queryFiledNames=sprintf($queryFiledNames, $moduleDb.'.'.$this->prefixGtp.$this->mainTable);
      $this->getResultSelectArray($queryFiledNames);
      $this->mainTableFieldsNames=$this->tResultadoQuery;

      if(count($this->mainTableFieldsNames))
      {
        $sqlFindString=' && (';
        foreach($this->mainTableFieldsNames as $key=>$value)
        {
          if(!(stristr(@$value['Type'],'varchar') || stristr(@$value['Type'],'text')) && !$this->includeNumbers)
            continue;

          $sqlFindString.='`'.$value['Field'].'` LIKE \'%%'.$searchString.'%%\' ||';
        }

        $sqlFindString=substr($sqlFindString,0,-3).')';
      }
    }

    if(isset($this->additionalFilters) && count($this->additionalFilters))
    {
      foreach($this->additionalFilters as $key=>$value)
      {
				if(isset($value['settingsField']))
				{
					$arrSettings=json_decode($value['settingsField']);

					if($arrSettings->tag->filterType == 'date')	// PARA FILTROS DE SOLO TIPO FECHA
					{
						$sqlSign='=';

						if($this->additionalFilters[$key]['from'])
							$sqlFindString.=' && DATE(`'.$key.'`) '.($this->additionalFilters[$key]['to'] ? '>=' : $sqlSign).' \''.$this->additionalFilters[$key]['from'].'\'';

						if($this->additionalFilters[$key]['to'])
							$sqlFindString.=' && DATE(`'.$key.'`) '.($this->additionalFilters[$key]['from'] ? '<=' : $sqlSign).' \''.$this->additionalFilters[$key]['to'].'\'';

						continue;
					}
					else
						$sqlFindString.=' && `'.$key.'`=\'';
				}
				else
				{
	        $sqlFindString.=' && `'.$key.'`=\'';
				}

        if(is_array($value))
        {
         if(count($value) == 1)
				 {
           $sqlFindString.=$value['valueToFind'];
				 }
         else
          foreach($value as $kk=>$vv)
          {
					 if($kk != 'valueToFind')
						 continue;

            $sqlFindString.=$vv;
          }
        }

        $sqlFindString.='\'';
      }
    }

    $this->query='SELECT COUNT(*) AS numRows FROM %s%s '.$sqlBaseStructure.(!$this->skipBdDefault ? ' WHERE 1'.$sqlFindString : '');  // NUM TOTAL DE FILAS DE LA TABLA "mainTable" / BBDD SELECCIONADA
    $this->query=sprintf($this->query, ($this->skipBdDefault ? '' : '`'.$moduleDb.'`.'), ($this->skipBdDefault ? '' : '`'.$this->prefixGtp).$this->mainTable.($this->skipBdDefault ? '' : '`'));
//die("\n".$this->query);
    $this->getResultSelectArray($this->query);
    $resSql=$this->tResultadoQuery;
    $this->totalNumRows=@$resSql[0]['numRows'];
 		$sqlLimit=' LIMIT '.(@$this->current ? (($this->current * $this->rowCount) - $this->rowCount) .', '. ($this->rowCount ? $this->rowCount : 10) : '10');
		$sqlSelectFields=$this->listFieldsOutput ? $this->listFieldsOutput : '*';
		$sqlSelectFields=$sqlSelectFields ? ($this->keyListLabel ? $this->keyListLabel.' AS keyListModule, '.$sqlSelectFields : $sqlSelectFields ) : '';

		if(isset($this->sortList) && count($this->sortList) && is_array($this->sortList))
		{
			$sqlOrderBy=' ORDER BY ';

			foreach($this->sortList as $fieldOrder=>$modeOrder)
				$sqlOrderBy.=$fieldOrder.' '.$modeOrder.',';

			$sqlOrderBy=substr($sqlOrderBy,0,-1);
		}

		$sqlWhere=$this->defaultWhereSql ? ' '.$this->defaultWhereSql.' ' : '';

		$this->query='SELECT '.$sqlSelectFields.' FROM %s%s WHERE 1 '.$sqlWhere.$sqlFindString.$sqlOrderBy.$sqlLimit;
		$this->query=sprintf($this->query, ($this->skipBdDefault ? '' : '`'.$moduleDb.'`.'), ($this->skipBdDefault ? '' : '`'.$this->prefixGtp).$this->mainTable.($this->skipBdDefault ? '' : '`'));
		$this->getResultSelectArray($this->query, $this->keyListLabel);
		$resSql=$this->tResultadoQuery;
    $arrData=array();
//print_r($resSql);die("\n".$this->query);


		/*
		 * SE RECORREN TODAS LAS PROPIEDADES DE CADA CAMPO A MOSTRAR
		 * PARA BUSCAR EVENTUALES TABLAS RELACIONADAS Y PODER ASI
		 * VISUALIZAR LOS CAMPOS DESEADOS
		 */
    foreach($resSql as $key=>$arrValues)
    {
//echo"\n".$arrValues;
      $count=0;
      foreach($arrValues as $k=>$v)
      {
				foreach($this->moduleFieldsStructure as $tmpField=>$x)
				{
					$tmpTableName='';

					if($tmpField == $k && (isset($this->moduleFieldsStructure[$k]['orderSelectField']) && $this->moduleFieldsStructure[$k]['orderSelectField']))
					{
						if(!@$this->moduleFieldsStructure[$k]['fieldTableName'])	// SI NO EXISTE UNA TABLA DONDE EXTRAER LOS DATOS, SE UTILIZA LA TABLA MAESTRA DEL MODULO ACTUAL
							$tmpTableName=$this->mainTable;
						else
							$tmpTableName=@$this->moduleFieldsStructure[$k]['fieldTableName'];
					}

					// SI EXISTE UNA TABLA RELACIONADA, SE PROCEDE A EXTRAR EL DATO RELACIONADO A DIBUJAR EN EL LISTADO
					if($tmpField == $k && ($tmpTableName != ''))
					{
						$queryCheckTable='SELECT table_name FROM information_schema.tables WHERE table_schema = \''.(isset($this->moduleFieldsStructure[$k]['includeMainBBDD']) && $this->moduleFieldsStructure[$k]['includeMainBBDD'] ? $this->db : $moduleDb).'\' AND table_name = \''.$tmpTableName.'\';';
						$this->getResultSelectArray($queryCheckTable);

						if(!$this->mNumRegs)	// INTENTA IDENTIFICAR LA TABLA COMO NOMBRE STANDARD DE TABLA
						{
							$queryCheckTable='SELECT table_name FROM information_schema.tables WHERE table_schema = \''.(isset($this->moduleFieldsStructure[$k]['includeMainBBDD']) && $this->moduleFieldsStructure[$k]['includeMainBBDD'] ? $this->db : $moduleDb).'\' AND table_name = \''.__QUERY_TABLES_PREFIX.$tmpTableName.'\';';
							$this->getResultSelectArray($queryCheckTable);
							$tmpTableName=$this->mNumRegs ? __QUERY_TABLES_PREFIX.$tmpTableName : $tmpTableName;
						}

						$queryGetFieldData='SELECT '.$this->moduleFieldsStructure[$k]['arrFieldsTagSelect'][1].' AS tmpFieldName FROM `'.(isset($this->moduleFieldsStructure[$k]['includeMainBBDD']) && $this->moduleFieldsStructure[$k]['includeMainBBDD'] ? $this->db : $moduleDb).'`.`'.$tmpTableName.'` WHERE '.$this->moduleFieldsStructure[$k]['arrFieldsTagSelect'][0].'='.(int)$v;
						$this->getResultSelectArray($queryGetFieldData);
						$tmpDataField=$this->tResultadoQuery;
						$tmpDataField=isset($tmpDataField[0]['tmpFieldName']) ? $tmpDataField[0]['tmpFieldName'] : '';

            // SI NO SE OBTIENE UN VALOR, SE INTENTA ASIGNAR "defaultSelectText"
						if(!$tmpDataField && isset($this->moduleFieldsStructure[$k]['defaultSelectText']))
							$tmpDataField=$this->moduleFieldsStructure[$k]['defaultSelectText'];

						$arrValues[$tmpField]=$tmpDataField;
					}
				}
        $arrData[$key]=$arrValues;
        $count++;
      }
    }

    $this->numRowsQuery=(int)count($arrData);
//		$this->logBackOffice();
		return $arrData;
	}

}