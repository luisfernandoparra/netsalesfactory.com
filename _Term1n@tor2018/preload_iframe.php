<?php
/**
 * PRELOAD BRIDGE IFRAME
 * LIST RECORD DATA (PART 2/3)
 *
 * @date 2016.08.08
 */
session_start();

if(count($_GET))
  $arraExtVars=$_GET;
else
  $arraExtVars=$_POST;

?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>preload page</title>

</head>
<body>
<div class="container">
    <center class="preloadData">
      <div style="display:block;margin-top:8%;font-family:arial,verdana;font-size:1em;font-style:italic;letter-spacing:1em;">CARGANDO...</div>
    </center>

</div>
</body>
</html>
