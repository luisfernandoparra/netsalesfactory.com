<?php
@session_start();
// DEFINE COOKIES TEXT VARS 
$cookieCompanyNameFull='FINTEL MARKETING SLU';
$cookieCompanyNameShort='LA AGENCIA';
/*
 * LISTA DE LAS COOKIES UTILIZADAS EN ESTA PAGINA
 */
$arrThirdCookies=array('_ga'=>'Es necesaria para el funcionamiento de Google Analytics y tiene un período de caducidad de 2 años');

?>

<style>
.blockCookiesContainer{
border:0 none;
font:inherit;
margin:0;
vertical-align:baseline;
padding:2%;
line-height:1.6em;
color:#333!important;
font-size:90%!important;
}
.cookieName{
display:inline;
font-weight:normal;
}
.cookiesHeader{
	border-bottom:1px solid #222;
	color:#222;
	font-weight:bold;
	margin-bottom:3px;
	padding-top:10px;
}
.cookiesHeader span{
	font-size:95%;
	font-weight:normal;
}

#cookieList li{
  padding:5px 0;
}
@media (max-width:860px){
	.blockCookiesContainer{padding:2%;}
}
@media (max-width:700px){
	.blockCookiesContainer{padding:4%;}
}
@media (max-width:480px){
	.blockCookiesContainer{padding:8%;font-size:95%!important;}
}
</style>

<div class="blockCookiesContainer">
	<div class="cookiesHeader">
		Cookies esenciales
		<span>
			- Estas cookies son necesarias para el correcto funcionamiento de la página web.
		</span>
	</div>
	<ul class="ulCookies">
		<li>
			<div class="cookieName">
				Cookies de preferencias
			</div>
			<div class="cookieDescription">
				- Esta cookie es necesaria para almacenar los permisos que otorga a nuestras cookies.
			</div>
		</li>
		<li>
			<div class="cookieName">
				PHP Session
			</div>
			<div class="cookieDescription">
				- Esta cookie se utiliza para el correcto funcionamiento del motor lógico de la página web.
			</div>
		</li>
	</ul>
	<div class="cookiesHeader">
		Cookies no esenciales
		<span>
			- Estas cookies están reconocidas por nuestro servidor pero puede usted bloquearlas si lo desea.
		</span>
	</div>
	<ul class="knownCookies">
		<li>
			<div class="cookieName">
				Piwik
			</div>
			<div class="cookieDescription">
				- Estas cookies se utilizan para recopilar información acerca de cómo los visitantes compran e interactuan en nuestra página web. Utilizamos la información para elaborar informes y para ayudarnos a mejorar el sitio.
			</div>
		</li>
		<li>
			<div class="cookieName">
				Google Analytics
			</div>
			<div class="cookieDescription">
				- Estas cookies se utilizan para recopilar información acerca del número de visitantes que tiene nuestra página web. Utilizamos la información para elaborar informes y para ayudarnos a mejorar el sitio. Las cookies recopilan información de forma anónima, incluyendo el número de visitantes, que secciones han visitado y como nos han encontrado.
			</div>
		</li>
		<li>
			<div class="cookieName">
				AddThis
			</div>
			<div class="cookieDescription">
				- Esta cookie es creada y leída por el sitio de intercambio social de AddThis con el fin de asegurarse de que el usuario ve el recuento actualizado si comparte una página. No se envía ningún dato de la cookie a la empresa AddThis. Más información en <a href="http://www.addthis.com/">http://www.addthis.com/</a>
			</div>
		</li>
	</ul>
	<div class="cookiesHeader">
		Cookies desconocidas
		<span>
			- Nuestro servidor no conoce el origen de estas cookies.
		</span>
	</div>
	<ul class="unknownCookies">
		<li>
			<div class="cookieName">
<?php
/*
 * 	PAINT THE COOKIES LIST FROM "$arrThirdCookies"
 */
foreach($arrThirdCookies as $cookieName=>$cokkieDesc)
{
  echo '<p>"'.$cookieName.'": '.$cokkieDesc.'.</p>';
}
?>
			</div>
		</li>
	</ul>
<br />
</div>
