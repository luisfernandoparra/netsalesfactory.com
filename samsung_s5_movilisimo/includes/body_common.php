<?php
$idLandingFormatted=str_pad($objSiteData->landingId,3,'0',STR_PAD_LEFT);
?>
<style>
.appbutton {
background-color:#009fe4;
background-image:url("img/f-boton-color.png");
background-position:-10px 0;
background-repeat:no-repeat;
border:medium none #00335b;
border-radius:3px;
box-shadow:0 1px 2px #424242;
color:white !important;
cursor:pointer;
display:inline-block;
font:14px Arial,Helvetica,sans-serif;
margin:5px 5px 0 0;
padding:8px 13px;
text-align:center;
text-decoration:none !important;
text-shadow:0 1px #00335b;
width:auto;
}
.innerModal{
box-sizing:border-box;
margin:0;
padding:0;
padding-top:6%;
position:relative;
line-height:22px!important;
font:12px Arial,Helvetica,sans-serif;
}

.blockCall{
color:#fff;
text-decoration:none;
border:none;
}

/* START IMAGE LEFT EFFECT */
.container3d{
	width:100%;
	height:100%;
	position:relative;
	font-family:Helvetica, Arial, sans-serif;
	-webkit-perspective:500px;
	-moz-perspective:500px;
	-o-perspective:500px;
	perspective:500px;
}

.flipper {
    width:100%;
    height:100%;
    top:0;
    left:0;
}

.container3d .image{
    position:relative;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background:#dadada;
}

.description {
    position:absolute;
    bottom:0;
    left:0;
    width:200px;
    color:white;
    background:rgba(0,0,0,.5);
}

h1, p {
    text-align:center;
}

.container3d .flipper {
-webkit-transform-origin:top left;
-webkit-transform:rotateY(99deg);
-webkit-transition:-webkit-transform 250ms ease;

-moz-transform-origin:top left;
-moz-transform:rotateY(99deg);
-moz-transition:-moz-transform 250ms ease;

-o-transform-origin:top left;
-o-transform:rotateY(99deg);
-o-transition:-o-transform 250ms ease;

transform-origin:top left;
transform:rotateY(115deg);
transition:transform 250ms ease;
}

.container3d-ready .flipper {
-webkit-transform-origin:left;
-webkit-transform:rotateY(0deg);
-webkit-transition:-webkit-transform 2000ms ease;

-moz-transform-origin:left;
-moz-transform:rotateY(0deg);
-moz-transition:-moz-transform 2000ms ease;

-o-transform-origin:left;
-o-transform:rotateY(0deg);
-o-transition:-o-transform 2000ms ease;

transform-origin:left;
transform:rotateY(0deg);
transition:transform 2000ms ease;
}
/* END IMAGE LEFT EFFECT */

.boxShadow{
opacity:.8;
}

header img:nth-child(0){
display:block;
width:100%;
text-align:left!important;
}
</style>
<header>
	<section class="cabecera boxShadow">
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
	echo $objSiteData->headerTopContent;
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
///echo $path_raiz_includes_local.$localLandingCuerpoImage;
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
	</section>
</header>

<section class="contenido">

	<section class="foto <?=$layoutType ? '' : 'hide'?>">
	<div class="container3d effLeft">
    <div class="flipper">
        <div class="image">

<?php

if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div class="marco">
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!
			</div>
			<span class="bolo"></span>
';
}

$defaultLocalPhoneCall='900806449';
///$inbound_phone=($inbound_phone != $inbound_phone_default) ? $inbound_phone : $defaultLocalPhoneCall;	// SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$inbound_phone=$defaultLocalPhoneCall;	//POR EL MOMENTO SOLO SE DIBUJA EL TELEFONO ESPECIFICO
$drawInbound_phone=preg_replace('/\d{3}/','$0.',str_replace('.',null,trim($inbound_phone)),2);
?>

        </div>
    </div>
</div>


	</section>

	<section class="formulario">

		<form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
				<input type="hidden" name="destino" id="destino" value="">

<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
	$formScriptName='form_script_'.$idLandingFormatted.'.php';
}

$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
	echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
	include($formScriptName);
?>

				</form>
		</section>

	<div class="clearfix"></div>
</section>

	<section class="contenido_texto">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
	</section>



<footer>
	<div>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
	</div>
</footer>
