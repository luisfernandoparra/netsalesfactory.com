// JavaScript Document
//Tipos de ClickToCall. CTC==máxima prioridad. Lead==formulario grande
var array_typeofctc = ["CTC", "LEAD"];
//var cookiesEnabled = 0;

var whitespace = " \t\n\r";
var reWhitespace = /^\s+$/;

function MM_openBrWindow(theURL, winName, features) { //v2.0
	checkCookie();
	window.open(theURL, winName, features);
}

//Función que pone pixel por javascript
function ponerPixelJS(pix, idProcess, isImage) {
	if (!isImage)
	{
		var s = document.createElement("script");
		s.type = "text/javascript";
	}
	else
	{
		var s = new Image;
		s.width = "1px";
		s.height = "1px";
	}
	pix = pix.replace("[XXfeaturedparamXX]", idProcess);
	s.src = pix;

	$("body").append(s);
}


function __replaceall(msg, needle, reemp) {
	return msg.split(needle).join(reemp);
}

function clearMyMessage(msg)
{
	msg = __replaceall(msg, "<br />", "\n");
	msg = __replaceall(msg, "<br>", "\n");
	msg = __replaceall(msg, "<strong>", "");
	msg = __replaceall(msg, "</strong>", "");
	msg = __replaceall(msg, "<b>", "");
	msg = __replaceall(msg, "<i>", "");
	msg = __replaceall(msg, '</b>', "");
	msg = __replaceall(msg, "</i>", "");
	msg = __replaceall(msg, "<p>&nbsp;</p>", "\n");
	msg = __replaceall(msg, "<p>", "");
	msg = __replaceall(msg, "</p>", "\n");
	return msg;
}

function _showModal(typ, cab, msg) {
	$.facebox.close();
	var alerta = (typeof is_movil !== undefined) && is_movil == '1';

	if (!alerta) {
		$.modal('<div class="innerModal">' + msg + '<br /><center><a href="#0" class="appbutton simplemodal-close">cerrar</a></center></div>', {
			containerCss: {
				backgroundColor: "#333232",
				borderColor: "#333",
				width: "80%",
				"max-width": "500px",
				close: true,
				padding: 0
			},
			onClose: function(dialog) {
				dialog.data.fadeOut('fast', function() {
					dialog.container.hide('slow', function() {
						dialog.overlay.slideUp('fast', function() {
							$.modal.close();
						});
					});
				});
			},
			overlayClose: true
		});

	} else {
		alert(clearMyMessage(msg));
		;
	}
}

function clearForm() {
	$('#nombre').val('');
	$('#email').val('');
	$('#telefono').val('');
	$('#cp').val('');
	$('#tratamiento').val('');
	//$('#apellidos').val('');
	$('select option:selected').removeAttr('selected');
	//$('#cp').val('');
	$("#cblegales").attr("checked", false);
	if (is_ctc == '1') {
		$("#telefono_ctc").val('');
	}
}

function ComprobarInsercion(data, ind) {
//    var data = eval("(" + dat + ")");
	var msg = "";
	var b_is_movil = (typeof is_movil !== undefined) && is_movil == '1';
	var v_tduid = (typeof tduid !== undefined) ? tduid : '';
	var pix = "";

	if (data.error == 0 || data.valor == 1) { //no ha habido error. Le estamos llamando.
		clearForm();

		$('body').append(pix);
		//Ponemos el pixel dependiendo del click realizado

		if (cookiesEnabled == 1) {
			if (arr_pixel_JS[0] != "") {
				ponerPixelJS(arr_pixel_JS[0], data.id, arr_pixel_JS[2]);
			}
			if (arr_pixel_JS[1] != "") {
				ponerPixelJS(arr_pixel_JS[1], data.id, arr_pixel_JS[2]);
			}
		}
		urlTarget=array_def_verticales_values[2].replace("[[EMAIL]]",data.email); 
		urlTarget=urlTarget.replace("[[PASSWORD]]",data.passwd); 
		window.location.href = urlTarget;

		var txt = "<br /><br /><i>Gracias por participar!</i><br /><br />";
		if (!b_is_movil)
			_showModal('success', 'Gracias por participar!', txt);
		else
		{
			$.facebox.close();
			alert(clearMyMessage(txt));
		}
	} else {
		msg = '<br />Ha habido un error al procesar los datos.';
		msg += data.mensaje ? '<br /><br />' + data.mensaje + "<br />" : '<br />';
		msg += '<br />Por favor, inténtelo más tarde.';
		if (data.mensaje == 'DUPLICADO') {
			msg = 'Ya existe éste teléfono en el sistema. <br /><br />Si desea ser contactado, por favor, déjenos su teléfono en el formulario superior.';
		} else if (data.mensaje == 'RECHAZADOWS') {

			msg = 'Usted ya es cliente de Jazztel , por favor póngase en contacto con Atención al cliente en el 1565';

		}
		if (!b_is_movil)
			_showModal('error', 'Error en el procesamiento de Datos', '<p>' + msg + '</p>');
		else
		{
			$.facebox.close();
			alert(clearMyMessage(msg));
		}

	}
}


function isEmpty(s) {
	return((s == null) || (s.length == 0));
}

function isWhitespace(s) {
	return(isEmpty(s) || reWhitespace.test(s));
}

/**
 * Función que comprobará o gestionará la cookie dentro del formulario
 */
function checkCookie() {
	if (cookiesManaged == 0) {
		manageCookies(1);
	}
}

$(document).ready(function() {
	//función que validará el formulario (M.F. 08.04.2014)
	$(".sendData").click(function(el){

		res = validator.form();
		if (!res)
			return;

		$.facebox.loading();
		var ccrea = "";
		if (typeof nomb_promo === 'undefined') {
			ccrea = '??????????';
		} else {
			ccrea = nomb_promo;
		}
		var source = "";
		if (typeof id_source === 'undefined') {
			source = "";
		} else {
			source = id_source;
		}

		ind = 1;
		if (is_ctc == 1) {
			ind = 0;
		}

		var valortype = array_typeofctc[ind];
		var campana = arr_campanas[ind];

		$.ajax({
			url: root_path_local + "ajaxs/procesar_registro_ctc.php",
			method: "post",
			dataType: "json",
			data: {
				idclient: id_client,
				sourcetype: valortype,
				campaign: campana,
				fuente: source,
				em: $('#email').val(),
				tratamiento: $("input[name=tratamiento]:checked").val(),
				cp: $('#cp').val(),
				nombre: $('#nombre').val(),
				apellidos: $('#apellidos').val(),
				telefono: $('#telefono').val(),
				provincia: $('#sel_prov').val(),
				provincia_text: $('#sel_prov').find('option:selected').text(),
				cblegales: 1,
				arrayDefVerticalesParams:array_def_verticales_params,
				arrayDefVerticalesValues:array_def_verticales_values,
				crea: ccrea
			},
			cache: false,
			async: true,
			success: function(response)
			{
				ComprobarInsercion(response, ind);
			},
			error: function(response) {
				console.log("err code 2B");
			}
		})	// ajax end

	});

	$("body").append("<div id='opaque' style='display: none;'></div>");
	//Política de cookies
	$(':input').focusin(function() {
		if ($(this).attr('id') == "cookie_rejection" || $(this).attr('id') == "cookie_acept")
			return false;
		checkCookie();
	});
	$(':checkbox').click(function(e) {
		checkCookie();
	});

});

var regExTelf = /([9|6|7|8])+[0-9]{8}/;// /^[6-7]{1}\d{8}$/
var regexOnlyLetters = /^[ñÑáÁéÉíÍóÓúÚçÇa-zA-Z\ \']+$/;
var regexCP = /^[0-9]{4,5}/;
var regExEmail = /^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/;	