<?php
//header('X-Content-Type-Options:nosniff');header('X-Frame-Options:SAMEORIGIN');header('X-XSS-Protection:1;mode=block');
$sec_tok='0'.$id_client.'_'.'DJK'.date('mHd').$arr_creas[$id_crea]['nombpromo'];
$sec_tok=md5($sec_tok);
$displayCaptcha=$id_crea == 140 ? 1 : ($sitesPruebas == 140 ? 1 : 1);	// AQUI PONER EL IDENTIFICADOR DE LA CREA PARA MOSTRAR EL CAPTCHA

$googleTransactionTotal=1;

if($displayCaptcha)
{
	$googleDomainCaptcha=$sitesPruebas ? '6LcykBoTAAAAAHyF6i-ZcCxJUThMdK7IVu8spaIK' : '6LfJewcUAAAAANXVieUWTpW6pKdOvE2Vw0ng9nba';
/**
 * RE-CAPTCHA NECESSARY LIB
 * promoseguros.axa.es
 * site: 6LfJewcUAAAAANXVieUWTpW6pKdOvE2Vw0ng9nba
 * secret: 6LfJewcUAAAAAONd98pETdMSMos0tEoib-wKHzlY
 *
//$remoteip='212.0.115.180';
 */
	$response='g-recaptcha';
	include($path_raiz_includes_local.'includes/captcha/captcha.lib.php');
//	echo '<script src="https://www.google.com/recaptcha/api.js?hl=es&reset=yes"></script>';
	echo '<script src="https://www.google.com/recaptcha/api.js?hl=es"></script>';
}

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$arr_creas[$id_crea]['title']?></title>
<?php
//	ONLY FOR MOBILE DEVICES
//echo '$layoutType='.$layoutType;
if($is_movil == 1 || $layoutType)
{
?>
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-control" content="no-store" />

<?php
}
if(count(@$objSiteData->SeoSiteParams))
{
	if(@$objSiteData->SeoSiteParams['meta_canonical'])
		echo '<link rel="canonical" href="'.$objSiteData->SeoSiteParams['meta_canonical'].'" />
';
//if($debugModeNew)echo'<pre>';print_r($objSiteData->SeoSiteParams);echo'</pre>';
	
}
// NOTA: SE HA INTENCIONALMENTE COLOCADO UNA VARIABLE CON TRES DIGITOS (PRECEDIDOS DE CEROS) PARA EL CSS PRINCIPAL
$defaultCSS='000';
@$localLandingCssFile='css/'.$prefixCss.'css_'.str_pad($id_crea,3,'0',STR_PAD_LEFT).'.css';
$localLandingCssFile=(file_exists($path_raiz_includes_local.$localLandingCssFile)) ? $path_raiz_aplicacion_local.$localLandingCssFile : $path_raiz_aplicacion_local.'css/'.$prefixCss.'css_'.$defaultCSS.'.css';

	echo '<link rel="icon" href="'.$path_raiz_aplicacion_local.'img/favicon.png" type="image/x-icon" />
<link rel="shortcut icon" href="'.$path_raiz_aplicacion_local.'img/favicon.png" type="image/x-icon" />
';

?>
<base href="<?=$path_raiz_aplicacion_local?>" />
<link href="<?=$path_raiz_aplicacion_local?>js/modal/css/modal.css" rel="stylesheet" type="text/css" />
<link href="<?=$path_raiz_aplicacion_local?>js/facebox/facebox.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$path_raiz_aplicacion_local?>css/common.css" rel="stylesheet" type="text/css" />
<link href="<?=$localLandingCssFile?>" rel="stylesheet" type="text/css" />


<script type="application/javascript">
var root_path = "<?=$path_raiz_aplicacion;?>";<?php /*ruta global para las imgs del facebox y del modal*/?>
var root_path_local = "<?=$path_raiz_aplicacion_local;?>";<?php /*ruta global para ajax*/?>
var nomb_promo = "<?=$arr_creas[$id_crea]['nombpromo']?>";
var id_source = "<?=(int)$id_source?>";
var id_client = "<?=$id_client?>";
var id_crea = <?=(int)$id_crea?>;
var cli = "<?=@$cli?>";
var layoutType = <?=(int)$layoutType?>;	// M.F. 2014.12.18
var thankyoupage = "<?=$url_thankyouPage?>";
var arr_campanas = [<?php  echo $campanas?>];
var is_ctc = "<?=trim($is_ctc)?>";
var is_movil = "<?=trim($is_movil)?>";
var root_path_ws = "<?=$path_raiz_ws;?>";
var url_ws = "<?=$url_ws;?>";
var file_ws = "<?=$file_ws;?>";
var arr_pixel_JS = ["<?=$arr_pixel_JS[0];?>","<?=$arr_pixel_JS[1]?>","<?=$arr_pixel_JS[2]?>"];
var tduid = "<?=$tduid?>";
var trdbl_organization = "<?=$organization?>"; //Pixel Tradedoubler. Organizacion
var trbdl_event = "<?=$event?>";
var trbdl_program = "<?php echo $trbdl_program; ?>";
var nombCliente = "<?=$nombCliente?>";
var is_prov = "<?=trim($is_prov)?>";
var sec_tok= "<?=trim($sec_tok)?>";

var refC = "<?=(int)$id_crea?>";

var googleSEM = "";
var googleRemarketingOnly = <?=$objSiteData->semGoogleParams['remarketing_only'] ? 1 : 0?>;
var googleSEO = 0;
var google_conversion_id;

var grecaptcha;	//RE-CAPTCHA CONTROL (2016.09.23)
</script>
<?php
$blockBodySEM='';
//$remarketingLandingAdWords=false;	// CONTROL FOR GOOGLE REMARKETING ONLY (2014.11.21 - M.F.)

/*
 * SI LA PROMO TIENE DATOS PARA SEM (Search Engine Marketing)
 * se rellena el array correspondiente para construir
 * el pixel correspondiente (M.F. 2014.06.26)
 */
//echo '<pre>';print_r($objSiteData->semGoogleParams);echo '</pre>';
if(count($objSiteData->semGoogleParams))
{
////////echo 'remarketing_only='.@$objSiteData->semGoogleParams['remarketing_only'];
	if(@$objSiteData->semGoogleParams['google_ga'])	// SOLO PARA `seo` Y PARA LAS LANDINGS ´SEM´ QUE TENGAN CONFIGURADO EL `id GA`
	{
		echo '
<script type="application/javascript">
var googleSEO = "'.$objSiteData->semGoogleParams['google_ga'].'";
</script>
';
	}

	$outDem='//www.googleadservices.com/pagead/conversion/';
	$outJs='
/* <![CDATA[ */
';
	$outJsAcceptRegister='';

	foreach($objSiteData->semGoogleParams as $key=>$value)
	{
		switch($key)
		{
			case 'conversion_id';
				$outDem.=$value.'/';
				$bodyJsGoogle[$key]=$value;
				$outJs.='var google_'.$key.'='.$value.';
';
				break;
			case 'remarketing_only';
				$outDem.=$value.'/';
				$bodyJsGoogle[$key]=$value ? 'true' : 'false';
				$outJs.='var google_'.$key.'=true;
';
				echo '<script>var xxx_'.$key.'=true;</script>';
				break;
			case 'conversion_label';
				if($key != 'remarketingLandingAdWords')	// SKIP GOOGLE REMARKETING ONLY EXCEPTION PARAM (2014.11.21 - M.F.)
				{
					$bodyJsGoogle[$key]=$value;
					$outDem.='?label='.$value.'&amp;guid=ON&amp;script=0';
				}

				break;
			case 'id_sem';
			case 'id_landing';
				break;
			default:
				$bodyJsGoogle[$key]=$value;
		}
	}
	$outJs.='var google_custom_params=window.google_tag_params;
';
	$outJs.='
/* ]]> */
';

	$commonGoogleParams='
		var google_conversion_id = '.$bodyJsGoogle['conversion_id'].';
		var google_conversion_language = "'.$bodyJsGoogle['conversion_language'].'";
		var google_remarketing_only = '.$bodyJsGoogle['remarketing_only'].';
';

	if($objSiteData->semGoogleParams['remarketing_only'])
		$specificGoogleParams='';
	else
		$specificGoogleParams='
		var google_conversion_format = "'.$bodyJsGoogle['conversion_format'].'";
		var google_conversion_color = "'.$bodyJsGoogle['conversion_color'].'";
		var google_conversion_label = "'.($bodyJsGoogle['conversion_label'] === 'remarketingLandingAdWords' ? '' : $bodyJsGoogle['conversion_label']).'";
';

	$blockBodySEM='
<script type="application/javascript">
'.$outJs.$outJsAcceptRegister.'googleSEM="'.$outDem.'";

$(document).ready(function(){
});

	function googleAdWords()
	{
		/* <![CDATA[ /
		'.$commonGoogleParams.'
		'.$specificGoogleParams.'
		/ ]]> */
		includeScript("//www.googleadservices.com/pagead/conversion.js","js");
	}

</script>
';
}

/*
 * EXCEPCION GOOGLE REMARKETING ONLY EXCEPTION PARAM
 * en la BBDD el campo conversion_label debe ser igual a "remarketingLandingAdWords"
 * (M.F. 2014.11.21)
 */
//echo '-->'.$objSiteData->semGoogleParams['remarketing_only'];
if($objSiteData->semGoogleParams['remarketing_only'])
{
	$blockBodySEM.='
<script type="application/javascript">
/* <![CDATA[ */
var google_conversion_id = '.$objSiteData->semGoogleParams['conversion_id'].';
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>

<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/'.$objSiteData->semGoogleParams['conversion_id'].'/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
';
}

?>
<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/jquery-1.10.1.min.js"></script>
<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/jquery.validate.min.js"></script>
<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/modal/js/jquery.simplemodal.js"></script>
<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/facebox/facebox.min.js"></script>
<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/funciones_comunes.js"></script>

<script>
var validator;

$(document).ready(function(){
	$("#telefono").val("");

	$("#enviarPorMailSolicitaInfo").validate({	// CHECK PHONE NUMBER
		onkeyup:false,
		onclick:false,
		submitHandler:function(form){},
		rules:{
			nombre:{
				required:true,
				dataFilter:function(value){
					var patt = /[a-z]/;
				}
			},
			telefono:{
				required:true,
				remote:{
					url:root_path_local +"includes/phone_check.php",
			    contentType:"application/json; charset=utf-8",  
					data:{cr:<?=(int)$id_crea?>},
					dataFilter:function(response){
						var jsonResponse="";
						jsonResponse = JSON.parse(response);
							if(jsonResponse.success == false)
							{
								return "\"" + jsonResponse.errorMessage + "\"";
							}else{
								return true;
							}
						return false;
					}
				}
			}
			,email:{
				required:true,
				remote:{
					url:root_path +"includes/api_email_check.php",
			    contentType:"application/json; charset=utf-8",  
					data:{cr:<?=(int)$id_crea?>,id_client:id_client,hashPipe:"<?=$controlChekAlgorithm?>"},
					beforeSend: function(){
						$("#btnProcesar").prop("disabled","true").css("opacity","0.8");
					},
					complete: function(){
						$("#btnProcesar").prop("disabled","").css("opacity","1");
					},
					dataFilter:function(response){
						var jsonResponse="";
						jsonResponse=JSON.parse(response);
							if(jsonResponse.success == false)
							{
								return "\"" + jsonResponse.errorMessage +  "\"";
							}else{
								return true;
							}
						return false;
					}
				}
			}
		},
		errorLabelContainer: $("#enviarPorMailSolicitaInfo div.error")
	});

	validator=$("#enviarPorMailSolicitaInfo").validate({
		errorContainer:$('div.error'),
		errorLabelContainer:$('div.error ul'),
		wrapper: 'li'
	})

<?php

// START RE-CAPTCHA CONTROL
if($displayCaptcha)
{
?>
/**
 * OK
var onloadCallback = function() {
	grecaptcha.render('html_element', {
    'sitekey' : '<?=$googleDomainCaptcha?>'
  });
};
onloadCallback();

$('#enviarPorMailSolicitaInfo').on('submit', function(e) {
  if(grecaptcha.getResponse() == "") {
    e.preventDefault();
    alert("You can't proceed!");
  } else {
    alert("Thank you");
  }
});
*////

//	jQuery.validator.addMethod("g-recaptcha-response", function (value, element, param)
//	{
//		var v = new Object();
//		v = grecaptcha.getResponse();
//
//		if(v == undefined)
//		{
////				alert("!!!!")
//			return false;
//		}
//
//		if(v.length != 0)
//			return true;
//	});
<?php
}
// END RE-CAPTCHA CONTROL

?>

});

</script>

<?php
if(!empty($arr_creas[$id_crea]['js']) && trim($arr_creas[$id_crea]['js']) != '' )
{
?>
	<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/<?=trim($arr_creas[$id_crea]['js'])?>"></script>
<?php

}else{
?>
	<script type="application/javascript" src="<?=$path_raiz_aplicacion_local?>js/validacion.js"></script>
<?php
}

if($displayCaptcha)
{
	$cssGoogleCaptcha='
<style>
#g-recaptcha{
width:30%;
margin:auto;
padding-top:10px;
}
@media (max-width: 680px){
#g-recaptcha{
width:50%;
}}

@media (max-width: 510px){
#g-recaptcha{
width:70%;
}}

@media (max-width: 380px){
#g-recaptcha{
width:80%;
}}

@media (max-width: 374px){
#g-recaptcha{
width:95%;
}}

@media (min-width:355px) and (max-width: 374px){
#g-recaptcha{
width:85%;
}}

@media (min-width:374px) and (max-width: 385px){
#g-recaptcha{
width:80%;
}}
</style>
';
}

if($layoutType && $arr_creas[$id_crea]['mobile_auto_modal_on_open'])  // GESTION MODAL "LLAMAME"
{
  echo '
<style>
.modalCllame{
display:block;
width:100%;
left:0px!important;
color:#009CD9;
}

.closeModalCall{
color:#000;
text-decoration:none;
}
.callMeText{
font-size:150%;
font-weight:bold;
}
.modalCloseImg{display:none!important;}

</style>';
}
?>
<style type="text/css">
label{display:block;text-align:left;color:#0178c8;font-weight:bold;padding-top:0px;margin-bottom:5px;}
/*estilos mensajes error*/
div.error1{display : none; border:2px solid #D81E05; }
div.error1{margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
div.error{ display : none; border:2px solid #D81E05; }
.error{color:#D81E05; background:#FCF1F0;}
div.error ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error ul li{margin:0 0 10 0;list-style-type:none;}
div.error ul li label{font-weight:normal}
div.error, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error, textarea.error{border:2px solid #D81E05;background-color:#FCF1F0;}
input.ok{background-color:#EFFFDA}

.innerModal{
display:inline-block;
background:#fff;
text-align:center;
width:100%;
height:250px;
}

.modal{
height:680px!important;
}

#simplemodal-container a.modalCloseImg{background:url(<?=$path_raiz_aplicacion_local;?>img/close_modal.png)no-repeat 2px 0px rgba(0, 0, 0, 0)!important;right:-8px;top:-14px;}
#facebox img{width:32px!important;height:1px!important;text-align:center;}
</style>
<script type="application/javascript">
var isModalDraw=false;
var leadNumber = Math.round((new Date().getTime() * Math.random()));
function offModal(){$.modal.close();} // CLOSE MODAL "LLAMAR"

$(document).ready(function(){
	$(".innerModal").css("height",(document.body.clientHeight)+"px");
	$("#informacion").delay(500).slideDown("slow");
});
</script>
<!--[if lte IE 8]><link rel="stylesheet" href="<?=$path_raiz_aplicacion_local?>css/ie.css" /><![endif]-->
</head>
