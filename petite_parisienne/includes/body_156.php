<?php
@session_start();
$idLandingFormatted = str_pad($objSiteData->landingId, 3, '0', STR_PAD_LEFT);
$rutaImgs = $path_raiz_aplicacion_local . 'img/' . $idLandingFormatted . '/';
$defaultLocalPhoneCall = '';
$inbound_phone = ($inbound_phone) ? $inbound_phone : ''; // SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
function obtenerNavegador2015($user_agent=0)
{
 $browsers='mozilla msie gecko firefox konqueror safari netscape navigator opera mosaic lynx amaya omniweb chrome';
 $browsers=@split(' ',$browsers);
 $nua=strToLower($_SERVER['HTTP_USER_AGENT']);
 $l=strlen($nua);
 for($i=0;$i<count($browsers);$i++)
 {
  $browser=$browsers[$i];
  $n=stristr($nua,$browser);
  if(strlen($n)>0)
  {
   $verNav = '';
   $navX = $browser;
   $j=strpos($nua, $navX)+$n+strlen($navX)+1;
   for(; $j<=$l; $j++)
   {
    $s=substr($nua,$j,1);
    if(is_numeric($verNav.$s))
    $verNav.= $s;
    else
    break;
   }
  }
 }
 if(!$navX) $navX=$_SERVER['HTTP_USER_AGENT'];
 $naveg=array(
	 'all'=>$navX.' '.$verNav,
	 'ver'=>$verNav,
	 'name'=>$navX
 );
 return $naveg;
}

$elNavegador=obtenerNavegador2015($_SERVER['HTTP_USER_AGENT']);
$userNavigator=$elNavegador['name'];
?>
<link href="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.css" type="text/css" rel="stylesheet" />
<link href="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.min.js"></script>
<style>
div.error2{ display : none; border:2px solid #D81E05; }
.error2{color:#D81E05; background:#FCF1F0;}
div.error2 ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error2 ul li{margin:0 0 10 0;list-style-type:none;}
div.error2 ul li label{font-weight:normal}
div.error2, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error2{border:2px solid #D81E05;background-color:#FCF1F0;}

.appbutton {
		background-color:#009fe4;
		background-image:url("img/f-boton-color.png");
		background-position:-10px 0;
		background-repeat:no-repeat;
		border:medium none #00335b;
		border-radius:3px;
		box-shadow:0 1px 2px #424242;
		color:white !important;
		cursor:pointer;
		display:inline-block;
		font:14px Arial,Helvetica,sans-serif;
		margin:5px 5px 0 0;
		padding:8px 13px;
		text-align:center;
		text-decoration:none !important;
		text-shadow:0 1px #00335b;
		width:auto;
}
.innerModal{
		box-sizing:border-box;
		margin:0;
		padding:0;
		padding-top:6%;
		position:relative;
		line-height:22px!important;
		font:12px Arial,Helvetica,sans-serif;
}
</style>
<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/lib/additional-methods.min.js"></script>

<script type="text/javascript">
var errCheckTel = 1;
var iconselected = 1;
var css_selected = 'circulo_azul ico_selected';
var css_not_selected = 'circulo_azul';
var isCtcPass=0;

$(document).ready(function() {

	$("#ctcForm").validate({
			rules:{
				telefono_ctc:{
					required:true
					,remote:{
						url:root_path_local +"includes/phone_check.php",
						contentType:"application/json; charset=utf-8",  
						data:{cr:<?=(int)$id_crea?>},
						dataFilter:function(response){
							var jsonResponse="";
							isCtcPass=1;
							jsonResponse = JSON.parse(response);
							if(jsonResponse.success == false)
							{
								isCtcPass=0;
								return "\"" + jsonResponse.errorMessage + "\"";
							}else{
								return true;
							}
							return false;
						}
					}
				}
			},
			errorLabelContainer: $("#ctcForm div.error2")
	});

	$('input[name="cookie_rejection"]').click(function(){
		setTimeout('$("#pie_galletero_left").css("width","95%").css("text-align","left");',350)
	});


	$("#boton_ctc").click(function(){
			var phoneBumberCtc=$.trim($('#telefono_ctc').val());
			res=validator2.element("#telefono_ctc");

			if(!isCtcPass)
			{
				res=validator2.element("#telefono_ctc");
				setTimeout('$("#boton_ctc").click();',300);
				return false;
			}

			if(!res && !isCtcPass)
				return false;

			if($.isNumeric(phoneBumberCtc) && phoneBumberCtc.length >= 9)
			{
					var valortype = array_typeofctc[0];
					var campana = arr_campanas[0] ? arr_campanas[0] : 0;
					$.facebox.loading();
					$.ajax({
							url: root_path_local + "includes/phone_check.php",
							method: "post",
							dataType: "json",
							data: {cr:<?= (int) $id_crea ?>, telefono_ctc: phoneBumberCtc},
							cache: false,
							async: false,
							success: function(response)
							{
									if (!response.error)
									{
											$.ajax({
													url: root_path_local + "ajaxs/procesar_registro_ctc.php",
													method: "post",
													dataType: "json",
													data: {
															telefono: phoneBumberCtc,
															sourcetype: valortype,
															campaign: campana,
															fuente: id_source,
															idclient: id_client,
															crea: nomb_promo
													},
													cache: false,
													async: false,
													success: function(response)
													{
															if(!response.error)
															{
																ComprobarInsercion(response, 0);
															}
															else
															{
																ComprobarInsercion(response, 0);
															}
													},
													error:function(response){
															console.log("err2");
															return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
													}
											});
									}
									else
									{
											console.log("err code 2C");
									}
							},
							error: function(response) {
									console.log("err2");
									return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
							}
					});
			}
			return false;
	});

	validator2 = $("#ctcForm").validate({
			errorContainer: $('div.error2'),
			errorLabelContainer: $('div.error2 ul'),
			wrapper: 'li'
	});

	/**
	 * Fiunción para cambiar el texto según se haga click en el icono
	 */
	$('[id^="ico-"]').click(function(e) {
			var id = $(this).attr('id');
			var tmp = id.split('-');
			var tmp_iconselected = tmp[1];
			if (iconselected != tmp_iconselected) {
					$('#text-' + iconselected).hide();
					$('#text-' + tmp_iconselected).fadeIn('slow');
					$('#selected-' + iconselected).removeClass(css_selected).addClass(css_not_selected);
					$('#selected-' + tmp_iconselected).removeClass(css_not_selected).addClass(css_selected);
					iconselected = tmp_iconselected;

			}
	})

	//********************
	// START NIF NIE CHECK
	jQuery.validator.addMethod("identificacionES", function(value, element)
	{
		"use strict";
		value = value.toUpperCase();
		// Texto común en todos los formatos
		if (!value.match('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)')){
			return false;
		}
		/* Inicio validacion NIF */
		if (/^[0-9]{8}[A-Z]{1}$/.test(value)){
			$("#doc_type").val(1);
			return ("TRWAGMYFPDXBNJZSQVHLCKE".charAt(value.substring(8, 0) % 23) === value.charAt(8));
		}
		//  Hay ciertos NIFs que empiezan por K, L o M
		if (/^[KLM]{1}/.test(value)){
			$("#doc_type").val(1);
			return (value[8] === String.fromCharCode(64));
		}
		/* Fin validacion NIF */

		/* Inicio validacion NIE */
		if (/^[T]{1}/.test(value)){
			$("#doc_type").val(2);
			return (value[8] === /^[T]{1}[A-Z0-9]{8}$/.test(value));
		}
		// Con los que empiezan por XYZ
		if (/^[XYZ]{1}/.test(value)){
			$("#doc_type").val(2);
			return(
				value[8] === "TRWAGMYFPDXBNJZSQVHLCKE".charAt(
				value.replace('X', '0')
				.replace('Y', '1')
				.replace('Z', '2')
				.substring(0, 8) % 23
				)
			);
		}
		/* Fin validacion NIE */
		return false;
	}, "El campo <b>Nº de documento</b> debe contener un <b>NIF / NIE</b> correcto");
	// END NIF NIE CHECK
	//********************

<?php
//if($userNavigator != 'chrome')	// SE OMITE EL CONTROL PARAS CHROME
{
?>
	// FILL WIDTH "/" DATE FIELD
	$('#birth_date').keydown(function(e){
		if(e.keyCode != 8){
			var l=$(this).val().length, x = l === 2 || l === 5 ? '/' : '';
			if(l >= 10) return false;
			$(this).val($(this).val()+x);
		}
	});
<?php
}

if($userNavigator != 'safari' && $userNavigator != 'chrome')	// FOR birth_date CONTROL SET
{
?>
//	$("#birth_date").focus(function(){$(this).attr("type","date")});
//	$("#birth_date").blur(function(){if($(this).val() == "") $(this).attr("type","text");});
<?php
}
?>

}); //document.ready
</script>

<header>
<?php
if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
echo $objSiteData->headerTopContent;

else
echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
</header>


<section class="basic-content">
	<section class="small-content">
		<section class="skeleton">
<?php
if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div class="marco">
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!
			</div>
';
}

?>
			</section>


    <section class="formulario">
<?php
/**
 * EXCEPCION PARA ENVIAR EL CORREO QUE LLEGA COMO PARAMETRO POR URL
 */
$emailingSource = (!empty($_REQUEST['emailingsource']) && strtoupper(trim($_REQUEST['emailingsource'])) == 'GMAIL');

$nombre = (!empty($_REQUEST['nombre'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', ucwords(strtolower(trim($_REQUEST['nombre'])))) : '';

$apellidos = (!empty($_REQUEST['apellidos'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', ucwords(strtolower(trim($_REQUEST['apellidos'])))) : '';
$email = (!empty($_REQUEST['email'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,EMAIL', strtolower(trim($_REQUEST['email']))) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', $_REQUEST['provincia_text']) : (normalizeInputData('INPUT,STRINGS,SQLINJECTION', @$_REQUEST['provincia']) ? $_REQUEST['provincia'] : 0);

$legal = (!empty($_REQUEST['cblegales'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['cblegales'])) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : 1;
$permanencia = (!empty($_REQUEST['perm'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['perm'])) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;

$crea = (!empty($_REQUEST['crea'])) ? (int)$_REQUEST['crea'] : $id_crea;
//$id_crea=(!empty($_REQUEST['id_crea'])) ? trim($_REQUEST['id_crea']) : 0;
//echo $crea;die();
//echo 'IdClient: '.$_REQUEST['idclient']. ' Crea:'.$crea;
$idSource = (!empty($_REQUEST['id_source']) && is_numeric($_REQUEST['id_source'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['id_source'])) : 1;
//$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['idclient'])) : '';

$sourcetype = (!empty($_REQUEST['sourcetype'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['sourcetype'])) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['campaign'])) : '0';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['fuente'])) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['prioridad'])) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['telefono'])) : '';
//die('Normalizado:'.normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER',trim($_REQUEST['telefono'])));
$use_priority = (!empty($_REQUEST['use_priority']) && is_numeric($_REQUEST['use_priority']) && $_REQUEST['use_priority'] == 1 && $prioridad != '') ? 1 : 0;

$sec_tok = !empty($_REQUEST['sec_tok']) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', $_REQUEST['sec_tok']) : 0;

$check_sec_tok = '0'.$id_client . '_' . 'DJK' . date('dmd') . '' . $crea;
//$check_sec_tok = '0'.$id_client.'_'.'DJK'.date('mHd').$arr_creas[$id_crea]['nombpromo'];
$check_sec_tok = md5($check_sec_tok);

//echo $id_client .' , $crea= '. $crea."\r\n";
//echo $sec_tok .' != '. $check_sec_tok;die();
if ($sec_tok != $check_sec_tok) {
    $telefono = '';
    $tokenError = ', Error de TOKEN de suguridad';
    $resCurl['msg'] = 'Error Token';
}
//die();
$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

/**
 * POR DEFECTO SE ENVIAN LAS DIRECCIONES DE "$transactionEmailDoctorSender"
 *
 * EN CASO DE QUE EXISTA "$transactionEmailDoctorSenderSpecivicCrea[$id_crea]" SE UTILIZAN LOS EMAILS AHI CONTENIDOS
 */
$defaultReceiver=(isset($transactionEmailDoctorSenderSpecivicCrea[$id_crea])) ? $transactionEmailDoctorSenderSpecivicCrea[$id_crea] : $transactionEmailDoctorSender;

$campos = array('idclient' => $id_client,'id_source' => $idSource, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad,
    'nombre' => $nombre, 'apellidos' => $apellidos, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI,
    'vcc' => 'fintel', 'pwd' => $secret_pwd, 'usr' => $secret_usr,
    'use_priority' => $use_priority);

if (!empty($_SESSION['ws_ip_id']) && is_numeric($_SESSION['ws_ip_id'])) {
	$campos['idIp'] = $_SESSION['ws_ip_id'];
}

$conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);

if (!$conexion->connectDB()){
	$msg_err = 'No se ha podido conectar a la BBDD. Error 102';
	$resCurl['msg'] = $msg_err;
}

$cur_conn_id = $conexion->get_id_conexion();
$nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
$nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');

$telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
$sql = 'INSERT INTO %s (id_cliente,id_crea,id_source,nombre,apellidos,telefono,id_provincia,b_legales,email) VALUES (\'%d\',\'%d\',\'%d\',\'%s\',\'%s\',\'%s\',\'%d\', %d,\'%s\');';
$sql = sprintf($sql, $table_registros, $id_client,$id_crea, $idSource, $nombreIns, $crea, $telefonoIns, (int) $_REQUEST['provincia'], $legal, $email);
$conexion->ejecuta_query($sql);
$idInsertRegistros = mysql_insert_id();
$resCurl['id']=$idInsertRegistros;
$conexion->disconnectDB();

$fileCsv='petiteparisienne/petite_parisienne-'.$crea.'-'.date('Ymd').'.csv';
$headerFile='';

if(!file_exists($fileCsv))
	$headerFile='E-mail; crea; ID source'."\r\n";;

$googleTransactionTotal=(filter_var(strtolower($email), FILTER_VALIDATE_EMAIL) !== false);

if($googleTransactionTotal)	// SOLO SI EL EMAIL ES SINTACTICAMENTE CORRECTO
{
	$fp=@fopen($fileCsv,'a');// or die('Unable to open file!');
	$line = $headerFile.$email.';'.$crea.';'.$idSource."\r\n";
	$raw=$error;
	$error=fwrite($fp, $line);
	fclose($fp);
	$error=(int)$error > 0 ? '' : 1;
}

$tmpDir=$_SERVER['PHP_SELF'];
$tmpDir=strrpos($tmpDir,'/');
$tmpS=substr($_SERVER['PHP_SELF'],0,$tmpDir);
$tmpUrlFirst=strrpos($tmpS,'/');
$tmpDir=substr($tmpS,$tmpUrlFirst+1);

$tmpUrl=$_SERVER['REQUEST_URI'];
$tmpUrl=strrpos($tmpUrl,'?');
$specificCountry=substr($_SERVER['REQUEST_URI'],1,$tmpUrl-1);
$strCountry=$tmpDir;
$tmpUser=$tmpDir.'-'.$specificCountry.'-'.$crea;
$landingUrl=$_SERVER['HTTP_HOST'].$_SERVER['REDIRECT_URL'];

$htmlData='
<html>
<head></head>
<body>
<div style=font-family:arial,verdana;font-size:1em;color:#000;display:inline-block;width:99%;float:left;text-align:left;>
E-mail: <b>'.$email.'</b><br />
Fecha del registro: <b>'.date('d-m-Y').'</b><br />
Referencia de la landing: <b>'.$specificCountry.'</b><br />
Referencia 2: <b>'.$crea.'</b><br />
Source: <b>'.$idSource.'</b><br />
</div>
</body>
</html>
';

if(@$_REQUEST['debugMode'] == 1)
{
echo '<hr>$tmpDir = '.$tmpDir;
echo '<hr>$specificCountry = '.$specificCountry;
echo '<hr>REQUEST_URI = '.$_SERVER['REQUEST_URI'];
echo '<hr>'.$tmpUser.'<hr>';
echo '<hr>$landingUrl  '.$landingUrl.'<hr>';
//echo'<pre>';print_r($_SERVER);
echo $htmlData;
die();
}

$arrData=array('user'=>$tmpUser, 'data'=>array(
		array(
		'receiver'=>$defaultReceiver,
		'subject'=>'Datos registro Petite Parisienne',
		'html'=>$htmlData,
		'text'=>$htmlData
		)
	)
);

if($googleTransactionTotal)	// SOLO SI EL EMAIL ES SINTACTICAMENTE CORRECTO
{
	$fields_string = json_encode($arrData);
	$url = $url_webservice . $file_webservice;//. '?user='.$crea;
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_POST, 1);
	curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, $timeout);
	$raw = curl_exec($ch);
//	$statusWs = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);

	if(isset($resCurl['correct']) && $resCurl['correct'] < 1)
		$error=1;

	$urlCrm='http://smart-segments.com/crm/ws_ns/server.php?fuente=338&email='.$email.'&tratamiento=&nombre=fake&apellido1=fake&apellido2=fake&telefono=&direccion=&numero=&cp=&poblacion=&provincia=&ip=&fecha_nacimiento=&fecha_inscripcion=&url_sorteo='.$landingUrl.'&premio='.$specificCountry.'&cod_afiliado=&afiliado=&email_ok=&tel_ok=&id_ext=';
	$resCrm=@file_get_contents($urlCrm);

	if(@$_REQUEST['debugMode'] == 2)
	{
		echo '<hr>';print_r($resCrm);
	}
}
?>
    </section> <!--CIERRE FORM -->
  </section>
</section>


<section class="text-content">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
	</section>
<!--CIERRE CONTENIDO --> 
</section>

	<footer class="information">
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
	</footer>