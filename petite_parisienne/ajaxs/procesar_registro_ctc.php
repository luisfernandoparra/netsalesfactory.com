<?php
/**
 * Modificación LFP 2015.03.23
 * Añadimos variable para procesar los datos que vienen de acción mobile emailing Gmail
 * Para eso añadimos variable $emailingSource que será true si viene de gmail
 * Si esta variable es true, no hacemos el die del final y tenemos los datos en un array
 * @date 2016.09.26 Forzamos a que la cabecera de respuesta sea json
 */
@session_start();
//header('Content-Type: text/html; charset=utf-8');
header('Content-type: application/json; charset=utf-8');
/**
 * Config loaded viene del fichero bridge. Si está definida y es true, es que ya ha cargado los configs
 */
if (empty($configLoaded)) {
    include('../../conf/config_web.php');
    include('../conf/config_web.php');
}
$resCurl = array();
$raw = '';
$tokenError = '';
$resCurl['error'] = 1;
//$sOutput['id'] = -1;
$cli = empty($_REQUEST['cli']) ? '' : $_REQUEST['cli'];
$emailingSource = (!empty($_REQUEST['emailingsource']) && strtoupper(trim($_REQUEST['emailingsource'])) == 'GMAIL');

$nombre = (!empty($_REQUEST['nombre'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', ucwords(strtolower(trim($_REQUEST['nombre'])))) : '';

$apellidos = (!empty($_REQUEST['apellidos'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', ucwords(strtolower(trim($_REQUEST['apellidos'])))) : '';
$email = (!empty($_REQUEST['em'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,EMAIL', strtolower(trim($_REQUEST['em']))) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', $_REQUEST['provincia_text']) : (normalizeInputData('INPUT,STRINGS,SQLINJECTION', @$_REQUEST['provincia']) ? $_REQUEST['provincia'] : 0);

$legal = (!empty($_REQUEST['cblegales'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['cblegales'])) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : 1;
$permanencia = (!empty($_REQUEST['perm'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['perm'])) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;

$crea = (!empty($_REQUEST['crea'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['crea'])) : '';
$creaId= (!empty($_REQUEST['creaId'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', (int)$_REQUEST['creaId']) : '';
$gcaptcha= (!empty($_REQUEST['gcaptcha'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', $_REQUEST['gcaptcha']) : null;

//echo 'IdClient: '.$_REQUEST['idclient']. ' Crea:'.$crea;
$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['idclient'])) : '';

$sourcetype = (!empty($_REQUEST['sourcetype'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['sourcetype'])) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['campaign'])) : '0';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['fuente'])) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', trim($_REQUEST['prioridad'])) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER', trim($_REQUEST['telefono'])) : '';
//die('Normalizado:'.normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER',trim($_REQUEST['telefono'])));
$use_priority = (!empty($_REQUEST['use_priority']) && is_numeric($_REQUEST['use_priority']) && $_REQUEST['use_priority'] == 1 && $prioridad != '') ? 1 : 0;

$sec_tok = !empty($_REQUEST['sec_tok']) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION', $_REQUEST['sec_tok']) : 0;

$check_sec_tok = '0' . $id_client . '_' . 'DJK' . date('mHd') . '' . $crea;
$check_sec_tok = md5($check_sec_tok);
//echo $sec_tok .' != '. $check_sec_tok;die();
//if ($sec_tok != $check_sec_tok) {
//    $telefono = '';
//    $tokenError = ', Error de TOKEN de suguridad';
//    $resCurl['msg'] = 'Error Token';
//}
//die();
$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

$campos = array(
'idclient' => $id_client, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad,
	'nombre' => $nombre, 'apellidos' => $apellidos, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI,
	'vcc' => 'fintel', 'pwd' => $secret_pwd, 'usr' => $secret_usr,
	'use_priority' => $use_priority
);

$campos['cli']=$cli;	// ADDED 2016.04.13 M.F.
//,"serverIp"=>"84.127.240.42"

if (!empty($_SESSION['ws_ip_id']) && is_numeric($_SESSION['ws_ip_id'])) {
    $campos['idIp'] = $_SESSION['ws_ip_id'];
}

$qs = http_build_query($campos);
$url = $url_ws . $file_ws . '?' . $qs;

//echo $url.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();
$conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
if(!$conexion->connectDB()){
		$msg_err = 'No se ha podido conectar a la BBDD. Error 102';
		$resCurl['msg'] = $msg_err;
}


switch($creaId)
{
	case 140:
		if(!$gcaptcha || strlen($gcaptcha) < 500)	//  NO SE PROCEDE SIN EL CAPTCHA CORRECTO
		{
			$error = 1;
			$msg_err = '<b>Por favor, verifique que <u>no es un robot</u></b><br /><br />';
			$resCurl['msg'] = $msg_err;
			$res = json_encode($resCurl);
			die($res);
		}
		break;

	case 999:
		break;
}
//echo $url;die();

if ($telefono != '')
{
    $cur_conn_id = $conexion->get_id_conexion();
    $nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
    $nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');

    $telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
    $sql = 'INSERT INTO %s (id_cliente,nombre,apellidos,telefono,id_provincia,b_legales,email) VALUES (\'%d\',\'%s\',\'%s\',\'%s\',\'%d\', %d,\'%s\');';
    $sql = sprintf($sql, $table_registros, $id_client, $nombreIns, $crea, $telefonoIns, (int) $_REQUEST['provincia'], $legal, $email);
    $conexion->ejecuta_query($sql);
    $id = mysql_insert_id();

//echo $url.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();
    $ch = curl_init();
    $timeout = 0; // set to zero for no timeout
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $raw = curl_exec($ch);
    curl_close($ch);
    $resCurl = json_decode($raw);
    $error = $resCurl->error;
    //print_r($resCurl);

    if ($error) {
        $msg_err = 'Parece que el problema se debe a lo siguiente: "' . $resCurl->mensaje . '".';
    } else {
        $tmpCrea = $_SESSION['idCreatividad'];
//            unset($_SESSION['namePromo']); // SE DESTRUYE LA REFERENCIA DE LA SESION (comentado el 2016.02.04)
        $tmpName = session_name('petite_parisienne' . date(Yimis));
//            $_SESSION['namePromo'] = session_name(); // SE CONSTRUYE UNA NUEVA SESION PARA PERMITIR REGISTROS NUEVOS DESDE LA MISMA APERTURA (comentado el 2016.02.04)
        $_SESSION['idCreatividad'] = $tmpCrea;
        if (!$emailingSource) {
            die($raw);
        } else {
            $resCurl = json_decode($raw, true);
        }
    }
} else {
    if ($telefono == '') {
        $error = 1;

        $msg_err = 'Falta el campo Teléfono. Error 104';
        $resCurl['msg'] = $msg_err;
    }
}

if ($error) {
    $query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
    $query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
    $conexion->getResultSelectArray($query);
    $resQuery = $conexion->tResultadoQuery;
    $idUpdate = $resQuery[0]['id'];
    $query = 'UPDATE %s SET http_referer=\'ERROR CTC: %s\' WHERE id=\'%d\'';
    $query = sprintf($query, $table_front_actions, $resCurl->mensaje . $tokenError, (int) $idUpdate);
    $conexion->ejecuta_query($query);
}
$conexion->disconnectDB();
if (!$emailingSource) {
    $res = json_encode($resCurl);
    die($res);
}
?>