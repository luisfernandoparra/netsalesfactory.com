<?php
include('../conf/config_web.php');
ini_set('display_errors',$paintErrors);
ini_set("session.cookie_secure", 1);
include('conf/config_web.php');
include($path_raiz_includes.'includes/initiate.php');
include($path_raiz_includes_local.'includes/local_vars.php');
$layoutType=null;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
/*
 * SE REDIRECCIONA SEGUN EL TIPO DE DISPOSITIVO UTILIZADO PARA ABRIR LA PROMOCION
 * 
 * $layoutType = PREFIJO DEL DISPOSITIVO UTILIZADO
 */
if((@$arr_creas[$id_crea]['manageDevice']))
{
	include($path_raiz_includes.'includes/index_mobile_manager.php');	// SCRIPT PARA DETECTAR EL DISPOSITIVO ABRE LA WEB
}
//echo '<h1>layoutType===>'.$layoutType.'</h1>';
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// SOLO TESTS::PARA FORZAR EL TIPO DE DISPOSITIVO A MOBILE
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if(@$_REQUEST['forzarmovil'])
{
	$layoutType=$arr_creas[$id_crea]['new_template'] ? 1 : 'mob_';
	$is_movil=1;
}

$layoutSorteo='';
//echo $_SERVER['HTTP_REFERER'];

//	START EXCEPCION PARA PERMITIR COMPONER LA PROMO CORRECTAMENTE EN EL IFRAME DE LOS SORTEOS, GTP (M.F. 20140226)
$arraySorteos=array(
  'http://www.gana25mileuros.com/landings/',
  'http://www.ganatucoche.com/landings/',
  'http://www.ganaunaplay.com/landings/',
  'http://www.ganaundiadecompras.com/landings/',
  'http://www.ganaungalaxy4.com/landings/',
  'http://www.ganaungranviaje.com/landings/',
  'http://www.ganaunhomecinema.com/landings/',
  'http://www.ganauniphone5.com/landings/',
  'http://www.ganaunpartido.com/landings/',
  'http://www.ganaunsegurodental.com/landings/',
  'http://www.sorteoandalucity.es/landings/',
  'http://www.sorteoelprogreso.es/landings/',
  'http://139.162.246.12/ganatuspremios.4.0.com/plantilla/landings/'
);

if(isset($_SERVER['HTTP_REFERER']) && in_array($_SERVER['HTTP_REFERER'],$arraySorteos))
{
  $layoutSorteo='sorteoIframe';
}
//	END EXCEPCION PARA PERMITIR COMPONER LA PROMO CORRECTAMENTE EN EL IFRAME DE LOS SORTEOS

if($layoutType)
{
//  $is_movil=$arr_creas[$id_crea]['new_template'] ? 1 : 'mob_';
	$is_movil=1;
}
//echo 'xx '.$layoutType;die();
//$is_movil=1;
// START EXCEPCION PARA LA IMAGEN DE LA CABECERA PARA UNA FECHA EN UNAS DETERMINADAS HORAS Y LA MODAL DE "LLAMAR"
$mobileImagePostName='';

$fechaActivacion='20140306';  // DIA DE ACTIVACION PARA LA EXCEPCION

//$mobileArraDayException=array(2=>'',3=>'',4=>'',6=>'',7=>'');	// UTILIZAR ESTA LINEA SOLO EN CASO SE DEBAN DESHABILITAR LAS PROMOS "mobile"
$mobileArraDayException=array(2=>'_callme',3=>'_callme',4=>'_callme',6=>'_callme',7=>'_callme');
$mobileRangeTimeException=(date('H') >= 10 && date('H') <= 13) ? DATE('Ymd') : '';  // parte final del nombre de la clase para estar activada desde las 10 a las 14 horas (SOLO SI NECESARIO)
// ********* SOLO PARA PRUEBAS, DESCOMENTAR LA DE ARRIBA ***************
//$mobileRangeTimeException=(date('H') >= 10 && date('H') <= 14) ? $fechaActivacion : '';

/*
 * VARIABLES PARA CONTROLAR CUANDO HA DE APARECER LA VENTANA MODAL DE "LLAMAR"
 */
$allowDays=(date('w') >= 1 && date('w') <= 5) ? true : false; // SOLO DIAS LABORALES
$timeStartModal=1000; // HORA + MINUTO DE INICIO DE HABILITACION
$timeEndModal=2030;	// HORA + MINUTO FIN DE HABILITACION

$allowHours=(date('Hi') >= $timeStartModal && date('Hi') <= $timeEndModal) ? true : false; // HORARIO RESTRINGIDO, DESDE ... HASTA
$mobileRangeTimeException=($allowDays && $allowHours) ? $fechaActivacion : null;


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// MODAL DE "LLAMAR"
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if($layoutType == 'mob_' && array_key_exists($id_crea,$mobileArraDayException) && $mobileRangeTimeException)
  $mobileImagePostName=$mobileArraDayException[$id_crea];//'_'.$mobileRangeTimeException;
//////if(array_key_exists($id_crea,$mobileArraDayException) && $mobileRangeTimeException)
// END EXCEPCION PARA LA IMAGEN DE LA CABECERA PARA UNA FECHA Y UNAS DETERMINADAS HORAS

//echo '-->'.$path_raiz_includes;
//echo$path_raiz_includes_local;
include($path_raiz_includes_local . 'includes/header'.($arr_creas[$id_crea]['new_template'] ? '_tmplt' : '').'.php');
?>
<body class="landing">
<?php
//echo '<h2z>layoutType===>'.$arr_creas[$id_crea]['body'].'</h2>';
///echo '<hr>'.$arr_creas[$id_crea]['new_template'].' ==> '.$layoutType;
///echo $path_raiz_includes_local.'includes/body'.str_pad($arr_creas[$id_crea]['new_template'],2,'0',STR_PAD_LEFT).'.php';


//echo $layoutType.' -> '.$layoutType.$arr_creas[$id_crea]['body'];


if($arr_creas[$id_crea]['new_template'])
{
	include ($path_raiz_includes_local . 'includes/'.$arr_creas[$id_crea]['body']);
	include($path_raiz_includes_local . 'includes/cookies_footer.php');
//  include ($path_raiz_includes_local . 'includes/'.$arr_creas[$id_crea]['body']);
//  include($path_raiz_includes_local . 'includes/'.'cookies_footer'.$layoutType.'.php');
}
else
{
  include ($path_raiz_includes_local . 'includes/'.$layoutType.$arr_creas[$id_crea]['body']);
  include($path_raiz_includes_local . 'includes/'.$layoutType.'cookies_footer.php');
}




?>
</body>
</html>
