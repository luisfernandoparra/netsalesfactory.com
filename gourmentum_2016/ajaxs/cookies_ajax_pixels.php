<?php
/*
 * SCRIPT INVOCADO DESDE "includes/cookies_footer.php"
 * 
 * CARGA $arr_isSpecificPixel Y DEVUELVE LOS PIXELS HABILITADOS PARA SER CARGADOS
 * EN CASO DE QUE LA POLITICA DE COOKIES SEA ACEPTADA IMPLICITAMENTE/EXPLICITAMENTE
 * 
 * M.F. (19.11.2013)
 * 
 * @date 2016.09.26 Forzamos a que la cabecera de respuesta sea json
 */
header("Content-type: application/json; charset=utf-8");
include('../../conf/config_web.php');
ini_set('display_errors',$paintErrors);
include('../conf/config_web.php');
include($path_raiz_includes.'includes/initiate.php');
include($path_raiz_includes_local.'includes/local_vars.php');
$outJson=array();

if($_REQUEST['isCookie']) // SIEMPRE DEVOLVERA POSITIVO AL HABER SIDO HABILITADAS LAS COOKIES, NECESARIO PARA LANZAR EL PIXEL LOCAL
  $outJson['res']=1;

if(count($arr_isSpecificPixel) && $_REQUEST['isCookie'])
{
  if(isset($isSpecificPixel) && !empty($isSpecificPixel) && $isSpecificPixel === 1)
  {
		if(isset($arr_isSpecificPixel) && is_array($arr_isSpecificPixel) && $id_source != '' && is_numeric($id_source) && array_key_exists($id_source,$arr_isSpecificPixel))
		{
			$outJson['pixel'][]=$arr_isSpecificPixel[$id_source];	// DEVUELVE EL ARRAY CON LOS PIXELES DEFINIDOS EN "conf\config_web.php"
		}
  }
  
}

$res=json_encode($outJson);
die($res);
?>