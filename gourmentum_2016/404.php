<?php 
//echo "<pre>";
//print_r($_SERVER);
//echo "</pre>";

?>
<!doctype html>
<html>

<head>
<meta charset="utf-8">
<title>404 error page</title>
<link href="axa/css/404.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
var url_jumpto = "https://<?php echo $_SERVER['HTTP_HOST'];?>";


function redirect() {
    window.location.href = url_jumpto;
    return false;
}

//Redirigimos tras 4 segundos
//setTimeout(redirect(),10000);
</script>

</head>

<body>

	<header>
	<section class="title-head">
	<img class="logo" src="axa/img/404/logo.jpg" alt="AXA" style="cursor: pointer;" onclick="javascript:redirect();" />		
	</section>
	</header>

	<section class="content">
    <h1>404</h1>
    <h2>P&aacute;gina no encontrada</h2>
    <p>Por favor int&eacute;ntelo de nuevo</p>
    <img class="doc" src="axa/img/404/doc2.png" alt="404 error" style="cursor: pointer;" onclick="javascript:redirect();"/>
	</section>
    
    <footer>
    </footer>

<body>
</body>
</html>
