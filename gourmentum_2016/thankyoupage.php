<?php
/**
 * Página de gracias
 */
@session_start();
include('../conf/config_web.php');	// ONLY GLOBAL CONFIG
ini_set('display_errors',0);
include('conf/config_web.php');
include($path_raiz_includes . 'includes/initiate_thankyoupage.php');
include($path_raiz_includes . 'class/class.pixels.management.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>¡Gracias de <?php echo ucwords($landingMainName); ?>!</title>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<?php echo $path_raiz_aplicacion_local; ?>css/estilos_thanks.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- ************************* 
    PIXELES DE MAKE 
****************************** -->
<!-- Google Tag Manager  for MAKE -->
<script>
dataLayer = [{
	'pageCategory':'signup',
	'visitorType':'high-value',
	'transactionTotal':'1',
	'transactionId':'<?=$_REQUEST['leadNumber']?>',
	'template' : 'thankyoupage'
}];
</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TFTJXD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TFTJXD');</script>
<!-- End Google Tag Manager for MAKE -->

	<header></header>
  <section class="photo">
		<img src="<?php echo $path_raiz_aplicacion_local; ?>img/photo.jpg" width="638" height="700" />
	</section>
	<section class="text-content">
		<img src="<?php echo $path_raiz_aplicacion_local; ?>img/logo.png" width="240" height="85" />
		<article>
			<h2> Formulario enviado correctamente.</h2>
			<h3> En breves nos pondremos en contacto contigo.</h3>
			<h4>Gracias por confiar en nosotros.</h4>
		</article>
	</section>

<?php
if (!empty($_REQUEST['debug'])) {
    new dBug($_REQUEST);
}
include($path_raiz_includes . 'includes/pixels_thankyoupage.php');
session_destroy();
?>
</body>
</html>