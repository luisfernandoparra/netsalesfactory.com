<?php
@session_start();
// DEFINE COOKIES TEXT VARS 
$cookieCompanyNameFull='FINTEL MARKETING SLU';
$cookieCompanyNameShort='LA AGENCIA';
/*
 * LISTA DE LAS COOKIES UTILIZADAS EN ESTA PAGINA
 */
$arrThirdCookies=array('_ga'=>'Es necesaria para el funcionamiento de Google Analytics y tiene un período de caducidad de 2 años');

?>
<div class="conditions">
<head>


    <style type="text/css">
		p{
			margin:10px 0;
			padding:0;
            font-family: sans-serif;
            font-size:12px;
		}

        ul{
         margin:10px 0;
			padding:0;
            font-family: sans-serif;
            font-size:12px;
        }

        li{
            margin:10px 0;
			padding:0;
            font-family: sans-serif;
            font-size:12px;
        }

    </style>
</head>

<p style="text-align:justify"><strong>&iquest;QU&Eacute; SON LAS COOKIES?</strong><br />
Las&nbsp;<em>cookies&nbsp;</em>son peque&ntilde;os ficheros que permiten reconocer el navegador de un usuario, as&iacute; como el tipo de dispositivo desde el que se accede al sitio Web, y se utilizan para facilitar la pr&oacute;xima visita del usuario y hacer que el sitio Web resulte m&aacute;s &uacute;til.</p>

<p style="text-align:justify">La informaci&oacute;n recogida por las cookies y otros m&eacute;todos de registro es an&oacute;nima, por lo que no se recogen datos que puedan identificar personalmente al usuario.</p>

<p style="text-align:justify"><br />
<strong>&iquest;QU&Eacute; TIPOS DE COOKIES UTILIZAMOS?</strong></p>

<p style="text-align:justify">El sitio web:<a href="http://gourmentum.com/" target="_blank">&nbsp;<strong>www.gourmentum.com</strong></a>&nbsp;(En adelante, el Sitio Web) propiedad de CONTENT COMMERCE FACTORY S.L. (en adelante, CONTENT COMMERCE FACTORY), utilizan cookies, p&iacute;xeles u otros dispositivos de almacenamiento y recuperaci&oacute;n de informaci&oacute;n para realizar un seguimiento de las interacciones de los usuarios con los productos ofrecidos en el Sitio Web. Los tipos de cookies que utiliza el Sitio Web son los siguientes:</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong>Cookies propias</strong></p>

<p style="text-align:justify">Las cookies propias son aqu&eacute;llas que enviamos al ordenador o terminal del usuario desde nuestro Sitio Web. Estas cookies permiten que el Sitio Web funcione correctamente por lo que son esenciales para que el usuario pueda usar todas las opciones del Sitio Web y pueda moverse por el mismo con normalidad.</p>

<table border="1" cellpadding="10" cellspacing="3" style="background-color:#ffffff; border-collapse:collapse; border:1px solid #000000; color:#000000; font-family:arial; font-size:12px; width:60%">
	<tbody>
		<tr>
			<td style="text-align:justify">Empresa</td>
			<td style="text-align:justify">Cookie</td>
			<td style="text-align:justify">Finalidad</td>
			<td style="text-align:justify">Duraci&oacute;n</td>
			<td style="text-align:justify">M&aacute;s informaci&oacute;n</td>
		</tr>
		<tr>
			<td style="text-align:justify">Content Commerce Factory S.L</td>
			<td style="text-align:justify">PHPSESSID</td>
			<td style="text-align:justify">Sesion de usuario</td>
			<td style="text-align:justify">24 horas</td>
			<td style="text-align:justify">En la presente pol&iacute;tica de Cookies encontrar&aacute; toda la informaci&oacute;n sobre las cookies utilizadas por el Sitio Web</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong>Cookies de terceros</strong></p>

<p style="text-align:justify">Las cookies de terceros son aqu&eacute;llas que se env&iacute;an al ordenador o terminal de un usuario desde una p&aacute;gina web que no es gestionada por nosotros, sino por otra empresa que trata los datos obtenidos trav&eacute;s de las cookies. En concreto, las cookies de terceros utilizadas en el Sitio Web son:</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong><em>Cookies de anal&iacute;tica</em></strong>: Permiten realizar el seguimiento y an&aacute;lisis del comportamiento de los usuarios en el Sitio Web. La informaci&oacute;n recogida mediante las&nbsp;<em>cookies anal&iacute;ticas</em>&nbsp;es utilizada para la elaboraci&oacute;n de perfiles de navegaci&oacute;n de los usuarios de dicho Sitio Web, con el fin de introducir mejoras en nuestro servicio.</p>

<p style="text-align:justify">Los objetivos principales que se persiguen con este tipo de cookies son:</p>

<ul>
	<li style="text-align:justify">&nbsp; Permitir la identificaci&oacute;n an&oacute;nima de los Usuarios navegantes y por lo tanto la contabilizaci&oacute;n aproximada del n&uacute;mero de visitantes y su tendencia en el tiempo.</li>
	<li style="text-align:justify">&nbsp; Identificar de forma an&oacute;nima los contenidos m&aacute;s visitados y por lo tanto m&aacute;s atractivos para los Usuarios.</li>
	<li style="text-align:justify">&nbsp; Saber si el Usuario que est&aacute; accediendo es nuevo o repite visita.</li>
</ul>

<table border="1" cellpadding="10" cellspacing="3" style="background-color:#ffffff; border-collapse:collapse; border:1px solid #000000; color:#000000; font-family:arial; font-size:12px; width:60%">
	<tbody>
		<tr>
			<td style="text-align:justify">Empresa</td>
			<td style="text-align:justify">Cookie</td>
			<td style="text-align:justify">Finalidad</td>
			<td style="text-align:justify">Duraci&oacute;n</td>
			<td style="text-align:justify">M&aacute;s informaci&oacute;n</td>
		</tr>
		<tr>
			<td style="text-align:justify">Google</td>
			<td style="text-align:justify">__qa</td>
			<td style="text-align:justify">Necesaria para el funcionamiento de Google Analytics</td>
			<td style="text-align:justify">2 a&ntilde;os</td>
			<td style="text-align:justify">Google - Pol&iacute;tica de privacidad y Google Analytics - Uso de cookies</td>
		</tr>
		<tr>
			<td style="text-align:justify">Google</td>
			<td style="text-align:justify">__gat</td>
			<td style="text-align:justify">Es utilizada para realizar la ubicaci&oacute;n de la visita</td>
			<td style="text-align:justify">6 meses</td>
			<td style="text-align:justify">Google - Pol&iacute;tica de privacidad y Google Analytics - Uso de cookies</td>
		</tr>
		<tr>
			<td style="text-align:justify">Google</td>
			<td style="text-align:justify">_gat_UA57566291_1 y _gat_UA71103740_3</td>
			<td style="text-align:justify">Cuenta Analitycs</td>
			<td style="text-align:justify">2 d&iacute;as</td>
			<td style="text-align:justify">Google - Pol&iacute;tica de privacidad y Google Analytics - Uso de cookies</td>
		</tr>
		<tr>
			<td style="text-align:justify">Hotjar</td>
			<td style="text-align:justify">JSESSIONID</td>
			<td style="text-align:justify">Anal&iacute;tica. Moniterar la sesi&oacute;n del usuario</td>
			<td style="text-align:justify">Al finalizar la sesi&oacute;n web</td>
			<td style="text-align:justify">https://www.hotjar.com/cookies</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p style="text-align:justify"><strong><em>Cookies publicitarias</em></strong>: Permiten la gesti&oacute;n, de la forma m&aacute;s eficaz posible, de los espacios publicitarios que, el editor haya incluido en el Sitio Web, aplicaci&oacute;n o plataforma desde la que presta el servicio solicitado en base a criterios como el contenido editado o la frecuencia en la que se muestran los anuncios. Permiten ofrecer publicidad af&iacute;n a los intereses del Usuario.</p>

<table border="1" cellpadding="10" cellspacing="3" style="background-color:#ffffff; border-collapse:collapse; border:1px solid #000000; color:#000000; font-family:arial; font-size:12px; width:60%">
	<tbody>
		<tr>
			<td style="text-align:justify"><span style="font-size:12px">Empresa</span></td>
			<td style="text-align:justify"><span style="font-size:12px">Cookie</span></td>
			<td style="text-align:justify"><span style="font-size:12px">Finalidad</span></td>
			<td style="text-align:justify"><span style="font-size:12px">Duraci&oacute;n</span></td>
			<td style="text-align:justify"><span style="font-size:12px">M&aacute;s informaci&oacute;n</span></td>
		</tr>
		<tr>
			<td style="text-align:justify"><span style="font-size:12px">Google Adwords</span></td>
			<td>
			<p style="text-align:justify"><span style="font-size:12px">AID</span></p>
			</td>
			<td>
			<p style="text-align:justify"><span style="font-size:12px">Realizar seguimiento de clicks publicitarios</span></p>
			</td>
			<td style="text-align:justify"><span style="font-size:12px">30 d&iacute;as</span></td>
			<td>
			<p style="text-align:justify"><span style="font-size:12px"><a href="https://support.google.com/adwords/answer/2549063?hl=es" target="_blank">https://support.google.com/adwords/answer/2549063?hl=es</a>&nbsp;</span></p>
			</td>
		</tr>
		<tr>
			<td style="text-align:justify"><span style="font-size:12px">Facebook Ads</span></td>
			<td style="text-align:justify">fr</td>
			<td>
			<p style="text-align:justify"><span style="font-size:12px">Realizar seguimiento de clicks publicitarios</span></p>
			</td>
			<td style="text-align:justify"><span style="font-size:12px">30 d&iacute;as</span></td>
			<td>
			<p style="text-align:justify"><span style="font-size:12px"><a href="https://www.facebook.com/help/cookies" target="_blank">https://www.facebook.com/help/cookies</a>&nbsp;</span></p>
			</td>
		</tr>
		<tr>
			<td style="text-align:justify">Google</td>
			<td style="text-align:justify">id,IDE,_drt_,DSID</td>
			<td style="text-align:justify">Utilizada para propositos publicitarios</td>
			<td style="text-align:justify">2 a&ntilde;os</td>
			<td style="text-align:justify">Google - Pol&iacute;tica de privacidad y Google Analytics - Uso de cookies</td>
		</tr>
		<tr>
			<td style="text-align:justify">VeInteractive</td>
			<td style="text-align:justify">_ngtid,&nbsp;_ssid,&nbsp;ouuid, tuuid</td>
			<td style="text-align:justify">Utilizada para propositos publicitarios (re-targetting)</td>
			<td style="text-align:justify">12 meses</td>
			<td style="text-align:justify">https://www.veinteractive.com/es/productos/veads/</td>
		</tr>
		<tr>
			<td style="text-align:justify">VeInteractive</td>
			<td style="text-align:justify">88BB9554-14FE-43DF-A328-E600053F62716</td>
			<td style="text-align:justify">Utilizada para propositos publicitarios (re-targetting)</td>
			<td style="text-align:justify">2 d&iacute;as</td>
			<td style="text-align:justify">https://www.veinteractive.com/es/productos/veads/</td>
		</tr>
		<tr>
			<td style="text-align:justify">Nexusapp</td>
			<td style="text-align:justify">sess</td>
			<td style="text-align:justify">Utilizada para publicidad de display</td>
			<td style="text-align:justify">24 horas</td>
			<td style="text-align:justify">https://www.appnexus.com/en/company/platform-privacy-policy</td>
		</tr>
		<tr>
			<td style="text-align:justify">Nexusapp</td>
			<td style="text-align:justify">uuid2</td>
			<td style="text-align:justify">Utilizada para publicidad de display</td>
			<td style="text-align:justify">3 meses</td>
			<td style="text-align:justify">https://www.appnexus.com/en/company/platform-privacy-policy</td>
		</tr>
		<tr>
			<td style="text-align:justify">ASCIO TECHNOLOGIES</td>
			<td style="text-align:justify">_kui</td>
			<td style="text-align:justify">Publicidad y Marketing</td>
			<td style="text-align:justify">1 a&ntilde;o</td>
			<td style="text-align:justify">.match.adsby.bidtheatre.com</td>
		</tr>
		<tr>
			<td style="text-align:justify">ENOM, INC.</td>
			<td style="text-align:justify">tr10002808</td>
			<td style="text-align:justify">Anal&iacute;tica y tracking</td>
			<td style="text-align:justify">1 a&ntilde;o</td>
			<td style="text-align:justify">https://www.convertexperiments.com/</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><strong>C&Oacute;MO DESHABILITAR LAS COOKIES:</strong></p>

<p style="text-align:justify">La mayor&iacute;a de navegadores web permiten gestionar las preferencias de cookies, para tener un control m&aacute;s preciso sobre la privacidad.</p>

<p style="text-align:justify">Los siguientes links muestran las instrucciones para desactivar la configuraci&oacute;n de las cookies en cada navegador:<br />
<br />
<strong>1. Internet Explorer&nbsp;(<a href="http://goo.gl/vFM6gb" target="_blank">http://goo.gl/vFM6gb</a></strong>)</p>

<ul>
	<li style="text-align:justify">En el men&uacute; de herramientas, seleccione &ldquo;Opciones de Internet&rdquo;.</li>
	<li style="text-align:justify">Haga clic en la pesta&ntilde;a de privacidad.</li>
	<li style="text-align:justify">Podr&aacute; configurar la privacidad con un cursor con seis posiciones que le permite controlar la cantidad de cookies que se instalar&aacute;n: Bloquear todas las cookies, Alta, Media Alto, Media (nivel por defecto), Baja, y Aceptar todas las cookies.</li>
</ul>

<p style="text-align:justify"><strong>2.&nbsp;Mozilla Firefox (<a href="http://goo.gl/QXWYmv" target="_blank">http://goo.gl/QXWYmv</a>)</strong></p>

<ul>
	<li style="text-align:justify">En la parte superior de la ventana de Firefox hacer clic en el men&uacute;&nbsp;Herramientas.</li>
	<li style="text-align:justify">Seleccionar&nbsp;Opciones.</li>
	<li style="text-align:justify">Seleccionar el panel&nbsp;Privacidad.</li>
	<li style="text-align:justify">En la opci&oacute;n Firefox podr&aacute; elegir&nbsp;Usar una configuraci&oacute;n personalizada para el historial&nbsp;para configurar las opciones.</li>
</ul>

<p style="text-align:justify"><strong>3. Google Chrome&nbsp;(<a href="http://goo.gl/fQnkSB" target="_blank">http://goo.gl/fQnkSB</a></strong>)</p>

<ul>
	<li style="text-align:justify">Hacer clic en el men&uacute; situado en la barra de herramientas.</li>
	<li style="text-align:justify">Seleccionar Configuraci&oacute;n.</li>
	<li style="text-align:justify">Hacer clic en Mostar opciones avanzadas.</li>
	<li style="text-align:justify">En la secci&oacute;n &#39;Privacidad&#39; hacer clic en el bot&oacute;n Configuraci&oacute;n de contenido.</li>
	<li style="text-align:justify">En la secci&oacute;n de &#39;Cookies&#39; se pueden configurar las opciones.</li>
</ul>

<p style="text-align:justify"><strong>4. Safari&nbsp;(<a href="http://goo.gl/KFBFh" target="_blank">http://goo.gl/KFBFh</a></strong>)</p>

<ul>
	<li style="text-align:justify">En el men&uacute; de configuraci&oacute;n, seleccione la opci&oacute;n de &ldquo;preferencias&rdquo;.</li>
	<li style="text-align:justify">Abra la pesta&ntilde;a de privacidad.</li>
	<li style="text-align:justify">Seleccione la opci&oacute;n que quiera de la secci&oacute;n de &ldquo;bloquear cookies&rdquo;.</li>
	<li style="text-align:justify">Recuerde que ciertas funciones y la plena funcionalidad del Sitio pueden no estar disponibles despu&eacute;s de deshabilitar los cookies.</li>
</ul>

<p style="text-align:justify">Si desea no ser rastreado por las cookies, Google ha desarrollado un complemento para instalar en su navegador al que puede acceder en el siguiente enlace:&nbsp;<strong><a href="http://goo.gl/up4ND" target="_blank">http://goo.gl/up4ND</a></strong>.<br />
<br />
A partir de la opci&oacute;n que tome acerca del uso de cookies en el sitio Web, se le enviar&aacute; una cookie adicional para salvaguardar su elecci&oacute;n y que no tenga que aceptar el uso de cookies cada vez que acceda al sitio Web propiedad de CONTENT COMMERCE FACTORY.<br />
<br />
Tenga en cuenta que si elige bloquear las cookies esto puede afectar o impedir el funcionamiento del Sitio Web de CONTENT COMMERCE FACTORY.<br />
<br />
&nbsp;</p>

<p style="text-align:justify"><strong>COOKIES EN LOS DISPOSITIVOS M&Oacute;VILES:&nbsp;</strong></p>

<p style="text-align:justify">CONTENT COMMERCE FACTORY tambi&eacute;n usa cookies u otros dispositivos de almacenamiento en dispositivos m&oacute;viles. En este caso, al igual que sucede en los navegadores de ordenadores, los navegadores de los dispositivos m&oacute;viles permiten configurar las opciones o ajustes de privacidad para desactivar o eliminar las cookies.<br />
<br />
Si desea modificar las opciones de privacidad siga las instrucciones especificadas por el desarrollador de su navegador para dispositivo m&oacute;vil.<br />
<br />
Asimismo, a continuaci&oacute;n podr&aacute; encontrar algunos ejemplos de los links que le guiar&aacute;n para modificar las opciones de privacidad en su dispositivo m&oacute;vil:</p>

<ul>
	<li style="text-align:justify"><strong>IOS: (<a href="http://goo.gl/61xevS" target="_blank">http://goo.gl/61xevS</a></strong>)</li>
	<li style="text-align:justify"><strong>Windows Phone: (<a href="http://goo.gl/Rx8QQ" target="_blank">http://goo.gl/Rx8QQ</a></strong>)</li>
	<li style="text-align:justify"><strong>Chrome Mobile: (<a href="http://goo.gl/XJp7N" target="_blank">http://goo.gl/XJp7N</a></strong>)</li>
	<li style="text-align:justify"><strong>Opera Mobile: (<a href="http://goo.gl/Nzr8s7" target="_blank">http://goo.gl/Nzr8s7</a></strong>)</li>
</ul>
</div>