<?php
/**
 * Cron que recupera el archivo diario y envia a la dirección especificada
 * @version 1.0
 * @date 2016.11.16
 * @author MF
 */
header('Content-Type: text/html; charset=utf-8');
include('../../conf/config_web.php');
include('../conf/config_web.php');
$isTestSite=null;

/**
 * POR DEFECTO SE ENVIAN LAS DIRECCIONES DE "$transactionEmailDoctorSender"
 * EN CASO DE QUE EXISTA "$transactionEmailDoctorSenderSpecivicCrea[$id_crea]" SE UTILIZAN LOS EMAILS AHI CONTENIDOS
 * NOTA IMPORTANT: emails SEPARADOS POR COMAS SIN ESPACIOS
 */
//$transactionEmailDoctorSender='mario.francescutto@netsales.es,contacto@gourmentum.com';	//, contacto@gourmentum.com
//$transactionEmailDoctorSenderSpecivicCrea[152]='mario.francescutto@netsales.es';

if(isset($sitesPruebas) && $sitesPruebas)
	$isTestSite=' (desde Maravillao)';

//if(!isset($url_local))
//{
//	$fileLog = 'log_errores-'.date('Ymd').'.log';
//	$fp=@fopen($fileLog,'a') or die('Unable to open file!');
//	$error=fwrite($fp, "\r\nError en la carga de".$isTestSite.": ../conf/config_web.php\r\n");
//	fclose($fp);
//	die('Error en la carga de config_web');
//}

//if(!isset($defaultReceiver))
	$defaultReceiver='mario.francescutto@netsales.es'.($sitesPruebas ? '' : ',contacto@gourmentum.com');
//	$defaultReceiver='mario.francescutto@netsales.es,contacto@gourmentum.com';

$dateProcess=date('Ymd');
$dateProcessTxt=date('d-m-Y');
//$dateProcess='20161115';
$fileCsv = '../gourmentumfiles/cesta_navidad-'.$dateProcess.'.csv';

if(file_exists($fileCsv))
{
/**
 * NO:
 * <br /><a href="'.$url_local.'/'.$subFolderLanding.'/gourmentumfiles/cesta_navidad-'.$dateProcess.'.csv" target="_blank">descargar archivo CSV</a>
 */
	$htmlData='
<html>
<head></head>
<body>
<div style=font-family:arial,verdana;font-size:1em;color:#000;display:inline-block;width:99%;float:left;text-align:left;>
Tiene un archivo de datos generado desde <b>la landing de Gourmentum</b>, generado el día <b>'.$dateProcessTxt.'</b>.<br /><br />

Link para descargar el contenido en formato CSV:
<br /><a href="http://178.79.159.246/promociones/gourmentum_2016/gourmentumfiles/cesta_navidad-'.$dateProcess.'.csv" target="_blank">descargar archivo CSV</a>
<br /><br />Fin del mensaje.

</div>
</body>
</html>
';

	$arrData=array('user'=>'gourmentum-2016', 'data'=>array(
			array(
			'receiver'=>$defaultReceiver,
			'subject'=>'Datos registro landing Gourmentum'.$isTestSite,
			'html'=>$htmlData,
			'text'=>$htmlData
			)
		)
	);

	$fields_string = json_encode($arrData);

	$url = $url_ws . $file_ws;//. '?user='.$crea;
/*
echo '<hr>url= '.$url;
echo '<hr>$fields_string= '.$fields_string;
echo '<hr>$fileCsv= '.$fileCsv;
echo '<hr>$crea (USR)= '.$crea;
echo '<hr>';
die();
*/
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_POST, 1);
	curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, $timeout);
	$raw = curl_exec($ch);
	$statusWs = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
//echo '-->'.$raw;echo "\r\n".$statusWs;die();
	$resCurl=json_decode($raw, true);

// !!!!!!!!!!!!!!!!!!! activar SOLO para solo test !!!!!!!!!!!!!!!!!!!!
//$resCurl['correct']=0;

	if(isset($resCurl['correct']) && $resCurl['correct'] < 1 || !isset($resCurl['correct']))
	{
		$subject = 'Error en envio del correo de Gourmentum'.$isTestSite;
		$message = '<h1>Error en el cron diario del envío de datos de Gourmentum</h1><br />El archivo que se ha generado es:<br /><span style=color:blue;font-weight:bold;>'.$fileCsv.'</span>.';
		$message .= '<br /><br />Fecha del intento:<br /><span style=color:red;font-weight:bold;>'.(date('d-m-Y / H:i:s')).'</span>.';
		$message .= '<br /><br />Link a enviar:<br /><a href="http://178.79.159.246/promociones/gourmentum_2016/gourmentumfiles/cesta_navidad-'.$dateProcess.'.csv" target="_blank">descargar archivo CSV</a>';
		$message .= '<br />Error devuelto: '.$raw;
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: webservicenetsales@netsales.netsales.es' . "\r\n" .
		'Reply-To: webservicenetsales@netsales.netsales.es' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();
		$emailsSend='mario.francescutto@netsales.es,luisfer.parra@netsales.es';
		@mail($emailsSend, $subject, $message, $headers);


		$message=str_replace('<br />', "\r\n", $message);
		$message=  strip_tags($message);

		$fileLog = 'log_errores-'.$dateProcess.'.log';
		$fp=@fopen($fileLog,'a') or die('Unable to open file!');
		$error=fwrite($fp, $message."\r\n-------------\r\n");
		fclose($fp);

		die('finalizado con error');
	}

//echo $resCurl['correct'].') <pre> WS devuelve='.($raw);

	$resCurl['error']=$error;

}

die();