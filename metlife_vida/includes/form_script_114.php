<?php
// SIN FORMULARIO STANDARD
?>
<div class="fila">
	<div class="fleft">
		<input type="text" name="nombre" id="nombre" value="" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre y Apellidos&lt;/strong&gt; es obligatorio" placeholder="Nombre y Apellidos" aria-required="true">
	</div>
</div>

<div class="fila">
	<div class="fleft">
		<input name="telefono" id="telefono" type="tel" class="celda" maxlength="9" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Teléfono" aria-required="true">
	</div>
</div>

<div class="row">
	<div class="col8">
		<input type="email" name="email" id="email" maxlength="100" class="celda" required="" data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="" placeholder="E-mail" aria-required="true">
	</div>
</div>

<div class="legal">
    <input required="" data-msg-required="Debe leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debe leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;Acepto la <a href="privacy_policy.php?cr=<?=$id_crea?>#privacy" class="smooth privacyBottom" target="_blank">política de privacidad</a>
</div>

<div class="row">
	<div class="espacio_btn">
		<input id="btnProcesar" name="registrate" type="button" value="¡Informadme!" class="button green sendData" data-direction="down">
	</div>
</div>

<div class="clearfix"></div>
