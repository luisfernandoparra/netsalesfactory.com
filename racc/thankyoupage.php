<?php
/**
 * Página de gracias
 */
@session_start();
include('../conf/config_web.php');
ini_set('display_errors',0);
include('conf/config_web.php');
include($path_raiz_includes . 'includes/initiate_thankyoupage.php');
include($path_raiz_includes . 'class/class.pixels.management.php');
//Debemos redirigir al usuario
//Entrada: 200002837=>Externo 200002698=>Interno
//Salida: 200003086=>Externo 200003085=>Interno
$nch_proc = (!empty($_REQUEST['nch_proc'])) ? trim($_REQUEST['nch_proc']) : '200002698';
$nch_proc_jump = ($nch_proc=='200002837') ? 200003086 : 200003085;
$url_jump_to = 'https://www.racc.es/proceso/presupuesto-coche/paso1.html?request_locale=es&utm_source=netsales&utm_medium=email&utm_campaign=bbdd_terceros&nch_proc='.$nch_proc_jump;
?>
<!doctype html>
<html lang="es">
<head>
<title>¡Gracias desde <?php echo ucwords($landingMainName); ?>!</title>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width,initial-scale=1"/> 
<link href="<?php echo $path_raiz_aplicacion_local; ?>css/estilos_thanks.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- ************************* 
    PIXELES DE MAKE 
****************************** -->
<!-- Google Tag Manager  for MAKE -->
<script>
dataLayer = [{
	'pageCategory':'signup',
	'visitorType':'high-value',
	'transactionTotal': 1,
	'transactionId':'<?=$_REQUEST['leadNumber']?>',
	'template' : 'thankyoupage'
}];
</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TFTJXD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TFTJXD');</script>
<!-- End Google Tag Manager for MAKE -->

	<section class="contenido">
		<img src="<?php echo $path_raiz_aplicacion_local; ?>img/thankyou.jpg"/>
	</section>

	<footer>
		<div class="texto">
			Cuando el alta sea efectiva se te abonarán 4.225 puntos que podrás canjear por este regalo o por otros regalos o descuentos del Programa de Puntos RACC. Este contenido es informativo y no tiene valor contractual. Las coberturas y las exclusiones son las que figuran en las prestaciones del carnet de socio del RACC. Derechos de imagen con licencia de Creative Commons.<br /><br />
<span>© Copyright 2015 RACC Automóvil Club</span>
		</div>
</footer>

<?php
if (!empty($_REQUEST['debug'])) {
    new dBug($_REQUEST);
}
 
include($path_raiz_includes . 'includes/pixels_thankyoupage.php');
session_destroy();
?>
<script>
    window.setTimeout(function(){window.location.href = "<?php echo $url_jump_to;?>";}, 8000);</script>
</body>
</html>