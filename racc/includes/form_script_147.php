<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);

?>
					<div id="campos">
						<div class="error"><ul></ul></div>
						<p>¡Solicita información!<br /><span class="for-text">SIN COMPROMISO</span>

						<div class="fila">
							<div class="fleft">
								<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda user" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" placeholder="Nombre" aria-required="true" />
							</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<input type="text" name="apellidos" id="apellidos" value="<?=$apellidos?>" class="celda book" maxlength="100" required data-msg-required="El campo &lt;strong&gt;1° Apellido&lt;/strong&gt; es obligatorio" placeholder="1° Apellido" aria-required="true" />
							</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<input type="text" name="apellido2" id="apellido2" value="<?=$apellido2?>" class="celda book" maxlength="200" placeholder="2° Apellido" />
							</div>
						</div>

						<div class="fila">
							<div class="col8">
								<input type="tel" maxlength="9" name="telefono" id="telefono" class="celda phone" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Teléfono" aria-required="true" />
							</div>
						</div>

						<div class="row">
							<div class="col8">
								<input type="email" name="email" id="email" maxlength="100" class="celda mail" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="Email" aria-required="true" />
							</div>
						</div>


						<div class="legal" style="padding-top:10px;">
							<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;Acepto la
							<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'privacy_policies.php';?>" data-ref="" target="_blank">política de privacidad</a>.
						</div><!--/12-->

						<div class="row">
							<div class="espacio_btn">
								<input class="button green sendData" type="button" id="btnProcesar" name="btnProcesar" value="¡Infórmate!">
							</div>
						</div>

						<div class="clearfix"></div>
					</div>
					<!--/#campos-->
