$.ajaxSetup ({
    cache: false
});
/* Variables necesarias */
  msg ='';
 clicked = false;

 checkedPhone = 0;
 check_tel='';
 maxPhoneCheck = 2;

 checkedEml = 0;
 check_eml='';
 maxEmlCheck = 2;

contador_telf = 0;

 cpf  = '';
 cep1 = '';
 cep2 = '';
 cep  = '';
 exp  = '';
 emailError = 0;
 
 cep_invalido = true;

	/* Textos de alerta */
	 txtCPFInvalido =			'Seu CPF é inválido.';
	 txtCPFIncompleto =			'Você não preencheu seu CPF';
	 txtCEPInvalido =			'O CEP é inválido.';
	 txtCEPIncompleto =			'Seu CEP é incompleto.';

	 txtCPF1 =					'Você deve preencher o primeiro campo de CPF.';
	 txtCPF2 =					'Você deve preencher o segundo campo de CPF.';
	 txtCPF3 =					'Você deve preencher o terceiro campo de CPF.';
	 txtCPF4 =					'Você deve preencher o quarto campo de CPF.';

	 txtGraciasPart = 			'Obrigado por participar';
	 txtValidacion = 			'Em breve receberá um e-mail. Clique no link para confirmar a sua participação.<br /><br /> Desejamos-lhe muita sorte.<br /><br /> Obrigado por participar';
	 txtBbddEml =				'Este e-mail já existe no banco de dados';
	 txtBbddTel =				'Este telefone já existe no banco de dados.';
	 txtBbddEmlTel =			'Este e-mail e telefone já existem no banco de dados.';

	 txtObl = 					'É obrigatoria';

	 txtTrat = 					'Você não selecionou seu tratamento.';
	 txtNombre = 				'Você não preencheu seu nome.';
	 txtNombreDosCar = 			'O nome deve conter pelo menos 2 caracteres.';
	 txtNombreCaracteres = 		'Foram inseridos caracteres inválidos no nome.';
	 txtPrimAp = 				'Você não preencheu seus sobrenomes.';
	 txtPrimApDosCar = 			'O primeiro nome deve conter pelo menos dois caracteres.';
	 txtPrimApCaracteres = 		'Foram inseridos caracteres inválidos no sobrenome.';

	 txtEdad = 					'Participação somente para maiores 18 anos.';
	 txtEmail = 				'Debes insertar el Email.';
	 txtEmailFormato = 			'O formato de e-mail está errado.';
	 txtMovil = 				'Você não preencheu o numero de seu celular.';
	 txtMovilFormato = 			'Foi inserido um formato de celular incorreto.';
	 txtMovilFormatoArea = 	    'O DDD está errado.';
	 txtTelNoExiste = 			'Este número de telefone não existe.';
	 txtEmlNoExiste =			'Este E-mail não existe.';
	 txtEmlDesconocido =		'Este E-mail não é correto.';

	 txtConfBases = 			'Você não confirmou as bases legais.';
	 txtProvincia = 			'Você deve selecionar o estado.';
	 txtProvinciaCp = 			'O CEP não corresponde com o estado';
	 txtLocalidad =				'Você deve selecionar o localidade';

/* Vamos con el meollo */
$(document).ready(function(){
  
  $("body").append("<div id='opaque' style='display: none;'></div>");
  
	$('#telf1').keypress(function(e){ return SoloCompatibleTlf(e);});
	$('#telf2').keypress(function(e){ return SoloCompatibleTlf(e);});
	$('#cpDir').keypress(function(e){ return SoloCompatibleTlf(e);});
	
	
	/* Función para saltar al siguiente campo de CPF */
        $("#cpf1").keyup(function() {
			var longCampo = $("#cpf1").val().length;
			if (longCampo >= 3) {
				document.getElementById("cpf2").focus();
			}
		});
         $("#cpf2").keyup(function() {
			  var longCampo = $("#cpf2").val().length;
			  if (longCampo >= 3) {
				  document.getElementById("cpf3").focus();
			  }
		  });
         $("#cpf3").keyup(function() {
			  var longCampo = $("#cpf3").val().length;
			  if (longCampo >= 3) {
				  document.getElementById("cpf4").focus();
			  }
		  });
         $("#cpf4").keyup(function() {
			  var longCampo = $("#cpf4").val().length;
			  if (longCampo >= 2) {
				  document.getElementById("dia_fech").focus();
			  }
		  });
		  
        /* Función para pasar al siguiente campo de CEP. */
         $("#cep1").keyup(function() {
			  var longCampo = $("#cep1").val().length;
			  if (longCampo >= 5) {
				  document.getElementById("cep2").focus();
			  }
		  });
        
         $("#cep2").keyup(function() {
			  var longCampo = $("#cep2").val().length;
			  if (longCampo >= 3) {
				  document.getElementById("cbLopd").focus();
			  }
		  });
		  
		  /* Función para comprobar el CEP. */
		  $("#cep1,#cep2").blur(function() {

				var cep1;
				var cep2;
				cep1 = $("#cep1").val();
				cep2 = $("#cep2").val();

				if ((cep1.length==5) && (cep2.length==3)){
					cep = cep1+"-"+cep2;

					$.ajaxSetup({async: false});
					$.post(root_path+"abril-group/ajax/ajax_comprobar_cep.php",{mi_cep:cep},
						function(data) {
							if (data.status == "0") {
								msg += "<div>- "+txtCEPInvalido+"</div>";
								error = true;
								cep_invalido = true;
								 $("#provinciaDir option[value='']").attr("selected",true).change();
								 $("#poblacionDir option[value='']").attr("selected",true).change();
							}else{  

							  $("#provinciaDir option[value="+data.id_uf+"]").attr("selected",true).change();
							  $("#poblacionDir option[value="+data.id_cidade+"]").attr("selected",true);
							  cep_invalido = false;
							}
						},'JSON');
					$.ajaxSetup({async: true});
				}	
			});
	
  /* Comprobando formulario */
  
  $('#btnProcesar1').click(function(e){
		e.preventDefault();
		if (!clicked) {
			//$.facebox.loading();
			clicked =  true;
			var tratamiento = '';
			
			tratamiento = $("[name=tratamiento]:checked").val();
			tratamiento = (typeof tratamiento !== 'undefined')?tratamiento : '';
			 
			var nombre = $('#nombre').val();
			var apellido1 = $('#apellidos1').val();
			
			var dia_fech = $('#dia_fech').val();
			var mes_fech = $('#mes_fech').val();
			var ano_fech = $('#ano_fech').val();
			
			if (dia_fech.length < 2) {dia_fech = "0"+dia_fech;}
			if (mes_fech.length < 2) {mes_fech = "0"+mes_fech;}
			
			var email = $('#email').val();
			var telf = $('#telf1').val()+$('#telf2').val();
			var telf1 = $('#telf1').val();
			var telf2 = $('#telf2').val();
			var email_chungo = "";
			
			var cep1 = $('#cep1').val();
			var cep2 = $('#cep2').val();
			
			var cpf1 = $('#cpf1').val();
			var cpf2 = $('#cpf2').val();
			var cpf3 = $('#cpf3').val();
			var cpf4 = parseInt($('#cpf4').val());
			
			var poblacion 	= $('#poblacionDir').val();
			var provincia 	= $('#provinciaDir').val();
			
			var cbLegal = ($("#cbLopd").is(':checked')) ? $("#cbLopd").val():'0';
			var f = dia_fech +"-"+mes_fech+"-"+ano_fech;
			var error = false;
			var msg='<div id="mensajitos" style=" background-color: #FFFFFF; margin: 10px; padding: 10px;">';
			
			if (tratamiento.length<=0) { msg += "<div>- "+txtTrat+"</div>";error = true;$('#sptratamiento').css('display','inline').fadeIn("slow");}
			if (nombre.length<=0) {msg += "<div>- "+txtNombre+"</div>";error = true;$('#spNombre').css('display','inline').fadeIn("slow");}
			else if (nombre.length<2) {msg += "<div>- "+txtNombreDosCar+"</div>";error = true;$('#spNombre').css('display','inline').fadeIn("slow");}
			else if (!(comprobarExprRegular(regexOnlyLetters,nombre))) {msg += "<div>- "+txtNombreCaracteres+"</div>";error = true; $('#spNombre').css('display','inline').fadeIn("slow");}
			else if (nombre == '') {
				msg += "<div>- "+txtNombre+"</div>";error = true; $('#spNombre').css('display','inline').fadeIn("slow");
			}
			if (apellido1.length<=0) {msg += "<div>- "+txtPrimAp+"</div>";error = true; $('#spapellidos1').css('display','inline').fadeIn("slow");}
			else if (apellido1.length<2) {msg += "<div>- "+txtPrimApDosCar+"</div>";error = true; $('#spapellidos1').css('display','inline').fadeIn("slow");}
			else if (!(comprobarExprRegular(regexOnlyLetters,apellido1))) {msg += "<div>- "+txtPrimApCaracteres+"</div>";error = true; $('#spapellidos1').css('display','inline').fadeIn("slow");}
			else if (apellido1 == 'Seus sobrenomes') {
				msg += "<div>- "+txtPrimAp+"</div>";error = true; $('#spapellidos1').css('display','inline').fadeIn("slow");
			}
			
			if (telf == '') {
				msg += "<div>- "+txtMovil+"</div>";error = true; } 
			else if (telf1=="") {msg += "<div>- "+txtMovil+"</div>";error = true; } 	
			else if (telf2=="") {msg += "<div>- "+txtMovil+"</div>";error = true; }
			else if (!(comprobarExprRegular(regexPrefArea,telf1))){msg += "<div>- "+txtMovilFormatoArea+"</div>";error = true;} 
			else if (telf2.length<='8'){
				if (!(comprobarExprRegular(regexTelfOcho,telf2))){msg += "<div>- "+txtMovilFormato+"</div>";error = true;}
			}else if (telf2.length=='9'){
				if (!(comprobarExprRegular(regexTelfNueve,telf2))){msg += "<div>- "+txtMovilFormato+"</div>";error = true;}
			}
			
//			if ((telf2.length<8) || (telf1.length < 2)){
//				{msg += "<div>- "+txtMovilFormato+"</div>";error = true;}
//			}
			
			
			//Compruebo que he rellenado el CPF 
			if (cpf1.length<=0) {msg += "<div>- "+txtCPF1+"</div>";error = true; $('#cpf1').css('display','inline').fadeIn("slow");}
			else if (cpf1.length<3) {msg += "<div>- "+txtCPFIncompleto+"</div>";error = true; $('#cpf1').css('display','inline').fadeIn("slow");}
			else if (cpf1 == 'cpf1') {
				msg += "<div>- "+txtCPF1+"</div>";error = true; $('#cpf1').css('display','inline').fadeIn("slow");
			}
			
			if (cpf2.length<=0) {msg += "<div>- "+txtCPF2+"</div>";error = true; $('#cpf2').css('display','inline').fadeIn("slow");}
			else if (cpf2.length<3) {msg += "<div>- "+txtCPFIncompleto+"</div>";error = true; $('#cpf2').css('display','inline').fadeIn("slow");}
			else if (cpf2 == 'cpf2') {
				msg += "<div>- "+txtCPF2+"</div>";error = true; $('#cpf2').css('display','inline').fadeIn("slow");
			}
			
			if (cpf3.length<=0) {msg += "<div>- "+txtCPF3+"</div>";error = true; $('#cpf3').css('display','inline').fadeIn("slow");}
			else if (cpf3.length<3) {msg += "<div>- "+txtCPFIncompleto+"</div>";error = true; $('#cpf3').css('display','inline').fadeIn("slow");}
			else if (cpf3 == 'cpf3') {
				msg += "<div>- "+txtCPF3+"</div>";error = true; $('#cpf3').css('display','inline').fadeIn("slow");
			}
			
			if (cpf4.length<=0) {msg += "<div>- "+txtCPF4+"</div>";error = true; $('#cpf4').css('display','inline').fadeIn("slow");}
			else if (cpf4.length<2) {msg += "<div>- "+txtCPFIncompleto+"</div>";error = true; $('#cpf4').css('display','inline').fadeIn("slow");}
			else if (cpf4 == 'cpf4') {
				msg += "<div>- "+txtCPF4+"</div>";error = true; $('#cpf4').css('display','inline').fadeIn("slow");
			}
			
			//Chequeo de CPF
			//Reuno todas las casillas de cpf en una sola variable
			cpf =cpf1+cpf2+cpf3+cpf4;
			//Descarto a los sospechosos habituales
			if (cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
				msg += "<div>- "+txtCPFIncompleto+"</div>";
				error = true;
			}else {
				//Valido el cpf
				exp = /\.|\-/g     
					cpf = cpf.toString().replace( exp, "" );     
					var digitoDigitado = eval(cpf.charAt(9)+cpf.charAt(10));     
					var soma1=0, soma2=0;     
					var vlr =11;     
					for(i=0;i<9;i++){         
						soma1+=eval(cpf.charAt(i)*(vlr-1));         
						soma2+=eval(cpf.charAt(i)*vlr);         
						vlr--;     
					}        
					soma1 = (((soma1*10)%11)==10 ? 0:((soma1*10)%11));     
					soma2=(((soma2+(2*soma1))*10)%11);     
					var digitoGerado=(soma1*10)+soma2;     
					if(digitoGerado!=digitoDigitado){
						msg += "<div>- "+txtCPFInvalido+"</div>";
						error = true;
					}
			}
			    	
		//Fin del chequeo del CPF
			
			
			if  (! __fecha_valida(dia_fech,mes_fech,ano_fech))  {msg += "<div>- "+txtFechaFormato+"</div>";error = true;$('#ano_fech').focus();$('#spano_fech').css('display','inline').fadeIn("slow");}
			else if (__calcular_edad(f) <18){msg += "<div>- "+txtEdad+"</div>";error = true;$('#ano_fech').focus();$('#spano_fech').css('display','inline').fadeIn("slow");}
			if (email.length<=0) {msg += "<div>- "+txtEmail+"</div>";error = true; $('#spemail').css('display','inline').fadeIn("slow");} 	
			else if (!(comprobarExprRegular(regexEmail,email))){msg += "<div>- "+txtEmailFormato+"</div>";error = true; $('#spemail').css('display','inline').fadeIn("slow");email_chungo='1';} 
			else if (email == '') 
			{msg += "<div>- "+txtEmail+"</div>";error = true; emailError=1;} 	
			
			

			//else {
				var emlExiste = 0;
				my_id_check_eml='';
				if ((emlExiste == 0) && (emailError==0)) {
					if(checkedEml < maxEmlCheck){
						if (email_chungo != '1'){
							$.ajaxSetup({async: false});
							//$.post(root_path+"ajax/ajax_comprobar_email.php",{eml:email,el_sorteo:sorteo},
							$.post(root_path+"abril-group/ajax/ajax_comprobar_email.php",{eml:email},
									function(data) {
									if (data.status == "2") {
												msg += "<div>- "+txtEmlNoExiste+"</div>";
												checkedEml++;
												error = true;
											 }  else if (data.status == "1"){
												 check_eml = data.emailAddressChecked;
												 checkedEml = maxEmlCheck;
											 } else if (data.status == "3"){
												 msg += "<div>- "+txtEmlDesconocido+"</div>";
												checkedEml++;
												error = true;
											 }
											 
												if (data.el_id_check_eml != ""){
													my_id_check_eml = data.el_id_check_eml;
												}
												
											 if (checkedEml == maxEmlCheck) {
												 $('#email').attr('readonly', true);
											 }
									},'JSON');
							$.ajaxSetup({async: true});
						}//if (email_chungo != '1'){	
					}//if(checkedEml < maxEmlCheck)
				}//if (emlExiste == 0)
				//Fin de chequeando el email
			//}

			
		/* Validación de CEP */
					cep = cep1+"-"+cep2;
				if (cep_invalido){
					$.ajaxSetup({async: false});
					$.post(root_path+"abril-group/ajax/ajax_comprobar_cep.php",{mi_cep:cep},
							function(data) {
								if (data.status == "0") {
									msg += "<div>- "+txtCEPInvalido+"</div>";
									error = true;
									 $("#provinciaDir option[value='']").attr("selected",true).change();
									$("#poblacionDir option[value='']").attr("selected",true).change();
								}  
								$("#provinciaDir option[value="+data.id_uf+"]").attr("selected",true).change();
								$("#poblacionDir option[value="+data.id_cidade+"]").attr("selected",true);
							},'JSON');
					
					if((cep1.length<5) && (cep2.length<3)){
						msg += "<div>- "+txtCEPIncompleto+"</div>";
						error = true;
					}
					$.ajaxSetup({async: true});
				}	
		//Fin validación de CEP
					
					if (provincia.length <= 0) {msg += "<div>- "+txtProvincia+"</div>";error = true;$('#provinciaDir').focus();}		
					if (poblacion.length <= 0) {msg += "<div>- "+txtLocalidad+"</div>";error = true;$('#poblacionDir').focus();}
					
		
		if (cbLegal=='0') {msg += "<div>- "+txtConfBases+"</div>";error = true;$('#cbLopd').focus();$('#spcbLopd').css('display','inline').fadeIn("slow");} 
		
		/* Muestro los errores en una ventana modal. */
			if (error) {
			  msg +='</div>';
				clicked=false;
			}else{
			  msg = '<div id="mensajitos" style=" background-color: #FFFFFF; margin: 10px; padding: 10px;">Todos los datos del formulario están validados y son correctos. Enhorabuena!</div>';
			} 
			$(msg).modal({
				  position: ["15%",],
				  overlayId: 'contact-overlay',
				  containerId: 'mensajitos',
				  width:700
			});
			return false;
		} //if clicked
	});	
});

//Comprobacíon del primer paso
function comprobarFaseUna(dat) {
	data = eval ("("+dat+")");
	clicked=false; 	
	if (data.error == "0") {
		$("#formID").submit();
	} else {	
		if (data.error == 0) {Terminar(mensError);}
		else {$.facebox(data.mensaje);}
	}
}

//Reemplazar acentos
function reemplazarAcentos(cadena) {
	cadena = cadena.replace	('á','a');
	cadena = cadena.replace	('é','e');
	cadena = cadena.replace	('í','i');
	cadena = cadena.replace	('ó','o');
	cadena = cadena.replace	('ú','u');
	return cadena;
}

//Comprobación de expresiones regulares
function comprobarExprRegular(regla,valor) {
	var pattern = new RegExp(regla);
	return pattern.test(valor);
}

function cargarPoblacion(prov,ponernombre) {
	ponernombre = typeof ponernombre !== 'undefined' ? 1 : 0;
	//$.facebox.loading();
	$.post(root_path+"abril-group/ajax/ajax_poblacion.php",{provincia:prov,acc:"1"},function(data){comprobarPoblacion(data,ponernombre)},'');
}
function comprobarPoblacion(dat,ponernombre) {
	var data = eval("("+dat+")");

	var i= 0;
	$("#poblacionDir").empty();
	$("#poblacionDir").append('<option value="">cidade</option>');
	if (data.error =="0") {
		var dat = eval("("+data.data+")");
		for (i=0;i<dat.length;i++) {
			$("#poblacionDir").append('<option value="'+dat[i].id_municipio+'">'+dat[i].municipio+'</option>');
		}
	}
		//$.facebox.close();
}

/* Expresiones regulares generales */
var regexOnlyLetters =  /^[áéíóúÁÉÍÓÚñÑÄËÏÖÜäëïöàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛçÇa-zA-Z\ \']+$/;
var regexEmail =  /^([A-Za-z0-9_\-\.\'])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,6})$/;
var regexOnlyLettersAndNumbers =  /^[áéíóúÁÉÍÓÚñÑÄËÏÖÜäëïöàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛçÇa-zA-Z0-9\.\ \']+$/;

/* Expresiones regulares para el telefono brasileño */
var regexPrefArea = /^[0-9][0-9]/;
var regexTelfOcho = /^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/;
var regexTelfNueve = /^[9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/;