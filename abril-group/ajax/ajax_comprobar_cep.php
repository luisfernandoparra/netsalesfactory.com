<?php
include("../../conf/config_web.php");
include("../conf/config_web.php");
ini_set('display_errors', '1');
//Recojo el cep aportado por el usuario
$mi_cep = trim($_REQUEST['mi_cep']);

//Inicializo las variables
$error = '0';
$id_status='0';
$cidade = '';
$logradouro = '';
$bairro = '';
$cep = '';
$tp_logradouro = '';
$uf = '';
$estado = '';
$id_uf ='';
$id_cidade ='';
$tp_select_logradouro = '';

//$mi_cep="20530-350";

//Cambio el nombre de la bbdd
//$db_name = "prueba_brasil";
//Defino la conexión
$conexion  = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name,$debug_pagina);

//Compruebo que la conexión es válida
if($conexion->connectDB()){
	//Compruebo que ha llegao el cep del usuario
	if(!empty($mi_cep)){
		//Hago una búsqueda en datos totales
		$sql_check = "SELECT cidade,logradouro,bairro,cep,tp_logradouro,uf,id_tipo_logradouro 
					FROM datos_totales
					WHERE
					cep='".$mi_cep."' 
					ORDER BY id DESC
					LIMIT 0 , 1";
		$conexion ->getResultSelectArray($sql_check);
		$conexion_cep = $conexion->tResultadoQuery;
		//Si mi búsqueda no ha generado ningún registro, el cep es inválido
		if(count($conexion_cep) <= 0) {
			$id_status = '0';
			$cidade = "";
			$logradouro = "";
			$bairro = "";
			$cep = "";
			$tp_logradouro = "";
			$uf = "";
			$estado="";
			
		//El cep aportado por el usuario es válido y tenemos todos los datos. Status a '1' y recojo toda la info.	
		}else{
			$cidade = $conexion_cep[0]['cidade'];
			$logradouro = $conexion_cep[0]['logradouro'];
			$bairro = $conexion_cep[0]['bairro'];
			$cep = $conexion_cep[0]['cep'];
			$tp_logradouro = $conexion_cep[0]['tp_logradouro'];
			$uf = $conexion_cep[0]['uf'];
			$id_status = '1';
			$tp_select_logradouro = $conexion_cep[0]['id_tipo_logradouro'];
		}
	}
	
	//Pongo el nombre completo del estado
	if ($id_status=='1'){
		$sql_estado = "SELECT Nome as estado,id_uf as id_uf  
					FROM uf
					WHERE
					UF  LIKE  '".$uf."'
					ORDER BY UF DESC
					LIMIT 0 , 1";
		
	
		$conexion ->getResultSelectArray($sql_estado);
		$conexion_estado = $conexion->tResultadoQuery;

	//	new dBug($conexion_estado); 	
	//	echo '<br>'.$sql_estado.count($conexion_estado).'<br>';
		
		if(count($conexion_estado) > 0) {
			$estado = $conexion_estado[0]['estado'];
			$id_uf  = $conexion_estado[0]['id_uf'];
		}
		
		
		if ($id_uf !=""){
			//echo 'Llego';
			$sql_cidade = "select id_cidade from cidades WHERE cidade LIKE '".$cidade."' order by id_cidade ";
			$conexion ->getResultSelectArray($sql_cidade);
			$conexion_cidade = $conexion->tResultadoQuery;
		//	echo "sql_cidade: ".$sql_cidade;
			if(count($conexion_cidade) > 0) {
				
				$id_cidade  = $conexion_cidade[0]['id_cidade'];
			}
		}
		
		
	}
	//echo "Select logradouro: ".$tp_select_logradouro;
	
	$conexion->disconnectDB();
	//Devuelvo la información al Jquery.
	echo ejecutasegunversion("JSON",array("status"=>$id_status,"cidade"=>$cidade,"logradouro"=>$logradouro,"bairro"=>$bairro,"cep"=>$cep,"tp_logradouro"=>$tp_logradouro,"uf"=>$uf,"estado"=>$estado,"id_uf"=>$id_uf,"id_cidade"=>$id_cidade,"tp_select_logradouro"=>$tp_select_logradouro));
}
?>
