<?php 
class TelephoneCheck{
	var $usuario;
	var $password;
	var $usr_correcto;
	var $result;
	var $company;
	var $url;
	var $usr_con;
	var $pwd_con;
	var $previous_months = 2;
	//private $arr_operators = array();
	
	public function telf_correcto($telefono) {
		return (!empty($telefono) && strlen($telefono)==9 && $telefono[0]==6);	
		
	}
	
	private function operators($id_op) {
		//var $output = '';
		$found = false;
		$cont = 0;
		$items = count($this->arr_operators);
		if (!empty($id_op) && is_numeric($id_op) && $id_op!=0 && $items>0){
			
			while (!$found && $cont<$items) {
				$tmp = $this->arr_operators[$cont]['id_operator'];
				if ($tmp == $id_op) {
					$output = $this->arr_operators[$cont]['operator'];
					$found = true;
					
				}
			} //while
										  
			
		} //if
		return $output;
		
	}
	public function TelephoneCheck($url,$usr_con,$pwd_con,$usuario,$password) { //constructor
		//En un futuro, se utilizara usuario y password para vender el producto
		$this->usuario = '';
		$this->password = '';
		$this->usr_correcto = false;
		$this->result = "KO";
		$this->company = '';
		$this->url = $url;
		$this->usr_con = $usr_con;
		$this->pwd_con = $pwd_con;
		//echo $usr_con." ".$pwd_con;
		if (trim($usuario) != '' and trim($password!='') and $this->url!='' and $this->usr_con!='' and $this->pwd_con!='') {
			$this->usr_correcto = true;
			$this->usuario = $usuario;
			$this->password = $password;
			//$sql = "select id_operator,operator from telephones_operators order by id_operator";
			//$this->arr_operators = ejecutaSELECT($sql,$GLOBALS['chk_var_control'],$GLOBALS['debug_pagina']);
		}
		
		 
	}
	
	function __TelephoneCheck() {//destructor
		$this->usuario = null;
		$this->password = null;
		$this->usr_correcto = null;
		$this->result = "KO";
		$this->company = null;
		$this->url = null;
		$this->usr_con = null;
		$this->pwd_con = null;
	}
	
	public function login($usuario,$password) { //EN UN FUTURO, SE UTILIZARÃ� PARA CLIENTES... PODRÃ�AMOS VENDER EL USO.
		$correcto = false;
		if (trim($usuario) != '' and trim($password!='') and $this->url!='' and $this->usr_con!='' and $this->pwd_con!='') {
			$correcto = true;
			$this->usuario = $usuario;
			$this->password = $password;
		}
		
		$this->usr_correcto = $correcto;	
		return $correcto;
		
	}
	
	public function chequearTelf($telf) {
		$output = array("resp"=>0,"idcheck"=>0,"country"=>0,"cod_comp"=>"0","source"=>'',"originalresp"=>'',"urltest"=>'',"error"=>'',"insert"=>'',"id_error"=>"0");
		$cod_error = '';
		if (($this->usr_correcto && !empty($telf) && strlen($telf)==9) && (($telf[0]==6) || ($telf[0]==7))) {
			
			$orTelf = $telf;
			
			$conexion  = new CBBDD($GLOBALS['db_type'], $GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pass'], $GLOBALS['db_name'],$GLOBALS['debug_pagina']);
			$conexion ->connectDB();
			$cur_conn_id = $conexion->get_id_conexion();
			
			
			$sql_check = "SELECT id_check,result,cod_pais,cod_operator 
					FROM telephones_check
					WHERE result <2 and 
						telephone='".$orTelf."' and 
						PERIOD_DIFF( DATE_FORMAT( NOW( ) ,  '%Y%m' ) , DATE_FORMAT( fecha,  '%Y%m' ) ) <=".$this->previous_months." 
					ORDER BY id_check DESC 
					LIMIT 0 , 1";
			$conexion ->getResultSelectArray($sql_check);
			$arr = $conexion->tResultadoQuery;
			//$arr = ejecutaSELECT($sql_check,$GLOBALS['chk_var_control'],$GLOBALS['debug_pagina']);
			
			if (!empty($arr) && count($arr)==1) {
				$output['resp']=$arr[0]['result'];
				$output['country']=$arr[0]['cod_pais'];
				$output['cod_comp']=$arr[0]['cod_operator'];
				//$output['company'] = $arr[0]['operator'];
				$output['idcheck'] = $arr[0]['id_check'];
				$output['source'] = 'BBDD';
				$output['urltest'] = $sql_check;
					
			} else {
				$output['source'] = 'WS';
				if (strlen(trim($telf))==9) {
					$telf = "+8934".$telf;
				//	$telf == utf8_encode($telf);
					//echo "uno!!";
				} 
				//$telf = str_replace("+","%2b",$telf);
				
				$campos = array("user"=>$this->usr_con,"password"=>$this->pwd_con,"to"=>$telf);
				$fields_string = http_build_query($campos);
				//$fields_string = "?user=".$this->usr_con."&password=".$this->pwd_con."&to=".$telf;
				//echo $fields_string;
				
				$url = ($this->url."?".$fields_string);
				$output['urltest'] = $url;
				//echo $url;
				$ch = curl_init();
				
				$timeout = 0; // set to zero for no timeout
				curl_setopt ($ch, CURLOPT_URL, $url);
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				$respuesta = curl_exec($ch);
				if ($respuesta === false) {
					$output['error'] = curl_errno($ch)." - ".curl_error($ch);
				}
				curl_close($ch);
				
				//$respuesta = file_get_contents($url);
				//echo $respuesta;
				$output['originalresp']=$respuesta;
				//echo $respuesta."<br>";
				if (strpos($respuesta,"ERROR_") === false && $output['error'] == '') {
					
					/*$tmp_resp = substr($respuesta,0,strpos($respuesta,$orTelf));
					
					$operator = intval(substr($tmp_resp,-2));
					$country = substr($tmp_resp,0,3);
					*/
					//$tmp_resp = substr($respuesta,0,strpos($respuesta,"X"));
					$tmp_resp = substr($respuesta,0,strpos($respuesta,$orTelf));
					if ($tmp_resp === false || $tmp_resp == '' || empty($tmp_resp)) {
						$tmp_resp = substr($respuesta,0,strpos($respuesta,"X"));
					}
					$operator = intval(substr($tmp_resp,-2));
					$country = substr($tmp_resp,-5,3);
					if ($operator != '' && $country != '') {
						$output['resp']=1;
						$output['country']=$country;
						$output['cod_comp']=$operator;
					} else {
						$output['resp']=2; //Error de sistema
						//error desconocido
						$cod_error = "20";
						$output['id_error'] = $cod_error;
						
						
					}
					//Como tenemos una tabla ya no lo necesitamos
					//$output['company'] = operators($operator);
					
					
					
					
				} else { //ha devuelto un error. En este caso, guardamos el error. Puede ser del curl o de otra cosa 
					$output['resp']=2; //Error
					if (strpos($respuesta,"ERROR_") !== false) { //Error del propio Ping
							$cod_error = trim(substr($respuesta,strpos($respuesta,"_")+1));
							$output['id_error'] = $cod_error;
							if ($cod_error == "0") {
								$output['resp']=0;
							}
							
						} else { //ha habido un error en el proceso curl
							$cod_error = "20";
							$output['id_error'] = $cod_error;
							
						}
				}
				$sql = "insert into telephones_check (telephone,result,cod_pais,cod_operator,id_error) values ('%s','%d','%d','%d','%s')";
				$sql = sprintf($sql,$orTelf,$output['resp'],$output['country'],$output['cod_comp'],$output['id_error']);
				$conexion->ejecuta_query($sql);
				$output['insert'] = $sql.";";
				$cur_conn_id = $conexion->get_id_conexion();
				$output['idcheck'] = mysqli_insert_id($cur_conn_id);
				if ($output['idcheck']=='0'){
					$sql_max_check = "select max(id_check) as max_id from %s telephones_check";
					$sql_max_check = sprintf($sql_max_check,$table_telephone_check);
					$conexion ->getResultSelectArray($sql_max_check);
					$conn_max_check = $conexion->tResultadoQuery;
					
					$output['idcheck'] = $conn_max_check[0]['max_id'];
				}
				/*$sql_max = "select last_insert_id() as id;";
				$conexion ->getResultSelectArray($sql_max);
				$arr_id = $conexion->tResultadoQuery;
				return $arr_id;
				
				
				$output['idcheck'] = $arr_id[0]['id'];
				*/
				//ejecutaInsertsUpdate($sql,$GLOBALS['chk_var_control'],$GLOBALS['debug_pagina']);
			//echo $respuesta;
			} // else if (count($arr)==1) {
			$conexion ->disconnectDB();
		}//if ($this->usr_correcto && trim($telf)!='') {
		return $output;
	}
}

?>