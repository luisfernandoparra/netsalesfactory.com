<?php

//********************************************************************************************
//Librería básica de funciones de manejo de base de datos
//
//         Funciones incorporadas:
//                     connectDB(): funcion que realiza la conexion y elige la BBDD con la que trabajaremos
//                     disconnectDB(): funcion que realiza la desconexion de la BBDD con la que trabajaremos
//                     getResultSelectArray($pQuery): funcion que guarda enun array el reultado de un SELECT
//                     ejecuta_query($pQuery): funcion que ejecuta una consulta que no es un SELECT (TRANSACCIONES EN MYSQL)
//                     get_error(): Obtiene el ultimo error producido
//                     get_consultas(): Obtiene todas las consultas ejecutadas y si estas se ejecutaron con exito o no
//                     get_tipo():  Obtiene el tipo de SGBD al que nos estamos conectando
//                     get_id_conexion(): Obtiene el identificado de conexion asignado por PHP
//                     getNumRows(): Obtiene el numero de registros de la consulta
//                     get_id() : Obtiene el ultimo id de una cosulta INSERT
//         Caracteristicas:
//                    v1:  Soporte acceso a multiples BBDDS: MySQL,SQLserver
//                    v2:  Soporte acceso PostGresSQL
//                         Soporte para transacciones
//
//********************************************************************************************
class CBBDD {
        //************************************************************************
        //                      VARIABLES DE CLASE
        //************************************************************************
        public $idConexion; //identificador de la conexion
        public $dbtype;     //tipo de BBDDs a la que nos conectamos
        public $db;         //nombre de la BBDDs a la que queremos conectarnos
        public $host;         //host donde se encuentra instalado el SGBD
        public $user;         //usuario de acceso a la BBDDs
        public $password;     //password de dicho usuario
        public $port;         //puerto de acceso (Mysql=3306,PostGreSQL=5432)
		public $charset;      //charset a utilizar (Por defecto utf-8)
        public $mQueryConExito;  //indica si la query se ejecuto con exito o no
        public $mError;       //guarda el ultimo error producido
       	public $mErrno;       //guarda el numero correspondiente al error producido
        public $mNumRegs;     //numero de registros que devuelve la ultima consulta
        public $tNumRegsConsultas;//array con el numero de registros de todas las consultas ejecutadas.Bueno para TRAZAS
        public $tResultadoQuery;//Es un array de dimension 2, las columnas corresponden a las
                             //columnas solicitadas y las filas corresponden a los registros encontrados
                             //Es un array de filas, que son a su vez array de columnas
        public $Id;             //guardamos el ultimo identificador de un INSERT
        public $tNombreColumnas;//Es un array de dimension 1, con los nombres de las columnas solicitadas en
                             //el mismo orden en el que están en el array tqueryResult

        public $tConsultasejecutadas;//Es un array donde guardamos todas las consultas que se han ejecutado
                                  //INteresante para las trazas
        public $tRegistroConsultas; //es un array donde guardamos si la consulta se ejecuto con exito o no
                                 //INteresante para las trazas		

        //************************************************************************
        //                      METODOS DE CLASE
        //************************************************************************
        //......................................................................
        //.......................CONSTRUCTOR....................................
        //......................................................................
		public function CBBDD($dbtype, $host, $user, $password, $db="",$objdebug="",$charset="utf8") //
		{
			//INICIALIZACION DE LAS VARIABLES;
           $this->idConexion = null;
           $this->dbtype = $dbtype;     //tipo de BBDDs a la que nos conectamos
           $this->host = $host;         //host donde se encuentra instalado el SGBD
           $this->user = $user;         //usuario de acceso a la BBDDs
           $this->password = $password;     //password de dicho usuario
           $this->db = $db;         //nombre de la BBDDs a la que queremos conectarnos	
		   $this->objdebug = $objdebug;		   
		   //$this->port=5432;        //Creo que esto es sólo para POSTGRESQL
                    $this->port=3306;        //Creo que esto es sólo para POSTGRESQL
		   $this->charset=$charset; 		//charset
		   $this->mNumRegs=0;     //numero de registros que devuelve la consulta
           $this->tNumRegsConsultas=array();
           $this->mError = null;
           $this->mErrno = null;
           $this->Id = null;
           $this->mQueryConExito = 'S';
           $this->tResultadoQuery=array();
           $this->tNombreColumnas=array();
           $this->tConsultasejecutadas=array();
           $this->tRegistroConsultas=array();		   		   		   
		}
        //......................................................................
		//............DESTRUCTOR................................................
        //......................................................................
		public function _CBBDD()
		{	
		   $this->obj=null;	
           $this->idConexion = null;
           $this->dbtype = null;
           $this->host = null;
           $this->user = null;
           $this->password = null;
           $this->db = null;
		   $this->objdebug=null;
           $this->port = null;
		   $this->charset=null;
		   $this->mError = null;
           $this->mErrno = null;
           $this->Id = null;
           $this->mQueryConExito = 'N';
           $this->mNumRegs=0;
           $this->tNumRegsConsultas=null;
           $this->tResultadoQuery=null;
           $this->tNombreColumnas=null;
           $this->tConsultasejecutadas=null;
           $this->tRegistroConsultas=null;
		   //$this->tDebug=null;		   		   
		}
		
		//CONNECT SEGUN BBDD		
        public function connectDB()
        {
				//echo "dbtype:::".strtoupper(trim($this->dbtype))."<br>";
            	$con=false;
				$obj=false;
				//CASOS DE BBDD
				switch(strtoupper(trim($this->dbtype))){
					case 'POSTGRESSQL': $obj = new CBBDDPostgress($this->dbtype,$this->host,$this->user,$this->password,$this->db,$this->objdebug,$this->charset); 
										break;  
					case 'MYSQL': 		$obj = new CBBDDMySql($this->dbtype,$this->host,$this->user,$this->password,$this->db,$this->objdebug,$this->charset); 
										break;
					case 'MSSQL': 		$obj = new CBBDDMsSql($this->dbtype,$this->host,$this->user,$this->password,$this->db,$this->objdebug,$this->charset);
										break;	
					case 'MSSQLI':		$obj = new CBBDDMsSqli($this->dbtype,$this->host,$this->user,$this->password,$this->db,$this->objdebug,$this->charset);
					
										break;	
				}
				//var_dump($obj);
				if($obj){
					$this->obj=$obj;
					$con = $obj->connectDB();
					
					if($con){
						$this->idConexion=$con;	
					
					
					//establecemos el charset
                                            $this->setCharset($this->charset,$con);		
                                        }
				}
				return $con;//devuelve true o false
				
        }//end function connectDB
		
		//DISCONNECT SEGUN BBDD
        public function disconnectDB(){
				if($this->idConexion){
				$this->obj->disconnect($this->get_id_conexion());
					//$this->_CBBDD();si pongo esto hay que poner el disconnet al final de la pagina pues si no cualqueir cosa despues de este no funcionaria
				}	
        }//end function disconnectDB
		
		public function setCharset($chst,$idconn){
			return $this->obj->setCharset($chst,$this->get_id_conexion());			
		}
		
		public function sendEmailReport($pQuery,$type) {
			if (QUERY_SEND_EMAIL_TRACKING==='1') {
				 $url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
				$body = "ERROR ".date("Y-m-d H:i:s")." \n QUERY: ".$pQuery." \n ERRN: ".$this->mErrno." \n ERRORMSG: ". $this->mError." \n PAG: ".$url;
				$subject = "ERROR EN ".$type." ".$_SERVER['SERVER_NAME']." ".date("Y-m-d H:i:s");
				/*$line = $url.";".$pQuery.";".$this->mError.";".$this->mErrno.";".$type."\r\n";
				$fp = fopen($GLOBALS['FileUploadDirectoryLogError'].date('Y-m-d').".csv",'a');
				fwrite($fp,$line); 
				fclose($fp);*/
				
				mail(QUERY_EMAIL_TRACKING_TO,$subject,$body);	
			}
		}		
		
		//Funcion que ejecuta una sentencia SELECT y guarda el Resultado en un array
        public function getResultSelectArray($pQuery)
		{
			$cur_id_conn = $this->get_id_conexion();
			//echo "current con SELECT ".$cur_id_conn."<br>";
			if(trim($this->mError)!="" || !$cur_id_conn) {return false;}
			else{//si no hay errores			
					if($pQuery){
						 //Ejecutamos la query
						 $mResult = $this->obj->executeQuery($pQuery,$cur_id_conn);
						 //var_dump($mResult); 
						 array_push ($this->tConsultasejecutadas,$pQuery); //guardamos dicha consulta
						 if ( !$mResult ) {
								 //obtenemos el error `roducido	
								 $this->mError = $this->obj->getLastError($cur_id_conn);						
								 //Caso de error en la consulta registramos dicho error
								 $this->mQueryConExito = 'N';
								 array_push ($this->tRegistroConsultas,$this->mQueryConExito); //guardamos si la consulta se ejecuto con exito o no
								 //METEMOS la consulta en el debug
								 if($this->objdebug!=""){						
									$this->objdebug->insertQueryToDebug($pQuery,"-1",$this->mQueryConExito,$this->mError); //insertamos consulta en el debug 
								 }
								 
								 $this->sendEmailReport($pQuery,'QUERY');
								 return false;
						}
						 else{
						 		$this->mNumRegs = $this->obj->getNumberRows($mResult);
								//echo "NROWS".$this->mNumReg;
								array_push ($this->tNumRegsConsultas,$this->mNumRegs); //guardamos num registros de  dicha consulta
								array_push ($this->tRegistroConsultas,$this->mQueryConExito); //guardamos si la consulta se ejecuto con exito o no						
								//METEMOS la consulta en el debug
								if($this->objdebug!=""){						
									$this->objdebug->insertQueryToDebug($pQuery,$this->mNumRegs,$this->mQueryConExito); //insertamos consulta en el debug 												
								}
								$i = 0;
		
								$this->tResultadoQuery = array();//por si utilizo el objeto para VARIAS consultas.MODIFIED 22/09/2008
								while ($i < $this->mNumRegs ) {
										//MODIFIED 24/09/2008.  						
										$vRow = $this->obj->getFila($mResult);
										//var_dump($vRow);
										//Construimos el array con los registros de la consulta
										$this->tResultadoQuery[$i] = $vRow;
										if($i==0){
												//Construimos el array con los campos de la consulta
												$k=0;
												while( list( $aCampo, $eValor ) = each( $this->tResultadoQuery[$i]) ){
													   $this->tNombreColumnas[$k] = strtolower($aCampo);
													   $k++;
												}//end while
		
										}//end if i==0
										$i++;
								}//end while
								//return $this->tResultadoQuery;
								return true; 						
						}	                     
					}
			}	 
		}
		
		//EJECUTAR QUERY INSERT,UPDATE,DELETE
		public function ejecuta_query($pQuery)
        {
				$cur_id_conn = $this->get_id_conexion();
				//echo "current con UPDATE".$cur_id_conn."<br>";
				if(trim($this->mError)=="" && $cur_id_conn){//si no hay errores
					$mResult = $this->obj->executeQuery($pQuery,$cur_id_conn);
				 	if(!$mResult && $this->mQueryConExito=='S'){
                      	$this->mQueryConExito= 'N';
                      	$this->mError = $this->obj->getLastError($cur_id_conn);
						$this->sendEmailReport($pQuery,'EXECUTION');
                 	}
                 	$this->mNumRegs = $this->obj->getAffectedRows($cur_id_conn);
					if(strpos(trim($pQuery),"INSERT")===0){//solo en caso de insertar, calculamos el ultimo id insertado.MODIFIED 20/09/2008
						$this->Id = $this->obj->getLastIdInserted($cur_id_conn);
				 	}
				 	array_push ($this->tConsultasejecutadas,$pQuery); //guardamos dicha consulta
                 	array_push ($this->tNumRegsConsultas,$this->mNumRegs); //guardamos num registros de  dicha consulta
                 	array_push ($this->tRegistroConsultas,$this->mQueryConExito); //guardamos si la consulta se ejecuto con exito o no
				 	//METEMOS EL OBJETO EN DEBUG	
				 	if($this->objdebug!=""){								
				 		$this->objdebug->insertQueryToDebug($pQuery,$this->mNumRegs,$this->mQueryConExito,$this->mError); //insertamos consulta en el debug 
				 	}
				}	
		}

		public function getNumRows(){
			return $this->mNumRegs;
		}
        
		//DEVOLVEMOS EL OBJETO
		public function getInstance(){
			return $this;
		}
		public function get_tipo(){
                 return $this->dbtype;
        }
        public function get_id_conexion(){
                 return $this->obj->get_id_conexion();
        }
        public function get_id(){
            return $this->Id;
        }
		public function getHost(){
			return $this->host;
		}
		public function getUser(){
			return $this->user;		
		}
		public function getClave(){
			return $this->password;		
		}
		public function getDb(){
			return $this->db;		
		}
		
		//......................................................................
        //............METODOS DE GESTION DE ERRORES.............................
        //......................................................................
        public function getError(){
            return $this->mError;
        }
		
        public function get_consultas(){   //TRAZAS
          for($i=0;$i<count($this->tConsultasejecutadas);$i++){
              echo "<br><b>--------CONSULTA $i ejecutada con exito::".$this->tRegistroConsultas[$i]."-------</b><br>";
              echo $this->tConsultasejecutadas[$i]."<BR>";
              echo "NUM REG:<b>".$this->tNumRegsConsultas[$i]."</b><BR>";
          }//end for
        }
		
		//ESTABLECEMOS VALORES
		public function setError($val){
			$this->mError = $val;
		}
		public function setQueryStatus($val){
			$this->mQueryConExito = $val;
		}
		public function insertErrorInToDebug($val){
			$a=$this->getError();
			$this->objdebug->insertErrorInToDebug($a); //insertamos error en el debug 			
		}
}//END CLASE 

//..........................................................................................................................................
//..........................................................................................................................................
//..................................................ACCESO A BBDD POSTGRESSQL...............................................................
//..........................................................................................................................................
//..........................................................................................................................................

class CBBDDPostgress extends CBBDD 
{
		//......................................................................
        //............METODOS DE CONEXION,DESCONEXION DE LA BBDD.............................
        //......................................................................
		public function connectDB(){//CONEXION CON POSTGRESS
				$conn = @pg_connect("dbname=".$this->getDb()." host=".$this->getHost()." user=".$this->getUser()." password=".$this->getClave());
				if( !$conn ) {
						$this->setError("No se puede conectar con el servidor:");
						$this->insertErrorInToDebug("No se puede conectar con el servidor:");		
                        return false;
               }
			   elseif(!(pg_dbname($conn))) {
			   			$this->setError("No se puede conectar con la BBDD:");
						$this->insertErrorInToDebug("No se puede conectar con la BBDD:");
						return false;
               }
			   $this->idConexion = $conn;
			   return $conn;             
		}
		
		public function disconnect($idConexion){
				pg_close($idConexion);
        }//end function disconnectDB
		
		public function setCharset($chst,$idConexion){
			return pg_set_client_encoding($idConexion,$chst);			
		}
		
		public function getNumberRows($mResult){//NUM ROWS EN UN SELECT
			if(!$mResult) return 0;
			else return pg_num_rows($mResult);
		}
		
		public function getAffectedRows($mResult){ //NUM ROWS DE UN INSERT,UPDATE,DELETE
			if(!$mResult) return 0;
			else{ return pg_affected_rows($mResult);}			
		}
		
		public function getNumRows(){//OBTENER NUM ROWS DE LA ULTIMA CONSULTA (SEA SELECT O INSERT;UPDATE;DELETE)
			return $this->mNumRegs;
		}
		
		public function getFila($mResult){//GET FILA
				return pg_fetch_assoc($mResult);
		}
		
		public function executeQuery($pQuery,$idConexion){//EJECUTAR QUERY
				return @pg_query($idConexion,$pQuery); 
		}
		
		public function getLastError($idConexion){
			 if(!$idConexion) return false;
			 else{ return pg_last_error($idConexion);}
		}
		
		public function getLastIdInserted($idConexion){//POR INVESTIGAR
			return 0;
		}
		public function get_id_conexion(){
			return $this->idConexion;
		}
		
}//end Postgress Clase

//..........................................................................................................................................
//..........................................................................................................................................
//.......................................................ACCESO A BBDD MYSQL................................................................
//..........................................................................................................................................
//..........................................................................................................................................

class CBBDDMySql extends CBBDD 
{
		//......................................................................
        //............METODOS DE CONEXION,DESCONEXION DE LA BBDD.............................
        //......................................................................
		public function connectDB(){//CONEXION CON MYSQL		
			   $conn = @mysql_connect($this->getHost(),$this->getUser(), $this->getClave());			  			  
			   if( !$conn ) {
			   				$this->setError("No se puede conectar con el servidor:");
							$this->insertErrorInToDebug("No se puede conectar con el servidor:");
							return false;								 
               }
               elseif(!(mysql_select_db($this->getDb(),$conn))) {//
			   				$this->setError("No se puede conectar con la BBDD:");
							$this->insertErrorInToDebug("No se puede conectar con la BBDD:");
							return false;								
               }
			   //echo "conn:::::".$conn."<br>";
			   $this->idConexion = $conn;
			   return $conn;             
		}
	
		public function disconnect($idConexion){
				//echo "disconnectDB:::".$this->idConexion."---".$idConexion."<br>";
				mysql_close($idConexion);				
        }//end function disconnectDB
		
				
		//ADDED 24/09/2008 para controlar el charset
		public function setCharset($chst,$idConexion){
			if (function_exists('mysql_set_charset') === false) {
				if ($idConexion == null) {
            		return mysql_query('SET NAMES "'.$chst.'"');
         		} else {
             		return mysql_query('SET NAMES "'.$chst.'"', $idConexion);
         		}	
			}
			else{
				return mysql_set_charset($chst,$idConexion); 
			}
		}
		
		public function getNumberRows($mResult){ //NUM ROWS DE UN SELECT
			if(!$mResult) return 0;
			else return mysql_num_rows($mResult);
		}
		
		public function getAffectedRows($idConexion){ //NUM ROWS DE UN INSERT,UPDATE,DELETE
			//echo "getAffectedRows:::---".$idConexion."----<br>";
			if(!$idConexion) return 0;
			else{ return mysql_affected_rows($idConexion);}//$idConexion			
		}
		
		public function getNumRows(){
			return $this->mNumRegs;
		}
		
		public function getFila($mResult){ //GET FILA
				return mysql_fetch_assoc($mResult);
		}
		
		public function executeQuery($pQuery,$idConexion){//EJECUTAR QUERY
				return @mysql_query($pQuery,$idConexion); 
		}
		
		public function getLastError($idConexion){
			 //echo "getLastError:::".$this->idConexion."---".$idConexion."<br>";
			 if(!$idConexion) return false;
			 else{ return mysql_error($idConexion);}//$idConexion
		}
		
		public function getLastIdInserted($idConexion){
			if(!$idConexion) return 0;
			return mysql_insert_id($idConexion);			
		}
		public function get_id_conexion(){
			return $this->idConexion;
		}
				
}//end Mysql Class 
 
//..........................................................................................................................................
//..........................................................................................................................................
//.......................................................ACCESO A BBDD MSSQL................................................................
//..........................................................................................................................................
//..........................................................................................................................................

class CBBDDMsSql extends CBBDD 
{
		//......................................................................
        //............METODOS DE CONEXION y DESCONEXION A LA BBDDs..............
        //......................................................................
		public function connectDB(){//CONEXION CON MSSQL
			   $conn = @mssql_connect($this->getHost(),$this->getUser(), $this->getClave()); 			   
			   if( !$conn ) {
                            $this->setError("No se puede conectar con el servidor:");
							$this->insertErrorInToDebug("No se puede conectar con el servidor:");
							return false;
               }
               elseif(!(mssql_select_db($this->getDb(),$conn))) {//
                          	$this->setError("No se puede conectar con la BBDD:");
							$this->insertErrorInToDebug("No se puede conectar con la BBDD:");
							return false;
 			   }
			   $this->idConexion = $conn;
			   return $conn;             
		}
		public function disconnect($idConexion){
				mssql_close($idConexion);
		}
		
		public function setCharset($chst,$idConexion){//POR INVESTIGAR
			return;
		}
		
		public function getNumberRows($mResult){//NUM ROWS EN UN SELECT
			if(!$mResult) return 0;
			else return mssql_num_rows($mResult);
		}
		
		public function getAffectedRows($idConexion){ //NUM ROWS DE UN INSERT,UPDATE,DELETE		
			if(!$idConexion) return 0;
			else{ return mssql_rows_affected($idConexion);}			
		}
		
		public function getNumRows(){//OBTENER NUM ROWS DE LA ULTIMA CONSULTA (SEA SELECT O INSERT;UPDATE;DELETE)
			return $this->mNumRegs;
		}
			
		public function getFila($mResult){//GET FILA
				return mssql_fetch_assoc($mResult);
		}
		
		public function executeQuery($pQuery,$idConexion){//EJECUTAR QUERY
				return @mssql_query($pQuery,$idConexion); 
		}
		
		public function getLastError($idConexion){
			 if(!$idConexion) return false;
			 else{ return mssql_get_last_message($idConexion);}
		}
		
		public function getLastIdInserted($idConexion){//POR INVESTIGAR
			return 0;
		}
		public function get_id_conexion(){
			return $this->idConexion;
		}
} 		

class CBBDDMsSqli extends CBBDD 
{
		//......................................................................
        //............METODOS DE CONEXION,DESCONEXION DE LA BBDD.............................
        //......................................................................
		public function connectDB(){//CONEXION CON MYSQL	
			   $conn = @mysqli_connect($this->getHost(),$this->getUser(), $this->getClave(),$this->getDb());			  			  
			   if( !$conn ) {
			   				$this->setError("No se puede conectar con el servidor imp :").mysqli_connect_errno()." / ".mysqli_connect_error();
                                                       //$this->setError("No se puede conectar con el servidor imp :".mysqli_error($conn)." ".$conn->connect_errno." ".$conn->connect_error);
                                                       
							$this->insertErrorInToDebug("No se puede conectar con el servidor dos:");
							return false;								 
               }/*
               elseif(!(mysql_select_db($this->getDb(),$conn))) {//
			   				$this->setError("No se puede conectar con la BBDD:");
							$this->insertErrorInToDebug("No se puede conectar con la BBDD:");
							return false;								
               }*/
			   //echo "conn:::::".$conn."<br>";
			   $this->idConexion = $conn;
			   return $conn;             
		}
		public function disconnect($idConexion){
				mysqli_close($idConexion);
		}
		
		public function setCharset($chst,$idConexion){
			if (! mysqli_set_charset($idConexion, $chst)) {
				$this->setError(mysqli_error($idConexion));
			}
			/*if (function_exists('mysql_set_charset') === false) {
				if ($idConexion == null) {
            		return mysql_query('SET NAMES "'.$chst.'"');
         		} else {
             		return mysql_query('SET NAMES "'.$chst.'"', $idConexion);
         		}	
			}
			else{
				return mysql_set_charset($chst,$idConexion); 
			}
			*/
		}
		public function getNumberRows($mResult){ //NUM ROWS DE UN SELECT
			if(!$mResult) return 0;
			else return mysqli_num_rows($mResult);
		}
		
		public function getNumRows(){
			return $this->num_rows;
		}
		
		public function getFila($mResult){ //GET FILA
				return mysqli_fetch_assoc($mResult);
		}
		public function executeQuery($pQuery,$idConexion){//EJECUTAR QUERY
				return @mysqli_query($idConexion,$pQuery); 
		}
		
		public function getAffectedRows($idConexion){ //NUM ROWS DE UN INSERT,UPDATE,DELETE
			//echo "getAffectedRows:::---".$idConexion."----<br>";
			if(!$idConexion) return 0;
			else{ return mysqli_affected_rows($idConexion);}//$idConexion			
		}
		
		public function getLastError($idConexion){
			 //echo "getLastError:::".$this->idConexion."---".$idConexion."<br>";
			 if(!$idConexion) return false;
			 else{ return mysqli_error($idConexion);}//$idConexion
		}
		
		public function get_id_conexion(){
			return $this->idConexion;
		}
		
		public function getLastIdInserted($idConexion){
			if(!$idConexion) return 0;
			return mysqli_insert_id($idConexion);			
		}
	
}
?>