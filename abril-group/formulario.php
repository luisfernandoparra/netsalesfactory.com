<?php
include('../conf/config_web.php');
ini_set('display_errors',0);
include('conf/config_web.php');

//include($path_raiz_includes.'includes/initiate.php');
//include($path_raiz_includes_local.'includes/local_vars.php');
//$layoutType=null;

$sql = "select id_provincia,provincia from %s order by provincia";
$sql = sprintf($sql,$table_provincias_br);
$conexion_prov = ejecutaSELECT($sql,$chk_var_control,$debug_pagina);


?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<title>Documento sin título</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	
	
	
<link href="<?php echo $path_raiz_aplicacion; ?>js/modal/css/modal.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $path_raiz_aplicacion; ?>js/window/css/window.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
		var root_path = "<?php echo $path_raiz_aplicacion;?>";<?php /*ruta global para las imgs del facebox y del modal*/?>
</script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/modal/js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/window/js/window.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion ?>abril-group/js/form.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/funciones_comunes.js"></script>
  </head>
  <body>
	<form id="formID" name="formID" method="post" action="#">
	  <table>
		<tr>
		  <td width="110px;">
			<input name="tratamiento" type="radio" value="H"  /> Sr.
			<input name="tratamiento" type="radio" value="M"  /> Sra.
		  </td>
		</tr>
	  </table>
	  <div>
	  <label for="nombre">Nome</label>
	  <input id="nombre" name="nombre" type="text" value="" maxlength="255" class="clickform">
	  <label for="apellidos1">Sobrenomes</label>
	  <input id="apellidos1" name="apellidos1" maxlength="250" type="text" value="" size="20" class="clickform" />
	  </div>
	  <div>
		<label for="apellidos1">Celular</label>
		<input id="telf1" name="telf1"  maxlength="3"  type="text" value="" class="clickform" /> - 
		<input id="telf2" name="telf2"  maxlength="9"  type="text" value="" class="clickform" />
	  </div>
	  <div>
		<label for="email>">Email</label>
		<input id="email" name="email"  maxlength="250" type="text" value="<?php //echo $email_pre; ?>" class="clickform">
	  </div>
	  <div>CPF (prenche seu cpf para validar sua participação):</div>
	  <div id="d_cpf" name="d_cpf" style="">
		<input name="cpf1" id="cpf1" maxlength="3" value="" type="text" class="clickform" style="width:40px;">
		<input name="cpf2" id="cpf2" maxlength="3" value="" type="text" class="clickform" style="width:40px;">
		<input name="cpf3" id="cpf3" maxlength="3" value="" type="text" class="clickform" style="width:40px;">
		<input name="cpf4" id="cpf4" maxlength="2" value="" type="text" class="clickform" style="width:40px">
	  </div>
	  <div>
		<div id="fecha_nombre">Data de Nascimento</div>
		<div id="capa_fecha">
			<select  name="dia_fech" id="dia_fech" class="f_dia" >
				<?php for ($i=1;$i<=31;$i++) {echo "<option value='".((strlen($i)==1) ? "0".$i : $i)."'"; echo ">".((strlen($i)==1) ? "0".$i : $i)."</option>";}?>
			</select>
			<select  name="mes_fech" id="mes_fech" class="f_mes">
				<?php for ($i=1;$i<=12;$i++) {echo "<option value='".((strlen($i)==1) ? "0".$i : $i)."'"; echo ">".ucwords($arr_meses[$i-1])."</option>";}?>
			</select>
			<select name="ano_fech" id="ano_fech" class="f_ano">
				<?php for ($i=date('Y',strtotime('-18 year',strtotime(date('Y'))));$i>=1900;$i--) {echo "<option value='".($i)."'"; echo ">".$i."</option>";}?>
			</select>
		</div>
	  </div>                  
	  <div id="d_cep">
		 <div>CEP (prenche seu cep para validar sua participação):</div>
		<input name="cep1" id="cep1" maxlength="5" value="" size="4" type="text" class="clickform" /> - 
		<input name="cep2" id="cep2" maxlength="3" value="" size="2" type="text" class="clickform" />
	  </div>
	  <div name="d_provincia" id="d_provincia">
		<select name="provinciaDir" id="provinciaDir" onchange="cargarPoblacion(this.value,1);">
			<option value=''>estado</option>
		   <?php for ($i=0;$i<count($conexion_prov);$i++) {?>
				<option value="<?php echo trim($conexion_prov[$i]['id_provincia']); ?>"<?php if($provincia==trim($conexion_prov[$i]['id_provincia'])){echo ' selected="selected"';}?>><?php echo trim($conexion_prov[$i]['provincia']); ?></option>
			<?php }?>
		</select>
	  </div>
	  <div name="d_poblacion" id="d_poblacion"> 
		  <label>
			<select  name="poblacionDir" id="poblacionDir" >
			    <option value=''>cidade</option>                        
			</select> 
		  </label>
	  </div>
	  <div class="consiento">
                		<input class="check" name="cbLopd" id="cbLopd" type="checkbox" value="1"<?php if($optin == "1"){echo ' checked="checked"';}?> />
                            Consinto a cessão de meus dados pessoais por parte da BLUE SALES.
               		</div>
	  
	  <input class="button btn_color" name="btnProcesar1" type="button" value="Continue, é gratis!" id="btnProcesar1" />
	 </form> 
  </body>
</html>