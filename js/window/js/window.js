/**Necesita de jquery.simplemodal para funcionar correctamente*/
function showModal(o){ //title,message,type,callback
	/*TPL ASOCIADO A LOS TIPOS DE VENTANA*/
	var tpl ='<div id="'+o.type+'">';	
	tpl+='		<div id="dialog-header">';
	tpl+='			<div id="dialog-title">'+o.title+'</div>';			
	tpl+='		</div>';
	tpl+='		<div id="dialog-content" class="'+o.type+'">';
    tpl+='    		<p><img src="'+root_path+'js/window/img/'+o.type+'_128.png" border="0" width="64" height="64" /></p>';
    tpl+='      	<div class="message"></div>';
	if(o.type=="confirm"){
		tpl+='      	<div class="modal-buttons"><a class="modal-button button yes" href="#">S&iacute;</a>&nbsp;<a class="modal-button button simplemodal-close" href="#">No</a></div>';
	} else {
		tpl+='      	<div class="modal-buttons"><a class="modal-button button simplemodal-close" href="#">OK</a></div>';
	}
    tpl+='      	<div style="clear:both;"></div>';        
    tpl+='   	</div>';     
	tpl+='</div>';
	
	if(o.tpl) tpl = o.tpl;
		
	//LLAMADA A SIMPLE MODAL	
	$.modal(tpl,{						 
						//closeHTML:"<a href='#' title='Close' class='modalCloseImg simplemodal-close'>x</a>",
						position: ["20%",],
						overlayId:'modal-overlay',
						containerId:'modal-container',
						autoResize:true,
						onShow: function (dialog) {
									var modal = this;
									if(o.title && $.trim(o.title)!=""){$('#dialog-title', dialog.data[0]).html(o.title);}
									$('.message', dialog.data[0]).html(o.message);																		
									var a = $("#modal-container").height('auto');																				
									if(o.type=="confirm"){
										// if the user clicks "yes"
										$('.yes', dialog.data[0]).click(function (e) {
											e.preventDefault();																				  
											// call the callback
											if ($.isFunction(o.callback)) {
												o.callback.apply();
											}
											// close the dialog
											modal.close(); // or $.modal.close();
										});
									}
							}
	});
	
}