﻿//*********************************************************************************************************
// VALIDACIONES
//*********************************************************************************************************
// 1- es_valido_campo_texto(campo_formulario,alerta_vacio,alerta_vocal,cantidad_carecteres_minimo,cantidad_caracteres_maximo,alerta_minimo,alerta_maximo)
// 2- es_valido_campo_editor(campo_formulario,alerta_vacio,alerta_maximo,cantidad_caracteres_maximo) -> comprueba que la longitud del texto no exceda de un tamaño
// 3- checkEmail(cadena_mail) -> comprueba que el email es correcto
// 4- Comprobarradio(radio) ->comprueba quese ha seleccionado un radiobutton
// 5- validar_formulario(num_campos,funcion) -> devuelve el numero de campos NO VALIDADOS de un FORMULARIO ejecutando la funcion pasada por parametro
// 6- validarCP(obj) -> validar codigo postal
// 7- es_nif_correcto(nif) -> Validacion del NIF y letraNif(dni) -> calculo letra del nif
// 8- listaCamposForm(nombre_formulario)->  Recoge todos los campos de un formulario y los concatena para el ajax.
// 9- establecer_valor(nombre_formulario,nombre_campo,valor,separador_varios_checkbox) -> establece los vaores para los tipos de campo

//************************FUNCIONES RELACIONADAS CON EL SELECT*******************************************
//1) llenar_select(id_campo,default_text,default_value,textos,valores,valor_selected) -> llena un combo con valores y selecciona un valor  
//2) borra el select(sel) -> borra el select "sel"
//3) seleccionar(sel, val) -> selecciona opcion con valor "val" dentro del select "sel"

//************************FUNCIONES RELACIONADAS CON LOS CHECKBOXS*******************************************
//contador_checks: variable que guarda el numero de checks que estan chequeados
//1) actualizar_contador_check(objeto_check) gestiona la variale "contador_checks" segun los checks chequeados 
//2) Comprobarcheck(ini,fin,objeto_formulario,nombre_check) -> comprueba si hay algun check chequeado dentro de un rango
//3) desactivar_todos(objeto_formulario,lista_ids) -> desmarcar todos elementos seleccionados
//4) almenosuncheck() -> haber selccionado almenos 1 checkbox de un grupo 

//*********************************************************************************************************
// FUNCIONES
//*********************************************************************************************************
//1) VALIDACION DE UN CAMPO DE TEXTO
function es_valido_campo_texto(campo_formulario,alerta_vacio,alerta_vocal,cantidad_carecteres_minimo,cantidad_caracteres_maximo,alerta_minimo,alerta_maximo)
{
	var salida = 0;
	mensaje_error_alert = "";
	if (campo_formulario)
	{
		if (campo_formulario.value.trim() == "")
		{
			if ((mensaje_error_alert == "") && alerta_vacio)
			{
				campo_formulario.focus();
				mensaje_error_alert = alerta_vacio;
			}
		}
		else if (!tiene_cantidad_de_caracteres_validos(campo_formulario,cantidad_caracteres_maximo,'1'))
		{
			if (mensaje_error_alert == "")
			{
				campo_formulario.focus();
				mensaje_error_alert = alerta_maximo;
			}
		}
		else if (!tiene_cantidad_de_caracteres_validos(campo_formulario,cantidad_carecteres_minimo,'0'))
		{
			if (mensaje_error_alert == "")
			{
				campo_formulario.focus();
				mensaje_error_alert = alerta_minimo;
			}
		}
		else if (!tiene_vocal(campo_formulario) && alerta_vocal)
		{
			if (mensaje_error_alert == "")
			{
				campo_formulario.focus();
				mensaje_error_alert = alerta_vocal;
			}
		}
		else
		{
			salida = 1;
		}
	}
	else
	{
		salida = 1;
	}
	
	return salida;
}

//2) VALIDACION DE longitud maxima de UN CAMPO DEL FCKEDITOR
function es_valido_campo_editor(campo_formulario,alerta_vacio,alerta_maximo,cantidad_caracteres_maximo)
{
	var salida = 0;
	mensaje_error_alert = "";	
	var largo_total = largo_valor_editor(campo_formulario);
	var largo_limpio = largo_valor_limpio_editor(campo_formulario);
		
	if((largo_limpio == 0) && (alerta_vacio != "")) {
		if ((mensaje_error_alert == "") && alerta_vacio)
		{
			mensaje_error_alert = alerta_vacio;
		}
	}
	else if ((largo_total > cantidad_caracteres_maximo) && (alerta_maximo != "")) 
	{
		if ((mensaje_error_alert == "") && alerta_maximo)
		{
			mensaje_error_alert = alerta_maximo+" posee "+largo_total+" caracteres, y solo puede insertar un maximo de "+cantidad_caracteres_maximo+".";
		}
	}
	else
	{
		salida = 1;
	}
	
	return salida;
}

//3) VALIDACION EMAIL
function checkMail(cadena_mail)
{
	ext=cadena_mail.substring(cadena_mail.lastIndexOf("."),cadena_mail.length);
	if(ext.length<5 && ext.length>1)
	{	
		var Template = /^[a-z][a-z-_0-9.]+@[a-z-_=>0-9.]+.[a-z]{2,3}$/i //Formato de direccion de correo electronico
		//return (Template.test(cadena_mail)) ? 1 : 0 //Compara "cadena_mail" con el formato "Template" y si coincidevuelve verdadero si no devuelve falso
		if (Template.test(cadena_mail) == 0)
		{ return false; //errStr = "El campo Email es incorrecto.\r ";
		} else{ return true;}
	} else { return false; //errStr = "El campo Email es incorrecto.\r ";
	}
}

//4)VALIDACION DE ELECCION DE UN RADIOBUTTON
function Comprobarradio(radio){ 
	var seleccionados=-1;	
 	longitud=eval(radio+'.length');
 	//alert(longitud);
 	lon=parseInt(longitud); 
 	for(i=0;i<lon;i++){  
  		if(eval(radio+'['+i+'].checked')){seleccionados=eval(radio+'['+i+'].value');} 	 
 	}
 	return seleccionados;   
}

//5)devuelve el numero de campos NO VALIDADOS de un FORMULARIO
//utiliza la funcion auxiliar que se encarga de VALIDAR CADA CAMPO DEL FORMULARIO. Este campo 
//viene definido por el contador "i".dicha funcion se define al validar un formulario y se llama en el onblur de cada campo
function validar_formulario(num_campos,funcion) {	
	n_campos_invalid = 0;	
	for(var h=1; h <= num_campos;h++)
	{		
		if( funcion != "" && (typeof funcion == "function") && eval(funcion(h)==false)){n_campos_invalid++;}						
	}	
	return n_campos_invalid; 
}

//6) VALIDACION DEL CODIGO POSTAL
function validarCP(obj){
	if(obj < 0 || obj > 53000 || isNaN(obj) || (obj.length < 5))
	{ return false;}
	else{return true;}
}

//7) VALIDACION DEL NIF
//********************************************// 
//**********  COMPROVACION DEL DNI  **********//
function es_nif_correcto(nif)
{
	lon = nif.length;
	
	if (lon<6) return false
	
	letraDNI = nif.charAt(lon-1)
	letraDNI = letraDNI.toUpperCase()
	dni = nif.substring(0,lon-1)
	
	if(isNaN(dni))
		return(false)
	else
	{
		letraCorrecta = letraNif(dni)
		
		if (letraDNI != letraCorrecta)
			return(false)
		else
			return(true)
	}
}

function letraNif(dni) //calculo letra del nif
{
	var asc
	asc= dni % 23;
	
	if ( asc == 0) { letra="T"; };
	if ( asc == 1) { letra="R"; };
	if ( asc == 2) { letra="W"; };
	if ( asc == 3) { letra="A"; };
	if ( asc == 4) { letra="G"; };
	if ( asc == 5) { letra="M"; };
	if ( asc == 6) { letra="Y"; };
	if ( asc == 7) { letra="F"; };
	if ( asc == 8) { letra="P"; };
	if ( asc == 9) { letra="D"; };
	if ( asc == 10) { letra="X"; };
	if ( asc == 11) { letra="B"; };
	if ( asc == 12) { letra="N"; };
	if ( asc == 13) { letra="J"; };
	if ( asc == 14) { letra="Z"; };
	if ( asc == 15) { letra="S"; };
	if ( asc == 16) { letra="Q"; };
	if ( asc == 17) { letra="V"; };
	if ( asc == 18) { letra="H"; };
	if ( asc == 19) { letra="L"; };
	if ( asc == 20) { letra="C"; };
	if ( asc == 21) { letra="K"; };
	if ( asc == 22) { letra="E"; };
	if ( asc == 23) { letra="T"; };

	return letra;
}

//8)Recoge todos los campos de un formulario y los concatena para el ajax.
function listaCamposForm(nombre_formulario){  
	var formulario = document[nombre_formulario];	
	var i = 0;
	var campos = "";
	var nombre_campo = "";
	var valor_campo = "";
	while (formulario.elements[i]){
		nombre_campo = "";
		valor_campo = "";
		
		if (formulario.elements[i].type != "radio" && formulario.elements[i].type != "checkbox" ){
			nombre_campo 	= formulario.elements[i].name;
			valor_campo 	= formulario.elements[i].value;
		}
		else if (formulario.elements[i].type == "checkbox" || formulario.elements[i].type == "radio"){
			if (formulario.elements[i].checked){
				nombre_campo 	= formulario.elements[i].name;
				valor_campo		= formulario.elements[i].value;
			}
		}
		
		if(formulario.elements[i].id!="fieldset"){
			if (campos && nombre_campo != "")
				campos += "&"+nombre_campo+"="+encodeURIComponent(valor_campo);
			else if (nombre_campo != "")
				campos = nombre_campo+"="+encodeURIComponent(valor_campo);
		}
		
		i++;
	}
	
	return campos;
}
//9) establece el valor de un campo radio,grupo checkbox,texto,hidden y select
function establecer_valor(nombre_formulario,nombre_campo,valor,separador_varios_checkbox)
{
	//*****  FUNCION SOLO PARA TIPO DE CAMPOS RADIO, CHEQUED Y SELECT  *****//
	var i = 0;
	var campo_formulario = document[nombre_formulario];
	campo_formulario = campo_formulario[nombre_campo];		
	if (!campo_formulario.type)
	{
		var tipo_campo = campo_formulario[i].type;
	}
	else
	{
		var tipo_campo = campo_formulario.type;
	}
	
//	alert(nombre_campo+": es de tipo \""+tipo_campo+"\"");
	
	if (tipo_campo == "radio")
	{
		while (campo_formulario[i])
		{
			if (campo_formulario[i].value == valor)
			{
				campo_formulario[i].checked = true;
				break;
			}
			i++;
		}
	}
	else if (tipo_campo == "checkbox")
	{
		if (separador_varios_checkbox)
		{
			var valor_campo = campo_formulario.value;
			var valor = String(valor);
			valor = valor.split(separador_varios_checkbox);
			var j = 0;
			var valor_tmp = "";
			
			while (j < valor.length)
			{
				valor_tmp = trim(valor[j]);
				if (campo_formulario.value == valor_tmp)
				{
					campo_formulario.checked = true;
					break;
				}
				j++;
			}
		}
		else
		{
			if (campo_formulario.value == valor)
			{
				campo_formulario.checked = true;
			}
		}		
	}
	else if (tipo_campo == "text" || tipo_campo == "hidden")
	{
		campo_formulario.value = valor;
	}
	else
	{
		while (i < campo_formulario.options.length)
		{
			if (campo_formulario.options[i].value == valor)
			{
				campo_formulario.options[i].selected = true;
				break;
			}
			i++;
		}
	}
}

//*******************************************************************************************************
//FUNCIONES RELACIONADAS CON EL SELECT
//*******************************************************************************************************

//1)  llenar_select(id_campo,default_text,default_value,textos,valores,valor_selected) 
function llenar_select(id_campo,default_text,default_value,textos,valores,valor_selected) {
	var campo = document.getElementById(id_campo);	
	campo.length = 0;	
	//campo.options[0] = new Option("-------------------","0");
	campo.options[0] = new Option(default_text,default_value);
	
	var j=0;
	var i=1;
	while(textos[j]) {		
		campo.options[i] = new Option(textos[j],valores[j]);
		if (valores[j] == valor_selected)
			campo.options[i].selected = true;
		j++;
		i++;
	}
}

//2) borra el select(sel) -> borra el select "sel"
function erase_select (sel) {
  while ( sel.options[0] != null )   
    sel.options[0] = null;
}

//3) seleccionar(sel, val) -> selecciona opcion con valor "val" dentro del select "sel"
function seleccionar(sel, val) {  
  for (var i=0; i<sel.options.length; i++) {		  
    if (sel.options[i].value == val) {sel.selectedIndex = i; break;}
  }
}

//*******************************************************************************************************
//FUNCIONES RELACIONADAS CON LOS CHECKBOXS
//*******************************************************************************************************

//contador_checks: variable que guarda el numero de checks que estan chequeados
//1) actualizar_contador_check(objeto_check) gestiona la variale "contador_checks" segun los checks chequeados 
//2) Comprobarcheck(ini,fin,objeto_formulario,nombre_check) -> comprueba si hay algun check chequeado dentro de un rango
//3) desactivar_todos_checks(objeto_formulario) -> desmarcar todos elementos seleccionados
//4) almenosuncheck() -> haber selccionado almenos 1 checkbox de un grupo 
var contador_checks=0;
//1)ACTUALIZAR EL CONTADOR DE CHECKBOX chequeados
function actualizar_contador_check(objeto){
	if(objeto.checked){ contador_checks = contador_checks + 1;}
	else{contador_checks = contador_checks - 1;}
	//alert(contador_checks)
}

//2)VALIDACION DE ELECCION DE AL MENOS 1 CHECKBOX
function Comprobarcheck(ini,fin,objeto_formulario,nombre_check){ 
 	for(i=ini;i<=fin;i++){  		
  		if(eval("document."+objeto_formulario+".elements['"+nombre_check+"_"+i+"'].checked==true")){ contador_checks++;}  
 	}
 	return contador_checks;
}

//3) desmarcar elementos seleccionados
function desactivar_todos_checks(objeto_formulario)
{
	if(contador_checks==0){
		alert("No hay elementos seleccionados")
	}
	else{
		for(j=0; j < objeto_formulario.elements.length;j++){
			if(objeto_formulario.elements[j].type=='checkbox'){//los checkbox
				objeto_formulario.elements[j].checked=false;				
			}
	    }	
		contador_checks=0;//actualizamos el contador de elementos chequeados		
	}			
}

//4)haber selccionado almenos 1 checkbox de un grupo
function almenosuncheck() {        
	var numsel = 0;
     var arr_chk = document.getElementsByTagName('input');
        for (var i=0; i<arr_chk.length; i++) {
            if (arr_chk[i].type == 'checkbox') {
                if (arr_chk[i].checked) {
                    numsel++;
                }
            }
        }
	if(numsel>0) return true;
	else return false;
}
function esAlfaNumerico(texto)
{
	var Template = /[A-Za-z]/;
	var Template2 = /[0-9]/;
	if(Template.test(texto) && Template2.test(texto)){
		return 1;
	}else{
		return 0;
	}
}

function solamenteNumeros(e,sinPunto,valor_campo) {
	boolean_valor_campo = 1;
	if (typeof valor_campo == "undefined") {
		valor_campo = '';
		boolean_valor_campo = 0;
	}
	var evt = e?e:event;		
	var sinPunto = (typeof sinPunto == "undefined")? 0 : sinPunto;	
	var keyAsciiCode = (evt.which)? evt.which : evt.keyCode;

	if (sinPunto == 1) {
		if(keyAsciiCode!=8 && keyAsciiCode!=9 && (keyAsciiCode<48 || keyAsciiCode>57))
			return false;
	}
	else {
		if (boolean_valor_campo == 0 && sinPunto == 0){
			alert("Debes colocar el tercer parametro de la funcion (this.value) 'return solamenteNumeros(event,0,this.value);'");
		}
		if((keyAsciiCode!=46 && keyAsciiCode!=8 && keyAsciiCode!=9 && (keyAsciiCode<48 || keyAsciiCode>57))  || (keyAsciiCode==46 && valor_campo != "" && (valor_campo).indexOf('.') != -1))
			return false;
	}
	return true;
}