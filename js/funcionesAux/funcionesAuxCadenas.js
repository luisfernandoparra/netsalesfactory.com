﻿//*****************************************************************************************
//								VARIABLES AUX DE GESTION DE CADENAS
//*****************************************************************************************
var RegEsLetra = /^([a-z]|[A-Z]|á|é|í|ó|ú|ñ|ü|ë|ä|Á|É|Í|Ó|Ú|Ñ|Ü|Ë|Ä|\s)+$/			//letras, '.' y '-'
var esEspecial = "'`´./ºª#|,;-()_:@ñ"; //conjunto de caracteres especiales
var esAlfa = "aáàäâbcçdeéèëêfghiíìïîjklmnñoóòöôpqrstuúùüûvwxyzAÁÀÄÂBCÇDEÉÈËÊFGHIÍÌÏÎJKLMNÑOÓÒÖÔPQRSTUÚÙÜÛVWXYZ'`´./ºª # |,;-()_:"; //conjunto de caracteres alfanumericos
var esAcentos = "áàäâéèëêíìïîóòöôúùüûÁÀÄÂÉÈËÊÍÌÏÎÓÒÖÔÚÙÜÛ"; //conjunto de vocales con acento

//*****************************************************************************************
//								FUNCIONES AUX DE GESTION DE CADENAS
//*****************************************************************************************
//1- String.prototype.trim -> funcion que hace un trim de un cadena
//2- String.prototype.ellipse -> funcion que establece una longitud maxima de una cadena y sustituye por "..." si dicha longitud es menor que la longitud de la cadena
//3- String.prototype.capitalize -> pone en mayuscula la primera letra de cada palabra de la cadena
//4- String.prototype.clean -> quitan los dobles espacios en blanco
//5- String.prototype.contains(string, s) -> busca "string" en la cadena
//6- esSubset(cadena, lcaracteres) -> comprueba si "cadena" es subcadena de "lcaracteres"
//7- tiene_blancos(cadena) -> comprueba si "cadena" tiene espacios en blanco entremedias
//8- tiene_especiales(cadena)	-> comprueba si "cadena" tiene caracteres especiales
//9- tiene_acentos(cadena) -> comprueba si "cadena" tiene acentos
//10- tiene_1_numeros(cadena) -> comprueba si "cadena" tiene al menos un numero
//11- soloLetras(cadena) -> verifca que una cadena contenga solo letras
//12- parsea(variable,car,cambio) -> sustituye "car"  por "cambio" en "variable"
//13- es_valido_campo_texto(campo_formulario,alerta_vacio,alerta_vocal,cantidad_carecteres_minimo,cantidad_caracteres_maximo,alerta_minimo,alerta_maximo)
//14- tiene_cantidad_de_caracteres_validos(campo_formulario,cantidad,max_o_min) -> validamos que la cadena tenga una cantidad maxima o minima de caracteres 
//15- tiene_vocal(campo_formulario) -> validamos si la cadena tiene vocales
//16- calculaExtension(obj) -> calcula la extension de un fichero
//17- URLEncode(parametro) -> formateo de una cadena para su envio en una url
//18- depurarStringAjax(cadena) -> depurar valores especiales de una cadena para su envio por ajax
//19- quitar_caracateres_sms (cadena) -> Eliminar caraceteres especiales que no admiten los SMS de una cadena

//***************************************************************************************************************************

//1)TRIM DE UNA CADENA
String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };
//2)ELIPSIS DE UNA CADENA
String.prototype.ellipse = function(maxLength){
    if(this.length > maxLength){ return this.substr(0, maxLength-3) + '...';}
    return this;
};
//3) CAPITALIZE
/*
	Property: capitalize
		Converts the first letter in each word of a string to Uppercase.
	Example:
		>"i like cookies".capitalize(); //"I Like Cookies"
	Returns:
		the capitalized string
	*/

String.prototype.capitalize = function(){
		return this.replace(/\b[a-z]/g, function(match){
			return match.toUpperCase();
		});
};
//4) CLEAN 
	/*
	Property: clean
		trims (<String.trim>) a string AND removes all the double spaces in a string.

	Returns:
		the cleaned string

	Example:
		>" i      like     cookies      \n\n".clean() //"i like cookies"
	*/
String.prototype.clean = function(){
		return this.replace(/\s{2,}/g, ' ').trim();
};
//5) CONTAINS
/*
	Property: contains
		checks if the passed in string is contained in the String. also accepts an optional second parameter, to check if the string is contained in a list of separated values.

	Example:
		>'a b c'.contains('c', ' '); //true
		>'a bc'.contains('bc'); //true
		>'a bc'.contains('b', ' '); //false
	*/
String.prototype.contains =	function(string, s){
		return (s) ? (s + this + s).indexOf(s + string + s) > -1 : this.indexOf(string) > -1;
};

//6)SUBSECUENCIA DE UNA CADENA
function esSubSet(cadena, lcaracteres){
	  var caracter;
	  var lo_es=false;	  
	  for (var i=0; i < cadena.length; i++) { 
			caracter=cadena.substring(i,i+1);
			if (lcaracteres.indexOf(caracter,0)!=-1) { lo_es=true;	break;}  
	  }
	  return lo_es;
}   
//7) verfica si una CADENA contiene espacios en blanco
function tiene_blancos(cadena){
	var lo_hay=false;
	for (var i=0; i < cadena.length; i++) { 
		if (cadena.substring(i,i+1) == " "){lo_hay=true;break;} 
	}
  	return lo_hay;
}
//8) verfica si una CADENA contiene caracteres especiales
function tiene_especiales(cadena){
	return(esSubSet(cadena, esEspecial))
}
//9) verfica si una CADENA contiene vocales acentuadas
function tiene_acentos(cadena){
	return(esSubSet(cadena, esAcentos))
}
//10) verfica si una CADENA contiene al menos un numero
function tiene_1_numeros(cadena){
	var ok=true;
	if(cadena.search(/\d{1,}/)==-1){ok=false;}
	return ok;
}
//11) Verifica si una cadena solo tiene letras
function soloLetras(cadena){
	if(!RegEsLetra.test(cadena)) return false;
	else return true;
}

//12)PARSEO DE CADENAS(replace)
//parseo de las cajas de texto y de los textareas
function parsea(variable,car,cambio) 
{
	 //Sustituye el caracter "car"  por "cambio" en "variable"	 
	 var salida = new Array();
	 salida = variable.value;		
	longitud=salida.length;
	while(longitud>0)
	{
		salida = salida.replace(car,cambio);
		longitud--;
	}
	variable.value = salida;
}

//13)VALIDACION DE UN CAMPO DE TEXTO
function es_valido_campo_texto(campo_formulario,alerta_vacio,alerta_vocal,cantidad_carecteres_minimo,cantidad_caracteres_maximo,alerta_minimo,alerta_maximo)
{
	var salida = 0;
	mensaje_error_alert = "";
	if (campo_formulario)
	{
		if (campo_formulario.value.trim() == "")
		{
			if ((mensaje_error_alert == "") && alerta_vacio)
			{
				campo_formulario.focus();
				mensaje_error_alert = alerta_vacio;
			}
		}
		else if (!tiene_cantidad_de_caracteres_validos(campo_formulario,cantidad_caracteres_maximo,'1'))
		{
			if (mensaje_error_alert == "")
			{
				campo_formulario.focus();
				mensaje_error_alert = alerta_maximo;
			}
		}
		else if (!tiene_cantidad_de_caracteres_validos(campo_formulario,cantidad_carecteres_minimo,'0'))
		{
			if (mensaje_error_alert == "")
			{
				campo_formulario.focus();
				mensaje_error_alert = alerta_minimo;
			}
		}
		else if (!tiene_vocal(campo_formulario) && alerta_vocal)
		{
			if (mensaje_error_alert == "")
			{
				campo_formulario.focus();
				mensaje_error_alert = alerta_vocal;
			}
		}
		else
		{salida = 1;}
	}
	else
	{salida = 1;}	
	return salida;
}

//14) 
function tiene_cantidad_de_caracteres_validos(campo_formulario,cantidad,max_o_min)
{
	// SI ES MAXIMO DE CARACTERES max_o_min=1 Y SI ES EL MINIMO max_o_min=0
	var salida = 1;
	var nom = new String(campo_formulario.value);
	
	if ((cantidad >= 0) && (cantidad != ""))
	{
		if (max_o_min == 1)  //MAXIMO DE CARACTERES
		{
			if (nom.length > cantidad)
			{
				salida = 0;
			}
		}
		else //MINIMO DE CARACTERES
		{
			if (nom.length < cantidad)
			{
				salida = 0;
			}
		}
	}	
	return salida;
}
//15) verifica si una cadena contiene vocales (sIN ACENTUAR)
/*OLD
function tiene_vocal(campo_formulario)
{
	var salida = 1;
	var nom = new String(campo_formulario.value);
	nom = nom.toLowerCase()
	if (nom.indexOf("a")<0 && nom.indexOf("e")<0 && nom.indexOf("i")<0 && nom.indexOf("o")<0 && nom.indexOf("u")<0){
		salida = 0;
	}	
	return salida;
}
*/
//15) verifica si una cadena contiene vocales (CON y SIN ACENTUAR)
function tiene_vocal(cadena){
	/* chequeo de vocales */
		nuevacadena = new String(cadena);
		nuevacadena = nuevacadena.toLowerCase();
		var vocales = "a,e,i,o,u,á,é,í,ó,à,è,ì,ò,ù,ä,ë,ï,ö,ü";
		var arrayVocales = new Array();
		arrayVocales = vocales.split(",");
		var resultadoValidacion = false;

		for(i=0; i <= (arrayVocales.length-1); i++){
			if (nuevacadena.indexOf(arrayVocales[i])>=0 ){resultadoValidacion=true;}
		}
		return(resultadoValidacion)
}



//16)CALCULA EXTENSION DE UN FICHERO
function calculaExtension(obj){
	//alert("")
	var extension="NO";
	if(obj!=""){	
			longitud1=obj.length;
			parte1=obj.substr(longitud1-6,6);  	
			donde1=parte1.indexOf('.',0)+1;
			ext1=parte1.substr(donde1,longitud1-donde1);
			extension=ext1.toLowerCase();
	}
	return extension;
}

//17) formateo de una cadena para su envio en una url
function URLEncode(parametro)
{	
	// The Javascript escape and unescape functions do not correspond
	// with what browsers actually do...
	var SAFECHARS = "0123456789" +					// Numeric
					"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +	// Alphabetic
					"abcdefghijklmnopqrstuvwxyz" +
					"-_.!~*'()";					// RFC2396 Mark characters
	var HEX = "0123456789ABCDEF";
	
	var plaintext = parametro;
	var encoded = "";
	for (var i = 0; i < plaintext.length; i++ ) {		
		var ch = plaintext.charAt(i);
	    if (ch == " ") {
		    encoded += "+";				// x-www-urlencoded, rather than %20
		} else if (SAFECHARS.indexOf(ch) != -1) {
		    encoded += ch;
		} else {
		    var charCode = ch.charCodeAt(0);
			if (charCode > 255) {
			    alert( "Unicode Character '" 
                        + ch 
                        + "' cannot be encoded using standard URL encoding.\n" +
				          "(URL encoding only supports 8-bit characters.)\n" +
						  "A space (+) will be substituted." );
				encoded += "+";
			} else {
				encoded += "%";
				encoded += HEX.charAt((charCode >> 4) & 0xF);
				encoded += HEX.charAt(charCode & 0xF);
			}
		}
	} // for
	return encoded;
}
//18 formateo de una cadena para su envio via ajax
function depurarStringAjax(cadena){
	var cadena = String(cadena);
	var arreglo_caracteres = new Array();
	
	arreglo_caracteres = ['2013','2014','20AC','0022','0026','003C','003E','0152','0153','0160','0161','0178','02C6','02DC','2021','2026','2022','2019','2018'];
	
	var i=0;
	while (arreglo_caracteres[i]) {
		caracter = "%u"+arreglo_caracteres[i];
		cadena = cadena.replace(eval("/"+caracter+"/g"), "**simbolo_"+arreglo_caracteres[i]+"**");
		i++;
	}
	
	return cadena;
}
//19 Eliminar caraceteres especiales de una cadena
function quitar_caracateres_sms (cadena) {

 especiales = new Array('Á','À','É','È','Í','Ì','Ó','Ò','Ú','Ù','Ý','á','à','é','è','í','ì','ó','ò','ú','ù','ý','\'','´','Æ','Â','Á','Â','À','Å','Ã','Ä','Ð','É','Ê','È','Ë','Í','Î','Ì','Ï','Ñ','Ó','Ô','Ò','Ø','Õ','Ö','Þ','Ú','Û','Ù','Ü','Ý','á','â','æ','à','å','ã','ä','ç','é','ê','è','ð','ë','í','î','ì','ï','ñ','ó','ô','ò','ø','õ','ö','ß','þ','ú','û','ù','ü','ý','ÿ','¨','ª','¬','º');
 
 normales   = new Array('A','A','E','E','I','I','O','O','U','U','Y','a','a','e','e','i','i','o','o','u','u','y','"' ,'"','A','A','A','A','A','A','A','A','D','E','E','E','E','I','I','I','I','N','O','O','O','_','O','O','_','U','U','U','U','Y','a','a','_','a','a','a','a','Ç','e','e','e','o','e','i','i','i','i','n','o','o','o','_','o','o','_','_','u','u','u','u','y','y',' ',' ',' ',' ');

 i=0;
 while (i<especiales.length) {
  cadena = cadena.split(especiales[i]).join(normales[i]);
  i++
 }

 return cadena;

}