﻿//*****************************************************************************************
//								FUNCIONES CON EL TECLADO
//*****************************************************************************************
//solamenteNumeros(e):: solo podemos teclear numeros .Ej de llamada onKeyPress =return solamenteNumeros(event)
//presionarEnter(eventObj, funcion):: ejecuta la funcion al presionar la tecla ENTER

function solamenteNumeros(e,permitePunto,permiteMas,permiteMenos) {
	var evt = e?e:event;	
	var sinPunto = (typeof sinPunto == "undefined")? 0 : sinPunto;	
	var keyAsciiCode = (evt.which)? evt.which : evt.keyCode;
	//alert(keyAsciiCode);		
	/*
	if (sinPunto == 1) {
		if(keyAsciiCode!=8 && keyAsciiCode!=9 && (keyAsciiCode<48 || keyAsciiCode>57))
			return false;
	}
	else {
		if(keyAsciiCode!=46 && keyAsciiCode!=8 && keyAsciiCode!=9 && (keyAsciiCode<48 || keyAsciiCode>57))
			return false;
	}
	*/
	if((permitePunto==0 && keyAsciiCode==46) || (permiteMas == 0 && keyAsciiCode==43) || (permiteMenos==0 && keyAsciiCode==45))
	return false;
	if(keyAsciiCode!=43 && keyAsciiCode!=45 && keyAsciiCode!=46 && keyAsciiCode!=8 && keyAsciiCode!=9 && (keyAsciiCode<48 || keyAsciiCode>57)) 
	return false;

	return true;
}

//*****************************************************************************************
//								FUNCIONES PARA EL EVENTO ONLOAD DE LA PAGINA
//*****************************************************************************************
presionarEnter = function (funcion) {		// Ej: ejecutarOnload(function() { alert('test'); });
	ejecutarFuncion(document, "keypress", function(){	var keyAsciiCode = (arguments[0].which)? arguments[0].which : arguments[0].keyCode;
														if (keyAsciiCode==13){
															funcion.apply(this, arguments);
														}
													});
}

ejecutarOnload = function (funcion) {		// Ej: ejecutarOnload(function() { alert('test'); });	
	ejecutarFuncion(window, "load", funcion);
}

/*
	Esta funcion sirve para ejecutar cualquier evento en cualquier objeto
*/
ejecutarFuncion = function (a,b,c) {
	if (a) {
		if(a.addEventListener){
			a.addEventListener(b,c,false)
		} else if(a.attachEvent){
			a.attachEvent("on"+b,c)
		}
	}
}