﻿//*******************************************************//
//**********  FUNCIONES PARA EL FCK EDITOR  ************//
//1- valor_limpio_editor(nombre_campo_editor) -> obtiene el texto del fckeditor quitando los caracteres HTML
//2- valor_editor(nombre_campo_editor) -> obtiene el texto COMPLETO introducido en el editor
//3- largo_valor_limpio_editor(nombre_campo_editor) -> obtiene el la longitud del texto del fckeditor quitando los caracteres HTML
//4- largo_valor_editor(nombre_campo_editor) -> obtiene el texto COMPLETO introducido en el editor
//5- set_valor_editor(nombre_campo_editor,texto) -> establece el valor del FCKEDITOR
var mensaje_error_alert = "";

function valor_limpio_editor(nombre_campo_editor) {
	var tagsHTML = /<[^>]*>/g;
	var espacio = /&nbsp;/g;
	var oEditor = FCKeditorAPI.GetInstance(nombre_campo_editor);
	var valor = oEditor.GetXHTML( false ).replace(tagsHTML, "");
	valor = valor.replace(espacio, "");	
	return valor;
}

function valor_editor(nombre_campo_editor) {
	var oEditor = FCKeditorAPI.GetInstance(nombre_campo_editor);
	var valor = oEditor.GetXHTML( false );	
	return valor;
}
/* OLD FUNTIONS
function largo_valor_limpio_editor(nombre_campo_editor) {
	var tmp = String(valor_limpio_editor(nombre_campo_editor));
	valor = tmp.length;	
	return valor;
}
*/

/*
function largo_valor_editor(nombre_campo_editor) {	
	var tmp = String(valor_editor(nombre_campo_editor));
	valor = tmp.length;	
	return valor;
}
*/

//3 y 4 MODIFICADAS POR AARON 17/11/2009 por problemas al contar los caracteres
function largo_valor_limpio_editor(nombre_campo_editor) {
	if (!document.getElementById('____FKCHiddenTemporal')) {
		var newObj = document.createElement("input");
		newObj.setAttribute('type', 'hidden');
		newObj.setAttribute('id', '____FKCHiddenTemporal');
		newObj.setAttribute('name', '____FKCHiddenTemporal');
		document.getElementsByTagName("body")[0].appendChild(newObj);

	} else {
		var newObj = document.getElementById('____FKCHiddenTemporal');
	}
	
	var tmp = String(valor_limpio_editor(nombre_campo_editor));
	newObj.value = tmp;
	valor = newObj.value.length;
	
	return valor;
}

function largo_valor_editor(nombre_campo_editor) {
	if (!document.getElementById('____FKCHiddenTemporal')) {
		var newObj = document.createElement("input");
		newObj.setAttribute('type', 'hidden');
		newObj.setAttribute('id', '____FKCHiddenTemporal');
		newObj.setAttribute('name', '____FKCHiddenTemporal');
		document.getElementsByTagName("body")[0].appendChild(newObj);

	} else {
		var newObj = document.getElementById('____FKCHiddenTemporal');
	}
	
	var tmp = String(valor_editor(nombre_campo_editor));
	newObj.value = tmp;
	valor = newObj.value.length;
	
	return valor;
}

function set_valor_editor(nombre_campo_editor,texto) {
	var oEditor = FCKeditorAPI.GetInstance(nombre_campo_editor);	
	var valor = oEditor.SetHTML( texto );	
	return valor;
}