// JavaScript Document
//Tipos de ClickToCall. CTC==máxima prioridad. Lead==formulario grande
var array_typeofctc = ["CTC", "LEAD"];
//var cookiesEnabled = 0;

var whitespace = " \t\n\r";
var reWhitespace = /^\s+$/;

function MM_openBrWindow(theURL, winName, features) { //v2.0
    checkCookie();
    window.open(theURL, winName, features);
}

//Función que pone pixel por javascript
function ponerPixelJS(pix, idProcess, isImage) {
    if (!isImage)
    {
        var s = document.createElement("script");
        s.type = "text/javascript";
    }
    else
    {
        var s = new Image;
        s.width = "1px";
        s.height = "1px";
    }
    pix = pix.replace("[XXfeaturedparamXX]", idProcess);
    s.src = pix;

    $("body").append(s);
}


function __replaceall(msg, needle, reemp) {
    return msg.split(needle).join(reemp);
}

function clearMyMessage(msg)
{
    msg = __replaceall(msg, "<br />", "\n");
    msg = __replaceall(msg, "<br>", "\n");
    msg = __replaceall(msg, "<strong>", "");
    msg = __replaceall(msg, "</strong>", "");
    msg = __replaceall(msg, "<b>", "");
    msg = __replaceall(msg, "<i>", "");
    msg = __replaceall(msg, '</b>', "");
    msg = __replaceall(msg, "</i>", "");
    msg = __replaceall(msg, "<p>&nbsp;</p>", "\n");
    msg = __replaceall(msg, "<p>", "");
    msg = __replaceall(msg, "</p>", "\n");
    return msg;
}

function _showModal(typ, cab, msg) {
    $.facebox.close();
    var alerta = (typeof is_movil !== undefined) && is_movil == '1';

    if (!alerta) {
        $.modal('<div class="innerModal">' + msg + '<br /><center><a href="#0" class="appbutton simplemodal-close">cerrar</a></center></div>', {
            containerCss: {
                backgroundColor: "#333232",
                borderColor: "#333",
                width: "80%",
                "max-width": "500px",
                close: true,
                padding: 0
            },
            onClose: function (dialog) {
                dialog.data.fadeOut('fast', function () {
                    dialog.container.hide('slow', function () {
                        dialog.overlay.slideUp('fast', function () {
                            $.modal.close();
                        });
                    });
                });
            },
            overlayClose: true
        });

    } else {
        alert(clearMyMessage(msg));
        ;
    }
}

function clearForm() {
//	if($("#nombre").length)
    $('#nombre').val('');
//	if($("#email").length)
    $('#email').val('');
//	if($("#telefono").length)
    $('#telefono').val('');
    //$('#apellidos').val('');
    $('select option:selected').removeAttr('selected');
    //$('#cp').val('');
    $("#cblegales").attr("checked", false);
    if (is_ctc == 1 || $("#telefono_ctc").length) {
        $("#telefono_ctc").val('');
    }
}

function ComprobarInsercion(data, ind) {
    var msg = "";
    var b_is_movil = (typeof is_movil !== undefined) && is_movil == '1';
    var v_tduid = (typeof tduid !== undefined) ? tduid : '';
    var pix = "";

    if (data.error == 0 || data.success == true) { //no ha habido error. Le estamos llamando.
        clearForm();

        if (id_client == 9)
        {
            pix = '<img src=\"http://tbl.tradedoubler.com/report?organization=' + trdbl_organization + '&event=' + trbdl_event;
            pix += '&leadNumber=' + data.id + '\"/>';
        }

        if (id_client == 49)
        {
					var arrLayoutType=new Array("d","m","t");
					pix = '<img src=\"http://tbl.tradedoubler.com/report?organization=1782746&event=312822';
					pix += '&leadNumber=' + data.id + '\"/>'; //+ "&affiliate=" + id_source
					window.criteo_q=window.criteo_q || [];window.criteo_q.push({event:"setAccount",account:17749},{event:"setSiteType",type:arrLayoutType[layoutType]},{event:"trackTransaction",id:data.id,item:[{id:"1",price:1,quantity:1}]});// LINE ADDED 2014.18.12 M.F.
        }

        $('body').append(pix);
        //Ponemos el pixel dependiendo del click realizado


        if (cookiesEnabled == 1) {

            if (arr_pixel_JS[0] != "") {
                ponerPixelJS(arr_pixel_JS[0], data.id, arr_pixel_JS[2]);
            }
            if (arr_pixel_JS[1] != "") {
                ponerPixelJS(arr_pixel_JS[1], data.id, arr_pixel_JS[2]);
            }

            //PIxel estático de retargeting
            // pix = "http://wrap.tradedoubler.com/wrap?id=8498";
            //ponerPixelJS(pix);
            //Modificado by LFP 2013.11.19 for Mario
//			$('body').append('<img src="https://secure.adnxs.com/seg?add=1177391&t=2" width="1" height="1" />');
            $('body').append('<img src="https://ad.yieldmanager.com/pixel?id=2441369&t=2" width="1" height="1" />');
            $('body').append('<img src="https://reachandrich.antevenio.com/track/compginc.asp?exptime=90&keyword=SanitasDentalCliente=visita">');

            if (id_client == 9) {	// 20140701 M.F.
                if (!window.mstag)
                    mstag = {loadTag: function () {
                        }, time: (new Date()).getTime()};
                includeScript("//flex.msn.com/mstag/site/c3e2496a-cd20-40a8-b693-d50bf352a3d5/mstag.js", "js");
                mstag.loadTag("analytics", {dedup: "1", domainId: "3165988", type: "1", actionid: "250700"});
                $('body').append('<iframe src="//flex.msn.com/mstag/tag/c3e2496a-cd20-40a8-b693-d50bf352a3d5/analytics.html?dedup=1&domainId=3165988&type=1&actionid=250700" frameborder="0" scrolling="no" width="1" height="1" style="visibility:hidden;display:none" />');

                /*
                 * ADDED 2014.07.29
                 */
                if (id_source == "2422098")
                {
                    includeScript(root_path_local + "js/google_" + id_source + "_" + id_client + ".js", "js");
                }

            }

            if (id_client == 49) {	// 2014.07.08 M.F.
                // START PIXEL M.F. 2014.07.11
                var TDConf = TDConf || {};
                TDConf.Config = {
                    protocol: document.location.protocol,
                    containerTagId: "11331"
                };
                var asinTD = document.createElement("script");

                if (typeof (TDConf) != "undefined") {
                    TDConf.sudomain = ("https:" == document.location.protocol) ? "swrap" : "wrap";
                    TDConf.host = ".tradedoubler.com/wrap";
                    TDConf.containerTagURL = (("https:" == document.location.protocol) ? "https://" : "http://") + TDConf.sudomain + TDConf.host;

                    if (typeof (TDConf.Config) != "undefined") {
                        asinTD.type = 'text/javascript';
                        asinTD.async = true;
                        asinTD.src = TDConf.containerTagURL + "?id=" + TDConf.Config.containerTagId;
                        document.body.appendChild(asinTD);
                    }
                }
                // END PIXEL M.F. 2014.07.11

                if (!window.mstag)
                    mstag = {loadTag: function () {
                        }, time: (new Date()).getTime()};
                includeScript("//flex.msn.com/mstag/site/8251f978-df20-40f0-8f17-90879c794c07/mstag.js", "js");
                mstag.loadTag("analytics", {dedup: "1", domainId: "251359", type: "1", actionid: "250700"});
                $('body').append('<iframe src="//flex.msn.com/mstag/tag/8251f978-df20-40f0-8f17-90879c794c07/analytics.html?dedup=1&domainId=251359&type=1&actionid=250700" frameborder="0" scrolling="no" width="1" height="1" style="visibility:hidden;display:none" />');
                mstag.loadTag("analytics", {dedup: "1", domainId: "3169474", type: "1", actionid: "251359"});
                $('body').append('<iframe src="//flex.msn.com/mstag/tag/8251f978-df20-40f0-8f17-90879c794c07/analytics.html?dedup=1&domainId=3169474&type=1&actionid=251359" frameborder="0" scrolling="no" width="1" height="1" style="visibility:hidden;display:none" />');	// M.F. 2014.07.10

                /*
                 * ADDED 2014.08.01
                 */
                if (id_source == "2422098")
                {
                    includeScript(root_path_local + "js/google_" + id_source + "_" + id_client + ".js", "js");
                }

            }
        }

        if ((typeof googleSEM !== 'undefined') && googleSEM && !googleRemarketingOnly)	// SEM (M.F. 2014.06.26)
        {
            googleAdWords();
            $('#googlePx1').append('<img src="' + googleSEM + '" width="1" height="1" style="border-style:none;" />');
        }

        var txt = "<p>Hemos recogido tus datos <strong>correctamente</strong>.</p><br /><p>En la mayor brevedad posible nuestro equipo Comercial se pondr&aacute; en contacto contigo.</p><br /><p>Gracias por confiar en nosotros.</p><br /><p><strong>Servicio disponible de lunes a viernes laborables, de 10.00 a 21.00 horas.</strong></p>";

        if (!data.success)	// CONTROL PARA EVITAR LA REPETICION DEL MENSAJE FINAL
        {
            if (!b_is_movil)
                _showModal('success', 'Datos recogidos correctamente', txt);
            else
            {
                $.facebox.close();
                alert(clearMyMessage(txt));
            }
        }
        if (id_crea == 10 || id_crea == 27 || id_crea==36) {
            //Necesario para GOOGLE REMARKETING ONLY
            if (typeof dataLayer !== 'undefined') {
                dataLayer.push({'event': 'FormOK'});
            }

        }

    } else {
        msg = '<br />Ha habido un error al procesar los datos.';
        msg += data.mensaje ? '<br /><br />' + data.mensaje + "<br />" : '<br />';
        msg += '<br />Por favor, inténtelo más tarde.';
        if (data.mensaje == 'DUPLICADO') {
            msg = 'Ya existe éste teléfono en el sistema. <br /><br />Si desea ser contactado, por favor, déjenos su teléfono en el formulario superior.';
        } else if (data.mensaje == 'RECHAZADOWS') {

            msg = 'Usted ya es cliente de Jazztel , por favor póngase en contacto con Atención al cliente en el 1565';

        }
        if (!b_is_movil)
            _showModal('error', 'Error en el procesamiento de Datos', '<p>' + msg + '</p>');
        else
        {
            $.facebox.close();
            alert(clearMyMessage(msg));
        }

    }
}


function isEmpty(s) {
    return((s == null) || (s.length == 0));
}

function isWhitespace(s) {
    return(isEmpty(s) || reWhitespace.test(s));
}

//función que validará el formulario grande deprecated
function validateFormOld(ind) {
    $.facebox.loading();
    var ccrea = '';
    var nomb = '';
    var email = '';
    var telf = '';
    var prov = "";
    var error = 0;
    var msg = '';
    var prov = $('#sel_prov').val();
    //modificado by LFP 2013.11.19 Mario
    var prov_text = $('#sel_prov').find('option:selected').text();
    var legales = ($("#cblegales").is(':checked')) ? '1' : '0';
    if (typeof nomb_promo === 'undefined') {
        ccrea = 'genericaDisfuncionErectil';
    } else {
        ccrea = nomb_promo;
    }
    var source = '';
    if (typeof id_source === 'undefined') {
        source = '';
    } else {
        source = id_source;
    }
    nomb = $('#nombre').val();
    email = $('#email').val();
    var apell = $('#apellidos').val();
    telf = $('#telefono').val();
    if (ind == 0 || ind == 2) {
        telf = $('#telefono_ctc').val();
    }
    prov = $('#sel_prov').val();

    //Validamos ahora dependiendo del parámetro ind de entrada.
    if (ind == 1) { //formulario largo

        if (nomb.length < 1 || isWhitespace(nomb) || nomb == 'Nombre y apellidos') {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            $('#nombre').focus();
        }
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono').focus();
        }
        else if (!(comprobarExprRegular(regExTelf, telf))) {
            error = 1;
            msg += "<p>* El número de teléfono solo admite 9 números sin espacio (p.ej 912345678 ó 612345678)</p>";
            $('#telefono').focus();
        }

//        if ((typeof is_prov !== 'undefined') && prov <= 1 && prov.length < 1) {
        if (prov <= 0 || prov.length < 1) {
            error = 1;
            msg += "<p>* Debes seleccionar una Provincia</p>";
            $('#sel_prov').focus();
        }
        if (email.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Email</p>";
            $('#email').focus();
        }
        else if (!(comprobarExprRegular(regExEmail, email))) {
            error = 1;
            msg += "<p>* Formato de Email incorrecto.</p>";
            $('#email').focus();
        }

        if (legales != '1') {
            error = 1;
            msg += "<p>*Debes aceptar la Política de Privacidad</p>";
        }
    } else if (ind == 0) { //Formulario CTC
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono_ctc').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono_ctc').focus();
        }
    } else if (ind == 2) { //Formulario especial con teléfono, nombre y Legales

        if (nomb.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            $('#nombre').focus();
        }
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono_ctc').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono_ctc').focus();
        }
        if (legales != '1') {
            error = 1;
            msg += "<p>*Debes aceptar la Política de Privacidad</p>";
        }
    } else if (ind == 3) { //Formulario especial con teléfono, nombre 
        //alert(ind);   
        if (nomb.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            $('#nombre').focus();
        }
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono').focus();
        }
        //if (legales != '1') {error = 1; msg += "<p>*Debes aceptar la Política de Privacidad</p>";}
    }
    if (error != 0) {

        _showModal('error', 'Error en los Datos Introducidos', msg);
        //$.facebox.close();
        //showModal({title:'Error en los Datos Introducidos',message:msg,type:'success'});
    } else {//Mandamos.
        //modificado por LFP 2013.11.26.
        //Se requiere mandar como si fuese un LEAD (ind==1) y no como un CTC (ind==0)
        if (ind == 3) {
            ind = 1;
        }
        if (ind >= 2) {
            ind = 0;
        }
        var valortype = array_typeofctc[ind];
        var campana = arr_campanas[ind];
        $.ajaxSetup({async: false});
        //$.post(url_ws+file_ws,{idclient:id_client,sourcetype:valortype,campaign:campana,fuente:source,nombre:nomb,em:email,telefono:telf,provincia:prov,cblegales:legales,crea:ccrea},function(data){ComprobarInsercion(data,ind);},'');	
        //modificado by LFP 2013.11.19 Mario
        $.post(root_path_local + "ajaxs/procesar_registro_ctc.php", {idclient: id_client, sourcetype: valortype, campaign: campana, fuente: source, em: email, nombre: nomb, apellidos: apell, telefono: telf, provincia: prov, provincia_text: prov_text, cblegales: legales, crea: ccrea}, function (data) {
            ComprobarInsercion(data, ind);
        }, '');	//,em:email
    }

}
/**
 * Función que comprobará o gestionará la cookie dentro del formulario
 */
function checkCookie() {
    if (cookiesManaged == 0) {
        manageCookies(1);
    }
}

$(document).ready(function () {

    //función que validará el formulario (M.F. 08.04.2014)
    $(".sendData").click(function (el) {
        res = validator.form();
        if (!res)
            return;

        $.facebox.loading();
        var ccrea = "";
        if (typeof nomb_promo === 'undefined') {
            ccrea = 'genericaDisfuncionErectil';
        } else {
            ccrea = nomb_promo;
        }
        var source = "";
        if (typeof id_source === 'undefined') {
            source = "";
        } else {
            source = id_source;
        }

        ind = 1;
        if (is_ctc == 1) {
            ind = 0;
        }

        var valortype = array_typeofctc[ind];
        var campana = arr_campanas[ind];
        $.ajax({
            url: root_path_local + "ajaxs/procesar_registro_ctc.php",
            method: "post",
            dataType: "json",
            data: {
                idclient: id_client,
                sourcetype: valortype,
                campaign: campana,
                fuente: source,
                em: $('#email').val(),
                nombre: $('#nombre').val(),
                apellidos: $('#apellidos').val(),
                telefono: $('#telefono').val(),
                provincia: $('#sel_prov').val(),
                provincia_text: $('#sel_prov').find('option:selected').text(),
                cblegales: 1,
                crea: ccrea
            },
            cache: false,
            async: true,
            success: function (response)
            {
                ComprobarInsercion(response, ind);
            },
            error: function (response) {
                console.log("err code 2");
            }
        })	// ajax end

    });

    $("body").append("<div id='opaque' style='display: none;'></div>");
    $('#telefono').keypress(function (e) {
        return SoloCompatibleTlf(e);
    });
    //Si existe el teléfono_ctc, lo añadimos
    if ($('#telefono_ctc').length) {
        $("#telefono_ctc").keypress(function (e) {
            return SoloCompatibleTlf(e);
        });
    }

    //Política de cookies
    $(':input').focusin(function () {
        if ($(this).attr('id') == "cookie_rejection" || $(this).attr('id') == "cookie_acept")
            return false;
        checkCookie();
    });
    $(':checkbox').click(function (e) {
        checkCookie();
    });
    if (is_ctc == 1) {
        $("#telefono_ctc").keypress(function (e) {
            return SoloCompatibleTlf(e);
        });
    }

    $('#btnProcesar_ctc').click(function (e) {
        e.preventDefault();
        validateFormOld(0);

    }); //btnProcesar_ctc
    /**
     * Nombre y teléfono
     */
    $('#btnProcesar_ctc_movil').click(function (e) {
        e.preventDefault();
        validateFormOld(3);

    });
});

var regExTelf = /([9|6|7|8])+[0-9]{8}/;// /^[6-7]{1}\d{8}$/
var regexOnlyLetters = /^[ñÑáÁéÉíÍóÓúÚçÇa-zA-Z\ \']+$/;
var regexCP = /^[0-9]{4,5}/;
var regExEmail = /^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/;	