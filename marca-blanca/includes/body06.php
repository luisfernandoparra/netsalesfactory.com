<?php
///echo 0;
?>
<header class="lite">
	<a href="http://diseno.sanitasweb.es/plantillas/express.html" id="logo" target="_blank"><img src="img/logo_body06.jpg"></a>
	<div id="super">
			<p class="hh1">SANITAS DENTAL MILENIUM <br><strong>POR SÓLO 7,40 €/MES</strong></p>
	</div>
</header>

<div id="cuerpo" class="row" style="background-image:url(img/background_cuerpo06.jpg); background-position:right top">

	<div id="informacion" class="col4">
    <section class="transparent">
			<div class="marco cyan">
				<p class="sub">SANITAS DENTAL MILENIUM</p>
				<h2>Cuida de tu boca y la de los tuyos por sólo 7,40€</h2>
				<p>Contrata tu seguro anual y obtendrás<br/>1 mes Gratis y un 5% de descuento</p>
				<p><em>Promoción exclusiva:<br/>Sorteo de un fin de semana</em></p>
			</div>
			<span class="bolo"></span>
		</section>
	</div><!--/col4-->

	<div id="accion" class="col8">
		<div style="padding:15px 10px 5px 10px;overflow:hidden;">
			<form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
				<input type="hidden" name="destino" id="destino" value="">
				<input type="hidden" name="funcionalidad" id="funcionalidad" value="342">

				<section class="white">
<!--
					<div class="error">
						<h3 class="tit">¡Vaya!</h3><p>Sucedió algún error al enviar el formulario.<br>Inténtalo de nuevo, por favor.</p>
					</div>

					<div class="ok">
						<h3 class="tit">Estamos en contacto</h3><p>Hemos recibido tus datos correctamente.<br>En breve uno de nuestros agentes contactará contigo.</p>
					</div>
-->
					<h3 id="h3_movil" class="color cyan">Infórmate GRATIS&nbsp;<i class="icono-grande icotel"></i><a class="telf" href="tel:902310122">900.834.152</a></h3>
					<h3 id="h3_web" class="color cyan">Solicita información GRATIS</h3>

					<div id="campos">
						<div class="error"><ul></ul></div>

						<div class="row">
							<div class="col4"><label for="nombre">Nombre</label></div>
							<div class="col8"><input type="text" name="nombre" maxlength="100" id="nombre" value="<?=$nombre?>" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" /></div>
						</div>

						<div class="row">
							<div class="col4">
								<label for="telefono">Teléfono</label>
							</div>

							<div class="col8">
								<input type="tel" maxlength="9" name="telefono" id="telefono" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" />
							</div>
						</div>

						<div class="row">
							<div class="col4">
								<label for="email">E-mail</label>
							</div>

							<div class="col8">
								<input type="email" name="email" maxlength="100" id="email" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" />
							</div>
						</div>

						<div class="row">
							<div class="col4">
								<label id="txt_sel_prov" for="sel_prov">Provincia</label>
							</div>

							<div class="col8">
								<select id="sel_prov" name="sel_prov" required="" data-msg-required="El campo &lt;strong&gt;Provincia&lt;/strong&gt; es obligatorio" data-msg-sel_prov="El campo &lt;strong&gt;Provincia&lt;/strong&gt; no es válido">
									<option value="">Seleccione Provincia</option>
<?php
foreach($arr_prov as $key=>$value)
{
	$selected = '';
	$valor=strtolower(trim($value['name']));
	$valor=str_replace($arr_input,$arr_output,$valor);

	if($valor === $provincia)
	{
		$selected=' selected ';
	}
	echo '<option value=\''.$value['id'].'\''.$selected.'\'>'.$value['name'].'</option>';
}
?>
								</select>
							</div>
						</div>

						<div class="row">
							<div class="col12">
								<input class="button orange sendData" type="button" id="btnProcesar" value="ENVIAR">
							</div>
						</div>

						<div class="row" style="padding-top:10px;">    
							<div class="col12"><p class="mini">El envío del formulario supone la aceptación de la 
								<a class="enlace_condiciones" href="#0" data-ref="proteccion_datos.php">política de privacidad</a> y 
								<a class="enlace_pp" href="#0" data-ref="sanitas_condicionescontratacion_6.php">condiciones generales.</a></p></div><!--/12-->
						</div>
						<div class="clearfix"></div>
					</div>
					<!--/#campos-->
				</section>
			</form>
		</div>
  </div>
	<div class="clearfix"></div>


	<div class="table productos colum4" style="width:100%">
        <div class="coberturas_servicios">
        
            <div class="coberturas">
            
                <ul>
                    <h1>Coberturas</h1>
                    
                    <li>Odontología preventiva.</li>
                    <li>Intervenciones quirúrgicas.</li>
                    <li>Odontología conservadora.</li>
                    <li>Endodoncia.</li>
                    <li>Odontología estética.</li>
                    <li>Odontopediatría.</li>
                    <li>Prótesis.</li>
                    <li>Periodoncia.</li>
                    <li>Ortodoncia.</li>
                    <li>Implantología.</li>
                    <li>Diagnóstico por imagen.</li>
                    <li>Patología Articulación Temporomandibular.</li>
               </ul>
            </div> 
            
            <div class="servicios">
                <ul>
                    <h1>Servicios</h1>
                    
                    <li>Consulta odontológica: exploración y diagnóstico.</li>
                    <li>Limpieza bucal</li>
                    <li>Cirugía</li>
                    <li>Extracción simple, pieza incluida.</li>
                    <li>Estudio Radiológico</li>
                    <li>Estudio Implantológico</li>
                    <li>Ortodoncia: consulta, estudio completo, extracción simple y protector bucal.</li>
                    <li>Diagnóstico por imagen: Cefalometría, Tomografía, Ortopantomografía</li>
                    <li>Férula para blanqueamiento de fotoactivación.</li>
               </ul>
               </br>
            </div>
        </div><!--coberturas_servicios-->

	</div>

	<footer>
		<p class="mini">* Servicio ofrecido por Sanitas S.L. de Diversificación (Sociedad Unipersonal), compañía perteneciente al Grupo Sanitas. Este servicio no está cubierto con carácter general por ninguno de nuestros productos aseguradores con excepción de aquellas pólizas donde se haya acordado expresamente su inclusión dentro de la cobertura asegurada. Consultar precios y disponibilidad del servicio en cada provincia.</p>
	</footer>

</div>
