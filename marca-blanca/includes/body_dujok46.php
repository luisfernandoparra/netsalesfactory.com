<?php
@session_start();
$idLandingFormatted=str_pad($objSiteData->landingId,3,'0',STR_PAD_LEFT);
?>
<style type="text/css">
/*estilos mensajes error*/
div.error label{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
.error{color:#D81E05; background:#FCF1F0;}
div.error ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error ul li{margin:0 0 10 0;list-style-type:none;}
div.error ul li label{font-weight:normal}
div.error, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error, textarea.error{border:2px solid #D81E05;background-color:#FCF1F0;}
input.ok{background-color:#EFFFDA}

.innerModal{
display:inline-block;
background:#fff;
text-align:center;
width:98%;
height:250px;
}

.modal{
height:680px!important;
}

#simplemodal-container a.modalCloseImg{background:url(<?=$path_raiz_aplicacion_local;?>img/close_modal.png)no-repeat 2px 0px rgba(0, 0, 0, 0)!important;right:-8px;top:-14px;}
#facebox img{width:32px!important;height:1px!important;text-align:center;}
#btnProcesar{
width:100%;
margin-top:5%;
margin-bottom:6%;
}
</style>

<script>

var validator;
$(document).ready(function(){
	$("#telefono").val("");

	$("#enviarPorMailSolicitaInfo").validate({	// CHECK PHONE NUMBER
		rules:{
			telefono:{
				required:true,
				remote:{
					url:root_path_local + "includes/phone_check.php",
			    contentType:"application/json; charset=utf-8",  
					data:{cr:idCr},
					dataFilter:function(response){
						var jsonResponse="";
						jsonResponse = JSON.parse(response);
							if(jsonResponse.success == false)
							{
								return "\"" + jsonResponse.errorMessage + "<br>\"";
							}else{
								return true;
							}
						return false;
					}
				}
			}
		},
		errorLabelContainer: $("#enviarPorMailSolicitaInfo div.error")
	});

	validator=$("#enviarPorMailSolicitaInfo").validate({
		errorContainer:$('div.error'),
		errorLabelContainer:$('div.error ul'),
		wrapper: 'li'
	});

	$("#pie_galletero").hide();
});
</script>

	<header>
		<img src="<?=$path_raiz_aplicacion_local?>img/<?=$idLandingFormatted?>/header_<?=$id_crea?>.jpg" class="informate" />
	</header>

	<section class="contenedor">
		<section class="formulario">
			<h1>Infórmate sin compromiso</h1>
			<form action="" method="get" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
	$formScriptName='form_script_'.$idLandingFormatted.'.php';
}

$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
	echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
	include($formScriptName);
?>
			</form>
		</section>
	</section>
