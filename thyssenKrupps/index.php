<?php
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ThyssenKrupp</title>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<link rel="stylesheet" href="thyssenKrupps/css/main.css" type="text/css" />
<script>
$(document).ready(function(){
	$('#condiciones').click(function(){
		$('#modal').show();
	});
	
	$('#cerrar').click(function(){
		$('#modal').hide();
	});
	
	$('#btn_enviar').click(function(){
		var sEmail = $('#T_email').val();
		if ($.trim(sEmail).length == 0) {
			alert('Indique un email válido');
			e.preventDefault();
		}
		if (!validateEmail(sEmail)) {
			alert('Email no válido');
			e.preventDefault();
		}else {
			var control = 0;
			if($("#T_nombre").val()==''){
				alert('indique un nombre');
				control++;
				e.preventDefault();
			}
			if($("#T_apellidos").val()==''){
				alert('indique los apellidos');
				control++;
				e.preventDefault();
			}
			if($("#T_telefono").val()==''){
				alert('indique un teléfono');
				control++;
				e.preventDefault();
			}
			if(control==0){
				if($("#bases").is(':checked')) { 
					var str = $("#frm_contacto").serialize();
					$.get("thyssenKrupps/contacto.php?"+str, function(data){
						$('#T_nombre').val('');
						$('#T_apellidos').val('');
						$('#T_email').val('');
						$('#T_telefono').val('');
                                                      $('<img src="//marketing.net.netsales.es/ts/i4462420/tsa?typ=i&trc=default&ctg=Registro&sid=confirmation&cid=auto" width="1" height="1" border="0">').appendTo('body');
                                                       $('<img src="//marketing.net.netsales.es/ts/i4912421/tsa?typ=i&trc=default&ctg=Registro&sid=confirmation&cid=auto" width="1" height="1" border="0"> ').appendTo('body');
						alert(data);
					});
				}else{
					alert('Debe aceptar las bases');
					e.preventDefault();
				}
			}
			
		}
	});
});
function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		return true;
	}else {
		return false;
	}
}
</script>
</head>

<body>

<div id="modal" style="display:none">
	<div id="modal_cont">
    	<p><strong>POL&Iacute;TICA DE PRIVACIDAD PROTECCI&Oacute;N DE DATOS</strong><br /><br /> De acuerdo con lo establecido en la Ley Org&aacute;nica 15/1999 los datos Personales recogidos por ThyssenKrupp Encasa, S.L., a trav&eacute;s de formularios, correo electr&oacute;nico u otro medio an&aacute;logo, ser&aacute;n objeto de tratamiento e incorporados a los correspondientes ficheros de car&aacute;cter personal de los que ThyssenKrupp Encasa, S.L., ser&aacute; titular y responsable.<br /> Con este objeto, ThyssenKrupp Encasa, S.L., pondr&aacute; a disposici&oacute;n de los Usuarios los recursos t&eacute;cnicos adecuados para que presten de forma inequ&iacute;voca su consentimiento, y para que, de forma previa a la prestaci&oacute;n del consentimiento, pueda acceder a nuestra pol&iacute;tica de protecci&oacute;n de datos. Salvo en los campos en que se indique lo contrario, las respuestas a los datos de car&aacute;cter personal son voluntarias, sin que la falta de contestaci&oacute;n a dichas preguntas implique una merma en la calidad o cantidad de los servicios correspondientes, a menos que se indique otra cosa.</p>
        <p><a  class="button" style="cursor:pointer" id="cerrar">Cerrar</a></p>
    </div>
</div>

<div id="contenedor">
	<img src="thyssenKrupps/images/img_top.jpg"  />
         <form id="frm_contacto" name="frm_contacto">
	<div id="cont_colums">
        <div id="colum_left">
            <img src="thyssenKrupps/images/tit_form.png" />
            <input type="text" class="campos" name="T_nombre" id="T_nombre" placeholder=" Nombre" maxlength="150" />
            <input type="text" class="campos" name="T_apellidos" id="T_apellidos" placeholder=" Apellidos" maxlength="150"/>
            <input type="email" class="campos" name="T_email" id="T_email" placeholder=" Email" />
            <input type="tel" class="campos" name="T_telefono" id="T_telefono" placeholder=" Tel&eacute;fono" maxlength="9" />
            <div class="cont_legal">
                <div class="icon_legal"><input type="checkbox" name="bases" id="bases" value="si" /></div>
                <p>He le&iacute;do y acepto las <a id="condiciones" style="text-decoration:underline; cursor:pointer">bases legales.</a></p>
            	<div class="btn_enviar" id="btn_enviar"></div>
            </div>
        </div>
        <img src="thyssenKrupps/images/img_home.jpg" class="img_home" />
    </div>
         
         </form>
         
    <div id="cont_tit">
    	<img src="thyssenKrupps/images/tit_1.jpg" />
    </div>
    <div id="colum_1" class="colums_bottom">
    	<img src="thyssenKrupps/images/silla_1.jpg" width="326" height="220" />
        <h3>Sillas salvaescaleras</h3>
        <p>La soluci&oacute;n m&aacute;s pr&aacute;ctica, c&oacute;moda y segura para mejorar la movilidad en el hogar. Nuestro equipo de profesionales te asesorar&aacute; en casa para ofrecerte una soluci&oacute;n personalizada a tus necesidades.</p>
    </div>
    <div id="colum_1" class="colums_bottom">
    	<img src="thyssenKrupps/images/silla_2.jpg" width="326" height="220" />
        <h3>Plataformas salvaescaleras</h3>
        <p>Una soluci&oacute;n excepcional pensada especialmente para personas que usan sillas de ruedas. Su versatilidad permite salvar escaleras rectas o curvas y usarlo para transportar otros peque&ntilde;os elementos.</p>
    </div>
    <div id="colum_1" class="colums_bottom">
    	<img src="thyssenKrupps/images/silla_3.jpg" width="339" height="220" />
        <h3>Elevadores verticales</h3>
        <p>Nuestros Home Elevadores est&aacute;n especialmente dise&ntilde;ados para adaptarse a tu hogar como anillo al dedo. Tanto para el interior como el exterior, espacios reducidos o amplios.</p>
    </div>
</div>
<div id="footer">
	<div id="footer_cont">
    	<img src="thyssenKrupps/images/img_bottom.jpg" />
    </div>
</div>

</body>
</html>
