<?php

/**
 * Cron que cogerá los datos y enviuará un log al cliente a una dirección de correo específica
 * @version 1.0 Scratch @date 2016.06.06 @author LFP
 */
$para = 'info@tkec.es';
//$para = 'luisfer.parra@netsales.es';
$from = 'bfernandez@netsales.es';
$subject = 'Log Diario Leads Generados ' . date('Y-m-d');
$cabeceras = 'MIME-Version: 1.0' . "\r\n";
$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Cabeceras adicionales
$cabeceras .= 'To: Thyssen <' . $para . '>' . "\r\n";
$cabeceras .= 'From: Thyssen Log <' . $from . '>' . "\r\n";
//$cabeceras .= 'Cc: birthdayarchive@example.com' . "\r\n";
$cabeceras .= 'Cc: bfernandez@netsalesfactory.com' . "\r\n";
$mensaje = '
	<html>
	<head>
	  <title>Log Diario Leads Generados</title>
	</head>
	<body>';
$file = empty($_REQUEST['fecha']) ? 'thyssen-'.(date('Ymd')-1).'.csv' : 'thyssen-'.$_REQUEST['fecha'].'.csv';

$file_exists = '../temp/'.$file;
//echo $file_exists;
$encontrado = 'NO Encontrado';
if (file_exists($file_exists)) {
    $file_download = 'http://www.elevadorthyssen.com/thyssenKrupps/temp/'.$file;
    $mensaje .= 'Aquí tienes los leads recogidos el día '.(date('Ymd')-1).' para su descarga. <br>';
    $mensaje .= '<a href="'.$file_download.'" target="_blank">'.$file_download.'</a>';
    $encontrado = $file_download;
} else {
    $menaje .= 'Ooops. No se ha encontrado ningún fichero... no hubo leads para ese día.';
}

$mensaje .= '</body></html>';
@mail($para, $titulo, $mensaje, $cabeceras);
//echo $mensaje;
die('OK||Lanzado: '.$encontrado);