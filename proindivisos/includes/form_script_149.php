<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
?>
						<div class="error"><ul></ul></div>
						<p class="callus">Llame GRATIS al <span>900 101 108</span> </p><br /><p>Solicita Informaci&oacute;n<br><span> sin compromiso</span></p>

						<div class="fila">
							<div class="fleft">
								<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda user-name" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre y Apellidos&lt;/strong&gt; es obligatorio" placeholder="Nombre y Apellidos" aria-required="true" />
							</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<input type="tel" maxlength="9" name="telefono" id="telefono" class="celda user-phone" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Teléfono" aria-required="true" />
							</div>
						</div>

						<div class="row">
							<div class="fleft">
								<input type="email" name="email" id="email" maxlength="100" class="celda user-mail" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="E-mail" aria-required="true" />
							</div>
						</div>

            <div class="fila">
							<div class="fleft">
								<input type="text" name="address" id="address" value="" class="celda user-adress" maxlength="200" required="" data-msg-required="El campo &lt;strong&gt;Direcci&oacute;n del proindiviso&lt;/strong&gt; es obligatorio" placeholder="Direcci&oacute;n del proindiviso" aria-required="true">
							</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<textarea maxlength="2000" name="comentarios" id="comentarios" class="celda user-comment" placeholder="Comentarios" required data-msg-required="El campo &lt;strong&gt;comentarios&lt;/strong&gt; es obligatorio"></textarea>
							</div>
						</div>

						<div class="legal" style="padding-top:10px;">
							<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;Acepto la
							<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'privacy_policies.php';?>" data-ref="" target="_blank">Pol&iacute;tica de Privacidad</a>
						</div><!--/12-->

						<div class="row">
							<div class="espacio-btn">
								<input class="green sendData" type="button" id="btnProcesar" name="btnProcesar" value="INF&Oacute;RMESE">
							</div>
						</div>

						<div class="clearfix"></div>
						<a id="callMeHref" href="tel:<?php echo $inbound_phone; ?>">
							<div class="phone-contact">O llame gratis al 900 101 108</div>
						</a>

						<!--<div class="phone-contact">O llame <span>gratis al 900 101 108</span></div>-->

