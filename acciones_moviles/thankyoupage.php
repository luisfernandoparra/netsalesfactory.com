<?php
/**
 * Página de gracias
 */
include('../conf/config_web.php');
ini_set('display_errors',0);
include('conf/config_web.php');
include($path_raiz_includes . 'includes/initiate_thankyoupage.php');
include($path_raiz_includes . 'class/class.pixels.management.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
		<title>¡Gracias por confiar en <?php echo ucwords($landingMainName); ?>!</title>
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width,initial-scale=1"/>
		<link href="<?php echo $path_raiz_aplicacion_local; ?>css/estilos_thanks.css" rel="stylesheet" type="text/css">

</head>
<body>
<header>
		<section class="contenido_cabecera">
			<img src="<?php echo $path_raiz_aplicacion_local.'img/'.$objSiteData->prefixFolders.'/header.jpg'; ?>"/>
		</section>

</header>
<section class="contenedor">
		<section class="thanku">

			<h1>¡Gracias por participar y mucha suerte!</h1>
				<p>Tu registro se ha completado con éxito</p>

				<p>En breve te llamaremos para confirmar tus datos.</p>

				<p>Un cordial saludo.</p>


		</section>
</section>

<?php
/* $id_crea=25;
  $leadNumber = 'test-d6t345';
  $saltarPixeles = true;
  $cookieEnable = true;

echo 'id_crea: ' . $id_crea;
echo ' salta pixel: ' . $saltarPixeles;
new dBug($_REQUEST);
 *  */
 
include($path_raiz_includes . 'includes/pixels_thankyoupage.php');
?>
</body>
</html>