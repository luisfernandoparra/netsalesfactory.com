<?php
/*
 * NOTA ACERCA DE INBOUND:
 * LOS TELEFONOS INBOUND SE ENCUENTRAN EN EL
 * `index.php` AS SER NECESARIO RECIBIR `$id_source`
 * QUE SE PROCESA EN `includes/initiate.php` Y SE
 * CARGA DESPUES DE ESTE SCRIPT DE CONFIGURACION
 * 
 */
//***************************************
// START CONFIG DINAMICA (M.F. 19.11.2013)
$sitesPruebas=($_SERVER['SERVER_ADDR'] == '139.162.246.12' || $_SERVER['SERVER_ADDR'] == '127.0.0.1' || substr($_SERVER['SERVER_ADDR'], 0, 9) == '192.168.2'); // SITIOS PARA PRUEBAS
$paintErrors=false;
$minTimeCookiesStart=30;  // TIEMPO (EN SEGUNDOS) PARA ACTIVAR AUTOMATICAMENTE LAS COOKIES
$subFolderLanding='acciones_moviles';

// START GLOBAL CHECK PHONE VARS (16.06.2014)
$landingMainName='NetsalesSimple';
// END GLOBAL CHECK PHONE VARS

//echo basename($_SERVER['HTTP_HOST']).' '.__FILE__;
if($sitesPruebas){
	$subFolderLanding='acciones_moviles';
  $url_local_condiciones = $fromServer.'/netsalesfactory.com/promociones/';	//ORIGINAL PARA MARAVILLAO / netsalesfactory.com
	$url_local = $fromServer.'/'.$rootMaravillaoFolder.$developersLocalFolder[$developerIp].'promociones';
//echo $developerIp;
	$dir_raiz_aplicacion_local = $rootMaravillaoFolder.$developersLocalFolder[$developerIp].'promociones/' . $subFolderLanding.'/';
//	$path_raiz_aplicacion_local = $fromServer.'/'.$dir_raiz_aplicacion_local . '';
	$path_raiz_aplicacion_local = '/'.$dir_raiz_aplicacion_local . '';
	$path_raiz_includes_local = $path_raiz_includes . $subFolderLanding . '/';
	$url_condiciones = $url_local .'/'.$subFolderLanding .'/condiciones.php';
	$paintErrors=true;
	$minTimeCookiesStart=3000;
  $url_thankyouPage = 'thankyoupage.php';
} else {
	$url_local = 'http://tutarjeta.netsalesfactory.com/nueva-visa-oro';
	$url_local_condiciones = 'http://tutarjeta.netsalesfactory.com/nueva-visa-oro';
	$dir_raiz_aplicacion_local = $dir_raiz_aplicacion . $subFolderLanding.'/';
	$path_raiz_aplicacion_local = '/' . $dir_raiz_aplicacion_local;
	$path_raiz_includes_local = $path_raiz_includes . $subFolderLanding.'/';
	$url_condiciones = $url_local .'/'.$subFolderLanding . '/condiciones.html';
  $url_thankyouPage = $idLandingCr == 33 ?  '/gracias-por-participar-sorteo-iphone6' : '/gracias-por-participar-sorteo-iphone6';
}
// END CONFIG DINAMICA (M.F. 19.11.2013)

//Variable para la tabla REGISTROS, para diferenciar qué cliente es
$id_client = (int)$_REQUEST['idClient'] ? (int)$_REQUEST['idClient'] : 44;

$arr_client_campaign=array(
	44=> array(
		'prefFichExtraccion' => 'NetsalesSimple',
		'refInconcert' => array('100000004', '100000004')
	)
);
//'refInconcert' => array('3', '3')
//by LFP 2014.09.04
//cambiamos el aplicativo a dialApplet
//$file_ws = 'ctcInConcert.php';//ctcDialApplet.php
////Lo mandamos al enrutador
//$file_ws = 'ctcAlisys.php';
$file_ws = 'router.php';

$prefFichExtraccion = $arr_client_campaign[$id_client]['prefFichExtraccion'];
$arr_campanas = $arr_client_campaign[$id_client]['refInconcert'];

//Array con todas las creas existentes. 
/*
 * Notas 2014:
 * 1. El parámetro `manageDevice` condiciona la detección del dispositivo que carga la promo
 * 2. `new_template` gestiona el tipo de modelo a seguir con la composición de la promo,
 * - 0: modo antiguo con archivos distintos para contenidos según PC o MOBILE
 * - 1: modo nuevo un archivo único para todos los dispositivos
 */

$arrSemGoogle=array();	//PARAMETROS PARA GOOGLE SEM

$arr_creas = array(
);
//'..'.'/'.$subFolderLanding. (landings 4-7-8)
//$is_ctc = 1;	// DEPRECATED 2014.05.28

//Parámetros del pixel de TradeDoubler
$organization='';
$event='';
$trbdl_program='';
$secret_pwd_conf='p3Rd€r_N@da';
$secret_usr=date('Y-m-d H:i:s');
$secret_pwd=md5($secret_usr.$secret_pwd_conf);

$nombCliente = 'NetsalesSimple';

//Pixel expecíficos
$isSpecificPixel = 1;

//Array de display
$arr_isSpecificPixel = array(
	);

// START FIXED PIXELS (ADDED 30.01.2014)
$mi_referer=strtolower(@$_SERVER['HTTP_REFERER']);
$arr_email_domain = array('orangemail.es', 'latinmail.com', 'webmail', 'mail');
$pos = false;

if(!empty($mi_referer)){
  foreach($arr_email_domain as $mi_valor)
	{
		$pos = strpos($mi_referer,$mi_valor);
		if ($pos === true)
			break;
	}

//strpos($mi_referer,$mi_valor)
}

if($pos === true || empty($_SERVER['HTTP_REFERER'])){
  $conditionalPixelStatic='';
}else {
  $conditionalPixelStatic='';
}
// END FIXED PIXELS 


/*
 * Para el cliente 2264375, lanzaremos 2... deber·n ser seguidos.
 * El parámetro `isImageObject` permite crear una imagen si está habilitado, en caso contrario se construirá un JS.
 */
$arr_isSpecificPixelJS = array(
	''=>array(0=> ''),
);


/*
 * SE SOBREESCRIBEN EL ARRAY `$arr_creas` CON LOS DATOS ALMACENADOS EN LA BBDD
 * CORRESPONDIENTES A LA PROMOCION ACTUAL SI ESTA EXISTE
 * NECESARIO TAMBIEN PARA MANTENER COMPATIBILIDAD CON LAS PROMOS ANTERIORES
 * 
 * (M.F. mayo 2014)
 * 
 */
if(@$isDataDb && @$objSiteData->landingId)
{
	$arr_creas[$objSiteData->landingId]['title']=$objSiteData->webTitle;
	$arr_creas[$objSiteData->landingId]['body']=$objSiteData->bodyScript;
	$arr_creas[$objSiteData->landingId]['nombpromo']=$objSiteData->landingName;
	$arr_creas[$objSiteData->landingId]['is_mobile']=0;
	$arr_creas[$objSiteData->landingId]['is_compro_prov']=1;
	$arr_creas[$objSiteData->landingId]['condiciones']=$objSiteData->landingTermsFile;
	$arr_creas[$objSiteData->landingId]['protecciondatos']=$objSiteData->landingPrivacyPolicyFile;
	$arr_creas[$objSiteData->landingId]['cookie_css']=$objSiteData->footerCookiesCssFile;
	$arr_creas[$objSiteData->landingId]['mobile_auto_modal_on_open']=$objSiteData->mobileAutoModal;
	$arr_creas[$objSiteData->landingId]['manageDevice']=1;
	$arr_creas[$objSiteData->landingId]['new_template']=1;
	$arr_creas[$objSiteData->landingId]['js']='';
	$arr_creas[$objSiteData->landingId]['skip_check_phone']=$objSiteData->skipCheckPhone;	//	EVITA EL CONTROL DEL NUMERO TELEFONICO (CURL)
}

if($debugModeNew)
{
//	$debugDujok=true;
//echo 'local-conf:<pre>';print_r($objSiteData->semGoogleParams);echo '</pre>';
}
?>