<?php
/*
 * GET BBDD PIXEL FROM AJAX
 * 
 * ONLY $id_crea PIXELS
 * AND COOKIES PERMITTED
 * (LFP - MF 2014.07.05)
*/
@session_start();
include('../../conf/config_web.php');
ini_set('display_errors',$paintErrors);
include('../conf/config_web.php');
include($path_raiz_includes.'includes/initiate.php');
include($path_raiz_includes_local.'includes/local_vars.php');

include($path_raiz_includes.'class/class.pixels.management.php');	// ADDED 2014.07.05
$cls_pix = new pixel(0);

//print_r($_POST);print_r($_SESSION);

$cls_pix->set_id_page(1);	// SET DEFAULT FLAG CONTROL PAGE (nectar)
$cls_pix->set_Cookie_policy(1);
$arrPixelsBBDD=$cls_pix->get_pixels($_SESSION['idCreatividad']);


if(!empty($_POST['id_source']))	// IF EXIST SORCE DATA, THE ELEMENTS ARE ADDED INTO ARRAY
{
	$cls_pix->set_Id_source($_POST['id_source']);
	$arrPixelsBBDD = array_merge($arrPixelsBBDD, $cls_pix->get_pixels($_SESSION['idCreatividad']));
}

$response['success']=count($arrPixelsBBDD) ? true : false;

if($response['success'])
	foreach($arrPixelsBBDD as $pixel)
		$response['pixel'][]=$pixel;

$res=json_encode($response);
unset($cls_pix);
unset($response);
die($res);

?>