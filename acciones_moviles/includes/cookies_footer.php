<?php
@session_start();
$cookiesMinTimeLimitStart = $minTimeCookiesStart; // TIEMPO MAXIMO ANTES DE LANZAR AUTOMATICAMENTE LAS COOKIES
//$_SESSION['cookiesMan']=1;
// START TIME CHECK COOKIES CONTROL INIT VARS
$timeUpdateCheckLanding = 5000;  // INTERVALO DE TIEMPO PARA EJECUTAR AUTOMATICAMENTE AJAX TEMPORIZADO (en milisegundos)
$jsInitStartXCounterLanding = 'var startCheckCookieLanding=0;';
ini_set('display_errors',$paintErrors);

//if((basename($_SERVER["SCRIPT_FILENAME"]) != 'index.php') && @!$_SESSION['sess_cookie_manage']) // ACEPTAR IMPLICITAMENTE AL NAVEGAR POR LA WEB

if (!@isset($_SESSION['startCheckCookieLanding'])) { // START VISITOR TIME COUNTER
    $_SESSION['startCheckCookieLanding'] = ceil(microtime(true));
    $_SESSION['sess_cookie_manage'] = 0;
    $jsInitStartXCounter = 'var startCheckCookieLanding=1;';
}

$elapsedTimeStart = ceil(microtime(true) - $_SESSION['startCheckCookieLanding']);
// END TIME CHECK COOKIES CONTROL INIT VARS
//Modificado por LFP 2013.11.21



/*
 * CARGA DEL ARCHIVO CSS PARA LOS CONTENIDOS Y GESTION DE LAS COOKIES
 */
if(isset($arr_creas) && isset($arr_creas[$id_crea]['cookie_css']) && !empty($arr_creas[$id_crea]['cookie_css']))
{
    $styleSheetFooter = trim($arr_creas[$id_crea]['cookie_css']);
}
else
{
    // START EXCEPCIONES PARA EL CSS SEGUN id_creatividad
    $styleSheetFooter = 'footer_cookies';
    $styleSheetFooterExtra = '';
    switch (@$id_crea['body']){
        case 4:
        case 5:
        case 6:
            $styleSheetFooterExtra = $id_crea['body'];
            break;
    }
    $styleSheetFooter.=$styleSheetFooterExtra . '.css';
    // END EXCEPCIONES PARA EL CSS SEGUN id_creatividad
}

// START GET DEFAULT PIXELS FROM BBDD 2014
if(!empty($id_crea))
	include($path_raiz_includes_local.'includes/pixel_2014_default.php');	// ADDED 2014.07.05

// END GET DEFAULT PIXELS FROM BBDD 2014

?>
<div id="startScroll"></div>
<div id="timerDisplay"></div>

<link href="<?= $path_raiz_aplicacion_local ?>css/<?=$styleSheetFooter?>" rel="stylesheet" type="text/css" />
<script type="text/javascript">

function drawDefaultPix()	// IF COOKIES ARE APPROVED
{
<?php
//if($conditionalPixelStatic)	echo 'includeScript("'.$conditionalPixelStatic.'","js");';	// SETUP THIS DATA: config_web
?>
}

// START COOKIES CONTROL
var checkElapsedTime;
var cookiesEnabled = 0;
var cookiesManaged = 0;
//// END COOKIES CONTROL

function includeScript(file_path, type)
{
  document.head = document.head || document.getElementsByTagName('head'[0]);
  if(type == "js")
  {
		var j = document.createElement("script");
		j.type = "text/javascript";
		j.src = file_path;
		document.head.appendChild(j);
  }
  return;
}

function addNoscript(content,theId) {
	$('<noscript>').prependTo(content).attr({
		id:theId
	}).appendTo('body');
}

function manageCookies(start)
{
	cookiesEnabled = start;
	cookiesManaged = 1;
	$("#pie_galletero_right").slideUp("fast", function() {
		$("#pie_galletero_right").css("width", "0px !important");
		$("#pie_galletero_right").css("width", "0px !important");
		$("#pie_galletero_left").removeClass("pie_galletero_left");
		$(".pie_galletero_left").css("width", "98% !important", function() {
			$("#cookie_info").css("text-align", "center")
			$("#blockTxtPieAsk").css("width","100%");
		});
		$("#cookie_info").css("text-align", "center");
		$("#pie_galletero_left").append("<br /><br />").css("width","98%").css("text-align","left");
	});

	if(start == 1){
		$.ajaxSetup({async: false});
		$.ajax({
			url:"<?= $path_raiz_aplicacion_local ?>includes/pixel_2014_ajax.php",
			method:"post",
			dataType:"json",
			data:{id_source:id_source},
			cache:false
			,success:function(response)
			{
				if(response.pixel != undefined){
					$.each(response.pixel,function(key,val)
					{
						// START GOOGLE NOSCRIPT EXCEPTION
						var pS=val.indexOf("[NOSCRIPT]");
						var pE=val.indexOf("[/NOSCRIPT]");

						if(pE >= 0)
						{
							var pF=(pS-1)-(pE);
							val=val.substr(pF,(Math.ceil(pE) - Math.ceil(pS)-13));
							addNoscript(val,"nos"+key);
						}
						// END GOOGLE NOSCRIPT EXCEPTION

						if(pE < 0)
							$("#footerWebPage").append(val);
					});
				}
			}
		});
		$.ajaxSetup({async: true});
	}
}

$(document).ready(function(){
		$(".closeCookiesPolicy").click(function() {
				$('#info_cook').slideUp("slow");
				return false;
		});

		$("#cookie_acept").click(function() {
				manageCookies(1);
		});

		$("#cookie_rejection").click(function() {
				manageCookies(0);
		});

		$("#pie_galletero_left").click(function(e) {
				e.preventDefault();
				$('#info_cook').show();
				$('html,body').animate({scrollTop:$("#startScroll").offset().top},500);
		});

		$("#pie_galletero").slideDown("slow");
});
</script>

<div id="pie_galletero" style="display:none;">
    <div id="pie_galletero_left" class="pie_galletero_left">
			<span id="blockTxtPieAsk">Utilizamos cookies propias, de analítica y de terceros para mejorar tu experiencia de usuario. Si aceptas la instalación de cookies o continúas navegando, consideramos que aceptas su uso.<br /></span>
    <div style="float:left;width:100%;">
        <center><a href="#startScroll" id="cookie_info" class="smooth">Más información aquí</a></center>
    </div>
    </div>
  <div id="pie_galletero_right"><center>
        <input type="button" id="cookie_acept" name="cookie_acept" value="Acepto" class="cookieButton" /> <input type="button" id="cookie_rejection" name="cookie_rejection" value="No Acepto" class="cookieButton" />
		</center>
    </div><br />
</div>


<div id="info_cook" style='display:none;'>
	<div class="closeCookiesPolicy" style='display:block;float:right;'><a href="#null" >X</a></div>
    <?php
    include($path_raiz_includes_local . 'includes/cookies_footer_txt.php');
    ?>
    <div class="closeCookiesPolicy" style='display:block;float:right;font-size:60% !important;'><a href="#null">Cerrar</a></div>
</div>

<div id="footerWebPage" style="display:none;"></div>
