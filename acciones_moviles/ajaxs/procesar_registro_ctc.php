<?php

@session_start();

/**
 * Modified by LFP 2014.11.18. Añadimos la deduplicación, dependiendo de la crea: 33: email y teléfono. 32: email solo
 * @version 1.5
 */
header('Content-Type: text/html; charset=utf-8');
include('../../conf/config_web.php');
include('../conf/config_web.php');
$resCurl = array();
$raw = '';
$resCurl['error'] = 1;

$nombre = (!empty($_REQUEST['nombre'])) ? trim($_REQUEST['nombre']) : '';
$apellidos = (!empty($_REQUEST['apellidos'])) ? trim($_REQUEST['apellidos']) : '';
$email = (!empty($_REQUEST['em'])) ? strtolower(trim($_REQUEST['em'])) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? $_REQUEST['provincia_text'] : $_REQUEST['provincia'];

$legal = (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : '1';
$permanencia = (!empty($_REQUEST['perm'])) ? trim($_REQUEST['perm']) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;
$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';

$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? trim($_REQUEST['idclient']) : '';
$sourcetype = (!empty($_REQUEST['sourcetype'])) ? trim($_REQUEST['sourcetype']) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? trim($_REQUEST['campaign']) : '';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? trim($_REQUEST['prioridad']) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? trim($_REQUEST['telefono']) : '';

$add_info_1 = (int) $_REQUEST['miembros'];

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

$campos = array('idclient' => $id_client, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad, 'nombre' => $nombre, 'apellidos' => $apellidos, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI, 'vcc' => 'fintel', 'pwd' => $secret_pwd, 'usr' => $secret_usr, 'add_info_1' => $add_info_1);

//,"serverIp"=>"84.127.240.42"
$qs = http_build_query($campos);
$url = $url_ws . $file_ws . '?' . $qs;
//echo $url.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();

if ($telefono != '' || $email) {
    $conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
    if (!$conexion->connectDB()) {
        $msg_err = 'No se ha podido conectar a la BBDD. Error 102';
    } else {
        $cur_conn_id = $conexion->get_id_conexion();
        /* $sql1="SET AUTOCOMMIT=0;";
          $conexion->ejecuta_query($sql1);

          $sql2="BEGIN";
          $conexion->ejecuta_query($sql2);
         */
        $nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
        $nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');
        $emailIns = addcslashes(mysql_real_escape_string(checkSqlInjection($email), $cur_conn_id), '%_');
        //$apellidos = str_replace("  "," ",ucwords(strtolower($apellidos)));
        //$apellidosIns =  addcslashes(mysql_real_escape_string(checkSqlInjection($apellidos),$cur_conn_id),'%_');

        $telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');

        /**
         * DEDUPLICACIÓN.
         */
        $sql = 'select count(id_registro) as numitems from %s where email=\'%s\'';
        $sql = sprintf($sql, $table_registros, $emailIns);
        if ((int) $crea == 33) {
            //SI ES LA 33, se introduce el teléfono y el email, por lo que hay que añadirle
            $sql.= ' and telefono=\'%s\'';
            $sql = sprintf($sql, $telefonoIns);
        }
        $conexion->getResultSelectArray($sql);
        $resDupl = $conexion->tResultadoQuery;
        $duplicado = (int) $resDupl[0]['numitems'] > 0;
        if ($duplicado) {
            $error = 1;
            $msg_err = 'Ya has participado en el sorteo con exactamente los mismos datos';
        } else {
            $ip = getRealIpAddr();
            $url_user = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
            $sql = 'INSERT INTO %s (id_cliente, id_crea, telefono, b_legales, email,ip,url) VALUES (%d, %d, \'%s\', %d, \'%s\', \'%s\', \'%s\');';
            $sql = sprintf($sql, $table_registros, (int) $id_client, (int) $crea, $telefonoIns, $legal, $emailIns,$ip,$url_user);

//echo $sql.'<hr>'.($sql.' - '.$table_registros.' - '.$id_client.' - '.$crea.' - '.$telefonoIns.' - '.$legal.' - '.$email).'<hr><pre>';print_r($_REQUEST);die();
            $conexion->ejecuta_query($sql);
            $id = mysql_insert_id();
            $conexion->disconnectDB();
//echo $id.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();
            /*
              $ch = curl_init();
              $timeout = 0; // set to zero for no timeout
              curl_setopt($ch, CURLOPT_URL, $url);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
              curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
              $raw = curl_exec($ch);
              curl_close($ch);
              $resCurl = json_decode($raw);
              $error = $resCurl->error;
             */

            $error = !$id ? 1 : 0;
            $resCurl['result'] = 'OK';
            $resCurl['mensaje'] = '';
            $resCurl['error'] = $error;
            $resCurl['id'] = $id;
//echo '';print_r($resCurl);
            if ($error) {
                $msg_err = 'Parece que el problema se debe a lo siguiente: "' . $resCurl->mensaje . '".';
            }
        }
    }
} else {
    if ($telefono == '') {
        $error = 1;
        $msg_err = 'Falta el campo Teléfono. Error 104';
    }
}

if ($error) {
    $query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
    $query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
    $conexion->getResultSelectArray($query);
    $resQuery = $conexion->tResultadoQuery;
    $idUpdate = $resQuery[0]['id'];
    $query = 'UPDATE %s SET http_referer=\'ERROR CTC: %s\' WHERE id=\'%d\'';
    $query = sprintf($query, $table_front_actions, $resCurl->mensaje, (int) $idUpdate);
    $conexion->ejecuta_query($query);

    $resCurl = array('result' => 'KO', 'error' => 1, 'mensaje' => $msg_err);
}

$res = json_encode($resCurl);
//echo $res.'<hr>';print_r($resCurl);die();

die($res);
?>