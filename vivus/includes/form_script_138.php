<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
?>
					<div id="campos" class="frmBox">
						<div class="error"><ul></ul></div>
						<p><b>AXA</b> Seguro de Vida</p>
						<div class="fila">
							<div class="fleft">
								<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" maxlength="100" required data-msg-required="El campo <strong>Nombre y Apellidos</strong> es obligatorio" placeholder="Nombre y Apellidos" aria-required="true" class="celda" pattern="[a-zA-Z0-9]+" data-msg-pattern="error!!!!!" data-rule-minlength="3" data-msg-minlength="El campo &lt;strong&gt;Nombre&lt;/strong&gt; debe contener por lo menos tres caracteres" />
							</div>
						</div>

						<div class="fila">
							<div class="col8">
								<input type="tel" maxlength="9" name="telefono" id="telefono" class="celda" required data-msg-required="El campo <strong>Teléfono</strong> es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo <strong>Teléfono</strong> sólo acepta números" data-msg-minlength="El campo <strong>Teléfono</strong> debe contener al menos 9 dígitos" placeholder="Teléfono" aria-required="true" />
							</div>
						</div>

						<div class="fila">
							<div class="col8">
								<input type="email" name="email" id="email" maxlength="100" class="celda" required data-msg-required="El campo <strong> E-mail</strong> es obligatorio" data-msg-email="El campo <strong>Email</strong> no es válido" value="<?=$email?>" placeholder="E-mail" aria-required="true">
							</div>
						</div>

						<div class="legal" style="padding-top:10px;">
								<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;He leído y acepto la 
								<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php';?>" data-ref="" target="_blank">política de privacidad</a>.
						</div>

						<div class="row">
							<div class="espacio-btn">
								<input class="green sendData" type="button" id="btnProcesar" name="btnProcesar" value="Solicita Informaci&oacute;n">
							</div>
						</div>

						<div class="clearfix"></div>
					</div>
