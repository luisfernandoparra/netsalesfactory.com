// JavaScript Document
//Tipos de ClickToCall. CTC==máxima prioridad. Lead==formulario grande
var array_typeofctc = ["CTC", "LEAD"];
//var cookiesEnabled = 0;

function MM_openBrWindow(theURL, winName, features) { //v2.0
    checkCookie();
    window.open(theURL, winName, features);
}

//Función que pone pixel por javascript
function ponerPixelJS(pix) {
    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = pix;
    // Use any selector
    $("head").append(s);
}


function __replaceall(msg, needle, reemp) {
    return msg.split(needle).join(reemp);
}

function _showModal(typ, cab, msg) {
    $.facebox.close();
    var alerta = (typeof is_movil !== undefined) && is_movil == '1';
    if (!alerta) {
        showModal({title: cab
                    , message: msg
                    , type: typ});//1 aler	
    } else { //para moviles
        msg = __replaceall(msg, '<strong>', '');
        msg = __replaceall(msg, '</strong>', '');
        msg = __replaceall(msg, '<p>&nbsp;</p>', '\n');

        msg = __replaceall(msg, '<p>', '');
        msg = __replaceall(msg, '</p>', '\n');
        alert(msg);
    }
}

function clearForm() {
    $('#nombre').val('');
    $('#email').val('');
    $('#telefono').val('');
    //$('#apellidos').val('');
    $('select option:selected').removeAttr('selected');
    //$('#cp').val('');
    $("#cblegales").attr("checked", false);
    if (is_ctc == '1') {
        $("#telefono_ctc").val('');
    }
}

function ComprobarInsercion(dat, ind){
    var data = eval("(" + dat + ")");
    var msg = "";
    var b_is_movil = (typeof is_movil !== undefined) && is_movil == '1';
    var v_tduid = (typeof tduid !== undefined) ? tduid : '';
    var pix = "";

    if (data.error == 0) { //no ha habido error. Le estamos llamando.
        clearForm();
        //Ponemos el pixel dependiendo del click realizado
        //if (vardlfj)
        if (cookiesEnabled == 1) {
            if (arr_pixel_JS[0] != "") {
                ponerPixelJS(arr_pixel_JS[0]);
            }
            if (arr_pixel_JS[1] != "") {
                ponerPixelJS(arr_pixel_JS[1]);
            }
            pix = '<img src=\"http://tbl.tradedoubler.com/report?organization=' + trdbl_organization + '&event=' + trbdl_event;
            if (b_is_movil) { //añadimos el parámetro tduid a la url que salta
                pix += '&tduid=' + v_tduid;
            }
            pix += '&leadNumber=' + data.id + '\"/>';
            $('body').append(pix);
            //PIxel estático de retargeting
            // pix = "http://wrap.tradedoubler.com/wrap?id=8498";
            //ponerPixelJS(pix);
            //Modificado by LFP 2013.11.19 for Mario
            $('body').append('<img src="https://secure.adnxs.com/seg?add=1177391&t=2" width="1" height="1" />');
            $('body').append('<img src="https://ad.yieldmanager.com/pixel?id=2441369&t=2" width="1" height="1" />');
            $('body').append('<img src="https://reachandrich.antevenio.com/track/compginc.asp?exptime=90&keyword=XXXXX=visita">');
        }
        var txt = "<p>Hemos recogido tus datos <strong>correctamente</strong>.</p><p>En la mayor brevedad posible el equipo Comercial de " + nombCliente + " se pondrá en contacto contigo</p><p>Gracias por confiar en " + nombCliente + "</p><p>&nbsp;</p><p><strong>Servicio disponible de lunes a viernes laborables, de 10.00 a 21.00 horas.</strong></p>";
        _showModal('success', 'Datos recogidos correctamente', txt);
    } else {
        msg = 'Ha habido un error al procesar los datos.<br>Por favor, inténtelo más tarde.';
        if (data.mensaje == 'DUPLICADO') {
            msg = 'Ya existe éste teléfono en el sistema. <br><br>Si desea ser contactado, por favor, déjenos su teléfono en el formulario superior.';
        } else if (data.mensaje == 'RECHAZADOWS') {
            msg = 'Usted ya es cliente de Caser, por favor póngase en contacto con Atención al cliente en el 1565';
        }
        _showModal('error', 'Error en el procesamiento de Datos', '<p>' + msg + '</p>');
    }
}


//función que validará el formulario grande
function validateForm(ind) {
    $.facebox.loading();
    var ccrea = '';
    var nomb = '';
    var email = '';
    var telf = '';
    var error = 0;
    var msg = '';
    var dato_2 = $('#DATO_2').val();
    var dato_3 = $('#DATO_3').val();

    //modificado by LFP 2013.11.19 Mario
    var dato_2_text = $('#DATO_2').val();
    var dato_3_text = $('#DATO_3').val();
    var legales = ($("#cblegales").is(':checked')) ? '1' : '0';
    if (typeof nomb_promo === 'undefined') {
        ccrea = 'CASER_1';
    } else {
        ccrea = nomb_promo;
    }
    var source = '';
    if (typeof id_source === 'undefined') {
        source = '';
    } else {
        source = id_source;
    }
    nomb = $('#nombre').val();
    email = $('#email').val();
    var apell = $('#apellidos').val();
    telf = $('#telefono').val();

    if (ind == 0 || ind == 2) {
        telf = $('#telefono_ctc').val();
    }


    //Validamos ahora dependiendo del parámetro ind de entrada.
    if (ind == 1) { //formulario largo
        if (nomb.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            $('#nombre').focus();
        }
        /*if (apell.length<1) {error = 1;msg += "<p>* Debes rellenar el campo Apellidos</p>";$('#apell').focus();}
         else if (!(comprobarExprRegular(regexOnlyLetters,apell))) {error = 1;msg += "<p>* El campo Apellidos contiene caracters no válidos. Solo se permiten letras y espacios</p>";$('#apell').focus();}	
         */
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono').focus();
        }
        else if (!(comprobarExprRegular(regExTelf, telf))) {
            error = 1;
            msg += "<p>* El número de teléfono solo admite 9 números sin espacio (p.ej 912345678 ó 612345678)</p>";
            $('#telefono').focus();
        }
        if (email.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Email</p>";
            $('#email').focus();
        }
        else if (!(comprobarExprRegular(regExEmail, email))) {
            error = 1;
            msg += "<p>* Formato de Email incorrecto.</p>";
            $('#email').focus();
        }

//        if ((typeof is_prov !== 'undefined') && is_prov == '1' && dato_2.length < 1) {
//            error = 1;
//            msg += "<p>* Debes seleccionar un seguro m&eacute;dico</p>";
//            $('#DATO_2').focus();
//        }

        if ((typeof is_prov !== 'undefined') && is_prov == '1' && dato_3.length < 1) {
            error = 1;
            msg += "<p>* Por favor responda a la pregunta para poder hacerle una oferta adaptada a sus necesidades</p>";
            $('#DATO_3').focus();
        }
        if (legales != '1') {
            error = 1;
            msg += "<p>* Debes aceptar la Política de Privacidad</p>";
        }
    } else if (ind == 0) { //Formulario CTC
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono_ctc').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono_ctc').focus();
        }
    } else if (ind == 2) { //Formulario especial con teléfono, nombre y Legales

        if (nomb.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            $('#nombre').focus();
        }
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono_ctc').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono_ctc').focus();
        }
        if (legales != '1') {
            error = 1;
            msg += "<p>*Debes aceptar la Política de Privacidad</p>";
        }
    } else if (ind == 3) { //Formulario especial con teléfono, nombre 
        //alert(ind);   
        if (nomb.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            $('#nombre').focus();
        }
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono').focus();
        }
        //if (legales != '1') {error = 1; msg += "<p>*Debes aceptar la Política de Privacidad</p>";}
    }
    if (error != 0) {
        _showModal('error', 'Error en los Datos Introducidos', msg);
        //$.facebox.close();
        //showModal({title:'Error en los Datos Introducidos',message:msg,type:'success'});
    } else {//Mandamos.
        //modificado por LFP 2013.11.26.
        //Se requiere mandar como si fuese un LEAD (ind==1) y no como un CTC (ind==0)
        if (ind==3) {ind = 1;} 
        if (ind >= 2) {
            ind = 0;
        }
        var valortype = array_typeofctc[ind];
        var campana = arr_campanas[ind];
        //$.post(url_ws+file_ws,{idclient:id_client,sourcetype:valortype,campaign:campana,fuente:source,nombre:nomb,em:email,telefono:telf,provincia:prov,cblegales:legales,crea:ccrea},function(data){ComprobarInsercion(data,ind);},'');	
        //modificado by LFP 2013.11.19 Mario
        $.post(root_path_local + "ajaxs/procesar_registro_ctc.php", {idclient: id_client, sourcetype: valortype, campaign: campana, fuente: source, em: email, nombre: nomb, apellidos: apell, telefono: telf, provincia: 0, DATO_2: dato_2_text, DATO_3: dato_3_text, cblegales: legales, crea: ccrea}, function(data) {
            ComprobarInsercion(data, ind);
        }, '');	//,em:email
    }

}
/**
 * Función que comprobará o gestionará la cookie dentro del formulario
 */
function checkCookie() {
    //alert("hey");
    if (cookiesManaged == 0) {
        timer.stop();
        manageCookies(1);
    }
}

$(document).ready(function() {
    $("body").append("<div id='opaque' style='display: none;'></div>");
    $('#telefono').keypress(function(e) {
        return SoloCompatibleTlf(e);
    });
    //Política de cookies
    $(':input').focusin(function() {
        if($(this).attr('id') == "cookie_rejection" || $(this).attr('id') == "cookie_acept")
		  return false;
        checkCookie();
    });
    $(':checkbox').click(function(e) {
        checkCookie();
    });
    if (is_ctc == '1') {
        $("#telefono_ctc").keypress(function(e) {
            return SoloCompatibleTlf(e);
        });
    }
    $('#btnProcesar').click(function(e) {
        e.preventDefault();
        validateForm(1);

    }); //btnprocesar
    $('#btnProcesar_ctc').click(function(e) {
        e.preventDefault();
        validateForm(0);

    }); //btnProcesar_ctc
    /**
     * Nombre y teléfono
     */
    $('#btnProcesar_ctc_movil').click(function(e) {
        e.preventDefault();
        validateForm(3);

    });
    /*$('#btnProcesar_ctc_resp').click(function(e){
     e.preventDefault();
     validateForm(2);				 
     
     }); //btnProcesar_ctc
     */
//_showModal('success', 'Hola', "TEST");
});

var regExTelf = /([9|6|7|8])+[0-9]{8}/;// /^[6-7]{1}\d{8}$/
var regexOnlyLetters = /^[ñÑáÁéÉíÍóÓúÚçÇa-zA-Z\ \']+$/;
var regexCP = /^[0-9]{4,5}/;
var regExEmail = /^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/;	