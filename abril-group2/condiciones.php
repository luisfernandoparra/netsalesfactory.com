<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Condiciones de Contratación</title>
</head>

<body style="font:Arial,Helvetica,sans-serif,Gadget,sans-serif;color:#000000;background:#fff;">


<div style="width:100%;background:#FFF;padding:0px;">

<h3 style="font-family: Arial, Helvetica, sans-serif;">Condiciones de Contratación

</h3>
<h3 style="font-family: Arial, Helvetica, sans-serif;">Caser:</h3>

<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
Puedes contratar Caser tengas o no otros seguro de Caser tan sólo por 5,95 € persona /mes(1). Precio garantizado durante el primer año de contratación.</p>


<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;"><br />
  - Sin límite de edad para su contratación.<br />
  - Los menores de 6 años no pagan prima, siempre que estén en la póliza con uno de los padres (o tutores).<br />
  - Las mensualidades de la prima neta gratuitas corresponden a los meses 2º y 6º a contar desde la fecha de entrada en vigor del seguro siempre que se encuentre al corriente de pago de la prima neta del seguro dental.<br />
  - Pago de franquicias para los servicios que lo precisen se realizan directamente en consulta.<br />
  - Participación en el coste de los servicios (copago): 3 euros en servicios no franquiciados.<br />
  - Forma de pago mensual, trimestral, semestral y anual. No se aplican descuentos.<br />
  - Sin carencias.<br />
  - Se admiten las patologías dentales preexistentes, es decir, el seguro médico cubre las patologías existentes con anterioridad a la fecha de contratación del producto.<br />
  - Garantía de 10 años en implantes y ortodoncia en las Clínicas Milenium Dental<br />
  <br />

</p>
<h3 style="font-family: Arial, Helvetica, sans-serif;">Caser Milenium:</h3>
 

<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">

 Puedes contratar Caser Dental Milenium tengas o no otro seguro de Caser tan sólo por 7 € persona/mes(1). Precio garantizado durante el primer año de contratación.<br />
<br />
<br />

- Sin límite de edad para su contratación.
  <br />
  - Los menores de 6 años no pagan prima, siempre que estén en la póliza con uno de los padres (o tutores).
  <br />
  - Las mensualidad de la prima neta gratuita corresponde al 2º mes a contar desde la fecha de entrada en vigor del seguro siempre que se encuentre al corriente de pago de la prima neta del seguro dental.<br />
- Pago de franquicias para los servicios que lo precisen se realizan directamente en consulta.<br />
- Sin participación en el coste de los servicios (copago).<br />
- Forma de pago mensual, trimestral, semestral y anual. No se aplican descuentos.
<br />
- Sin carencias.<br />
- Se admiten las patologías dentales preexistentes, es decir, el seguro médico cubre las patologías existentes con anterioridad a la fecha de contratación del producto.<br />
- Garantía de 10 años en implantes y ortodoncia en las Clínicas Milenium Dental<br />
  <br />

</p>


<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">(1) Prima neta mensual por asegurado a la que se le aplicará el recargo del Consorcio de Compensación de Seguros 0,15% sobre la prima neta y todos los gastos e impuestos repercutibles según la normativa aplicable. Primas netas válidas para altas hasta el 31/12/2013 y garantizada durante el primer año de contratación.<br />
  <br />
  
Teléfono de sugerencias odontológicas 24 horas 902 122 402<br />
Caser, Sociedad Anónima de Seguros - c/ Ribera del Loira, nº 52 - 28042 Madrid.

www.sanitas.es   |    © Todos los derechos reservados

</p>
</div>








</body>
</html>
