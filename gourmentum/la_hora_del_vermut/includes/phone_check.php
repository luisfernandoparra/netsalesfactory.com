<?php
/*
 * REMOTE PHONE NUMBER CHECK
 */
@session_start();
(int)$phoneToCheck=$_REQUEST['telefono'] ? $_REQUEST['telefono'] : $_REQUEST['telefono_ctc'];

if(empty($_SESSION['namePromo']))
{
	$res['success']=false;
	$res=json_encode($res);
	die($res);
}

$res=array();
$_SESSION['skipLogAction']=true;	// EVITA LOG INNECESARIO
include('../../conf/config_web.php');
include('../conf/config_web.php');
$_SESSION['skipLogAction']=false;	// SE REACTIVAN LOS LOGS BBDD

if(@$arr_creas[(int)$_REQUEST['cr']]['skip_check_phone'])	// SE OMITE EFECTUAR EL CHECK DEL TELEFONO
{
	$res['success']=true;
	$laAccion='phone-check-skipped';
}
else
{
	include('../../includes/check_phone.class.php');
	$phoneToCheck=str_replace($arr_sql_inject_original, $arr_sql_inject_cambio, (int)$phoneToCheck);
	$objeto=new remoteCheckPhone();
	$objeto->landingMainName=$landingMainName;
	$objeto->phoneCheckUserAccess=$phoneCheckUserAccess;
	$objeto->phoneCheckPasswordAccess=$phoneCheckPasswordAccess;
	$objeto->phoneCheckUrlTarget=$phoneCheckUrlTarget;
	$res['success']=$objeto->phoneCheckRemote($phoneToCheck) ? true : false;	// BRIDGE TO CHECK PHONE NUMBER;
	$res['error']=!$res['success'];
	$res['errorMessage']=$res['success'] ? '' : 'Parece que el número <b>'.$phoneToCheck.'</b> no es correcto.';
	$laAccion=$res['success'] ? 'phone-check-OK;'.$phoneToCheck.';'.$objeto->idChekTel : 'phone-check-ERROR;'.$phoneToCheck.';'.$objeto->idChekTel;
}

$table_front_actions=$prefixTbl.'log_web_actions'.date(Y);
/*
 * start LOG ACTIONS
 */
if(!empty($_SESSION['namePromo']))
{
	$pagToLog=$_SERVER['PHP_SELF'];
	$pagToLog=substr($pagToLog,strrpos($pagToLog,'/')+1);
	$pagToLog=substr($pagToLog,0,strpos($pagToLog,'.'));
	$laAccion=$laAccion ? $laAccion : '?';

	$query='INSERT INTO %s (session_id,fecha,ip_usuario,script,accion,id_crea,action_number)VALUES(\'%s\',NOW(),\'%s\',\'%s\',\'%s\',%d,%d)';
	$query=sprintf($query,$table_front_actions,$_SESSION['namePromo'],$_SERVER['REMOTE_ADDR'],$pagToLog,$laAccion,$idLandingCr,$_SESSION['sessionCountPromo']);
	$conexion->ejecuta_query($query);
}
	$_SESSION['sessionCountPromo']++;
/*
 * end LOG ACTIONS
 */

$res=json_encode($res);
die($res);
?>