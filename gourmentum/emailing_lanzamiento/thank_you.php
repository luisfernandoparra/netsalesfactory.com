<?php
@session_start();
$specificLandingConfig = '';
include('../../conf/config_web.php');
ini_set('display_errors', 1);
include('conf/config_web.php');
$id_crea = 0;
$landingMainName = 'gourmentum';
$email = (!empty($_REQUEST['email'])) ? trim($_REQUEST['email']) : '';
$crea = (!empty($_REQUEST['crea'])) ? strtoupper(trim($_REQUEST['crea'])) : '1';
$cblegales = (!empty($_REQUEST['cblegales']) && $_REQUEST['cblegales'] == '1') ? '1' : '0';

//echo $path_raiz_includes.$subFolderLanding.'/emailing_lanzamiento' .'<pre>';print_r($GLOBALS);print_r($_REQUEST);die('<hr>');



$delaySeconds = 7000;
$nombre = 'UNKNOWN';
$apellidos = 'UNKNOWN';
$newInserted = false;

switch ($crea) {
    case '1':
        $html = './email/coupon.html';
        $subject = 'Gourmentum: Tu código descuento';
        $img_top = 'th_01.png';
        $img_middle = 'th_03.png';
        $img_bottom = 'th_02.png';
        break;
    case 'GMAIL':
        $html = './email/coupon_gmail.html';
        $subject = 'Todo lo que necesitas con un 25% de descuento';
        $img_top = 'logo_gmail.png';
        $img_middle = 'thank_gmail.png';
        $img_bottom = 'boton_gmail.png';
        break;

    default:
        $html = './email/coupon.html';
        $subject = 'Gourmentum: Tu código descuento';
        $img_top = 'th_01.png';
        $img_middle = 'th_03.png';
        $img_bottom = 'th_02.png';
        break;
}



// START BBDD UPDATE
//$id_source = (!empty($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';
if ($email != '' && filter_var($email, FILTER_VALIDATE_EMAIL) && $cblegales == '1') {
    $conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
    $conexion->connectDB();
    $cur_conn_id = $conexion->get_id_conexion();
    $conexion_existe = array();
    if ($email != 'luisfer.parra@dujok.com') {

        $sql_existe = 'select id_registro from %s where id_cliente=%d and email=\'%s\'';
        $sql_existe = sprintf($sql_existe, $table_registros, $id_client, $email);
        $conexion->getResultSelectArray($sql_existe);
        $conexion_existe = $conexion->tResultadoQuery;
    }
    $ip = getRealIpAddr();
    // $url_user = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $url_user = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
    $sql = 'INSERT INTO %s (id_cliente,email,b_legales,ip,url,nombre,apellidos) VALUES (%d,\'%s\',1,\'%s\',\'%s\',\'%s\',\'%s\')';
    $sql = sprintf($sql, $table_registros, $id_client, $email, $ip, $url_user, $nombre, $apellidos);


    if (count($conexion_existe) == 0) {

        $conexion->ejecuta_query($sql);
        $id = mysql_insert_id();
        $newInserted = true;

        // START CSV WRITING
        $dataCsv = @$email;
        $tmpDate = date('Y-m-d');
        $tmpFile = $path_raiz_includes . $subFolderLanding . '/emailing_lanzamiento/subscriptions/' . $tmpDate . '_gourmentum_' . $crea . '.csv';
        $fileHandle = @fopen($tmpFile, 'a');
        @fwrite($fileHandle, $dataCsv . ";\r\n");
        @fclose($fileHandle);
// END CSV WRITING

        try {
            require_once $path_raiz_includes . 'class/mailchimp-api/Mandrill.php';
            $mandrill = new Mandrill($mailchimp_apikey);

            $html = file_get_contents($html);

            $message = array(
                'html' => $html,
                'subject' => $subject,
                'from_email' => 'contacto@gourmentum.com',
                'from_name' => 'Gourmentum.com',
                'to' => array(
                    array(
                        'email' => $email,
                        'type' => 'to'
                    )
                ),
                'track_opens' => true,
                'track_clicks' => true
            );
            $async = false;
            $ip_pool = 'Main Pool';
            $send_at = '';
            /* if ($email=='luisfer.parra@dujok.com'){
              echo "<pre>";
              print_r($message);
              }
             * 
             */

            $result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
            //print_r($result);
        } catch (Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            throw $e;
        }
    } else {
        if ($email != 'luisfer.parra@dujok.com')
            $id = $conexion_existe[0]['id_registro'];
    }

    unset($conexion);
}
// END BBDD UPDATE
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title><?php echo $landingMainName; ?></title>
        <style>
            img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
            a img {border:none;}
            *{margin: 0px; padding:0px; border: 0px;}

            /*STYLES*/
            header.container {width: 40%; margin: 0px auto;}
            header.container img{width: 80%; margin: 0px auto;}
            section {width: 40%; margin: 0px auto;}
            section img {width: 80%; margin: 0px auto;}


            /*TABLET STYLES*/
            @media (min-width: 480px) and (max-width: 960px) {
                header.container {width: 70%; margin: 0px auto;}
                header.container img {width: 100%; margin: 0px auto;}
                section {width: 70%; margin: 0px auto;}
                section img {width: 100%;}
            }


            /*MOBILE STYLES*/
            @media (max-width: 480px) {
                header.container {width: 90%; margin: 0px auto;}
                header.container img  {width: 100%; margin: 0px auto;}
                section {width: 90%; margin: 0px auto;}
                section img {width: 100%; margin: 0px auto;}
            }
            /* Client-specific Styles */
            /*
            #outlook a {padding:0;} 
            body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
            .ExternalClass {width:100%;} 
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} 
            #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
            img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
            a img {border:none;}
            .image_fix {display:block;}
            p {
                margin: 0px 0px !important;
                font-family: Alcefun;
                text-align: center;
                font-size: 22px;
            }
            table td {border-collapse: collapse;}
            table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
            a {color: #0a8cce;text-decoration: none;text-decoration:none!important;}
           
            table[class=full] { width: 100%; clear: both; }
            
            @media only screen and (max-width: 640px) {
                a[href^="tel"], a[href^="sms"] {
                    text-decoration: none;
                    color: #0a8cce; 
                    pointer-events: none;
                    cursor: default;
                }
                .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                    text-decoration: default;
                    color: #0a8cce !important;
                    pointer-events: auto;
                    cursor: default;
                }
                table[class=devicewidth] {width: 440px!important;text-align:center!important;}
                table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
                img[class=banner] {width: 440px!important;height:132px!important;}
                img[class=colimg2] {width: 290PX!important;height:169px!important;}
                img[class=colimg3] {width: 290PX!important;height:59px!important;}

            }
            
            @media only screen and (max-width: 480px) {
                a[href^="tel"], a[href^="sms"] {
                    text-decoration: none;
                    color: #0a8cce; 
                    pointer-events: none;
                    cursor: default;
                }
                .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                    text-decoration: default;
                    color: #0a8cce !important; 
                    pointer-events: auto;
                    cursor: default;
                }
                table[class=devicewidth] {width: 280px!important;text-align:center!important;}
                table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
                img[class=banner] {width: 280px!important;height:84px!important;}
                img[class=colimg2] {width: 280px!important;height:163px!important;}
                td[class=mobile-hide]{display:none!important;}
                td[class="padding-bottom25"]{padding-bottom:25px!important;}
                img[class=colimg3] {width: 280PX!important;height:57px!important;}
            }
            */
        </style>


        <script type="text/javascript">
            /*setTimeout(function () {
             window.location = "<?php echo $urljumpto ?>";
             }, <?php echo $delaySeconds; ?>);
             */

        </script>
    </head>

    <body>
        <?php if ($newInserted) { ?>
            <img src="http://tbl.tradedoubler.com/report?organization=2023827&event=339623&leadNumber=<?= $id ?>" border="0" />   
        <?php } ?>


        <header class="container">
            <img src="<?= $pathLocalHtml ?>img/thankyou/<?php echo $img_top; ?>"/>
        </header>

        <section>
            <img src="<?= $pathLocalHtml ?>img/thankyou/<?php echo $img_middle; ?>"/>
        </section>

        <section>
            <a href="http://gourmentum.com/"><img src="<?= $pathLocalHtml ?>img/thankyou/<?php echo $img_bottom; ?>"/></a>
        </section>


    </body>
</html>