<?php
@session_start();
$specificLandingConfig = '';
include('../../conf/config_web.php');
ini_set('display_errors', 1);
include('conf/config_web.php');
$id_crea = 0;
//$pathLocal=$path_raiz_aplicacion_local.'emailing_lanzamiento/';
$landingMainName = 'gourmentum';
//new dBug($_SERVER);
//RECOGEMOS LOS PARÁMETROS
$email = (!empty($_REQUEST['email'])) ? strtolower(trim($_REQUEST['email'])) : '';

$crea = (empty($_REQUEST['crea'])) ? '' : trim($_REQUEST['crea']);

//echo $path_raiz_aplicacion.'<pre>'.$email;print_r($_REQUEST);print_r($GLOBALS);die('<hr>');
$id_source = (!empty($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title><?php echo $landingMainName; ?></title>
        <style type="text/css">

            div.error{display:none; border:2px solid #D81E05;max-width:60%;}
                        .error{color:#D81E05; background:#FCF1F0;}
                        div.error ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
                        div.error ul li{margin:0 0 10 0;list-style-type:none;}
                        div.error ul li label{font-weight:normal}
                        div.error, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
                        .ok{color:#333333; background-color:#EFFFDA;padding:10px;}
                        input.error, textarea.error{border:2px solid #D81E05;background-color:#FCF1F0;}
                        input.ok{background-color:#EFFFDA}
            
            img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
            a img {border:none;}
            *{margin: 0px; padding:0px; border: 0px;}

            /*STYLES*/
            div.contenedor {width: 70%; margin: auto;}
            header.container {width: 80%; margin: auto;}
            header.container img{width: 40%; margin: auto; display: block;}
            section {width: 70%; margin: 0px auto;}
            section p.texto{font-family: Arial; font-size: 38px; text-align: center; color: #000000;}
            section p.negrita{font-family: Arial; font-size: 38px; text-align: center; color: #000000; font-weight: bold;}
            section img.logo {width: 90%; margin:  auto;}
            section img.desc {width: 100%;}
            section img.cata{float: left; width: 25%; margin-left: 5%;}
            section img.molec{float: left; width: 25%;}
            section img.shuk{float: left; width: 25%;}
            section img.pull{float: left; width: 20%;}
            section.rell{width: 70%; margin: 0px auto;}
            section input.checkbox  label {margin: 0px auto; font-family: Arial, sans-serif; font-size: 14px; color: #000000; text-align: center;}
            section input.checkbox a{font-family: Helvetica; font-size: 14px; color: blue; text-decoration: underline;}
            section input.prerell {width: 80%; border: 1px solid black; font-family: Arial, sans-serif; font size: 14px; color: #000000; padding-left: 5%;}
            section.boton {width: 70%; margin: 0px auto;}
            img.bt {width: 30%; margin: 0px auto; margin-left: 35%;}



            /*TABLET STYLES*/
            @media (min-width: 480px) and (max-width: 960px){
                div.contenedor {width: 80%; margin: auto; overflow: hidden;}
                header.container {width: 80%; margin: auto; display: block; }
                header.container img{width: 70%; margin: 0px auto;}
                section {width: 80%; margin: 0px auto;}
                section p.texto{font-family: Arial; font-size: 25px; text-align: center; color: #000000; padding-top: 2%;}
                section p.negrita{font-family: Arial; font-size: 25px; text-align: center; color: #000000; font-weight: bold;}
                section img.logo {width: 90%; margin: 0px auto; }
                section img.desc {width: 200%; margin-left: -45%;}
                section img.cata{float: left; width: 35%; display: none; }
                section img.molec{float: left; width: 45%;}
                section img.shuk{float: right; width: 50%;}
                section img.pull{float: left; width: 20%;  display:none;}                  	
                section input.checkbox label {margin: 0px auto; font-family: Arial, sans-serif; font-size: 12px; color: #000000; text-align: center;}
                section input.checkbox a{font-family: Arial, sans-serif; font-size: 12px; color: blue; text-decoration: underline;}
                section input.prerell {width: 70%; border: 1px solid black; font-family: Arial, sans-serif; font size: 14px; color: #000000; padding-left: 5%;}
            }

            /*MOBILE STYLES*/
            @media (max-width: 480px) {
                div.contenedor {width: 100%; margin: 0px auto; overflow: hidden;}
                header.container {width: 90%; margin: 0px auto; }
                header.container img  {width: 100%; margin: auto; display: block;}
                section {width: 90%; margin: 0px auto;}
                section p.texto{font-family: Arial; font-size: 18px; text-align: center; color: #000000;}
                section p.negrita{font-family: Arial; font-size: 18px; text-align: center; color: #000000; font-weight: bold; display: none;}
                section img.logo {width: 100%; margin: 0px auto; }
                section img.desc {width: 230%; margin-left: -65%;}
                section img.cata{float: left; width: 35%; display: none; }
                section img.molec{width: 80%; margin-left: 10%;}
                section img.shuk{width: 70%; margin-left: 15%; display: none;}
                section img.pull{float: left; width: 20%;  display:none;}
                section input.checkbox label{width:100%; border: 1px; border-color: black; margin: 0px auto; font-family: Arial, sans-serif; font-size: 10px; color: #000000;}
                section input.checkbox a{font-family: Arial, sans-serif; font-size: 10px; color: blue; text-decoration: underline;}
                section input.prerell {width: 80%; border: 1px solid black; font-family: Arial, sans-serif; font size: 14px; color: #000000; padding-left: 5%;}
                section.boton {width: 90%; margin: 0px auto;}
                img.bt {width: 75%; margin: 0px auto; margin-left: 12%;}
            }
        </style>
        <style type="text/css">
            /*
                        div.error{display:none; border:2px solid #D81E05;max-width:60%;}
                        .error{color:#D81E05; background:#FCF1F0;}
                        div.error ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
                        div.error ul li{margin:0 0 10 0;list-style-type:none;}
                        div.error ul li label{font-weight:normal}
                        div.error, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
                        .ok{color:#333333; background-color:#EFFFDA;padding:10px;}
                        input.error, textarea.error{border:2px solid #D81E05;background-color:#FCF1F0;}
                        input.ok{background-color:#EFFFDA}
            
                        img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
                        a img {border:none;}
                        *{margin: 0px; padding:0px; border: 0px;}
            
                        
                        header.container {width: 40%; margin: 0px auto;  }
                        header.container img{width: 80%; margin: 0px auto;}
                        section {width: 40%; margin: 0px auto;}
                        section img {width: 90%; margin: 0px auto;}
                        section input.checkbox  label {margin: 0px auto; font-family: Arial, sans-serif; font-size: 14px; color: #000000; text-align: center;}
                        section input.checkbox a{font-family: Arial, sans-serif; font-size: 14px; color: blue; text-decoration: underline;}
                        section input.prerell {width: 80%; border: 1px solid black; font-family: Arial, sans-serif; font size: 14px; color: #000000; padding-left: 5%; }
            
                     
                        @media (min-width: 480px) and (max-width: 960px){
                            header.container {width: 70%; margin: 0px auto; }
                            header.container img{width: 100%; margin: 0px auto;}
                            section {width: 70%; margin: 0px auto;}
                            section img {width: 100%; margin: 0px auto;}
                            section input.checkbox {margin: 0px auto; font-family: Arial, sans-serif; font-size: 12px; color: #000000;}
                            section input.checkbox a{font-family: Arial, sans-serif; font-size: 12px; color: blue; text-decoration: underline;}
                            section input.prerell {width: 70%; border: 1px solid black; font-family: Arial, sans-serif; font size: 14px; color: #000000; padding-left: 5%; }
                        }
            
                        
                        @media (max-width: 480px) {
                            header.container {width: 80%; margin: 0px auto; }
                            header.container img  {width: 100%; margin: 0px auto;}
                            section {width: 80%; margin: 0px auto;}
                            section img {width: 100%; margin: 0px auto;}
                            section input.checkbox label{width:100%; border: 1px; border-color: black; margin: 0px auto; font-family: Arial, sans-serif; font-size: 10px; color: #000000;}
                            section input.checkbox a{font-family: Arial, sans-serif; font-size: 10px; color: blue; text-decoration: underline;}
                            section input.prerell {width: 80%; border: 1px solid black; font-family: Arial, sans-serif; font size: 14px; color: #000000; padding-left: 5%; }
                        }
            */
        </style>

        <script type="text/javascript" src="<?= $path_raiz_aplicacion ?>js/jquery-1.10.1.min.js"></script>
        <script type="text/javascript" src="<?= $path_raiz_aplicacion ?>js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?= $path_raiz_aplicacion ?>js/modal/js/jquery.simplemodal.js"></script>

        <script type="text/javascript">
            var root_path = "<?= $path_raiz_aplicacion; ?>";<?php /* ruta global para las imgs del facebox y del modal */ ?>
            var root_path_local = "<?= $path_raiz_aplicacion_local; ?>";<?php /* ruta global para ajax */ ?>
            var nomb_promo = "";
            var id_source = "<?= (int) $id_source ?>";
            var id_client = "<?= $id_client ?>";
            var id_crea = <?= (int) $id_crea ?>;
            var layoutType = <?= @(int) $layoutType ?>;
            var thankyoupage = "<?= @$url_thankyouPage ?>";

            $(document).ready(function () {


                $("#enviarPorMailSolicitaInfo").validate({// CHECK PHONE NUMBER
                    onkeyup: false,
                    onclick: false,
                    submitHandler: function (form) {
                        form.submit();
                    },
                    rules: {
                        cblegales: {
                            required: true
                        },
                        email: {
                            required: true,
                            remote: {
                                url: root_path + "includes/api_email_check.php",
                                contentType: "application/json; charset=utf-8",
                                data: {cr:<?= (int) $id_crea ?>, id_client: id_client},
                                beforeSend: function () {
                                    $("#btnProcesar").prop("disabled", "true").css("opacity", "0.8");
                                },
                                complete: function () {
                                    $("#btnProcesar").prop("disabled", "").css("opacity", "1");
                                },
                                dataFilter: function (response) {
                                    console.log(response);
                                    var jsonResponse = "";
                                    jsonResponse = JSON.parse(response);
                                    if (jsonResponse.success == false)
                                    {
                                        return "\"" + jsonResponse.errorMessage + "\"";
                                    } else {
                                        return true;
                                    }
                                    return false;
                                }
                            }
                        }
                    },
                    errorLabelContainer: $("#enviarPorMailSolicitaInfo div.error")
                });
                /* $("#enviarPorMailSolicitaInfo").validate({
                 onkeyup: false,
                 onclick: false,
                 submitHandler: function (form) {},
                 rules: {
                 , email: {
                 required: true,
                 remote: {
                 url: root_path + "includes/api_email_check.php",
                 contentType: "application/json; charset=utf-8",
                 data: {cr:<?= (int) $id_crea ?>, id_client: id_client},
                 beforeSend: function () {
                 $("#btnProcesar").prop("disabled", "true").css("opacity", "0.8");
                 },
                 complete: function () {
                 $("#btnProcesar").prop("disabled", "").css("opacity", "1");
                 },
                 dataFilter: function (response) {
                 var jsonResponse = "";
                 jsonResponse = JSON.parse(response);
                 if (jsonResponse.success == false)
                 {
                 return "\"" + jsonResponse.errorMessage + "\"";
                 } else {
                 return true;
                 }
                 return false;
                 }
                 }
                 }
                 },
                 errorLabelContainer: $("#enviarPorMailSolicitaInfo div.error3")
                 });*/


                validator = $("#enviarPorMailSolicitaInfo").validate({
                    errorContainer: $('div.error'),
                    errorLabelContainer: $('div.error ul'),
                    wrapper: 'li'
                })

                $("#btnProcesar").click(function () {
                    $("#enviarPorMailSolicitaInfo").submit();
                })
            });

        </script>
    </head>

    <body>
        <!--<img src="http://impes.tradedoubler.com/imp?type(inv)g(22282856)a(2463578)" /> -->
        <div class="contenedor">
            <header class="container" align="center">
                <img class="logo" src="<?= $pathLocalHtml ?>img/start/2.jpg" />
            </header>

            <section>
                <p class="texto">En Gourmentum estamos de estreno</p>
                <p class="negrita">¡Queremos compartirlo contigo!</p>
            </section>

            <section>
                <img class="cata" src="<?= $pathLocalHtml ?>img/start/6.jpg" />
                <img class="molec" src="<?= $pathLocalHtml ?>img/start/7.jpg" />
                <img class="shuk" src="<?= $pathLocalHtml ?>img/start//8.jpg" />
                <img class="pull" src="<?= $pathLocalHtml ?>img/start/9.jpg" />
                <br />
            </section>
            <br />
            <section>
                <p class="texto"><br />Suscríbete y tendrás un</p>
            </section>



            <section>
                <img class="desc" src="<?= $pathLocalHtml ?>img/start/2_04.png" />
            </section></br>
            <form method="post" action="http://gracias-por-participar.gourmentum.com" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
                <?php if (!empty($crea)): ?>
                <input type="hidden" name="crea" id="crea" value="<?php echo $crea;?>"></input>
           <?     endif;
?>
                <section class="rell">
                    <div class="error"><ul></ul></div>
                    <input id="email" name="email" type="email" class="prerell" id="textfield" size="80" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio<br />" data-msg-email="El campo &lt;strong&gt;e-mail&lt;/strong&gt; no es válido<br />" placeholder="e-mail"  value="<?= $email ?>" /> </br> </br>
                    <input class="checkbox" required="true" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;<br />" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla<br />" type="checkbox" name="cblegales" id="cblegales" value="1" />

                    <label for="cblegales">Al registrarme declaro que he leído y acepto las <a href="http://gourmentum.com/content/3-terminos-y-condiciones-de-uso" title="condiciones- Gourmentum" target="_blank"> condiciones generales</a>, la<a href="http://gourmentum.com/content/6-politica-de-privacidad-y-proteccion-de-datos" title="Politica de privacidad-Gourmentum" target="_blank"> política de privacidad</a> y la <a href="http://gourmentum.com/content/8-politica-de-cookies" title="Política de cookies-Gourmentum" target="_blank">política de cookies</a>.</label>

<!---<input type="checkbox" name="checkbox" class="checkbox" />
<label for="checkbox">Al registrarme declaro que he leído y acepto las <a href="http://gourmentum.com/content/3-terminos-y-condiciones-de-uso" title="condiciones- Gourmentum">condiciones generales</a>, la<a href="http://gourmentum.com/content/6-politica-de-privacidad-y-proteccion-de-datos" title="Politica de privacidad-Gourmentum"> política de privacidad</a> y la <a href="http://gourmentum.com/content/8-politica-de-cookies" title="Política de cookies-Gourmentum">política de cookies</a>.</label>
                    -->
                </section>

                </br>

                <section class="boton">
                    <a href="#" id="btnProcesar"><img class="bt" src="<?= $pathLocalHtml ?>img/start/10.jpg"></a>
                </section>
            </form>
        </div>



<?php /*
        <form method="post" action="http://gracias-por-participar.gourmentum.com" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
            <section>
                <div class="error"><ul></ul></div>
                <input id="email" name="email" type="email" class="prerell" id="textfield" size="80" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio<br />" data-msg-email="El campo &lt;strong&gt;e-mail&lt;/strong&gt; no es válido<br />" placeholder="e-mail"  value="<?= $email ?>" /> </br> </br>
                <input class="checkbox" required="true" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;<br />" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla<br />" type="checkbox" name="cblegales" id="cblegales" value="1" />

                <label for="cblegales">Al registrarme declaro que he leído y acepto las <a href="http://gourmentum.com/content/3-terminos-y-condiciones-de-uso" title="condiciones- Gourmentum" target="_blank"> condiciones generales</a>, la<a href="http://gourmentum.com/content/6-politica-de-privacidad-y-proteccion-de-datos" title="Politica de privacidad-Gourmentum" target="_blank"> política de privacidad</a> y la <a href="http://gourmentum.com/content/8-politica-de-cookies" title="Política de cookies-Gourmentum" target="_blank">política de cookies</a>.</label>

            </section>

            <section>
               <!---  <a href="#" id="btnProcesar"><img src="<?= $pathLocalHtml ?>img/start/2_09.png" width="144" height="65" alt="suscripcion" /></a> -->
                <a href="#" id="btnProcesar"><img src="<?= $pathLocalHtml ?>img/start/2_08.png"></a>
            </section>

        </form>

        <!-- End of Header -->
        <!-- Start of main-banner 
        
          <form method="post" action="http://gracias-por-participar.gourmentum.com" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
          <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
          <tr>
          <td align="center">
          <div class="error"><ul></ul></div>
          </td>
          </tr>
          </table>


          <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner">
          <tbody>
          <tr>
          <td>
          <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
          <tbody>

          <td width="100%">
          <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
          <tbody>
          <tr>
          <!-- start of image -->
          <td align="center" st-image="banner-image"><img src="<?=$pathLocalHtml?>img/start/2_06.jpg" width="600" height="109" alt="promo" /></td>
          </tr>
          <tr>
          <!-- start of image -->
          <td align="center" st-image="banner-image"><p>&nbsp;</p>
          <input id="email" name="email" type="email" id="textfield" size="80" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio<br />" data-msg-email="El campo &lt;strong&gt;e-mail&lt;/strong&gt; no es válido<br />" placeholder="e-mail"  value="<?=$email?>" />
          </td>
          </tr>
          <tr>
          <!-- start of image -->
          <td align="center" st-image="banner-image">
          <p>&nbsp;</p>
          &nbsp; &nbsp; &nbsp;
          <input required="true" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;<br />" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla<br />" type="checkbox" name="cblegales" id="cblegales" value="1" />
          <label for="cblegales">Al registrarme declaro que he leído y acepto las <a href="http://gourmentum.com/content/3-terminos-y-condiciones-de-uso" title="condiciones- Gourmentum" target="_blank">condiciones generales</a>, la<a href="http://gourmentum.com/content/6-politica-de-privacidad-y-proteccion-de-datos" title="Politica de privacidad-Gourmentum" target="_blank"> política de privacidad</a> y la <a href="http://gourmentum.com/content/8-politica-de-cookies" title="Política de cookies-Gourmentum" target="_blank">política de cookies</a>.</label>
          </td>
          </tr>
          </tbody>
          </table>
          <!-- end of image -->
          </td>
          </tr>
          </table>
          </td>
          </tr>
          </tbody>
          </table>



          <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
          <tbody>
          <tr>
          <td>
          <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
          <tbody>
          <tr>
          <td width="600" height="30" align="center" style="font-size:1px; line-height:1px;">
          <p>
          <!--<input class="button orange sendData" type="button" id="btnProcesar" name="btnProcesar" value="ENVIAR" src="img/start/2_09.png" />-->
          <a href="#" id="btnProcesar"><img src="<?=$pathLocalHtml?>img/start/2_09.png" width="144" height="65" alt="suscripcion" /></a>
          </p>
          <p>&nbsp; </p>
          <br />
          <br />
          </td>
          </tr>
          </tbody>
          </table>
          </td>
          </tr>
          </tbody>
          </table>
          </form>
         */ ?>
        </body>
        </html>