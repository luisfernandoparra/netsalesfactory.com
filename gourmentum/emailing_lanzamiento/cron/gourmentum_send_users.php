<?php

/**
 * Cron diario que cogerá el fichero generado el día anterior y mandará el fichero almacenado en subscriptions
 */
include('../../../conf/config_web.php');
ini_set('display_errors', 1);

include('../conf/config_web.php');
ini_set('display_errors', '1');
$currDate = strftime('%Y-%m-%d', strtotime("-1 days"));
//$emailto = 'beatriz@gourmentum.com,marta@gourmentum.com';
$emailto = 'luisfer.parra@dujok.com';
$titulo_email = 'Registros Gourmentum de Netsales con fecha ' . $currDate;


$email_from = "csv@netsalesfactory.com";

//$tmpFile = $path_raiz_includes . $subFolderLanding . '/emailing_lanzamiento/subscriptions/' . $currDate . '_gourmentum.csv';
$fileName = $currDate . '_gourmentum.csv';
$filepath = '../subscriptions/' . $fileName;
if (file_exists($filepath)) {
    $email_message = '';
    $msg_email = 'Estimado cliente,<br><br>Adjuntado el fichero CSV de los leads para del día ' . $currDate . ' para la campaña de Gourmentum Descuento casi 50%';
    $fileatt = $filepath; // Path to the file
    $fileatt_type = "application/octet-stream"; // File Type
    $fileatt_name = $fileName; // Filename that will be used for the file as the attachment
    // Who the email is from
    $email_subject = $titulo_email; // The Subject of the email
    $email_txt = $msg_email; // Message that the email has in it

    $headers = "From: " . $email_from;

    $file = fopen($fileatt, 'rb');
    $data = fread($file, filesize($fileatt));
    fclose($file);
    $data = chunk_split(base64_encode($data));

    $semi_rand = md5(time());
   /* $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

    $headers .= "\nMIME-Version: 1.0\n" .
            "Content-Type: multipart/mixed;\n" .
            " boundary=\"{$mime_boundary}\"";

    $email_message .= "This is a multi-part message in MIME format.\n\n" .
            "--{$mime_boundary}\n" .
            "Content-Type:text/html; charset=\"iso-8859-1\"\n" .
            "Content-Transfer-Encoding: 7bit\n\n" .
            $email_txt . "\n\n";

    

    $email_message .= "--{$mime_boundary}\n" .
            "Content-Type: {$fileatt_type};\n" .
            " name=\"{$fileatt_name}\"\n" .
            "Content-Transfer-Encoding: base64\n\n" .
            $data . "\n\n" .
            "--{$mime_boundary}--\n";

    $ok = @mail($email_to, $email_subject, $email_message, $headers);

*/



    /**
     * Con la Api de Mandril
     */
    $mand_mimetype = 'text/csv';
    $mand_base64 = $data;
    $mand_name = $fileName;
    try {
        require_once $path_raiz_includes . 'class/mailchimp-api/Mandrill.php';
        $mandrill = new Mandrill($mailchimp_apikey);
        $html = file_get_contents('../email/email_log_attach.html');
        $html = str_replace('[%%FECHA%%]', $currDate, $html);
        $message = array(
            'html' => $html,
            'subject' => 'Gourmentum: Tus Leads generados '.$currDate,
            'from_email' => 'contacto@gourmentum.com',
            'from_name' => 'Gourmentum.com',
            'to' => array(
                array(
                    'email' => 'beatriz@gourmentum.com',
                    'name'=>'Beatriz',
                    'type' => 'to'
                ),
                array(
                    'email'=>'marta@gourmentum.com',
                    'name'=>'Marta',
                    'type' =>'to'
                )
            ),
            'track_opens' => true,
            'track_clicks' => true,
            'attachments' => array(
                array(
                    'type'=>$mand_mimetype,
                    'name'=>$mand_name,
                    'content'=>$mand_base64
                    )
            )
        );
        $async = false;
        $ip_pool = 'Main Pool';
        $send_at = '';
        $result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
    } catch (Mandrill_Error $e) {
        // Mandrill errors are thrown as exceptions
        echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
        // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
        throw $e;
    }
} else {
    @mail($email_to, $titulo_email . ' SIN REGISTROS', 'SIN REGISTROS');
    die('OK||SIN REGISTROS');
} //else file exists
die('OK||' . $fileName);
