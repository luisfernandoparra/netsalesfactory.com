<?php
$idLandingFormatted=str_pad($objSiteData->landingId,3,'0',STR_PAD_LEFT);
$arrFraseInfForm=array(
25=>'Cuida Tu Salud y Ahorra con Tu Seguro Dental<br>1.800 clínicas dentales, 55 servicios gratuitos, Acceso inmediato, Sin Carencias.',
26=>'50 servicios gratuitos: Limpieza, Tratamiento de caries infantil, Fluoraciones…<br>Descuentos de más del 50% en el resto: Brackets, Ortodoncia invisible…'
);
?>
<style>
.appbutton {
background-color:#009fe4;
background-image:url("img/f-boton-color.png");
background-position:-10px 0;
background-repeat:no-repeat;
border:medium none #00335b;
border-radius:3px;
box-shadow:0 1px 2px #424242;
color:white !important;
cursor:pointer;
display:inline-block;
font:14px Arial,Helvetica,sans-serif;
margin:5px 5px 0 0;
padding:8px 13px;
text-align:center;
text-decoration:none !important;
text-shadow:0 1px #00335b;
width:auto;
}
.innerModal{
box-sizing:border-box;
margin:0;
padding:0;
padding-top:6%;
position:relative;
line-height:22px!important;
font:12px Arial,Helvetica,sans-serif;
}

.blockCall{
color:#fff;
text-decoration:none;
border:none;
}

.boxShadow{
opacity:.8;
}

header img:nth-child(0){
display:block;
width:100%;
text-align:left!important;
}


.showbox{
-webkit-transition:.5s ease-in-out;
-moz-transition:.5s ease-in-out;
-o-transition:.5s ease-in-out;
transition:.5s ease-in-out;
border:0;
}

.showoff:hover{
transform: scale(1.7);
}

.highlightLanding{
-webkit-transition: all 1s ease-in-out;
-moz-transition: all 1s ease-in-out;
-o-transition: all 1s ease-in-out;
transition: all 1s ease-in-out;
/*zoom:1.5;*/
transform:scale(1.5);
}

div.error2{ display : none; border:2px solid #D81E05; }
.error2{color:#D81E05; background:#FCF1F0;}
div.error2 ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error2 ul li{margin:0 0 10 0;list-style-type:none;}
div.error2 ul li label{font-weight:normal}
div.error2, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error2{border:2px solid #D81E05;background-color:#FCF1F0;}

</style>

<script type="text/javascript">
var errCheckTel=1;

$(document).ready(function(){
	var id_sourceTMP=id_source > 0 ? "?fuente="+id_source : "<?=$sitesPruebas ? '?idClient="+id_client+"&id_source="+id_source' : '"'?>;

	function zoomImage(elBlock)
	{
		$("."+elBlock).delay(1000).addClass("highlightLanding").delay(1000,function(){
				$("."+elBlock).delay(500).removeClass("highlightLanding");
		});
	}

	$("#ctcForm").validate({
		rules:{
			telefono_ctc:{
				required:true
			}
		},
		errorLabelContainer: $("#ctcForm div.error2")
	});

	$("#boton_ctc").click(function(){
		validator2.form();
		var phoneBumberCtc=$.trim($('#telefono_ctc').val());
		
		if($.isNumeric(phoneBumberCtc) && phoneBumberCtc.length >= 9)
		{
			var valortype=array_typeofctc[0];
			var campana=arr_campanas[0] ? arr_campanas[0] : 0;
			$.facebox.loading();
			$.ajax({
				url:root_path_local +"includes/phone_check.php",
				method:"post",
				dataType:"json",
				data:{cr:<?=(int)$id_crea?>,telefono_ctc:phoneBumberCtc},
				cache:false,
				async:false,
				success:function(response)
				{
					if(!response.error)
					{

						$.ajax({
							url:root_path_local+"ajaxs/procesar_registro_ctc.php",
							method:"post",
							dataType:"json",
							data:{
								telefono:phoneBumberCtc,
								sourcetype:valortype,
								campaign:campana,
								fuente:id_source,
								idclient:id_client,
								crea:nomb_promo
							},
							cache:false,
							async:false,
							success:function(response)
							{
								if(!response.error)
								{
									ComprobarInsercion(response,0);
								}
								else
								{
									console.log("err code 2C");
								}
							},
							error:function(response){console.log("err2");return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";}
						});
						ComprobarInsercion(response,0);
					}
					else
					{
						console.log("err code 2C");
					}
				},
				error:function(response){console.log("err2");return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";}
			});
		}
			return false;
	})

	validator2=$("#ctcForm").validate({
		errorContainer:$('div.error2'),
		errorLabelContainer:$('div.error2 ul'),
		wrapper: 'li'
	});
	zoomImage("showbox");

<?php
/*
 * SE SOBREESCRIBE EL CONTENIDO DE LA CAJA SI NO CORRESPONDE A ALGUNA DE LAS FUENTES PREDETERMINADAS
 * M.F. 2014.12.02
 */
if(!in_array(@$id_source,@$arr_source_draw_callme))
{
	echo '$(".llamanos").html("");';
}
?>

});
</script>

<header>
	<div class="contenido_centrado">
<?php

if($objSiteData->headerTopContent)	// CARGA LA CABECERA DESDE bbdd SI EXISTE
{
	$drawHeawder=$objSiteData->headerTopContent;	// SE RECUPERA EL CONTENIDO A DIBUJAR DESDE LA BBDD
	/*
	 * SE DIBUJA EL TELEFONO SEGUN LA FUENTE RECIBIDA
	 * IMPORTANTE RECORDAR QUE EN LA BBDD HAY QUE COLOCAR LA STRING '[[drawPhoneCall]]'
	 * DONDE SE QUIERA QUE APAREZCA EL NUMERO DE TELEFONO QUE SE EXTRAE DEL ARRAY DEL index.php
	 * M.F. 2014.12.02
	 */
	$drawHeawder=str_replace('[[drawPhoneCall]]',$source_phone,$drawHeawder);
	echo $drawHeawder;
}
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
///echo $path_raiz_includes_local.$localLandingCuerpoImage;
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
	</div>
</header>

<section class="contenido">
	<div class="contenido_centrado">
		<section class="foto">
			<img class="web" src="img/<?=$idLandingFormatted?>/fondo-familia-nectar.jpg"/>
			<img class="mvl" src="img/<?=$idLandingFormatted?>/fondo-familia-nectar-mvl.jpg"/>
		</section>

		<section class="formulario">
			<form method="post" action="" name="ctcForm" id="ctcForm">
				<div class="error2"><ul></ul></div>
					<p>Te llamamos GRATIS</p>
					<div class="fila">
							<div class="fleft">
								<input type="text" class="celda" id="telefono_ctc" name="telefono_ctc" maxlength="9" value="" placeholder="Teléfono" required data-msg-required="Si deseas que te llamemos es necesario que introduzcas tu n&uacute;mero de &lt;strong&gt;teléfono&lt;/strong&gt;" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo de &lt;strong&gt;teléfono&lt;/strong&gt; para llamarte debe contener al menos 9 dígitos" style="width:140px;" />
								<input name="llamar" type="button" value="Llamádme" id="boton_ctc" />
							</div>
					</div>
			</form>

			<form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
					<input type="hidden" name="destino" id="destino" value="">


<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
	$formScriptName='form_script_'.$idLandingFormatted.'.php';
}

$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
	echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
	include($formScriptName);
?>

				</form>
		</section>
	</div>


	

<?php

if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div style="height:6px;">&nbsp;</div>
';
}
//900.102.287
$defaultLocalPhoneCall='00806449';
///$inbound_phone=($inbound_phone != $inbound_phone_default) ? $inbound_phone : $defaultLocalPhoneCall;	// SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$inbound_phone=$defaultLocalPhoneCall;	//POR EL MOMENTO SOLO SE DIBUJA EL TELEFONO ESPECIFICO
$drawInbound_phone=preg_replace('/\d{3}/','$0.',str_replace('.',null,trim($inbound_phone)),2);
?>

			<!--<div class="clearfix"></div>-->
</section>

<section class="frase_secun">
<?php
/*
 * EX EPCION PARA CAMBIAR FRASE DEBAJO
 * DEL FORMULARIO SEGUN LA CREA
 */
echo $arrFraseInfForm[(int)$idLandingFormatted];
?>
       
</section>

<section class="contenido_info">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
</section>


<footer>
	<div>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
	</div>
</footer>
