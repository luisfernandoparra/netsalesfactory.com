<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);

?>
<style>
.appbutton {
background-color:#009fe4;
background-image:url("img/f-boton-color.png");
background-position:-10px 0;
background-repeat:no-repeat;
border:medium none #00335b;
border-radius:3px;
box-shadow:0 1px 2px #424242;
color:white !important;
cursor:pointer;
display:inline-block;
font:14px Arial,Helvetica,sans-serif;
margin:5px 5px 0 0;
padding:8px 13px;
text-align:center;
text-decoration:none !important;
text-shadow:0 1px #00335b;
width:auto;
}
.innerModal{
box-sizing:border-box;
margin:0;
padding:0;
padding-top:6%;
position:relative;
line-height:22px!important;
font:12px Arial,Helvetica,sans-serif;
}

.blockCall{
color:#fff;
text-decoration:none;
border:none;
}
.msgFormBox{
color:lightgreen;
/*transform:scale(1.6);*/
}
</style>

<script type="text/javascript">
</script>
					<p class="msgFormBox">y si quieres...</p>
					<div id="campos" class="mainForm hide">
						<div class="error"><ul></ul></div>

						<div class="fila ">
							<div class="col8"><input type="text" name="nombre" maxlength="100" id="nombre" value="<?=$nombre?>" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" placeholder="Nombre" /></div>
						</div>

						<div class="fila ">
							<div class="col8">
								<input type="email" name="email" maxlength="100" id="email" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="E-mail" />
							</div>
						</div>

						<div class="fila ">
							<div class="col8">
								<input type="tel" maxlength="9" name="telefono" id="telefono" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Tel&eacute;fono" />
							</div>
						</div>

						<div class="fila ">
							<div class="col8">
								<select ID="miembros" name="miembros" required data-msg-required="El campo &lt;strong&gt;N. de miembros&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-min="3"  data-msg-min="El campo &lt;strong&gt;N. de miembros&lt;/strong&gt; es obligatorio">
										<option value="0">Nº de miembros en la familia</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
								</select>

								<!--<input type="tel" maxlength="9" name="telefono" id="telefono" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Tel&eacute;fono" />-->
							</div>
						</div>

						<div class="clearfix"></div>

						<div class="" style="padding-top:10px;">
							<div class="legal">
									<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />
								He leído y acepto la <a href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php';?>" target="_blank">política de privacidad</a>
							</div><!--/12-->
						</div>

							<div class="espacio_btn">
								<input class="sendData" type="button" id="btnProcesar" name="btnProcesar" value="REGISTRATE" data-direction="down" />
							</div>
						<div class="clearfix"></div>
					</div>
					<!--/#campos-->
