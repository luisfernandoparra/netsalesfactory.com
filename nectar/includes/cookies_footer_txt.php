<?php
@session_start();
// DEFINE COOKIES TEXT VARS 
$cookieCompanyNameFull='FINTEL MARKETING SLU';
$cookieCompanyNameShort='LA AGENCIA';
/*
 * LISTA DE LAS COOKIES UTILIZADAS EN ESTA PAGINA
 */
$arrThirdCookies=array('_ga'=>'Es necesaria para el funcionamiento de Google Analytics y tiene un período de caducidad de 2 años');

?>

<style>
.blockCookiesContainer{
border:0 none;
font:inherit;
margin:0;
vertical-align:baseline;
padding:2%;
line-height:1.6em;
color:#333;
font-size:90%!important;
}
.cookieName{
display:inline;
font-weight:normal;
}
.cookiesHeader{
border-bottom:1px solid #222;
color:#222;
font-weight:bold;
margin-bottom:3px;
padding-top:10px;
}
.cookiesHeader span{
font-size:95%;
font-weight:normal;
}

#cookieList li{
  padding:5px 0;
}
@media (max-width:860px){
	.blockCookiesContainer{padding:2%;}
}
@media (max-width:700px){
	.blockCookiesContainer{padding:4%;}
}
@media (max-width:480px){
	.blockCookiesContainer{padding:8%;font-size:95%!important;}
}
</style>

<div class="blockCookiesContainer">
A los efectos de la Ley Orgánica 15/1999 de Protección de Datos el asegurado consiente de forma expresa que sus datos de carácter personal, incluidos datos de salud, (en adelante los “Datos”), obtenidos como consecuencia de la relación negocial entre las partes, sean incluidos en un fichero cuyo responsable es Sahna-e, Servicios Integrales de Salud, Sociedad Anónima de Seguros y Reaseguros (en adelante “nectar”), con la finalidad de llevar a cabo la relación contractual, la gestión de la actividad aseguradora, prevención e investigación del fraude, valorar y delimitar el riesgo, consultar los Datos en posteriores solicitudes de seguro o en solicitudes de asistencia o prestación para la determinación de coberturas y permitir el cruce de información contenida en distintos ficheros titularidad de la compañía, incluidos datos de salud, y con relación a cualesquiera productos contratados por el asegurado, ya sea con anterioridad a la contratación o de forma posterior. Asimismo consiente: que sus Datos, sean tratados por otras entidades aseguradoras, reaseguradoras, centros médicos, profesionales de la medicina y terceros asistenciales, que por razones de reaseguro, coaseguro o por la operativa en la gestión de siniestros intervenga en la gestión de la póliza y de sus siniestros; que nectar pueda solicitar de profesionales sanitarios y centros médicos información adecuada referente a la salud de los asegurados con la finalidad de la verificación de los coberturas de la póliza, tramitación de siniestros, autorizaciones, prevención e investigación del fraude, atención de reclamaciones y cumplimiento de obligaciones; la grabación de las llamadas telefónicas que realice a nectar, a los efectos de control de calidad de las llamadas y gestión de reclamaciones. Las comunicaciones podrán efectuarse por cualquier medio, incluido el electrónico. El tomador tras haber informado del contenido íntegro de la presente cláusula al resto de asegurados en la póliza, ha obtenido su consentimiento expreso para facilitar a nectar los Datos de dichos asegurados, y para que nectar le facilite tales Datos vinculados a la prestación de los servicios sanitarios, incluido su historia de visita médica. El tomador autoriza a nectar que remita información publicitaria y comercial, incluso por medios electrónicos sobre productos y servicios financieros o de seguros. Con la misma finalidad publicitaria, el tomador consiente la comunicación de los Datos (nombre, apellidos, dirección, nº teléfono, móvil, email) sin necesidad de comunicar la primera cesión a entidades del grupo hna**. Transcurridos 30 días desde la presente, los Datos que reciban las citadas sociedades, se incorporaran en un fichero responsabilidad de cada entidad que reciba los datos, pudiendo el titular de los datos ejercitar sus derechos de acceso, rectificación, cancelación y oposición. En el caso de que se produzca alguna variación en sus Datos, el asegurado lo notificará a nectar para que proceda a su modificación. Una vez finalizada la relación contractual nectar podrá seguir usando los Datos para fines comerciales y publicitarios hasta que se revoque el consentimiento dado o, en cualquier caso, una vez transcurra un año desde la terminación de la relación contractual. El titular de los datos podrá ejercitar los derechos de acceso, rectificación, cancelación y oposición al tratamiento de los mismos o la oposición a los tratamientos y cesiones anteriores por escrito, de forma gratuita, en la dirección de correo electrónico néctar car@nectar.es, o a través del sobre pre franqueado que se le remitirá a su domicilio una vez lo solicite en el teléfono 913 840 458.
</div>
