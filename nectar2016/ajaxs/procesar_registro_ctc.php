<?php

/**
 * Modificación LFP 2015.03.23
 * Añadimos variable para procesar los datos que vienen de acción mobile emailing Gmail
 * Para eso añadimos variable $emailingSource que será true si viene de gmail
 * Si esta variable es true, no hacemos el die del final y tenemos los datos en un array
 * 
 * Modificaciones LFP 2016.07.05
 * A petición de Isabel Sanchez, si vienen los campos have_children e insurance y no vienen los dos a 1, no hacer nada.
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 1);

/**
 * Config loaded viene del fichero bridge. Si está definida y es true, es que ya ha cargado los configs
 */
function getTextBetweenTags($string, $tagname) {
    $pattern = "/<$tagname>(.*?)<\/$tagname>/";
    preg_match($pattern, $string, $matches);
    return $matches[1];
}

if (empty($configLoaded)) {
    include('../../conf/config_web.php');
    include('../conf/config_web.php');
}
$resCurl = array();
$raw = '';
$resCurl['error'] = 1;


$id_crea = (empty($_REQUEST['id_crea'])) ? 99 : (int) $_REQUEST['id_crea'];
$cli = (empty($_REQUEST['cli'])) ? '' : $_REQUEST['cli'];

//$sOutput['id'] = -1;

$emailingSource = (!empty($_REQUEST['emailingsource']) && strtoupper(trim($_REQUEST['emailingsource'])) == 'GMAIL');

$nombre = (!empty($_REQUEST['nombre'])) ? trim($_REQUEST['nombre']) : '';
$apellidos = (!empty($_REQUEST['apellidos'])) ? trim($_REQUEST['apellidos']) : '';
$have_childern = (!empty($_REQUEST['have_childern'])) ? (int) $_REQUEST['have_childern'] : 0;
$have_insurance = (!empty($_REQUEST['have_insurance'])) ? (int) $_REQUEST['have_insurance'] : 0;
$seguir = $id_crea!='117' || (isset($_REQUEST['have_childern']) && $have_childern == '1' && isset($_REQUEST['have_insurance']) && $have_insurance == '1');
$email = (!empty($_REQUEST['em'])) ? trim($_REQUEST['em']) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? $_REQUEST['provincia_text'] : (@$_REQUEST['provincia'] ? $_REQUEST['provincia'] : 0);

$legal = (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : 1;
$permanencia = (!empty($_REQUEST['perm'])) ? trim($_REQUEST['perm']) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;
$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';

$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? trim($_REQUEST['idclient']) : '';
$sourcetype = (!empty($_REQUEST['sourcetype'])) ? trim($_REQUEST['sourcetype']) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? trim($_REQUEST['campaign']) : '0';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? trim($_REQUEST['prioridad']) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? trim($_REQUEST['telefono']) : '';
$use_priority = (!empty($_REQUEST['use_priority']) && is_numeric($_REQUEST['use_priority']) && $_REQUEST['use_priority'] == 1 && $prioridad != '') ? 1 : 0;

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

$campos = array('idclient' => $id_client, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad,
    'nombre' => $nombre, 'apellidos' => $apellidos, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI,
    'vcc' => 'fintel', 'pwd' => $secret_pwd, 'usr' => $secret_usr,
    'use_priority' => $use_priority);

//,"serverIp"=>"84.127.240.42"

if (!empty($_SESSION['ws_ip_id']) && is_numeric($_SESSION['ws_ip_id'])) {
    $campos['idIp'] = $_SESSION['ws_ip_id'];
}

$qs = http_build_query($campos);
$url = $url_ws . $file_ws . '?' . $qs;
$conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
$error = false;
if (!$conexion->connectDB()) {
    $error = true;
    $msg_err = 'No se ha podido conectar a la BBDD. Error 102';
    $resCurl['msg'] = $msg_err;
}
//echo $url.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();
if (!$error & $telefono != '') {

//    if (!$conexion->connectDB()) {
//        $msg_err = 'No se ha podido conectar a la BBDD. Error 102';
//    } else {
    $cur_conn_id = $conexion->get_id_conexion();
    $nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
    $nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');

    $telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
    $sql = 'INSERT INTO %s (id_cliente, nombre, apellidos,telefono,id_provincia,b_legales,email) VALUES (\'%d\',\'%s\',\'%s\',\'%s\',\'%d\', %d,\'%s\');';
    $sql = sprintf($sql, $table_registros, $id_client, $nombreIns . ' ' . $apellidos, $crea, $telefonoIns, (int) @$_REQUEST['provincia'], $legal, $email);
//echo $sql.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();
    $conexion->ejecuta_query($sql);
    $id = mysql_insert_id();
// $conexion->disconnectDB();
//        $fields = array(
//            'Nombre' => $nombreIns,
//            'Apellidos' => $apellidos,
//            'Telefono' => $telefonoIns,
//            'Ciudad' => '',
//            'Mail' => $email,
//            'Comentarios' => '',
//            'cli' => $cli
//        );
//        $fields_string = http_build_query($fields);

    $t = '30600';
    $e = 52200;
    $start = sprintf("%02d%s%02d%s%02d", floor($t / 3600), ':', ($t / 60) % 60, ':', $t % 60);
    $end = sprintf("%02d%s%02d%s%02d", floor($e / 3600), ':', ($e / 60) % 60, ':', $e % 60);
    $suffix = ((date('H:i:s') > $start && date('H:i:s') < $end) ? 'morning-' : 'afternoon-');
    $fileCsv = '../nectarfiles/nectar-' . $suffix . date('Ymd') . '.csv';
    $headerFile = '';
    if ($seguir) {
        if (!file_exists($fileCsv))
            $headerFile = 'Nombre; E-mail; Teléfono; Tiene seguro; Tiene hijos' . "\r\n";
        ;

        $fp = @fopen($fileCsv, 'a') or die('Unable to open file!');
        $line = $headerFile . $nombre . ';' . $email . ';' . $telefono . ';' . $have_insurance . ';' . $have_childern . "\r\n";
        fwrite($fp, $line);
        fclose($fp);
//print_r($_REQUEST);die();
        $result_ws = 1;

        $error = true;
        $response_ws = 0;


//Accedemos a ver el idchannel del emaily telefono
        $cls_misc = new cls_misc();
        $arr_idchann = json_decode($cls_misc->ws_getIdChannels($email, $telefono), true);
        $id_ret = $id . '-' . $arr_idchann['email'] . '-' . $arr_idchann['telf'];
        unset($cls_misc);

        $error = false;
        $response_ws = 1;
    } else {
        $fileCsv = '../nectarfiles/nectar-NOTFilter-' . $suffix . date('Ymd') . '.csv';
         if (!file_exists($fileCsv))
            $headerFile = 'Nombre; E-mail; Teléfono; Tiene seguro; Tiene hijos' . "\r\n";
        

        $fp = @fopen($fileCsv, 'a') or die('Unable to open file!');
        $line = $headerFile . $nombre . ';' . $email . ';' . $telefono . ';' . $have_insurance . ';' . $have_childern . "\r\n";
        fwrite($fp, $line);
        fclose($fp);
        $id_ret = 0;
        $result_ws = 0;
        $response_ws = 0;
    }
    $resCurl['error'] = 0;
    $resCurl['id'] = $id_ret;
//echo "OOOOKKKKKKKK";
    $tmpCrea = $_SESSION['idCreatividad'];
    $tmpName = session_name('nectar2016' . date('Yimis'));

    $sql = 'update %s set response_ws=%d,response_extended=\'%s\' where id_registro=%d';
    $sql = sprintf($sql, $table_registros, $response_ws, $result_ws, $id);
//die($sql);

    $conexion->ejecuta_query($sql);
//}
} else {
    if ($telefono == '') {
        $error = 1;

        $msg_err = 'Falta el campo Teléfono. Error 104';
        $resCurl['msg'] = $msg_err;
    }
}

if ($error) {
    $query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
    $query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
    $conexion->getResultSelectArray($query);
    $resQuery = $conexion->tResultadoQuery;
    $idUpdate = $resQuery[0]['id'];
    $query = 'UPDATE %s SET http_referer=\'ERROR CTC: %s\' WHERE id=\'%d\'';
    $query = sprintf($query, $table_front_actions, $resCurl->mensaje, (int) $idUpdate);
    $conexion->ejecuta_query($query);
}
$conexion->disconnectDB();
if (!$emailingSource) {
    $res = json_encode($resCurl);
    die($res);
}
?>