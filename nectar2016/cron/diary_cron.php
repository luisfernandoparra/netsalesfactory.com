<?php
/**
 * CRON DIARIO X ENVIO DEL DIA ANTERIOR O ENVIO DE LA MAÑANA DEL DIA CORRIENTE
 * DE LOS ARCHIVOS CSV PRESENTES EN LA CARPETA
 * bectar
 * 
 * M.F. 2016.06.20
 * 
 */
header('Content-Type: text/html; charset=utf-8');
include('../../conf/config_web.php');
include('../conf/config_web.php');

// isanchez@netsalesfactory.com, rodrigo.miranda@hna.es
$emailsSend=$sitesPruebas ? 'mario.francescutto@netsales.es' : 'isanchez@netsales.es, rodrigo.miranda@hna.es, luisfer.parra@netsales.es'; //luisfer.parra@netsales.es cesar@cntcommerce.com
$yesterday=date('Ymd') - 1;

$e=52200;
$end=sprintf("%02d%s%02d%s%02d", floor($e/3600), ':', ($e/60)%60, ':', $e%60);
$suffix=(date('H:i:s') > $end && date('H:i:s') < '23:59:59') ? 'morning-' : 'afternoon-';
$fileCsv = 'nectar-'.$suffix.($suffix == 'afternoon-' ? date('Ymd')-1 : date('Ymd')).'.csv';
//$csvName='zeng-'.$yesterday.'.csv';
$downloadLink=$url_local .'/'.$subFolderLanding.'/nectarfiles/'.$fileCsv;

$file=$path_raiz_includes_local.'nectarfiles/'.$fileCsv;

if(!file_exists($file)) {	// SI NO HAY UN ARCHIVO, SE ABORTA EL SCRIPT
	die($file.'--'.$fileCsv);
}

//echo $file.'<hr>'.$fileCsv;die();

$file_size = filesize($file);
$handle = fopen($file, 'r');
$content = fread($handle, $file_size);
fclose($handle);
$content = chunk_split(base64_encode($content));

//echo '<hr><pre>'.$fileCsv;die();

$eol=PHP_EOL;
$separator=md5(uniqid(time()));
$replyto='noreplay@netsales.es';

$subject = 'Archivo CSV NECTAR';

$message= '<h1>ARCHIVO CSV ADJUNTO</h1>Adjunto el archivo CSV con los datos obtenidos del d&iacute;a <b>'.date('d-m-Y',strtotime($yesterday)).'</b><br />Nombre del archivo: <b>'.$fileCsv.'</b>.<br />';
$message.= '<br />Link para descargar el archivo: <a href="'.$downloadLink.'" target="_blank">Descargar el earchivo</a>.';
$message.= '<br />';

$headers .= 'MIME-Version: 1.0' . $eol;
$headers .= 'Content-Type: multipart/mixed; boundary="' . $separator . '"' . $eol;
$headers .= 'Content-Transfer-Encoding: 7bit' . $eol;
$headers .= 'From: Servicio Netsales <alertas@netsales.es>'.$eol;
$headers .= 'Reply-To: '.$replyto.$eol;

// message
$headers .= '--' . $separator . $eol;
$headers .= 'Content-Type: text/html; charset="iso-8859-1"' . $eol;
$headers .= 'Content-Transfer-Encoding: 8bit' . $eol;
$headers .= $eol;
$headers .= $message . $eol;

// attachment
$headers .= '--' . $separator . $eol;
$headers .= 'Content-Type: application/octet-stream; name="' . $fileCsv . '"' . $eol;
$headers .= 'Content-Transfer-Encoding: base64' . $eol;
$headers .= 'Content-Disposition: attachment' . $eol;
$headers .= $content . $eol;
$headers .= '--' . $separator . '--';

@mail($emailsSend, $subject, $message, $headers);
