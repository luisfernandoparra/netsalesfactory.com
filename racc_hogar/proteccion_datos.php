<?php
@session_start();
include('../conf/config_web.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<!--<link href="nuevalanding/dental 21/laser_prostatico.css" rel="stylesheet" type="text/css" />-->
<?php
if($_REQUEST['deviceType'] == 'mob_'){
?>
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=2.0; user-scalable=yes;">
<meta http-equiv="Pragma" content="no-cache">
<?php
}
?>

<style>
.Part{
	font-family:arial,verdana;
	font-size:1em!important;
}
</style>

</head>

<BODY bgcolor=white text=black link=blue vlink=purple alink=fushia >
<DIV class="Part" 

><P 

><FONT size="+1" color="#000000"><B>POL&Iacute;TICA DE PRIVACIDAD  Y PROTECCI&Oacute;N DE DATOS DE AGENCIA DE MEDIACI&Oacute;N  DE SEGUROS  </P
><P 

>FINTEL MARKETING SLU PARA SANITAS S.A. DE SEGUROS </B>  </P
><H1 

><B>1. PRESENTACI&Oacute;N Y DERECHO DE INFORMACI&Oacute;N </H1
><P 

></B>La presente la web, cuyo titular es FINTEL MARKETING SLU, AGENCIA DE SEGUROS de SANITAS S.A. DE SEGUROS (en adelante SANITAS), seg&uacute;n contrato entre ambas partes (en adelante, LA AGENCIA), con CIF B86420213, domicilio en Plaza Callao, n&ordm; 4, 6&ordm; planta, 28013, en MADRID, pone a disposici&oacute;n de los Usuarios (en adelante, los <I>&ldquo;Usuarios&rdquo;</I> o el <I>&ldquo;Usuario&rdquo;</I>) de Internet interesados en los servicios (en adelante, los <I>&ldquo;Servicios&rdquo;</I>) y contenidos (en adelante, los <I>&ldquo;Contenidos&rdquo;</I>) alojados en el Sitio Web. </P
><P 

>Se entiende que el Usuario acepta expresamente la presente Pol&iacute;tica de Privacidad y Protecci&oacute;n de datos una vez que cumplimente el formulario de registro del Sitio Web haciendo click en la palabra &ldquo;CONTINUAR&rdquo; o &ldquo;ENVIAR&rdquo;.  </P
><P 

><B>2.- RECOMENDACIONES  </P
><P 

></B>Por favor lea detenidamente y siga las siguientes recomendaciones:  </P
><DL 

><DD 

><FONT size="+1" color="#000000"></B>- Utilice contrase&ntilde;as con una extensi&oacute;n m&iacute;nima de 8 caracteres, alfanum&eacute;ricos y con uso de may&uacute;sculas y min&uacute;sculas.  </DD
><DD 

>- El Sitio Web no est&aacute; dirigido a menores de edad. Por favor active el control parental para prevenir y controlar el acceso de menores de edad a Internet e informar a los menores de edad sobre aspectos relativos a la seguridad.  </DD
><DD 

>- Mantenga en su equipo un software antivirus instalado y debidamente actualizado, que garantice que su equipo se encuentra libre de software maligno, as&iacute; como de aplicaciones spyware que pongan en riesgo su navegaci&oacute;n en Internet, y en peligro la informaci&oacute;n alojada en el equipo.  </DD
><DD 

>- Revise y lea las condiciones generales de uso y la pol&iacute;tica de privacidad que la plataforma pone a su disposici&oacute;n en el Sitio Web.  </DD
></DL
><P 

><B>3.- PROCESAMIENTO DE DATOS PERSONALES, FINALIDAD DEL TRATAMIENTO Y CONSENTIMIENTO PARA LA CESI&Oacute;N DE DATOS </P
><P 

></B>Los datos de los Usuarios que se recaban a trav&eacute;s de los formularios de registro online disponibles en el Sitio Web, son recabados por LA AGENCIA con la finalidad de poder prestarles los Servicios ofrecidos a trav&eacute;s del Sitio Web.  </P
><P 

>Asimismo, le informamos de que sus datos personales de contacto (nombre, apellidos, tel&eacute;fono m&oacute;vil, direcci&oacute;n, empresa, cargo, sitio web, direcci&oacute;n de correo electr&oacute;nico, etc.) ser&aacute;n incorporados a un fichero automatizado y utilizados para remitirle Newsletters y comunicaciones comerciales y promocionales relacionadas con los servicios de LA AGENCIA por carta, tel&eacute;fono, correo electr&oacute;nico, SMS/MMS, o por otros medios de comunicaci&oacute;n electr&oacute;nica equivalentes y ello al amparo de lo establecido en la Ley Org&aacute;nica 15/1999, de 13 de diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal (en adelante, LOPD), en la Ley 34/2002 de 11 de Julio, de Servicios de la Sociedad de la Informaci&oacute;n y de Comercio Electr&oacute;nico y en la Ley 32/2003 de 3 de Noviembre, General de Telecomunicaciones. </P
><P 

>El usuario, una vez cumplimente el formulario de registro del Sitio Web, consiente expresamente que sus datos (nombre, apellidos, direcci&oacute;n postal y datos de geolocalizaci&oacute;n) puedan ser utilizados por SANITAS S.A. DE SEGUROS, en adelante, SANITAS, para la promoci&oacute;n de sus productos, as&iacute; como para LA AGENCIA, este &uacute;ltimo como Agencia de Mediaci&oacute;n de SANITAS (y en particular a las sociedades pertenecientes al grupo FAST TRACK NETWORK SL), otras previstas en la Ley  o cedidos (incluso para depurar datos personales) a otras entidades espa&ntilde;olas o de la Uni&oacute;n Europea para enviarle, por carta, tel&eacute;fono, correo electr&oacute;nico, SMS/MMS, o por otros medios de comunicaci&oacute;n electr&oacute;nica equivalentes, a trav&eacute;s nuestro o de otras entidades, publicidad y ofertas comerciales y de servicios relacionados con los sectores de: </P
><DL 

><DD 

><FONT size="+1">- <B>Marketing o pertenecientes a la Federaci&oacute;n Espa&ntilde;ola de Comercio Electr&oacute;nico y Marketing Directo </B>(FECEMD) </DD
><DD 

>- <B>Telecomunicaciones:</B> Productos y Servicios de telecomunicaciones y tecnolog&iacute;a. </DD
><DD 

>- <B>Financiero:</B> Prestados por entidades financieras, Aseguradoras y de Previsi&oacute;n social. </DD
><DD 

>- <B>Ocio:</B> Editorial, Turismo, Deportes, Coleccionismo, Fotograf&iacute;a, Pasatiempos, Jugueter&iacute;a, Transporte, Jardiner&iacute;a, Hobbies, Loter&iacute;as, pe&ntilde;as de loter&iacute;as, Comunicaci&oacute;n y entretenimiento. </DD
></DL
><DL 

><DD 

>- <B>Gran consumo:</B> Electr&oacute;nica, Inform&aacute;tica, Textil, Imagen y Sonido, Complementos, Hogar, Bazar, Cuidado personal (Cosm&eacute;tica, Perfumer&iacute;a, Parafarmacia, especialidades Farmac&eacute;uticas publicitarias) Mobiliario, Inmobiliario, Alimentaci&oacute;n y Bebidas, salud y belleza, Material de oficina. Moda y decoraci&oacute;n. </DD
><DD 

>- <B>Automoci&oacute;n:</B> Productos y Servicios relacionados con el Autom&oacute;vil, Motocicletas y Camiones. </DD
><DD 

>- <B>Energ&iacute;a y agua:</B> Productos relacionados con la Electricidad, Hidrocarburos, Gas y Agua. </DD
><DD 

>- <B>ONG:</B> Productos y Servicios relacionados con ONG. </DD
></DL
><P 

>El usuario acepta y consiente que LA AGENCIA trate sus Datos Personales y consulte ficheros de terceras entidades con la finalidad de determinar su perfil y ofrecerle productos y servicios adecuados, propios o de terceras empresas pertenecientes a los sectores anteriormente relacionados. El Usuario acepta expresamente la presente Pol&iacute;tica de Privacidad y otorga su consentimiento expreso al tratamiento automatizado de los datos personales facilitados. No obstante, el Usuario podr&aacute; revocar el consentimiento, en cada comunicado comercial o publicitario que se le haga llegar, y en cualquier momento, mediante notificaci&oacute;n en la siguiente direcci&oacute;n de correo electr&oacute;nico <A href="mailto:legales@fasttracknet.com">
<FONT color="#1F487C">legales@fasttracknet.com</A>
<FONT color="#000000">; o<FONT size="+1"> <FONT size="+1">mediante carta dirigida a<B> </B>FINTEL MARKETING SLU domiciliada en el Palacio de la Prensa, Plaza de Callao 4, 6&ordm;, 28013 Madrid. </P
><P 

><B>4.- CAR&Aacute;CTER OBLIGATORIO O FACULTATIVO DE LA INFORMACI&Oacute;N FACILITADA POR EL USUARIO Y VERACIDAD DE LOS DATOS </P
><P 

></B>El Usuario garantiza que los datos personales facilitados son veraces y se hace responsable de comunicar a LA AGENCIA cualquier modificaci&oacute;n de los mismos.  </P
><P 

>El Usuario responder&aacute;, en cualquier caso, de la veracidad de los datos facilitados, LA AGENCIA el derecho a excluir de los servicios registrados a todo Usuario que haya facilitado datos falsos, sin perjuicio de la dem&aacute;s acciones que procedan en Derecho.  </P
><P 

>Se recomienda tener la m&aacute;xima diligencia en materia de Protecci&oacute;n de Datos mediante la utilizaci&oacute;n de herramientas de seguridad, no pudi&eacute;ndose responsabilizar a LA AGENCIA de sustracciones, modificaciones o p&eacute;rdidas de datos il&iacute;citas. </P
><P 

><B>5. DERECHOS DE ACCESO, RECTIFICACI&Oacute;N, CANCELACI&Oacute;N Y OPOSICI&Oacute;N </P
><P 

></B>Le recordamos que, en el momento que lo desee, pueda ejercitar sus derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n y oposici&oacute;n o de oposici&oacute;n al tratamiento general o con fines promocionales de sus datos personales envi&aacute;ndonos una comunicaci&oacute;n gratuita a la direcci&oacute;n de correo electr&oacute;nico <A href="mailto:legales@fasttracknet.com">
<FONT color="#1F487C">legales@fasttracknet.com</A>
<FONT color="#000000"> indicando la referencia:&ldquo;NETSALES-Ejercicio derechos LOPD/LSSI]) que deber&aacute; contener: su nombre y apellidos, fotocopia de su DNI (pasaporte u otro documento v&aacute;lido que lo identifique), petici&oacute;n en que se concreta la solicitud, direcci&oacute;n a efectos de notificaciones, fecha, firma y documentos acreditativos de la petici&oacute;n que formula, en su caso.  </P
><H2 

><B>6. USO DE COOKIES </H2
><P 

></B>De conformidad con el Real Decreto-Ley 13/2012, de 30 de marzo, que entr&oacute; en vigor el 1 de abril de 2012, LA AGENCIA ha contratado un servicio que posibilita al usuario que accede al Sitio Web de LA AGENCIA aceptar o rechazar que LA AGENCIA utilice cookies en su navegador. </P
><P 

>Mediante la aceptaci&oacute;n de la presente Pol&iacute;tica de Privacidad, el Usuario acepta los servicios de retargeting que se podr&aacute;n llevar a cabo mediante el env&iacute;o de comunicaciones comerciales. El Usuario consiente que dichas comunicaciones comerciales contengan dispositivos de almacenamiento o cookies de publicidad que se instalar&aacute;n en su navegador. </P
><P 

>Estos servicios de retargeting tienen como finalidad proporcionar m&aacute;s informaci&oacute;n sobre productos o servicios que puedan interesar al Usuario mediante: </P
><P 

>-La instalaci&oacute;n de dispositivos de almacenamiento y recuperaci&oacute;n de datos o cookies en equipos terminales en algunos de los correos electr&oacute;nicos enviados a los usuarios.  </P
><P 

>-Env&iacute;o de correos electr&oacute;nicos con comunicaciones comerciales a los que se haya instalado las Cookies mediante la visita de una p&aacute;gina web o mediante la instalaci&oacute;n a trav&eacute;s de correo electr&oacute;nico. Adem&aacute;s, en cada comunicaci&oacute;n comercial que contenga cookies, se informar&aacute; al usuario de qu&eacute; tipo de cookies contiene y c&oacute;mo desactivarlas. </P
><P 

>En todo caso, el usuario, mediante la aceptaci&oacute;n de la pol&iacute;tica de protecci&oacute;n de datos y de privacidad del Sitio Web, salvo menci&oacute;n expresa en contrario por el usuario, acepta expresamente que LA AGENCIA pueda utilizar las cookies. Aun as&iacute;, el usuario tiene la posibilidad de configurar su navegador para ser avisado de la recepci&oacute;n de cookies y para impedir su instalaci&oacute;n en su equipo. Por favor, consulte las instrucciones y manuales de su navegador para ampliar </P
><P 

>esta informaci&oacute;n. Puede obtener m&aacute;s informaci&oacute;n acerca del funcionamiento de las cookies en <A href="http://www.youronlinechoices.com/es/">
<FONT color="#1F487C">http://www.youronlinechoices.com/es/</A>
 <FONT color="#000000"> </P
><P 

>Las cookies son ficheros enviados al navegador por medio de un servidor web con la finalidad de registrar las actividades del usuario durante su tiempo de navegaci&oacute;n. Las cookies utilizadas por el sitio web se asocian &uacute;nicamente con un usuario an&oacute;nimo y su ordenador, y no proporcionan por s&iacute; mismas los datos personales del usuario. Mediante el uso de las cookies resulta posible que el servidor donde se encuentra la web, reconozca el navegador web utilizado por el usuario con la finalidad de que la navegaci&oacute;n sea m&aacute;s sencilla, permitiendo, por ejemplo, el acceso a los usuarios que se hayan registrado previamente, acceder a las &aacute;reas, servicios, promociones o concursos reservados exclusivamente a ellos sin tener que registrarse en cada visita. Se utilizan tambi&eacute;n para medir la audiencia y par&aacute;metros del tr&aacute;fico, controlar el progreso y n&uacute;mero de entradas. </P
><P 

>Para utilizar el sitio web, no resulta necesario que el usuario permita la instalaci&oacute;n de las cookies enviadas por el sitio web, o el tercero que act&uacute;e en su nombre, sin perjuicio de que sea necesario que el usuario inicie una sesi&oacute;n como tal en cada uno de los servicios cuya prestaci&oacute;n requiera el previo registro o &ldquo;login&rdquo;. </P
><P 

>7<B>.- MEDIDAS DE SEGURIDAD </P
><P 

></B>LA AGENCIA mantiene los niveles de seguridad de protecci&oacute;n de datos personales conforme a la LOPD y al Real Decreto 1720/2007, de 21 de diciembre, por el que se aprueba el Reglamento de desarrollo de la LOPD que contengan datos de car&aacute;cter personal y ha establecido todos los medios t&eacute;cnicos a su alcance para evitar la p&eacute;rdida, mal uso, alteraci&oacute;n, acceso no autorizado y robo de los datos que el usuario facilite a trav&eacute;s del Sitio Web, sin perjuicio de informarle de que las medidas de seguridad en Internet no son inexpugnables. </P
><P 

>LA AGENCIA se compromete a cumplir con el deber de secreto y confidencialidad respecto de los datos personales contenidos en el fichero automatizado de acuerdo con la legislaci&oacute;n aplicable, as&iacute; como a conferirles un tratamiento seguro en las cesiones que, en su caso, puedan producirse. </P
><P 

>8<B>.- LINKS A P&Aacute;GINAS WEB </P
><P 

></B>El Sitio Web de LA AGENCIA podr&iacute;a contener links a p&aacute;ginas web de compa&ntilde;&iacute;as y entidades de terceros.  </P
><P 

>LA AGENCIA no puede hacerse responsable de la forma en la que estas compa&ntilde;&iacute;as tratan la protecci&oacute;n de la privacidad y de los datos personales, por lo que le aconsejamos que lea detenidamente las declaraciones de pol&iacute;tica de privacidad de &eacute;stas p&aacute;ginas web que no son propiedad de LA AGENCIA con relaci&oacute;n al uso, procesamiento y protecci&oacute;n de datos personales. Las condiciones que ofrecen &eacute;stas p&aacute;ginas web pueden no ser las mismas que las que ofrece LA AGENCIA. </P
><P 

>9<B>.- PREGUNTAS </P
><P 

></B>Si tiene alguna pregunta sobre esta Pol&iacute;tica de Privacidad, rogamos que se ponga en contacto con nosotros enviando un email a <A href="mailto:legales@fasttracknet.com">
<FONT color="#1F487C">legales@fasttracknet.com</A>
.<FONT color="#000000"> </P
><P 

>10<B>.- CAMBIOS </P
><P 

></B>LA AGENCIA se reserva el derecho de revisar su Pol&iacute;tica de Privacidad en el momento que lo considere oportuno. Por esta raz&oacute;n, le rogamos que compruebe de forma regular esta declaraci&oacute;n de privacidad para leer la versi&oacute;n m&aacute;s reciente de la pol&iacute;tica de privacidad de LA AGENCIA. </P
><P 

>11<B>.- CONDICIONES PARTICULARES  </P
><P 

></B>El acceso a determinados Contenidos ofrecidos a trav&eacute;s del Sitio Web puede encontrarse sometido a ciertas condiciones particulares propias que, seg&uacute;n los casos, sustituyen, completan las modifican las condiciones generales. Por tanto, con anterioridad al acceso y/o utilizaci&oacute;n de dichos Contenidos, el Usuario ha de leer atentamente tambi&eacute;n las correspondientes condiciones particulares.  </P
></DIV
></BODY>
</html>
<?
//print_r($_REQUEST['deviceType']);
?>