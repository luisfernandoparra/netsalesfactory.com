<?php

/**
 * Modificación LFP 2015.03.23
 * Añadimos variable para procesar los datos que vienen de acción mobile emailing Gmail
 * Para eso añadimos variable $emailingSource que será true si viene de gmail
 * Si esta variable es true, no hacemos el die del final y tenemos los datos en un array
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');
/**
 * Config loaded viene del fichero bridge. Si está definida y es true, es que ya ha cargado los configs
 */
if (empty($configLoaded)) {
    include('../../conf/config_web.php');
    include('../conf/config_web.php');
}

//$resCurl = array();
$resCurl = new stdClass;
///$sOutput = array(); // ARRAY PARA LOS DATOS DE SALIDA JSON
$raw = '';
$resCurl->error = 1;
//$resCurl['error'] = 1;
//$sOutput['mensaje'] = 'ha habido un error en el proceso';
$sOutput['id'] = -1;

$emailingSource = (!empty($_REQUEST['emailingsource']) && strtoupper(trim($_REQUEST['emailingsource'])) == 'GMAIL');

$nombre = (!empty($_REQUEST['nombre'])) ? trim($_REQUEST['nombre']) : '';
$apellidos = (!empty($_REQUEST['apellidos'])) ? trim($_REQUEST['apellidos']) : '';
$apellido2 = (!empty($_REQUEST['apellido2'])) ? trim($_REQUEST['apellido2']) : '';
$email = (!empty($_REQUEST['em'])) ? trim($_REQUEST['em']) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? $_REQUEST['provincia_text'] : $_REQUEST['provincia'];

$legal = (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : '1';
$permanencia = (!empty($_REQUEST['perm'])) ? trim($_REQUEST['perm']) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;
$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';

$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? trim($_REQUEST['idclient']) : '';
$sourcetype = (!empty($_REQUEST['sourcetype'])) ? trim($_REQUEST['sourcetype']) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? trim($_REQUEST['campaign']) : '';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? trim($_REQUEST['prioridad']) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? trim($_REQUEST['telefono']) : '';
$use_priority = (!empty($_REQUEST['use_priority']) && is_numeric($_REQUEST['use_priority']) && $_REQUEST['use_priority'] == 1 && $prioridad != '') ? 1 : 0;
$nch_proc = !empty($_REQUEST['nch_proc']) ? trim($_REQUEST['nch_proc']) : 0; // ADDED 2016.06.10 M.F.

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

$campos = array('idclient' => $id_client, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad,
    'nombre' => $nombre, 'apellidos' => $apellidos, 'apellido2' => $apellido2, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI,
    'vcc' => 'fintel', 'pwd' => $secret_pwd, 'usr' => $secret_usr,
    'use_priority' => $use_priority);

//,"serverIp"=>"84.127.240.42"

if (!empty($_SESSION['ws_ip_id']) && is_numeric($_SESSION['ws_ip_id'])) {
    $campos['idIp'] = $_SESSION['ws_ip_id'];
}

$qs = http_build_query($campos);
$url = $url_ws . $file_ws . '?' . $qs;
//print_r($_REQUEST);print_r($campos);die($nch_proc);die();

if ($telefono != '') {
    $conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
    if (!$conexion->connectDB()) {
        $msg_err = 'No se ha podido conectar a la BBDD. Error 102';
    } else {
        $cur_conn_id = $conexion->get_id_conexion();
        /* $sql1="SET AUTOCOMMIT=0;";
          $conexion->ejecuta_query($sql1);

          $sql2="BEGIN";
          $conexion->ejecuta_query($sql2);
         */
        $nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));

        $telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
        $sql = 'INSERT INTO %s (id_cliente,nombre,apellidos,apellido2,telefono,id_provincia,b_legales,email,crea,id_crea,cp) VALUES (\'%d\',\'%s\',\'%s\',\'%s\',\'%s\',\'%d\', %d,\'%s\',\'%s\',%d,\'%s\');';
        $sql = sprintf($sql, $table_registros, $id_client, $nombre, $apellidos, $apellido2, $telefono, (int) $_REQUEST['provincia'], $legal, $email, $crea, $idLandingCr, $provI);
        $conexion->ejecuta_query($sql);
        $id=$conexion->get_id();

        $cls_misc = new cls_misc();
        $arr_idchann = json_decode($cls_misc->ws_getIdChannels($email, $telefono), true);
        $id_ret = $id . '-' . $arr_idchann['email'] . '-' . $arr_idchann['telf'];
        $resCurl->id = $id_ret;
        unset($cls_misc);



        include($path_raiz_includes . 'class/nusoap/nusoap.php');
//				$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
//				$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
//				$proxyusername = isset($_POST['usuario']) ? $_POST['usuario'] : 'netsales';
//				$proxypassword = isset($_POST['password']) ? $_POST['password'] : 'netsales2015_';
        $useCURL = isset($_POST['usecurl']) ? $_POST['usecurl'] : 0;

        $urlNamespaces = 'http://schemas.xmlsoap.org/ws/2002/07/secext';
//        $nusoapMode = true;
//        $nsPrefix = '';
        $nusoapMode = false;
        $nsPrefix = 'racc:';
        $urlNamespaces = null;

        $client = new nusoap_client($url_ws, $nusoapMode, $urlNamespaces);
        $client->soap_defencoding = 'utf-8';
        $err = $client->getError();

        if ($err) { // NUSOAP INIT ERROR (COMENTAR $resCurl['mensaje'] EN PRODUCCION)
//echo 'ERROR NUSOAP:::';print_r($client);print_r($_REQUEST);die($url);die();
            $resCurl->response_ws = $err;
            $resCurl->response_extended = htmlspecialchars(strip_tags($client->getDebug()), ENT_QUOTES);
            $resCurl->mensaje = '<div style=display:inline-block;position:absolute;width:99%;text-wrap:break-word;top:0;left:0;font-size:80%;color:red;>' . $err . '</div><span style=display:inline-block;width:98%;text-wrap:break-word;margin-top:-20px;font-size:75%;color:#ff3333;>' . $resCurl->response_extended . '</span>';
            $resCurl->error = 1;
            $error = true;
        } else { // CONTINUE NUSOAP PROCEDURE
            $paramsNusoap = array();
            $client->setUseCurl($useCURL);

            if($nusoapMode)
              $client->loadWSDL();

            if($sitesPruebas) { // CONFIGURACION SOLO PARA PRUEBAS
							$client->setCredentials('netsales', 'netsales2015_', 'basic'); // SOLO TESTS
							$paramsNusoap[$nsPrefix . 'controlAcceso']['nombreAplicacion'] = 'external_user_contacts';
							$paramsNusoap[$nsPrefix . 'controlAcceso']['claveAplicacion'] = '12345';
							$paramsNusoap[$nsPrefix . 'controlAcceso']['origen'] = 'RACC';
            }else{ // CONFIGURACION SOLO PARA PRODUCCION
							$client->setCredentials('adpepper', 'Planning_22', 'basic'); // SOLO PRODUCCION
							$paramsNusoap[$nsPrefix . 'controlAcceso']['nombreAplicacion'] = 'adpepper';
							$paramsNusoap[$nsPrefix . 'controlAcceso']['claveAplicacion'] = 'Planning_22';
							$paramsNusoap[$nsPrefix . 'controlAcceso']['origen'] = 'RACC';
            }
						$fieldNum=1;
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'nombre', 'valor' => $campos['nombre'], 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'apellido1', 'valor' => $campos['apellidos'], 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'apellido2', 'valor' => $campos['apellido2'], 'tipo' => 'alfanumerico');
//            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::3'] = array('clave' => 'cp', 'valor' => '', 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'cp', 'valor' => '28000', 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'email', 'valor' => $campos['em'], 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'localidad', 'valor' => '', 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'provincia', 'valor' => $campos['province'], 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'telefono', 'valor' => $campos['telefono'], 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'idioma', 'valor' => '2', 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'xml', 'valor' => 'xml_data_here', 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'esquema', 'valor' => 'xml', 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'cod', 'valor' => '82', 'tipo' => 'alfanumerico');
            $paramsNusoap[$nsPrefix . 'datosContacto']['campo']['campoContacto::'.($fieldNum++)] = array('clave' => 'nch_proc', 'valor' => $nch_proc, 'tipo' => 'alfanumerico');

            $numFields = count($paramsNusoap[$nsPrefix . 'datosContacto']['campo']);
            $paramsNusoap[$nsPrefix . 'datosContacto']['num_campos'] = $numFields;

            $resNusoap = $client->call('webservice_wsAltaContacto', $paramsNusoap, false, true); // LLAMADA AL WS DEL CLIENTE
//						$resNusoap=new stdClass();
/*
echo"<p>\r\n\r\n".'$RESPONSE NUSOAP  ==> '."\r\n";
echo ($client->response);
echo"\r\n\r\n";print_r($resNusoap);
echo"\r\n\r\n";print_r($paramsNusoap);die("\r\nURL = ".$url_ws);
*/
//$resXml=xmlrpc_decode($client->response);
//$resXml=simplexml_import_dom($client->response);
//echo"\r\n\r\n";print_r($resXml);
//echo"\r\n\r\n";print_r($client->response);die('!!');

//echo"\r\n\r\client=";print_r($client->responseData);
//echo"\r\n\r\nclient->response=";print_r($client->response);die('!!');




            if($resNusoap['faultcode']){
                $error = true;
                $resCurl->response_ws = $resNusoap['faultcode'];
                $resCurl->response_extended = $resNusoap['faultstring'];
                $resCurl->mensaje = '<div style=display:inline-block;position:absolute;width:99%;text-wrap:break-word;top:0;left:0;font-size:80%;color:red;>' . $resCurl->response_ws . '</div><span style=display:inline-block;width:98%;text-wrap:break-word;margin-top:-20px;font-size:95%;color:#ff3333;>' . $resCurl->response_extended . '</span>';
                $resCurl->error = 1;
                $error = true;

            } else {
                try {
                    // LOG DE LA RESPUESTA RECIBIDA
                    $fp = fopen('./temp/racc-' . date('Ymd') . '.csv', 'a');
                    fwrite($fp, '------------------' . "\r\n");
                    $line = print_r($resNusoap, true);
                    if (empty($line))
                        $line = $resNusoap;

                    fwrite($fp, $line . "\r\n");
                    $line = print_r($client->response, true);
                    if (empty($line))
                        $line = $client->response;
                    fclose($fp);
                } catch (Exception $exc) {
                    echo $exc->getTraceAsString();
                }

                $arrResponse = explode('codigoRetorno', $client->response);
                $fp = fopen('./temp/racc-' . date('Ymd') . '.csv', 'a');
                fwrite($fp, print_r($arrResponse, true));

                fclose($fp);
                $codeResponse = strip_tags($arrResponse[1]);
                $codeResponse = substr($codeResponse, 0, 3);
                $resCurl->response_ws = $codeResponse;
                $resCurl->response_extended = $codeResponse == '000' ? 'CORRECTO' : 'ERROR';

                if ($codeResponse == '000') {
									//Cogemos el identificador devuelto
									$arrCodeResponseId = explode('<respuesta xsi:type="xsd:int">', $arrResponse[1]);
									$codeResponseId = explode('</respuesta>', $arrCodeResponseId[1]);
									$codRacc = (!empty($codeResponseId[0])) ? $codeResponseId[0] : '';
									if (!empty($codRacc)) {
											$id_ret = $codRacc . '-' . $arr_idchann['email'] . '-' . $arr_idchann['telf'];
											$resCurl->id = $id_ret;
									}


									$query = 'UPDATE %s SET response_ws=\'%s\', response_extended=\'%s\' WHERE id_registro=\'%d\'';
									$query = sprintf($query, $table_registros, $codeResponse, $codeResponse == '000' ? $id_ret : '', $id);
//                    $query = sprintf($query, $table_registros, $codeResponse, $codeResponse == '000' ? 'OK' : '', $id);
									$conexion->ejecuta_query($query);
									$resCurl->error = 0;
									$error = false;
									$tmp = array();
									$tmp['error'] = 0;
									$tmp['id'] = $id_ret;
									$tmp['mensaje'] = 'OK';
									$raw = json_encode($tmp);
                } else {
									$resCurl->error = 1;
									$error = true;

									switch ($codeResponse) {
										case '014':
											$resCurl->mensaje = 'Parece que usted ya ha efectuado anteriormente esta petición.';
											break;
										case '006':
											$resCurl->mensaje = 'Parece que el correo no es correcto.';
											break;
										case '':
											break;
										default:
											$resCurl->mensaje = 'No hemos podido procesar su petición';
									}

									$carriers=array(); $outExtend=''; $cont=0;
									$domDocument=new DOMDocument();
									$domDocument->loadXML($client->responseData);
									$results=$domDocument->getElementsByTagName('item');

									foreach($results as $result)
									{
										foreach($result->childNodes as $node)
										{
											if($node instanceof DOMElement)
											{
												array_push($carriers, $node->textContent);
												$outExtend.=$node->textContent.(($cont/2) == (int)($cont/2) ? '=' : ', ');
											}
											$cont++;
										}
									}

									$query = 'UPDATE %s SET response_ws=\'%s\', response_extended=\'%s\' WHERE id_registro=\'%d\'';
//									$query = sprintf($query, $table_registros, $codeResponse, $codeResponse == '000' ? $id_ret : '', $id);
									$query = sprintf($query, $table_registros, $codeResponse, $outExtend, $id);
									$conexion->ejecuta_query($query);
//echo $query."\r\n\r\ID=".$id;print_r($client->responseData);echo"\r codeResponse=";print_r($codeResponse);die();
                }
            }
        }

        $conexion->disconnectDB();

        if ($error) {
            $msg_err = 'Parece que el problema se debe a lo siguiente: "' . $resCurl->mensaje . '".';
        } else {
            $tmpCrea = $_SESSION['idCreatividad'];
            unset($_SESSION['namePromo']); // SE DESTRUYE LA REFERENCIA DE LA SESION
            $tmpName = session_name('sanitas' . date(Yimis));
            $_SESSION['namePromo'] = session_name(); // SE CONSTRUYE UNA NUEVA SESION PARA PERMITIR REGISTROS NUEVOS DESDE LA MISMA APERTURA
            $_SESSION['idCreatividad'] = $tmpCrea;

            if (!$emailingSource) {
                die($raw);
            } else {
                $resCurl = json_decode($raw, true);
            }
        }
    }
} else {
    if ($telefono == '') {
        $error = 1;
        $msg_err = 'Falta el campo Teléfono. Error 104';
    }
}

if ($error) {
    $query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
    $query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
    $conexion->getResultSelectArray($query);
    $resQuery = $conexion->tResultadoQuery;
    $idUpdate = $resQuery[0]['id'];

    $query = 'UPDATE %s SET response_ws=\'%s\', response_extended=\'%s\' WHERE id_registro=\'%d\'';
    $query = sprintf($query, $table_registros, $resCurl->response_ws, $resCurl->response_extended, $id);
    $conexion->ejecuta_query($query);
}

if (!$emailingSource) {
    $res = json_encode($resCurl);
    die($res);
}
?>