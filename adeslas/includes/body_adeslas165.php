<?php
@session_start();
$idLandingFormatted = str_pad($objSiteData->landingId, 3, '0', STR_PAD_LEFT);
$rutaImgs = $path_raiz_aplicacion_local . 'img/' . $idLandingFormatted . '/';
$defaultLocalPhoneCall = '';
$inbound_phone = ($inbound_phone) ? $inbound_phone : ''; // SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
?>

<link href="<?=$path_raiz_aplicacion_local?>css/owl.carousel.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=$path_raiz_aplicacion_local?>js/owl.carousel.min.js"></script>

<style>
div.error2{ display : none; border:2px solid #D81E05; }
.error2{color:#D81E05; background:#FCF1F0;}
div.error2 ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error2 ul li{margin:0 0 10 0;list-style-type:none;}
div.error2 ul li label{font-weight:normal}
div.error2, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error2{border:2px solid #D81E05;background-color:#FCF1F0;}

.appbutton {
		background-color:#009fe4;
		background-image:url("img/f-boton-color.png");
		background-position:-10px 0;
		background-repeat:no-repeat;
		border:medium none #00335b;
		border-radius:3px;
		box-shadow:0 1px 2px #424242;
		color:white !important;
		cursor:pointer;
		display:inline-block;
		font:14px Arial,Helvetica,sans-serif;
		margin:5px 5px 0 0;
		padding:8px 13px;
		text-align:center;
		text-decoration:none !important;
		text-shadow:0 1px #00335b;
		width:auto;
}
.innerModal{
		box-sizing:border-box;
		margin:0;
		padding:0;
		padding-top:6%;
		position:relative;
		line-height:22px!important;
		font:12px Arial,Helvetica,sans-serif;
}
</style>

<script type="text/javascript">
var errCheckTel = 1;
var iconselected = 1;
var css_selected = 'circulo_azul ico_selected';
var css_not_selected = 'circulo_azul';
var isCtcPass=0;

$(document).ready(function() {

	$("#ctcForm").validate({
			rules:{
				telefono_ctc:{
					required:true
					,remote:{
						url:root_path_local +"includes/phone_check.php",
						contentType:"application/json; charset=utf-8",  
						data:{cr:<?=(int)$id_crea?>},
						dataFilter:function(response){
							var jsonResponse="";
							isCtcPass=1;
							jsonResponse = JSON.parse(response);
							if(jsonResponse.success == false)
							{
								isCtcPass=0;
								return "\"" + jsonResponse.errorMessage + "\"";
							}else{
								return true;
							}
							return false;
						}
					}
				}
			},
			errorLabelContainer: $("#ctcForm div.error2")
	});

	$('input[name="cookie_rejection"]').click(function(){
		setTimeout('$("#pie_galletero_left").css("width","95%").css("text-align","left");',350)
	});


	$("#boton_ctc").click(function(){
			var phoneBumberCtc=$.trim($('#telefono_ctc').val());
			res=validator2.element("#telefono_ctc");

			if(!isCtcPass)
			{
				res=validator2.element("#telefono_ctc");
				setTimeout('$("#boton_ctc").click();',300);
				return false;
			}

			if(!res && !isCtcPass)
				return false;

			if($.isNumeric(phoneBumberCtc) && phoneBumberCtc.length >= 9)
			{
					var valortype = array_typeofctc[0];
					var campana = arr_campanas[0] ? arr_campanas[0] : 0;
					$.facebox.loading();
					$.ajax({
							url: root_path_local + "includes/phone_check.php",
							method: "post",
							dataType: "json",
							data: {cr:<?= (int) $id_crea ?>, telefono_ctc: phoneBumberCtc},
							cache: false,
							async: false,
							success: function(response)
							{
									if (!response.error)
									{
											$.ajax({
													url: root_path_local + "ajaxs/procesar_registro_ctc.php",
													method: "post",
													dataType: "json",
													data: {
															telefono: phoneBumberCtc,
															sourcetype: valortype,
															campaign: campana,
															fuente: id_source,
															idclient: id_client,
															crea: nomb_promo
													},
													cache: false,
													async: false,
													success: function(response)
													{
															if(!response.error)
															{
																ComprobarInsercion(response, 0);
															}
															else
															{
																ComprobarInsercion(response, 0);
															}
													},
													error:function(response){
															console.log("err2");
															return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
													}
											});
									}
									else
									{
											console.log("err code 2C");
									}
							},
							error: function(response) {
									console.log("err2");
									return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
							}
					});
			}
			return false;
	});
	validator2 = $("#ctcForm").validate({
			errorContainer: $('div.error2'),
			errorLabelContainer: $('div.error2 ul'),
			wrapper: 'li'
	});
	/**
	 * Fiunción para cambiar el texto según se haga click en el icono
	 */
	$('[id^="ico-"]').click(function(e) {
			var id = $(this).attr('id');
			var tmp = id.split('-');
			var tmp_iconselected = tmp[1];
			if (iconselected != tmp_iconselected) {
					$('#text-' + iconselected).hide();
					$('#text-' + tmp_iconselected).fadeIn('slow');
					$('#selected-' + iconselected).removeClass(css_selected).addClass(css_not_selected);
					$('#selected-' + tmp_iconselected).removeClass(css_not_selected).addClass(css_selected);
					iconselected = tmp_iconselected;

			}
	})

// START SPECIFIC EFFECTS
	var owl = $("#owl-adeslas");

	owl.owlCarousel({
		items : 3, //10 items above 1000px browser width
		itemsDesktop : [1000,5], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
		itemsTablet: [600,2], //2 items between 600 and 0;
		itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
	});

	// Custom Navigation Events
	$(".next").click(function(){
		owl.trigger('owl.next');
	})
	$(".prev").click(function(){
		owl.trigger('owl.prev');
	})
	$(".play").click(function(){
		owl.trigger('owl.play',1000);
	})
	$(".stop").click(function(){
		owl.trigger('owl.stop');
	});

// END SPECIFIC EFFECTS


}); //document.ready
</script>
<?php
/** START EXCEPTION FOR DRAW GRATIS TOP PHONE ON TOP PAGE**/
/**
 * $arrGratisPhones = array que relaciona la fuente con el numero de teléfono a dibujar
 */
//echo'<pre>';print_r($objSiteData->phoneNumbersPerSources);echo'</pre>';

$arrGratisPhones[$objSiteData->phoneNumbersPerSources['id_surce']]=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($objSiteData->phoneNumbersPerSources['phone_number'])), 2);
$gratisPhoneCall=''; $sourceField='';
$sepParam=$sitesPruebas ? '&' : '/?' ;
$sourceField=$sepParam.'fuente='.$id_source;	// SIEMPRE SE ENVIA EL PARAMETRO DE LA FUENTE
$sourceField = $id_source ? $sourceField : '';

if($objSiteData->phoneNumbersPerSources['id_surce'] === $id_source && $objSiteData->phoneNumbersPerSources['id_surce'])
{
	$gratisPhoneCall='<section class="llamanos">	<div>		<span style="color:#1caae3; font-size:1.2em;">Llámanos GRATIS <font style="color:#036dbe;">'.$arrGratisPhones[$objSiteData->phoneNumbersPerSources['id_surce']].'</font></span>		<img src="img/tel.png" alt="Adeslas">		<span style="font-size:.9em;">Si ya eres cliente: 902.242.242</span>	</div></section>';
}
/** END EXCEPTION FOR DRAW GRATIS TOP PHONE ON TOP PAGE**/

?>

<header>
	<section class="cabecera">
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
{
	$resOutput=$objSiteData->headerTopContent;
	$resOutput=str_replace('[[gratisPhoneCall]]',$gratisPhoneCall,$resOutput);	// EXCEPTION FOR DRAW GRATIS TOP PHONE ON TOP PAGE
	echo $resOutput;
}
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
	</section>
</header>


<section class="basic-content">
	<section class="small-content">
		<section class="skeleton">
<?php
if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}

?>
		</section>


		<section class="formulario">
			<form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
				<input type="hidden" name="destino" id="destino" value="">
<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
	$formScriptName='form_script_'.$idLandingFormatted.'.php';
}

$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
	echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
	include($formScriptName);
?>
				</form>
		</section> <!--CIERRE FORM -->
	</section>
</section>


<section class="text-content">
<?php
if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}
?>
</section>
<!--CIERRE CONTENIDO --> 

<footer>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini"></div>
';
}

?>
</footer>
<script>
  (function (w,i,d,g,e,t,s) {w[d] = w[d]||[];t= i.createElement(g);
    t.async=1;t.src=e;s=i.getElementsByTagName(g)[0];s.parentNode.insertBefore(t, s);
  })(window, document, '_gscq','script','//widgets.getsitecontrol.com/58152/script.js');
</script>