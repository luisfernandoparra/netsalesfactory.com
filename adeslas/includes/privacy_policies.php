<?php
@header('Content-Type: text/html; charset=utf-8');
@session_start();
include('../../conf/config_web.php');
$query='SELECT privacy_policy_file FROM %s WHERE id=%d';
$query=sprintf($query,$table_landings_site_config,(int)$idLandingCr);
$conexion->getResultSelectArray($query);
$txtToDraw=$conexion->tResultadoQuery;
$txtToDraw=$txtToDraw[0];
$txtToDraw=$txtToDraw['privacy_policy_file'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Condiciones de Contratación</title>
<?php
echo '<link rel="icon" href="'.$path_raiz_aplicacion_local.'favicon111.png" type="image/x-icon" />
<link rel="shortcut icon" href="'.$path_raiz_aplicacion_local.'favicon111.png" type="image/x-icon" />
';

if($_REQUEST['deviceType'] == 'mob_'){
?>
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=2.0; user-scalable=yes;">
<meta http-equiv="Pragma" content="no-cache">
<?php
}
?>

<style>
body, .conditions{
width:auto;
padding:2%;
font-family:Arial, Helvetica, sans-serif, Gadget, sans-serif;
color:#000000;
background-color:#FFF;
}

.Part{
	font-family:arial,verdana;
	font-size:.9em!important;
}

</style>

</head>
<body>

<?php
if(!(int)$idLandingCr)
	echo '<center>Lamentablemente ha ocurrido un error al intentar cargar la información solicitada.<br /><br />Por favor, inténtelo más tarde.</center>';
else
	echo $txtToDraw;
//	echo utf8_encode($txtToDraw);
?>

</body>
</html>