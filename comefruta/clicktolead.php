<?php

/**
 * Página que simplemente generará ClicksToLead recibiendo los datos desde la creatividad del email
 * @date 2016.06.27
 * @author LFP
 */
include('../conf/config_web.php');
ini_set('display_errors', 1);
include('conf/conf.php');
//Recogemos parámetros
include($path_raiz_includes . 'includes/initiate.php');

$debug = (!empty($_REQUEST['debug']));
/**
 * Variable para coregistro. No saltará el pixel al no tener cli
 */
$responseSimple = (!empty($_REQUEST['responsesimple']));
$responseSimple_txt = 'KO';
//$email = strtolower(trim($_REQUEST['email']));
//$cli = $_REQUEST['cli'];
//
//$email = (filter_var($email,FILTER_VALIDATE_EMAIL)===false) ? '' : $email;
if ($email != '') {

    $origen = ($responseSimple) ? 'Coreg' : 'ClicktoLead';

    $ip = getRealIpAddr();
    //Le registramos, le mandamos al servidor ingenious
    $sql = 'insert into %s  (id_cliente, email,ip,pixelId_click) values (%d,\'%s\',\'%s\',\'%s\')';
    $sql = sprintf($sql, $table_registros, $idClient, $email, $ip, $cli);

    $conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
    $conexion->connectDB();
    $conexion->ejecuta_query($sql);
    $id = mysql_insert_id();

    $id = empty($id) ? $conexion->Id : $id;
    if (empty($id)) {
        $sql = 'select max(id_registro) as maximo from %s where id_cliente=%d';
        $sql = sprintf($sql, $table_registros, $idClient);
        $conexion->getResultSelectArray($sql);
        $dat = $conexion->tResultadoQuery;
        $id = empty($dat[0]['maximo']) ? rand() : $dat[0]['maximo'];
    }
    $conexion->disconnectDB();
    //Mandamos al aplicativo o donde sea.
    $arr_data = array('status' => 'subscribed', 'email_address' => $email, 'ip_signup' => $ip, 'merge_fields' => array('MMERGE3' => $origen));
    if ($debug) {
        echo "<hr><strong>Array Data</strong><br> ";
        print_r($arr_data);
        echo "<br><br>";
    }
    $data_string = json_encode($arr_data);
    ///lists/{list_id}/members
    $mailchimp_url .= 'lists/' . $mailchimp_listId . '/members';
    if ($debug) {
        echo "<hr><strong>Url Mailchimp</strong><br> " . $mailchimp_url . "<br><br>";
    }
    // die($mailchimp_url);
    $ch = curl_init();
    $timeout = 0; // set to zero for no timeout
    curl_setopt($ch, CURLOPT_URL, $mailchimp_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERPWD, "netsales:$mailchimp_apikey");
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );
    $raw = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $resp_mail = json_decode($raw, true);
    if ($debug) {
        echo "<hr><strong>Resp Mailchimp</strong><br> ";
        if (is_array($resp_mail) || is_object($resp_mail)) {
            print_r($resp_mail);
        } else
            echo $raw;
        echo "<br><br>";
        echo "<hr><strong>Status Mailchimp</strong><br> ";
        if (is_array($status) || is_object($status)) {
            print_r($status);
        } else
            echo $status;
        echo "<br><br>";
    }

    curl_close($ch);

    $resp_msg = '';
    $resp = $status;
    $resp_msg = ($status == '200') ? $resp_mail['status'] : $resp_mail['detail'];
    $sql = 'update %s set response_ws=%d,response_extended=\'%s\' where id_registro=%d';
    $sql = sprintf($sql, $table_registros, $resp, $resp_msg, $id);
    $conexion->ejecuta_query($sql);
    if ($debug) {
        echo "<hr><strong>Sql</strong><br> " . $sql . "<br><br>";
    }
    if ($cli != '' && $status == '200') {

        //Lanzamos el pixel
        foreach ($ingenious_url as $ing_url) {


            $ing_url = str_replace(array('[%%ID%%]', '[%%CLI%%]'), array($id, $cli), $ing_url);
            if ($debug) {
                echo "<hr><strong>Url Ingenious</strong><br> " . $ing_url . "<br><br>";
            }
            $ch = curl_init();
            $timeout = 0; // set to zero for no timeout
            curl_setopt($ch, CURLOPT_URL, $ing_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $raw = curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            if ($debug) {
                echo "<hr><strong>Resp Ingenous</strong><br> ";
                if (is_array($raw) || is_object($raw)) {
                    print_r($raw);
                } else
                    echo $raw;
                echo "<br><br>";
                echo "<hr><strong>Status Ingenious</strong><br> ";
                if (is_array($status) || is_object($status)) {
                    print_r($status);
                } else
                    echo $status;
                echo "<br><br>";
            }
        }
    }
    $responseSimple_txt = (($responseSimple && $status == '200') ? 'OK' : 'KO');
}
if ($responseSimple)
    die($responseSimple_txt);
if ($debug)
    die('pirate');
header("Location: $url_jumpto");
exit;
