// JavaScript Document
//Tipos de ClickToCall. CTC==máxima prioridad. Lead==formulario grande
var array_typeofctc = ["CTC", "LEAD"];
var array_priorities = [20, 40];

var whitespace = " \t\n\r";
var reWhitespace = /^\s+$/;

function MM_openBrWindow(theURL, winName, features) { //v2.0
    checkCookie();
    window.open(theURL, winName, features);
}

//Función que pone pixel por javascript
function ponerPixelJS(pix, idProcess, isImage) {
    if (!isImage)
    {
        var s = document.createElement("script");
        s.type = "text/javascript";
    }
    else
    {
        var s = new Image;
        s.width = "1px";
        s.height = "1px";
    }
    pix = pix.replace("[XXfeaturedparamXX]", idProcess);
    s.src = pix;

    $("body").append(s);
}

function addHiddenFields(id, val, element) {
    $('<input>').attr({
        type: 'hidden',
        id: id,
        name: id,
        value: val
    }).appendTo('#' + element);
}


function __replaceall(msg, needle, reemp) {
    return msg.split(needle).join(reemp);
}

function clearMyMessage(msg)
{
    msg = __replaceall(msg, "<br />", "\n");
    msg = __replaceall(msg, "<br>", "\n");
    msg = __replaceall(msg, "<strong>", "");
    msg = __replaceall(msg, "</strong>", "");
    msg = __replaceall(msg, "<b>", "");
    msg = __replaceall(msg, "<i>", "");
    msg = __replaceall(msg, '</b>', "");
    msg = __replaceall(msg, "</i>", "");
    msg = __replaceall(msg, "<p>&nbsp;</p>", "\n");
    msg = __replaceall(msg, "<p>", "");
    msg = __replaceall(msg, "</p>", "\n");
    return msg;
}

function _showModal(typ, cab, msg) {
    $.facebox.close();
    var alerta = (typeof is_movil !== undefined) && is_movil == '1';

    if (!alerta) {
        $.modal('<div class="innerModal">' + msg + '<center><a href="#0" class="appbutton simplemodal-close">cerrar</a></center></div>', {
            containerCss: {
                backgroundColor: "#333232",
                borderColor: "#333",
                width: "80%",
                "max-width": "500px",
                close: true,
                padding: 0
            },
            onClose: function (dialog) {
                dialog.data.fadeOut('fast', function () {
                    dialog.container.hide('slow', function () {
                        dialog.overlay.slideUp('fast', function () {
                            $.modal.close();
                        });
                    });
                });
            },
            overlayClose: true
        });

    } else {
        alert(clearMyMessage(msg));
        ;
    }
}

function clearForm() {
	$('#nombre').val('');
	$('#email').val('');
	$('#telefono').val('');
	$('select option:selected').removeAttr('selected');
	$("#cblegales").attr("checked", false);
	if (is_ctc == 1 || $("#telefono_ctc").length) {
			$("#telefono_ctc").val('');
	}
}

function ComprobarInsercion(data, ind) {
	var msg = "";
	var b_is_movil = (typeof is_movil !== undefined) && is_movil == '1';
	var v_tduid = (typeof tduid !== undefined) ? tduid : '';
	var pix = "";

	if (data.error == 0 || data.success == true) { //no ha habido error. Le estamos llamando.
		clearForm();

		$('<form>').attr({
			id: 'form_submit',
			name: 'form_submit',
			method: 'POST',
			action: thankyoupage
		}).appendTo('body');
		addHiddenFields('id_source', id_source, 'form_submit');
		addHiddenFields('leadNumber', data.id, 'form_submit');
		addHiddenFields('id_client', id_client, 'form_submit');
		addHiddenFields('id_crea', refC, 'form_submit');
		addHiddenFields('cookieEnable', cookiesEnabled, 'form_submit');
		addHiddenFields('sourcetype', array_typeofctc[ind], 'form_submit');

		if(Object.keys(addparam).length)
		{
			for(var i in addparam)
			{
				if(addparam.hasOwnProperty(i))
					addHiddenFields('addparam['+i+']', addparam[i], 'form_submit');
			}
		}

		 $('#form_submit').submit();
	} else {
		msg = '<br />Ha habido un error al procesar los datos.';
		msg += data.mensaje ? '<br /><br />' + data.mensaje + "<br />" : '<br />';
		msg += '<br />Por favor, inténtelo más tarde.<br /><br />';
		if (data.mensaje == 'DUPLICADO') {
			msg = 'Ya existe éste teléfono en el sistema. <br /><br />Si desea ser contactado, por favor, déjenos su teléfono en el formulario superior.';
		} else if (data.mensaje == 'RECHAZADOWS') {
			msg = 'Usted ya es cliente de Jazztel , por favor póngase en contacto con Atención al cliente en el 1565';
		}
		if (!b_is_movil)
				_showModal('error', 'Error en el procesamiento de Datos', '<p>' + msg + '</p>');
		else
		{
			$.facebox.close();
			alert(clearMyMessage(msg));
		}
	}
}


function isEmpty(s) {
    return((s == null) || (s.length == 0));
}

function isWhitespace(s) {
    return(isEmpty(s) || reWhitespace.test(s));
}

//función que validará el formulario grande deprecated
function validateFormOld(ind) {
    $.facebox.loading();
    var ccrea = '';
    var nomb = '';
    var email = '';
    var telf = '';
    var prov = "";
    var error = 0;
    var msg = '';
    var prov = $('#sel_prov').val();
    //modificado by LFP 2013.11.19 Mario
    var prov_text = $('#sel_prov').find('option:selected').text();
    var legales = ($("#cblegales").is(':checked')) ? '1' : '0';
    if (typeof nomb_promo === 'undefined') {
        ccrea = 'SANITAS_1';
    } else {
        ccrea = nomb_promo;
    }
    var source = '';
    if (typeof id_source === 'undefined') {
        source = '';
    } else {
        source = id_source;
    }
    nomb = $('#nombre').val();
    email = $('#email').val();
    var apell = $('#apellidos').val();
    telf = $('#telefono').val();
    if (ind == 0 || ind == 2) {
        telf = $('#telefono_ctc').val();
    }
    prov = $('#sel_prov').val();

    //Validamos ahora dependiendo del parámetro ind de entrada.
    if (ind == 1) { //formulario largo

        if (nomb.length < 1 || isWhitespace(nomb) || nomb == 'Nombre y apellidos') {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            $('#nombre').focus();
        }
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono').focus();
        }
        else if (!(comprobarExprRegular(regExTelf, telf))) {
            error = 1;
            msg += "<p>* El número de teléfono solo admite 9 números sin espacio (p.ej 912345678 ó 612345678)</p>";
            $('#telefono').focus();
        }

//        if ((typeof is_prov !== 'undefined') && prov <= 1 && prov.length < 1) {
        if (prov <= 0 || prov.length < 1) {
            error = 1;
            msg += "<p>* Debes seleccionar una Provincia</p>";
            $('#sel_prov').focus();
        }
        if (email.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Email</p>";
            $('#email').focus();
        }
        else if (!(comprobarExprRegular(regExEmail, email))) {
            error = 1;
            msg += "<p>* Formato de Email incorrecto.</p>";
            $('#email').focus();
        }

        if (legales != '1') {
            error = 1;
            msg += "<p>*Debes aceptar la Política de Privacidad</p>";
        }
    } else if (ind == 0) { //Formulario CTC
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono_ctc').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono_ctc').focus();
        }
    } else if (ind == 2) { //Formulario especial con teléfono, nombre y Legales

        if (nomb.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            $('#nombre').focus();
        }
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono_ctc').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono_ctc').focus();
        }
        if (legales != '1') {
            error = 1;
            msg += "<p>*Debes aceptar la Política de Privacidad</p>";
        }
    } else if (ind == 3) { //Formulario especial con teléfono, nombre 
        //alert(ind);   
        if (nomb.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";
            $('#nombre').focus();
        }
        else if (!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            $('#nombre').focus();
        }
        if (telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            $('#telefono').focus();
        }
        else if (telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            $('#telefono').focus();
        }
    }
    if (error != 0) {
        _showModal('error', 'Error en los Datos Introducidos', msg);
    } else {//Mandamos.
        //modificado por LFP 2013.11.26.
        //Se requiere mandar como si fuese un LEAD (ind==1) y no como un CTC (ind==0)
        if (ind == 3) {
            ind = 1;
        }
        if (ind >= 2) {
            ind = 0;
        }
        var valortype = array_typeofctc[ind];
        var campana = arr_campanas[ind];
        var prioridad = array_priorities[ind];
        $.ajaxSetup({async: false});
        //modificado by LFP 2013.11.19 Mario
        $.post(root_path_local + "ajaxs/procesar_registro_ctc.php", {idclient: id_client, sourcetype: valortype, campaign: campana, fuente: source, em: email, nombre: nomb, apellidos: apell, telefono: telf, provincia: prov, provincia_text: prov_text, cblegales: legales, crea: ccrea, prioridad: prioridad, use_priority: 1}, function (data) {
            ComprobarInsercion(data, ind);
        }, '');	//,em:email
    }

}
/**
 * Función que comprobará o gestionará la cookie dentro del formulario
 */
function checkCookie() {
    if (cookiesManaged == 0) {
        manageCookies(1);
    }
}

$(document).ready(function () {

    //función que validará el formulario (M.F. 08.04.2014)
    $(".sendData").click(function (el) {
        res = validator.form();
        if (!res)
            return;

        $.facebox.loading();
        var ccrea = "";
        if (typeof nomb_promo === 'undefined') {
            ccrea = 'SANITAS_1';
        } else {
            ccrea = nomb_promo;
        }
        var source = "";
        if (typeof id_source === 'undefined') {
            source = "";
        } else {
            source = id_source;
        }

        ind = 1;
        if (is_ctc == 1) {
            ind = 0;
        }

        var valortype = array_typeofctc[ind];
        var campana = arr_campanas[ind];
        $.ajax({
            url: root_path_local + "ajaxs/procesar_registro_ctc.php",
            method: "post",
            dataType: "json",
            data: {
                idclient: id_client,
                sourcetype: valortype,
                campaign: campana,
                spain_city: $('#spain_city').val(),
								comentarios:$('#comentarios').val(),
                fuente: source,
                id_source: id_source,
                id_crea: id_crea,
                em: $('#email').val(),
                nombre: $('#nombre').val(),
                apellidos: $('#apellidos').val(),
                telefono: $('#telefono').val(),
                provincia: $('#sel_prov').val(),
                addparam: addparam,
                sec_tok: sec_tok,
                provincia_text: $('#sel_prov').find('option:selected').text(),
                cblegales: 1,
                crea: ccrea
            },
            cache: false,
            async: true,
            success: function (response)
            {
                ComprobarInsercion(response, ind);
            },
            error: function (response) {
                console.log("err code 2");
            }
        })	// ajax end

    });

    $("body").append("<div id='opaque' style='display: none;'></div>");
    $('#telefono').keypress(function (e) {
        return SoloCompatibleTlf(e);
    });
    //Si existe el teléfono_ctc, lo añadimos
    if ($('#telefono_ctc').length) {
        $("#telefono_ctc").keypress(function (e) {
            return SoloCompatibleTlf(e);
        });
    }

    //Política de cookies
    $(':input').focusin(function () {
        if ($(this).attr('id') == "cookie_rejection" || $(this).attr('id') == "cookie_acept")
            return false;
        checkCookie();
    });
    $(':checkbox').click(function (e) {
        checkCookie();
    });
    if (is_ctc == 1) {
        $("#telefono_ctc").keypress(function (e) {
            return SoloCompatibleTlf(e);
        });
    }

    $('#btnProcesar_ctc').click(function (e) {
        e.preventDefault();
        validateFormOld(0);

    }); //btnProcesar_ctc
    /**
     * Nombre y teléfono
     */
    $('#btnProcesar_ctc_movil').click(function (e) {
        e.preventDefault();
        validateFormOld(3);

    });
});

var regExTelf = /([9|6|7|8])+[0-9]{8}/;// /^[6-7]{1}\d{8}$/
var regexOnlyLetters = /^[ñÑáÁéÉíÍóÓúÚçÇa-zA-Z\ \']+$/;
var regexCP = /^[0-9]{4,5}/;
var regExEmail = /^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/;	