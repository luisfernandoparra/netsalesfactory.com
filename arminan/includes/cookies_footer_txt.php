<?php
@session_start();
// DEFINE COOKIES TEXT VARS 
$cookieCompanyNameFull='FINTEL MARKETING SLU';
$cookieCompanyNameShort='LA AGENCIA';
/*
 * LISTA DE LAS COOKIES UTILIZADAS EN ESTA PAGINA
 */
$arrThirdCookies=array('_ga'=>'Es necesaria para el funcionamiento de Google Analytics y tiene un período de caducidad de 2 años');

?>
<h1>POL&Iacute;TICA DE COOKIES ARMI&Ntilde;AN</h1>

<p><b>¿Qu&eacute; son las cookies?</b></p>

<p>Nuestras webs utilizan cookies, un sistema de archivos que pueden
descargarse en su equipo a trav&eacute;s de nuestra p&aacute;gina. Las cookies son
herramientas que tienen un papel esencial para la prestaci&oacute;n de
numerosos servicios de la sociedad de la informaci&oacute;n. Entre otros,
permiten a una p&aacute;gina web almacenar y recuperar informaci&oacute;n sobre
los h&aacute;bitos de navegaci&oacute;n de un usuario o de su equipo y,
dependiendo de la informaci&oacute;n obtenida, se pueden utilizar para
reconocer al usuario y mejorar el servicio ofrecido.</p>

<p><b>¿Qu&eacute; tipos de cookies existen?</b></p>

<p>Dependiendo de la entidad que gestione el dominio desde donde se
env&iacute;an las cookies y traten los datos que se obtengan se pueden
distinguir dos tipos: cookies propias y cookies de terceros.
Existe tambi&eacute;n una segunda clasificaci&oacute;n seg&uacute;n el plazo de tiempo
que permanecen almacenadas en el navegador del cliente, pudiendo
tratarse de cookies de sesi&oacute;n o cookies persistentes.
Por &uacute;ltimo, existe otra clasificaci&oacute;n con cinco tipos de cookies se &uacute;n la
finalidad para la que se traten los datos obtenidos: cookies t&eacute;cnicas,
cookies de personalizaci&oacute;n, cookies de an&aacute;lisis, cookies publicitarias y
cookies de publicidad comportamental.
Para m&aacute;s informaci&oacute;n a este respecto puede consultar la Gu&iacute;a sobre
el uso de las cookies de la Agencia Espa&ntilde;ola de Protecci&oacute;n de Datos
Este enlace se abre en una nueva pantalla.</p>

<p><b>Cookies utilizadas en la web</b></p>

<p><b>A continuaci&oacute;n identificamos las cookies que est&aacute;n siendo
utilizadas en nuestras webs, as&iacute; como su tipolog&iacute;a y funci&oacute;n:</b>

<p><b>Cookies de an&aacute;lisis:</b> Son aqu&eacute;llas que bien tratadas por nosotros,
nos permiten cuantificar el n&uacute;mero de usuarios y as&iacute; realizar la
medici&oacute;n y an&aacute;lisis estad&iacute;stico de la utilizaci&oacute;n que hacen los usuarios
del servicio ofertado. Para ello se analiza su navegaci&oacute;n en nuestra
p&aacute;gina web con el fin de mejorar la oferta de productos o servicios que
le ofrecemos. Estas cookies no ir&aacute;n asociadas a ning&uacute;n dato de
car&aacute;cter personal que pueda identificar al usuario, dando informaci&oacute;n
sobre el comportamiento de navegaci&oacute;n de forma an&oacute;nima.</p>

<p><b>Cookies publicitarias:</b> Son aqu&eacute;llas que, bien tratadas por nosotros,
nos permiten gestionar de la forma m&aacute;s eficaz posible la oferta de los
espacios publicitarios que hay en la p&aacute;gina web, adecuando el
contenido del anuncio al contenido del servicio solicitado o al uso que
realice de nuestra p&aacute;gina web. Para ello podemos analizar sus h&aacute;bitos
de navegaci&oacute;n en Internet y podemos mostrarle publicidad relacionada
con su perfil de navegaci&oacute;n.</p>

<p><b>Google Analytics:</b> Es la herramienta de anal&iacute;tica que ayuda a los
sitios web y a los propietarios de aplicaciones a entender el modo en
que sus visitantes interact&uacute;an con sus propiedades. Puede utilizar un
conjunto de cookies para recopilar informaci&oacute;n e informar de las
estad&iacute;sticas de uso de los sitios web sin identificar personalmente a
los visitantes de Google. Si lo desea, puede encontrar m&aacute;s
informaci&oacute;n sobre los tipos de cookies que utiliza Google aqu&iacute;. Si
usted quiere puede denegar el permiso para el tratamiento estad&iacute;stico
de los datos o la informaci&oacute;n con Google Analytics. ARMI&ntilde;AN
CATERING no puede controlar ni se hace responsable del contenido y
veracidad de los t&eacute;rminos y condiciones y las pol&iacute;ticas de privacidad
de Google referenciadas en esta pol&iacute;tica de cookies.<p>

<p><b>Otras cookies de terceros:</b> En algunas de nuestras p&aacute;ginas se
pueden instalar cookies de terceros que permitan gestionar y mejorar
los servicios que &eacute;stos ofrecen. Un ejemplo de este uso son los
enlaces a las redes sociales que permiten compartir nuestros
contenidos.</p>

<p><b>Aceptaci&oacute;n de la pol&iacute;tica de cookies</b><p>

<p>Navegando por las p&aacute;ginas webs propiedad de Armi&ntilde;an Catering se
asume que usted acepta el uso de las cookies. No obstante,
mostraremos la informaci&oacute;n sobre la Pol&iacute;tica de cookies en nuestras
p&aacute;ginas con cada inicio de sesi&oacute;n con el objeto de que usted sea
consciente de su existencia.</p>

<p><b>¿Se puede modificar la configuraci&oacute;n de las cookies?</b>
Usted podr&aacute; restringir, bloquear o borrar las cookies de las web de
Armi&ntilde;an Catering, utilizando su navegador. En cada navegador la
operaci&oacute;n es diferente y la funci&oacute;n de ?Ayuda? le mostrar&aacute; c&oacute;mo
hacerlo.</p>
<p>C&oacute;mo modificar la configuraci&oacute;n de las cookies en Internet</p>
<p>C&oacute;mo modificar la configuraci&oacute;n de las cookies en FireFox</p>
<p>C&oacute;mo modificar la configuraci&oacute;n de las cookies en Chrome</p>

<p><b>C&oacute;mo modificar la configuraci&oacute;n de las cookies en Safari</b></p>

<p>Su navegador tambi&eacute;n puede incluir la posibilidad de seleccionar con
detalle las cookies que desea que se instalen en su ordenador. En
concreto, el usuario puede normalmente aceptar alguna de las
siguientes opciones:</p>

<p><b>Adem&aacute;s, tambi&eacute;n puede gestionar el almac&eacute;n de cookies en su
navegador a trav&eacute;s de herramientas como las siguientes:</b></p>

<p>Ghostery</p>
<p>Your online choices</p>

<p><b>Ley aplicable y jurisdicci&oacute;n</b></p>

<p>La relaci&oacute;n entre Armi&ntilde;an Catering y el USUARIO se regir&aacute; por la
normativa espa&ntilde;ola vigente y cualquier controversia se someter&aacute; a la
jurisdicci&oacute;n de los Tribunales de Madrid, a la que el USUARIO se
somete expresamente.</p>