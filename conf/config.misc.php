<?php
/**
 * Fichero cargado desde el config donde se almacenerán variables del entorno miscelaneas (sin sección definida)
 * @version 1.1
 * @author LFP
 * @date 2016.08.01
 */
//echo 'conf/config.misc.php ---->'.date('H:i:s');die();
$logs_emails = 'mario.francescutto@dujok.com';
$versionGTP='1.2/'.substr(PHP_VERSION,0,3);
$versionBackOffice=$versionGTP.($sitesPruebas ? ' <span class=c-yellow title="estás en el servidor de desarrollo">(DEV)</span>' : '');

// ARRAY CON TODOS LOS POSIBLES NOMBRES DE CAMPOS DE ESTADO ACTIVO/DESACTIVO DE LA TABLA UTILIZADA
$arrEnabledFieldStatus=array(
  'b_enabled', 'is_enabled', 'b_active', 'activo'
);

//Sql Injection
$arr_sql_inject_original=array('select','insert','drop','create','execute','union all','union','like','truncate','delete','concat','servers','mysql','order by','host.','unhex','authentication','--','test.','show tables','response','write','/etc','md5','acunetix_wvs_security_test','{');

$arr_sql_inject_cambio=array('s3l3ct','1ns4rt','dr0p','cr34te','3x3cut3','4un10n a11','4un10n','1ik3','t4unc4t3','d31et3','c0nc4t','s3rv3rs','m4sq1','ord3r b4','h0st.','4nh3x','4unth3nticat10n','','t3st.','chow t4bl3s','r3spons3','wr1t3','3tc','m45','acun3t1x_wvs_s3curity_t3st','(');

$arr_meses=array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
$mensaje_error='Ha habido un error en el proceso. Int&eacute;ntelo de nuevo';

// LANGS GLOBAL SETTINGS
define('__URL_ISO3_FLAGS', $web_url.'isoFlags/');
define('__URL_CSS_FILES', $web_url.'css/');
define('__HASH_CONTROL_SCRIPT', md5(date('Ymd')).'DUJOK'.md5(date('mmm')));
define('__MSG_ACCESS_ERROR', '<div style=display:inline-block;width:80%;background:#000;color:red;padding:10%;text-align:center;font-size:200%;font-family:verdana,arial,serif;>ERROR DE ACCESO<br /></div>');

/**
 * ARRAY CON PLUG-INS DISPONIBLES PARA EL ACTUAL BACK-OFFICE
 */
$arrPluginAddOns=array(
  'topChat'=>array(
    'folderPlugin'=>$dirPluginsAdmin.'top_chat/'
    ,'baseFolderPlugin'=>$web_url.$adminFolder.'/_plugins/top_chat/'
    ,'dirPlugin'=>$dirPluginsAdmin.'top_chat/'
    ,'fileName'=>'index_plugin.php'
    ,'bbddPlugin'=>array('tblNameOnline'=>'system_ajax_chat_online','tblNameMessages'=>'system_ajax_chat_messages','tblNameBans'=>'system_ajax_chat_bans','tblNameInvitations'=>'system_ajax_chat_invitations')
    ,'minAccessLevel'=>__LEVEL_ACCESS_OPERATOR
  ),
  'topNotes'=>array(
    'folderPlugin'=>$dirPluginsAdmin.'top_notes/'
    ,'baseFolderPlugin'=>$web_url.$adminFolder.'/_plugins/top_notes/'
    ,'dirPlugin'=>$dirPluginsAdmin.'top_notes/'
    ,'fileName'=>'modal_notes.php'
    ,'tblName'=>'system_plugin_top_user_notes'
    ,'minAccessLevel'=>__LEVEL_ACCESS_OPERATOR
  )
);

if(!isset($arrJsAdditionalPlugins))
  $arrJsAdditionalPlugins=array(); // SE INICIALIZA EL CONTENEDOR DE JS DE LOS PLUG-INS CARGADOS

$arrAllowedUploadExtensions=array('js','txt','doc','docx','pdf','gif','jpg','png','jpeg','bmp','ttf','woff','woff2','svg','eot','otf','css','php');

// COLORES POR DEFECTO PARA LOS ELEMENTOS COMUNES CONFIGURABLES
$arrDefaultColor=array(
  'background_bckCol'=>'red'
  ,'btn_default_bckCol'=>'white'
  ,'btn_default_forCol'=>'red'
  ,'top_header_theme_bckCol'=>'red'
);

$arrForeAdminColors=array(
  'white'=>'Blanco'
  ,'black'=>'Negro'
  ,'brown'=>'Marr&oacute;n'
  ,'pink'=>'Rosa'
  ,'red'=>'Rojo'
  ,'blue'=>'Azul'
  ,'purple'=>'P&uacute;rpura'
  ,'deeppurple'=>'P&uacute;rpura oscuro'
  ,'lightblue'=>'Azul claro'
  ,'cyan'=>'Ciano'
  ,'teal'=>'Turquesa'
  ,'green'=>'Verde'
  ,'lightgreen'=>'Verde claro'
  ,'lime'=>'Lima'
  ,'yellow'=>'Amarillo'
  ,'amber'=>'&Aacute;mbar'
  ,'orange'=>'Naranja'
  ,'deeporange'=>'Naranja oscuro'
  ,'gray'=>'Gris'
  ,'bluegray'=>'Gris azulado'
  ,'indigo'=>'Indigo'
);

$arrBackAdminColors=array(
  'white'=>'Blanco'
  ,'black'=>'Negro'
  ,'brown'=>'Marr&oacute;n'
  ,'pink'=>'Rosa'
  ,'red'=>'Rojo'
  ,'blue'=>'Azul'
  ,'purple'=>'P&uacute;rpura'
  ,'deeppurple'=>'P&uacute;rpura oscuro'
  ,'lightblue'=>'Azul claro'
  ,'cyan'=>'Ciano'
  ,'teal'=>'Turquesa'
  ,'green'=>'Verde'
  ,'lightgreen'=>'Verde claro'
  ,'lime'=>'Lima'
  ,'yellow'=>'Amarillo'
  ,'amber'=>'&Aacute;mbar'
  ,'orange'=>'Naranja'
  ,'deeporange'=>'Naranja oscuro'
  ,'gray'=>'Gris'
  ,'lightgray'=>'Gris muy claro'
  ,'bluegray'=>'Gris azulado'
  ,'indigo'=>'Indigo'
);

// COLORES AUTOMATICOS PARA PRIMER PLANO DE FONDOS CONFIGURADOS POR EL USUARIO
$arrForeHeaderAutoColor=array(
  'white'=>'black'
  ,'black'=>'white'
  ,'brown'=>'white'
  ,'pink'=>'white'
  ,'red'=>'white'
  ,'blue'=>'white'
  ,'purple'=>'white'
  ,'deeppurple'=>'white'
  ,'lightblue'=>'black'
  ,'cyan'=>'black'
  ,'teal'=>'white'
  ,'green'=>'white'
  ,'lightgreen'=>'black'
  ,'lime'=>'black'
  ,'yellow'=>'black'
  ,'amber'=>'black'
  ,'orange'=>'black'
  ,'deeporange'=>'white'
  ,'gray'=>'black'
  ,'bluegray'=>'white'
  ,'indigo'=>'white'
);

$arrIso3ToIso2=array(
'and'=>'ad','are'=>'ae','afg'=>'af','atg'=>'ag','aia'=>'ai','alb'=>'al','arm'=>'am','ago'=>'ao','ata'=>'aq','arg'=>'ar','asm'=>'as','aut'=>'at','aus'=>'au','abw'=>'aw','ala'=>'ax','aze'=>'az','bih'=>'ba','brb'=>'bb','bgd'=>'bd','bel'=>'be','bfa'=>'bf','bgr'=>'bg','bhr'=>'bh','bdi'=>'bi','ben'=>'bj','blm'=>'bl','bmu'=>'bm','brn'=>'bn','bol'=>'bo','bes'=>'bq','bra'=>'br','bhs'=>'bs','btn'=>'bt','bvt'=>'bv','bwa'=>'bw','blr'=>'by','blz'=>'bz','can'=>'ca','cck'=>'cc','cod'=>'cd','caf'=>'cf','cog'=>'cg','che'=>'ch','civ'=>'ci','cok'=>'ck','chl'=>'cl','cmr'=>'cm','chn'=>'cn','col'=>'co','cri'=>'cr','cub'=>'cu','cpv'=>'cv','cuw'=>'cw','cxr'=>'cx','cyp'=>'cy','cze'=>'cz','deu'=>'de','dji'=>'dj','dnk'=>'dk','dma'=>'dm','dom'=>'do','dza'=>'dz','ecu'=>'ec','est'=>'ee','egy'=>'eg','esh'=>'eh','eri'=>'er','esp'=>'es','eth'=>'et','fin'=>'fi','fji'=>'fj','flk'=>'fk','fsm'=>'fm','fro'=>'fo','fra'=>'fr','gab'=>'ga','gbr'=>'gb','grd'=>'gd','geo'=>'ge','guf'=>'gf','ggy'=>'gg','gha'=>'gh','gib'=>'gi','grl'=>'gl','gmb'=>'gm','gin'=>'gn','glp'=>'gp','gnq'=>'gq','grc'=>'gr','sgs'=>'gs','gtm'=>'gt','gum'=>'gu','gnb'=>'gw','guy'=>'gy','hkg'=>'hk','hmd'=>'hm','hnd'=>'hn','hrv'=>'hr','hti'=>'ht','hun'=>'hu','idn'=>'id','irl'=>'ie','isr'=>'il','imn'=>'im','ind'=>'in','iot'=>'io','irq'=>'iq','irn'=>'ir','isl'=>'is','ita'=>'it','jey'=>'je','jam'=>'jm','jor'=>'jo','jpn'=>'jp','ken'=>'ke','kgz'=>'kg','khm'=>'kh','kir'=>'ki','com'=>'km','kna'=>'kn','prk'=>'kp','kor'=>'kr','xkx'=>'xk','kwt'=>'kw','cym'=>'ky','kaz'=>'kz','lao'=>'la','lbn'=>'lb','lca'=>'lc','lie'=>'li','lka'=>'lk','lbr'=>'lr','lso'=>'ls','ltu'=>'lt','lux'=>'lu','lva'=>'lv','lby'=>'ly','mar'=>'ma','mco'=>'mc','mda'=>'md','mne'=>'me','maf'=>'mf','mdg'=>'mg','mhl'=>'mh','mkd'=>'mk','mli'=>'ml','mmr'=>'mm','mng'=>'mn','mac'=>'mo','mnp'=>'mp','mtq'=>'mq','mrt'=>'mr','msr'=>'ms','mlt'=>'mt','mus'=>'mu','mdv'=>'mv','mwi'=>'mw','mex'=>'mx','mys'=>'my','moz'=>'mz','nam'=>'na','ncl'=>'nc','ner'=>'ne','nfk'=>'nf','nga'=>'ng','nic'=>'ni','nld'=>'nl','nor'=>'no','npl'=>'np','nru'=>'nr','niu'=>'nu','nzl'=>'nz','omn'=>'om','pan'=>'pa','per'=>'pe','pyf'=>'pf','png'=>'pg','phl'=>'ph','pak'=>'pk','pol'=>'pl','spm'=>'pm','pcn'=>'pn','pri'=>'pr','pse'=>'ps','prt'=>'pt','plw'=>'pw','pry'=>'py','qat'=>'qa','reu'=>'re','rou'=>'ro','srb'=>'rs','rus'=>'ru','rwa'=>'rw','sau'=>'sa','slb'=>'sb','syc'=>'sc','sdn'=>'sd','ssd'=>'ss','swe'=>'se','sgp'=>'sg','shn'=>'sh','svn'=>'si','sjm'=>'sj','svk'=>'sk','sle'=>'sl','smr'=>'sm','sen'=>'sn','som'=>'so','sur'=>'sr','stp'=>'st','slv'=>'sv','sxm'=>'sx','syr'=>'sy','swz'=>'sz','tca'=>'tc','tcd'=>'td','atf'=>'tf','tgo'=>'tg','tha'=>'th','tjk'=>'tj','tkl'=>'tk','tls'=>'tl','tkm'=>'tm','tun'=>'tn','ton'=>'to','tur'=>'tr','tto'=>'tt','tuv'=>'tv','twn'=>'tw','tza'=>'tz','ukr'=>'ua','uga'=>'ug','umi'=>'um','usa'=>'us','ury'=>'uy','uzb'=>'uz','vat'=>'va','vct'=>'vc','ven'=>'ve','vgb'=>'vg','vir'=>'vi','vnm'=>'vn','vut'=>'vu','wlf'=>'wf','wsm'=>'ws','yem'=>'ye','myt'=>'yt','zaf'=>'za','zmb'=>'zm','zwe'=>'zw','scg'=>'cs','ant'=>'an');

$arrDates=array();

if(isset($_SESSION['userAdmin']['pref_']['top_header_theme_bckCol']))
  $foreAutoColor='c-'.$arrForeHeaderAutoColor[substr($_SESSION['userAdmin']['pref_']['top_header_theme_bckCol'], 4)];
else
  $foreAutoColor='c-white';

asort($arrForeAdminColors);
asort($arrBackAdminColors);

if(isset($_SESSION['userAdmin']['pref_']['ref_lang']))
{
	$dirLang=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/lang/';

	if(file_exists($dirLang))
	{
		include_once($dirLang.'common_'.strtolower($_SESSION['userAdmin']['pref_']['ref_lang']).'.php');
	}
}
