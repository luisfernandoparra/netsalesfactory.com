<?php
/**
 * NOTA
 *
 * ESTE ARCHIVO ESTA UBUICADO EN EL SERVIDOR DE VTP
 *
 * LA CARPETA SONDE ESTÁ UBICADO EL BACK-OFFICE DE LAS LANDINGS ES promociones_netsales
 *
 * 
 */
//@header('Content-Type: text/html; charset=utf-8');
if(isset($_SERVER['HTTPS'])) {
  $secure = true;
  session_set_cookie_params($secure);
//  session_set_cookie_params($secure,null,null,$secure);
}
@session_start();
/*
 * ========================================
 * =====> LOCAL CONFIGURATION SCRIPT <=====
 * ========================================
 *
 * ###################################################################################################################################################################
 * CONFIGURACIONES COMUMES (ADMIN / FRONT ) NOTA: las variables marcadas con (*) son las que hay que cambiar en funcion del proyecto
 * ###################################################################################################################################################################
 */
$developerIp=$_SERVER['REMOTE_ADDR'];
$rootMaravillaoFolder='';	// CARPETA ORIGINAL PARA MARAVILLAO

// START GLOBAL CHECK PHONE VARS (16.06.2014)
// generacion para los datos de acceso (dinamicos) del bridge con PORTUGAL
$secret_pwd='p3Rd€r_N@da'; $phoneCheckUserAccess=md5(date('Y-m-d H:i:s')); $phoneCheckPasswordAccess=md5($phoneCheckUserAccess.$secret_pwd);
// curl url phone check control
$phoneCheckUrlTarget='http://212.71.235.216/webservices/ping_server/ws_ping_spain.php';
// END GLOBAL CHECK PHONE VARS

$controlChekAlgorithm='--'.md5(date('mYd').md5($_SERVER['SERVER_ADDR'])).(md5(session_id()));	// HASH DE CONTROL PARA AJAX (M.F. 2017.05.16)

/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	EL SIGUIENTE ARRAY SOLO SE DEBE CONFIGURAR SI ´PROMOCIONES´ EN MARAVILLAO ESTA DISTRIBUIDA PARA LOS VARIOS DEVELOPERS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
$developersLocalFolder=array(
//		'192.168.2.59'=>'luisfer/',
//		'192.168.2.59'=>'mario/'
);

//$sitesPruebas=($_SERVER['SERVER_ADDR'] == 'localhost' || $_SERVER['SERVER_ADDR'] == '127.0.0.1'|| $_SERVER['SERVER_ADDR'] == '139.162.246.12');	// SITIOS PARA PRUEBAS
$sitesPruebas=($_SERVER['SERVER_ADDR'] == '139.162.246.12' || $_SERVER['SERVER_ADDR'] == '127.0.0.1' || $_SERVER['SERVER_ADDR'] == '139.162.246.12' || $_SERVER['SERVER_ADDR'] == '212.0.115.183' || $_SERVER['SERVER_ADDR'] == '192.168.2.102'); // SITIOS PARA PRUEBAS
$sitesPruebas=0;
$debugModeNew=(int)@$_REQUEST['debugModeNew'];
$displayErrors=0;
$debugDujok=null;
$port=5432;

$bbdd_logs='netsalesfactory';	// NOMBRE BBDD PARA LOGS WEB PUBLICA
$tbl_system='nl_sys_';

//@define('QUERY_SEND_EMAIL_TRACKING', 'mario.francescutto@netsales.es');
define('__LEVEL_ACCESS_GOD', 20);
define('__LEVEL_ACCESS_MASTER', 18);
define('__LEVEL_ACCESS_RESPONSIBLE', 15);
define('__LEVEL_ACCESS_OPERATOR', 12);
define('__LEVEL_ACCESS_VISITOR', 10);
define('__LEVEL_ACCESS_VISIT', 10);

@define('__QUERY_TABLES_PREFIX', 'nl_');
@define('__TABLE_CURRENT_LOGS', 'log_backoffice_'.date('Y'));
@define('__TABLE_FRONT_WEB_LOGS', 'log_front_web_'.date('Y'));

$passwordsSuthFile='/etc/.htpasswd';
$errorReportDefault='E_ALL | E_STRICT';
$errorReportDefault='';
$autoUpdateTime=isset($_SESSION['userAdmin']['pref_']['auto_update_time']) ? (int)$_SESSION['userAdmin']['pref_']['auto_update_time'] : 5;
error_reporting($errorReportDefault);  # ...but do log them

$http_host = $_SERVER['HTTP_HOST'];
$webtitle = 'N.F. landings';
$folderRootApp = 'promociones_netsales/'; //DIRECTORIO ROOT DEL SITE
$folderRootFront = $_SERVER['DOCUMENT_ROOT'].''; //DIRECTORIO ROOT DEL SITE

$systemData=new stdClass();	// PARAMETROS GENERALES DEL BACK-OFFICE
// BASIC ADMIN PARAMS
$systemData->minStdEnableRole=__LEVEL_ACCESS_VISIT;
$systemData->minStdEditRole=__LEVEL_ACCESS_OPERATOR;
$systemData->minStdDeleteRole=__LEVEL_ACCESS_RESPONSIBLE;
$systemData->minSysEnableRole=__LEVEL_ACCESS_MASTER;
$systemData->minSysEditRole=__LEVEL_ACCESS_MASTER;
$systemData->minSysDeleteRole=__LEVEL_ACCESS_GOD;

if($sitesPruebas)
{
	$adminFolder='_Term1n@tor2018_DEV';
	@define('ADMINFOLDER', $adminFolder);

	$passwordsSuthFile='/var/www/promociones_netsales/'.$adminFolder.'/.htpasswd';
  $fromServer='//'.$_SERVER['HTTP_HOST'];
	$web_url = $fromServer.'/promociones_netsales/';
  $dir_raiz_aplicacion=$rootMaravillaoFolder.'promociones_netsales';
	$folderRootApp=$dir_raiz_aplicacion.'/';
  $path_raiz_aplicacion = '/'.$dir_raiz_aplicacion;
  $webtitle = 'DEMO-PROMO';	//Site Title (pages title) (*)
  $displayErrors = true;
  $debugDujok=true;

	// START DDBB COMPARE URL
	$thisUrl=parse_url($_SERVER['REQUEST_URI']);
	$firstUriLabel=substr($thisUrl['path'],0,-1);
	$posLastBackSlash=strripos($firstUriLabel,'/');
	$firstUriLabel=substr($thisUrl['path'],$posLastBackSlash+1);
	$uriParams=(empty($thisUrl['query'])) ? '' : $thisUrl['query'];	// PARAMETROS DE LA URL
	// END DDBB COMPARE URL
  $path_raiz_includes=$_SERVER['DOCUMENT_ROOT'].trim($path_raiz_aplicacion).'/';		//UTILIZADO PARA INCLUIR LOS FICHEROS SERVIDOR DEL FRONT .php
	$path_raiz_includes_admin=$path_raiz_includes.$adminFolder.'/';
  $path_imagenes='//'.$_SERVER['HTTP_HOST'].$path_raiz_aplicacion.'emails/img/';	//usado para envio de emails con imagenes

  $dir_raiz_ws='../../';	//DIRECTORIO ROOT DEL SITE (*)
  $path_raiz_ws ='/'.$dir_raiz_ws;
  //$file_ws	='ctcInConcert.php';
  //$file_ws	='ctcAlisys.php';
  $file_ws	='router.php';
  $url_ws = 'https://ws-server.netsales.es/webservices/clickToCall/';
  $url_ws_defaulter = 'https://ws-server.netsales.es/webservices/clickToCall/';
  $url_repository_files = '??????????/extraccion/files/';
  $db_user='user_connect';
  $db_pass='L1nK4me@!';
  $db_host='127.0.0.1';
  $db_name='netsalesfactory';
  $db_type='MSSQLi';
  $dir_raiz_aplicacion='//'.$_SERVER['HTTP_HOST'].'/'.$dir_raiz_aplicacion.'/'; // PARCHE 28.01.2014 M.F.
	$path_raiz_aplicacion.='/';
	$port=1312;

	if($_SERVER['SERVER_ADDR'] == '192.168.2.102')
	{
		$dir_raiz_ws='../../';
		$port=3306;
		$db_user='root';
		$db_pass='';
		$db_host='localhost';
		$db_host='127.0.0.1';
		$db_name='netsalesfactory';
		$passwordsSuthFile='F:/www/netsalesfactory.com/promociones/'.$adminFolder.'/.htpasswd';
	}
}
else
{
	$adminFolder='_Term1n@tor2018';
	@define('ADMINFOLDER', $adminFolder);

	$port=3306;

  $fromServer='//'.$_SERVER['HTTP_HOST'];
	$web_url = $fromServer.'/promociones_netsales/';

  $dir_raiz_aplicacion='promociones_netsales';
  $path_raiz_aplicacion= '/'.$dir_raiz_aplicacion;
  $webtitle='Bienvenido'; 				//Site Title (pages title) (*)
  $path_raiz_includes=$_SERVER['DOCUMENT_ROOT'].trim($path_raiz_aplicacion);			//UTILIZADO PARA INCLUIR LOS FICHEROS SERVIDOR DEL FRONT .php
  $path_imagenes='//'.$_SERVER['HTTP_HOST'].$path_raiz_aplicacion.'emails/img/';	//usado para envio de emails con imagenes

	// START DDBB COMPARE URL
	$thisUrl=parse_url($_SERVER['REQUEST_URI']);
	$firstUriLabel=substr($thisUrl['path'],0,-1);
	$posLastBackSlash=strripos($firstUriLabel,'/');
	$firstUriLabel=substr($thisUrl['path'],$posLastBackSlash+1);
	$uriParams=@$thisUrl['query'];	// PARAMETROS DE LA URL
	// END DDBB COMPARE URL

  $dir_raiz_ws  = 'netsalesfactory.com/acciones/webservice/'; 								//DIRECTORIO ROOT DEL SITE (*)
  $path_raiz_ws = '/'.$dir_raiz_ws;
  $file_ws = 'router.php';
  $url_ws = 'https://ws-server.netsales.es/webservices/clickToCall/';
  //$url_ws_defaulter = 'https://ws-server.com/webservices/defaulter/';
	$url_ws_defaulter = 'https://ws-server.netsales.es/webservices/clickToCall/';

	$path_raiz_includes_admin	= $path_raiz_includes.'/'.$adminFolder;
  $url_repository_files = $path_raiz_includes_admin.'extraccion/files/';

  $db_user='user_connect';
  $db_pass='L1nK4me@!';
  $db_host='178.79.159.246';
  $db_name='netsalesfactory';
//  $db_type='MySQL';
  $db_type='MSSQLi';
}



$dirModAdmin=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_mod_bbdd/';
$dirClassesAdmin=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_classes/';
$dirPluginsAdmin=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/_plugins/';
$dirModulesAdmin=$_SERVER['DOCUMENT_ROOT'].'/'.$folderRootApp.$adminFolder.'/';
$pathBackupFiles=$dirModulesAdmin.'_backups/';
$clave_acceso = 'p3Rd€r_N@da';


//**********************************************************************************
///////////////COMMOM FILE VERSIONS (PRODUCCION / DESARROLLO)///////////////
//**********************************************************************************
define('JS_MINIFIED_SUFFIX','.min');  //PARA ENTORNOS DE PRODUCCION, DEBEMOS MINIMIZAR LOS JS PARA AUMENTAR EL RENDIMIENTO AL CARGAR, por tanto en el mismo directorio, tendremos los ficheros de desarrollo y los minimizados cCON ESTE SUFIJO que seran los que se suban a produccion
define('CSS_MINIFIED_SUFFIX','.min'); //PARA ENTORNOS DE PRODUCCION, DEBEMOS MINIMIZAR LOS CSS PARA AUMENTAR EL RENDIMIENTO AL CARGAR, por tanto en el mismo directorio, tendremos los ficheros de desarrollo y los minimizados cCON ESTE SUFIJO que seran los que se suban a produccion
define('JSCSS_SHOWTIME','1');         //AÃ‘ADIMOS A LOS CSS y JS LA FECHA DE ULTIMA MODIFICACION, POR SI TENEMOS ENTORNO CACHEADOS PARA QUE SE VUELVA A PEDIR AL SERVIDOR EL FICHERO y evitar que el usuario no vea el cambio

//***************************************************************************************************************************
//////////////COMMON DEBUG SETTINGS////////////////
//***************************************************************************************************************************
//FOR SHOW DEBUGGING QUERIES (VIA IP IF YOU WANT) AND TRACKING VIA EMAIL
define('SHOW_DEBUG','N');										//values: Y/N //activate / deactivate (ADMIN, FRONT) FOR ALL PEOPLE (*)
$ips_show_debug = array('62.97.72.29');			//'62.97.72.29' RANGO DE IPS QUE PODRAN VER EL DEBUG CUANDO ESTE SE ENCUENTRE EN 'N'. UTIL PARA PONER DEBUG EN PRODUCCION (*)
define('QUERY_SEND_EMAIL_TRACKING','1');		//0: no se envia email, 1 si; (*)
define('QUERY_EMAIL_TRACKING_TO','luisfer.parra@dujok.com');	//email al programador o programadores en caso de error en una query (*)
//FOR DEBUGGING POST AJAXS CALLS
$a_vars = $_POST;
$chk_var_control = (isset($_REQUEST['debugmode'])) ? trim($_REQUEST['debugmode']) : 0;
if($chk_var_control == 1){$a_vars = $_REQUEST;}

//***************************************************************************************************************************
//////////////BBDD TABLES,STORED PROCEDURES ETC NAMES (*)////////////////
//***************************************************************************************************************************

$table_registros = 'registros';				//nombre de la tabla con los usuarios con acceso al admin
$table_registros_capping = 'registros_capping';
$table_provincias= 'provincias';
$table_countries = 'countries';
$table_sources	 = 'sources';
//Tablas Administrador
$table_admin_usuarios = 'adminusers';

/*
//Sql Injection
$arr_sql_inject_original = array('select','insert','drop','create','execute','union all','union','like','truncate','delete','concat','servers','mysql','order by','host.','unhex','authentication','--','test.','show tables','response','write','/etc','md5','acunetix_wvs_security_test','{');
$arr_sql_inject_cambio = array('s3l3ct','1ns4rt','dr0p','cr34te','3x3cut3','4un10n a11','4un10n','1ik3','t4unc4t3','d31et3','c0nc4t','s3rv3rs','m4sq1','ord3r b4','h0st.','4nh3x','4unth3nticat10n','','t3st.','chow t4bl3s','r3spons3','wr1t3','3tc','m45','acun3t1x_wvs_s3curity_t3st','(');
*/
//sql iNJECTION
//$arr_sql_inject_original = array('select','insert','drop','create','execute','union','like','truncate','delete','%');
//$arr_sql_inject_cambio = array('s3l3ct','1ns4rt','dr0p','cr34te','3x3cut3','4un10n','1ik3','t4unc4t3','d31et3',' ');


//Sql Injection
$arr_sql_inject_original = array('select','insert','drop','create','execute','union all','union','like','truncate','delete','concat','servers','mysql','order by','host.','unhex','authentication','--','test.','show tables','response','write','/etc','md5','acunetix_wvs_security_test','{','table','function','from','"',"'",';','-',' order ','<',')','/','\\');
$arr_sql_inject_cambio = array('s3l3ct','1ns4rt','dr0p','cr34te','3x3cut3','4un10n a11','4un10n','1ik3','t4unc4t3','d31et3','c0nc4t','s3rv3rs','m4sq1','ord3r b4','h0st.','4nh3x','4unth3nticat10n','','t3st.','chow t4bl3s','r3spons3','wr1t3','3tc','m45','acun3t1x_wvs_s3curity_t3st','(','t4bl3','f4nct10n','fr0m','','','','',' 0rd3r ','','','','');


//Para generaciÃ³n de CSVs
$sep=';';
$eol="\r\n";
$encryptKeyRegistro='484nD34D0';
$adminRequest=strpos($_SERVER['PHP_SELF'],$adminFolder);

//echo $_SERVER['PHP_SELF'].'<hr>web ....<hr>'.$adminRequest.')'; die();
if(!$adminRequest)
{
	//INCLUDES NECESARIOS TANTO PARA DEBUG COMO PARA LA QUERIES
	///echo @$debugDujok ? $path_raiz_includes : '';
	require_once($path_raiz_includes.'class/class.debug.php');
	$debug_pagina = new CDEBUG();
	require_once($path_raiz_includes.'class/class.bbdd.php');
	require_once($path_raiz_includes.'class/class.dumpvar.php');
	//FUNCIONES COMUNES TANTO AL FRONT COMO AL ADMIN
	require_once($path_raiz_includes.'includes/funciones_comunes.php');
	/**Clase Misc **/
	require_once($path_raiz_includes.'class/class.misc.php');


	// START MODIFICACIONES PARA CONFIGURAR SITE DESDE BBDD (M.F. 04.2014)
	$prefixTbl='nl_';
	$table_landings_site_config=$prefixTbl.'landings_site_config';
	$table_customers_site_config=$prefixTbl.'customers_site_config';
	$table_landings_data_contents=$prefixTbl.'landings_data_contents';
	$table_landings_customers=$prefixTbl.'landings_customers';
	$table_front_actions=$prefixTbl.'log_web_actions'.date('Y');
	$table_sem_google_params=$prefixTbl.'sem_google_params';
	$table_seo_site_params=$prefixTbl.'seo_site_params';
	$table_landings_phone_number_per_sources=$prefixTbl.'landings_phone_number_per_sources';

	//Sección  PIxeles
	$table_pixels_pages = 'pixels_pages';
	$table_pixels_pages_dynamic_parameters = 'pixels_pages_dynamic_parameters';
	$table_pixels_dynamic_parameters = 'pixels_dynamic_parameters';
	$table_pixels_management = 'pixels_management';
	$table_pixels_management_pages = 'pixels_management_pages';
	$table_pixels_output_format = 'pixels_output_format';

	//$conexion=new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name,$debug_pagina,null,$port);
	$conexion=new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name,$debug_pagina,'utf8',$port);
	$conexion->connectDB();
	$cur_conn_id=$conexion->get_id_conexion();
	//$cur_conn_id=$conexion->obj->idConexion;
	$query='SELECT * FROM %s WHERE is_enabled';
	$query=sprintf($query,$table_customers_site_config);
	$tmpQuery=$query;
	//echo $query.'<hr><pre><hr>';print_r($conexion);die();
	$conexion->getResultSelectArray($query);
	$datosRespuesta=$conexion->tResultadoQuery;

	$isDataDb=false;
	$idLandingCr=(int)@$_REQUEST['cr'] ? (int)@$_REQUEST['cr'] : (int)@$_SESSION['idCreatividad'];

	/*
	 * start EXPERIMENTAL LOG ACCESS
	 */
	if (empty($_SESSION['sessionCountPromo']))
			$_SESSION['sessionCountPromo'] = 0;

	if(!empty($_SESSION['namePromo']) && !@$_SESSION['skipLogAction'])
	{
		$pagToLog=$_SERVER['PHP_SELF'];
		$pagToLog=substr($pagToLog,strrpos($pagToLog,'/')+1);
		$pagToLog=substr($pagToLog,0,strpos($pagToLog,'.'));
		$laAccion=@$_REQUEST['em'] ? $_REQUEST['em'] : '-';

		if($_SESSION['sessionCountPromo'] > 0)
		{
			$query='INSERT INTO %s (session_id,fecha,ip_usuario,script,accion,id_crea,action_number)VALUES(\'%s\',NOW(),\'%s\',\'%s\',\'%s\',%d,%d)';
			$query=sprintf($query,$table_front_actions,$_SESSION['namePromo'],$_SERVER['REMOTE_ADDR'],$pagToLog,$laAccion,$idLandingCr,$_SESSION['sessionCountPromo']);
			$conexion->ejecuta_query($query);
		}
	}
		$_SESSION['sessionCountPromo']++;
	/*
	 * end EXPERIMENTAL LOG ACCESS
	 */

	//if(count($datosRespuesta))	// LA LANDING ACTUAL ESTA CONFIGURADA EN LA BBDD
	{
	//echo $query.'<br><pre>';print_r($datosRespuesta);
		$idByDomain=null;
		$specificLandingConfig=array();
		$specificLandingData=array();
		$objSiteData=new stdClass();

		$arrSubDomains=array(
			3=>'servicios-dentales-milenium',
			6=>'servicios-dentales-milenium-lvb'
		);

		$firstUriLabelTmp=substr($firstUriLabel,0,-1);

		if($subDomain=in_array($firstUriLabelTmp, $arrSubDomains))
		{
			$resArrSearch=array_keys($arrSubDomains,$firstUriLabelTmp);
			$idByDomain=(int)$resArrSearch[0];
		}

		$isBBDDLanding=0;
		if(isset($_REQUEST['cr']))
		{
			$query='SELECT * FROM %s WHERE id=%d && is_enabled';
			$query=sprintf($query,$table_landings_site_config,(int)$_REQUEST['cr']);
			$conexion->getResultSelectArray($query);
			$specificLandingConfig=$conexion->tResultadoQuery;
			$specificLandingConfig=@$specificLandingConfig[0];
			$isBBDDLanding=$specificLandingConfig['id'];
		}

		if($idByDomain)
		{
			$query='SELECT * FROM %s WHERE id=%d && is_enabled';
			$query=sprintf($query,$table_landings_site_config,(int)$idByDomain);
			$conexion->getResultSelectArray($query);
			$specificLandingConfig=$conexion->tResultadoQuery;
			$specificLandingConfig=$specificLandingConfig[0];
			$isBBDDLanding=$specificLandingConfig['id'];
		}

		foreach($datosRespuesta as $key=>$value)
		{
			if(@$specificLandingConfig['id'] == @$isBBDDLanding)
			{
				$isDataDb=true;
				$query='SELECT * FROM %s WHERE site_landing_id=%d && is_enabled';
				$query=sprintf($query,$table_landings_data_contents,(int)$specificLandingConfig['id']);
				$conexion->getResultSelectArray($query);
				$specificLandingData=$conexion->tResultadoQuery;
				$specificLandingData=@$specificLandingData[0];
				$objSiteData->path_raiz_includes_local=$specificLandingConfig['url_local'] ? $specificLandingConfig['url_local'] : $path_raiz_includes.$datosRespuesta[$key]['root_local_includes_path'];
				$objSiteData->landingName=$specificLandingConfig['landing_identifier'] ? $specificLandingConfig['landing_identifier'] : $datosRespuesta[$key]['landing_identifier'];
				$objSiteData->customerId=$datosRespuesta[$key]['client_id'];
				$objSiteData->landingId=$specificLandingConfig['id'];
				$objSiteData->webTitle=@$specificLandingConfig['txt_title_site'] ? $specificLandingConfig['txt_title_site'] : $datosRespuesta[$key]['txt_title_site'];
				$objSiteData->bodyScript=@$specificLandingConfig['script_body'] ? $specificLandingConfig['script_body'] : 'body_common.php';
				$objSiteData->landingTermsFile=@$specificLandingConfig['landing_terms_file'] ? $specificLandingConfig['landing_terms_file'] : 'landing_terms.php';
				$objSiteData->landingPrivacyPolicyFile=@$specificLandingConfig['privacy_policy_file'] ? $specificLandingConfig['privacy_policy_file'] : '';
				$objSiteData->footerCookiesCssFile=@$specificLandingConfig['footer_cookies_css'];
				$objSiteData->skipCheckPhone=@$specificLandingConfig['skip_check_phone_number'] ? true : false;

				$objSiteData->headerTopContent=@$specificLandingData['header_top_content'];
				$objSiteData->boxTopLeft=$specificLandingData['box_top_left'];
				$objSiteData->boxTopLeft=$specificLandingData['box_top_left'];
				$objSiteData->boxBottomForm=$specificLandingData['box_bottom_form'];
				$objSiteData->boxFooterText=$specificLandingData['box_footer_text'];
				$objSiteData->scriptFormName=$specificLandingData['script_form_name'];
				$objSiteData->mobileAutoModal=$specificLandingData['mobile_auto_modal'];
				$objSiteData->prefixFolders=str_pad((@$specificLandingConfig['id'] ? @$specificLandingConfig['id'] : @$_REQUEST['id_crea']),3,'0',STR_PAD_LEFT);	// M.F. 20141014
	//echo'...'. $objSiteData->prefixFolders;
				$query='SELECT * FROM %s WHERE id_landing=%d';	// GOOGLE PARAMS
				$query=sprintf($query,$table_sem_google_params,(int)$specificLandingConfig['id']);
				$conexion->getResultSelectArray($query);
				$semGoogleParams=$conexion->tResultadoQuery;
				$objSiteData->semGoogleParams=@$semGoogleParams[0];

				$query='SELECT * FROM %s WHERE id_landing=%d && is_enabled';	// SEO DATA
				$query=sprintf($query,$table_seo_site_params,(int)$specificLandingConfig['id']);
				$conexion->getResultSelectArray($query);
				$seoParams=$conexion->tResultadoQuery;
				$objSiteData->SeoSiteParams=@$seoParams[0];

				$query='SELECT * FROM %s WHERE id_landing=%d && is_enabled';	// SEO DATA
				$query=sprintf($query,$table_landings_phone_number_per_sources,(int)$specificLandingConfig['id']);
				$conexion->getResultSelectArray($query);
				$phoneNumbersPerSources=$conexion->tResultadoQuery;
				$objSiteData->phoneNumbersPerSources=@$phoneNumbersPerSources[0];
				break;
			}
		}
	}
	// END MODIFICACIONES PARA CONFIGURAR SITE DESDE BBDD (M.F. 04.2014)
}

//***************************************************************************************************************************
////////////////COMMON PLATTFORM TYPE. Needed to check emails DNS////////////////
//***************************************************************************************************************************
$platform_type_email 				= 'WINEMAILDNS';											//Windows plattform parameter

//***************************************************************************************************************************
///////////////COMMON EMAIL SETTINGS (all the params would be overwritten for each case inside the respective php file (*)/////////////
//***************************************************************************************************************************
$email_smtp_server       		= 'localhost';       										// SMTP SERVER 192.168.3.210 (*)
$email_pop3_server        		= '';           												// POP3 SERVER (*)
$email_smtp_authentication    	= 'false';          											//If authentication is needed by smtp server (*)
//$email_from_smtp         		= 'manuel.fonseca@arnold4d.es';									//'manuel.fonseca@arnold4d.es';      //email address from  (*)
$email_from_smtp         		= 'abanderado@ganalacarreraalfrio.com';
$email_from_name        		= 'Abanderado'; 													//FROM ALIAS (*)
$email_subject_regalo			= '¡Enhorabuena! Has sido premiado';
$email_subject_confirmar_emai	= 'Descubre si has ganado unas gafas de sol';
$email_subject_confirmar_emai_2	= 'Descubre si has ganado un cheque carburante';
$email_subject_contacto			= 'GanalaCarreraAlFrio: Nuevo Contacto';
$email_to_contacto				= 'abanderado.spain@dbaeu.com';
//$email_to_contacto				= 'abanderado@ganalacarreraalfrio.com';

//***************************************************************************************************************************
//////////////ADMIN SETTINGS (PATHS,VARS,...) (*) ////////////////
//***************************************************************************************************************************
$path_raiz_aplicacion_admin = $path_raiz_aplicacion.'admin/';
$admintitle = 'Dujok -- Admin Masmovil';																	//Admin Site Title (pages title) (*)
$admincorporate='Dujok Brownies Management';																//Nombre de la empresa que ha hecho el admin (*)
$dir_raiz_aplicacion_admin = $dir_raiz_aplicacion. 'admin/'; 									//DIRECTORIO ROOT DEL ADMIN DEL SITE
///$path_raiz_aplicacion_admin			= '/'.$dir_raiz_aplicacion_admin;									//UTILIZADO PARA INCLUIR LOS FICHEROS CLIENTE DEL BACK (js,css,img)
$path_control_acceso_admin = '//'.$_SERVER['HTTP_HOST'].trim($path_raiz_aplicacion_admin);//CONTROL DE ACCESO AL ADMIN
define('TIME_COOKIES_ADMIN',3600);

/**
 * IpSearchEngine
 * WS que dada la IP del usuario recoge datos.
 * Parámetros por defecto
 */
$ws_ip_url = 'http://212.71.235.216/webservices/ipSearchEngine/ip.php';

$ws_ip_usr = 'adeslasIpCheck'; //Por defecto ponemos Adeslas
$ws_ip_pwd = '5efdf1d7d65df54330ac233e5097ac48'; //Por defecto ponemos Adeslas
$ws_ip_arr_parameters = array('usr'=>$ws_ip_usr,'pwd'=>$ws_ip_pwd,'ip'=>'');
//echo 'conf/config_web.php ---->'.date('H:i:s');die();
