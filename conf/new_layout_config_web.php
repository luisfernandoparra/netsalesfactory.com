<?php
///###################################################################################################################################################################
///==================================================================================================================================================================
/// CONFIGURACIONES COMUMES (ADMIN / FRONT ) NOTA: las variables marcadas con (*) son las que hay que cambiar en funcion del proyecto
///==================================================================================================================================================================
///###################################################################################################################################################################
//***************************************************************************************************************************
//////////////COMMON SETTINGS (PATHS,VARS,...)  (*)////////////////
//***************************************************************************************************************************
$sitesPruebas=($_SERVER['SERVER_ADDR'] == 'localhost' || $_SERVER['SERVER_ADDR'] == '127.0.0.1'|| $_SERVER['SERVER_ADDR'] == '139.162.246.12');	// SITIOS PARA PRUEBAS
$debugModeNew=(int)@$_REQUEST['debugModeNew'];
$displayErrors=0;
$debugDujok=null;

if($sitesPruebas)
{
  $fromServer = 'http://'.$_SERVER['HTTP_HOST'];
  $web_url = $fromServer.'/netsalesfactory.com/promociones/jazztel/';
  $dir_raiz_aplicacion = 'netsalesfactory.com/promociones';
  $path_raiz_aplicacion = '/'.$dir_raiz_aplicacion;
  $webtitle = 'DEMO-PROMO';	//Site Title (pages title) (*)
  $displayErrors = true;
  $debugDujok=true;

	// START DDBB COMPARE URL
	$thisUrl=parse_url($_SERVER['REQUEST_URI']);
	$firstUriLabel=substr($thisUrl['path'],0,-1);
	$posLastBackSlash=strripos($firstUriLabel,'/');
	$firstUriLabel=substr($thisUrl['path'],$posLastBackSlash+1);
	$uriParams=$thisUrl['query'];	// PARAMETROS DE LA URL
	// END DDBB COMPARE URL
  $path_raiz_includes 	=$_SERVER['DOCUMENT_ROOT'].trim($path_raiz_aplicacion).'/';		//UTILIZADO PARA INCLUIR LOS FICHEROS SERVIDOR DEL FRONT .php
  $path_imagenes      	='http://'.$_SERVER['HTTP_HOST'].$path_raiz_aplicacion.'emails/img/';	//usado para envio de emails con imagenes

  $dir_raiz_ws  ='../../';	//DIRECTORIO ROOT DEL SITE (*)
  $path_raiz_ws ='/'.$dir_raiz_ws; 
  $file_ws	='router.php';
  $url_ws = 'http://212.71.235.216/webservices/clickToCall/';
  $url_repository_files = '??????????/extraccion/files/';
  $db_user='user_connect';
  $db_pass='L1nK4me@!';
  $db_host='127.0.0.1';
  $db_name='netsalesfactory';
  $db_type='MySQL';
  $dir_raiz_aplicacion 			= 'http://'.$_SERVER['HTTP_HOST'].'/'.$dir_raiz_aplicacion.'/'; // PARCHE 28.01.2014 M.F.
	$path_raiz_aplicacion.='/';
}
else
{
  $web_url 						= 'http://www.netsalesfactory.com/acciones/masmovil/'; 		//Web Site URL (*)
  $dir_raiz_aplicacion 			= '';
  $path_raiz_aplicacion 		= '/'.$dir_raiz_aplicacion;
  $webtitle 					= 'Bienvenido'; 				//Site Title (pages title) (*)
  $path_raiz_includes 	=$_SERVER['DOCUMENT_ROOT'].trim($path_raiz_aplicacion);			//UTILIZADO PARA INCLUIR LOS FICHEROS SERVIDOR DEL FRONT .php
  $path_imagenes      	='http://'.$_SERVER['HTTP_HOST'].$path_raiz_aplicacion.'emails/img/';	//usado para envio de emails con imagenes

  $dir_raiz_ws  = 'netsalesfactory.com/acciones/webservice/'; 								//DIRECTORIO ROOT DEL SITE (*)
  $path_raiz_ws = '/'.$dir_raiz_ws; 
  $file_ws = 'router.php';
  $url_ws = 'http://212.71.235.216/webservices/clickToCall/';
  $url_repository_files = $path_raiz_includes_admin.'extraccion/files/';
  $db_user='netuserdb';
  $db_pass='netUser1102';
  $db_host='127.0.0.1';
  $db_name='netsalesfactory';
  $db_type='MySQL';
}

//**********************************************************************************
///////////////COMMOM FILE VERSIONS (PRODUCCION / DESARROLLO)///////////////
//**********************************************************************************
define('JS_MINIFIED_SUFFIX','.min');  //PARA ENTORNOS DE PRODUCCION, DEBEMOS MINIMIZAR LOS JS PARA AUMENTAR EL RENDIMIENTO AL CARGAR, por tanto en el mismo directorio, tendremos los ficheros de desarrollo y los minimizados cCON ESTE SUFIJO que seran los que se suban a produccion 
define('CSS_MINIFIED_SUFFIX','.min'); //PARA ENTORNOS DE PRODUCCION, DEBEMOS MINIMIZAR LOS CSS PARA AUMENTAR EL RENDIMIENTO AL CARGAR, por tanto en el mismo directorio, tendremos los ficheros de desarrollo y los minimizados cCON ESTE SUFIJO que seran los que se suban a produccion 
define('JSCSS_SHOWTIME','1');         //AÑADIMOS A LOS CSS y JS LA FECHA DE ULTIMA MODIFICACION, POR SI TENEMOS ENTORNO CACHEADOS PARA QUE SE VUELVA A PEDIR AL SERVIDOR EL FICHERO y evitar que el usuario no vea el cambio

//***************************************************************************************************************************
//////////////COMMON DEBUG SETTINGS////////////////
//***************************************************************************************************************************
//FOR SHOW DEBUGGING QUERIES (VIA IP IF YOU WANT) AND TRACKING VIA EMAIL
define('SHOW_DEBUG','N');										//values: Y/N //activate / deactivate (ADMIN, FRONT) FOR ALL PEOPLE (*)
$ips_show_debug = array('62.97.72.29');			//'62.97.72.29' RANGO DE IPS QUE PODRAN VER EL DEBUG CUANDO ESTE SE ENCUENTRE EN 'N'. UTIL PARA PONER DEBUG EN PRODUCCION (*)
define('QUERY_SEND_EMAIL_TRACKING','1');		//0: no se envia email, 1 si; (*)
define('QUERY_EMAIL_TRACKING_TO','luisfer.parra@dujok.com');	//email al programador o programadores en caso de error en una query (*)
//FOR DEBUGGING POST AJAXS CALLS
$a_vars = $_POST;
$chk_var_control = (isset($_REQUEST['debugmode'])) ? trim($_REQUEST['debugmode']) : 0; 
if($chk_var_control == 1){$a_vars = $_REQUEST;}

//***************************************************************************************************************************
//////////////BBDD TABLES,STORED PROCEDURES ETC NAMES (*)////////////////
//***************************************************************************************************************************

$table_registros	  = 'registros';				//nombre de la tabla con los usuarios con acceso al admin
$table_provincias	  = 'provincias';
$table_countries	  = 'countries';
$table_sources		  = 'sources';
//Tablas Administrador
$table_admin_usuarios = 'adminusers';

//sql iNJECTION
$arr_sql_inject_original = array('select','insert','drop','create','execute','union','like','truncate','delete','%');
$arr_sql_inject_cambio = array('s3l3ct','1ns4rt','dr0p','cr34te','3x3cut3','4un10n','1ik3','t4unc4t3','d31et3',' ');

//Para generación de CSVs
$sep=';';
$eol="\r\n";
$encryptKeyRegistro='484nD34D0';

//INCLUDES NECESARIOS TANTO PARA DEBUG COMO PARA LA QUERIES
///echo @$debugDujok ? $path_raiz_includes : '';
require_once($path_raiz_includes.'class/class.debug.php');
$debug_pagina = new CDEBUG();
require_once($path_raiz_includes.'class/class.bbdd.php');
require_once($path_raiz_includes.'class/class.dumpvar.php');
//FUNCIONES COMUNES TANTO AL FRONT COMO AL ADMIN
require_once($path_raiz_includes.'includes/funciones_comunes.php');

// START MODIFICACIONES PARA CONFIGURAR SITE DESDE BBDD (M.F. 04.2014)

$prefixTbl='nl_';
$table_landings_site_config=$prefixTbl.'landings_site_config';
$table_customers_site_config=$prefixTbl.'customers_site_config';
/*
//Sql Injection
$arr_sql_inject_original = array('select','insert','drop','create','execute','union all','union','like','truncate','delete','concat','servers','mysql','order by','host.','unhex','authentication','--','test.','show tables','response','write','/etc','md5','acunetix_wvs_security_test','{');
$arr_sql_inject_cambio = array('s3l3ct','1ns4rt','dr0p','cr34te','3x3cut3','4un10n a11','4un10n','1ik3','t4unc4t3','d31et3','c0nc4t','s3rv3rs','m4sq1','ord3r b4','h0st.','4nh3x','4unth3nticat10n','','t3st.','chow t4bl3s','r3spons3','wr1t3','3tc','m45','acun3t1x_wvs_s3curity_t3st','(');
*/

$conexion=new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name,$debug_pagina);
$conexion->connectDB();
$cur_conn_id=$conexion->get_id_conexion();
$query='SELECT * FROM %s WHERE is_enabled';
$query=sprintf($query,$table_customers_site_config);
$conexion->getResultSelectArray($query);
$datosRespuesta=$conexion->tResultadoQuery;
$isDataDb=false;

if(count($datosRespuesta))	// LA LANDING ACTUAL ESTÁ CONFIGURADA EN LA BBDD
{
	$specificLandingData=array();
	$objSiteData=new stdClass();
	if(isset($_REQUEST['cr']))
	{
		$query='SELECT * FROM %s WHERE id=%d && is_enabled';
		$query=sprintf($query,$table_landings_site_config,(int)$_REQUEST['cr']);
		$conexion->getResultSelectArray($query);
		$specificLandingData=$conexion->tResultadoQuery;
		$specificLandingData=$specificLandingData[0];
	}

	foreach($datosRespuesta as $key=>$value)
	{
		if(strrpos($firstUriLabel, $datosRespuesta[$key]['root_local_path']) > -1)
		{
			$isDataDb=true;
			$objSiteData->dir_raiz_aplicacion_local=$dir_raiz_aplicacion.$datosRespuesta[$key]['root_local_path'];
			$objSiteData->path_raiz_includes_local=$path_raiz_includes.$datosRespuesta[$key]['root_local_includes_path'];
			$objSiteData->landingName=$specificLandingData['landing_identifier'] ? $specificLandingData['landing_identifier'] : $datosRespuesta[$key]['landing_identifier'];
			$objSiteData->customerId=$datosRespuesta[$key]['client_id'];
			$objSiteData->landingId=$specificLandingData['id'];
			$objSiteData->webTitle=$specificLandingData['txt_title_site'] ? $specificLandingData['txt_title_site']:  $datosRespuesta[$key]['txt_title_site'];
			$objSiteData->bodyScript=$specificLandingData['script_body'] ? $specificLandingData['script_body'] : 'body_common.php';
			$objSiteData->landingTermsFile=$specificLandingData['landing_terms_file'] ? $specificLandingData['landing_terms_file'] : 'landing_terms.php';
			$objSiteData->landingPrivacyPolicyFile=$specificLandingData['privacy_policy_file'] ? $specificLandingData['privacy_policy_file'] : 'privacy_policy.php';
			$objSiteData->footerCookiesCssFile=$specificLandingData['footer_cookies_css'];
//			$objSiteData->landingPrivacyPolicyFile=$specificLandingData['root_local_path'];
//echo '<h1>se debe distinguir entre una determinada promocion entre varias de un mismo cliente!</h1><pre>';print_r($datosRespuesta[$key]);
		}
	}

	if($isDataDb)
	{
//echo $db_name.'<hr>'.$thisUrl['path'].'<pre>';print_r($objSiteData);die();
	}
}

// END MODIFICACIONES PARA CONFIGURAR SITE DESDE BBDD (M.F. 04.2014)


//DIRECTORIO ROOT DEL SITE (*)
//***************************************************************************************************************************
////////////////COMMON PLATTFORM TYPE. Needed to check emails DNS////////////////
//***************************************************************************************************************************
$platform_type_email 				= 'WINEMAILDNS';											//Windows plattform parameter

//***************************************************************************************************************************
///////////////COMMON EMAIL SETTINGS (all the params would be overwritten for each case inside the respective php file (*)/////////////
//***************************************************************************************************************************
$email_smtp_server       		= 'localhost';       										// SMTP SERVER 192.168.3.210 (*)
$email_pop3_server        		= '';           												// POP3 SERVER (*)
$email_smtp_authentication    	= 'false';          											//If authentication is needed by smtp server (*)
//$email_from_smtp         		= 'manuel.fonseca@arnold4d.es';									//'manuel.fonseca@arnold4d.es';      //email address from  (*)
$email_from_smtp         		= 'abanderado@ganalacarreraalfrio.com';	
$email_from_name        		= 'Abanderado'; 													//FROM ALIAS (*)
$email_subject_regalo			= '¡Enhorabuena! Has sido premiado';
$email_subject_confirmar_emai	= 'Descubre si has ganado unas gafas de sol';
$email_subject_confirmar_emai_2	= 'Descubre si has ganado un cheque carburante';
$email_subject_contacto			= 'GanalaCarreraAlFrio: Nuevo Contacto';
$email_to_contacto				= 'abanderado.spain@dbaeu.com';
//$email_to_contacto				= 'abanderado@ganalacarreraalfrio.com';

//***************************************************************************************************************************
//////////////ADMIN SETTINGS (PATHS,VARS,...) (*) ////////////////
//***************************************************************************************************************************
$admintitle = 'Dujok -- Admin Masmovil';																	//Admin Site Title (pages title) (*)
$admincorporate='Dujok Brownies Management';																//Nombre de la empresa que ha hecho el admin (*)													
$dir_raiz_aplicacion_admin  		= $dir_raiz_aplicacion. 'admin/'; 									//DIRECTORIO ROOT DEL ADMIN DEL SITE
$path_raiz_aplicacion_admin			= '/'.$dir_raiz_aplicacion_admin;									//UTILIZADO PARA INCLUIR LOS FICHEROS CLIENTE DEL BACK (js,css,img)
$path_raiz_includes_admin			= $_SERVER['DOCUMENT_ROOT'].trim($path_raiz_aplicacion_admin);	 	//UTILIZADO PARA INCLUIR LOS FICHEROS SERVIDOR .php	
$path_control_acceso_admin  		= 'http://'.$_SERVER['HTTP_HOST'].trim($path_raiz_aplicacion_admin);//CONTROL DE ACCESO AL ADMIN
define('TIME_COOKIES_ADMIN',3600);	

																				
///###################################################################################################################################################################
///==================================================================================================================================================================
/// CONFIGURACIONES FRONT  NOTA: las variables marcadas con (*) son las que hay que cambiar en funcion del proyecto
///==================================================================================================================================================================
///###################################################################################################################################################################

//***************************************************************************************************************************
//////////////FRONT SETTINGS (PATHS,VARS....) ////////////////
//***************************************************************************************************************************
//if($sitesPruebas)
//{
//  $path_raiz_includes 	=$_SERVER['DOCUMENT_ROOT'].trim($path_raiz_aplicacion).'/';		//UTILIZADO PARA INCLUIR LOS FICHEROS SERVIDOR DEL FRONT .php
//  $path_imagenes      	='http://'.$_SERVER['HTTP_HOST'].$path_raiz_aplicacion.'emails/img/';	//usado para envio de emails con imagenes
//}
//else
//{
//  $path_raiz_includes 	=$_SERVER['DOCUMENT_ROOT'].trim($path_raiz_aplicacion);			//UTILIZADO PARA INCLUIR LOS FICHEROS SERVIDOR DEL FRONT .php
//  $path_imagenes      	='http://'.$_SERVER['HTTP_HOST'].$path_raiz_aplicacion.'emails/img/';	//usado para envio de emails con imagenes
//}

/* CLAVE PARA ENCRIPTADO MD5 EN EL REGISTRO */
//$encryptKeyRegistro				= '484nD34D0';
$path_raiz_aplicacion_admin = $path_raiz_aplicacion.'admin/'; 
$path_raiz_includes_admin	= $path_raiz_includes.'admin/';

if($debugModeNew)
{
	$debugDujok=true;
//echo $query.'<pre>';print_r($conexion);
}

?>