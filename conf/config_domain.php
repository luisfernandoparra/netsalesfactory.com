<?php
/**
 * BASE DOMAIN CONFIGURATION DATA
 *
 */
@header('Content-Type: text/html; charset=utf-8');
@session_start();
$siteSetting=new stdClass();
$siteSetting->arrFrontQueries=array();

// MANDATORY PARAMS
$siteSetting->isDevSession=$sitesPruebas;
$siteSetting->ddbbLogs=$bbdd_logs;
//$siteSetting->ddbbCommunityLogs=$bbdd_community_logs;	// TABLA LOGS: 'comm_log_front_web_'.date('Y')
//$siteSetting->tables->leads='leads';

$siteSetting->prefixTbl=__QUERY_TABLES_PREFIX;
$siteSetting->NumberLoginAttempts=5;	// NUMERO DE INTENTOS MAXIMOS PARA EL LOGIN DEL LEAD ACTUAL

$siteSetting->numBlocksHistoryReviews=2;	// NUMERO DE ELEMENTOS DEL HISTORICO DE OPINIONES A MOSTRAR CON CADA CARGA DE NUEVOS RATINGS
$siteSetting->maxRatingPerPage=4;	// NUMERO DE ELEMENTOS DEL HISTORICO MOSTRADOS AL CARGAR LA PAGINA

// FORZADO A SPANISH (209) MIENTRAS NO HAYA OTRA COSA......
$siteSetting->months[209]=[1=>'enero', 2=>'febrero', 3=>'marzo', 4=>'abril', 5=>'mayo', 6=>'junio', 7=>'julio', 8=>'agosto', 9=>'septiembre', 10=>'octubre', 11=>'noviembre', 12=>'diciembre'];
