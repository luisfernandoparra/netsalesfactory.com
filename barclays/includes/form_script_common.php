<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
//$drawInbound_phone='900806449';
//$drawInbound_phone='';

if($drawInbound_phone)	// SI EXISTE, SE DIBUJA EL TAG PARA PODER LLAMAR DESDE EL DISPOSITIVO MOVIL HACIENDO UNICAMENTE CLICK
{
	$tagInboundPhoneStart='<a class="telf" href="tel:'.$inbound_phone.'" style="text-decoration:none;">';
	$tagInboundPhoneEnd='</a>';
}

?>
<style>
.innerModal{
box-sizing:border-box;
margin:0;
padding:0;
padding-top:6%;
position:relative;
line-height:22px!important;
font:12px Arial,Helvetica,sans-serif;
}

</style>
<?=$tagInboundPhoneStart?>
					<img src="img/028/llamanos.jpg" border="0"/>
					<p style="text-align:center;">O te llamamos sin compromiso</p><br/>
<?=$tagInboundPhoneEnd?>

					<div id="campos" class="mainForm hide">
						<div class="error"><ul></ul></div>

						<div class="fila ">
							<div class="fleft">
								<p>Nombre y apellidos</p>
								<input type="text" name="nombre" maxlength="100" id="nombre" value="<?=$nombre?>" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" placeholder="" />
							</div>
						</div>

						<div class="fila ">
							<div class="fleft">
								<p>E-mail</p>
								<input type="email" name="email" maxlength="100" id="email" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="" />
							</div>
						</div>

						<div class="fila ">
							<div class="fleft">
								<p>Tel&eacute;fono</p>
								<input type="tel" maxlength="9" name="telefono" id="telefono" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="" />
							</div>
						</div>

						<div class="clearfix"></div>

						<div class="" style="padding-top:10px;">
							<div class="legal">
									<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />
								He leído y acepto la <a href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php';?>" target="_blank">política de privacidad</a>
							</div><!--/12-->
						</div>

							<div class="espacio_btn">
								<input class="sendData" type="button" id="btnProcesar" name="btnProcesar" value="Solicitar información" data-direction="down" />
							</div>
						<div class="clearfix"></div>
					</div>
					<!--/#campos-->
