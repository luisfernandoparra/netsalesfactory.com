<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$arr_creas[$id_crea]['title']?></title>
<?php
//	ONLY FOR MOBILE DEVICES
//if($is_movil == 1 || $layoutType)
{
?>
<meta name="HandheldFriendly" content="true">
<!--<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">-->
<meta name="viewport" content="width=device-width,initial-scale=1"/> 
<meta http-equiv="Pragma" content="no-cache">
<?php
}
if(count(@$objSiteData->SeoSiteParams))
{
	if(@$objSiteData->SeoSiteParams['meta_canonical'])
		echo '<link rel="canonical" href="'.$objSiteData->SeoSiteParams['meta_canonical'].'" />
';
//if($debugModeNew)echo'<pre>';print_r($objSiteData->SeoSiteParams);echo'</pre>';
	
}
// NOTA: SE HA INTENCIONALMENTE COLOCADO UNA VARIABLE CON TRES DIGITOS (PRECEDIDOS DE CEROS) PARA EL CSS PRINCIPAL
$defaultCSS='000';
$localLandingCssFile='css/'.$prefixCss.'css_'.str_pad($id_crea,3,'0',STR_PAD_LEFT).'.css';
$localLandingCssFile=(file_exists($path_raiz_includes_local.$localLandingCssFile)) ? $path_raiz_aplicacion_local.$localLandingCssFile : $path_raiz_aplicacion_local.'css/'.$prefixCss.'css_'.$defaultCSS.'.css';

?>
<meta name="description" content="Néctar Plan Dental Familia. La salud dental para parejas por menos de 0,45€ al día">
<base href="<?=$path_raiz_aplicacion_local?>" />
<link href="<?=$path_raiz_aplicacion?>js/modal/css/modal.css" rel="stylesheet" type="text/css" />
<link href="<?=$path_raiz_aplicacion?>js/facebox/facebox.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$path_raiz_aplicacion_local?>css/common.css" rel="stylesheet" type="text/css" />
<link href="<?=$localLandingCssFile?>" rel="stylesheet" type="text/css" />

<script type="text/javascript">
var root_path = "<?=$path_raiz_aplicacion;?>";<?php /*ruta global para las imgs del facebox y del modal*/?>
var root_path_local = "<?=$path_raiz_aplicacion_local;?>";<?php /*ruta global para ajax*/?>
var nomb_promo = "<?=$arr_creas[$id_crea]['nombpromo']?>";
var id_source = "<?=(int)$id_source?>";
var id_client = "<?=$id_client?>";
var thankyoupage = "<?=$url_thankyouPage?>";

var arr_campanas = [<?php  echo $campanas?>];
var is_ctc = "<?=trim($is_ctc)?>";
var is_movil = "<?=trim($is_movil)?>";
var root_path_ws = "<?=$path_raiz_ws;?>";
var url_ws = "<?=$url_ws;?>";
var file_ws = "<?=$file_ws;?>";
var arr_pixel_JS = ["<?=$arr_pixel_JS[0];?>","<?=$arr_pixel_JS[1]?>","<?=$arr_pixel_JS[2]?>"];
var tduid = "<?=$tduid?>";
var trdbl_organization = "<?=$organization?>"; //Pixel Tradedoubler. Organizacion
var trbdl_event = "<?=$event?>";
var trbdl_program = "<?php echo $trbdl_program; ?>";
var nombCliente = "<?=$nombCliente?>";
var is_prov = "<?=trim($is_prov)?>";
var refC = "<?=(int)$id_crea?>";

var googleSEM = "";
var googleSEO = 0;
var google_conversion_id;
</script>
<?php
$blockBodySEM='';
/*
 * SI LA PROMO TIENE DATOS PARA SEM (Search Engine Marketing)
 * se rellena el array correspondiente para construir
 * el pixel correspondiente (M.F. 2014.06.26)
 */
//echo '<pre>';print_r($objSiteData->semGoogleParams);echo '</pre>';
if(count($objSiteData->semGoogleParams))
{
	if(@$objSiteData->semGoogleParams['google_ga'])	// SOLO PARA el `seo`  DE LAS LANDINGS SEM QUE TENGAN CONFIGURADO EL `id GA`
	{
		echo '
<script type="text/javascript">
var googleSEO = "'.$objSiteData->semGoogleParams['google_ga'].'";
</script>
';
	}

	$outDem='//www.googleadservices.com/pagead/conversion/';
	$outJs='
/* <![CDATA[ */
';
	$outJsAcceptRegister='';

	foreach($objSiteData->semGoogleParams as $key=>$value)
	{
		switch($key)
		{
			case 'conversion_id';
				$outDem.=$value.'/';
				$bodyJsGoogle[$key]=$value;
				$outJs.='var google_'.$key.'='.$value.';
';
				break;
			case 'remarketing_only';
				$outDem.=$value.'/';
				$bodyJsGoogle[$key]=$value ? 'true' : 'false';
				$outJs.='var google_'.$key.'=true;
';
				echo '<script>var xxx_'.$key.'=true;</script>';
				break;
			case 'conversion_label';
				$bodyJsGoogle[$key]=$value;
				$outDem.='?label='.$value.'&amp;guid=ON&amp;script=0';
				break;
			case 'id_sem';
			case 'id_landing';
				break;
			default:
				$bodyJsGoogle[$key]=$value;
		}
	}
	$outJs.='var google_custom_params=window.google_tag_params;
';
	$outJs.='
/* ]]> */
';

	$blockBodySEM='
<script type="text/javascript">
'.$outJs.$outJsAcceptRegister.'googleSEM="'.$outDem.'";

$(document).ready(function(){
});

	function googleAdWords()
	{
		/* <![CDATA[ /
		var google_conversion_id = '.$bodyJsGoogle['conversion_id'].';
		var google_conversion_language = "'.$bodyJsGoogle['conversion_language'].'";
		var google_conversion_format = "'.$bodyJsGoogle['conversion_format'].'";
		var google_conversion_color = "'.$bodyJsGoogle['conversion_color'].'";
		var google_conversion_label = "'.$bodyJsGoogle['conversion_label'].'";
		var google_remarketing_only = '.$bodyJsGoogle['remarketing_only'].';
		/ ]]> */
		includeScript("//www.googleadservices.com/pagead/conversion.js","js");
	}

</script>
';
}
?>

<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/modal/js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/facebox/facebox.min.js"></script>
<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/funciones_comunes.js"></script>

<script type="text/javascript">
var validator;


$(document).ready(function(){
	$("#telefono").val("");

	$("#enviarPorMailSolicitaInfo").validate({
		rules:{
			telefono:{
				required:true,
				remote:{
					url:root_path_local +"includes/phone_check.php",
			    contentType:"application/json; charset=utf-8",  
					data:{cr:<?=(int)$id_crea?>,hashPipe:"<?=$controlChekAlgorithm?>"},
					dataFilter:function(response){
						var jsonResponse="";
						jsonResponse=JSON.parse(response);
							if(jsonResponse.success == false)
							{
								return "\"" + jsonResponse.errorMessage + "\"";
							}else{
								return true;
							}
						return false;
					}
				}
			}
		},
		errorLabelContainer: $("#enviarPorMailSolicitaInfo div.error")
	});


	validator=$("#enviarPorMailSolicitaInfo").validate({
		errorContainer:$('div.error'),
		errorLabelContainer:$('div.error ul'),
		wrapper: 'li'
	})

$(".effLeft").addClass("container3d-ready");
	$(".mainForm").delay(0).slideDown(0,function(){
		if($("body").width() > 683)
		{
			$(".campo_secundario").each(function (i, item){
				$(this).removeClass("campo_secundario");
			});
		}

		$(".fila:not('.campo_secundario')").each(function (i, item){
			$(".boxShadow").css("opacity",(1*(i+1)));//.css("-webkit-box-shadow","0 0 1px 0 #333").css("box-shadow","0 0 1px 0 #333")
		});
		$("#pie_galletero").fadeIn("slow",function(){
		});

	});

});
</script>

<?php
if(!empty($arr_creas[$id_crea]['js']) && trim($arr_creas[$id_crea]['js']) != '' )
{
?>
	<script type="text/javascript" src="<?=$path_raiz_aplicacion_local?>js/<?=trim($arr_creas[$id_crea]['js'])?>"></script>
<?php

}else{
?>
	<script type="text/javascript" src="<?=$path_raiz_aplicacion_local?>js/validacion.js"></script>
<?php
}

if($layoutType && $arr_creas[$id_crea]['mobile_auto_modal_on_open'])  // GESTION MODAL "LLAMAME"
{
  echo '
<style>
.modalCllame{
display:block;
width:100%;
left:0px!important;
color:#009CD9;
}

.closeModalCall{
color:#000;
text-decoration:none;
}
.callMeText{
font-size:150%;
font-weight:bold;
}
.modalCloseImg{display:none!important;}

</style>';
}
?>
<style type="text/css">
label{display:block;text-align:left;color:#fff;font-weight:normal;padding-top:0px;margin-bottom:5px;}
/*estilos mensajes error*/
div.error1{display : none; border:2px solid #D81E05; }
div.error1{margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
div.error{ display : none; border:2px solid #D81E05; }
.error{color:#D81E05; background:#FCF1F0;}
div.error ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error ul li{margin:0 0 10 0;list-style-type:none;}
div.error ul li label{font-weight:normal}
div.error, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error, textarea.error{border:2px solid #D81E05;background-color:#FCF1F0;}
input.ok{background-color:#EFFFDA}

.innerModal{
display:inline-block;
background:#fff;
text-align:center;
width:100%;
height:250px;
}

.modal{
height:680px!important;
}

#simplemodal-container a.modalCloseImg{background:url(<?=$path_raiz_aplicacion_local;?>img/close_modal.png)no-repeat 2px 0px rgba(0, 0, 0, 0)!important;right:-8px;top:-14px;}
#facebox img{width:32px!important;height:1px!important;text-align:center;}
</style>
<script type="text/javascript">
var isModalDraw=false;
var leadNumber = Math.round((new Date().getTime() * Math.random()));
function offModal(){$.modal.close();} // CLOSE MODAL "LLAMAR"

$(document).ready(function(){
	$(".innerModal").css("height",(document.body.clientHeight)+"px");
	//$("#informacion").delay(500).slideDown("slow");

<?php
// START GESTION MODAL "LLAMAME" (ONLY IF MOBILE IS ENABLED)
if($layoutType && $arr_creas[$id_crea]['mobile_auto_modal_on_open'])
{
  // PARA EVITAR ENVIAR LA LLAMADA AL SERVIDOR DE tradedoubler EN PRUEBAS
  $serverTradedoubler=$sitesPruebas ? 'http://139.162.246.12/netsalesfactory.com/promociones/sanitas-new-web/img/p.gif?' : 'http://tbl.tradedoubler.com/report?organization=1782746&event=307764&leadNumber=';
?>

  $("body").click(function(){

	if(!isModalDraw)  // CLICK ONLY ONCE
	{
	  $.modal('<div style="display:inline-block;background:#fff;text-align:center;width:300px;height:200px;"><center><br /><span class="callMeText">¡Inf&oacute;rmate AHORA!<br />Llama GRATIS</span><br /><br /><a id="callMeHref" href="tel:900834152"><img class="info callProceed" src="<?=$path_raiz_aplicacion_local?>img/btn_call_me.png" border="0" style="width:70%;" /></a><span style="display:block;margin-top:8px;margin-bottom:4px;"><center><br /><a class="closeModalCall" href="javascript:offModal();">cerrar <img align="right" border="0" style="width:24px;float:right;right:40%;bottom:6px;" height="25" src="<?=$path_raiz_aplicacion_local?>img/close_modal.png" /></a></span></center></div>',{
		  containerCss:{
			  backgroundColor:"#333",
			  borderColor:"#333",
			  width:"320px",
			  height:"215px!important",
			  close:true,
			  padding:0
		  },
		  onClose:function(dialog){
			dialog.data.fadeOut('fast',function(){
				dialog.container.hide('slow',function(){
					dialog.overlay.slideUp('fast',function(){
						$.modal.close();
					});
				});
			});
		  },
		  overlayClose:true
	  });
	  isModalDraw=true;
	}

	  $(".callProceed").click(function(){ // CALL PIXEL & CLOSE MODAL
			setTimeout("var pxTrade=new Image();pxTrade.src='<?=$serverTradedoubler?>"+leadNumber+"';",100);
			offModal();
	  });
	});

  setTimeout('$("body").click()',5000);
<?php
}
// END GESTION MODAL "LLAMAME"
?>
});
</script>
<!--[if lte IE 8]><link rel="stylesheet" href="<?=$path_raiz_aplicacion_local?>css/ie.css" /><![endif]-->
</head>
<!--[if lte IE 8]>
	<span style="display:block;color:red;font-size:2em;background:#fff!important;"><center><p>T&uacute; est&aacute;s utilizando un navegador <strong>obsoleto</strong>.<br />Por favor, <a href="http://browsehappy.com/">actualiza tu navegador</a> para mejorar tu experiencia <br />y poder continuar en esta p&aacute;gina.</p><br /><br /><hr /><hr /><br /><strong>Perdona las molestias!</strong><br /><br /><hr /><hr /><br /></center></span>
<![endif]-->
