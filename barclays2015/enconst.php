<?php
/**
 * Página de en construcción de Barclays
 * @date 2016.12.23
 */
include('../conf/config_web.php');
ini_set('display_errors', 0);
include('conf/config_web.php');



?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>En construcci&oacute;n</title>
<link href="<?php echo $path_raiz_aplicacion_local; ?>css/const.css" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="<?php echo $path_raiz_aplicacion_local; ?>img/const/favicon.png" type="image/x-icon">
</head>

<body>

<section class="basic-content">
<section class="small-content">
	<section class="skeleton">
    	<img class="constructor1" src="<?php echo $path_raiz_aplicacion_local; ?>img/const/tuercas.png" width="600" height="540" alt="En construcción"/> 
    	<h1>SITIO EN CONSTRUCCI&Oacute;N</h1>
        <h2>Nuestro equipo esta remodelando este sitio web</h2>
        <p>&iexcl;Gracias por tu visita!</p>
    	<img class="constructor2" src="<?php echo $path_raiz_aplicacion_local; ?>img/const/cono.png" width="190" height="200" alt="En construcción"/>
    </section>
</section>
</section>

</body>
</html>
