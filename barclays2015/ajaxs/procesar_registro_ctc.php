<?php
@session_start();
header('Content-Type: text/html; charset=utf-8');
include('../../conf/config_web.php');
include('../conf/config_web.php');
include($path_raiz_includes.'includes/capping_control.php');
$resCurl = array();
///$sOutput = array(); // ARRAY PARA LOS DATOS DE SALIDA JSON
$raw='';
$resCurl['error'] = 1;
//$sOutput['mensaje'] = 'ha habido un error en el proceso';
$sOutput['id'] = -1;
$cli = (empty($_REQUEST['cli'])) ? '' : $_REQUEST['cli'];

$nombre = (!empty($_REQUEST['nombre'])) ? trim($_REQUEST['nombre']) : '';
$apellidos = (!empty($_REQUEST['apellidos'])) ? trim($_REQUEST['apellidos']) : '';
$email = (!empty($_REQUEST['em'])) ? trim($_REQUEST['em']) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? $_REQUEST['provincia_text'] : $_REQUEST['provincia'];

$legal = (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : '1';
$permanencia = (!empty($_REQUEST['perm'])) ? trim($_REQUEST['perm']) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;
$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';

$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? trim($_REQUEST['idclient']) : '';
$sourcetype = (!empty($_REQUEST['sourcetype'])) ? trim($_REQUEST['sourcetype']) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? trim($_REQUEST['campaign']) : '';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? trim($_REQUEST['prioridad']) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? trim($_REQUEST['telefono']) : '';
$use_priority = (!empty($_REQUEST['use_priority']) && is_numeric($_REQUEST['use_priority']) && $_REQUEST['use_priority']==1 && $prioridad !='') ? 1 : 0;

$docType = (!empty($_REQUEST['doc_type'])) ? trim($_REQUEST['doc_type']) : '';
$docNum = (!empty($_REQUEST['doc_num'])) ? trim($_REQUEST['doc_num']) : '';

$arrBirthDate=explode('/',$_REQUEST['birth_date']);
$birthDate = !empty($_REQUEST['birth_date']) ? date('Y-m-d',strtotime($arrBirthDate[2].'-'.$arrBirthDate[1].'-'.$arrBirthDate[0])) : '';

//include($path_raiz_includes.'includes/capping_control.php');

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;

$campos = array('idclient' => $id_client, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad, 
    'nombre' => $nombre, 'apellidos' => $apellidos, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI, 
    'vcc' => 'fintel', 'pwd' => $secret_pwd, 'usr' => $secret_usr,
    'use_priority'=>$use_priority,
		'doc_type'=>$docType, 'doc_num'=>$docNum, 'birth_date'=>$birthDate);


if($docNum)	// SI EXISTE '$docNum', SE INICIA LA GESTION DE MOROSOS (M.F. 2015.08.21)
{
	$url_ws = $url_ws_defaulter;
	$file_ws = 'router.php';
}

//,"serverIp"=>"84.127.240.42"
$campos['urlOrigen']=$_SERVER['HTTP_REFERER'];	// ADDED 2016.02.23 M.F.
$campos['cli']=$cli;	// ADDED 2016.04.13 M.F.

$qs = http_build_query($campos);
$url = $url_ws . $file_ws . '?' . $qs;
//echo $url;die();
if ($telefono != '') {
    $conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
    if (!$conexion->connectDB()) {
        $msg_err = 'No se ha podido conectar a la BBDD. Error 102';
    } else {
        $cur_conn_id = $conexion->get_id_conexion();
        /* $sql1="SET AUTOCOMMIT=0;";
          $conexion->ejecuta_query($sql1);

          $sql2="BEGIN";
          $conexion->ejecuta_query($sql2);
         */
        $nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
        $nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');
        $telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
        $sql = "INSERT INTO %s (id_cliente,nombre,apellidos,telefono,id_provincia,b_legales,email,doc_type,doc_num,birth_date) VALUES ('%d','%s','%s','%s','%d', %d, '%s', %d, '%s', '%s');";
        $sql = sprintf($sql, $table_registros, $id_client, $nombreIns, $crea, $telefonoIns, (int) $_REQUEST['provincia'], $legal, $email, (int)$docType, $docNum, $birthDate);
        $conexion->ejecuta_query($sql);
        $id = mysql_insert_id();

        $conexion->disconnectDB();
//echo $url.'<hr><pre>';print_r($_REQUEST);print_r($campos);die();
        $ch = curl_init();
        $timeout = 0; // set to zero for no timeout
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $raw = curl_exec($ch);
        curl_close($ch);
        $resCurl = json_decode($raw);
        $error = $resCurl->error;

//        $sOutput['resCurl'] = $resCurl;
//        $sOutput['mensaje'] = $resCurl->mensaje;

        if($error) {
            $msg_err = 'Parece que el problema se debe a lo siguiente: "' . $resCurl->mensaje . '".';
        } else {
						$tmpCrea=$_SESSION['idCreatividad'];
						unset($_SESSION['namePromo']);	// SE DESTRUYE LA REFERENCIA DE LA SESION
						$tmpName=session_name('barclays'.date(Yimis));
						$_SESSION['namePromo']=session_name();	// SE CONSTRUYE UNA NUEVA SESION PARA PERMITIR REGISTROS NUEVOS DESDE LA MISMA APERTURA
						$_SESSION['idCreatividad']=$tmpCrea;

						if($docNum && $id && $resCurl->error == 2)
						{
							$sql='UPDATE %s SET defaulter_status=%d WHERE id=%d';
							$sql=sprintf($sql, $table_registros, $resCurl->error, $id);
						}
//echo '<hr>'.$sql;
//print_r($_REQUEST);
//print_r($resCurl);
//die('$resCurl->error = '.$resCurl->error);
            die($raw);
        }
    }
} else {
    if ($telefono == '') {
        $error = 1;
        $msg_err = 'Falta el campo Teléfono. Error 104';
    }
}

if($error)
{
	$query='SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1';
	$query=sprintf($query,$table_front_actions,$email,$_SESSION['namePromo']);
	$conexion->getResultSelectArray($query);
	$resQuery=$conexion->tResultadoQuery;
	$idUpdate=$resQuery[0]['id'];
	$query='UPDATE %s SET http_referer=\'ERROR CTC: %s\' WHERE id=\'%d\'';
	$query=sprintf($query,$table_front_actions,$resCurl->mensaje,(int)$idUpdate);
	$conexion->ejecuta_query($query);
//echo $query;print_r($resQuery);
//	$conexion->ejecuta_query($query);
}

//$sOutput['error'] = intval($error);
//$sOutput['mensaje'] = $msg_err;
//$sOutput['id'] = $id;

$res = json_encode($resCurl);
die($res);
?>