<?php
/**
 * Página de gracias de Nectar
 */
include('../conf/config_web.php');
ini_set('display_errors', 0);
include('conf/config_web.php');
include($path_raiz_includes . 'includes/initiate_thankyoupage.php');
include($path_raiz_includes . 'class/class.pixels.management.php');
$ln = (empty($_REQUEST['leadNumber'])) ? '0' : trim($_REQUEST['leadNumber']);
$okln = (strpos($ln,'ctc')===false) ? '0' : '1';
?>
<!DOCTYPE html>
<html lang="es">
<head>
		<title>¡Gracias por confiar en <?php echo ucwords($landingMainName); ?>!</title>
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width,initial-scale=1"/>
		<link href="<?php echo $path_raiz_aplicacion_local; ?>css/estilos_thanks.css" rel="stylesheet" type="text/css">

</head>
<body>
<!-- ************************* 
    PIXELES DE MAKE 
****************************** -->
<!-- Google Tag Manager  for MAKE -->
<script>
dataLayer = [{
	'pageCategory':'signup',
	'visitorType':'high-value',
	'transactionTotal':'1',
	'transactionId':'<?php echo $ln;?>',
	'template' : 'thankyoupage',
         'validlead' : '<?php echo $okln;?>'
}];
</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TFTJXD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TFTJXD');</script>
<!-- End Google Tag Manager for MAKE -->

	<section class="contenedor">
			<section class="foto">
					<img src="<?php echo $path_raiz_aplicacion_local . 'img/' . $objSiteData->prefixFolders . '/thanks_ppal.jpg'; ?>"/>
					<img src="<?php echo $path_raiz_aplicacion_local . 'img/' . $objSiteData->prefixFolders . '/thanks_mvl.jpg'; ?>"/>
			</section>

	</section>
<?php
	/* $id_crea=25;
		$leadNumber = 'test-d6t345';
		$saltarPixeles = true;
		$cookieEnable = true;

		echo 'id_crea: ' . $id_crea;
		echo ' salta pixel: ' . $saltarPixeles;
		new dBug($_REQUEST);
	 *  */
	if ($correctProcess)
			include($path_raiz_includes . 'includes/pixels_thankyoupage.php');
?>
</body>
</html>