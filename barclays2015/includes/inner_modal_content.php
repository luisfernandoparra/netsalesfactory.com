<?php
switch ($_REQUEST['cr'])
{
	case 96:
		$content='
<style>
img{
	width:98%;
	height:auto;
	}

.text-pop{
	font-family:Arial, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", serif;
	font-size:1.3em;
	max-width:700px;
	margin:auto;
	}
	
.text-pop h1{
	font-size:1.3em;
	color:#00a1e4;
	}
	
.first{
	font-size:0.9em;
	color:#000000;
	}	
	
.text-pop p{
	font-size:0.8em!important;
	color:#000!important;
	}
</style>
<section class="text-pop">

<h1>Servicio de Transferencia de Línea de Crédito</h1>
<br />
<p class="first">(exclusivo para la cancelación de la deuda que tengas en otra tarjeta de otra entidad).</p>
<br />
<p>Ejemplos:</p>
<br />
<img src="img/096/table11.jpg" width="979" height="261" alt=""/>
<p class="first">Tu Barclaycard Oro te permitirá, además, cancelar la deuda que tengas en otra tarjeta de otra entidad, y posiblemente ahorrarte dinero al mismo tiempo. Porque a la cantidad que nos solicites para ello le aplicaremos un 0% TIN los 12 primeros meses, y un 23,90% TIN el resto del periodo (si es que lo decides abonar a más plazo), y al resto de operaciones que hagas con tu tarjeta (14,27%TAE2 para un plazo de 24 meses).</p>
<br />
<img src="img/096/table2.jpg" width="659" height="388" alt=""/> 
<br />
<p>Concesión de la tarjeta sujeta a los criterios habituales de aprobación de Barclays Bank PLC, Sucursal en España. El límite máximo de una tarjeta Barclaycard (incluyendo el saldo para la cancelación de la deuda en otra entidad) nunca será de más de 7.000€.</p>
<br />
<p>Servicio Cancelación de Deuda (también denominado Servicio de Transferencia de Línea de Crédito): Oferta sometida a los criterios habituales de aprobación de Barclays Bank PLC, Sucursal en España. Exclusivo para nuevos clientes y contratable únicamente en el mismo momento que la tarjeta al que está asociado. En ningún caso el importe concedido será para cancelar una deuda pendiente en una tarjeta Barclaycard. En caso de que en algún momento de los 12 primeros meses se produzca una situación irregular de impago del saldo dispuesto y/o intereses del servicio de cancelación de deuda y/o de la tarjeta, dejará de tener vigencia el tipo de interés promocional del 0% TIN, aplicándose en el periodo restante el tipo de interés reflejado en el contrato de la tarjeta aunque posteriormente se regularice la situación indicada. En determinados casos, es posible que para la concesión se solicite algún tipo de documentación.</p>
<br />
<p><sup>1</sup>Comisión por Servicio de Transferencia de Línea de Crédito del 3,99% del importe concedido y que también es financiada al 0% TIN. Ejemplo para 12 meses: para un importe de 3.000€ a 12 meses, la comisión es de 119,70€. Importe total a financiar 3.119,70€. La cuota mensual los 12 meses sería de 259,98€ (Importe total adeudado 3.119,76€). El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.
</p>
<br />
<p><sup>2</sup>Comisión por Servicio de Transferencia de Línea de Crédito del 3,99% del importe concedido y que también es financiada al 0% TIN. Ejemplo para 24 meses: para un importe de 3.000€ para la cancelación de la deuda en otra entidad (la comisión es de 119,70€) y de 1.000€ de saldo dispuesto en la tarjeta el primer mes. Importe total a financiar 4.119,70€. La cuota mensual los 12 primeros meses sería de 182,81€ y de 200,24€ los siguientes (Importe total adeudado 4.596,64€). Si a la fecha de vencimiento correspondiente no se hubiese satisfecho completamente el saldo total, los pagos parciales se imputarán en el orden indicado en los términos y condiciones. El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.
</p>
<br />
<p><sup>3</sup>Comisión por Servicio de Transferencia de Línea de Crédito del 3,99% del importe concedido y que también es financiada al 0% TIN. Ejemplo para 24 meses: para un importe de 3.000€ para la cancelación de la deuda en otra entidad (la comisión es de 119,70€). Importe total a financiar 3.119,70€. La cuota mensual los 12 primeros meses sería de 129,99€ y de 147,42€ las siguientes (Importe total adeudado 3.328,93€). El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.</p>
<br />
<p><sup>4</sup>Tipo de interés de la tarjeta del 23,90% TIN (26,70% TAE). Ejemplo: para un importe de 1.500€, la cuota mensual sería de 79,23€ durante 24 meses. Importe total adeudado 1.901,56€. Si a la fecha de vencimiento correspondiente no se hubiese satisfecho completamente el saldo total, los pagos parciales se imputarán en el orden indicado en los términos y condiciones. El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.</p>
<br />
<p><sup>5</sup>Seguros y Servicios Gratuitos: sujetos a las condiciones, límites de cobertura y exclusiones de la póliza de seguro o el servicio. Además son de carácter temporal, reservándose Barclaycard la facultad discrecional de renovarlos anualmente, siendo comunicada su posible no renovación al cliente con previo aviso Consulta condiciones de los asociados a la tarjeta antes de contratarlos en el 901 01 02 03 o en www.barclaycard.es.</p>
<br />
<p>Barclaycard es una marca registrada de Barclays Bank, PLC, Sucursal en España, con domicilio social en Plaza de Colón, nº 1, 28046 Madrid, - C.I.F. W0061418-J y está inscrita en el Registro Mercantil de Madrid al tomo 5213, General 4353 de la Sección 3ª del Libro de Sociedades, hoja M-41341.
</p>
<br />

</section>
';
		$success=true;
		break;
	case 115:
		$content='
<style>
@charset "UTF-8";

img{
	width:98%;
	height:auto;
	}

body{
	font-family:Arial, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", serif
	}

.text-pop{
	max-width:700px;
	margin:auto;
	}
	
.text-pop h1{
	font-size:1.3em;
	color:#00a1e4;
	}
	
.first{
	font-size:0.9em;
	color:#000000;
	}	
	
.text-pop p{
	font-size:0.8em;
	color:#000000;
}
</style>
<section class="text-pop">

<h1>Servicio de Transferencia de Línea de Crédito</h1>

<p class="first">(exclusivo para la cancelación de la deuda que tengas en otra tarjeta de otra entidad).</p>

<p>Ejemplos:</p>

<img src="img/115/table11.jpg" width="979" height="261" alt=""/>
<p class="first">Tu Barclaycard Oro te permitirá, además, cancelar la deuda que tengas en otra tarjeta de otra entidad, y posiblemente ahorrarte dinero al mismo tiempo. Porque a la cantidad que nos solicites para ello le aplicaremos un 0% TIN los 12 primeros meses, y un 23,90% TIN el resto del periodo (si es que lo decides abonar a más plazo), y al resto de operaciones que hagas con tu tarjeta (14,27%TAE2 para un plazo de 24 meses).</p>

<img src="img/115/table2.jpg" width="659" height="388" alt=""/> 

<p>Concesión de la tarjeta sujeta a los criterios habituales de aprobación de Barclays Bank PLC, Sucursal en España. El límite máximo de una tarjeta Barclaycard (incluyendo el saldo para la cancelación de la deuda en otra entidad) nunca será de más de 7.000€.</p>

<p>Servicio Cancelación de Deuda (también denominado Servicio de Transferencia de Línea de Crédito): Oferta sometida a los criterios habituales de aprobación de Barclays Bank PLC, Sucursal en España. Exclusivo para nuevos clientes y contratable únicamente en el mismo momento que la tarjeta al que está asociado. En ningún caso el importe concedido será para cancelar una deuda pendiente en una tarjeta Barclaycard. En caso de que en algún momento de los 12 primeros meses se produzca una situación irregular de impago del saldo dispuesto y/o intereses del servicio de cancelación de deuda y/o de la tarjeta, dejará de tener vigencia el tipo de interés promocional del 0% TIN, aplicándose en el periodo restante el tipo de interés reflejado en el contrato de la tarjeta aunque posteriormente se regularice la situación indicada. En determinados casos, es posible que para la concesión se solicite algún tipo de documentación.</p>

<p><sup>1</sup>Comisión por Servicio de Transferencia de Línea de Crédito del 3,99% del importe concedido y que también es financiada al 0% TIN. Ejemplo para 12 meses: para un importe de 3.000€ a 12 meses, la comisión es de 119,70€. Importe total a financiar 3.119,70€. La cuota mensual los 12 meses sería de 259,98€ (Importe total adeudado 3.119,76€). El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.
</p>

<p><sup>2</sup>Comisión por Servicio de Transferencia de Línea de Crédito del 3,99% del importe concedido y que también es financiada al 0% TIN. Ejemplo para 24 meses: para un importe de 3.000€ para la cancelación de la deuda en otra entidad (la comisión es de 119,70€) y de 1.000€ de saldo dispuesto en la tarjeta el primer mes. Importe total a financiar 4.119,70€. La cuota mensual los 12 primeros meses sería de 182,81€ y de 200,24€ los siguientes (Importe total adeudado 4.596,64€). Si a la fecha de vencimiento correspondiente no se hubiese satisfecho completamente el saldo total, los pagos parciales se imputarán en el orden indicado en los términos y condiciones. El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.
</p>

<p><sup>3</sup>Comisión por Servicio de Transferencia de Línea de Crédito del 3,99% del importe concedido y que también es financiada al 0% TIN. Ejemplo para 24 meses: para un importe de 3.000€ para la cancelación de la deuda en otra entidad (la comisión es de 119,70€). Importe total a financiar 3.119,70€. La cuota mensual los 12 primeros meses sería de 129,99€ y de 147,42€ las siguientes (Importe total adeudado 3.328,93€). El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.</p>

<p><sup>4</sup>Tipo de interés de la tarjeta del 23,90% TIN (26,70% TAE). Ejemplo: para un importe de 1.500€, la cuota mensual sería de 79,23€ durante 24 meses. Importe total adeudado 1.901,56€. Si a la fecha de vencimiento correspondiente no se hubiese satisfecho completamente el saldo total, los pagos parciales se imputarán en el orden indicado en los términos y condiciones. El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.</p>

<p><sup>5</sup>Seguros y Servicios Gratuitos: sujetos a las condiciones, límites de cobertura y exclusiones de la póliza de seguro o el servicio. Además son de carácter temporal, reservándose Barclaycard la facultad discrecional de renovarlos anualmente, siendo comunicada su posible no renovación al cliente con previo aviso Consulta condiciones de los asociados a la tarjeta antes de contratarlos en el 901 01 02 03 o en www.barclaycard.es.</p>

<p>Barclaycard es una marca registrada de Barclays Bank, PLC, Sucursal en España, con domicilio social en Plaza de Colón, nº 1, 28046 Madrid, - C.I.F. W0061418-J y está inscrita en el Registro Mercantil de Madrid al tomo 5213, General 4353 de la Sección 3ª del Libro de Sociedades, hoja M-41341.

Solicita tu tarjeta Rellena Clock Solicítala ahora online Te puede interesar Mi Barclaycard Servicio al cliente Simulador Barclaycard Otras tarjetas destacadas Nueva Visa Barclaycard
</p>


</section>';
		$success=true;
		break;
	case 122:
		$content='
<style>
@charset "UTF-8";

img{
	width:98%;
	height:auto;
	}

body{
	font-family:Arial, "Hoefler Text", "Liberation Serif", Times, "Times New Roman", serif
	}

.text-pop{
	max-width:700px;
	margin:auto;
	}

.text-pop h1{
	font-size:1.3em;
	color:#00a1e4;
	}

.first{
	font-size:0.9em;
	color:#000000;
	}

.text-pop p{
	font-size:0.8em;
	color:#000000;
	}
</style>
<section class="text-pop">

<h1>Servicio de Transferencia de Línea de Crédito</h1>

<p class="first">(exclusivo para la cancelación de la deuda que tengas en otra tarjeta de otra entidad).</p>

<p>Ejemplos:</p>

<img src="img/122/table11.jpg" width="979" height="261" alt=""/>
<p class="first">Tu Barclaycard Oro te permitirá, además, cancelar la deuda que tengas en otra tarjeta de otra entidad, y posiblemente ahorrarte dinero al mismo tiempo. Porque a la cantidad que nos solicites para ello le aplicaremos un 0% TIN los 12 primeros meses, y un 23,90% TIN el resto del periodo (si es que lo decides abonar a más plazo), y al resto de operaciones que hagas con tu tarjeta (14,27%TAE2 para un plazo de 24 meses).</p>

<img src="img/122/table2.jpg" width="659" height="388" alt=""/>

<p>Concesión de la tarjeta sujeta a los criterios habituales de aprobación de Barclays Bank PLC, Sucursal en España. El límite máximo de una tarjeta Barclaycard (incluyendo el saldo para la cancelación de la deuda en otra entidad) nunca será de más de 7.000€.</p>
<br />
<p>Servicio Cancelación de Deuda (también denominado Servicio de Transferencia de Línea de Crédito): Oferta sometida a los criterios habituales de aprobación de Barclays Bank PLC, Sucursal en España. Exclusivo para nuevos clientes y contratable únicamente en el mismo momento que la tarjeta al que está asociado. En ningún caso el importe concedido será para cancelar una deuda pendiente en una tarjeta Barclaycard. En caso de que en algún momento de los 12 primeros meses se produzca una situación irregular de impago del saldo dispuesto y/o intereses del servicio de cancelación de deuda y/o de la tarjeta, dejará de tener vigencia el tipo de interés promocional del 0% TIN, aplicándose en el periodo restante el tipo de interés reflejado en el contrato de la tarjeta aunque posteriormente se regularice la situación indicada. En determinados casos, es posible que para la concesión se solicite algún tipo de documentación.</p>
<br />
<p><sup>1</sup>Comisión por Servicio de Transferencia de Línea de Crédito del 3,99% del importe concedido y que también es financiada al 0% TIN. Ejemplo para 12 meses: para un importe de 3.000€ a 12 meses, la comisión es de 119,70€. Importe total a financiar 3.119,70€. La cuota mensual los 12 meses sería de 259,98€ (Importe total adeudado 3.119,76€). El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.
</p>
<br />
<p><sup>2</sup>Comisión por Servicio de Transferencia de Línea de Crédito del 3,99% del importe concedido y que también es financiada al 0% TIN. Ejemplo para 24 meses: para un importe de 3.000€ para la cancelación de la deuda en otra entidad (la comisión es de 119,70€) y de 1.000€ de saldo dispuesto en la tarjeta el primer mes. Importe total a financiar 4.119,70€. La cuota mensual los 12 primeros meses sería de 182,81€ y de 200,24€ los siguientes (Importe total adeudado 4.596,64€). Si a la fecha de vencimiento correspondiente no se hubiese satisfecho completamente el saldo total, los pagos parciales se imputarán en el orden indicado en los términos y condiciones. El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.
</p>
<br />
<p><sup>3</sup>Comisión por Servicio de Transferencia de Línea de Crédito del 3,99% del importe concedido y que también es financiada al 0% TIN. Ejemplo para 24 meses: para un importe de 3.000€ para la cancelación de la deuda en otra entidad (la comisión es de 119,70€). Importe total a financiar 3.119,70€. La cuota mensual los 12 primeros meses sería de 129,99€ y de 147,42€ las siguientes (Importe total adeudado 3.328,93€). El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.</p>
<br />
<p><sup>4</sup>Tipo de interés de la tarjeta del 23,90% TIN (26,70% TAE). Ejemplo: para un importe de 1.500€, la cuota mensual sería de 79,23€ durante 24 meses. Importe total adeudado 1.901,56€. Si a la fecha de vencimiento correspondiente no se hubiese satisfecho completamente el saldo total, los pagos parciales se imputarán en el orden indicado en los términos y condiciones. El saldo generado por los movimientos distintos al servicio de Cancelación de Deuda generará interés al tipo indicado de la tarjeta.</p>
<br />
<p><sup>5</sup>Seguros y Servicios Gratuitos: sujetos a las condiciones, límites de cobertura y exclusiones de la póliza de seguro o el servicio. Además son de carácter temporal, reservándose Barclaycard la facultad discrecional de renovarlos anualmente, siendo comunicada su posible no renovación al cliente con previo aviso Consulta condiciones de los asociados a la tarjeta antes de contratarlos en el 901 01 02 03 o en www.barclaycard.es.</p>
<br />
<p>Barclaycard es una marca registrada de Barclays Bank, PLC, Sucursal en España, con domicilio social en Plaza de Colón, nº 1, 28046 Madrid, - C.I.F. W0061418-J y está inscrita en el Registro Mercantil de Madrid al tomo 5213, General 4353 de la Sección 3ª del Libro de Sociedades, hoja M-41341.
<br />
Solicita tu tarjeta Rellena Clock Solicítala ahora online Te puede interesar Mi Barclaycard Servicio al cliente Simulador Barclaycard Otras tarjetas destacadas Nueva Visa Barclaycard
</p>

</section>';
		$success=true;
		break;
	case 142:
		$content='
<style>
@charset "UTF-8";

@font-face {
	font-family: "Conv_Verdana";
	src: url("fonts/Verdana.eot");
	src: url("fonts/Verdana.woff") format("woff"), url("fonts/Verdana.ttf") format("truetype"), url("fonts/Verdana.svg") format("svg");
	font-weight: normal;
	font-style: normal;
}

img{
	width:98%;
	height:auto;
	}

.text-pop{
display:inlin-block;
	font-family:Conv_Verdana",arial;
	max-width:700px;
	margin:auto;
	background-color: #FFFFFF;
	}

.text-pop h1{
	font-size:2.1em;
	color:#00a1e4;
	font-weight:lighter;
	}

.text-pop h2{
	font-size:1.2em;
	color:#00a1e4;
	}

.text-pop h2 sup{
	font-size:0.4em;
	}

.first{
	font-size:0.9em;
	color:#000000;
	}

.text-pop p{
	font-size:0.8em;
	color:#000000;
	}

.legal-bank{
	color:#00a1e4;
	width:90%;
	font-family:"verdada", Arial;
    max-width:980px;
    margin:3em auto 0 auto;
	font-size:.77em;
	border: 1px solid #00a1e4;
	-webkit-border-radius: 15px;
	-moz-border-radius: 15px;
	border-radius: 15px;
	padding:1em;
	}

@media(max-width:700px){

.text-pop h1{
	font-size:1.5em;
	}
}
</style>
<section class="text-pop">


<h1>Servicio de Transferencia de L&iacute;nea de Cr&eacute;dito
0&#37; TIN para un plazo de 12 meses para la primera transferencia. 7,53&#37; TAE
</h1>
<br />
<h2>
Para el resto del periodo y para el resto de operaciones de la Tarjeta: 23,90&#37; TIN, 26,70&#37; TAE<sup>4</sup>
</h2>
<br />
<p>Ejemplos:</p>

<img src="img/142/table11.jpg" width="979" height="261" alt=""/>
<br />
<p>Un servicio pionero que te permitir&aacute; cancelar la deuda que tengas en otra tarjeta de otra entidad, y posiblemente ahorrarte dinero al mismo tiempo. Porque a la cantidad que nos solicites para ello le aplicaremos un 0&#37; TIN los 12 primeros meses, y un 23,90&#37; TIN el resto del periodo (si es que lo decides abonar a m&aacute;s plazo), y al resto de operaciones que hagas con tu tarjeta (14,27&#37;TAE2 para un plazo de 24 meses).</p>
<br />
<p>Exclusivo para la cancelaci&oacute;n de la deuda que tengas en otra tarjeta de otra entidad.</p>
<br />
<h2>C&oacute;mo funciona</h2>

<p>•	En tu solicitud de tarjeta, podr&aacute;s indicarnos la cantidad que deseas solicitar para cancelar tu deuda en tu otra tarjeta.
</p>

<p>•	Si cumples nuestros criterios habituales de aprobaci&oacute;n (tanto para la concesi&oacute;n de la tarjeta como para el importe destinado a cancelar tu deuda), procederemos a informarte v&iacute;a tel&eacute;fono, carta, email o SMS.</p>

<p>•	Y unos d&iacute;as despu&eacute;s recibir&aacute;s en la cuenta corriente que nos hayas indicado el importe solicitado (y en tu direcci&oacute;n postal tu tarjeta). En ese momento deber&aacute;s proceder a cancelar la deuda de tu otra tarjeta mediante un traspaso desde tu cuenta, o en la forma que desees.</p>
<br />
<h2>Cu&aacute;nto pagar&iacute;as</h2>

<p>En Barclaycard abonar&aacute;s por esa deuda que ahora tienes en otra tarjeta:</p>

<p>•	Un 0&#37; TIN los 12 primeros meses.</p>

<p>•	Y un 23,90&#37; TIN el resto del periodo (si es que lo decides abonar a m&aacute;s plazo) y al resto de operaciones que hagas con tu tarjeta.</p>

<p>•	14,27&#37; TAE2 para un plazo de 24 meses.<br>
Para el resto del periodo y para el resto de operaciones de la Tarjeta: 23,90&#37; TIN, 26,70&#37; TAE<sup>4</sup>
</p>

<article class="legal-bank">
Barclays Bank PLC ha transmitido su negocio de tarjetas de cr&eacute;dito en Espa&ntilde;a a WiZink Bank, S.A., entidad de cr&eacute;dito autorizada por el Banco de Espa&ntilde;a, inscrita en el Registro de Bancos y Banqueros con el n&uacute;mero 0229 y en el Registro Mercantil de Madrid, al Tomo 12.468, folio 178, hoja M-198.598, inscripci&oacute;n 13ª. Tiene su domicilio social en Madrid, c/Vel&aacute;zquez nº 34 (CP 28001). Las marcas son propiedad de Barclays Bank PLC y est&aacute;n licenciadas a favor de WiZink Bank, S.A.
</article>
<br />
<p>Aprobaci&oacute;n de concesi&oacute;n de Tarjeta y servicios sujetos a los criterios habituales de aprobaci&oacute;n de WiZink Bank, S.A. El l&iacute;mite m&aacute;ximo de una tarjeta Barclaycard (incluyendo el saldo para la cancelaci&oacute;n de la deuda en otra entidad) nunca ser&aacute; de m&aacute;s de 5.000&euro;. Antes de contratar la tarjeta y/o los servicios descritos consulta en barclaycard.es o en el 91 836 36 36 los t&eacute;rminos y condiciones y costes.</p>
<br />
<p>Servicio Cancelaci&oacute;n de Deuda (tambi&eacute;n denominado Servicio de Transferencia de L&iacute;nea de Cr&eacute;dito): Oferta sometida a los criterios habituales de aprobaci&oacute;n de WiZink Bank, S.A. Exclusivo para nuevos clientes y contratable &uacute;nicamente en el mismo momento que la tarjeta al que est&aacute; asociado. En ning&uacute;n caso el importe concedido ser&aacute; para cancelar una deuda pendiente en una tarjeta Barclaycard o WiZink. En caso de que en alg&uacute;n momento de los 12 primeros meses se produzca una situaci&oacute;n irregular de impago del saldo dispuesto y/o intereses del servicio de cancelaci&oacute;n de deuda y/o de la tarjeta, dejar&aacute; de tener vigencia el tipo de inter&eacute;s promocional del 0&#37; TIN, aplic&aacute;ndose en el periodo restante el tipo de inter&eacute;s reflejado en el contrato de la tarjeta aunque posteriormente se regularice la situaci&oacute;n indicada. En determinados casos, es posible que para la concesi&oacute;n se solicite alg&uacute;n tipo de documentaci&oacute;n.</p>
<br />
<p>(1) Comisi&oacute;n por Servicio de Transferencia de L&iacute;nea de Cr&eacute;dito del 3,99&#37; del importe concedido y que tambi&eacute;n es financiada al 0&#37; TIN. Ejemplo para 12 meses: para un importe de 3.000&euro; a 12 meses, la comisi&oacute;n es de 119,70&euro;. Importe total a financiar 3.119,70&euro;. La cuota mensual los 12 meses ser&iacute;a de 259,98&euro; (Importe total adeudado 3.119,76&euro;). El saldo generado por los movimientos distintos al servicio de Cancelaci&oacute;n de Deuda generar&aacute; inter&eacute;s al tipo indicado de la tarjeta.</p>
<br />
<p>(2) Comisi&oacute;n por Servicio de Transferencia de L&iacute;nea de Cr&eacute;dito del 3,99&#37; del importe concedido y que tambi&eacute;n es financiada al 0&#37; TIN. Ejemplo para 24 meses: para un importe de 3.000&euro; para la cancelaci&oacute;n de la deuda en otra entidad (la comisi&oacute;n es de 119,70&euro;) y de 1.000&euro; de saldo dispuesto en la tarjeta el primer mes. Importe total a financiar 4.119,70&euro;. La cuota mensual los 12 primeros meses ser&iacute;a de 182,81&euro; y de 200,24&euro; los siguientes (Importe total adeudado 4.596,64&euro;). Si a la fecha de vencimiento correspondiente no se hubiese satisfecho completamente el saldo total, los pagos parciales se imputar&aacute;n en el orden indicado en los t&eacute;rminos y condiciones. El saldo generado por los movimientos distintos al servicio de Cancelaci&oacute;n de Deuda generar&aacute; inter&eacute;s al tipo indicado de la tarjeta.</p>
<br />
<p>(3) Comisi&oacute;n por Servicio de Transferencia de L&iacute;nea de Cr&eacute;dito del 3,99&#37; del importe concedido y que tambi&eacute;n es financiada al 0&#37; TIN. Ejemplo para 24 meses: para un importe de 3.000&euro; para la cancelaci&oacute;n de la deuda en otra entidad (la comisi&oacute;n es de 119,70&euro;). Importe total a financiar 3.119,70&euro;. La cuota mensual los 12 primeros meses ser&iacute;a de 129,99&euro; y de 147,42&euro; las siguientes (Importe total adeudado 3.328,93&euro;). El saldo generado por los movimientos distintos al servicio de Cancelaci&oacute;n de Deuda generar&aacute; inter&eacute;s al tipo indicado de la tarjeta.</p>
<br />
<p>(4) Tipo de inter&eacute;s de la tarjeta del 23,90&#37; TIN (26,70&#37; TAE). Ejemplo: para un importe de 1.500&euro;, la cuota mensual ser&iacute;a de 79,23&euro; durante 24 meses. Importe total adeudado 1.901,56&euro;. Si a la fecha de vencimiento correspondiente no se hubiese satisfecho completamente el saldo total, los pagos parciales se imputar&aacute;n en el orden indicado en los t&eacute;rminos y condiciones. El saldo generado por los movimientos distintos al servicio de Cancelaci&oacute;n de Deuda generar&aacute; inter&eacute;s al tipo indicado de la tarjeta.</p>
<br />
<p>WiZink Bank, S.A. C/ Vel&aacute;zquez nº 34, 28001 Madrid. Reg. Merc. de Madrid. T. 12.468, Folio 178, H nº M-198598, Insc.13 – CIF: A-81831067.</p>
<br />
</section>
';
		$success=true;
		break;
	default:
		$content='<center><h2>Parece que no se ha podido encontrar la información solicitada</h2><br /><br /><br />Disculpa las molestias</center>';
		$success=false;
}

//$response['content']='<br /><br />-->'.$_REQUEST['cr'];

$response['content']=$content;
$response['success']=$success;
$response=json_encode($response);
die($response);