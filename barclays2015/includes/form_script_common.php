<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);

?>
					<p>Solicita información</p>

					<div id="campos">
						<div class="error"><ul></ul></div>

						<div class="row">
							<div class="col8"><input class="celda" type="text" name="nombre" maxlength="100" id="nombre" value="<?=$nombre?>" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" placeholder="Nombre y Apellidos" /></div>
						</div>

						<div class="row">

							<div class="col8">
								<input class="celda" type="email" name="email" maxlength="100" id="email" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="Email" />
							</div>
						</div>

						<div class="row">

							<div class="col8">
								<input class="celda" type="tel" maxlength="9" name="telefono" id="telefono" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Tel&eacute;fono" />
							</div>
						</div>

						<div class="legal" style="padding-top:10px;">
									<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;He leído y acepto la 
								<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php';?>" data-ref="" target="_blank">política de privacidad</a>
						</div>

						<div class="row">
							<div class="col12 espacio_btn">
								<input class="button green sendData" type="button" id="btnProcesar" name="btnProcesar" value="Enviar">
							</div>
						</div>

						<div class="clearfix"></div>
					</div>
