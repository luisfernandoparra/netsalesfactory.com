<?php
@session_start();
$idLandingFormatted = str_pad($objSiteData->landingId, 3, '0', STR_PAD_LEFT);
$rutaImgs = $path_raiz_aplicacion_local . 'img/' . $idLandingFormatted . '/';
$defaultLocalPhoneCall = '';
$inbound_phone = ($inbound_phone) ? $inbound_phone : ''; // SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);

function obtenerNavegador2015($user_agent=0)
{
 $browsers='mozilla msie gecko firefox konqueror safari netscape navigator opera mosaic lynx amaya omniweb chrome';
 $browsers=@split(' ',$browsers);
 $nua=strToLower($_SERVER['HTTP_USER_AGENT']);
 $l=strlen($nua);
 for($i=0;$i<count($browsers);$i++)
 {
  $browser=$browsers[$i];
  $n=stristr($nua,$browser);
  if(strlen($n)>0)
  {
   $verNav = '';
   $navX = $browser;
   $j=strpos($nua, $navX)+$n+strlen($navX)+1;
   for(; $j<=$l; $j++)
   {
    $s=substr($nua,$j,1);
    if(is_numeric($verNav.$s))
    $verNav.= $s;
    else
    break;
   }
  }
 }
 if(!$navX) $navX=$_SERVER['HTTP_USER_AGENT'];
 $naveg=array(
	 'all'=>$navX.' '.$verNav,
	 'ver'=>$verNav,
	 'name'=>$navX
 );
 return $naveg;
}

$elNavegador=obtenerNavegador2015($_SERVER['HTTP_USER_AGENT']);
$userNavigator=$elNavegador['name'];

?>
<style>
    div.error2{ display : none; border:2px solid #D81E05; }
    .error2{color:#D81E05; background:#FCF1F0;}
    div.error2 ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
    div.error2 ul li{margin:0 0 10 0;list-style-type:none;}
    div.error2 ul li label{font-weight:normal}
    div.error2, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
    .ok{color:#333333; background-color:#EFFFDA;padding:10px;}
    input.error2{border:2px solid #D81E05;background-color:#FCF1F0;}

    .appbutton {
        background-color:#009fe4;
        background-image:url("img/f-boton-color.png");
        background-position:-10px 0;
        background-repeat:no-repeat;
        border:medium none #00335b;
        border-radius:3px;
        box-shadow:0 1px 2px #424242;
        color:white !important;
        cursor:pointer;
        display:inline-block;
        font:14px Arial,Helvetica,sans-serif;
        margin:5px 5px 0 0;
        padding:8px 13px;
        text-align:center;
        text-decoration:none !important;
        text-shadow:0 1px #00335b;
        width:auto;
    }
    .innerModal{
        box-sizing:border-box;
        margin:0;
        padding:0;
        padding-top:6%;
        position:relative;
        line-height:22px!important;
        font:12px Arial,Helvetica,sans-serif;
    }


</style>

<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/lib/additional-methods.min.js"></script>

<script type="text/javascript">
var errCheckTel = 1;
var iconselected = 1;
var css_selected = 'circulo_azul ico_selected';
var css_not_selected = 'circulo_azul';
var isCtcPass=0;

$(document).ready(function() {

	$("#ctcForm").validate({
			rules:{
				telefono_ctc:{
					required:true
					,remote:{
						url:root_path_local +"includes/phone_check.php",
						contentType:"application/json; charset=utf-8",  
						data:{cr:<?=(int)$id_crea?>},
						dataFilter:function(response){
							var jsonResponse="";
							isCtcPass=1;
							jsonResponse = JSON.parse(response);
							if(jsonResponse.success == false)
							{
								isCtcPass=0;
								return "\"" + jsonResponse.errorMessage + "\"";
							}else{
								return true;
							}
							return false;
						}
					}
				}
			},
			errorLabelContainer: $("#ctcForm div.error2")
	});

	$('input[name="cookie_rejection"]').click(function(){
		setTimeout('$("#pie_galletero_left").css("width","95%").css("text-align","left");',350)
	});


	$("#boton_ctc").click(function(){
			var phoneBumberCtc=$.trim($('#telefono_ctc').val());
			res=validator2.element("#telefono_ctc");

			if(!isCtcPass)
			{
				res=validator2.element("#telefono_ctc");
				setTimeout('$("#boton_ctc").click();',300);
				return false;
			}

			if(!res && !isCtcPass)
				return false;

			if($.isNumeric(phoneBumberCtc) && phoneBumberCtc.length >= 9)
			{
					var valortype = array_typeofctc[0];
					var campana = arr_campanas[0] ? arr_campanas[0] : 0;
					$.facebox.loading();
					$.ajax({
							url: root_path_local + "includes/phone_check.php",
							method: "post",
							dataType: "json",
							data: {cr:<?= (int) $id_crea ?>, telefono_ctc: phoneBumberCtc},
							cache: false,
							async: false,
							success: function(response)
							{
									if (!response.error)
									{
											$.ajax({
													url: root_path_local + "ajaxs/procesar_registro_ctc.php",
													method: "post",
													dataType: "json",
													data: {
															telefono: phoneBumberCtc,
															sourcetype: valortype,
															campaign: campana,
															fuente: id_source,
															idclient: id_client,
															crea: nomb_promo
													},
													cache: false,
													async: false,
													success: function(response)
													{
															if(!response.error)
															{
																ComprobarInsercion(response, 0);
															}
															else
															{
																ComprobarInsercion(response, 0);
															}
													},
													error:function(response){
															console.log("err2");
															return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
													}
											});
									}
									else
									{
											console.log("err code 2C");
									}
							},
							error: function(response) {
									console.log("err2");
									return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
							}
					});
			}
			return false;
	});

	validator2 = $("#ctcForm").validate({
			errorContainer: $('div.error2'),
			errorLabelContainer: $('div.error2 ul'),
			wrapper: 'li'
	});

	/**
	 * Fiunción para cambiar el texto según se haga click en el icono
	 */
	$('[id^="ico-"]').click(function(e) {
			var id = $(this).attr('id');
			var tmp = id.split('-');
			var tmp_iconselected = tmp[1];
			if (iconselected != tmp_iconselected) {
					$('#text-' + iconselected).hide();
					$('#text-' + tmp_iconselected).fadeIn('slow');
					$('#selected-' + iconselected).removeClass(css_selected).addClass(css_not_selected);
					$('#selected-' + tmp_iconselected).removeClass(css_not_selected).addClass(css_selected);
					iconselected = tmp_iconselected;

			}
	})

	//********************
	// START NIF NIE CHECK
	jQuery.validator.addMethod("identificacionES", function(value, element)
	{
		"use strict";
		value = value.toUpperCase();
		// Texto común en todos los formatos
		if (!value.match('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)')){
			return false;
		}
		/* Inicio validacion NIF */
		if (/^[0-9]{8}[A-Z]{1}$/.test(value)){
			$("#doc_type").val(1);
			return ("TRWAGMYFPDXBNJZSQVHLCKE".charAt(value.substring(8, 0) % 23) === value.charAt(8));
		}
		//  Hay ciertos NIFs que empiezan por K, L o M
		if (/^[KLM]{1}/.test(value)){
			$("#doc_type").val(1);
			return (value[8] === String.fromCharCode(64));
		}
		/* Fin validacion NIF */

		/* Inicio validacion NIE */
		if (/^[T]{1}/.test(value)){
			$("#doc_type").val(2);
			return (value[8] === /^[T]{1}[A-Z0-9]{8}$/.test(value));
		}
		// Con los que empiezan por XYZ
		if (/^[XYZ]{1}/.test(value)){
			$("#doc_type").val(2);
//			$('#doc_type>option:eq(2)').attr('selected', true)
			return(
				value[8] === "TRWAGMYFPDXBNJZSQVHLCKE".charAt(
				value.replace('X', '0')
				.replace('Y', '1')
				.replace('Z', '2')
				.substring(0, 8) % 23
				)
			);
		}
		/* Fin validacion NIE */
		return false;
	}, "El campo <b>Nº de documento</b> debe contener un <b>NIF / NIE</b> correcto");
	// END NIF NIE CHECK
	//********************

<?php
//if($userNavigator != 'chrome')	// SE OMITE EL CONTROL PARAS CHROME
{
//	echo 'console.log("1.'.$userNavigator.'");';
?>
	// FILL WIDTH "/" DATE FIELD
	$('#birth_date').keydown(function(e){
		if(e.keyCode != 8){
			var l=$(this).val().length,	x = l === 2 || l === 5 ? '/' : '';
			if(l >= 10) return false;
			$(this).val($(this).val()+x);
		}
	});
<?php
}
?>

<?php
if($userNavigator != 'safari' && $userNavigator != 'chrome')	// FOR birth_date CONTROL SET
{
?>
//	$("#birth_date").focus(function(){$(this).attr("type","date")});
//	$("#birth_date").blur(function(){if($(this).val() == "") $(this).attr("type","text");});
<?php
}
?>

}); //document.ready
</script>

<header>
	<section class="cabecera">
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
echo $objSiteData->headerTopContent;

else
echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>

<?php
/**
* comentado el 2015.01.19 M.F.

if($inbound_phone != '')
{
?>
<a class="blockCall" href="tel:<?=$inbound_phone?>">
<p>Llámanos GRATIS</p>
<p><?= $drawInbound_phone ?></p>
</a>
<?php
}
*/
?>
	</section>
</header>


<section class="contenido">
	<section class="foto">
<?php
if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div class="marco">
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!
			</div>
';
}

?>
    </section>


    <section class="formulario">
        <form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
            <input type="hidden" name="destino" id="destino" value="">
<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
	$formScriptName='form_script_'.$idLandingFormatted.'.php';
}

$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
	echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
	include($formScriptName);
?>
        </form>
    </section> <!--CIERRE FORM -->


<section class="contenido_texto">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
	</section>
	<div class="img_tabla">
			<img src="img/069/footer_gold.jpg" alt="Barclaycard" />
	</div>
<!--CIERRE CONTENIDO --> 
</section>

	<footer>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
	</footer>