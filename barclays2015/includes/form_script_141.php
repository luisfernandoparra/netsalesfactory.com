<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
//$birth_date_pattern=(strpos($resDevice, 'android')) ? '' : '(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}';	// FOR birth_date CONTROL SET
$birth_date_pattern=(strpos($resDevice, 'android')) ? '' : '^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$';
?>
					<p>Rellena tus datos sin compromiso</p>

					<div id="campos">
						<div class="error"><ul></ul></div>

						<div class="form_bloque">
							<div class="row">
								<input class="celda" type="text" name="nombre" id="nombre" maxlength="100" value="<?=$nombre?>" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" placeholder="Nombre y Apellidos*" data-rule-minlength="2" data-msg-minlength="El campo &lt;strong&gt;Nombre&lt;/strong&gt; debe contener por lo menos dos caracteres" />
							</div>

							<div class="row">
								<select class="celda tipo_doc" type="text" name="doc_type" id="doc_type" maxlength="100" value="Tipo de documento" required data-msg-required="El campo &lt;strong&gt;Tipo de documento&lt;/strong&gt; es obligatorio" placeholder="Tipo de documento" aria-required="true">
									<option value="">Tipo de documento</option>
									<option value="1">NIF</option>
									<option value="2">NIE</option>
								</select>
							</div>
						</div>

						<div class="form_bloque">
							<div class="row">
								<input class="celda" type="tel" maxlength="9" name="telefono" id="telefono" required data-msg-required="El campo &lt;strong&gt;Tel&eacute;fono m&oacute;vil&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Tel&eacute;fono m&oacute;vil&lt;/strong&gt; sólo acepta números" data-msg-minlength="El campo &lt;strong&gt;Tel&eacute;fono m&oacute;vil&lt;/strong&gt; debe contener 9 dígitos" placeholder="Tel&eacute;fono m&oacute;vil*" />
							</div>
							<div class="row">
								<input class="celda identificacionES" type="text" maxlength="10" name="doc_num" id="doc_num" required data-msg-required="El campo &lt;strong&gt;Nº de documento&lt;/strong&gt; es obligatorio" data-rule-minlength="8" data-msg-minlength="El campo &lt;strong&gt;Nº de documento&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Nº de documento*" />
							</div>
						</div>

						<div class="form_bloque">
							<div class="row">
								<input class="celda" type="email" name="email" maxlength="100" id="email" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="Email" />
							</div>
							<div class="row">
								<input class="celda" type="text" name="birth_date" maxlength="10" id="birth_date" required data-msg-required="El campo &lt;strong&gt;Fecha de nacimiento&lt;/strong&gt; es obligatorio" data-msg-date="El campo &lt;strong&gt;Fecha de nacimiento&lt;/strong&gt; no es válido, &lt;strong&gt;ej: 20/04/1995&lt;/strong&gt;" value="<?=$birth_date?>" placeholder="DD / MM / AAAA" data-msg-pattern="El campo &lt;strong&gt;Fecha de nacimiento&lt;/strong&gt; no es válido, &lt;strong&gt;ej: 20/04/1995&lt;/strong&gt;" pattern="<?=$birth_date_pattern?>" />
							</div>
						</div>

						<div class="form_bloque">
							<div class="row">
								<div class="legal">
										<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;He leído y acepto la <a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php';?>" data-ref="" target="_blank">política de privacidad</a>
								</div>
							</div>

							<div class="row">
								<div class="col12 espacio_btn">
									<input class="button green sendData" type="button" id="btnProcesar" name="btnProcesar" value="Recibir informaci&oacute;n">
								</div>
							</div>
						</div>

						<div class="clearfix"></div>
					</div>
