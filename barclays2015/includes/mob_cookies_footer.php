<?php
@session_start();
$cookiesMinTimeLimitStart = $minTimeCookiesStart; // TIEMPO MAXIMO ANTES DE LANZAR AUTOMATICAMENTE LAS COOKIES
//$_SESSION['cookiesMan']=1;
//	START TIME CHECK COOKIES CONTROL INIT VARS
$timeUpdateCheckLanding = 5000;  // INTERVALO DE TIEMPO PARA EJECUTAR AUTOMATICAMENTE AJAX TEMPORIZADO (en milisegundos)
$jsInitStartXCounterLanding = 'var startCheckCookieLanding=0;';
ini_set('display_errors',$paintErrors);

//if((basename($_SERVER["SCRIPT_FILENAME"]) != 'index.php') && @!$_SESSION['sess_cookie_manage']) // ACEPTAR IMPLICITAMENTE AL NAVEGAR POR LA WEB

if (!@isset($_SESSION['startCheckCookieLanding'])) { // START VISITOR TIME COUNTER
    $_SESSION['startCheckCookieLanding'] = ceil(microtime(true));
    $_SESSION['sess_cookie_manage'] = 0;
    $jsInitStartXCounter = 'var startCheckCookieLanding=1;';
}

$elapsedTimeStart = ceil(microtime(true) - $_SESSION['startCheckCookieLanding']);
//	END TIME CHECK COOKIES CONTROL INIT VARS

//	Modificado por LFP 2013.11.21
if(isset($arr_creas) && isset($arr_creas[$id_crea]['cookie_css']) && !empty($arr_creas[$id_crea]['cookie_css']))
{
    $styleSheetFooter = $layoutType.trim($arr_creas[$id_crea]['cookie_css']);
} else {

    // START EXCEPCIONES PARA EL CSS SEGUN id_creatividad
    $styleSheetFooter = 'footer_cookies';
    $styleSheetFooterExtra = '';
    switch ($id_crea['body']) {
        case 4:
        case 5:
        case 6:
            $styleSheetFooterExtra = $id_crea['body'];
            break;
    }
    $styleSheetFooter.=$layoutType.$styleSheetFooterExtra . '.css';
    // END EXCEPCIONES PARA EL CSS SEGUN id_creatividad
}

?>
<div id="startScroll"></div>
<div id="timerDisplay"></div>

<style type="text/css">
.modalBottomCookies{
background-color: rgba(255,255,255,0.6);
width:100%;
z-index:1000;
bottom:0px;
position:fixed;
}
.modalBottomCookies .innerCookiesModal{
border-top:1px solid #fff;padding:28px 0;}
.modalBottomCookies .contentModalBottomCookies{max-width:930px;margin:0 auto;padding:7px 17px 7px 17px;vertical-align:middle;text-align:justify;background-color:#f1f1f1;-moz-border-radius:4px; -webkit-border-radius:4px;border-radius:4px; -webkit-box-shadow:inset 0 0 2px 0 rgba(0,0,0,0.2);box-shadow:inset 0 0 2px 0 rgba(0,0,0,0.2);border:2px solid #999;outline:1px solid #fff;position:relative;}
.modalBottomCookies .contentModalBottomCookies p{font:normal 11px/16px Arial,Helvetica,sans-serif;color:#333;text-shadow:0 1px 0 #fff;margin:0;}
.modalBottomCookies .contentModalBottomCookies p strong{background:url(img/icoInfoCookiesBottom.png) 0 3px no-repeat;display:block;padding-left:19px;margin-bottom:3px;line-height:19px;font-weight:bold;}
.modalBottomCookies .contentModalBottomCookies p a{font-weight:bold;color:#0097c8;}
.modalBottomCookies .contentModalBottomCookies a.closeModalBottomCookies{float:right;text-decoration:none;font-size:14px;color:#333;text-align:right;right:-5px;}
.modalBottomCookies .contentModalBottomCookies a.closeModalBottomCookies:hover{color:#000;}
.modalBottomCookies .contentModalBottomCookies p.cerrar{text-align:right;}
</style>


<div class="modalBottomCookies" id="modalBottomCookies">
	<div class="innerCookiesModal">
		<div class="contentModalBottomCookies">
			<a title="Cerrar" class="closeModalBottomCookies" href="javascript:void(0);">X</a>
			<p><strong>Uso de cookies</strong></p>
			<p>Utilizamos cookies propias, de analítica y de terceros para mejorar tu experiencia de usuario. Al continuar con la navegación consideramos que aceptas la instalación y el uso de cookies, <a target="_blank" id="id_enlace_politica_privacidad_cookies" href="<?=$path_raiz_aplicacion_local?>includes/cookies_footer_txt.php">más información aquí</a>.</p>
		</div>
	</div>
</div>


<link href="<?= $path_raiz_aplicacion_local ?>css/<?= $styleSheetFooter ?>" rel="stylesheet" type="text/css" />

<script type="text/javascript">

function drawDefaultPix()
{
//  $("head").append('<img src="https://secure.adnxs.com/seg?add=1177389&t=2" width="1" height="1" />');
//  $("head").append('<img src="https://ad.yieldmanager.com/pixel?id=2441367&t=2" width="1" height="1" />');
//  $("head").append('<img src="https://reachandrich.antevenio.com/track/compginc.asp?exptime=90&keyword=SanitasDental=visita" width="1" height="1" />');
//  includeScript("<?=$conditionalPixelStatic?>","js");
}

function includeScript(file_path, type)
{
  document.head = document.head || document.getElementsByTagName('head'[0]);
  if (type == "js")
  {
	var j = document.createElement("script");
	j.type = "text/javascript";
	j.src = file_path;
	document.head.appendChild(j);
  }
  return;
}

// START COOKIES CONTROL
var checkElapsedTime;
var cookiesEnabled = 0;
var cookiesManaged = 0;
//// END COOKIES CONTROL


function manageCookies(start)
{
	cookiesEnabled = start;
	cookiesManaged = 1;
	$("#pie_galletero_right").slideUp("fast", function() {
		$("#pie_galletero_right").css("width","0px!important");
		$(".pie_galletero_left").css("width","98%!important",function() {
			$("#cookie_info").css("text-align","center");
		});
		$("#cookie_info").css("text-align", "center");
	});
	if(start == 1){
		drawDefaultPix();
		$.ajaxSetup({async: false});
		$.ajax({
			url:"<?= $path_raiz_aplicacion_local ?>ajaxs/cookies_ajax_pixels.php",
			method:"post",
			dataType:"json",
			data:{isCookie: cookiesEnabled, fuente: id_source},
			cache:false
			,success:function(response)
			{
				if(response.pixel != undefined){
				$.each(response.pixel,function(key,val)
				{
					$("#footerWebPage").append(val);
				});
				}
			}
		});
		$.ajaxSetup({async: true});
	}
}

/* START MOD POL COO M.F. 2014.12.23 */
var firstStep=true;
function eraseModalBottomCookies()
{
	$(".modalBottomCookies").fadeOut("fast");
}
/* END MOD POL COO M.F. 2014.12.23 */


$(document).ready(function(){

/* START MOD POL COO M.F. 2014.12.23 */
	$(window).click(function(){
		if(firstStep === true)
		{
			manageCookies(1);
			setTimeout("eraseModalBottomCookies()",500)
		}
		firstStep=false;
	});

	$(window).on('scroll',function(){
		if(firstStep === true)
		{
			manageCookies(1);
			setTimeout("eraseModalBottomCookies()",500)
		}
		firstStep=false;
	})

	$(".closeModalBottomCookies").click(function(){
		eraseModalBottomCookies()
	});
/* END MOD POL COO M.F. 2014.12.23 */

	$(".closeCookiesPolicy").click(function() {
			$('#info_cook').slideUp("slow");
	});

	$("#cookie_acept").click(function() {
			manageCookies(1);
	});

	$("#cookie_rejection").click(function() {
			manageCookies(0);
	});

	$("#pie_galletero_left").click(function(e) {
			e.preventDefault();
			$('#info_cook').show();
			$('html,body').animate({scrollTop:$("#startScroll").offset().top},500);
	});
});

</script>

<div id="pie_galletero">
    <div id="pie_galletero_left" class="pie_galletero_left">
        Utilizamos cookies propias, de analítica y de terceros para mejorar tu experiencia de usuario. Si aceptas la instalación de cookies o continúas navegando, consideramos que aceptas su uso.<br />
        <center><a href="#startScroll" id="cookie_info" class="smooth">Más información aquí</a></center>
    </div>
	<br />
	<div id="pie_galletero_right"><center>
		  <input type="button" id="cookie_acept" name="cookie_acept" value="Acepto" class="cookieButton" /> <input type="button" id="cookie_rejection" name="cookie_rejection" value="No Acepto" class="cookieButton" />
		  </center>
	</div>
    <div class="clearfix">&nbsp;</div>
</div>

<div id="info_cook" style='display:none;'>
  <div class="closeCookiesPolicy" style='display:block;float:right;'><a href="#null">&nbsp;&nbsp;X&nbsp;&nbsp;</a></div>
<?php
include($path_raiz_includes_local . 'includes/cookies_footer_txt.php');
?>
    <div class="closeCookiesPolicy" style='display:block;float:right;font-size:60% !important;'><a href="#null">&nbsp;&nbsp;Cerrar&nbsp;&nbsp;</a></div>
</div>

<div id="footerWebPage" style="display:block;"></div>
