//Tipos de ClickToCall. CTC==máxima prioridad. Lead==formulario grande
var array_typeofctc = ["CTC", "LEAD"];

var whitespace = " \t\n\r";
var reWhitespace = /^\s+$/;
$.ajaxSetup({async:false});

function MM_openBrWindow(theURL, winName, features) { //v2.0
    checkCookie();
    window.open(theURL, winName, features);
}

function ponerPixelJS(pix,idProcess,isImage){
	if(!isImage)
	{
    var s = document.createElement("script");
    s.type = "text/javascript";
	}
	else
	{
		var s = new Image;
    s.width = "1px";
    s.height = "1px";
	}
	pix=pix.replace("[XXfeaturedparamXX]",idProcess);
	s.src = pix;

	$("body").append(s);
}

function includeJSScriptTradeContainer(pix){
	document.write(unescape("%3Cscript src='" + pix +" ' type='text/javascript'%3E%3C/script%3E"));
}

function __replaceall(msg, needle, reemp) {
    return msg.split(needle).join(reemp);
}

function _showModal(typ, cab, msg, fieldError){
	$.facebox.close();
	var alerta = (typeof is_movil !== undefined) && is_movil == '1';
	if(!alerta){
		showModal({
			title:cab
			,message: msg
			,type: typ
		});
	} else { //para moviles
		msg = __replaceall(msg, '<br />', "\n");
		msg = __replaceall(msg, '<br>', "\n");
		msg = __replaceall(msg, '<strong>', '');
		msg = __replaceall(msg, '</strong>', '');
		msg = __replaceall(msg, '<p>&nbsp;</p>', '\n');
		msg = __replaceall(msg, '<p>', '');
		msg = __replaceall(msg, '</p>', '\n');
		alert(msg);
		$('#'+fieldError).click().focus().focusin(function(){$('#'+fieldError).val("")});
	}
}

function clearForm() {
    $('#nombre').val('');
    $('#email').val('');
    $('#telefono').val('');
    //$('#apellidos').val('');
    $('select option:selected').removeAttr('selected');
    //$('#cp').val('');
    $("#cblegales").attr("checked", false);
    if(is_ctc == '1') {
        $("#telefono_ctc").val('');
    }
}

function ComprobarInsercion(dat, ind) {
    var data = eval("(" + dat + ")");
    var msg = "";
    var b_is_movil = (typeof is_movil !== undefined) && is_movil == '1';
    var v_tduid = (typeof tduid !== undefined) ? tduid : '';
    var pix = "";
    if(data.error == 0) { //no ha habido error. Le estamos llamando.
        clearForm();

		if (id_client == 9)
		{
			pix = '<img src=\"http://tbl.tradedoubler.com/report?organization=' + trdbl_organization + '&event=' + trbdl_event;
			pix += '&leadNumber=' + data.id + '\"/>'; //"&affiliate=" + id_source +
		}

		$('body').append(pix);
        //Ponemos el pixel dependiendo del click realizado
        if(cookiesEnabled == 1) {

            if(arr_pixel_JS[0] != "")
                ponerPixelJS(arr_pixel_JS[0],data.id,arr_pixel_JS[2]);

            if(arr_pixel_JS[1] != "")
                ponerPixelJS(arr_pixel_JS[1],data.id,arr_pixel_JS[2]);
            
						if(id_source == '2399134'){ //	INSERTADO EL 09.04.2014
								$('body').append('<img src="http://yourlondonbridge.go2cloud.org/SLQr?adv_sub='+data.id+'" width="1" height="1" />');
                                                      }

            //PIxel estático de retargeting
            pix = "http://wrap.tradedoubler.com/wrap?id=10009";
            ponerPixelJS(pix);
            //Modificado by LFP 2013.11.19 for Mario
//            $('body').append('<img src="https://reachandrich.antevenio.com/track/compginc.asp?exptime=90&keyword=SanitasDentalCliente=visita">');

						if(id_client == 9){	// 20140701 M.F.
							if(!window.mstag) mstag={loadTag:function(){},time:(new Date()).getTime()};
							includeScript("//flex.msn.com/mstag/site/c3e2496a-cd20-40a8-b693-d50bf352a3d5/mstag.js","js");
							mstag.loadTag("analytics",{dedup:"1",domainId:"3165988",type:"1",actionid:"250700"});
							$('body').append('<iframe src="//flex.msn.com/mstag/tag/c3e2496a-cd20-40a8-b693-d50bf352a3d5/analytics.html?dedup=1&domainId=3165988&type=1&actionid=250700" frameborder="0" scrolling="no" width="1" height="1" style="visibility:hidden;display:none" />');

							/*
							 * ADDED 2014.07.29
							 */
							if(id_source == "2422098")
							{
								includeScript(root_path_local + "js/google_"+id_source+"_"+id_client+".js","js");
							}
						}

					/*
					 * ADDED 2014.08.01
					 */
					if(id_source == "2422098" && id_client == 28)
					{
						includeScript(root_path_local + "js/google_"+id_source+"_"+id_client+".js","js");
					}
        }

        var txt = "<p>Hemos recogido tus datos <strong>correctamente</strong>.</p><p>En la mayor brevedad posible el equipo Comercial de " + nombCliente + " se pondrá en contacto contigo</p><p>Gracias por confiar en " + nombCliente + "</p><p>&nbsp;</p><p><strong>Servicio disponible de lunes a viernes laborables, de 10.00 a 21.00 horas.</strong></p>";
        _showModal('success', 'Datos recogidos correctamente', txt,"");
    } else {
        msg = 'Ha habido un error al procesar los datos.<br />Por favor, inténtelo más tarde.';
        if(data.mensaje == 'DUPLICADO') {
            msg = 'Ya existe éste teléfono en el sistema. <br /><br />Si desea ser contactado, por favor, déjenos su teléfono en el formulario superior.';
        } else if(data.mensaje == 'RECHAZADOWS') {

            msg = 'Usted ya es cliente de Jazztel , por favor póngase en contacto con Atención al cliente en el 1565';

        }
        _showModal('error', 'Error en el procesamiento de Datos', '<p>' + msg + '</p>',"");
    }
}



function isEmpty(s){
  return((s == null) || (s.length == 0));
}

function isWhitespace (s){
  return(isEmpty(s) || reWhitespace.test(s));
}

//función que validará el formulario grande
function validateForm(ind) {
    $.facebox.loading();

		var fieldError='';
    var ccrea = '';
    var nomb = '';
    var email = '';
    var telf = '';
    var prov = "";
    var error = 0;
    var msg = '';
    prov = $('#sel_prov').val();
    //modificado by LFP 2013.11.19 Mario
    var prov_text = $('#sel_prov').find('option:selected').text();
    var legales = ($("#cblegales").is(':checked')) ? '1' : '0';
    if(typeof nomb_promo === 'undefined') {
        ccrea = 'barclays_1';
    } else {
        ccrea = nomb_promo;
    }
    var source = '';
    if(typeof id_source === 'undefined') {
        source = '';
    } else {
        source = id_source;
    }
    nomb = $('#nombre').val();
    email = $('#email').val();
    var apell = $('#apellidos').val();
    telf = $('#telefono').val();
    if(ind == 0 || ind == 2) {
        telf = $('#telefono_ctc').val();
    }
    prov = $('#sel_prov').val();

    //Validamos ahora dependiendo del parámetro ind de entrada.
    if(ind == 1) { //formulario largo
		if(nomb.length < 1 || isWhitespace(nomb) || nomb == 'Nombre y apellidos'){
//        if(nomb.length < 1 || nomb != 'Nombre y apellidos') {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";

            if(!is_movil) $('#nombre').focus();
						fieldError=fieldError ? fieldError : 'nombre';
        }
        else if(!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            if(!is_movil) $('#nombre').focus();
						fieldError=fieldError ? fieldError : 'nombre';
        }
        /*if(apell.length<1) {error = 1;msg += "<p>* Debes rellenar el campo Apellidos</p>";$('#apell').focus();}
         else if(!(comprobarExprRegular(regexOnlyLetters,apell))) {error = 1;msg += "<p>* El campo Apellidos contiene caracters no válidos. Solo se permiten letras y espacios</p>";$('#apell').focus();}	
         */
        if(telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            if(!is_movil) $('#telefono').focus();
						fieldError=fieldError ? fieldError : 'telefono';
        }
        if(telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            if(!is_movil) $('#telefono').focus();
						fieldError=fieldError ? fieldError : 'telefono';
        }
        if(!(comprobarExprRegular(regExTelf, telf))) {
            error = 1;
            msg += "<p>* El número de teléfono solo admite 9 números sin espacio (p.ej 912345678 ó 612345678)</p>";
            if(!is_movil) $('#telefono').focus();
						fieldError=fieldError ? fieldError : 'telefono';
        }
				if(fieldError != 'telefono' && !error)	// COMPROBACION DEL NUMERO DE TELEFONO
				{
					$.ajax({
						url:root_path_local + "includes/phone_check.php",
						method:"post",
						dataType:"json",
						data:{telefono:telf,cr:idCr},
						cache:false,
						async:false,
						success:function(response)
						{
							if(response.success != true)
							{
								error=1;
								msg += "<p>* El número de teléfono no es correcto</p>";
								if(!is_movil) $('#telefono').focus();
								fieldError=fieldError ? fieldError : 'telefono';
							}
						}
						,error:function(response){error = 1;msg += "<p center style=color:red;>ERROR INESPERADO, Perdonad las molestias!</p>";}
					})
				}

        if((typeof is_prov !== 'undefined') && is_prov == 1 && prov <= 1) {
//        if(prov <= 0 || prov.length < 1) {
            error = 1;
            msg += "<p>* Debes seleccionar una Provincia</p>";
            if(!is_movil) $('#sel_prov').focus();
						fieldError=fieldError ? fieldError : 'sel_prov';
        }
        if(email.length < 1  || email == "Email") {
            error = 1;
            msg += "<p>* Debes rellenar el campo Email</p>";
            if(!is_movil) $('#email').focus();
						fieldError=fieldError ? fieldError : 'email';
        }
        else if(!(comprobarExprRegular(regExEmail, email)) && email != "Email"){
            error = 1;
            msg += "<p>* Formato de Email incorrecto.</p>";
            if(!is_movil) $('#email').focus();
						fieldError=fieldError ? fieldError : 'email';
        }

        /*if(cp.length<1) {error = 1;msg += "<p>* Debes rellenar el campo Código Postal</p>";$('#cp').focus();}
         else if(cp.length>5) {error = 1;msg += "<p>* El número de Código Postal solo puede tener 5 dígitos</p>";$('#cp').focus();}
         else if(!(comprobarExprRegular(regexCP,cp))) {error = 1;msg += "<p>* El Código Postal solo admite 5 números sin espacio (p.ej 28000 ó 08348)</p>";$('#cp').focus();}
         */
        if(legales != '1') {
            error = 1;
            msg += "<p>*Debes aceptar la Política de Privacidad</p>";
        }
    } else if(ind == 0) { //Formulario CTC
        if(telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            if(!is_movil) $('#telefono_ctc').focus();
						fieldError=fieldError ? fieldError : 'telefono_ctc';
        }
        else if(telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            if(!is_movil) $('#telefono_ctc').focus();
						fieldError=fieldError ? fieldError : 'telefono_ctc';
        }
    } else if(ind == 2) { //Formulario especial con teléfono, nombre y Legales

        if(nomb.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";
            if(!is_movil) $('#nombre').focus();
						fieldError=fieldError ? fieldError : 'nombre';
        }
        else if(!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            if(!is_movil) $('#nombre').focus();
						fieldError=fieldError ? fieldError : 'nombre';
        }
        if(telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            if(!is_movil) $('#telefono_ctc').focus();
						fieldError=fieldError ? fieldError : 'telefono_ctc';
        }
        else if(telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            if(!is_movil) $('#telefono_ctc').focus();
						fieldError=fieldError ? fieldError : 'telefono_ctc';
        }
        if(legales != '1') {
            error = 1;
            msg += "<p>*Debes aceptar la Política de Privacidad</p>";
        }
    } else if(ind == 3) { //Formulario especial con teléfono, nombre 
        //alert(ind);   
        if(nomb.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Nombre</p>";
            if(!is_movil) $('#nombre').focus();
						fieldError=fieldError ? fieldError : 'nombre';
        }
        else if(!(comprobarExprRegular(regexOnlyLetters, nomb))) {
            error = 1;
            msg += "<p>* El campo Nombre contiene caracters no válidos. Solo se permiten letras y espacios</p>";
            if(!is_movil) $('#nombre').focus();
						fieldError=fieldError ? fieldError : 'nombre';
        }
        if(telf.length < 1) {
            error = 1;
            msg += "<p>* Debes rellenar el campo Teléfono</p>";
            if(!is_movil) $('#telefono').focus();
						fieldError=fieldError ? fieldError : 'telefono';
        }
        else if(telf.length > 9) {
            error = 1;
            msg += "<p>* El número de teléfono solo puede tener 9 dígitos</p>";
            if(!is_movil) $('#telefono').focus();
						fieldError=fieldError ? fieldError : 'telefono';
        }
        //if(legales != '1') {error = 1; msg += "<p>*Debes aceptar la Política de Privacidad</p>";}
    }
    if(error != 0) {

        _showModal('error', 'Error en los Datos Introducidos', msg, fieldError);
    } else {//Mandamos.
        //modificado por LFP 2013.11.26.
        //Se requiere mandar como si fuese un LEAD (ind==1) y no como un CTC (ind==0)
        if(ind==3) {ind = 1;} 
        if(ind >= 2) {
            ind = 0;
        }
        var valortype = array_typeofctc[ind];
        var campana = arr_campanas[ind];
        //modificado by LFP 2013.11.19 Mario
        $.post(root_path_local + "ajaxs/procesar_registro_ctc.php", {idclient: id_client, sourcetype: valortype, campaign: campana, fuente: source, em: email, nombre: nomb, apellidos: apell, telefono: telf, provincia: prov, provincia_text: prov_text, cblegales: legales, crea: ccrea}, function(data) {
            ComprobarInsercion(data, ind);
        }, '');	//,em:email
    }

}
/**
 * Función que comprobará o gestionará la cookie dentro del formulario
 */
function checkCookie() {
    //alert("hey");
    if(cookiesManaged == 0) {
        manageCookies(1);
    }
}

$(document).ready(function() {
    $("body").append("<div id='opaque' style='display: none;'></div>");
    $('#telefono').keypress(function(e) {
        return SoloCompatibleTlf(e);
    });
    //Política de cookies
    $(':input').focusin(function(){
        if($(this).attr('id') == "cookie_rejection" || $(this).attr('id') == "cookie_acept")
		  return false;
        checkCookie();
    });
    $(':checkbox').click(function(e) {
        checkCookie();
    });
    if(is_ctc == '1') {
        $("#telefono_ctc").keypress(function(e) {
            return SoloCompatibleTlf(e);
        });
    }
    $('#btnProcesar').click(function(e) {
        e.preventDefault();
        validateForm(1);

    }); //btnprocesar
    $('#btnProcesar_ctc').click(function(e) {
        e.preventDefault();
        validateForm(0);

    }); //btnProcesar_ctc
    /**
     * Nombre y teléfono
     */
    $('#btnProcesar_ctc_movil').click(function(e) {
        e.preventDefault();
        validateForm(3);

    });

});

var regExTelf = /([9|6|7|8])+[0-9]{8}/;// /^[6-7]{1}\d{8}$/
var regexOnlyLetters = /^[ñÑáÁéÉíÍóÓúÚçÇa-zA-Z\ \']+$/;
var regexCP = /^[0-9]{4,5}/;
var regExEmail = /^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/;	