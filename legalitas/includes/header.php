<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
if($is_movil == 1 || $layoutType){
?>
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="Pragma" content="no-cache">
<?php
}
$prefixCss=($layoutType == 'mob_') ? $inbound_prefix . $layoutType : '';
?>
<title><?php echo $arr_creas[$id_crea]['title']; ?></title>

<link href="<?=$path_raiz_aplicacion?>js/modal/css/modal.css" rel="stylesheet" type="text/css" />
<link href="<?=$path_raiz_aplicacion?>js/facebox/facebox.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$path_raiz_aplicacion?>js/window/css/window.css" rel="stylesheet" type="text/css" />

<link href="<?=$path_raiz_aplicacion_local?>css/common.css" rel="stylesheet" type="text/css" />
<link href="<?=$path_raiz_aplicacion_local?>css/<?=$prefixCss?>css_<?=$id_crea?>.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
var root_path			= "<?php echo $path_raiz_aplicacion;?>";<?php /*ruta global para las imgs del facebox y del modal*/?>
var root_path_local		= "<?php echo $path_raiz_aplicacion_local;?>";<?php /*ruta global para ajax*/?>
var nomb_promo			= "<?php echo $arr_creas[$id_crea]['nombpromo']; ?>";
var id_source			= "<?php echo trim($id_source); ?>";
var id_client			= "<?php echo $id_client; ?>";
var idCr			= "<?php echo $id_crea; ?>";	// (addesd 13.06.2014)

var arr_campanas		= [<?php  echo $campanas; ?>];
var is_ctc				= "<?php echo trim($is_ctc); ?>";
var is_movil			= "<?php echo trim($is_movil); ?>";
var root_path_ws		= "<?php echo $path_raiz_ws;?>";
var url_ws				= "<?php echo $url_ws;?>";
var file_ws				= "<?php echo $file_ws;?>";
var arr_pixel_JS		= ["<?=$arr_pixel_JS[0];?>","<?=$arr_pixel_JS[1]?>","<?=$arr_pixel_JS[2]?>"];
var tduid				= "<?php echo $tduid; ?>";
var trdbl_organization	= "<?php echo $organization; ?>"; //Pixel Tradedoubler. Organizacion
var trbdl_event			= "<?php echo $event; ?>";
var trbdl_program		= "<?php echo $trbdl_program; ?>";
var nombCliente			= "<?php echo $nombCliente; ?>";
var is_prov				= "<?php echo trim($is_prov); ?>";
var googleSEM = "";
var googleRemarketingOnly = false;	// FOR COMPATIBILITY 2014 js
var googleSEO = 0;
var google_conversion_id;
</script>

<?php
if($layoutType){
?>
<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/jquery-1.10.1.min.js"></script>
<?php
}
else
{
?>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/jquery-1.9.1.min.js"></script>
<?php
}
?>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/modal/js/jquery.simplemodal.1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/facebox/facebox.min.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/window/js/window.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/funciones_comunes.js"></script>
<?php
//if(!empty($arr_creas[$id_crea]['js']) && trim($arr_creas[$id_crea]['js']) != '' ){
if(!$layoutType){	// PARA CREAS WEB
?>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion_local;  ?>js/<?php echo trim($arr_creas[$id_crea]['js']); ?>"></script>
<?php

} else {	// SOLO PARA CREAS MOBILE, + NUEVO CONTROL JAVASCRIPT DE LOS CAMPOS POR VALIDATE
?>
<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion_local;?>js/validacion.min.js"></script>

<?php
}

if($layoutType && $is_prov)  // GESTION MODAL "LLAMAME"
{
  echo '
<style>
.modalCllame{
display:block;
width:100%;
left:0px!important;
color:#009CD9;
}

.closeModalCall{
color:#000;
text-decoration:none;
}
.callMeText{
font-size:150%;
font-weight:bold;
}
.modalCloseImg{display:none!important;}

div.error{ display : none; border:2px solid #D81E05; }
.error{color:#D81E05; background:#FCF1F0;}
div.error ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error ul li{margin:0 0 10 0;list-style-type:none;}
div.error ul li label{font-weight:normal}
</style>';
}
?>

<script type="text/javascript">
var isModalDraw=false;
var leadNumber = Math.round((new Date().getTime() * Math.random()));
function offModal(){$.modal.close();} // CLOSE MODAL "LLAMAR"

$(document).ready(function(){
<?php
// START GESTION MODAL "LLAMAME" (ONLY IF MOBILE IS ENABLED)
if($layoutType && $is_prov)
{
  // PARA EVITAR ENVIAR LA LLAMADA AL SERVIDOR DE tradedoubler EN PRUEBAS
  $serverTradedoubler=$sitesPruebas ? 'http://139.162.246.12/netsalesfactory.com/promociones/sanitas-new-web/img/p.gif?' : 'http://tbl.tradedoubler.com/report?organization=1782746&event=307764&leadNumber=';
?>
  $("body").click(function(){

	if(!isModalDraw)  // CLICK ONLY ONCE
	{
	  $.modal('<div style="display:inline-block;background:#fff;text-align:center;width:300px;"><center><br /><span class="callMeText">¡Inf&oacute;rmate AHORA!<br />Llama GRATIS</span><br /><br /><a id="callMeHref" href="tel:900834152"><img class="info callProceed" src="<?=$path_raiz_aplicacion_local?>img/btn_call_me.png" border="0" style="width:70%;" /></a><span style="display:block;margin-top:8px;margin-bottom:4px;"><a class="closeModalCall" href="javascript:offModal();">continuar</a></span></center></div>',{
		  containerCss:{
			  backgroundColor:"#333",
			  borderColor:"#333",
			  width:"320px",
			  height:"215px",
			  close:true,
			  padding:0
		  },
		  onClose:function(dialog){
			dialog.data.fadeOut('fast',function(){
				dialog.container.hide('slow',function(){
					dialog.overlay.slideUp('fast',function(){
						$.modal.close();
					});
				});
			});
		  },
		  overlayClose:true
	  });
	  isModalDraw=true;
	}

	  $(".callProceed").click(function(){ // CALL PIXEL & CLOSE MODAL
		setTimeout("var pxTrade=new Image();pxTrade.src='<?=$serverTradedoubler?>"+leadNumber+"';",100);
		offModal();
	  });
	});

  setTimeout('$("body").click()',3000);

<?php
}
// END GESTION MODAL "LLAMAME"
?>
	$("#msgLocalWelcome").delay(4000).hide("slow");

});
</script>

</head>
