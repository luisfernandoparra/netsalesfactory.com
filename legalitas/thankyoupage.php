<?php
/**
 * Página de gracias
 */
@session_start();
include('../conf/config_web.php');
ini_set('display_errors',0);
include('conf/config_web.php');
include($path_raiz_includes . 'includes/initiate_thankyoupage.php');
include($path_raiz_includes . 'class/class.pixels.management.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>¡Gracias de <?php echo ucwords($landingMainName); ?>!</title>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width,initial-scale=1"/> 
<link href="<?php echo $path_raiz_aplicacion_local; ?>css/estilos_thanks.css" rel="stylesheet" type="text/css">
</head>
<body>

<header>
	<section class="cabecera">
    <img class="logo" src="<?php echo $path_raiz_aplicacion_local; ?>img/logo-legalitas.png" width="285" height="67" alt=""/> 
    </section>
</header>

<section class="contenido_basico">
	<h1>Formulario enviado correctamente</h1> 
	<img class="ico" src="<?php echo $path_raiz_aplicacion_local; ?>img/tick.png" width="90" height="78" alt=""/> 
    
    <h2>¡Muchas gracias por confiar en Legálitas!</h2>
    
    <p>En breve nos pondremos en contacto contigo.</p>
    
	<img class="photo" src="<?php echo $path_raiz_aplicacion_local; ?>img/img_web.png" width="677" height="440" alt=""/> 
</section>

<footer></footer>
<?php
if (!empty($_REQUEST['debug'])) {
    new dBug($_REQUEST);
}
/* $id_crea=25;
  $leadNumber = 'test-d6t345';
  $saltarPixeles = true;
  $cookieEnable = true;

echo 'id_crea: ' . $id_crea;
echo ' salta pixel: ' . $saltarPixeles;
new dBug($_REQUEST);
 *  */
 
include($path_raiz_includes . 'includes/pixels_thankyoupage.php');
session_destroy();
?>
</body>
</html>