<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);

?>
					<div id="campos">
						<div class="error"><ul></ul></div>
						<p>
							<span class="titlle-form">¿Qu&eacute; necesitas?</span><br />
							D&eacute;janos tus datos,<br /><span>te llamamos nosotros
							</span>
						</p>

						<div class="fila">
							<p class="celda-name">Nombre*</p>
							<div class="fleft">
								<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" placeholder="" aria-required="true" />
							</div>
						</div>

						<div class="fila">
							<p class="celda-name">Apellidos*</p>
							<div class="fleft">
								<input type="text" name="apellidos" id="apellidos" value="<?=$apellidos?>" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Apellidos&lt;/strong&gt; es obligatorio" placeholder="" aria-required="true" />
							</div>
						</div>

						<div class="fila">
							<p class="celda-name">Tel&eacute;fono*</p>
							<div class="col8">
								<input type="tel" maxlength="9" name="telefono" id="telefono" class="celda" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="" aria-required="true" />
							</div>
						</div>

						<div class="row">
							<p class="celda-name">Email*</p>
							<div class="col8">
								<input type="email" name="email" id="email" maxlength="100" class="celda" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="" aria-required="true" />
							</div>
						</div>

                        <div class="fila">
                        <p class="celda-name">Servicio*</p>
							<div class="col8">
								<select class="celda-servicio" type="text" name="service" id="service" maxlength="100" value="Servicio" required data-msg-required="El campo &lt;strong&gt;Servicio&lt;/strong&gt; es obligatorio" placeholder="" aria-required="true">
									<option value=""></option>
									<option value="1">FONTANEROS</option>
									<option value="2">ELECTRICISTAS</option>
									<option value="3">CERRAJEROS</option>
									<option value="4">CLIMATIZACI&Oacute;N</option>
									<option value="5">CALEFACI&Oacute;N</option>
									<option value="6">PINTORES</option>
									<option value="7">REFORMAS</option>
									<option value="8">ELECTRODOM&Eacute;STICOS</option>
									<option value="9">REPARAR PERSIANAS</option>
									<option value="10">CRISTALEROS</option>
									<option value="11">CARPINTEROS</option>
									<option value="12">TOLDOS Y P&Eacute;RGOLAS</option>
									<option value="13">REPARAR PARQUET</option>
									<option value="14">ALBA&Ntilde;ILES</option>
									<option value="15">MANITAS</option>
								</select>
							</div>
						</div>

            <div class="fila">
							<p class="celda-name">Comentarios*</p>
							<div class="col8">
								<textarea required="" class="celda-area" id="comments" name="comments" data-msg-required="El campo &lt;strong&gt;Comentarios&lt;/strong&gt; es obligatorio" data-msg-comments="Debes dejar un comentario adicional" ></textarea>
							</div>
						</div>

						<div class="legal" style="padding-top:10px;">
							<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;He le&iacute;do y acepto la
							<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'privacy_policies.php';?>" data-ref="" target="_blank">política de privacidad</a>
							<br /><br />
							<input data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cbcommercialuse" id="cbcommercialuse" value="1" aria-required="true">&nbsp;&nbsp;No quiero recibir comunicaciones comerciales
						</div>

						<div class="row">
							<div class="espacio_btn">
								<input class="button green sendData" type="button" id="btnProcesar" name="btnProcesar" value="Pedir presupuesto">
							</div>
						</div>

						<div class="clearfix"></div>
					</div>
					<!--/#campos-->
