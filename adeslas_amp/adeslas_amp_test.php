<?php
@session_start();
header('Access-Control-Allow-Origin: *');
header('AMP-Access-Control-Allow-Source-Origin: '.$_SESSION['access_Control_Allow_Source_Origin']);
include('../conf/config_web.php');
include('conf/config_web.php');
ini_set('display_errors',$paintErrors);
$resPhoneChecked=false;
$resEmailChecked=false;

if(count($_REQUEST))
  $arraExtVars=$_REQUEST;
elseif(isset($_GET))
		$arraExtVars=$_GET;
else
  $arraExtVars=$_POST;
 

$contentsFile="\r\n".'['.date('d-m-Y H:i:s').'] crea_id: '.@$arraExtVars['ref_crea'].(isset($arraExtVars['telefono']) && trim($arraExtVars['telefono']) ? ', phone: '.$arraExtVars['telefono'] : '').(isset($arraExtVars['fuente']) && trim($arraExtVars['fuente']) ? ', fuente: '.$arraExtVars['fuente'] : '').'';
$filelOG=$path_raiz_includes.$subFolderLanding.'/logs/log_'.date('d-m-Y').'.log';
//echo $filelOG;die();

// LOG DEL (INTENTO DE) REGISTRO DEL FORMULARIO
$fp=@fopen($filelOG,'a') or die('Unable to open file!');
$line = $contentsFile;
$raw=$error;
$error=fwrite($fp, $line);
fclose($fp);

if(isset($arraExtVars['telefono']) && trim($arraExtVars['telefono']) && !$resPhoneChecked)
{
	//******************************
	// START PHASE 1 ---- PHONE CHECK
	$configLoaded=true;
	$fields['telefono']=$arraExtVars['telefono'];
	$fields['cr']=@$arraExtVars['ref_crea'];
	$qs = http_build_query($fields);
	include($path_raiz_includes.$subFolderLanding.'/includes/phone_check.php');

	// LOG DEL RESULTADO DEL CHECK TELEFONICO
	$fp=@fopen($filelOG,'a') or die('Unable to open file!');
	$line = ($res['success'] ? ', check_tel: OK' : ', check_tel: ERROR');
	$raw=$error;
	$error=fwrite($fp, $line);
	fclose($fp);
	$resPhoneChecked=true;
	// END PHASE 1 ---- PHONE CHECK
	//******************************

	//******************************
	// START PHONE CHECK ERROR
	if(!$res['success'])
	{
		$response['response']['success']=0;
		$response['response']['title']='HA OCURRIDO UN ERROR';
		$response['response']['msg']='Parece que el teléfono '.@$arraExtVars['telefono'].' es incorrecto';
		$res=json_encode($response);
		die($res);
	}
	// END PHONE CHECK ERROR
	//******************************

	//******************************
	// START SEND DATA
	if($res['success'])
	{
		$arrFields=array(
			'idclient' => (int)$id_client,
			'cli' => (int)$id_client,
			'sourcetype' => $arrSourceTypes[$isCtc],
			'campaign' => $arr_campanas[$isCtc],
			'fuente' => (isset($arraExtVars['fuente']) ? $arraExtVars['fuente'] : ''),
			'prioridad' => (int)@$prioridad,	// ---> REVISAR
			'nombre' => (isset($arraExtVars['nombre']) ? $arraExtVars['nombre'] : ''),
			'apellidos' => (isset($arraExtVars['apellidos']) ? $arraExtVars['apellidos'] : ''),
			'crea' => (isset($arraExtVars['ref_crea']) ? $arraExtVars['ref_crea'] : ''),
			'province' => (isset($arraExtVars['provincia']) ? $arraExtVars['provincia'] : ''),
			'telefono' => (isset($arraExtVars['telefono']) ? $arraExtVars['telefono'] : ''),
			'email' => (isset($arraExtVars['email']) ? $arraExtVars['email'] : ''),
			'cblegales' => (isset($arraExtVars['cblegales']) ? $arraExtVars['cblegales'] : ''),
			'vcc' => 'fintel',
			'pwd' => $secret_pwd,
			'usr' => $secret_usr,
			'use_priority' => (!empty($arraExtVars['use_priority']) && is_numeric($arraExtVars['use_priority']) && $arraExtVars['use_priority'] == 1 && @$prioridad != '') ? 1 : 0
		);

		if (!empty($_SESSION['ws_ip_id']) && is_numeric($_SESSION['ws_ip_id']))
			$arrFields['idIp'] = $_SESSION['ws_ip_id'];

		$qs = http_build_query($arrFields);
		$url = $url_ws . $file_ws . '?' . $qs;

		$conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);

		if(!$conexion->connectDB()){
			$response['response']['success']=0;
			$response['response']['title']='HA OCURRIDO UN ERROR';
			$response['response']['msg']='No se ha podido conectar a la BBDD. Error 102';
			$res=json_encode($response);
			die($res);
		}

    $cur_conn_id = $conexion->get_id_conexion();
    $nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
    $nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');

    $telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
    $query='INSERT INTO %s (id_cliente,nombre,apellidos,id_crea,telefono,id_provincia,b_legales,email,ip) VALUES (\'%d\',\'%s\',\'%s\',\'%d\',\'%s\',\'%d\', %d,\'%s\',\'%s\');';
    $query=sprintf($query, $table_registros, (int)$arrFields['idclient'], $arrFields['nombre'], $arrFields['apellidos'],$arrFields['crea'], $arrFields['telefono'], (int)$arrFields['province'], (int)$arrFields['cblegales'], $arrFields['email'],@$_SERVER['REMOTE_ADDR']);
    $conexion->ejecuta_query($query);
    $id = mysql_insert_id();

    $ch = curl_init();
    $timeout = 0; // set to zero for no timeout
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $raw = curl_exec($ch);
    curl_close($ch);
    $resCurl=json_decode($raw);

//echo $url."\n res=";print_r($raw);
//echo $url."\n raw=";print_r($resCurl);
//die();

		// RESPONSE CURL SUCCESS OK
		if(@$resCurl->error === 0)
		{
			$response['response']['title']='GRACIAS POR PARTICIPAR';
			$response['response']['success']=1;
			$response['response']['msg']='Hemos recibido correctamente los datos, en breve nos pondremos en contacto.';
		}
		else
		{
			$response['response']['title']='HA OCURRIDO UN ERROR AL PROCESAR LOS DATOS';
			$response['response']['success']=0;
			$response['response']['msg']='Por favor, inténtalo más tarde';
		}

		$res=json_encode($response);
		die($res);
	}
	// END SEND DATA
	//******************************


//echo"\n resCurl";print_r($objeto);die('fin');
}


//$response['response']['success']=true;
//$response['response']['phone']=@$arraExtVars['telefono'];
//$response['response']['scriptName']='adeslas_amp_test :: '.date('d-m-Y H:i:s');
//$response['response']['msg']='Este es el texto del mensaje devuelto en formato SIMPLE';
//$response['response']['res']=true;
//$res=json_encode($response);
//die($res);
?>