body{
font-family:Helvetica, Arial Text, Liberation Serif, Times, Times New Roman, serif;
background-color:#FFFFFF;
}

.header{
width:99%;
max-width:750px;
margin:auto;
}

.logo{
max-width:310px;
width:40%;
display:inline-block;
vertical-align:middle;
margin:0.6em 0 0.6em 0.6em;
}

.header h1{
display:inline-block;
vertical-align:middle;
font-size:1.1em;
font-weight:400;
color:#a3bf2a;
text-align:left;
margin-left:10.20em;
}

.skeleton{
width:100%;
max-width:750px;
margin:auto;
}

.formulario{
width:98%;
max-width:750px;
margin:auto;
background-color:#FFFFFF;
border:2px solid #a3bf2a;
box-sizing:border-box;
}

section.formulario p{
line-height:1px;
}

.l-text{
font-size:1.6em;
text-align:center;
color:#a3bf2a;
font-weight:lighter;
}

.b-text{
font-size:2em;
text-align:center;
color:#a3bf2a;
font-weight:bold;
}

.celda{
background-color:#FFF;
font-size:.9em;
display:block;
width:39%;
min-height:40px;
margin:-0.2em auto 0 auto;
text-align:left;
border: 1px solid #a3bf2a;
padding-left:1em;
font-size:1.1em;
color:#000000;
-webkit-appearance: none;
-webkit-border-radius:0;
-moz-border-radius:0;
border-radius:0;
}

.button{
width:41.70%;
color:white;
padding:0.3em 0;
margin:0.6em auto 0.6em auto;
display:block;
border: 0;
font-size:2.1em;
background-color:#EF8900;
margin-bottom:0.3em;
cursor:pointer;
font-weight:bold;
-webkit-appearance: none;
-webkit-border-radius:0;
-moz-border-radius:0;
border-radius:0;
font-weight:bold;
}

.legal{
font-size:.7em;
width:43%;
text-align:left;
color:#000000;
margin:1em auto -1em auto;
}

.legal input{
vertical-align:middle;
}

.legal a{
color:#000000;
}

.text-content{
width:99%;
max-width:750px;
margin:auto;
background-color:#fafafa;
text-align:center;
padding-top:1.5em;
}

.text-content amp-img{
max-width:75px;
margin: auto;
}

.text-content h2{
font-size:1.5em;
font-weight:bold;
text-align:center;
color:#009ddd;
margin-top:0.04em;
}

.text-content p{
color:#000000;
font-size: 0.9em;
margin-top:-0.9em;
}

.legal-box{
text-align:left;
padding:3em 0;
margin-left:2em;
}

.legal-box a{
color:#000000;
}

footer{
width:99%;
max-width:750px;
margin:-1em auto;
height:40px;
background-color:#009ddd;
}

@media (max-width: 760px){

.header h1{
font-size:1em;
margin-left:5em;
}
}

@media (max-width: 565px){

.l-text{
font-size:1.2em;
}

.b-text{
font-size:1.6em;
}

.celda{
width:70%;
}

.button{
width:75%;
}

.formulario{
width:100%;
}

.legal{
width:75%;
}

.legal-box{
margin-left:0.8em;
}
}

@media (max-width: 550px){

.header h1{
font-size:0.8em;
margin-left:2em;
}

.logo{
margin:0.6em 0 0em 0.6em;
}
}

@media (max-width: 480px){

.text-content h2{
font-size:1.3em;
}

.text-content p{
font-size:0.8em;
}
}

@media (max-width: 400px){
	.header h1{
	font-size:0.7em;
	margin-left:1em;
	}

	.l-text{
	font-size:1em;
	}

	.b-text{
	font-size:1.4em;
	}

	.legal{
	width:78%;
	}

	.text-content h2{
	font-size:1.1em;
	}

	.text-content p{
	font-size:0.75em;
	}
}


form.amp-form-submit-success [submit-success],
form.amp-form-submit-error [submit-error]{
text-align:center;
margin-top: 16px;
}
form.amp-form-submit-success [submit-success] {
color: green;
}
form.amp-form-submit-error [submit-error] {
background-color: red;
color: #fff;
}
form.amp-form-submit-success.hide-inputs > input {
display: none
}
.horizontal-display {
display: flex;
align-items: center;
}
.other-input {
margin: 0 16px;
}