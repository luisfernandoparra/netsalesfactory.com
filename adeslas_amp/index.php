<?php
/*
 * NOTAS IMPORTANTES:
 * PARA TODAS LAS CREATIVIDADES "responsive" Y "mobile"
 * EL SISTEMA JS DE CONTROL DE CAMPOS SE EFECTUA POR VALIDATE DE JQUERY
 * EXCEPCIONES: TODAS LAS PROMOS WEB VIEJO MODELO Y FORMULARIOS CON IFRAMES
 *
 * PARAMETROS CONDICIONANTES
 *
 * forzarmovil = 1 PARA FORZAR LA PAGINA VISTA DESDE UN MOVIL EN UN DESKTOP PC ($layoutType)
 * inbound = 1 (+ forzarmovil = 1 DESDE PC) FORZAR PROMO INBOUND
 * ctc = 1 FORZAR CLICK TO CALL
 */
$secure = true;
session_set_cookie_params($secure);
@session_start();
$blockBodySEM='';	// SOLO PARA LANDINGS CON GOOGLE SEM (M.F. 20140704)

if(empty($_SESSION['namePromo']))
{
	$tmpName=session_name('petite_parisienne'.date(Ymdis).rand(10,99));
	$_SESSION['namePromo']=session_name();
	$_SESSION['sessionCountPromo']=-1;
	$_SESSION['idCreatividad']=@$_REQUEST['cr'] ? @$_REQUEST['cr'] : @$id_crea;
}

include('../conf/config_web.php');
ini_set('display_errors',$paintErrors);
include('conf/config_web.php');
include($path_raiz_includes.'includes/initiate.php');
include($path_raiz_includes_local.'includes/local_vars.php');
$layoutType=null;
$id_crea= $id_crea ? $id_crea : $objSiteData->landingId;

if(isset($_REQUEST['ref_crea']))
{
	echo '<pre>';
	print_r($_REQUEST);
	echo '</pre>';
}

?>
<!doctype html>
<html amp lang="es">
<?php
/*
 * SE EFECTUA EL LOG (COMPLETO) SOLO PRIMER ACCESO
 */
if($_SESSION['sessionCountPromo'] == 0)
{
	if(isset($_SESSION['deviceMobileData']))
	{
		$query='INSERT INTO %s (session_id,fecha,ip_usuario,script,accion,http_referer,user_agent,id_crea,device_type,device_os,device_browser,action_number)VALUES(\'%s\',NOW(),\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',%d,\'%s\',\'%s\',\'%s\',%d)';
		$query=sprintf($query,$table_front_actions,$_SESSION['namePromo'],$_SERVER['REMOTE_ADDR'],$pagToLog,$laAccion,$_SERVER['HTTP_REFERER'],$_SERVER['HTTP_USER_AGENT'],(int)$_REQUEST['cr'],$_SESSION['deviceMobileData'][0],$_SESSION['deviceMobileData'][1],$_SESSION['deviceMobileData'][2],(int)$_SESSION['sessionCountPromo']);
	}
	else
	{
		$query='INSERT INTO %s (session_id,fecha,ip_usuario,script,accion,http_referer,user_agent,id_crea,action_number)VALUES(\'%s\',NOW(),\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',%d,%d)';
		$query=sprintf($query,$table_front_actions,$_SESSION['namePromo'],$_SERVER['REMOTE_ADDR'],$pagToLog,$laAccion,$_SERVER['HTTP_REFERER'],$_SERVER['HTTP_USER_AGENT'],(int)$id_crea,(int)$_SESSION['sessionCountPromo']);
	}
	$conexion->ejecuta_query($query);
}


if(@$objSiteData->landingId)	// PARA PROMOS BASADAS EN BBDD
	$arr_creas[$id_crea]['js']='';

//	START EXCEPCION PARA PERMITIR COMPONER LA PROMO CORRECTAMENTE EN EL IFRAME DE LOS SORTEOS, GTP (M.F. 20140226)
$layoutSorteo='';
$arraySorteos=array();

if(isset($_SERVER['HTTP_REFERER']) && in_array($_SERVER['HTTP_REFERER'],$arraySorteos))
{
  $layoutSorteo='sorteoIframe';
}
//	END EXCEPCION PARA PERMITIR COMPONER LA PROMO CORRECTAMENTE EN EL IFRAME DE LOS SORTEOS

if($layoutType)
{
	$is_movil=1;
}

// START EXCEPCION PARA LA IMAGEN DE LA CABECERA PARA UNA FECHA EN UNAS DETERMINADAS HORAS Y LA MODAL DE "LLAMAR"
$mobileImagePostName='';


include($path_raiz_includes_local . 'includes/header_tmplt'.'.php');
?>


<?php
echo $blockBodySEM;

if($arr_creas[$id_crea]['new_template'])
{
	$condicionesPromo=$arr_creas[$objSiteData->landingId]['condiciones'];
	$privacyPolicy=$arr_creas[$objSiteData->landingId]['protecciondatos'];

	/*
	 * SE DETERMINA SI EL VALOR $condicionesPromo ES UN ARCHIVO
	 * O UN TEXTO ALMACENADO EN LA BBDD
	 */
	if(strlen($condicionesPromo) > 100 && !strripos($condicionesPromo,'.php'))	// ESTE VALOR NO PERTENECE A UN ARCHIVO FISICO
		$condicionesPromo=$path_raiz_aplicacion_local.'includes/landing_conditions.php?cr='.(int)$id_crea;	// SE CARGA EL SCRIPT CORRESPONDIENTE PARA DIBUJAR EL TEXTO PRESENTE EN LA BBDD
	else
		$condicionesPromo=$path_raiz_aplicacion_local.$condicionesPromo;

	if(strlen($privacyPolicy) > 100 && !strripos($privacyPolicy,'.php'))	// ESTE VALOR NO PERTENECE A UN ARCHIVO FISICO
		$privacyPolicy='includes/privacy_policies.php?cr='.(int)$id_crea;	// SE CARGA EL SCRIPT CORRESPONDIENTE PARA DIBUJAR EL TEXTO PRESENTE EN LA BBDD

	$privacyPolicy=$privacyPolicy ? $path_raiz_aplicacion_local.$privacyPolicy : $path_raiz_aplicacion_local.'proteccion_datos.php';

	/*
	 * INCLUDES PARA EL CONTENIDO PRINCIPAL DE LA PROMO
	 * MÁS EL ARCHIVO cookies_footer
	 */
	include($path_raiz_includes_local . 'includes/'.$arr_creas[$id_crea]['body']);
	include($path_raiz_includes_local . 'includes/cookies_footer.php');
}
else
{
  include($path_raiz_includes_local . 'includes/'.$layoutType.$arr_creas[$id_crea]['body']);
  include($path_raiz_includes_local . 'includes/'.$layoutType.'cookies_footer.php');
}
?>
</body>
</html>
