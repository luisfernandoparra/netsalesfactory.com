<?php
/*
 * REMOTE PHONE NUMBER CHECK
 * 
 * @date 2016.09.26 Forzamos a que la cabecera de respuesta sea json
 */
@session_start();
header("Content-type: application/json; charset=utf-8");
//$paintErrors=true;//$xx=ini_get_all();//echo '<pre>';print_r($xx);

$debug = (!empty($_REQUEST['debug']));
if ($debug)
ini_set('display_errors',$paintErrors);

if(empty($_SESSION['namePromo']))
{
	$res['success']=false;
	$res=json_encode($res);
	die($res);
}

$checkEmail=$_REQUEST['email'];

$sanitizedData=filter_var($checkEmail, FILTER_SANITIZE_EMAIL);

$res=array();
$_SESSION['skipLogAction']=true;	// EVITA LOG INNECESARIO

if(empty($configLoaded))
{
	include('../../conf/config_web.php');
	include('../conf/config_web.php');
}
$_SESSION['skipLogAction']=false;	// SE REACTIVAN LOS LOGS BBDD

if(@$arr_creas[(int)$_REQUEST['cr']]['skip_check_phone'])	// SE OMITE EFECTUAR EL CHECK DEL TELEFONO
{
	$res['success']=true;
	$laAccion='phone-check-skipped';
}
else
{
	include($path_raiz_includes.'includes/check_phone.class.php');

	$telf = 0;
	if (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) {
			$telf = (int)trim($_REQUEST['telefono']);
	} elseif (!empty($_REQUEST['telefono_ctc']) && is_numeric($_REQUEST['telefono_ctc'])) {
			$telf = (int)trim($_REQUEST['telefono_ctc']);
	}

	if((int)$telf != $telf)
	{
		$res=$response['success'];
		$res=json_encode($res);
		die($res);
	}
//echo$telf."\n ===> request=";print_r($_REQUEST);print_r($_SESSION);die();

	$phoneToCheck=str_replace($arr_sql_inject_original, $arr_sql_inject_cambio, $telf);
	//$phoneToCheck=$phoneToCheck ? $phoneToCheck : str_replace($arr_sql_inject_original, $arr_sql_inject_cambio, (int)$_REQUEST['telefono_ctc']);
	$objeto=new remoteCheckPhone();
	$objeto->landingMainName=$landingMainName;
//echo$phoneToCheck."\n request=";print_r($_REQUEST);print_r($objeto);die();
	$objeto->phoneCheckUserAccess=$phoneCheckUserAccess;
	$objeto->phoneCheckPasswordAccess=$phoneCheckPasswordAccess;
	$objeto->phoneCheckUrlTarget=$phoneCheckUrlTarget;
	$res['success']=$objeto->phoneCheckRemote($phoneToCheck) ? true : false;	// BRIDGE TO CHECK PHONE NUMBER;
if(@$debug)
	echo ($objeto->phoneCheckRemote($phoneToCheck));
	$res['errorMessage']='Parece que el número <b>'.$phoneToCheck.'</b> no es correcto.';

//	$laAccion=$res['success'] ? 'phone-check-OK;'.$_REQUEST['telefono'].';'.$objeto->idChekTel : 'phone-check-ERROR;'.$_REQUEST['telefono'].';'.$objeto->idChekTel;
	$laAccion=$res['success'] ? 'phone-check-OK;'.$phoneToCheck.';'.$objeto->idChekTel : 'phone-check-ERROR;'.$phoneToCheck.';'.$objeto->idChekTel;
}

$table_front_actions=$prefixTbl.'log_web_actions'.date('Y');
/*
 * start LOG ACTIONS
 */
if(!empty($_SESSION['namePromo']))
{
	$pagToLog=$_SERVER['PHP_SELF'];
	$pagToLog=substr($pagToLog,strrpos($pagToLog,'/')+1);
	$pagToLog=substr($pagToLog,0,strpos($pagToLog,'.'));
	$laAccion=$laAccion ? $laAccion : '?';

	$query='INSERT INTO %s (session_id,fecha,ip_usuario,script,accion,id_crea,action_number)VALUES(\'%s\',NOW(),\'%s\',\'%s\',\'%s\',%d,%d)';
	$query=sprintf($query,$table_front_actions,$_SESSION['namePromo'],$_SERVER['REMOTE_ADDR'],$pagToLog,$laAccion,$idLandingCr,$_SESSION['sessionCountPromo']);
	$conexion->ejecuta_query($query);
}
	$_SESSION['sessionCountPromo']++;
/*
 * end LOG ACTIONS
 */

//$res=json_encode($res);
//die($res);
?>