<?php
@session_start();
$idLandingFormatted = str_pad($objSiteData->landingId, 3, '0', STR_PAD_LEFT);
$rutaImgs = $path_raiz_aplicacion_local . 'img/' . $idLandingFormatted . '/';
$defaultLocalPhoneCall = '';
$inbound_phone = ($inbound_phone) ? $inbound_phone : ''; // SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);

function obtenerNavegador2015($user_agent=0)
{
 $browsers='mozilla msie gecko firefox konqueror safari netscape navigator opera mosaic lynx amaya omniweb chrome';
 $browsers=@split(' ',$browsers);
 $nua=strToLower($_SERVER['HTTP_USER_AGENT']);
 $l=strlen($nua);
 for($i=0;$i<count($browsers);$i++)
 {
  $browser=$browsers[$i];
  $n=stristr($nua,$browser);
  if(strlen($n)>0)
  {
   $verNav = '';
   $navX = $browser;
   $j=strpos($nua, $navX)+$n+strlen($navX)+1;
   for(; $j<=$l; $j++)
   {
    $s=substr($nua,$j,1);
    if(is_numeric($verNav.$s))
    $verNav.= $s;
    else
    break;
   }
  }
 }
 if(!$navX) $navX=$_SERVER['HTTP_USER_AGENT'];
 $naveg=array(
	 'all'=>$navX.' '.$verNav,
	 'ver'=>$verNav,
	 'name'=>$navX
 );
 return $naveg;
}

$elNavegador=obtenerNavegador2015($_SERVER['HTTP_USER_AGENT']);
$userNavigator=$elNavegador['name'];
?>

<section class="header">

<?php
//echo$path_raiz_includes_local;//.'<pre>';print_r($objSiteData->headerTopContent);die();
if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
{
	$objSiteData->headerTopContent=str_replace('src="img/', 'src="http://178.79.159.246/promociones/adeslas_amp/img/', $objSiteData->headerTopContent);
	echo $objSiteData->headerTopContent;
}
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
</section>


<section class="skeleton">
<?php
if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
//	$objSiteData->boxTopLeft=str_replace('src="img/', 'src="https://www.lastarjetasbancarias.com/promociones/adeslas_amp/img/', $objSiteData->boxTopLeft);
	$objSiteData->boxTopLeft=str_replace('src="img/', 'src="http://178.79.159.246/promociones/adeslas_amp/img/', $objSiteData->boxTopLeft);
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div class="marco">
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!
			</div>
';
}
// https://ampbyexample.com/components/amp-form/submit-form-xhr
//lightbox_wait.hide
//submit:lightbox_wait;
?>
</section>

<section class="formulario">
	<p class="l-text">&iquest;A&uacute;n no tienes un seguro de salud?</p>
	<p class="b-text">Te llamamos GRATIS</p>

		<!--<form method="post" action-xhr="https://www.lastarjetasbancarias.com/amp-test" target="_top" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">-->
		<form method="post" action-xhr="<?=$urlFormAction?>" target="_top" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo" on="submit-success:lightbox_response;submit-error:lightbox_error">
			<input type="hidden" name="ref_crea" value="<?=$id_crea?>" />
			<input type="hidden" name="fuente" value="<?=@(int)$_REQUEST['fuente']?>" />
A
			<input type="tel" maxlength="9" name="telefono" id="telefono" class="celda data-input" required placeholder="Nº de Teléfono" pattern="^[0-9]*" minlength="9" />

			<div class="legal">
				<input required="" type="checkbox" name="cblegales" value="1" aria-required="true">&nbsp;&nbsp;Acepto la
				<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php';?>" data-ref="" target="_blank">pol&iacute;tica de privacidad</a>
			</div>

    <input type="submit" value="¡Llamadme!" class="button">

		<amp-lightbox id="lightbox_error" class="lightbox" layout="nodisplay" on="open:lightbox_wait.hide ">
			<div class="lightboxBody">
				<div class="cansle" on="tap:lightbox_error.close" role="button" tabindex="0">X</div>
				<center><h2>ATENCIÓN</h2></center>
				<br /><span class="responseSuccess0"><center>ha ocurrido un error <i>interno</i></center></span>
					<template type="amp-mustache">
				<div submit-error class="lightboxContent">
					<center CLASS="responseSuccess0">SE HAN PRODUCIDO ERRORES</center><br />
						<br />ERROR!!! <b>{{v}}</b>
				</div>
					</template>
			</div>
		</amp-lightbox>

		<amp-lightbox id="lightbox_response" class="lightbox" layout="nodisplay" on="open:lightbox_wait.dismiss" >
			<div class="lightboxBody">
				<div class="cansle" on="tap:lightbox_response.close" role="button" tabindex="0">X</div>
					<div submit-success class="lightboxContent">
						<template type="amp-mustache">
							<center class="responseSuccess{{response.success}}">{{response.title}}</center><br />
							<br /><span class="responseSuccess{{response.success}}"><center><b>{{response.msg}}</b></center></span>
							<!--<br />mensaje: <b>{{response.msg}}</b>-->
							<!--<br />respuesta: <b>{{response.scriptName}}</b>-->
						</template>
					</div>
			</div>
		</amp-lightbox>

		<amp-lightbox id="lightbox_wait" class="lightbox" layout="nodisplay" on="close:lightbox_error">
			<div class="lightboxBodyWait">
				<br /><br />Enviando los datos, por favor, espera...<br /><br /><br />
			</div>
		</amp-lightbox>


	</form>
</section> <!--CIERRE FORM -->


<section class="text-content">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	$objSiteData->boxBottomForm=str_replace('src="img/', 'src="http://178.79.159.246/promociones/adeslas_amp/img/', $objSiteData->boxBottomForm);
	$objSiteData->boxBottomForm=str_replace('<a href="includes/', '<a href="http://178.79.159.246/promociones/adeslas_amp/includes/', $objSiteData->boxBottomForm);
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
	</section>
<!--CIERRE CONTENIDO -->
</section>

	<footer class="information">
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo @$objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">

			</div>
';
}

?>
	</footer>


<amp-user-notification layout=nodisplay id="amp-user-notification6" data-persist-dismissal="false" class="cookiesRowBottom">
Utilizamos cookies propias, de analítica y de terceros para mejorar tu experiencia de usuario. Al continuar con la navegación consideramos que aceptas la instalación y el uso de cookies,
<a href="http://www.google.com" target="_blank">más información aquí</a>.
<button on="tap:amp-user-notification6.dismiss" role="button" tabindex="1">Aceptar</button>
</amp-user-notification>
