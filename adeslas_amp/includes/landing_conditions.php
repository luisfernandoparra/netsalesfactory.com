<?php
@session_start();
include('../../conf/config_web.php');
$query='SELECT landing_terms_file FROM %s WHERE id=%d';
$query=sprintf($query,$table_landings_site_config,(int)$idLandingCr);
$conexion->getResultSelectArray($query);
$txtToDraw=$conexion->tResultadoQuery;
$txtToDraw=$txtToDraw[0];
$txtToDraw=$txtToDraw['landing_terms_file'];
?>
<!doctype html>
<html amp lang="es">
<head>
<meta charset="utf-8">
<title>Condiciones de Contratación</title>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<link rel="canonical" href="#"/>
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">


<style amp-custom>
body, .conditions{font-family:Arial, Helvetica, sans-serif, Gadget, sans-serif;color:#000000;background-color:#fff}
.conditions{padding:10px;}
</style>

</head>
<body>

<div class="conditions cond<?=$idLandingCr?>">
<?php
if(!(int)$idLandingCr)
	echo '<center>Lamentablemente ha ocurrido un error al intentar cargar la información solicitada.<br /><br />Por favor, inténtelo más tarde.</center>';
else
	echo $txtToDraw;
?>
</div>
</body>
</html>