<?php
//header('X-Content-Type-Options:nosniff');header('X-Frame-Options:SAMEORIGIN');header('X-XSS-Protection:1;mode=block');
$sec_tok='0'.$id_client.'_'.'DJK'.date('mHd').$arr_creas[$id_crea]['nombpromo'];
$sec_tok=md5($sec_tok);
$displayCaptcha=$id_crea == 999 ? 1 : 0;	// AQUI PONER EL IDENTIFICADOR DE LA CREA PARA MOSTRAR EL CAPTCHA

$googleTransactionTotal=1;

if($displayCaptcha)
{
	$googleDomainCaptcha=$sitesPruebas ? '6LcykBoTAAAAAHyF6i-ZcCxJUThMdK7IVu8spaIK' : '6LfJewcUAAAAANXVieUWTpW6pKdOvE2Vw0ng9nba';
/**
 * RE-CAPTCHA NECESSARY LIB
 * promoseguros.axa.es
 * site: 6LfJewcUAAAAANXVieUWTpW6pKdOvE2Vw0ng9nba
 * secret: 6LfJewcUAAAAAONd98pETdMSMos0tEoib-wKHzlY
 */
	$response='g-recaptcha';
	include($path_raiz_includes_local.'includes/captcha/captcha.lib.php');
	echo '<script src="https://www.google.com/recaptcha/api.js?hl=es"></script>';
}
//echo '-->'.$path_raiz_includes_local;die();
?>
<head>
<meta charset="utf-8">
<title><?=$arr_creas[$id_crea]['title']?></title>


<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
<script async custom-element="amp-user-notification" src="https://cdn.ampproject.org/v0/amp-user-notification-0.1.js"></script>
<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>

<link rel="canonical" href="<?=$linkCanonicalAmp?>"/>
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">

<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="Adeslas, Lo importante es saber lo que es importante">
<meta itemprop="description" content="This is a landing page about Adeslas">

<style amp-custom>
<?php
include($path_raiz_includes_local.'css/css_'.$id_crea.'.php');
?>

/* clases generales */
.responseSuccess1{color:green;font-weight:bold;}
.responseSuccess0{color:red;font-weight:bold;}
.cookiesRowBottom{
	min-height:30px;
	color:#000;
	font-size:.9em;
	line-height:30px;
	padding:8px;
	background:#BDEBFF;
	text-align:center;
	opacity:.8;
	-webkit-animation:fadeIn ease-in 1s 1 forwards;
	-moz-animation:fadeIn ease-in 1s 1 forwards;
	animation:fadeIn ease-in 1s 1 forwards;

}

.cookiesRowBottom button{
	font-size:.85em;
	border:none;
	border-radius:2px;
	color:#fff;
	height:26px;
	min-width:32px;
	padding:0 16px;
	margin:0 16px;
	text-transform:uppercase;
	letter-spacing:0;
	cursor:pointer;
	vertical-align:middle;
	line-height:26px;
	text-align:center;
	background:#009DDE;
}

#lightbox_response, #lightbox_error{
background: rgba(0,0,0,0.8);
width:100%;
height:100%;
position:absolute;
text-align:left;
z-index:10;
}

#lightbox_wait{
background: rgba(0,0,0,0.6);
width:100%;
height:100%;
position:absolute;
text-align:center;
padding:0;
z-index:1;
}

.lightboxBody{
background: rgba(254,254,254,1);
width:50%;
height:30%;
left:25%;
top:2%;
position:absolute;
text-align:left;
border-radius:10px;
z-index:20000;
}

.lightboxBodyWait{
background: rgba(254,254,254,1);
width:20%;
height:auto;
left:43%;
top:10%;
position:absolute;
text-align:center;
border-radius:6px;
padding:0;
padding-left:10px;
padding-right:10px;
marging:0;
z-index:-10;
vertical-align:middle;
}

.lightboxContent{
padding:3%;
}

#lightbox_error h1 {
color: white;
background-color:rgba(254,254,254,1);
}

#lightbox_error div .errorBody{
color:#222;
padding:10px;
background-color:rgba(254,254,254,1);
}

.cansle{
width:10%;
text-align:center;
background:#009ddd;
border:1px solid #009ddd;
min-height:40px;
line-height:40px;
color:#fff;
font-size:20px;
margin-left:89.8%;
border-radius:4px;
cursor:pointer;
}

@media (max-width: 560px){
.cansle{
width:20%;
margin-left:79%;
}
}

</style>

<?php
if($displayCaptcha)
{
	$cssGoogleCaptcha='
<style>
#g-recaptcha{
width:30%;
margin:auto;
padding-top:10px;
}
@media (max-width:680px){
#g-recaptcha{
width:50%;
}}

@media (max-width:510px){
#g-recaptcha{
width:70%;
}}

@media (max-width:380px){
#g-recaptcha{
width:80%;
}}

@media (max-width:374px){
#g-recaptcha{
width:95%;
}}

@media (min-width:355px) and (max-width:374px){
#g-recaptcha{
width:85%;
}}

@media (min-width:374px) and (max-width:385px){
#g-recaptcha{
width:80%;
}}
</style>
';
}
?>
<!--[if lte IE 8]><link rel="stylesheet" href="<?=$path_raiz_aplicacion_local?>css/ie.css" /><![endif]-->

</head>

<body>