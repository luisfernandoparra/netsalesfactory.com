<?php
header('X-Content-Type-Options: nosniff');
header('X-Frame-Options: SAMEORIGIN');
header('X-XSS-Protection: 1;mode=block');
@session_start();
include('../../conf/config_web.php');
$query='SELECT privacy_policy_file FROM %s WHERE id=%d';
$query=sprintf($query,$table_landings_site_config,(int)$idLandingCr);
$conexion->getResultSelectArray($query);
$txtToDraw=$conexion->tResultadoQuery;
$txtToDraw=$txtToDraw[0];
$txtToDraw=$txtToDraw['privacy_policy_file'];
?>
<!doctype html>
<html amp lang="es">
<head>
<meta charset="utf-8">
<title><?=$arr_creas[$id_crea]['title']?></title>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<link rel="canonical" href="#"/>
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">

<style amp-custom>
body, .conditions{
width:auto;
padding:2%;
font-family:Arial, Helvetica, sans-serif, Gadget, sans-serif;
color:#000000;
background-color:#FFF;
}

.Part{
	font-family:arial,verdana;
	font-size:.9em!important;
}

</style>

</head>
<body>

<?php
if(!(int)$idLandingCr)
	echo '<center>Lamentablemente ha ocurrido un error al intentar cargar la información solicitada.<br /><br />Por favor, inténtelo más tarde.</center>';
else
	echo $txtToDraw;
?>

</body>
</html>