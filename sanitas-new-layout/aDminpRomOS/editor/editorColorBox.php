<?php
/*
 * NOTAS IMPORTANTES:
 * 
 * PARA QUE SE CARGUE CORRECTAMENTE EL CSS DEL REFISTRO CAMPO SELECCIONADO
 * SE DEBE MANTENER EL MISMO PROCEDIMIENTO PARA SU GENERACIÓN QUE EN EL
 * FRONT (ej.: str_pad($_REQUEST['site_landing_id'],3,'0',STR_PAD_LEFT)
 * 
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');
include('../../../conf/config_web.php');
include('../../conf/config_web.php');

$sqlFilter=$_REQUEST['site_landing_id'] ? 'lsc.id='.(int)$_REQUEST['site_landing_id'] : 'lsc.client_id='.(int)$_REQUEST['customer_landing_id'];

if(count($objSiteData) && $sqlFilter)	// SE OBTIENEN LOS PARAMETROS NECESARIOS PARA CONTRUIR LAS RUTAS NECESARIAS
{
	$query='SELECT lc.prefix AS customer_name, csc.root_local_path FROM %s AS lsc INNER JOIN %s AS csc ON csc.client_id=lsc.client_id INNER JOIN %s AS lc ON lc.id_landing=lsc.client_id WHERE '.$sqlFilter.' LIMIT 1';
	$query=sprintf($query,$table_landings_site_config,$table_customers_site_config,$table_landings_customers);
	$conexion->getResultSelectArray($query);
	$specificLandingConfig=$conexion->tResultadoQuery;
	$specificLandingConfig=$specificLandingConfig[0];
	$_SESSION['localLandingImages']=$specificLandingConfig['root_local_path'];
}

$rutaRompe=stripos($_SERVER['SCRIPT_NAME'],'/aDminpRomOS');
$rutaBase='http://'.$_SERVER['SERVER_NAME'].''.substr($_SERVER['SCRIPT_NAME'],0,$rutaRompe).'/';
$generalPromosPath=substr($rutaBase,0,-1);
$generalPromosPathPos=strripos($generalPromosPath,'/');
$localPromoPath=substr($generalPromosPath,0,$generalPromosPathPos).'/'.$_REQUEST['rootLocalPath'].'/';
$cssSpecificLandig=$localPromoPath.'css/css_'.str_pad($_REQUEST['site_landing_id'],3,'0',STR_PAD_LEFT).'.css';

//echo '<hr><pre>'.$_REQUEST['rootLocalPath'].']';
/*
 * START ARRAYS BOX EDITING
 * PARA PERMITIR CREAR TAGSS
 * ALREDEDOR DEL CONTENIDO A EDITAR
 * Y SIMULAR LA PANTALLA CORRECTA
 * PARA PODER SER VISUALIZADO
 * MAS COHERENTEMENTE
 */
$arrFieldsWidthSpecialClasses=array(
		'data_box_bottom_form'=>'contenido_texto',
		'data_header_top_content'=>'cabecera',
		'data_box_top_left'=>'foto'
);
$arrFieldsExtraTags=array(
		'data_header_top_content'=>'<header>'
		,'data_box_top_left'=>'<section class="foto hide">'
);
// ARRAYS BOX EDITING

$tagToManageEnd='';
$tagToManage=$arrFieldsExtraTags[$_REQUEST['nombreCampo']];

if($tagToManage)	// SOLO SI ESTÁ DEFINIDO ESTE ELEMENTO
{
	$tagToManage=str_replace('"','\'',$tagToManage);
	$tagToManageStart=preg_split('/[\s,]+/', $tagToManage, -1, PREG_SPLIT_OFFSET_CAPTURE);	// SUBDIVIDIR CADENA SEPARADA POR ESPACIOS
	$tagToManageEnd='</'.substr($tagToManageStart[0][0],1).'>';	// CREAR TAG DE CIERRE SEGUN $tagToManageStart
	$strLenTag=strlen($tagToManageEnd);

	if(substr($tagToManageEnd,-1) == substr($tagToManageEnd,$strLenTag-2,-1))	// EN EL CASO DE UN TAG SIN PARAMETROS
		$tagToManageEnd=substr($tagToManageEnd,0,-1);
}
//echo "\r\n".$tagToManageStart[0][0];
//echo "\r\n".$tagToManageEnd;
//echo $tagToManage."\r\n".$tagToManageEnd;die();
if(substr($_SERVER['SERVER_ADDR'],0,3)=='192')	// PARA EL SERVIDOR LOCAL
	$rutaBase='http://'.$_SERVER['SERVER_ADDR'].substr($_SERVER['PHP_SELF'],0,$rutaRompe).'/';

//<p style="text-align: center;"><br /><br /><br /><br />Página en construcción, disculpen las molestias</p>

if($_REQUEST['accion'] == 1)
{
	$datos=$_REQUEST['elm1'];
	$datos=trim($datos);
	$datos=str_replace(chr(10), ' ', $datos);
	$datos=str_replace(chr(13), ' ', $datos);
	$datos=str_replace(chr(145), chr(39), $datos);
	$datos=str_replace(chr(146), chr(39), $datos);
	$datos=str_replace("'", '&#39;', $datos);
//	$datos=str_replace("€", '&euro;', $datos);
//print_r($datos);die();
//	$datos=utf8_decode($datos);
//	$datos=urlencode($datos);

	if(!$_REQUEST['idRegistro'])
	{
		echo '
<script language="JavaScript">
	parent.$.colorbox.close();';
		if(substr($_REQUEST['nombreCampo'],0,5) == 'data_')	// ONLY FOR POPLINE FIELD NAMES (31.01.2014)
			echo 'parent.$("#'.$_REQUEST['nombreCampo'].'").html(\''.urldecode($datos).'\');';
		else
			echo 'parent.document.'.$_REQUEST['nombreFormulario'].'.'.$_REQUEST['nombreCampo'].'.value="'.($datos).'";';
		echo '</script>';
	}
	else
		echo "
<script language='JavaScript'>
	alert(document.location)
	//window.opener.document.".$_REQUEST['nombreFormulario'].".".$_REQUEST['nombreCampo'].".value='".($datos)."';
	//window.close();
</script>
";
	exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Editando: <?=$_REQUEST['nombreCampo']?></title>

<?php
$plugInCssLanding='';

if($_REQUEST['site_landing_id'])	// SOLO SI LA ID DE LA LANDING ESTÁ DEFINIDA
{
	$plugInCssLanding=' importcss';
}

?>

<!--<script type='text/javascript' src='../_js/jquery.min.js'></script>-->

<!-- TinyMCE -->
<script type="text/javascript" src="tinymce.min.js"></script>
<script type="text/javascript" src="../_js/jquery-1-8-2.min.js"></script>
<style>
.emulateContenidoTexto{
display:block;
border:2px solid red!important;
}
</style>

<script type="text/javascript">
<!--
var additionalTagStart="<?=$tagToManage?>";
var additionalTagEnd="<?=$tagToManageEnd?>";

function limpiarTexto(type, value)
{
	switch (type)
	{
		case "get_from_editor":
			if(value.substr(0,3) == '<p>')
			{
				value=value.substr(3);
				if(value.substr(value.length-6,4) == '</p>')
					value=value.substr(0,value.length-6);
			}
			break;
	}
	return value;
}

$(document).ready(function(){
	$("#elm1").show("slow");
})

var objSceen;

function execFullScreen(inst)
{
	if(objSceen.editorId != 'mce_fullscreen')
		objSceen.execCommand('mceFullScreen');
}

//var make_wysiwyg = function(inst){
////    $(tmceIframe).find("body#tinymce article").wrapInner('<div class="post-content"/>');
//}

function resizeToFullScreen(inst)
{
	objSceen=inst;
	setTimeout("execFullScreen();",300);
}

var heightFileManagerBox=$(window).height()-50;	// ALTURA DEL BOX PARA EL PLUG-IN DEL RESPONSIVEFILEMANAGER

var inst=tinymce.init({
	setup:function(editor) {
//		editor.on('PreProcess', function(event){
//			var tmpData=event.content;
//			var tmpData=$(tmpData).html();
//			var dataTmp=$(tmpData);
//console.log(editor.startContent);
//		});

		editor.on('PostProcess', function(event){
			var tmpData=event.content;
			var dataTmp=$(tmpData).html();
			var test_Data=event;
//			var tmpData=$(tmpData).html();
//			var dataTmp=$(tmpData);
console.log(dataTmp.indexOf('<section id="tmpDiv" class="foto">'));
console.log("<?=$arrFieldsWidthSpecialClasses[$_REQUEST['nombreCampo']]?>");
			if(tmpData.length > 0 && "<?=$arrFieldsWidthSpecialClasses[$_REQUEST['nombreCampo']]?>")
			{
				tinyMCE.activeEditor.setContent(dataTmp);
			}
		});

		 editor.on('init', function(event){
			if("<?=$arrFieldsWidthSpecialClasses[$_REQUEST['nombreCampo']]?>")	// ADD SECTION ONLY FOR ELEMENT WIDTH SPECIFIC BLOCK CSS
				tinyMCE.activeEditor.setContent(additionalTagStart+"<section id='tmpDiv' class='<?=$arrFieldsWidthSpecialClasses[$_REQUEST['nombreCampo']]?>'>"+event.target.startContent+"</section>"+additionalTagEnd);	//ESTO SI INSERTA EL DIV EN EL CONTENIDO AL CARGAR

//tinyMCE.activeEditor.dom.addClass(tinyMCE.activeEditor.dom.select('.mce-content-body'),'emulateContenidoTexto');
//tinyMCE.activeEditor.formatter.apply('color',{value : 'red'})
//tinyMCE.activeEditor.setContent("<section class='contenido_texto'>"+event.target.startContent+"</section>");
//$('.mce-content-body').addClass("emulateContenidoTexto");
//var ed=tinymce.get("elm1");
//$(ed.startContent).wrapInner('<div class="post-content"/>');
//parent.parent.$(".mce-content-body ").html('<div class="post-content"/>');
//console.log($(ed.startContent));
//					edContents = event.getContent();
            //editor.mceEditArea().className += ' ' + "";
//console.log(event.target.startContent);
//						$(".mce-content-body").wrapInner('<div class="XXXXX" />')
//						event.setContent("<p>XXXX</p>"+edContents);
//						$('#tinymce').addClass("contenido_texto");
//						$('.mce-edit-area').html(event.target.startContent);
//						$('.mce-edit-area').addClass("contenido_texto");

        });
    },

	selector:"textarea",
	forced_root_block:"",
	plugins:[
		"advlist autolink autosave save link image lists charmap preview hr anchor pagebreak",
		"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
		"table contextmenu directionality template textcolor paste textcolor<?=$plugInCssLanding?> responsivefilemanager"
	],
	toolbar1:"newdocument save | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor",
	toolbar2:"preview searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image responsivefilemanager | inserttime",
	toolbar3:"table | hr removeformat | subscript superscript | charmap | ltr rtl | visualchars visualblocks nonbreaking pagebreak restoredraft | code fullscreen",

	image_advtab:true,
	language:"es",
	menubar:false,
	init_instance_callback:"resizeToFullScreen",
	toolbar_items_size:"small",
	relative_urls:false,
	remove_script_host:true,
	
	convert_urls:false,
	document_base_url: "../../",
	
	convert_newlines_to_brs:false,
	cleanup_callback:"limpiarTexto",
	force_p_newlines:true,
	remove_linebreaks:false,
	force_br_newlines:false,
	remove_trailing_nbsp:false,
	verify_html:false,
 	inline_styles:true,
	mode:"textareas",
	style_formats:[
		{
		title: "Imagen izquierda",
		selector: "img",
		styles: {
			"float": "left", 
			"margin": "0 10px 0 10px"
		}
		},
		{
		 title:"Imagen derecha",
		 selector:"img", 
		 styles:{
			 "float": "right", 
			 "margin": "0 0 10px 10px"
		 }
		}
	],
<?php
if($_REQUEST['site_landing_id'] > 0)
	echo '
	importcss_append:true,
	content_css:"'.$cssSpecificLandig.'?sed='.date('ish').'",
';
else
	echo '';

$refFiles=$_REQUEST['site_landing_id'] ? ' (XXXXX_'.str_pad($_REQUEST['site_landing_id'],3,'0',STR_PAD_LEFT).')' : '';
$titleModalFileManager=$specificLandingConfig['customer_name'] ? 'Archivos de '.$specificLandingConfig['customer_name'].$refFiles : 'Sin cliente seleccionado';
?>
	/*
	templates: [
		{title: 'Test template 1', content: 'Test 1'},
		{title: 'Test template 2', content: 'Test 2'}
	],
	*/
  filemanager_title:"<?=$titleModalFileManager?>",
//	external_filemanager_path:"../filemanager/",
//  external_plugins:{"filemanager":"../filemanager/plugin.min.js"}
	external_filemanager_path:"<?=$rutaBase?>aDminpRomOS/filemanager/",
  external_plugins:{"filemanager":"<?=$rutaBase?>aDminpRomOS/filemanager/plugin.min.js"}
});
///console.log("==><?=$_REQUEST['site_landing_id']?>");
<?php
//if($_REQUEST['site_landing_id'] > 0)
//	echo '
//tinymce.init({
//	importcss_append:true
////	,content_css:"../../css/'.$arr_creas[$_REQUEST['site_landing_id']]['cookie_css'].'?sed='.date('ish').'"
////	,content_css:"../../css/css_'.$_REQUEST['site_landing_id'].'.css?sed='.date('ish').'"
//	,content_css:"'.$rutaBase.'css/css_'.$_REQUEST['site_landing_id'].'.css?sed='.date('ish').'"
//});
//';
?>
-->
</script>
<!-- /TinyMCE -->

<link rel='stylesheet' href='../_css/estilos.css' type='text/css'>

</head>
<body>
<form name="xxxxxxx" method="post" action="editorColorBox.php?example=true">
<input name="nombreFormulario" value="<?=$_REQUEST['nombreFormulario']?>" type="hidden" />
<input name="nombreCampo" value="<?=$_REQUEST['nombreCampo']?>" type="hidden" />
<input name="accion" value="1" type="hidden" />
<span style="font-family:Arial, Helvetica,sans-serif;font-size:12px;color:#333">&nbsp;Editando el contenido del campo: <b><?=$_REQUEST['nombreCampo']?></b></span>

<textarea id="elm1" name="elm1" rows="10" cols="50" style="width:80%;display:inline-block;float:left;">
</textarea>
<?php
echo '<script>';
if(substr($_REQUEST['nombreCampo'],0,5) == 'data_')	// POPLINE SCRIPT CONTENT - SE INSERTA EL CONTENIDO DEL ELEMENTO RECIBIDO (31.01.2014)
	echo '$("#elm1").val(parent.$("#'.$_REQUEST['nombreCampo'].'").html());';
else
	echo '$("#elm1").val(parent.document.'.$_REQUEST['nombreFormulario'].'.'.$_REQUEST['nombreCampo'].'.value);';
echo '</script>';

?>
</form>
</body>
</html>