﻿<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require('_inc/cabeceraadmin.php');
require('_cnf/connect2.php');
require('_inc/phones_per_id_sources.class.php');
require('_inc/log.class.php');
$objeto=new phonesPerIdSources($sql, $admin);

$objeto->filasporpagina=10;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=20;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=1;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// NÚMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$objeto->modalHeight='170px';
$objeto->modalWidth='70%';
$objeto->modalLeftPos='12%';
$objeto->innerTextHeight='200';
$objeto->editarFila=1;
$nombreFilePhpBase='phones_per_id_sources';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR

$txtTitularFormularioNewRecord='<b>Insertar / editar</b> teléfono por ID de source';
$txtTitularBuscador='Buscar teléfonos por ID de source';
$txtMsgStandard='teléfonos por ID de source presentes: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';
//$txtTitularEdicion='Editar men&uacute; nivel 2';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>