<?php
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/companies.class.php');
require_once('_inc/log.class.php');

$objeto=new companies($sql, $admin);
$objeto->carga($_REQUEST['id']);

$nombreFilePhpBase='companies';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$objeto->nombreFilePhpBase=$nombreFilePhpBase;

$objeto->tamanioMaximoAttach=750000;	// EN bites
$objeto->tamanioMaximoImagenes=200000;	// EN BYTES
$objeto->anchoMaximoImagenes=720;	// EN PIXELS
$objeto->path_imagenes='../images/';
$objeto->innerTextHeight="300"; // altura innerTinyBoxEdit
$txtLogEditFinish='record stored';
$txtTitularEdicion='Editar la empresa';

include('_inc/common_form_edit.php');
?>