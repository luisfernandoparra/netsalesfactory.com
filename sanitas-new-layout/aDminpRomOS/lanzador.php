<?php
$localDemosPath='../../../';
echo 999;die();
?>
<link rel="stylesheet" type="text/css" href="_css/stylesInnerTabs.css"/>
<link rel="stylesheet" type="text/css" href="_css/colorbox2013.css"/>

<!--
<link rel="stylesheet" type="text/css" href="css/styleLetters.css"/>

<script type="text/javascript" src="js/jquery.lettering.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.hoverwords.js"></script>
-->

<script>
$(document).ready(function(){

// START COLORBOX MODAL WINDOWS
	$(".popWidth84").colorbox({iframe:true, width:"84%", height:"100%"});
	$(".popWidth65").colorbox({iframe:true, width:"65%", height:"98%"});
	$(".scriptGeneric").attr("title",$(".scriptGeneric").attr("text")).colorbox({iframe:true, width:"90%", height:"90%"});
	$(".scriptCSSGen").colorbox({iframe:true, width:"80%", height:"80%"});
// END COLORBOX MODAL WINDOWS

/*
	$("#reload").find("li:not(:last) > a")
				 .hoverwords({overlay:true,speed:200,dir:"rightleft"})
				 .end()
				 .find("li:last > a")
				 .hoverwords({
					overlay:true,speed:200,dir:"rightleft"
				});
*/
	$(".wrapTabs").fadeIn("fast");
});
</script>

<link rel="stylesheet" href="css/coda-slider.css" type="text/css" charset="utf-8" />
<script src="js/jQuery.ScrollTo.js" type="text/javascript"></script>
<script src="js/jQuery.LocalScroll.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jQuery.serialScroll.js" type="text/javascript" charset="utf-8"></script>
<script src="js/coda-slider.js" type="text/javascript" charset="utf-8"></script>

<center> <a class="btn rounded" href="">Recargar</a></center>
<br />
<center>
<div class="wrapTabs" style="display:none;">
        <div id="slider">
            <ul class="navigation">
                <li><a title="accesos a varias demos" class="mnuTbas" href="#tab01">Demos</a></li>
                <li><a title="utilidades operativas" class="mnuTbas" href="#tab02">Helpers</a></li>
                <li><a class="mnuTbas" href="#tab03">scripts JS</a></li>
                <li><a title="Scripts con jQuery" class="mnuTbas" href="#tab04">jQuery</a></li>
            </ul>

            <img class="scrollButtons left" src="img/scroll_left.png">
			<div style="overflow: hidden;" class="scroll">

                <div style="width: 4340px;" class="scrollContainer">

<!-- DEMOS -->
					<div style="float: left; position: relative;" class="panel" id="tab01">
								<ol class="scriptsVarios ">
									<li>Port-folio <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>mario/SlideDownMario/" target="_blank">local</a> / </span><a href="http://mfrancescutto.com/portfolio2013/" target="_blank">real</a></li>
									<li><i>Port-folio</i> back-office <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>mario/SlideDownMario/admin/" target="_blank">local</a> / </span><a href="http://www.mfrancescutto.com/portfolio2013/admin/" target="_blank">real</a></li>
									<li>&Aacute;baco: <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>abaco/site/" target="_blank">local</a> / </span><a href="http://www.mfrancescutto.com/abaco/" target="_blank">demo</a> / <a href="http://abacoasesoria.es" target="_blank">real</a></li>
									<li><i>&Aacute;baco</i> Back-office: <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>abaco/site/admin2013/" target="_blank">local</a> / </span><a href="http://www.mfrancescutto.com/abaco/admin2013/" target="_blank">demo</a> / <a href="http://abacoasesoria.es/admin2013/" target="_blank">real</a></li>
									<li><span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>ebooks/site/" target="_blank">E-books</a> / </span><a href="http://mfrancescutto.com/2012/ebooks/" target="_blank">E-books real</a></li>
									<li title="Pinceladagrafica">Rafael Deco (pinceladagrafica): <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>rafael/deco2013/decoration/" target="_blank">local</a> / </span><a href="http://www.mfrancescutto.com/rafael_deco/market/" target="_blank">demo</a> / <a href="http://www.pinceladagrafica.es/" target="_blank">real</a></li>
									<li title="Pinceladagrafica"><i>Deco</i> (pinceladagrafica) Admin: <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>rafael/deco2013/decoration/admin/" target="_blank">local</a> / </span><a href="http://www.mfrancescutto.com/rafael_deco/market/admin/" target="_blank">demo</a> / <a href="http://www.pinceladagrafica.es/adMiN/" target="_blank">real</a></li>
									<li title="es una version SOLO para test, no es el real">SRDD (tests): <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>XXXX/srdd/siteTests/" target="_blank">local</a> / </span><a href="http://mfrancescutto.com/XXXX/srdd2/" target="_blank">demo</a></li>
									<li title="es una version SOLO para test, no es el real">SRDD back-office: <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>XXXX/srdd/siteTests/AdmiN/" target="_blank">local</a> / </span><a href="http://mfrancescutto.com/XXXX/srdd2/AdmiN/" target="_blank">demo</a></li>
									<li title="es la version en la que se basa la Web real">SRDD (activa) <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>XXXX/srdd/siteReal/" target="_blank">local</a> / </span><a href="http://diligenciadebida.es/" target="_blank">real</a></li>
									<li title="es la version en la que se basa la Web real"><i>SRDD</i> Back-office(act.) <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>XXXX/srdd/siteReal/" target="_blank">local</a> / </span><a href="https://diligenciadebida.es/AdmiN/" target="_blank" title="pass=srdd2012">real </a></li>
									<li>XXX: <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>XXXX/" target="_blank">local</a> / </span><a href="http://www.mfrancescutto.com/XXXX/" target="_blank">demo</a></li>
									<li>back-office: <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>/site/AdmiN/" target="_blank">local</a> / </span><a href="http://www.mfrancescutto.com/AdmiN/" target="_blank">demo</a></li>
									<li>Mio Curriculum 2010: <span class="<?=$isLocal ? 'localClass' : 'hide'?>"><a href="<?=$localDemosPath?>mario/site_port-folio/" target="_blank">local</a> / </span><a href="http://www.mfrancescutto.com/curriculum/" target="_blank">real</a></li>
								</ol>
					</div>


<!-- -Utilidades y helpers -->
					<div style="float: left; position: relative;" class="panel" id="tab02" title="Utilidades">
						<ol class="scriptsVarios ">
							<li><a title="CSS3 generador" class="scriptCSSGen" href="<?=$localDemosPath?>2013/css3-generator/">CSS3 generador</a></li>
							<li><a title="C&oacute;digos HTML" class="popWidth84" href="<?=$localDemosPath?>HTML_Codes.htm">HTML codes</a></li>
							<li><a title="Tricks jQuery" class="popWidth65" href="<?=$localDemosPath?>2013/tricksJquery.php">Tricks jQuery</a></li>
							<li><a title="" class="popWidth84" href="<?=$localDemosPath?>info.php">Local Server info</a></li>
						</ol>
					</div>


<!-- bloque javascripts varios-->
					<div style="float: left; position: relative;" class="panel" id="tab03">
						<ol class="scriptsVarios">
							<li><a title="" class="scriptGeneric" href="<?=$localDemosPath?>2013/JavaScriptBannerRotatorDemo.php">Simple banner rotator</a></li>
						</ol>
					</div>


<!-- bloque jQuery-->
					<div style="float: left; position: relative;" class="panel" id="tab04">
						<ol class="scriptsVarios">
							<li><a title="" class="scriptGeneric" href="<?=$localDemosPath?>2013/popline-editor/">Basic-editor f&aacute;cil</a></li>
							<li><a title="Coda horizontal slider" class="popWidth84" href="<?=$localDemosPath?>2013/codaSlider/coda_slider.php">Coda horizontal slider</a> (utilizado en esta misma herramienta)</li>
							<li><a title="Tablas 0rdenadas (javascript con ejemplo)" class="popWidth84" href="<?=$localDemosPath?>2013/sortTables/useSortTables.php">Tablas 0rdenadas</a></li>
							<li><a title="Tricks jQuery" href="#tab02">Tricks jQuery</a></li>
						</ol>
					</div>

                </div>
            </div><img class="scrollButtons right" src="img/scroll_right.png">
<div id="shade"></div>

		</div>

</div>


</center>
