<?php
header('Content-Type: text/html; charset=utf-8');
@include('cabeceraadmin_logica.php');
@include('_cnf/connect2.php');
@include('_inc/avisos.class.php');
@include('_inc/log.class.php');
@include('_inc/config_back_office.class.php');

$objeto=new Avisos($sql, $admin);
$objeto->config=new Configuracion($sql, $_SESSION['id_usuario']);
$objeto->config->carga_datos();	// SE CARGA LA CONFIGURACION DEL BACK-OFFICE PARA EL USUARIO ACTUAL
$objeto->ObtenerNavegador();
$objeto->nameNavegador;
$objeto->getAdminLevel();

//echo '&nbsp;&nbsp;<b>'.date('H:i:s').'</b>';

include('../conf/config_web.php');
$staticCreativities='';

foreach($arr_creas as $key=>$value)
{
	$staticCreativities.=$key.', ';
}
$staticCreativities=substr($staticCreativities,0,-2);


if($_SESSION['usuarioNivel'] < 11)
	$arrNovedades[]='Bienvenido a la Web de administraci&oacute;n de <b>'.utf8_decode($_SESSION['nombre_site']).'</b>';

$arrNovedades[]='Actualmente tu IP es la siguiente: <b>'.$_SERVER['REMOTE_ADDR'].'</b>';

if($_SESSION['usuarioNivel'] > 1 && $_SESSION['usuarioNivel'] < 99)
{
  $objeto->obtenerAyudaModulo();
  if(count($objeto->txtHelpModule))
  {
	foreach($objeto->txtHelpModule as $ref=>$txtModule)
	  $arrNovedades[]=''.utf8_encode($txtModule);
  }
}

if($_SESSION['usuarioNivel'] > 10)
{
	$arrNovedades[]='Diseñado para los mejores navegadores modernos (Mozilla, Safari, Chrome...) y también compatible con I-explorer a partir de la versión 8.';
	$arrNovedades[]='<h3>NOTAS IMPORTANTES</h3>';
	$arrNovedades[]='ID´s de <b>cratividades</b> que <b><u>NO se deben configurar</u></b> desde esta herramienta, ya que están gestionadas ´al viejo estilo´ completamente desde archivos: <b>'.$staticCreativities.'</b>.';
}

if($_SESSION['usuarioNivel'] > 14 && $_SESSION['usuarioNivel'] < 15)
{
	$arrNovedades[]='Total modularidad para  todas las secciones y áreas.';
	$arrNovedades[]='Varios niveles de acceso, con distintos roles de administración (3 + 1).';
	$arrNovedades[]='Nuevo sistema de gestión de fichas, con sistema automático de generación del área de trabajo, renovable dinámicamente.';
}

echo '
<div style="display:block;padding:0px;min-height:400px;width:100%;border:0px solid #ccc;">
	<span style="display:block;height:40px;line-height:30px;padding-left:10px;">
	<b>Herramienta de gesti&oacute;n</b> (<i>versi&oacute;n '.$_SESSION['VERSION'].'</i>).
	</span>';

echo '<div style="position:relative;display:block;float:left;min-width:200px;width:99%;height:auto;padding:0px;border:0px solid red">';

echo '<table class="implicitNotices" width="100%" style="padding-left:8px;" cellpadding="0" cellspacing="0" border="0">';
echo '<tr valign="top"><td width="90%" style="display:block;;height:350px;overflow:auto!important;padding:4px;">';

foreach($arrNovedades as $key=>$novedad)
{
	if($key) echo '<br />';
	echo '<div>- '.($novedad).'</div>';
}

echo '</td>';

if($_SESSION['usuarioNivel'] >= $objeto->levelAccessModules['SuperAdmin'])
{
	echo '<td>';
	echo '<div style="position:relative;display:inline-block;float:left;width:560px;border:0px solid yellow; ">';
	require('avisos.local.tabs.php');
	echo '</div>';
}
else
{
	echo '<td width="10%">';
	echo '<div style="position:relative;display:inline-block;float:left;width:auto;border:0px solid yellow; ">';
	include('mod_calendar_smp.php');
	echo '</div>';
}

echo '</td></tr>';
echo '</table>';

echo '
</div>
';
?>
<script>
$(document).ready(function(){

// START COLORBOX MODAL WINDOWS
	$(".popWidth84").colorbox({iframe:true, width:"84%", height:"96%"});
	$(".popWidth65").colorbox({iframe:true, width:"65%", height:"94%"});
	$(".scriptGeneric").attr("title",$(".scriptGeneric").attr("text")).colorbox({iframe:true, width:"90%", height:"90%"});
	$(".scriptCSSGen").colorbox({iframe:true, width:"80%", height:"80%"});
// END COLORBOX MODAL WINDOWS
});
</script>
<?php
//echo $query.'<div style=display:block;float:left;width:100%;><pre style=color:red;>';print_r($objeto->arrTabsTitles);echo'<pre></div>';
////echo 'este es un texto corrido para ver como queda colocado tras el include';
//echo '<p>'..'</b>';//print_r($_SERVER['REMOTE_ADDR']);

if($objeto->nameNavegador=='msie' && strlen(strpos($objeto->versionNavegador,'6.')))
	echo '<br /><br /><div style="display:block;padding:10px;background-color:#fff;color:red;"><b>NOTA IMPORTANTE</b>: el navegador que est&aacute;¡ utilizando no funciona con <b>'.$_SESSION['nombre_site'].'</b><br><br>Debe cerrar este navegador y utilizar uno m&aacute;¡s moderno... lamentamos las molestias</div>';

new Log($sql, $id_pagina);
include('footer.php');
?>
