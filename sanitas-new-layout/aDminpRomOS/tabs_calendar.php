<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/log.class.php');
?>
<style>
body{
	font:.8em Verdana,Arial,Geneva,Helvetica,sans-serif;
}

#divCalendar{display:none;width:40%;height:auto;}

.calendar-month{
-moz-box-shadow:-2px 2px 6px <?=$_SESSION['colorTableBordeLight']?>;
-webkit-box-shadow:-2px 2px 6px <?=$_SESSION['colorTableBordeLight']?>;
}

.SpDay<?=number_format(date('d'),0)?>{color:red;}
.tdDay<?=number_format(date('d'),0,'','').'_'.number_format(date('m'),0,'','').'_'.date('Y')?>{
display:block;float:right;
border:1px solid <?=$_SESSION['colorTableBordeLight']?>;
color:<?=$_SESSION['colorTextHighli']?>;
background-color:<?=$_SESSION['colorFondoOverBotones']?>;
-moz-box-shadow:-1px -1px 4px <?=$_SESSION['colorTableBordeLight']?>;
-webkit-box-shadow:-1px -1px 4px <?=$_SESSION['colorTableBordeLight']?>;
background:-webkit-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
background:-moz-linear-gradient(<?=$_SESSION['colorFondoTitular']?>,<?=$_SESSION['colorFondoOverBotones']?>);
background:-o-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
border-radius:6px;
border-radius:6px;
font-weight:bold;
}

.weekday{
border-top:1px solid #333;
}

.passMont{margin-bottom:4px;}
.current-month{
background-color:<?=$_SESSION['colorFondoTitular']?>;
background:-webkit-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
background:-moz-linear-gradient(<?=$_SESSION['colorFondoTitular']?>,<?=$_SESSION['colorFondoWeb']?>);
background:-o-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
}
#current-month{display:block;position:relative;margin:4px;}
li{list-style-type:none;}

img{border:none;}

a:link, a:visited{
	color:#039;
	text-decoration:none;
}

a:hover, a:active{
	color:#000;
	text-decoration:none;
}

.active{
	font-size:2.0em;
}

.error{
	color:#f00;
}

.error-highlight{
	border:1px solid #f00;
}

td, th{
	border-left:1px solid #999;
	border-bottom:1px solid #999;
	width:80px;
	padding:5px 0;
	text-align:center;
}

table{
	border-right:1px solid #999;
}

th{
	background:#666;
	color:#fff;
}

.other-month{
background-color:<?=$_SESSION['colorFondoTitular']?>;
background:-webkit-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
background:-moz-linear-gradient(<?=$_SESSION['colorFondoWarning']?>,<?=$_SESSION['colorFondoTitular']?>);
background:-o-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
}

.day{height:10px;}

</style>
<script>
<!--
(function($){
	function calendarWidget(el,params)
	{
		var now  =new Date();
		var thismonth=now.getMonth();
		var thisyear=now.getYear() + 1900;

		var opts={
			month:thismonth,
			year:thisyear
		};

		$.extend(opts,params);
		var monthNames=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
		var dayNames=['Domingo','Lunes','Martes','Mi�rcoles','Jueves','Viernes','Sabado'];
		month=i=parseInt(opts.month);
		year=parseInt(opts.year);
		var m=0;
		var table='';
		
			// next month
			if(month==11){
				var next_month='<a class="btn passMont" href="?month=' + 1 + '&amp;year=' + (year + 1) + '" title="' + monthNames[0] + ' ' + (year + 1) + '">&raquo;&nbsp;' + monthNames[0] + ' ' + (year + 1) + '</a><br /><br />';
			} else{
				var next_month='<a class="btn passMont" href="?month=' + (month + 2) + '&amp;year=' + (year) + '" title="' + monthNames[month + 1] + ' ' + (year) + '">&raquo;&nbsp;' + monthNames[month + 1] + ' ' + (year) + '</a><br /><br />';
			}
				
			// previous month
			if(month==0){
				var prev_month='<a class="btn passMont" href="?month=' + 12 + '&amp;year=' + (year - 1) + '" title="' + monthNames[11] + ' ' + (year - 1) + '">' + monthNames[11] + ' ' + (year - 1) + '&nbsp;&laquo;</a>';
			} else{
				var prev_month='<a class="btn passMont" href="?month=' + (month) + '&amp;year=' + (year) + '" title="' + monthNames[month - 1] + ' ' + (year) + '">' + monthNames[month - 1] + ' ' + (year) + '&nbsp;&laquo;</a>';
			}		
				
			table+=('<span id="current-month">'+monthNames[month]+' '+year+'</span>');
			// uncomment the following lines if you'd like to display calendar month based on 'month' and 'view' paramaters from the URL
			table+=('<span class="nav-prev">'+ prev_month +'</span>');
			table+=('&nbsp;&nbsp;<span class="nav-next">'+ next_month +'</span>');
			table+=('<table class="calendar-month " ' +'id="calendar-month'+i+' " cellspacing="0">');	
		
			table += '<tr>';
			
			for (dia=0; dia<7; dia++){
				table += '<th class="weekday">' + dayNames[dia] + '</th>';
			}
			
			table += '</tr>';
		
			var days=getDaysInMonth(month,year);
            var firstDayDate=new Date(year,month,1);
            var firstDay=firstDayDate.getDay();
			
			var prev_days=getDaysInMonth(month,year);
            var firstDayDate=new Date(year,month,1);
            var firstDay=firstDayDate.getDay();
			
			var prev_m=month == 0 ? 11 :month-1;
			var prev_y=prev_m == 11 ? year - 1 :year;
			var prev_days=getDaysInMonth(prev_m, prev_y);
			firstDay=(firstDay == 0 && firstDayDate) ? 7 :firstDay;
	
			var i=0;
      for(diasCalendario=0;diasCalendario<42;diasCalendario++)
			{
        if((diasCalendario<firstDay))
				{
         	table += ('<td class="other-month"><span class="day">'+ (prev_days-firstDay+diasCalendario+1) +'</span></td>');
			  }
				else if
				((diasCalendario >= firstDay+getDaysInMonth(month,year)))
				{
					i=i+1;
          table+=('<td class="other-month"><span class="day">'+ i +'</span></td>');
          }
					else
					{
            table += ('<td class="current-month tdDay'+((diasCalendario-firstDay+1)+'_'+(1+month)+'_'+year)+'"><span class="SpDay'+((diasCalendario-firstDay+1)+'_'+year)+'">'+(diasCalendario-firstDay+1)+'</span></td>');
//								if(j-firstDay+1=='<?=date('d')?>') alert(j-firstDay+1)
           }
           if (diasCalendario%7==6)  table += ('</tr>');
         }
        table += ('</table>');
		el.html(table);
	}
	
	function getDaysInMonth(month,year)
	{
		var daysInMonth=[31,28,31,30,31,30,31,31,30,31,30,31];
		if((month==1)&&(year%4==0)&&((year%100!=0)||(year%400==0)))
		{
		  return 29;
		}
		else
		{
		  return daysInMonth[month];
		}
	}
	
	
// jQuery plugin initialisation
$.fn.calendarWidget=function(params){    
	calendarWidget(this, params);		
	return this; 
}; 

})(jQuery);

$(document).ready(function(){
	$("#divCalendar").calendarWidget({
	year:<?=($_REQUEST['year']?$_REQUEST['year']:date('Y'))?>,
	month:<?=($_REQUEST['month']?$_REQUEST['month']-1:(date('m')-1))?>
 });
 $("#divCalendar").slideDown(1000);
})

-->
</script>
<center><div id="divCalendar"><p>ATENCI�N: javascript no correctamente habilitado!.</p></div></center>
<?
//echo '<pre>';print_r($_REQUEST);
?>
