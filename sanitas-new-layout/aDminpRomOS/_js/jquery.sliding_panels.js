var panelWidth = 0;
var startPanel = 1;

$(document).ready(function() {
	/* progressive enhancement */
	$('.boxTabs .tabs').css('display','block');
	$('.boxTabs .panel_container .panel').css({'position':'absolute', 'height':'400px'});
	$('.boxTabs .panel_container .panels').css({'position':'absolute', 'top':'0px'});

	window.panelWidth = $('.boxTabs').width();

	/* Position the panels */
	$('.panel_container .panel').each(function(index){
		$(this).css({'width':window.panelWidth+'px','left':(index*window.panelWidth)});
		$('.boxTabs .panels').css('width',(index+1)*window.panelWidth+'px');
	});

	/* Add click events to tabs */
	$('.boxTabs .tabs span').each(function(){
		$(this).on('click', function(){
			changePanels($(this).index() );
		});
	});

	/* Trigger the starting panel */
	$('.boxTabs .tabs span:nth-child('+window.startPanel+')').trigger('click');
});


function changePanels(newIndex){
	var newPanelPosition = (window.panelWidth * newIndex) * -1;
	var newPanelHeight = $('.boxTabs .panel:nth-child('+(newIndex+1)+')').find('.panel_content').height() + 15;

	$('.boxTabs .tabs span').removeClass('selected');
	$('.boxTabs .tabs span:nth-child('+(newIndex+1)+')').addClass('selected');

	if(newIndex > 1)
	{
//		$('.panel_content').html('<hr/>'+"box"+newPanelHeight+"<br /><br />");
	}

	$('.boxTabs .panels').animate({left:newPanelPosition},500);
	$('.boxTabs .panel_container').animate({height:newPanelHeight},500);
}
