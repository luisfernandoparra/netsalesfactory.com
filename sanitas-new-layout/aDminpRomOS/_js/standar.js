var img_fondo_btn='xxfnd01.jpg';
var path='_img/';
var colStart;
var navegador = navigator.appName;
var color_foreground;
var color_fondo;
var noListar=0;
var paginarBloque=0;

function resaltarCampo(el)
{
	document.getElementById("tdTitularFormulario").innerHTML="REVISAR EL ERROR";
	document.getElementById(el.name).style.backgroundColor='red';
}

function restauraCampo(el)
{
	document.getElementById("tdTitularFormulario").innerHTML=textoCabecera;
	document.getElementById(el.name).style.backgroundColor='';
}

function controlNumerico(numero,campo,formulario,soloNumeros,borrarValor)	// RECIBE UN NUMERO Y EL NOMBRE DEL CAMPO DEL FORMULARIO A VERIFICAR
{
	hayError=false;
	if(!borrarValor) borrarValor=false;
	if(soloNumeros)
	{
		if(validarSoloNumero(numero))	// se permiten solo numeros
		{
			document.forms[formulario][campo].style.backgroundColor='red';
			alert('Por favor, revise el valor introducido, solo se permiten n�meros (sin espacios blancos)');
			document.forms[formulario][campo].style.backgroundColor='';
			hayError=true;
		}
	}
	else
		if(validarNumero(numero))	// se permiten solo numeros con decimales
		{
			document.forms[formulario][campo].style.backgroundColor='red';
			alert('Por favor, revise el valor introducido, solo se permiten n�meros (y el punto como separador DECIMAL)');
			document.forms[formulario][campo].style.backgroundColor='';
			hayError=true;
		}

	document.forms[formulario][campo].focus();
	if(borrarValor && hayError) document.forms[formulario][campo].value='';
	return hayError;
}

function validarNumero(numero)	// VERIFICA QUE LOS CARACTERES INTRODUCIDOS SEAN SOLO NUMEROS Y EL PUNTO(.)
{
	numero=''+numero+''; reserv="1234567890."; reserv+='"'; hayError=false;

	for(var cont=0; cont<numero.length; cont++)
	{
		control=numero.substring(cont,cont+1);
		if(reserv.indexOf(control)!=-1) continue;
		else hayError=true;
	}
	return hayError;
}

function validarSoloNumero(numero)	// VERIFICA QUE LOS CARACTERES INTRODUCIDOS SEAN SOLO NUMEROS Y EL PUNTO(.)
{
	numero=''+numero+''; reserv="1234567890"; reserv+='"'; hayError=false;

	for(var cont=0; cont<numero.length; cont++)
	{
		control=numero.substring(cont,cont+1);
		if(reserv.indexOf(control)!=-1) continue;
		else hayError=true;
	}
	return hayError;
}

function abreAyudas(pagina,texto)	// PAGINAS DE AYUDA PARA EL FORMULARIO ABIERTO
{
	var ancho=650;
	var alto=220;
	var x=(sc_an-ancho)/2;
	var y=(sc_al-alto)/2;
	ventana=window.open ('ayudas_backoffice_pop.php?nombre_pagina='+pagina+'&titulo='+texto,'ayudas','width='+(ancho+10)+',height='+(alto+15)+',left='+x+',top='+y+',scrollbars=yes');
	ventana.focus();
	return;
}

/*----------------------------------------------------------------------------------------
// PERMITE FILTRAR LAS OPTIONS DE LA SELECT 'id_select' CON EL CONTENIDO RECIBIDO DE 'nombre_busca_u'

id_select= ID DEL SELECT DONDE SE DEBER� EFECTUAL EL FILTRO
longitudVar= LONGITUD DEL TEXTO DEL PREFIJO UTILIZADO PARA DIFERENCIAR LAS VARIABLES ENTRE LAS FICHAS DE UNA MISMA SECCI�N
sufijoCampo= NOMBRE DEL TEXTO DEL PREFIJO UTILIZADO PARA DIFERENCIAR LAS VARIABLES ENTRE LAS FICHAS DE UNA MISMA SECCI�N
nombreTablaSelect= TABLA PARA OBTENER LAS 'OPTIONS' DE LA SELECT (LA SALIDA SER� UN TEXTO DESCRIPTIVO + ID)
nombreTablaFuente= TABLA DONDE EFECTUAR LAS B�SQUEDAS DEL TEXTO LIBRE RECIBIDO EN 'nombre_busca_u'
refIndiceId= NOMBRE DEL CAMPO DE LA TABLA '' A LIGAR CON 'id_select' EN LA QUERY DE RESULTADOS
nomreCampoBuscar= NOMBRE DEL CAMPO QUE CONTIENE EL TEXTO A BUSCAR DE LA TABLA 'nombreTablaFuente'
PhpCampoMostar= NOMBRE DEL CAMPO DE LA TABLA 'nombreTablaSelect' CON LA FUENTE DEL TEXTO A BUSCAR
nombreIframe=                                                        */
function actualizSelectBuscCap(id_select,longitudVar,sufijoCampo,nombreTablaFuente,nombreTablaSelect,refIndiceId,nomreCampoBuscar,PhpCampoMostar,nombreIframe)
{
	document.getElementById('ifrApoyoB').style.display='inline';document.getElementById('ifrApoyoB').style.top='0px';
	nombreCampo=sufijoCampo+nomreCampoBuscar+'_busca_u';
	textoBuscar=document.frmBuscar[nombreCampo].value;
	existeCaracterProhibido1=textoBuscar.lastIndexOf("\"");
	existeCaracterProhibido2=textoBuscar.lastIndexOf("'");
	spanCadenaEnunciadoId='spnBuscCapitulo';

	if(existeCaracterProhibido1>0 || existeCaracterProhibido2>0)
	{
		setTimeout("document.frmBuscar['"+sufijoCampo+nomreCampoBuscar+"_busca_u'].focus();",100);
		alert('Por favor, para efectuar b�squedas, no utilice las comillas simples o dobles en el texto');
		return;
	}
	identifNombreCampo=id_select.lastIndexOf('_buscN') - longitudVar;
	spnTitrTemp=document.getElementById('spnTitr').innerHTML;
	document.getElementById('spnTitr').innerHTML="<span style=color:red;background-color:#fff>&nbsp;<b>VERIFICANDO</b> esperar...&nbsp;</span>";
	document.frames['ifrApoyoB'].document.location=nombreIframe+'?accion=12&textoEncontrar='+textoBuscar+'&id_select='+id_select+'&campoFiltrar='+identifNombreCampo+'&nombreTablaSelect='+nombreTablaSelect+'&PhpCampoMostar='+PhpCampoMostar+'&PhpIdSelect=id&spanCadenaEnunciadoId='+spanCadenaEnunciadoId+'&nombreTablaFuenteDatos='+nombreTablaFuente+'&refIndiceId='+refIndiceId+'&nomreCampoBuscar='+nomreCampoBuscar;
	return;
}

function mostrarMensaje(id_control,textoMensaje)	// PARA LLAMAR LA ATENCI�N AL CAMBIAR EL CONTENIDO EN UN CAMPO DE TEXTO DEL BUSCADOR
{
	document.getElementById(id_control).innerHTML=textoMensaje;
	setTimeout("document.getElementById('"+id_control+"').innerHTML='"+textoMensaje+"';",2200);
}

function reportError(msg,url,line)
{return;
	var str = "Error de javascript:<br/>" + msg + "<br/>L�nea: <b>" + line + "</b><br/>File:<b> "+ url;
	var a=screen.width;
	var b=screen.height;
	var ancho=800;
	var alto=180;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var t=window.open ('../error.htm','error','width='+ancho+',height='+alto+',left='+x+',top='+y+',scrollbars=yes,resizable=yes');
	t.document.write(str);
	t.focus();
	return false;
}
window.onerror = reportError;

var sc_an=screen.width;
var sc_al=screen.height;

function agregarElemntoSelect(elSelect,laClase,valor)	// ABRE EL IFRAME PARA A�ADIR UN NUEVO ELEMENTO DEL CONTROL SELECCIONADO
{
	parametro='';
	if(valor) parametro='&'+valor+'';
	if(laClase) laClase='&clase_objeto='+laClase;
	else laClase='&clase_objeto=';
	
	if(!elSelect.id)
	{
		alert('ATENCI�N: falta poner el ID al control !!!'+elSelect.id);
		return;
	}

	document.frames['ifrApoyo'].document.location='iframe_opciones_select.php?nombreSelect='+elSelect.id+laClase+parametro;
	document.getElementById('ifrApoyo').style.top=(maxAlto/2-98)+'px';
	return;
}

function admin_abre_imagen(dir,ancho,alto)	// POP DE LAS IMAGENES DE ADMINISTRACION
{
	var x=(sc_an-ancho)/2;
	var y=(sc_al-alto)/2;
	imag=window.open (dir,'img','width='+(ancho+10)+',height='+(alto+15)+',left='+x+',top='+y);
}

function trim(cadena)
{
	for(i=0; i<cadena.length; )
	{
		if(cadena.charAt(i)==" ")
			cadena=cadena.substring(i+1, cadena.length);
		else
			break;
	}

	for(i=cadena.length-1; i>=0; i=cadena.length-1)
	{
		if(cadena.charAt(i)==" ")
			cadena=cadena.substring(0,i);
		else
			break;
	}
	
	return cadena;
}

function removeOptionSelected(nombreSelect)	// ELIMINA TODOS LOS ELEMENTOS DEL SELECT 'nombreSelect'
{
	var elSel = document.getElementById(nombreSelect);
	for(i=elSel.length ;i>=1;i--)	// CAMBIAR 'i>0' A 'i>=0' SI HAY QUE ELIMINAR TODAS LAS OPCIONES DEL SELECT O 'i>=0' SI HAY QUE QUITAR TAMBI�N 'seleccionar'
		elSel.remove(i);
}

function appendOptionLast(num,id,texto,nombreSelect)	// A�ADE ELEMENTOS (OPTIONS) A LA SELECT 'nombreSelect'
{
  var elOptNew = document.createElement('option');
  elOptNew.text = id;
  elOptNew.value = texto;
  var elSel = document.getElementById(nombreSelect);
  try	//intenta la instrucci�n siguiente, si lo consigue, ejecuta
	{
    elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
  }
  catch(ex)	// si falla la anterior instrucci�n, se ejecutar� la siguiente
	{
    elSel.add(elOptNew); // IE only
  }
}

function over_pro(scr,colorFondoOver,colorFore)
{
	color_foreground=scr.style.color;
	scr.style.cursor="pointer";
	scr.style.color=colorFore;
	color_fondo=scr.style.backgroundColor;
	scr.style.backgroundColor=colorFondoOver;
}

function out_pro(scr)
{
	scr.style.color=color_foreground;
	scr.style.backgroundColor=color_fondo;
}

function btn_over(scr,color)
{
	scr.style.backgroundColor='3D2E1C';
	colStart=scr.style.color;
	scr.style.color='#E1E4E7';
	scr.style.paddingBottom='1px';
	scr.style.cursor='pointer';
}

function btn_out(scr,color)
{
	scr.style.backgroundColor=color;
	scr.style.color='#000000';
	scr.style.paddingBottom='0px';
	scr.style.color=colStart;
}

function verificarMail(cadena,campo,formulario)
{
	punto=cadena.substring(cadena.lastIndexOf('.') + 1, cadena.length);	// cadena del .com
	dominio=cadena.substring(cadena.lastIndexOf('@') + 1, cadena.lastIndexOf('.'));	// dominio @lala.com
	usuario=cadena.substring(0, cadena.lastIndexOf('@'));	// cadena lalala@

	reserv="@/�'+*{}\<>?�[]�����#��!^*;,:`�";	// Letras reservadas
	reserv+='"';
	
	valido=true;
	// verifica qie el usuario no tenga un caracter especial
	for (var cont=0; cont<usuario.length; cont++)
	{
		controlUsuario=usuario.substring(cont,cont+1);
		if(reserv.indexOf(controlUsuario)!=-1)
			valido=false;
	}

	// verifica qie el punto no tenga un caracter especial
	for (var cont=0; cont<punto.length; cont++)
	{
		controlUsuario=punto.substring(cont,cont+1);
		if (reserv.indexOf(controlUsuario)!=-1)
			valido=false;
	}

	// verifica qie el dominio no tenga un caracter especial
	for (var cont=0; cont<dominio.length; cont++)
	{
		controlUsuario=dominio.substring(cont,cont+1);
		if (reserv.indexOf(controlUsuario)!=-1)
			valido=false;
	}

	// Verifica la sintaxis b�sica.....
	if (punto.length<2 || dominio <1 || cadena.lastIndexOf('.')<0 || cadena.lastIndexOf('@')<0 || usuario<1)
		valido=false;

	if(valido)
		return true;
	else
	{
		if(campo)
		{
//			if(document.forms[formulario])				resaltarCampo(document.forms[formulario][campo].name)
//			else
				resaltarCampo(campo);
		}
		alert('El correo indicado es incorrecto!');
		return false;
	}
}

getDimensions = function(oElement)	// OBJETO CON DIMENSIONES Y POSICI�N DE "oElement"
{
	var x, y, w, h;
	x = y = w = h = 0;
	if(document.getBoxObjectFor) // Mozilla
	{
		var oBox=document.getBoxObjectFor(oElement);
		x =oBox.x-1;
		w =oBox.width;
		y =oBox.y-1;
		h =oBox.height;
	}
	else if(oElement.getBoundingClientRect) // IE
	{
		var oRect=oElement.getBoundingClientRect();
		x =oRect.left-2;
		w =oElement.clientWidth;
		y =oRect.top-2;
		h =oElement.clientHeight;
}
	return {x: x, y: y, w: w, h: h};
}