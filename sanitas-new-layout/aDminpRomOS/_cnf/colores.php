<?php
//	personalizacion de colores del backoffice
//echo '['.($_SESSION['colorForeTitular']).']';
$colBorde='#64737C';
$colSolapaActivaFondo='#444';
$colPaginacionActivoFondo='#013A6B';
$col_fondo2='#013A6B';
$foreColTwo='#aaa';

$_SESSION['colorOverImportant']='red';
$_SESSION['colorOverListado']='#000';
$_SESSION['colorOverListadoFront']='#00089A';
$_SESSION['colorOverListadoFondo']='#fffccc';
$_SESSION['listadoFondoClaro']='#E3E3E3';
$_SESSION['listadoFondoOscuro']='#C8C8C8';
$_SESSION['listadoFondoClaroDisabled']='#EDCF8E';
$_SESSION['listadoFondoOscuroDisabled']='#F7E3B7';
$_SESSION['colorTableBorde']='#888';
$_SESSION['colorTableBordeLight']=$_SESSION['colorTableBordeLight']?$_SESSION['colorTableBordeLight']:'#909090';
$_SESSION['colorFondoWeb']=$_SESSION['colorFondoWeb']?$_SESSION['colorFondoWeb']:'#fff';
$_SESSION['colorFondoFlotante']='#fff';
$_SESSION['colorFondoDivFlotante']='#013461';
$_SESSION['colorTextoStd']=$_SESSION['colorTextoStd']?$_SESSION['colorTextoStd']:'#000';
$_SESSION['colorForeBotones']=$_SESSION['colorForeBotones']?$_SESSION['colorForeBotones']:'green';
$_SESSION['colorForeOverBotones']=$_SESSION['colorForeOverBotones']?$_SESSION['colorForeOverBotones']:'red';
$_SESSION['colorFondoBotones']=$_SESSION['colorFondoBotones']?$_SESSION['colorFondoBotones']:'#fff';
$_SESSION['colorFondoOverBotones']=$_SESSION['colorFondoOverBotones']?$_SESSION['colorFondoOverBotones']:'#A5C0D5';
$_SESSION['colorTextWarning']=$_SESSION['colorTextWarning']?$_SESSION['colorTextWarning']:'red';
$_SESSION['colorForeTitular']=$_SESSION['colorForeTitular']?$_SESSION['colorForeTitular']:'#000';
$_SESSION['colorFondoTitular']=$_SESSION['colorFondoTitular']?$_SESSION['colorFondoTitular']:'#ccc';
$_SESSION['colorFondoWarning']=$_SESSION['colorFondoWarning']?$_SESSION['colorFondoWarning']:'orange';
$_SESSION['colorTextHighli']=$_SESSION['colorTextHighli']?$_SESSION['colorTextHighli']:'green';
$_SESSION['colorTextLoose']=$_SESSION['colorTextLoose']?$_SESSION['colorTextLoose']:'#aaa';
$_SESSION['colorFondoCampos']=$_SESSION['colorFondoCampos']?$_SESSION['colorFondoCampos']:'#fff';
?>