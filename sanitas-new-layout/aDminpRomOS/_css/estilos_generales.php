<?php
header('Content-Type: text/html; charset=utf-8');
/* CSS estilos */
echo '
.botones2{color:#000;border-right:1px solid #A59B89;border-bottom:1px solid #111}
.listaSimple{color:#ccc;background-image:url(_img/fnd_tit_lista_off.gif);}

.ordDesc{background-image:url(_img/flecha_arriba.gif);background-position:right;background-repeat:no-repeat;}
.ordAsc{background-image:url(_img/flecha_abajo.gif);background-position:right;background-repeat:no-repeat;}

.ordEnable{cursor:pointer;}
.ordEnable:hover{color:'.$_SESSION['colorTextHighli'].';background-color:'.$_SESSION['colorFondoWarning'].';

-moz-transform:scale(1.2);-webkit-transform:scale(1.2);}
.ordAct{background-color:'.$colSolapaActivaFondo.';color:#fff}
.lkOff{color:'.$_SESSION['col_campo_fore_disabled'].';cursor:default;text-align:center;}

.borde{
border:1px solid '.$_SESSION['colorTableBordeLight'].';
background-color:'.$_SESSION['colorFondoWeb'].';
box-shadow: 0px -1px 1px 1px '.$_SESSION['colorTableBordeLight'].';
-moz-box-shadow:-1px -1px 4px '.$_SESSION['colorTableBordeLight'].';
-webkit-box-shadow:-1px -1px 4px '.$_SESSION['colorTableBordeLight'].';
border-top-left-radius:6px;
border-top-right-radius:6px;
}

.bordeRect{
border:1px solid '.$_SESSION['colorTableBordeLight'].';
background-color:'.$_SESSION['colorFondoWeb'].';
box-shadow: 0px 0px 2px 0px '.$_SESSION['colorTableBordeLight'].';
-moz-box-shadow:-1px 0px 4px '.$_SESSION['colorTableBordeLight'].';
-webkit-box-shadow:-1px 0px 4px '.$_SESSION['colorTableBordeLight'].';
}

.bordeHeader{
background-color:'.$_SESSION['colorFondoTitular'].';
}

#tblHeaderModuleEdit{display:block;color:'.$_SESSION['colorForeTitular'].';background-color:'.$_SESSION['colorFondoTitular'].';}

#tblHeaderModule{
border:1px solid #ccc;
border-top-right-radius:3px;
border-bottom-right-radius:26px;
color:'.$_SESSION['colorForeTitular'].';
background-color:'.$_SESSION['colorFondoTitular'].';
background-position:bottom;
background:-webkit-linear-gradient('.$_SESSION['colorFondoWeb'].','.$_SESSION['colorFondoTitular'].');
background:-moz-linear-gradient('.$_SESSION['colorFondoWeb'].','.$_SESSION['colorFondoTitular'].');
background:-o-linear-gradient('.$_SESSION['colorFondoWeb'].','.$_SESSION['colorFondoTitular'].');
border-top:0;border-left:0;
}

#tblHeaderModuleInverse{
border:1px solid #ccc;
border-top-left-radius:3px;
border-top-left-radius:26px;
color:'.$_SESSION['colorForeTitular'].';
background-color:'.$_SESSION['colorFondoTitular'].';
background-position:bottom;
background:-webkit-linear-gradient('.$_SESSION['colorFondoTitular'].','.$_SESSION['colorFondoWeb'].');
background:-moz-linear-gradient('.$_SESSION['colorFondoTitular'].','.$_SESSION['colorFondoWeb'].');
background:-o-linear-gradient('.$_SESSION['colorFondoTitular'].','.$_SESSION['colorFondoWeb'].');
border-bottom:0;
}

.tblEditModuleFixed{
height:22px;
}

#tblFilas tr:hover{
color:'.$_SESSION['colorOverListado'].';
cursor:pointer;
box-shadow: 0px 0px 2px 0px #000;
-moz-box-shadow:0 0 15px #000;
-webkit-box-shadow:0 0 15px #000;
}

#ifrFlotanteEdicion{padding-top:3px;}
#divEdicionFlotante{
z-index:100;
vertical-align:top;
margin-top:30px;
border-top-left-radius:6px;
border-top-right-radius:6px;
box-shadow: 0px 0px 2px 0px '.$_SESSION['colorTableBordeLight'].';
-moz-box-shadow:rgba(0,0,0,0.80) -5px -2px 20px 1px;
-webkit-box-shadow:0 0 20px #000;
background-color:'.$_SESSION['colorFondoTitular'].';
border:1px solid '.$_SESSION['colorTableBordeLight'].';
border-bottom:5px solid '.$_SESSION['colorTableBordeLight'].';
}

input, select, textarea, .tcalInput{
font-family: Geneva,Verdana,Arial;
font-size:11px;
text-decoration:none;
color:'.$_SESSION['colorTextoStd'].';
border:1px solid #777;
background-color:'.$_SESSION['colorFondoCampos'].';
}

.btn,.btn:active,.btn:visited{cursor:pointer;
border:1px solid '.$_SESSION['colorBtnBorder'].';text-align:center;text-decoration:none;color:'.$_SESSION['colorForeBotones'].';background-color:'.$_SESSION['colorFondoBotones'].';
padding-left:4px;padding-right:4px;-moz-border-radius:2px;-webkit-border-radius:2px;}

.btn:hover{background-color:'.$_SESSION['colorFondoOverBotones'].';color:'.$_SESSION['colorForeOverBotones'].';
-moz-transform:scale(1.1);-webkit-transform:scale(1.1);}

body{
background-color:'.$_SESSION['colorFondoWeb'].';
color:'.$_SESSION['colorTextoStd'].';
margin:0;padding:0;
font-family:Arial; font-size:.8em;
text-decoration:none;
}

.mbTabset a.mbTab.sel{
color:'.$_SESSION['colorForeBotones'].';
background-color:'.$_SESSION['colorFondoWeb'].';
background-image:none;
border:1px solid #888;
border-bottom:0;
border-left:0;
border-top-left-radius:6px;
border-top-right-radius:6px;
box-shadow: -1px -1px 1px 1px #bbb;
-moz-box-shadow:-2px -3px 3px #888;
-webkit-box-shadow:-2px -3px 3px #888;
}
.mbTabset a.mbTab.sel span{
border:0;
background-color:'.$_SESSION['colorFondoWeb'].';
background-image:none;
border-left:1px solid #888;
border-top-left-radius:6px;
border-top-right-radius:6px;
}

.fondoPagina{
background-color:'.$_SESSION['colorFondoWeb'].';
}
.boton,.boton:visited{font-size:11px;color:#000;border:1px solid #5F5F5F;height:16px;text-decoration:none;padding-left:10px;padding-right:10px}
.boton:hover{background-color:#000;color:#fff;padding-left:10px;padding-right:10px}
.boton:active{background-color:#fff;color:#000;padding-left:10px;padding-right:10px}

.btnRoundBottom{
width:100%;
border-bottom-left-radius:5px;
border-bottom-right-radius:5px;
}
';
?>
