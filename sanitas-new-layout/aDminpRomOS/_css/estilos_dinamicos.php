<?php
/* CSS estilos */
echo '
td.enc,td.enc2{
color:'.$_SESSION['colorForeTitular'].';
}

td.enc{
color:'.$_SESSION['colorForeTitular'].';
border-right:1px solid #ddd;
}

thead{
color:'.$_SESSION['colorForeTitular'].';
background-color:'.$_SESSION['colorFondoTitular'].';
}

tbody{color:'.$_SESSION['colorTextoStd'].';}
td.cont{border-right:1px solid #A59B89;}

td.enclargo, td.enc2largo{
color:#CCD2D6;
border-right:1px solid #000000;
font-weight:bold;
background-image:url(../_img/fnd_btn_out3.gif);
background-position:top;
background-repeat:repeat-x;
background-color:#E9F2F3;
border-bottom:1px solid #000000}

td.enc2largo{border-right:0;border-bottom:1px solid #000000}

#spnErr{display:block;padding:10px;border:1px solid red;text-align:center;color:#fff;background-color:orange;-moz-border-radius:6px;-webkit-border-radius:6px;font-size:1.2em;font-weight:bold;}

.lk,.lk:visited,.lk:active{
padding-left:4px;
padding-right:4px;
text-decoration:none;
background-color:#ccc;
color:'.$_SESSION['colorTextoStd'].';
border:0px solid #A2B4E4;
font-size:11px;
}

.lk:hover{
font-size:11px;
background-color:'.$_SESSION['colorOverListadoFondo'].';
color:'.$_SESSION['colorOverListado'].';
}

.xcancel{}
.xcancel:hover{border:1px solid '.$_SESSION['colorTextWarning'].';font-size:1.01em;}

.mbTabset a.mbTab.sel{
color:'.$_SESSION['colorTextoStd'].';
background-color:'.$_SESSION['colorFondoWeb'].';
background-image:none;
border:1px solid #888;
border-bottom:0;
border-left:0;
border-top-left-radius:6px;
border-top-right-radius:6px;
}
.mbTabset a.mbTab.sel span{
border:0;
background-color:'.$_SESSION['colorFondoWeb'].';
background-image:none;
border-left:1px solid #888;
border-top-left-radius:6px;
border-top-right-radius:6px;
}

#btnEsconderSubTabs
{
position:absolute;right:36px;top:22px;font-size:9px;display:none;float:right;
}

.implicitNotices ul{
list-style-type:square;
padding-left:2%;
margin:0em;
}

.implicitNotices li{
margin-bottom:0.1em;
}

.implicitNotices ol{
padding-left:2.2%;
list-style-type:decimal-leading-zero;
margin-top:4px;
}

';
?>
