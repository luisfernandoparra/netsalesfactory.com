<?php
header('Content-Type: text/html; charset=utf-8');
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/usuarios.class.php');
require_once('_inc/log.class.php');

$objeto=new Usuarios($sql, $admin);
$objeto->navegador=$objeto->ObtenerNavegador($_SERVER['HTTP_USER_AGENT']);
$objeto->filasporpagina=10;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=10;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=1;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
	$objeto->decimalesEnListado=0;	// NUMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICION DE LA VENTANA RESPECTO A LAS DEMOS EN SU MISMA SECCION O AREA
$objeto->editarFila=0;
$objeto->permiteEliminarGlobal=1;
$nombreFilePhpBase='usuarios';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR

$txtTitularFormularioNewRecord='Usuarios';
$txtTitularBuscador='Buscar usuarios';
$txtMsgStandard='Usuarios presentes: ';
$txtMsgREcordInsertado='<span style=color:lightgreen><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:yellow><b>Datos almacenados correctamente&nbsp;</b></span>';
$txtTitularEdicion='Edici&oacute;n de usuario';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>