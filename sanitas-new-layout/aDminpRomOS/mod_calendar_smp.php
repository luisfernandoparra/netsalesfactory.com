<?php
@session_start();
//include_once('cabeceraadmin_logica.php');
$opcionesCalendario=explode(',',$_REQUEST['calendarVars']);
$arraCalendarParams[]='year';
$arraCalendarParams[]='month';

if(count($opcionesCalendario))	// SE DESGLOSAN LOS PAR�METROS DEL M�DULO PARA SER PROCESADOS
{
	foreach($opcionesCalendario as $key=>$value)
	{
		$_REQUEST[$arraCalendarParams[$key]]=$value;
	}
}

?>
<style>
<!--
#divCalendar{display:none;width:480px;float:right;margin-right:10px;margin-bottom:10px;margin-left:20px;
}

.tblInfoCal{
height:24px;
font-size:100%;
line-height:24px;
background:-webkit-gradient(linear, 0% 0%, 0% 100%,from(#999),to(#555),color-stop(.5,#333));
background:-moz-linear-gradient(top,#666 0%,#333 60%,#555 100%);
/*
background:-webkit-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
background:-moz-linear-gradient(<?=$_SESSION['colorFondoTitular']?>,<?=$_SESSION['colorFondoOverBotones']?>);
background:-o-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
*/
}

.actualDate{
border:0px;
width:25%;
padding:0;
font-weight:bold;
color:<?=$_SESSION['colorForeTitular']?>;
}

.calendar-month{
border-top-left-radius:2px;
border-top-right-radius:2px;
-moz-box-shadow:-2px 2px 6px <?=$_SESSION['colorTableBordeLight']?>;
-webkit-box-shadow:-2px 2px 6px <?=$_SESSION['colorTableBordeLight']?>;
}

.SpDay<?=number_format(date('d'),0)?>{}

/* FECHA D�A ACTUAL*/
.tdDay<?=number_format(date('d'),0,'','').'_'.number_format(date('m'),0,'','').'_'.date('Y')?>{
color:<?=$_SESSION['colorTextHighli']?>;
background-color:<?=$_SESSION['colorFondoCampos']?>;


/*
background:-webkit-gradient(linear, 0% 0%, 0% 100%,from(#999),to(#333),color-stop(.5,#333));
background:-moz-linear-gradient(top,#999 0%,#333 50%,#333 100%);

border:1px solid <?=$_SESSION['colorTableBorde']?>;

-moz-box-shadow:-1px -1px 4px <?=$_SESSION['colorTableBordeLight']?>;
-webkit-box-shadow:-1px -1px 4px <?=$_SESSION['colorTableBordeLight']?>;

background:-moz-linear-gradient(<?=$_SESSION['colorFondoTitular']?>,<?=$_SESSION['colorFondoOverBotones']?>);
background:-webkit-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
background:-o-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);

border-radius:4px;
*/
font-weight:bold;
font-size:120%;
overflow:hidden;
}

.weekday{
font-weight:normal !important;
padding:3px;
border-top:1px solid <?=($_SESSION['colorTableBorde']?$_SESSION['colorTableBorde']:'#333')?>;
background-color: #333;
background:-webkit-gradient(linear, 0% 0%, 0% 100%,from(#999),to(#333),color-stop(.5,#333));
background:-moz-linear-gradient(top,#999 0%,#333 50%,#333 100%);
}

.passMont{}

.current-month<?=number_format(date('d'),0,'','').'_'.number_format(date('m'),0,'','').'_'.date('Y')?>{
background:-webkit-gradient(linear, 0% 0%, 0% 100%,from(<?=$_SESSION['colorFondoBotones']?>),to(<?=$_SESSION['colorFondoBotones']?>),color-stop(.5,<?=$_SESSION['colorFondoWeb']?>));
background:-moz-linear-gradient(top,<?=$_SESSION['colorFondoBotones']?> 0%,<?=$_SESSION['colorFondoBotones']?> 0%,<?=$_SESSION['colorOverListadoFondo']?> 60%);
background-color:<?=$_SESSION['colorFondoBotones']?>;
-moz-box-shadow:-2px -2px 4px <?=$_SESSION['colorTableBordeLight']?>;
-webkit-box-shadow:-2px -2px 4px <?=$_SESSION['colorTableBordeLight']?>;

}
.current-month{
background-color:<?=$_SESSION['colorFondoWeb']?>;

background:-moz-linear-gradient(<?=$_SESSION['colorFondoTitular']?>,<?=$_SESSION['colorForeTitular']?>);
background:-webkit-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
background:-o-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
}

#current-month{display:block;position:relative;margin:0px;text-align:left;font-weight:bold;}

.calendar-month td,th{
border-left:1px solid <?=($_SESSION['colorTableBorde']?$_SESSION['colorTableBorde']:'#333')?>;
border-bottom:1px solid <?=($_SESSION['colorTableBorde']?$_SESSION['colorTableBorde']:'#333')?>;
width:70px;
padding:4px 0 4px;
text-align:center;
}

table{
border-right:1px solid <?=($_SESSION['colorTableBorde']?$_SESSION['colorTableBorde']:'#333')?>;
}

th{
background:<?=$_SESSION['colorFondoTitular']?>;
color:<?=$_SESSION['colorForeTitular']?>;
}

.other-month{
color:<?=$_SESSION['colorTextLoose']?>;
background-color:<?=$_SESSION['colorFondoTitular']?>;
background:-moz-linear-gradient(<?=$_SESSION['listadoFondoOscuro']?>,<?=$_SESSION['colorTableBorde']?>);
background:-webkit-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
background:-o-linear-gradient(<?=$_SESSION['colorFondoWeb']?>,<?=$_SESSION['colorFondoTitular']?>);
}
-->
</style>

<script type="text/javascript">

if(typeof jQuery == 'undefined'){
	alert("NO se ha cargado jquery, necesario para 'mod_calendar_smp.php'");
}

(function($){
	function calendarWidget(el,params){
		var now=new Date();
		var thismonth=now.getMonth();
		var thisyear=now.getYear()+1900;

		var opts={
			month:thismonth,
			year:thisyear
		};

		$.extend(opts,params);
		var monthNames=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
		var dayNames=['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'];
		month=i=parseInt(opts.month);
		year=parseInt(opts.year);
		var m=0;
		var table='';

			// next month
			if(month==11){
				var next_month='<a class="btn passMont" href="?month=' + 1 + '&amp;year=' + (year + 1) + '" title="' + monthNames[0] + ' ' + (year + 1) + '">' + monthNames[0] + ' ' + (year + 1) + '</a>';
			} else{
				var next_month='<a class="btn passMont" href="?month=' + (month + 2) + '&amp;year=' + (year) + '" title="' + monthNames[month + 1] + ' ' + (year) + '">' + monthNames[month + 1] + ' ' + (year) + '</a>';
			}

			// previous month
			if(month==0){
				var prev_month='<a class="btn passMont" href="?month=' + 12 + '&amp;year=' + (year - 1) + '" title="' + monthNames[11] + ' ' + (year - 1) + '">' + monthNames[11] + ' ' + (year - 1) + '</a>';
			} else{
				var prev_month='<a class="btn passMont" href="?month=' + (month) + '&amp;year=' + (year) + '" title="' + monthNames[month - 1] + ' ' + (year) + '">' + monthNames[month - 1] + ' ' + (year) + '</a>';
			}

			table+=('<table class="calendar-month " ' +'id="calendar-month'+i+' " cellspacing="0">');

			table += '<tr>';

			for (dia=0; dia<7; dia++){
				table += '<th class="weekday">' + dayNames[dia] + '</th>';
			}

			table += '</tr>';

			var days=getDaysInMonth(month,year);
            var firstDayDate=new Date(year,month,1);
            var firstDay=firstDayDate.getDay();

			var prev_days=getDaysInMonth(month,year);
            var firstDayDate=new Date(year,month,1);
            var firstDay=firstDayDate.getDay();

			var prev_m=month == 0 ? 11 :month-1;
			var prev_y=prev_m == 11 ? year - 1 :year;
			var prev_days=getDaysInMonth(prev_m, prev_y);
			firstDay=(firstDay == 0 && firstDayDate) ? 7 :firstDay;

			var i=0;
			for(diasCalendario=0;diasCalendario<42;diasCalendario++)
			{
				if((diasCalendario<firstDay))
				{
					table += ('<td class="other-month"><span class="day">'+ (prev_days-firstDay+diasCalendario+1) +'</span></td>');
				}

				else if
				((diasCalendario>=firstDay+getDaysInMonth(month,year)))
				{
					i=i+1;
					table += ('<td class="other-month"><span class="day">'+ i +'</span></td>');
				}
				else
				{
					table += ('<td class="current-month'+((((diasCalendario-firstDay+1)+'_'+(1+month)+'_'+year)==("<?=number_format(date('d'),0,'','').'_'.number_format(date('m'),0,'','').'_'.date('Y')?>")?(diasCalendario-firstDay+1)+'_'+(1+month)+'_'+year:""))+' tdDay'+((diasCalendario-firstDay+1)+'_'+(1+month)+'_'+year)+'"><span class="SpDay'+((diasCalendario-firstDay+1)+'_'+year)+'">'+(diasCalendario-firstDay+1)+'</span></td>');
				}
				if (diasCalendario%7==6)  table += ('</tr>');
			}

		table+=('<tr><td colspan="7" style="border-top:0px;padding:0;text-align:right" valign="top">');
		table+=('<table width="100%" cellpadding="0" cellspacing="0" border="0" class="tblInfoCal"><tr valign="top">');
		table+=('<td class="actualDate">'+ monthNames[month]+' '+year+'</td>');
		table+=('<td style="border:0px;padding:0;width:10%" align="center">'+prev_month+'</td>');
		table+=('<td style="border:0px;padding:0;width:6%" align="center"><a href="?month=<?=date('m')?>&amp;year=<?=date('Y')?>" title="Ver el calendario de hoy" class="btn">Hoy</a></td>');
		table+=('<td style="border:0px;padding:0;width:10%" align="center">'+next_month+'</td>');
		table+=('</tr></table>');
		table+=('</td></tr>');
		table += ('</table>');

		el.html(table);
	}

	function getDaysInMonth(month,year) {
		var daysInMonth=[31,28,31,30,31,30,31,31,30,31,30,31];
		if((month==1)&&(year%4==0)&&((year%100!=0)||(year%400==0))){
		  return 29;
		}else{
		  return daysInMonth[month];
		}
	}

	// jQuery plugin initialisation
	$.fn.calendarWidget=function(params){
		calendarWidget(this, params);
		return this;
	};
})(jQuery);

$(document).ready(function(){
	$("#divCalendar").calendarWidget({
	year:<?=($_REQUEST['year']?$_REQUEST['year']:date('Y'))?>,
	month:<?=($_REQUEST['month']?$_REQUEST['month']-1:(date('m')-1))?>
 });
  setTimeout('$("#divCalendar").slideDown(500);',200);

})

</script>

<?php



echo '<center><div id="divCalendar"><h3>ATENCI&Oacute;N: error en javascript!</h3></div></center>';
//echo '(mod_calendar_smp.php)<pre>';print_r($_REQUEST);
?>
