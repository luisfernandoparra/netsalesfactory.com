﻿<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/backoffice_tabs_jquery.class.php');
require_once('_inc/log.class.php');
$nombreFilePhpBase='backoffice_tabs_jquery';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$objeto=new backofficeTabsJquery($sql, $admin);
$objeto->debug=0;
$objeto->navegador=$objeto->ObtenerNavegador($_SERVER['HTTP_USER_AGENT']);
$objeto->nombreFilePhpBase=$nombreFilePhpBase;	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR

$objeto->filasporpagina=8;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=20;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=1;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// NÚMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$objeto->modalHeight='400px';
$objeto->modalWidth='850px';
$objeto->modalLeftPos='11%';
$objeto->innerTextHeight='400';
$objeto->editarFila=true;

$txtTitularFormularioNewRecord='Insertar un nuevo tab jQuery (contenido incrustado)';
$txtTitularBuscador='Buscar tabs jQuery';
$txtMsgStandard='Tabs jQuery presentes: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';
$txtTitularEdicion='Editar tabs jQuery';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>
