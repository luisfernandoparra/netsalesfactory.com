<?php
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/idiomas_meses.class.php');
require_once('_inc/log.class.php');
$nombreFilePhpBase='idiomas_meses';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR

$_REQUEST['id_grabar']=$_REQUEST['id'];	// PARA GRBAR EL "ID" SEA CUAL SEA LA TABLA
$_REQUEST['logEstado']='process delete start';
new Log($sql, $id_pagina);

$objeto=new IdiomasMeses($sql, $admin);
$objeto->carga($_REQUEST['id']);

$txtMsgEndProcess='</b><i style=color:black;background-color:orange;>| Registro eliminado correctamente |</i>';
$txtLogDeleteAbort='deletion canceled';
$txtLogDeleteOK='deleted record';

include('_inc/common_form_delete.php');
?>