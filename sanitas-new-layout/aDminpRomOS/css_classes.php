﻿<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/css_classes.class.php');
require_once('_inc/log.class.php');

$objeto=new cssClasses($sql, $admin);
$objeto->filasporpagina=15;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=20;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=1;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// NÚMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$objeto->editarFila=1;
$nombreFilePhpBase='css_classes';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$objeto->nombreFilePhpBase='css_classes';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$objeto->debug=0;
$objeto->modalWidth='700px';
$objeto->modalHeight='170px';
$objeto->modalLeftPos='15%';
$objeto->editarFila=1;	// SE OMITE LA EDICIÓN DE REGISTROS
$objeto->permiteEliminarGlobal=0;
$objeto->minLevelSpecialEdit=14;
$objeto->ocultarEliminar=$_SESSION['usuarioNivel'] > 15 ? 0 : 1;

$txtTitularFormularioNewRecord='Visualizar las clases disponibles (utilizados para las modales incrustadas)';
$txtTitularBuscador='Buscar clases CSS';
$txtMsgStandard='Clases CSS presentes: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';
//$txtTitularEdicion='Editar participante';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>
