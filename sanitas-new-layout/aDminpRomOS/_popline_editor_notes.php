﻿<?php
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html>
<head>
<title>Actual server <?=$_SERVER['SERVER_NAME']?></title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<!-- START POPLINE -->
<link rel='stylesheet' href='_css/popline_themes/default.css' type='text/css' />
<link rel='stylesheet' href='_css/normalize.css' type='text/css' />
<link rel='stylesheet' href='_css/toggle-switch.css' type='text/css' />
<link rel='stylesheet' href='font-awesome/css/font-awesome.min.css' type='text/css' />
<!-- END POPLINE -->

<script type="text/javascript" src="_js/jquery-1-8-2.min.js"></script>

<!-- START POPLINE -->
<script type='text/javascript' src='_js/jquery.popline.min.js'></script>
<!-- END POPLINE -->

<script type="text/javascript">
$(document).ready(function(){
	$(".editorPopline").popline();
})
</script>

</head>
<body>


<div dir="ltr" class="editorPopline" style="padding:20px;padding-top:0px;padding-bottom:0px;line-height:1.5em;">
	<p>versión 1.0, 31.01.2014 M.F.</p>
	Cambios para incluir en el back-office <i>popline</i> (<b>edición directa&nbsp;</b><b>on-line de textos html</b>):
	<br /><br />
	<div>Files a incluir:</div>
	<div>_css/popline_themes/default.<wbr>css<br></div>
	<div>_css/popline_themes/default.<wbr>png<br></div>
	<div>_css/popline_themes/default.<wbr>scss<br></div>
	<div>_css/normalize.css<br></div>
	<div>_css/toggle-switch.css</div>
	<div>
		<div>
			<div>font-awesome/css/font-awesome.<wbr>min.css<b><br></b></div>
		</div>
		<div>
			<div>font-awesome/css/font-awesome.<wbr>css<b><br>
				</b>
			</div>
		</div>
		<div>
			<div>font-awesome/css/font-awesome-<wbr>ie7.css<b><br></b></div>
		</div>
		<div>
			<div>font-awesome/css/font-awesome-<wbr>ie7.min.css<b><br></b></div>
		</div>
		<div>
			<div>
				<div>
					<div>font-awesome/font/FontAwesome.<wbr>otf<b><br></b></div>
				</div>
			</div>
		</div>
		<div>
			<div>font-awesome/font/fontawesome-<wbr>webfont.eot<b><br></b></div>
		</div>
		<div>
			<div>font-awesome/font/fontawesome-<wbr>webfont.svg<b><br></b></div>
		</div>
		<div>
			<div>font-awesome/font/fontawesome-<wbr>webfont.ttf<b><br>
				</b>
			</div>
		</div>
		<div>
			<div>font-awesome/font/fontawesome-<wbr>webfont.woff<b><br></b></div>
		</div>
		<br /><br />
		<div>_js/jquery.popline.min.js<br></div>
		<br /><br />
		<div>Detalles de los scripts afectados:</div>
		<div><b>======================</b><br></div>
		<div><b>===&gt;_inc/</b><b>cabeceraadmin.php<br></b></div>
		<br /><br />
		<div><b><font color="#ff0000">linea 21:</font></b><br></div>
		<div>
			<div>&lt;!-- START POPLINE --&gt;</div>
			<div>&lt;link rel='stylesheet' href='_css/popline_themes/<wbr>default.css' type='text/css' /&gt;</div>
			<div>&lt;link rel='stylesheet' href='_css/normalize.css' type='text/css' /&gt;</div>
			<div>&lt;link rel='stylesheet' href='_css/toggle-switch.css' type='text/css' /&gt;</div>
			<div>&lt;link rel='stylesheet' href='font-awesome/css/font-<wbr>awesome.min.css' type='text/css' /&gt;</div>
			<div>&lt;!-- END POPLINE --&gt;</div>
		</div>
		<br /><br />
		<div>
			<div><b><font color="#ff0000">linea 60: (despues de cargar jQuery, min v.1.8)</font></b><br></div>
			<div></div>
			<div>&lt;!-- START POPLINE --&gt;</div>
			<div>&lt;script type='text/javascript' src='_js/jquery.popline.min.<wbr>js'&gt;&lt;/script&gt;</div>
			<div>&lt;!-- END POPLINE --&gt;</div>
		</div>
		<br /><br />
		<div>
			<div><b><font color="#ff0000">linea 127 (circa, en el <i>$(document).ready</i>):</font></b><br></div>
			<div></div>
			<div>$(".editorPopline").popline(); // POPLINE INIT</div>
		</div>
		<br /><br />
		<div><b>========================</b></div>
		<div><b>===&gt;</b><b>editor/editorColorBox.php</b><br></div>
		<div>
			<div><b><font color="#ff0000"><br></font></b></div>
			<div>
				<b><font color="#ff0000">linea 20:</font></b>
			</div>
			<div><span style="white-space:pre-wrap">		</span>echo '</div>
			<div>&lt;script language="JavaScript"&gt;</div>
			<div><span style="white-space:pre-wrap">	</span>parent.$.colorbox.close();';</div>
			<div><span style="white-space:pre-wrap">		</span>if(substr($_REQUEST['<wbr>nombreCampo'],0,5) == 'data_')<span style="white-space:pre-wrap">	</span>// POPLINE SCRIPT CONTENT (31.01.2014)</div>
			<div><span style="white-space:pre-wrap">			</span>echo 'parent.$("#data_texto").html(<wbr>\''.html_entity_decode($datos)<wbr>.'\');';</div>
			<div><span style="white-space:pre-wrap">		</span>else</div>
			<div><span style="white-space:pre-wrap">			</span>echo 'parent.document.'.$_REQUEST['<wbr>nombreFormulario'].'.'.$_<wbr>REQUEST['nombreCampo'].'.<wbr>value="'.($datos).'";';</div>
			<div><span style="white-space:pre-wrap">		</span>echo '&lt;/script&gt;';</div>
		</div>
		<br /><br />
		<div><b><font color="#ff0000">linea 132:</font></b></div>
		<br /><br />
		<div>
			<div>echo '&lt;script&gt;';</div>
			<div>if(substr($_REQUEST['<wbr>nombreCampo'],0,5) == 'data_')<span style="white-space:pre-wrap">	</span>// POPLINE SCRIPT CONTENT (31.01.2014)</div>
			<div><span style="white-space:pre-wrap">	</span>echo 'document.xxxxxxx.elm1.value=<wbr>parent.$("#data_texto").html()<wbr>;';</div>
			<div>else</div>
			<div><span style="white-space:pre-wrap">	</span>echo 'document.xxxxxxx.elm1.value=<wbr>parent.document.'.$_REQUEST['<wbr>nombreFormulario'].'.'.$_<wbr>REQUEST['nombreCampo'].'.<wbr>value;';</div>
			<div>echo '&lt;/script&gt;';</div>
		</div>
		<br /><br />
		<div><b>======================</b><br></div>
		<div><b>===&gt;_inc/form_std_edit.php</b><br></div>
		<div><b><font color="#ff0000"><br></font></b></div>
		<div><b><font color="#ff0000">linea 70:</font></b></div>
		<div>
			<div>function popeditorSaveData(formName)<span style="white-space:pre-wrap">	</span>// POPLINE TRANSFERT DATA TO TEXTAREA FIELD (31.01.2014)</div>
			<div>{</div>
			<div><span style="white-space:pre-wrap">	</span>var contentPopline="";</div>
			<div><span style="white-space:pre-wrap">	</span>$("form[name="+formName+"] textarea").each(function(el,<wbr>xx){</div>
			<div><span style="white-space:pre-wrap">		</span>contentPopline=($("#data_"+$(<wbr>xx).attr("name")).html());</div>
			<div><span style="white-space:pre-wrap">		</span>$(xx).html(contentPopline);</div>
			<div><span style="white-space:pre-wrap">	</span>})</div>
			<div><span style="white-space:pre-wrap">	</span>document[formName].submit();</div>
			<div>}</div>
		</div>
		<br /><br />
		<div>
			<div><b>=========================</b></div>
			<div><b>===&gt;_inc/<i>script clase a modificar</i></b><br></div>
			<div><font color="#ff0000"><br></font></div>
			<div><font color="#ff0000">- function guarda_datos()</font><br></div>
		</div>
		<div>utf8_decode($this-&gt;<font color="#0000ff">CAMPO</font>)<br></div>
		<br /><br />
		<div><font color="#ff0000">-&nbsp;function formulario_datos($nombreForm)</font></div>
		<div>para <b>cada campo</b> textarea a modificar</div>
		<div>
			<div><span style="white-space:pre-wrap">	</span>$dataPopline=str_replace("\r\<wbr>n",'',($this-&gt;<font color="#0000ff">CAMPO</font>));</div>
			<div><span style="white-space:pre-wrap">	</span>$dataPopline=html_entity_<wbr>decode($dataPopline);</div>
			<div><span style="white-space:pre-wrap">	</span>$dataPopline=htmlspecialchars_<wbr>decode($dataPopline);</div>
			<div><span style="white-space:pre-wrap">	</span>$this-&gt;<font color="#0000ff">CAMPO</font>=utf8_decode($<wbr>this-&gt;<font color="#0000ff">CAMPO</font>);</div>
		</div>
		<br /><br />
		<div>- Editar los <b>TAGS</b> donde se encuentre el campo/s a gestionar:</div>
		<div>
			<div><span style="white-space:pre-wrap">	</span>&lt;div id="data_<font color="#0000ff">CAMPO</font>" class='editorPopline' contenteditable='true' style="display:block;border:<wbr>1px solid #999;width:&lt;?=$this-&gt;id ? '450px' : '440px';?&gt;;height:&lt;?=$this-&gt;id ? '280px' : '100px';?&gt;;overflow:auto;"&gt;&lt;?=<wbr>$dataPopline?&gt;&lt;/div&gt;</div>
			<div><span style="white-space:pre-wrap">	</span>&lt;textarea id='<font color="#0000ff">CAMPO</font>' name='<font color="#0000ff">CAMPO</font>' style='display:none;' &gt;&lt;?=$dataPopline?&gt;&lt;/textarea&gt;</div>
			<br />- Se debe cambiar también la referencia del campo que se envía al editor <b>"tiny_mce"</b> por medio del parámetro <i>HREF</i> del botón; ejemplo:
			<br /><i>HREF="</i>... &nombreCampo=<font color="#0000ff">CAMPO</font>" title="Editor completo">HTML</a> ...
			<br />
			<i>HREF="</i>... &nombreCampo=<b>data_</b><font color="#0000ff">CAMPO</font>" title="Editor completo">HTML</a> ...
			
		</div>
		<br /><br />
		<div><font color="#ff0000">-&nbsp;function form_editar_datos($destino,$<wbr>accion,$titulo)</font></div>
		<div>sustituir</div>
		<div>
			<div><span style="white-space:pre-wrap">	</span>document.f&lt;?=$idunico?&gt;.<wbr>submit();</div>
			<div>por</div>
			<div><span style="white-space:pre-wrap">	</span>popeditorSaveData("f&lt;?=$<wbr>idunico?&gt;");</div>
			<div class="yj6qo"></div>
			<div class="adL"><br></div>
		</div>
		<div class="adL"><br></div>
	</div>
</div>

</body>
</html>
