﻿<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/ayudas_backoffice.class.php');
require_once('_inc/log.class.php');
$nombreFilePhpBase='ayudas_backoffice';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR

$objeto=new ayudas_backoffice($sql, $admin);
$objeto->nombreFilePhpBase=$nombreFilePhpBase;	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR

$objeto->navegador=$objeto->ObtenerNavegador($_SERVER['HTTP_USER_AGENT']);
$objeto->filasporpagina=10;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=20;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=1;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// NÚMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$objeto->modalHeight='400px';
$objeto->modalWidth='800px';
$objeto->modalLeftPos='11%';
$objeto->innerTextHeight='400';
$objeto->editarFila=0;

$txtTitularFormularioNewRecord='Ayudas para el Back-Office';
$txtTitularBuscador='Buscar ayudas del backoffice';
$txtMsgStandard='Ayudas presentes: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';
$txtTitularEdicion='Editar página de ayuda del Back-Office';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>