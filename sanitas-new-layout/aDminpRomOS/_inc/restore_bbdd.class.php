<?php
class restoreBBDD extends Base
{
	//Datos propios de la clase
	var $tamanioMaximoArchivo;
	var $path_archivos;
	var $posicionSolapa;	// POSICION DE LA VENTANA RESPECTO A LAS DEMAS DE SU MISMA SECCION E IDENTIFICADOR PARA LOS CAMPOS DE LAS BUSQUEDAS
	var $mensajeSalida;

//******************************************************
//	INICIALIZACION DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->mensajeSalida=NULL;
		$this->temporalSql='tmp_ddbb.txt';
		$this->temporalFolder='../tmp_files/';
		$this->sql=$sql;
	}

//******************************************************
//	EJECUTAR EL BACKUP DE LA BBDD
//******************************************************
	function ejecutarRestore()
	{
		set_time_limit(200);
		if($_FILES['archivo_restaurar']['name'])
		{
			if($_FILES['archivo_restaurar']['type']!='text/plain')
			{
				$this->mensajeSalida='<b>ERROR:</b> ARCHIVO NO SOPORTADO (<i> <b>'.$_FILES['archivo_restaurar']['name'].'</b> </i>), SOLO ARCHIVOS DE TEXTO PLANO CON EXTENSION \"TXT\"';
				$_REQUEST['logEstado']='Restore FAILED: no supported file';
				set_time_limit(30);
				return;
			}
		}

		$tamanio=$_FILES['archivo_restaurar']['size'];

		if($tamanio>$this->tamanioMaximoArchivo)	// SE HA SUPERADO EN BYTES EL TAMA�O MAXIMO PERMITIDO PARA EL OBJETO DE FLASH
		{
			$tamanio=number_format(($tamanio/1000),2,',','.').'Kb';
			$this->mensajeSalida='ERROR: el archivo es demasiado grande: <b>'.$tamanio.'</b> (MAX: '.($this->tamanioMaximoArchivo/1000).'Kb)';
			$_REQUEST['logEstado']='Restore FAILED: file too large';
			set_time_limit(30);
			return;
		}

		$archivoRestaurar=$this->quitar_acentos($_FILES['archivo_restaurar']['name']);
		$archivoRestaurar=$this->limpianombre($archivoRestaurar);

		if(is_executable($_FILES['archivo_restaurar']['tmp_name']))	// SE HA SUPERADO EN BYTES EL TAMA�O MAXIMO PERMITIDO PARA EL OBJETO DE FLASH
		{
			$this->mensajeSalida='ERROR: el archivo <b>'.$archivoRestaurar.'</b> parece ser un ejecutable, verifique el contenido del archivo.<br /><br />Imposible proseguir.';
			$_REQUEST['logEstado']='Restore FAILED: executable file';
			set_time_limit(30);
			return;
		}

		if($_REQUEST['accion']==6)	// SE OBTIENEN LOS DATOS DEL FILE PRESENTE EN EL SERVIDOR CON UN NOMBRE DE BBDD DISTINTO AL ACTUAL
		{
			$fp=fopen($this->temporalFolder.$this->temporalSql,'r');
			$contents='';
			do
			{
				$data=fread($fp,8192);
				if(strlen($data)==0)
					break;
				$contents.=$data;
			}
			while(true);
		}
		else
		{
			$fp=fopen($_FILES['archivo_restaurar']['tmp_name'],'r');
			$contents='';
			do
			{
				$data=fread($fp,8192);
				if(strlen($data)==0)
					break;
				$contents.=$data;
			}
			while(true);
		}

//		$contents=substr($contents,strpos($contents,'drop table'));
		$contentsPulido=substr($contents,strpos($contents,'drop table'));

		$sentenciasSql=explode(";\r",$contents);	// SE SEPARAN POR PUNTO Y COMA + RETORNOS LAS SENTENCIAS SQL
		sort($this->tablasOmitidas);

		if(count($sentenciasSql)==1)	// SI NO SE HA OBTENIDO CORRECTAMENTE EL ARRAY, SE PROCEDE A CREARLO NUEVAMENTE
		{
			$sentenciasSql=explode(";\n",$sentenciasSql[0]);	// SE SEPARAN POR PUNTO Y COMA + RETORNOS DUROS LAS SENTENCIAS SQL
			$sentenciasSqlPulido=explode(";\n",$contentsPulido);	// SE SEPARAN POR PUNTO Y COMA + RETORNOS LAS SENTENCIAS SQL
		}

		if(!count($_REQUEST['tablaRestaurar']))	// NO SE SELECCIONARON TABLAS PARA RESTAURAR
		{
			set_time_limit(30);
			$this->mensajeSalida='<h3 style=color:orange;background-color:#fff;width:300px;padding:30px;><b>ADVERTENCIA:</b> No se seleccion&oacute; ni una sola tabla para ejecutar su restauraci&oacute;n.<br /><br />No se ha restaurado NADA!</h3>';
			$_REQUEST['logEstado']='Restore FAILED: no tables selected';
			return;
		}

		if(!strpos(substr($contents,0,200),$_REQUEST['nombre_bbdd']) && $_REQUEST['accion']!=6)	// SE VERIFICA QUE LA BBDD SEA LA CORRECTA
		{
			set_time_limit(30);
			$cadenaBuscar='# Database Backup para:';	// NECESARIO PARA LOCALIZAR EL NOMBRE DE LA BBDD DEL ARCHIVO A RESTAURAR
			$charStart=strpos(substr($contents,0,200),$cadenaBuscar);
			$lenCadenaBuscar=strlen($cadenaBuscar);
			$cadenaStart=substr($contents,($charStart+$lenCadenaBuscar),100);
			$cadenaFin=strpos(substr($cadenaStart,0,100),"\n");
			$nombreTablaBackup=trim(substr($cadenaStart,0,$cadenaFin));

			if($nombreTablaBackup)
			{
				$inputsNombresTablas='';
				$resC=@copy($_FILES['archivo_restaurar']['tmp_name'], $this->temporalFolder.$this->temporalSql);

				if(!$resC)
				{
					$nombreCarpeta=substr($this->temporalFolder,3,(strrpos($this->temporalFolder,'/')-3));
					$this->mensajeSalida='<b>ERROR:</b> La carpeta de grabaci&oacute;n de archivos temporales <b>`<u>'.$nombreCarpeta.'</u>`</b> no existe o no tiene permisos de escritura.<br /><br /><br /><br /><h3 style=color:red;background-color:#fff;width:300px;padding:30px;>La operaci&oacute;n ha sido INTERRUMPIDA!</h3>';
					$_REQUEST['logEstado']='COPY ERROR: no access to '.$nombreCarpeta;
					return;
				}

				foreach($_REQUEST['tablaRestaurar'] as $key=>$value)
					$inputsNombresTablas.='<input type=\'hidden\' name=\'tablaRestaurar[]\' value=\''.$value.'\' />';

				$this->mensajeSalida='<form action='.$_REQUEST['script_name'].' method=post name=\'formNotificar\' enctype=multipart/form-data><input type=\'hidden\' name=\'accion\' value=6 /><input type=\'hidden\' name=\'nombre_bbdd\' value=\''.$_REQUEST['nombre_bbdd'].'\' />'.$inputsNombresTablas.'<div style=padding-left:25%;font-size:1.2em;text-align:left;><span style=display:block;width:300px;margin-left:80px;background-color:orange;color:#fff;padding:4px;text-align:center;><b>ATENCION!!</b>&nbsp;&nbsp;&nbsp;<i>lea atentamente</i></span><br /><br />Se ha detectado que en el archivo `<b>'.$_FILES['archivo_restaurar']['name'].'</b>`:<br />&#8226;&nbsp;la base de datos del archivo a restaurar se llama `<b><i>'.$nombreTablaBackup.'</i></b>`,<br />&#8226;&nbsp;mientras que el nombre de la base de datos actual es `<i><b>'.$_REQUEST['nombre_bbdd'].'</b></i>`.<br /><br />Desea continuar de todos modos?<br /></div><br /><br /><br /><a href=\'javascript:document.formNotificar.submit();restoreFromOtherBBDD();\' class=\'btn\' style=padding-left:20px;padding-right:20px; >Continuar con el proceso de restauraci&oacute;n</a></form>';

				$_REQUEST['logEstado']='BREAK restoring from: '.$nombreTablaBackup;
				return;
			}
			else
			{
				if($_REQUEST['accion']!=6)
				{
					$this->mensajeSalida='<b>ERROR:</b> LA BASE DE DATOS <b>`<u>'.$_REQUEST['nombre_bbdd'].'</u>`</b> no se encuentra en el archivo <i><b>`'.$_FILES['archivo_restaurar']['name'].'`</b></i> del cual usted intentaba hacer la restauraci&oacute;n.<br /><br /><br /><br /><h3 style=color:red;background-color:#fff;width:300px;padding:30px;>La operaci&oacute;n ha sido INTERRUMPIDA!</h3>';
					$_REQUEST['logEstado']='Restore FAILED: '.$_REQUEST['nombre_bbdd'].' NOT FOUND IN '.$_FILES['archivo_restaurar']['name'];
					return;
				}
			}

		}


		if(count($_REQUEST['tablaRestaurar']))
			echo '<span style=color:'.$_SESSION['colorTextWarning'].' >- Se han restaurado las siguientes tablas:</span><blockquote>';

		set_time_limit(30);
		$numTablasRestauradas=0;

//echo '----><pre>';print_r($sentenciasSql);return;;

		foreach($sentenciasSqlPulido as $key=>$instruccion)
		{
			if(!trim($instruccion)) continue;	// SE OMITEN FILAS VACIAS
			if(substr($instruccion,0,1)=='#') continue;	// SE OMITEN FILAS CON COMENTARIOS

			$restaurarTabla=NULL;

			foreach($_REQUEST['tablaRestaurar'] as $laTabla)
			{
				$lengthNombreTabla=strlen($laTabla);
				if(stristr(substr($instruccion,0,strpos(substr($instruccion,0,300),'(')),'`'.$laTabla.'`') || stristr(substr($instruccion,strpos(substr($instruccion,0,50),'drop table'),($lengthNombreTabla+24)),'`'.$laTabla.'`'))
					$restaurarTabla=$laTabla;
			}
			if($restaurarTabla)
			{
//echo $laTabla.'<br />'.$restaurarTabla.'<Hr />----><pre>';print_r($sentenciasSql);return;;

				$instruccion=str_replace('DROP TABLE IF EXISTS',"\r\nDROP TABLE IF EXISTS",$instruccion);

				$this->sql->query($instruccion);
				sleep(.9);

				if($tmpTabla!=$restaurarTabla)
				{
					echo'<span style=color:'.$_SESSION['colorTextWarning'].'><i>'.$restaurarTabla.'</i></span><br />';
					$numTablasRestauradas++;
				}
				$tmpTabla=$restaurarTabla;
			}
		}

		if($numTablasRestauradas)
			echo '<blockquote>';
		else
			echo '<i>NINGUNA</i><center><br /><br /><b>NOTA: </b>No se encontr&oacute; ninguna tabla a restaurar entre las seleccionadas y las presentes en el archivo "<b>'.$_FILES['archivo_restaurar']['name'].'</b>".</center><br /><br />';
		if(count($_REQUEST['tablaRestaurar']))
			echo '
			<center><a href=\'javascript:document.location="'.$_REQUEST['script_name'].'?accion=0";\' class=\'btn\' style=\'padding-left:20px;padding-right:20px;\'>&nbsp;&nbsp;Volver</a></center>';
//echo'<pre>';print_r($_REQUEST);exit;

		set_time_limit(30);
		return;
	}

//******************************************************
//	FORMULARIO
//******************************************************
	function formularioEjecutarRestore($destino,$numXXX=0,$fraseTitularFormulario)
	{
		$botonEjecutar='hacerRestore()';
		if($_SESSION['usuarioNivel']<15)
			$botonEjecutar='alert(\'Lamentamos informar que usted no tiene los permisos adecuados para efectuar esta operaci&oacute;n.\n\nP&oacute;ngase en contacto con el administrador para solucionar este inconveniente\');';
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
$(function(){$('input').customInput();});

jQuery.fn.customInput=function()
{
	$(this).each(function(i){
		if($(this).is('[type=checkbox]')){
			var input = $(this);
			// get the associated label using the input's id
			var label = $('label[for='+input.attr('id')+']');

			//get type, for classname suffix
			var inputType = 'checkbox';

			// wrap the input + label in a div
			$('<div class="custom-'+ inputType +'"></div>').insertBefore(input).append(input, label);

			// find all inputs in this set using the shared name attribute
			var allInputs = $('input[name='+input.attr('id')+']');

			// necessary for browsers that don't support the :hover pseudo class on labels
			label.hover(
				function(){
					$(this).addClass('hover');
					if(inputType == 'checkbox' && input.is(':checked')){
						$(this).addClass('checkedHover');
					}
				},
				function(){ $(this).removeClass('hover checkedHover'); }
			);

			//bind custom event, trigger it, bind click,focus,blur events
			input.bind('updateState', function(){
				if(input.is(':checked')){
					label.addClass('checked');
					$(this).blur();
				}
				else{label.removeClass('checked checkedHover checkedFocus');}

			})
			.trigger('updateState')
			.click(function(){
				$(this).trigger('updateState');
			})
			.focus(function(){
				label.addClass('focus');
				if(inputType == 'checkbox' && input.is(':checked')){
					$(this).addClass('checkedFocus');
					$(this).blur();
				}
			})
			.blur(function(){ label.removeClass('focus checkedFocus'); });
		}
	});
};

var estadoMarcados=1;

function marcarDesmarcarCheckBox(formulario,el)
{
	marcar=false;
	if(el) marcar="true";
	var n=0;

	for(i=0;i<document.forms[formulario].elements.length;i++)
	{
		if(document.forms[formulario][i].type!='checkbox') continue;

		var input = $(document.forms[formulario]["check-"+n]);
		var label = $('label[for='+input.attr('id')+']');

		if(marcar)
		{
			document.forms[formulario]["check-"+n].checked=true;
			label.addClass('checked');
		}
		else
		{
			document.forms[formulario]["check-"+n].checked=false;
			label.removeClass('checked checkedHover checkedFocus');
		}
		n++;
	}

	estadoMarcados=1;
	if(el) estadoMarcados=0;
	return false;
}

function hacerRestore()
{
	if(document.f<?=$idunico?>.archivo_restaurar.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.archivo_restaurar);
		document.f<?=$idunico?>.archivo_restaurar.focus();
		alert('Por favor, seleccione el archivo de datos a restaurar');
		restauraCampo(document.f<?=$idunico?>.archivo_restaurar);
		return;
	}
	if(document.f<?=$idunico?>.nombre_bbdd.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.nombre_bbdd);
		document.f<?=$idunico?>.nombre_bbdd.focus();
		alert('Por favor, escriba el nombre de la BBDD del servidor');
		restauraCampo(document.f<?=$idunico?>.nombre_bbdd);
		return;
	}

	document.getElementById("tblNota").style.display='none';
	document.getElementById("tblFormulario").style.display='none';
	document.getElementById("divMesaje").style.display='inline';
//	document.getElementById("spnBase").innerHTML="para '"+document.f<?=$idunico?>.nombre_bbdd.value+"'";
	document.f<?=$idunico?>.momento.value="<?=date('Hi')?>";
	document.f<?=$idunico?>.accion.value=1;
	document.f<?=$idunico?>.submit();
}

noListar=1;
-->
</script>

<?php
// SE GENERA UN ARRAY CON LAS TABLAS PRESENTES EN EL SITIO
		$query='SHOW FULL TABLES FROM '.$this->sql->db.'';
		$res=$this->sql->query($query);

		while($arra=$this->sql->fila($res))
		{
			$this->nombreTablasBBDD[]=$arra[0];
		}

		$numColumnas=6;
		$salidatablas='<br /><table border="0" cellspacing="0" cellpadding="6" align="center" style="" class="borde">';
		$salidatablas.='<thead><tr><td colspan="'.$numColumnas.'" align="left">Tablas presentes en la base de datos:&nbsp;<span style="color:'.$_SESSION['colorTextWarning'].'">MARCAR LAS TABLAS QUE SE DEBAN RESTAURAR</span></td></tr></thead>';
		$salidatablas.='<tr>';

		foreach($this->nombreTablasBBDD as $key=>$nombreTabla)
		{
			$marcado='checked="true"';
			if(in_array($nombreTabla,$this->tablasOmitidas)) $marcado='';

			$salidatablas.='<td>
				<input id="check-'.$key.'" type="checkbox" name="tablaRestaurar['.$key.']" '.$marcado.' value="'.$nombreTabla.'" /><label for="check-'.$key.'" title="Marcar/desmarcar">&nbsp;&nbsp;&nbsp;'.strtolower(str_replace('_',' ',$nombreTabla)).'&nbsp;</label></td>';

			if(($key%ceil($numColumnas)==($numColumnas-1)))
				$salidatablas.='</tr><tr>';
		}

		$salidatablas.='</tr>';
		$salidatablas.='<tr><td colspan="'.$numColumnas.'" align="right"><a href="#null" class="btn" onclick="marcarDesmarcarCheckBox(\'f'.$idunico.'\',estadoMarcados)" style="padding-left:20px;padding-right:20px;" >Marcar / Desmarcar todas las tablas</a></td></tr>';
		$salidatablas.='<table>';

//echo'<pre>';print_r($this->nombreTablasBBDD);
?>
<style>
<!--
/*	wrapper divs */
.custom-checkbox{position:relative;color:<?=($_SESSION['colorTextoStd']?$_SESSION['colorTextoStd']:'#888;')?>;}

/* input, label positioning */
.custom-checkbox input{position:absolute;left:2px;top:3px;margin:0;z-index:0;}

.custom-checkbox label{
display:block;
position:relative;
z-index:1;
font-size:1.1em;
padding-right:1em;
line-height:1;
padding:.5em 0 .5em 20px;
margin:0 0 .3em;
cursor:pointer;
}

/* states */
.custom-checkbox label{background:url(_img/checkbox.gif) no-repeat;}
.custom-checkbox label{background-position:-10px -14px;}
.custom-checkbox label.hover,.custom-checkbox label.focus{background-position:-10px -114px;}
.custom-checkbox label.checked{background-position:-10px -214px;color:<?=$_SESSION['colorOverImportant']?>;}
.custom-checkbox label.checkedHover,.custom-checkbox label.checkedFocus{background-position:-10px -314px;}
.custom-checkbox label.focus{outline:1px dotted #ccc;}
-->
</style>
<form action='<?=$destino?>' method='post' name='f<?=$idunico?>' enctype='multipart/form-data' onsubmit='alert("Por favor, para validar el formulario, pulse el bot&oacute;n [Guardar datos]");return false'>
<input type='hidden' name='accion' value='<?=$accion?>'>
<input type='hidden' name='momento' value='<?=$_REQUEST['momento']?>'>
<input type='hidden' name='script_name' value='<?=$destino?>'>

<div id='divMesaje' style='display:none;text-align:center;width:100%'><br /><br /><br /><i><h3 style="color:#FF3B00">INICIANDO LA EJECUCI&Oacute;N DEL PROCESO...</h3></i><br /><br /><br /><br /><center>Por favor, no interrumpa este proceso.</center><span id="spnBase" style="font-size:20px;font-weight:bold"></span></div>
<table align='center' cellpadding='3' cellspacing='0' class='borde' border='0' width='580' id='tblFormulario'>
<thead>
<tr>
	<td colspan='12' class='enc2' id='tdTitularFormulario'><?=$fraseTitularFormulario?></td>
</tr>
</thead>
<tr>
	<td align="right">Archivo TXT a restaurar:</td>
	<td valign="top" colspan="3"><input type="file" id="archivo_restaurar" name="archivo_restaurar" style="width:134px" />&nbsp;&nbsp;
	</td>
</tr>
<tr>
		<td align="right">Nombre de la BBDD:</td>
		<td>
<?php
echo '<input type="text" id="nombre_bbdd" name="nombre_bbdd" style="" value="';
if(!$_REQUEST['nombre_bbdd']) echo $this->sql->db;
echo '" />';
?>
		</td>
</tr>
<!--
<tr>
	<td colspan='' align='right' height="20" valign="middle">Comprimir el archivo generado</td>
	<td colspan='' align='left' height="20" valign="middle"><input type="checkbox" name="comprimido" id="comprimido" value="1" /></td>
</tr>
-->
<tr>
	<td colspan='12' align='center' height="50" valign="middle">Pulse el bot&oacute;n aqu&iacute; abajo para realizar la restauraci&oacute;n</td>
</tr>
<tr height="22">
	<td colspan='12' align='center' valign="bottom"><a href="#null" class='btn' onclick="<?=$botonEjecutar?>" style="width:200px;" >&nbsp;&nbsp;Ejecutar la restauraci&oacute;n&nbsp;&nbsp;</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#null" class='btn xcancel' onclick="document.location='<?=$destino?>?reset=1'" style="color:<?=$_SESSION['colorTextWarning']?>;background-color:<?=$_SESSION['colorFondoWarning']?>">Reset</a></td>
</tr>
<tr>
	<td colspan='12' align='center' height="20" valign="bottom">&nbsp;</td>
</tr>
<?=$trRecargar?>
</table>
<table align="center" id="tblNota">
<tr>
	<td colspan='12' align='center' height="50" valign="middle" style="font-size:1.2em"><b><i>MUY IMPORTANTE</i>:</b> TODA LA ACTUAL INFORMACION ALMACENADA<br />EN LAS TABLAS SELECCIONADAS DE LA BASE DE DATOS <b>SE PERDER&Aacute;</b>.<br /><br /><u>Se recomienda <b>efectuar PRIMERO una copia de seguridad</b> de la BBDD</u>.</td>
</tr>
<tr>
<td>
<?php
		echo $salidatablas;
?>
</td>
</tr>
</table>
</form>
<?php
/*
if(count($this->tablasOmitidas))
{
	$salida='<br /><blockquote>Listado de tablas que NO ser&aacute;n restauradas a la base de datos <b>'.$this->sql->db.'</b>:<blockquote>';

	foreach($this->tablasOmitidas as $key=>$laTabla)
	{
		$salida.=$key.'&nbsp;-&nbsp;<i>'.strtoupper($laTabla).'</i><br />';
	}
	echo '</blockquote></blockquote>'.$salida;
}
*/
//echo '<pre>';print_r($_REQUEST);print_r($this);print_r($_SESSION);exit;
		return $idunico;
	}
}
?>