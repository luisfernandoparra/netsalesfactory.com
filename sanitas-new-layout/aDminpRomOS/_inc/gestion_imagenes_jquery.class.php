<?php
/*
------------- NOTA IMPORTANTE ----------------
Para configurar las carpetas de las imagenes que han de ser accesibles desde admin, se debe editar el archivo de configuración del plug-in "iBrowser" en:
editor/plugins/ibrowser/config/config.inc.php

Los siguientes array, con las rutasw para pruebas y el otro para el site real:
$cfg['ilibs']
$cfg['ilibs']

*/
class ImagenesJquery extends Base
{
	//Datos propios de la clase
	var $id;
	var $idioma;
	var $rutaDesdeAdmin;
	var $tamanioMaximoImagenes;
	var $path_imagenes;
	var $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BuSQUEDAS

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
//echo $_SERVER['SERVER_ADDR'];
		$this->sql=$sql;
		$this->rutaDesdeAdmin='../';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		$this->arrayCarpetasImagenes=array(
			$this->rutaDesdeAdmin.'images/',
			$this->rutaDesdeAdmin.'img_diapos/'
		);
		//$this->arrayCarpetasImagenes[]=;

		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset']==1) unset($_SESSION['filtros']);
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		return;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
echo $query,'111<pre>0====>';print_r($_REQUEST);print_r($objeto);return;
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->imagen=$a['imagen'];
		return;
	}



//******************************************************
//	GUARDAR DATOS DE INSERCIONES Y EDICIONES
//******************************************************
	function guarda_datos()
	{
		$modif_imagen=0; $isError=null;

echo '0<br />'.$this->tamanioMaximoImagenes.'<br /><pre>';print_r($_REQUEST);print_r($_SESSION);exit;

		if(count($_FILES['imagen']) && !$_REQUEST['quitar_archivo'])	// SOLO SI ES UNA IMAGEN
		{
			if(!is_dir($_REQUEST['carpeta']))
			{
				if(!@mkdir($_REQUEST['carpeta']))	// SI SE PRODUCIRSE UN ERROR DE CREACIÓN DE CARPETA, SE VISUALIZA UN MENSAJE Y SE ABORTA LA OPERACIÓN
				{
					$this->mensajeSalida='<span class=enc2 style=background-color:red;color:#fff>&nbsp;ERROR GRAVE&nbsp;</span><br />NO EXISTE Ó NO SE PUEDE ESCRIBIR EN LA CARPETA DE <br />'.$_REQUEST['carpeta'];
					$isError=1;
				}
			}
//echo '0<br />'.$this->tamanioMaximoImagenes.'<br /><pre>';print_r($_REQUEST);print_r($_FILES);exit;

			// SOLO SOLO SE PUEDEN SUBIR AL SERVIDOR IMAGENES VÁLIDAS
			if($_FILES['imagen']['name'])
			{
				if($_FILES['imagen']['type']!='image/jpeg' && $_FILES['imagen']['type']!='image/x-png' && $_FILES['imagen']['type']!='image/pjpeg' && $_FILES['imagen']['type']!='image/gif' && $_FILES['imagen']['type']!='image/png' && $_FILES['imagen']['type']!='image/x-icon')
				{
					$this->mensajeSalida='ARCHIVO NO SOPORTADO, SOLO IMAGENES';
					$isError=1;
				}

				$tamanio=$_FILES['imagen']['size'];

				if($tamanio > $this->tamanioMaximoImagenes)	// SE HA SUPERADO EN BYTES EL TAMAÑO MÁXIMO PERMITIDO PARA EL OBJETO DE FLASH
				{
					$tamanio=number_format(($tamanio/1000),2,',','.').'Kb';
					$this->mensajeSalida='ERROR: es demasiado grande: '.$tamanio.' (MAX: '.($this->tamanioMaximoImagenes/1000).'Kb)';

					if(!$_REQUEST['id'])	// ES UN RECORD NUEVO
					{
						$this->mensajeSalida='<br /><br /><center><p ><h4 style="color:red">'.$this->mensajeSalida.'</h4></p><br /><br /></center>';
						$isError=1;
					}
//					return;
				}

				if($isError)
				{
						echo '<br /><br /><center><p ><h4 style="color:red">'.$this->mensajeSalida.'</h4></p><br /><br /><br /><a href="#null" style="width:100px" class="btn" onclick="document.location=\''.$this->nombreFilePhpBase.$enEdicion.'.php?id='.$this->id.'\';">volver a editar</a></center>';				exit;
				}

				$imageSaveServer=$_FILES['imagen']['name'];
				$imageSaveServer=$this->limpianombre($imageSaveServer);
//die();
				if(file_exists($_REQUEST['carpeta'].$imageSaveServer))	// YA EXISTE UN ARCHIVO EN EL MISMO DIRECTORIO CON EL MISMO NOMBRE
				{
					$this->mensajeSalida='<br /><br /><center><p ><h4 style="color:red">ERROR</h4><br /><br />no se puede subir el archivo:<br />"<b>'.$imageSaveServer.'</b>"<br /><br />&Eacute;ste ya existe en la carpeta actualmente seleccionada.<br /><br /><br /><br /><span style="color:#fff">Para subirlo al servidor, primero debe eliminar el actual.</span></p><br /><br /><br /><a href="#null" style="width:100px" class="btn" onclick="history.go(-1)">volver</a></center>';
					$isError=1;
//					exit;;
				}

				if(is_uploaded_file($_FILES['imagen']['tmp_name']))
				{
					copy($_FILES['imagen']['tmp_name'], $_REQUEST['carpeta'].$imageSaveServer);
					return;
				}
			}
		}

		if($_REQUEST['quitar_archivo'])	// SI SE HA SELECCIONADO QUITAR EL ARCHIVO, SE PROCEDE A BORRARLO DE LA TABLA
		{
			if(file_exists($_REQUEST['carpeta'].$_REQUEST['nombreImagenEliminar']))
				@unlink($_REQUEST['carpeta'].$_REQUEST['nombreImagenEliminar']);
		}
		return;
	}

//******************************************************
//	FORMULARIO DE EDICIÓN /INSERCIÓN DE DATOS
//******************************************************
	function formulario_datos()
	{
		if(isset($_REQUEST['laImagen']))
		{
			$rutaImagen=substr($_REQUEST['laImagen'],0,strrpos($_REQUEST['laImagen'],'/')+1);
			$imagenNombre=substr($_REQUEST['laImagen'],strrpos($_REQUEST['laImagen'],'/')+1);
		}
?>
<tr>
	<td colspan="10" align="center" id="tdTitularFormulario">&nbsp;</td>
</tr>
<tr>
	<td valign="top" align="right">Carpeta:</td>
<?php
		if($imagenNombre)
		{
			echo '<td valign="top" title="Carpeta donde se encuentra la imagen"><input id="carpeta" name="carpeta" value="'.$rutaImagen.'" style="width:300px" disabled="disabled">';
		}
		else
		{
			$outDisplay='<td valign="top"><select id=\'carpeta\' name=\'carpeta\' style=\'width:220px\' >';
			$outDisplay.='<option value="-1" '.(!isset($_REQUEST['carpeta'])? '':' selected="selected"	').'>Seleccionar</option>';

			foreach($this->arrayCarpetasImagenes as $key=>$value)
			{
				$outDisplay.='<option value=\''.$value.'\' ';
				if(($this->id-1) === $key)
				{
					$outDisplay.='selected=\'selected\'';
					$this->actualFolderEdit=$value;
				}
				$outDisplay.='>'.$value.'</option>';
			}
			$outDisplay.='</select>
';
		}
		echo $outDisplay;
?>
	</td>
<!--
	<td align='right' valign="top" title="">
<?php
		if($imagenNombre)	// SE DIBUJA EL BOTÓN PARA ABRIR LA IMAGEN A TANAÑO REAL EN UNA VENTANA POP
		{
			$dat=@getimagesize($rutaImagen.$imagenNombre);
			echo '<input type="hidden" name="nombreImagenEliminar" value="'.$imagenNombre.'" /><a href="#null" class="btn" style="border:2px solid green" onclick="im(\''.$rutaImagen.$imagenNombre.'\','.$dat[0].','.$dat[1].')" title="click para ver la imagen">&nbsp;Ver la imagen </a>';
		}
		else
			echo 'imágen:';
?>
  </td>
	<td valign="top" title="10px (ancho) x 12px (alto)"><input type="file" id="imagen" name="imagen" style="width:134px" title="10px (ancho) x 12px (alto)" />&nbsp;&nbsp;
<?php
		if($imagenNombre)
		{
			$archivoLaBanderita=$rutaImagen.$imagenNombre;

			if(file_exists($archivoLaBanderita))	// SE VISUALIZA LA CASILLA PARA BORRAR LA IMAGEN SOLO EN EDICIÓN DE REGISTRO
				echo '<br /><span title="Marcar para eliminar la imágen">Eliminar imagen: <input type="checkbox" name="quitar_archivo" value="1" style="border:0px;height:14px;width:14px"></span>';
		}
?>
	</td>
</tr>
-->
<?php
//echo '<pre>';print_r($_SESSION['filtros']);print_r($_REQUEST);
//		if($imagenNombre) echo '<tr><td height="25"></td></tr>';
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
var textoCabecera="&nbsp;"
function actualizarDespuesCambioImagen()
{
	document.location="<?=$destino.'?posicionSolapa='.$this->posicionSolapa?>";
}

function revisa()
{
<?php
		$salidaControl=(isset($_REQUEST['laImagen']))? 'modificacion="'.$_REQUEST['laImagen'].'";':'modificacion=0;';
		echo $salidaControl;
?>
//alert($("#carpeta")[0])
	if($('#carpeta').val() < 0)
	{
		resaltarCampo($("#carpeta")[0]);
		$("#carpeta").focus();
		alert('Por favor, indique la carpeta');
		restauraCampo($("#carpeta")[0]);
		return;
	}
/*
	if(!$('#imagen').val() && !modificacion)
	{
		resaltarCampo($("#imagen")[0]);
		document.f<?=$idunico?>.imagen.focus();
		alert('Por favor, indique la ruta de la imágen en su ordenador');
		restauraCampo($("#imagen")[0]);
		return;
	}
*/
	document.getElementById("carpeta").disabled=false;
	document.f<?=$idunico?>.submit();
}

-->
</script>
<?php
$pathAdjust='jquery_upload/';

if($this->id)
{

}
?>
<form action='<?=$destino?>' method='post' name='f<?=$idunico?>' enctype='multipart/form-data' onsubmit='alert("Por favor, para validar el formulario, pulse el botón [Guardar datos]");return false'>
<input type='hidden' name='posicionSolapa' value='<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>'>
<input type='hidden' name='accion' value='<?=$accion?>'>
<input type='hidden' name='id' value='<?=$this->id?>'>
<input type='hidden' name='destino' value='<?=$destino?>'>
<table width="<?=$this->id?'100%':'70%'?>" align='center' cellpadding='3' cellspacing='0' class="<?=$this->id?'tblEditModuleFixed':'borde'?>"  border="<?=$_SESSION['debugBackOffice'] ? 1 : 0?>">
<thead>
<tr>
<?php
	if($this->id)
		echo '
	<td class="enc" width="97%">'.$titulo.'</td>
	<td class="enc"><a onclick="parent.edicionClose();" class="btn xcancel tooltip" style="height:18px;line-height:10px;padding:4px;" href="#null" >X</a></td>
';
	else
		echo '
	<td class="enc" width="100%">'.$titulo.'</td>
';
?>
</tr>
</thead>
</table>
<table width="<?=$this->id?'100%':'70%'?>" align='center' cellpadding='3' cellspacing='0' class="<?=$this->id ? 'tblEditModule' : 'bordeRect'?>"  border="<?=$_SESSION['debugBackOffice'] ? 1 : 0?>">
<?php
		if($this->id)
			echo '<script>document.body.style.border="0px";</script><tr><td height="5"></td></tr>';
		$this->formulario_datos();
?>

	<td colspan='12' align='center' height="10" valign="bottom">
<a href="#null" class='btn' onclick="revisa()" style="width:200px;height:18px;line-height:10px;padding:4px;" >Guardar datos</a>
<?php
if($_REQUEST['id'])
	echo '&nbsp;&nbsp;&nbsp;<a href="#null" class="btn xcancel" onclick="parent.edicionClose()" style="width:110px;height:18px;line-height:10px;padding:4px;" >Cerrar sin guardar</a>';
?>
</td>
</tr>
<tr>
	<td colspan='10' class='txt' align='center' style='font-size:.85em' height='2'></td>
</tr>
</table>
</form>

<?php
if($this->id)
{
//echo $this->actualFolderEdit;
?>

<script>
var actualFolderEdit="<?=$this->actualFolderEdit?>";
//var actualFolderEdit="jquery_upload/server/php/";
</script>
<div style="display:block;width:98%;">
	<link rel="stylesheet" href="<?=$pathAdjust?>css/bootstrap_site.css">
	<link rel="stylesheet" href="<?=$pathAdjust?>css/style.css">
	<link rel="stylesheet" href="<?=$pathAdjust?>css/blueimp-gallery.min.css">
	<link rel="stylesheet" href="<?=$pathAdjust?>css/jquery.fileupload.css">
	<link rel="stylesheet" href="<?=$pathAdjust?>css/jquery.fileupload-ui.css">

	<script src="<?=$pathAdjust?>js/vendor/jquery.ui.widget.js"></script>
	<script src="<?=$pathAdjust?>js/tmpl.min.js"></script>
	<script src="<?=$pathAdjust?>js/load-image.min.js"></script>
	<script src="<?=$pathAdjust?>js/canvas-to-blob.min.js"></script>
	<script src="<?=$pathAdjust?>js/bootstrap.min.js"></script>
	<script src="<?=$pathAdjust?>js/jquery.blueimp-gallery.min.js"></script>
	<script src="<?=$pathAdjust?>js/jquery.iframe-transport.js"></script>
	<script src="<?=$pathAdjust?>js/jquery.fileupload.js"></script>
	<script src="<?=$pathAdjust?>js/jquery.fileupload-process.js"></script>
	<script src="<?=$pathAdjust?>js/jquery.fileupload-image.js"></script>
	<script src="<?=$pathAdjust?>js/jquery.fileupload-audio.js"></script>
	<script src="<?=$pathAdjust?>js/jquery.fileupload-video.js"></script>
	<script src="<?=$pathAdjust?>js/jquery.fileupload-validate.js"></script>
	<script src="<?=$pathAdjust?>js/jquery.fileupload-ui.js"></script>
	<script src="<?=$pathAdjust?>js/main.js"></script>
	<!-- jquery_upload/server/php/ -->
	<form id="fileupload" action="jquery_upload/server/php/" method="POST" enctype="multipart/form-data">
		<!-- Redirect browsers with JavaScript disabled to the origin page -->
		<noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/"></noscript>
		<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
		<div class="row fileupload-buttonbar">
			<div class="col-lg-7">
				<!-- The fileinput-button span is used to style the file input field as button -->
				<span class="btn btn-success fileinput-button">
					<i class="glyphicon glyphicon-plus"></i>
					<span>Añadir archivos...</span>
					<input type="file" name="files[]" multiple>
				</span>
				<button type="submit" class="btn btn-primary start">
					<i class="glyphicon glyphicon-upload"></i>
					<span>Subir al servidor</span>
				</button>
				<button type="reset" class="btn btn-warning cancel">
					<i class="glyphicon glyphicon-ban-circle"></i>
					<span>Cancelar</span>
				</button>
				<button type="button" class="btn btn-danger delete">
					<i class="glyphicon glyphicon-trash"></i>
					<span>Eliminar</span>
				</button>
				<input type="checkbox" class="toggle">
				<!-- The global file processing state -->
				<span class="fileupload-process"></span>
			</div>
			<!-- The global progress state -->
			<div class="col-lg-5 fileupload-progress fade">
				<!-- The global progress bar -->
				<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
					<div class="progress-bar progress-bar-success" style="width:0%;"></div>
				</div>
				<!-- The extended global progress state -->
				<div class="progress-extended">&nbsp;</div>
			</div>
		</div>
		<!-- The table listing the files available for upload/download -->
	<div style="display:block;float:left;width:102%;max-height:280px;overflow:auto;">
		<table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
	</div>
	</form>

	<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
		<div class="slides"></div>
		<h3 class="title"></h3>
		<a class="prev">‹</a>
		<a class="next">›</a>
		<a class="close">×</a>
		<a class="play-pause"></a>
		<ol class="indicator"></ol>
	</div>

	<!-- The template to display files available for upload -->
	<script id="template-upload" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
		<tr class="template-upload fade">
			<td>
				<span class="preview"></span>
			</td>
			<td>
				<p class="name">{%=file.name%}</p>
				<strong class="error text-danger"></strong>
			</td>
			<td>
				<p class="size">Processing...</p>
				<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
			</td>
			<td>
				{% if (!i && !o.options.autoUpload) { %}
					<button class="btn btn-primary start" disabled>
						<i class="glyphicon glyphicon-upload"></i>
						<span>Enviar</span>
					</button>
				{% } %}
				{% if (!i) { %}
					<button class="btn btn-warning cancel">
						<i class="glyphicon glyphicon-ban-circle"></i>
						<span>Cancel</span>
					</button>
				{% } %}
			</td>
		</tr>
	{% } %}
	</script>
	<!-- The template to display files available for download -->
	<script id="template-download" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
		<tr class="template-download fade">
			<td>
				<span class="preview">
					{% if (file.thumbnailUrl) { %}
						<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
					{% } %}
				</span>
			</td>
			<td>
				<p class="name">
					{% if (file.url) { %}
						<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
					{% } else { %}
						<span>{%=file.name%}</span>
					{% } %}
				</p>
				{% if (file.error) { %}
					<div><span class="label label-danger">Error</span> {%=file.error%}</div>
				{% } %}
			</td>
			<td>
				<span class="size">{%=o.formatFileSize(file.size)%}</span>
			</td>
			<td>
				{% if (file.deleteUrl) { %}
					<button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
						<i class="glyphicon glyphicon-trash"></i>
						<span>Eliminar</span>
					</button>
					<input type="checkbox" name="delete" value="1" class="toggle">
				{% } else { %}
					<button class="btn btn-warning cancel">
						<i class="glyphicon glyphicon-ban-circle"></i>
						<span>Cancelar</span>
					</button>
				{% } %}
			</td>
		</tr>
	{% } %}
	</script>
</div>
<?php
}
?>


<br />

<?php
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->quitar_archivo=$datos['quitar_archivo'];
		$this->imagen=$datos['imagen'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca,$destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$cont=0;$num_res=0;
		$_SESSION['filtros']['offset_u']*=1;

		foreach($this->arrayCarpetasImagenes as $key=>$value)
		{
			$archivos[$key]=$this->recuperar_files($value);
			count($this->recuperar_files($value));

			if($archivos[0])
			{
				foreach($archivos[$key] as $nf=>$nombreFile)
				{
					if(isset($_SESSION['filtros'][$this->posicionSolapa.'imagen_nombre_busca_u']))
					{
						if(!strstr($nombreFile,$_SESSION['filtros'][$this->posicionSolapa.'imagen_nombre_busca_u'])) continue;
					}

					if(isset($_SESSION['filtros'][$this->posicionSolapa.'ref_carpeta_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'ref_carpeta_buscN'] != $key) continue;
					$tmpCarpetaImagen[$num_res]=$value;
					$tmpIdCarpeta[$num_res]=$key;
					$tmpArchivoListar[$num_res]=$nombreFile;
					$num_res++;
				}
			}
		}

		$elOffset=$_REQUEST['offset_u'];

		if(!$elOffset)
		{
			$elOffset=0;
			$limiteVisualizar=$this->filasporpagina;
		}
		else
		{
			$limiteVisualizar=($elOffset)+$this->filasporpagina;
		}

		for($n=$elOffset;$n < $limiteVisualizar;$n++)
		{
			if(!$tmpCarpetaImagen[$n]) continue;
			$arraCarpetaImagen[$n]=$tmpCarpetaImagen[$n];
			$arraIdCarpeta[$n]=$tmpIdCarpeta[$n];
			$archivoListar[$n]=$tmpArchivoListar[$n];
			$scriptJs.='arrafiles['.$n.']="'.$tmpCarpetaImagen[$n].$tmpArchivoListar[$n].'";';
//echo '<br />'.$n.'==>'.$elOffset.' / '.$limiteVisualizar;
		}

		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';

//******************************************************
//Formulario para efectuar busquedas
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- VARIABLE NECESARIA PARA GESTIONAR EL ARCHIVO SELECCIONADO -->
<input type='hidden' name='laImagen' value='<?=$this->laImagen?>'>

<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->

<table align='center' cellpadding='3' cellspacing='0' class='borde' width='810' border="<?=$_SESSION['debugBackOffice'] ? 1 : 0?>">
<thead>
<tr>
	<td class='enc2' colspan='3'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td width="30">Carpeta:</td>
<?php
			echo '
	<td>';
			echo '<select id=\''.$this->posicionSolapa.'ref_carpeta_buscN\' name=\''.$this->posicionSolapa.'ref_carpeta_buscN\' style=\'width:200px\' >';
			echo '<option value="-1" '.(!isset($_SESSION['filtros'][$this->posicionSolapa.'ref_carpeta_buscN'])? '':' selected="selected"	').'>Todas</option>';

			foreach($this->arrayCarpetasImagenes as $key=>$value)
			{
				echo '<option value=\''.$key.'\' ';
				if($_SESSION['filtros'][$this->posicionSolapa.'ref_carpeta_buscN']==$key && isset($_SESSION['filtros'][$this->posicionSolapa.'ref_carpeta_buscN']))
					echo 'selected=\'selected\'';
				echo '>'.$value.'</option>';
			}
			echo '</select>
';
?>
	</td>
	<td width='30'>Nombre:</td>
	<td>
<input type='text' name='<?=$this->posicionSolapa?>imagen_nombre_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'imagen_nombre_busca_u']?>' style='width:150px'>
	</td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>

<script language="JavaScript" type="text/javascript">
<!--
var arrafiles=new Array();
<?php
	echo $scriptJs;
?>

var xXp=-100; var fMH=0; var qMH=0;
var opacidad=90;
var tmpConsulta;

function sustituirImagen(id,nombre,carpeta)
{
	var fileSustituir='';
	fileSustituir="<?=$destinobusca?>".substr(0,"<?=$destinobusca?>".length);

	if(confirm('¿Desea realmente sustituir la imagen "'+nombre+'"?'))
	{
		var a=screen.width;
		var b=screen.height;
		var ancho=500;
		var alto=340;
		var x=(a-ancho)/2;
		var y=(b-alto)/2;
		var t=window.open (fileSustituir+'_sustituir.php?accion=4&posicionSolapa=<?=$this->posicionSolapa?>&nombreImagen='+nombre+'&fullName='+arrafiles[id]+'&carpetaImagen='+carpeta,'sustituir','width='+ancho+',height='+alto+',left='+x+',top='+y+'');
		t.focus();
		return;
	}
}

-->
</script>
<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'470px','','70%','14%');

		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('Carpeta','Nombre imagen','Imagen');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id');
		$xc=0; $arraLongitudes=array($xc++=>30,$xc++=>30,$xc++=>130); // no se incluye el valor para el ID
		$xc=0; $arraLongitudesTitulares=array($xc++=>45,$xc++=>150,$xc++=>360,$xc++=>174);
		$xc=0; $arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'num',$xc++=>'img');
		$verEmergentesaccionsFragmentados=1;
		$decimales=0;

		if(count($archivoListar))
		{
			foreach($archivoListar as $key=>$value)
			{
				$xc=0;
				$arraDatos[]=array($xc++=>$arraIdCarpeta[$key]+1,$xc++=>$arraCarpetaImagen[$key],$xc++=>$value,$xc++=>$arraCarpetaImagen[$key].$value);
			}
		}

		$idRefPops=0; $txtRefEliminar=3; $ocultarEliminar=0;
		$posicFondo=0;	// NUMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados,$this->decimalesEnListado,$posicFondo,$ocultarEliminar,0,1,$this->NoeditarFila);
		unset($laAccion);
		/*******************  FIN DEL LISTADO  ********************/

		if($xc && !$this->contFilas)
		{
			foreach($_SESSION['filtros'] as $key=>$value)	// SE RECARGA LA PAGINA SI NO HAY FILAS EN PANTALLA Y HAY RESULTADOS EN LA BUSQUEDA
			{
				$pos=strpos($key,'_busca');
				if ($pos>0)
					$url.='&'.$key.'='.urlencode($value);
			}
			$_SESSION['filtros']['offset_u']=0;
		}
?>
</table>
<table align='center' cellpadding='2'>
<tr><td style='line-height:22px' align='center'><span id='spn_infoPage'></span>
<?php
	$url='';
	foreach($_SESSION['filtros'] as $key=>$value)
	{
		$pos=strpos($key,'_busca');
		if ($pos>0)
			$url.='&'.$key.'='.urlencode($value);
	}
	if(isset($_REQUEST['offset_u']))
		$offset_u = $_REQUEST['offset_u'];
	else
		$offset_u = 0;

// ********************************************************************************************************
// nota importante: EL ULTIMO PARÁMETRO ES LA REFERENCIA DEL IFRAME RESPECTO A LOS DEMÁS DE LA PÁGINA PADRE, es BÁSICO
// ********************************************************************************************************
	$hay=$this->paginacion($num_res, $this->filasporpagina, $offset_u, $destinobusca.".php?filtrar=".$_REQUEST['filtrar'].$url.'&sentidoOrdenacion='.$_REQUEST['sentidoOrdenacion'].'&ordenarPor='.$_REQUEST['ordenarPor']."&offset_u=",$this->TotPagMostrar,$this->posicionSolapa);
// ********************************************************************************************************
	if(trim($hay))
		echo '<script>
$("#paginarTop").html(\''.$hay.'\');
$("#paginarTop").css("display","block");
</script>';
echo $hay;
?>
</td></tr></table>
</form>
<script>
function mostrarPagina(direccion,posicionSolapa)	// se recarga la página desde los links de la barra inferior de paginación (2011)
{
	$("#mesajes_formulario").html("<i>accediendo...</i>");
	document.location=direccion+"&posicionSolapa="+posicionSolapa;
	return;
}
</script>
<?php
//echo count($_SESSION['filtros']);echo '<pre>';print_r($_SESSION['filtros']);//print_r($_REQUEST);
	}
}
?>