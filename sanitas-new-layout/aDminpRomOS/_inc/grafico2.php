<?php
include('phplot.php');  // here we include the PHPlot code 
$graph =& new PHPlot();   // here we define the variable graph
//Define some data
/*
$graph->SetLineWidth(0);
*/
//$graph->SetErrorBarShape('tee');
//$graph->SetXLabelAngle(10);
$graph->SetPlotType('pie');
//$graph->SetLineWidth(1);
//$graph->SetGridColor ('#5A9AA3');

//$graph->SetBackgroundColor("'".$_REQUEST['backColor']."'");
$graph->SetBackgroundColor('#000000');
$graph->SetNumHorizTicks(2);
$graph->SetNumVertTicks(50);
$graph->SetFileFormat('gif');
$graph->safe_margin=90;
$graph->y_axis_position=500;
//$graph->SetMarginsPixels(0,-50,-50,-10);
//$graph->SetXLabel(0);
//$graph->SetHorizTickIncrement(3);
//$graph->SetLineStyles('solid');
//$graph->SetXGridLabelType('time');

$example_data = array
(
	array('datos2',$_REQUEST['valor2'],$_REQUEST['valor4'],$_REQUEST['valor3'])
);
$graph->SetDataValues($example_data);

//Draw it
$graph->DrawGraph(); // remember, since in this example we have one graph, PHPlot does the PrintImage part for you

?>
