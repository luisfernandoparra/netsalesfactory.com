<?php

class Conexion
{
	public $servidor;
	public $db;
	public $user;
	public $pass;
	public $conexion;
	public $xml=0;
	public $eco=0; //0 para no debug, 1 para debug de globales, 2 para debug de globales y sentencias SQL
	public $prefixTbl;

	function Conexion($servidor,$user,$pass,$db='',$port=5432)
	{
		$this->servidor=$servidor;
		$this->db=$db;
		$this->user=$user;
		$this->pass=$pass;
		$this->prefixTbl='nl_';
		$this->port=$port;
		//Activamos el servidor de echo
		if($this->eco)
		{
			$datos="Fichero: ".$_SERVER['PHP_SELF']."\r\n";
			$datos.=str_replace("\n","\r\n",print_r($_REQUEST,true));
			$this->debug($datos,0,1);
			$datos="Fichero: ".$_SERVER['PHP_SELF']."\r\n";
			$datos.=str_replace("\n","\r\n",print_r($_SESSION,true));
			$this->debug($datos,0,2);
		}
	}

	function debug($datos,$convierte=0,$tipo=1)
	{
//echo "<p style='color:red'>=======>".$ip_destino.'</p>';
		$puerto=8887+$tipo;
		if($convierte)
			$datos=str_replace("\n","\r\n",$datos);
		$datos.="\r\n=====================================================================\r\n";
		$socket=@socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
		$result=@socket_connect($socket,'127.0.0.1',$puerto);
		@socket_write($socket,$datos,strlen($datos));
		@socket_close($socket);
	}

	function conecta()
	{
		$connectHost=$this->port ? $this->servidor.':'.$this->port : $this->servidor;
//echo $connectHost;die();
		$this->conexion=@mysql_connect($connectHost,$this->user,$this->pass);
		if(!$this->conexion)
			return 0;
		else
		{
			if(@$_SESSION['nuevaBBDD'])	// PARA EJETUTAR BACK-UP DE OTRAS BBDD
			{
				mysql_close($this->conexion);
				$this->db=$_REQUEST['nombre_bbdd'];
				$this->conexion=@mysql_connect($this->servidor,$this->user,$this->pass);
			}

			if(mysql_select_db($this->db))
				return 1;
			else return 0;
		}
	}

	function query($sql,$debug=0)
	{
		if($this->eco==2 || $debug)
		{
			$dat=$sql."\r\n\r\n";
			$res=mysql_query('EXPLAIN '.$sql,$this->conexion);
			while($a=mysql_fetch_row($res))
				$dat.="TABLE: $a[0]\t\tTYPE: $a[1]\r\nPOSSIBLE KEYS: $a[2]\r\nKEY: $a[3]\t\tKEY LEN:$a[4]\t\tREF: $a[5]\r\nROWS: $a[6]\t\tEXTRA: $a[7]\r\n\r\n";
			$this->debug($dat,0,3);
		}

		if(!$res=mysql_query($sql,$this->conexion))
		{
			$error=mysql_error();

			$fich=fopen('err/err.log','a');

			if($fich)
			{
				fputs($fich,"\r\n\r\n$sql\r\n\r\n".$error."\r\nen script: ".substr($_SERVER['SCRIPT_FILENAME'],strrpos($_SERVER['SCRIPT_FILENAME'],'/')+1)."\r\n...........\r\n");
				fclose($fich);
			}
			else
				echo '<b>SQL ERROR:</b><br />'.$error.'<br /><br /><p style=color:red><i>'.$sql.'</i>';

			$this->debug("ERROR EN QUERY:\r\n$sql\r\n$error\r\n\r\n",0,3);

			if(!$this->xml)
				die('<table width="100%" height="100%"><tr><td align="center" valign="middle"><hr style="color:#000000;height:6px"><b style="font-family:Arial;font-size:11px;color:#FF0000">ERROR DE CONEXIÓN A LA BASE DE DATOS:</b><br />'.$error.'<hr style="color:#000000;height:6px"><h2>revisar el LOG para m&aacute;s detalles</h2></td></tr></table>');
			else
			{
				header('Content-Type: text/xml');
				die('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><respuesta><resultado>ERRBBDD</resultado></respuesta>');
			}
		}
		return $res;
	}

	function fila($res)
	{
		return mysql_fetch_row($res);
	}

	function fila2($res)
	{
		return mysql_fetch_assoc($res);
	}

	function lastid()
	{
		return mysql_insert_id();
	}

	function numres($res)
	{
		return mysql_num_rows($res);
	}

	function resetres(&$res)
	{
		mysql_data_seek($res,0);
	}

	function valor($query)
	{
		$res=$this->query($query);
		$a=$this->fila($res);
		return $a[0];
	}

}

?>
