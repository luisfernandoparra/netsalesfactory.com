<?php
class MenuNivel1 extends Base
{
	//Datos propios de la clase
	var $id;
	var $linkmenu;
	var $nombreTabla;
	var $posicionSolapa;	// POSICI�N DE LA VENTANA RESPECTO A LAS DEM�S DE SU MISMA SECCI�N E IDENTIFICADOR PARA LOS CAMPOS DE LAS BUSQUEDAS

//******************************************************
//	INICIALIZACI�N DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla='mnu_level_one';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las b�squedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->getAdminLevel();	// RETRIEVE CONSTANTS LEVELS USER
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE ID="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->linkMenu=$this->output_datos($a['linkMenu']);
		$this->etiqueta=$this->output_datos($a['etiqueta']);
		$this->alias=$a['alias'];
		$this->orden=$a['orden'];
		$this->activo=$a['activo'];
		$this->scope_element=$a['scope_element'];
		$this->id_usuario=$a['id_usuario'];
		$this->tip=$a['tip'];
		return;
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(id) FROM '.$this->sql->db.'.contenidos WHERE id_niv_one="'.$this->id.'"';
//echo $query;exit;
		$existen_paginas=$this->sql->valor($query);

		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		if(!$this->id) $this->id=0;

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id,linkMenu,etiqueta,alias,orden,activo,scope_element,tip,id_usuario) VALUES ("'.$this->id.'", "'.trim($this->linkMenu).'", "'.trim($this->etiqueta).'", "'.trim($this->alias).'","'.$this->orden.'","'.$this->activo.'","'.$this->scope_element.'","'.trim($this->tip).'", "'.$this->id_usuario.'")';
//echo'<pre>';print_r($_REQUEST);print_r($this);

//echo $query;exit;
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos()
	{
?>
<tr>
	<td>
<?php
		echo '<select id="scope_element" name="scope_element" style="width:50px;">';
		echo '<option value=-1 ';
		if($this->scope_element==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->scope_element == 1)
			echo 'selected=\'selected\'';
		echo '>Solapas</option>';

		echo '<option value=1 ';
		if($this->scope_element == 2)
			echo 'selected=\'selected\'';
		echo '>Footer</option>';
		echo '</select>';
?>
	</td>
	<td align="right">Texto Men�:</td>
	<td colspan="1"><input type='text' id="etiqueta" name='etiqueta' value='<?=$this->etiqueta?>' style='width:150'></td>
	<td align="right">Alias:</td>
	<td><input type='text' id='alias' name='alias' value='<?=$this->alias?>' style='width:120'></td>
	<td align="right" title="Link a una p�gina externa">Link:</td>
	<td colspan="3"><input type='text' id='linkMenu' name='linkMenu' value='<?=$this->linkMenu?>' style='width:164px'></td>
</tr>
<tr>
	<td align="right" title="El men� es activo?">Est� activo:</td>
	<td>
<?php
		echo '<select id="activo" name="activo" style="width:50px;">';
		echo '<option value=-1 ';
		if($this->activo==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->activo==0 && isset($this->activo))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->activo==1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>
	<td align="right">Posici�n:</td>
	<td><input type='text' id='orden' name='orden' value='<?=$this->orden?>' style='width:30px;text-align:right' maxlength="2"></td>
	<td align="right">Tip:</td>
	<td><input type='text' id='tip' name='tip' value='<?=$this->tip?>' style='width:120'></td>
	<td align="right" title="Usuario propietario de este determinado elemento o accesible a todos">Usuario:</td>
	<td>
<?php
		$salida='';
		$query='SELECT id, CONCAT(nombre," ",apellidos) AS theName,nivel_acceso FROM '.$this->sql->db.'.usuarios WHERE activo=1 ORDER BY nombre';
		$res2=$this->sql->query($query);
		$salida.='<option value="0"';

		if(isset($this->id_usuario) && $this->id_usuario == 0) $salida.=' selected="selected" ';
		$salida.='>Global</option>';

		while($arra=$this->sql->fila2($res2))
		{
			//SE OMITEN OTROS USUARIOS CON NIVEL SUPERIOR DE ACCESO AL DEL USUARIO ACTUAL
			if($_SESSION['usuarioNivel'] < $arra['nivel_acceso'])	continue;

			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->id_usuario == $arra['id']) $salida.=' selected="selected" ';
			$salida.='>'.utf8_encode($arra['theName']).'</option>';
		}

		echo '<select id="id_usuario" name="id_usuario" style=\'width:165px\' ><option value="-1" class="tx10" style="color:#666">?</option>';
		echo $salida;
		echo '</select>';
?>
	</td>
</tr>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
	if(document.f<?=$idunico?>.etiqueta.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.etiqueta);
		document.f<?=$idunico?>.etiqueta.focus();
		alert('Por favor, la etiqueta que aparecer� Men�');
		restauraCampo(document.f<?=$idunico?>.etiqueta);
		return;
	}
	if(document.f<?=$idunico?>.alias.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.alias);
		document.f<?=$idunico?>.alias.focus();
		alert('Por favor, IMPRESCINDIBLE el alias (solo letras alfabeto ingl�s)');
		restauraCampo(document.f<?=$idunico?>.alias);
		return;
	}

	err=controlNumerico(document.f<?=$idunico?>.orden.value,'orden','f<?=$idunico?>',1);
	if(err)
	{
		document.f<?=$idunico?>.orden.value='';
		return false;
	}
	if(document.f<?=$idunico?>.orden.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.orden);
		document.f<?=$idunico?>.orden.focus();
		alert('Por favor, indique la posici�n del elemento del men�');
		restauraCampo(document.f<?=$idunico?>.orden);
		return;
	}
	if($('#id_usuario').val() < 0)
	{
		resaltarCampo($("#id_usuario")[0]);
		$("#id_usuario").focus();
		alert('Por favor, indique un usuario');
		restauraCampo($("#id_usuario")[0]);
		return;
	}

	document.f<?=$idunico?>.submit();
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}
-->
</script>
<?php
		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->linkMenu=$datos['linkMenu'];
		$this->etiqueta=$datos['etiqueta'];
		$this->alias=$datos['alias'];
		$this->orden=$datos['orden'];
		$this->activo=$datos['activo'];
		$this->scope_element=$datos['scope_element'];
		$this->id_usuario=$datos['id_usuario'];
		$this->tip=$datos['tip'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N�mero registros: ')
	{
		$idunico=rand(1,10000).time();
//		if (count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b�squeda:</span> ";
//
//		$query='SELECT * FROM '.$this->nombreTabla.' WHERE 1 ';
//
//		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID
//
////$_SESSION['usuarioNivel']
//		$_SESSION['filtros']['offset_u']*=1;
//		$querytotal='SELECT COUNT(*) FROM '.$this->nombreTabla.' WHERE 1 '.$filtroSql;
//
//		$ordenacion='orden'; $ordenarHacia=' ASC';
//		if($_REQUEST['ordenarPor'])
//			$ordenacion=substr($_REQUEST['ordenarPor'],8);
//
//		if($_REQUEST['sentidoOrdenacion'])
//			$ordenarHacia=' DESC';
//
//		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
//		$num_res=$this->sql->valor($querytotal);
//		$res=$this->sql->query($query);
//		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';

		$query='SELECT lab.*,u.nivel_acceso FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		$querytotal='SELECT COUNT(lab.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 '.$filtroSql;

		$ordenacion='orden'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;

		if($_SESSION['filtros']['visitantes_distintos_busca'])
		{
			$res2=$this->sql->query($querytotal);
			$num_res=mysql_num_rows($res2);
		}
		else
			$num_res=$this->sql->valor($querytotal);

		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $this->levelAccessMaxLevel.'<hr>';
//echo $query;


//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->

<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='780' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
<?php
		echo '<td width="60" title="Buscar por usuarios">Del usuario:</td>
			<td>';
		$salida='';
		$query='SELECT id, CONCAT(nombre," ",apellidos) AS theName, nivel_acceso FROM '.$this->sql->db.'.usuarios WHERE activo=1 ORDER BY nombre';
		$res2=$this->sql->query($query);
		$salida.='<option value="0"';

		if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN'] == 0) $salida.=' selected="selected" ';
		$salida.='>Global</option>';

		while($arra=$this->sql->fila2($res2))
		{
			//SE OMITEN OTROS USUARIOS CON NIVEL SUPERIOR DE ACCESO AL DEL USUARIO ACTUAL
			if($_SESSION['usuarioNivel'] < $arra['nivel_acceso'])	continue;

			$salida.='<option value=\''.$arra['id'].'\'';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN'] == $arra['id'])
				$salida.=' selected="selected"';
			$salida.='>'.utf8_encode($arra['theName']).'</option>';
		}

		echo '<select id="'.$this->posicionSolapa.'id_usuario_buscN" name="'.$this->posicionSolapa.'id_usuario_buscN" style=\'width:90px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select></td>';
?>
	<td width="52" align="right">Etiqueta:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>etiqueta_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'etiqueta_busca_u']?>' style='width:80px'></td>
	<td width="20">Link:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>linkMenu_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'linkMenu_busca_u']?>' style='width:80px'></td>
<!--
	<td width="30">Activo:</td>
	<td>
<?php
		$arraActivo[0]='NO';
		$arraActivo[1]='S�';
		echo '<select name="'.$this->posicionSolapa.'activo_buscN" style=\'width:50px;\'>';
		echo '<option value="-1" >?</option>';
		foreach($arraActivo as $key=>$value)
		{
			echo '<option value='.$key.' ';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'activo_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'activo_buscN']==$key)
				echo 'selected=\'selected\'';
			echo ' >'.$value.'</option>';
		}
		echo '</select>';
?>
  </td>
-->
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->


<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'34%','','60%','21%');

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		if(!$_SESSION['forcedLangSite']) $arraTitulares[]='Idioma';
		$arraTitulares[]='Etiqueta';
		$arraTitulares[]='Alias';
		$arraTitulares[]='Posici&oacute;n';
		$arraTitulares[]='Usuario';
		$arraTitulares[]='Activo';
		$arraTitulares[]='Destino';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		if(!$_SESSION['forcedLangSite']) $arraCamposOrdenar[]='id_idioma';
		$arraCamposOrdenar[]='etiqueta';
		$arraCamposOrdenar[]='alias';
		$arraCamposOrdenar[]='orden';
		$arraCamposOrdenar[]='id_usuario';
		$arraCamposOrdenar[]='activo';
		$arraCamposOrdenar[]='scope_element';

		 // no se incluye el valor para el ID
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=50;
		$arraLongitudes[]=49;
		$arraLongitudes[]=29;
		$arraLongitudes[]=29;
		$arraLongitudes[]=42;
		$arraLongitudes[]=60;
		$arraLongitudes[]=60;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=25;	// id
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=90;
		$arraLongitudesTitulares[]=200;
		$arraLongitudesTitulares[]=180;
		$arraLongitudesTitulares[]=50;
		$arraLongitudesTitulares[]=140;
		$arraLongitudesTitulares[]=45;
		$arraLongitudesTitulares[]=45;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='rop';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;
		$conutRowsDisplayed=0;

		while($a=$this->sql->fila2($res))
		{
		  $rop=0;
			//	SOLO SE PERMITEN VER LOS ELEMENTOS PROPIOS Y LOS GLOBALES (EXCEPTO PARA GOOD)
		  if($_SESSION['id_usuario'] != $a['id_usuario'] && $a['id_usuario'] != 0 && $this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])
				$rop=1; // fila deshabilitada

			$xc=0; $estaActivo='<center style=color:orange>no</center>';
			if($a['activo']==-1) $esActivo='?';
			if($a['activo']==0) $esActivo='<span style=color:red>NO</span>';
			if($a['activo']==1) $esActivo='SI';
			if($a['activo']==1) $esActivo='SI';
			$query='SELECT CONCAT(nombre," ",apellidos) AS theName FROM '.$this->sql->db.'.usuarios WHERE id="'.$a['id_usuario'].'"';
			$elUsuario=utf8_encode($this->sql->valor($query));
			$elUsuario=$elUsuario?$elUsuario:'<span style=color:red;><b>Todos</b></span>';

			if($a['activo']) $estaActivo='<center style=color:lightgreen><b>SI</b></center>';

			$tmpArr[0]=$a['id'];
			array_push($tmpArr,$a['etiqueta']);
			array_push($tmpArr,$a['alias']);
			array_push($tmpArr,$a['orden']);
			array_push($tmpArr,$elUsuario);
			array_push($tmpArr,$esActivo);
			array_push($tmpArr,$a['scope_element']);
			array_push($tmpArr,$rop);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
			$conutRowsDisplayed++;
		}

		$idRefPops=0;
		$txtRefEliminar=2;
		$posicFondo=0;	// NUMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<script>
$(function(){
	if("<?=$rop?>")	// AVISO DE REGISTROS AJENOS
	{
		setTimeout('$("#tdTitularFormulario").append("&nbsp;&nbsp;<b>NOTA</b>: existen registros de otros usuarios, est&aacute;n deshabilitados");',1000);
	}
});
</script>
<?php
	}
}
?>
