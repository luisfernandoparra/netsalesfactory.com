<?php
class paramsSemGoogle extends Base
{
	//Datos propios de la clase
	public $id;
	public $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEM�?S DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS
	public $linkmenu;

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'sem_google_params';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->getAdminLevel();	// RETRIEVE CONSTANTS LEVELS USER
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->id_landing=$a['id_landing'];
		$this->conversion_id=$a['conversion_id'];
		$this->conversion_language=$a['conversion_language'];
		$this->conversion_format=$a['conversion_format'];
		$this->conversion_color=$a['conversion_color'];
		$this->conversion_label=$this->output_datos($a['conversion_label']);
		$this->remarketing_only=$a['remarketing_only'];
		$this->google_ga=$a['google_ga'];
		return;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->id_landing=$datos['id_landing'];
		$this->conversion_id=$datos['conversion_id'];
		$this->conversion_language=$datos['conversion_language'];
		$this->conversion_format=$datos['conversion_format'];
		$this->conversion_color=$datos['conversion_color'];
		$this->conversion_label=htmlspecialchars_decode($datos['conversion_label']);
		$this->remarketing_only=$datos['remarketing_only'];
		$this->google_ga=$datos['google_ga'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
//		$query='SELECT COUNT(c.id) + IFNULL(mlt.enabled,0) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS mlt LEFT JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'contenidos AS c ON c.id_niv_two=mlt.id WHERE mlt.id="'.(int)$this->id.'"';
//		$existen_paginas=$this->sql->valor($query);
		$existen_paginas=0;
//echo $query;die();
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.'(id, id_landing, conversion_id, conversion_language, conversion_format, conversion_color, conversion_label, remarketing_only, google_ga) VALUES ('.(int)$this->id.', '.(int)$this->id_landing.', \''.utf8_decode($this->conversion_id).'\', \''.utf8_decode($this->conversion_language).'\', \''.utf8_decode($this->conversion_format).'\', \''.utf8_decode($this->conversion_color).'\', \''.utf8_decode($this->conversion_label).'\', '.(int)$this->remarketing_only.', \''.utf8_decode($this->google_ga).'\')';
//echo $query.'<pre>'.$query;print_r($_REQUEST);print_r($this);die();
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos($nombreForm=0)
	{
?>
<tr>
	<td align="right" class="tooltip" title="Landing a la que pertenecen los parámetros para<br />el Google <b>SEM</b> (Search Engine Marketing).<br /><span style=color:#bbb;>Recuerda que al insertar un <b>nuevo registro</b>, aparecen<br />disponibles únicamente las promociones que no tienen<br />todavía parámetros SEM guardados.</span>">Landing:</td>
	<td>
<?php
		$salida='';
		$sqlFilter=1;

		if(!$this->id)	// SE LIMITA LA LISTA A LANDING SIN CONTENIDOS YA EXISTENTES
			$sqlFilter.=' && lsc.id NOT IN (SELECT id_landing FROM '.$this->sql->db.'.'.$this->nombreTabla.')';

 		$query='SELECT DISTINCT(lsc.id), lsc.landing_name AS landingElement FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_site_config AS lsc WHERE '.$sqlFilter.' ORDER BY lsc.landing_name';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->id_landing == $arra['id']) $salida.=' selected="selected"';
			$salida.='>'.($arra['landingElement'] ? $arra['landingElement'] : '???').'</option>';
		}

		echo '<select id="id_landing" name="id_landing" style=\'width:150px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>

	<td align="right" class="tooltip" title="Parámetro ´<b>google_conversion_id</b>´.<br />Ej.: 971711280">C. id:</td>
	<td><input type='text' id='conversion_id' name='conversion_id' value='<?=$this->conversion_id?>' style='width:80px' /></td>

	<td align="right" class="tooltip" title="Parámetro ´<b>google_conversion_language</b>´.<br />Ej.: en">C. language:</td>
	<td><input type='text' id='conversion_language' name='conversion_language' value='<?=$this->conversion_language?>' style='width:20px' maxlength="2" /></td>

	<td align="right" class="tooltip" title="Parámetro ´<b>google_conversion_format</b>´.<br />Ej.: 2">C. format:</td>
	<td><input type='text' id='conversion_format' name='conversion_format' value='<?=$this->conversion_format?>' style='width:40px;text-align:right;' /></td>
</tr>
<tr>

	<td align="right" class="tooltip" title="Parámetro ´<b>google_remarketing_only</b>´.<br />Valores: <i>true/false</i>">Remarketing only:</td>
	<td width="80">
<?php
		echo '<select id="remarketing_only" name="remarketing_only" style="width:44px;">';
		echo '<option value=-1 ';
		if($this->remarketing_only == -1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->remarketing_only == 0 && isset($this->remarketing_only))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->remarketing_only == 1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>
	<td align="right" class="tooltip" title="Parámetro ´<b>google_conversion_color</b>´.<br />Ej.: FFFFFF">C. color:</td>
	<td><input type='text' id='conversion_color' name='conversion_color' value='<?=$this->conversion_color?>' style='width:80px;text-align:left;' /></td>

	<td align="right" class="tooltip" title="Parámetro ´<b>google_conversion_label</b>´.<br />Ej.: 3XiFCOiE3AgQsMaszwM<br />Recordar que si este pixel es solo para remarketing de Landing,<br>este campo debe tener el valor `<b>remarketingLandingAdWords</b>`">C. label:</td>
	<td><input type='text' id='conversion_label' name='conversion_label' value='<?=  utf8_encode($this->conversion_label)?>' style='width:90%;' /></td>

	<td align="right" class="tooltip" title="Parámetro ´<b>GA (ID Google Analitycs)</b>´.<br /><b> <span style=color:<?=$_SESSION['colorTextWarning']?>>*</span> NOTA:</b> solo si este valor está definido,<br />se cargará el script de G. Analitycs.<br />Ej.: UA-51783940-1">GA code:</td>
	<td><input type='text' id='google_ga' name='google_ga' value='<?=  utf8_encode($this->google_ga)?>' style='' /></td>

</tr>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--

function revisa()
{
	if($('#id_landing').val() < 0)
	{
		displayFormError("#id_landing","Por favor, selecciona una landing");
		return false;
	}
	document.f<?=$idunico?>.submit();
}

$(document).ready(function(){
});

function resaltarCasilla(id,xx,zz)	// ABRIR LANDIN ALMACENADA EN BBDD
{
	var tmpElem=$("#"+id).parent();
	var tmpObj=$(tmpElem[0]).children();

	$(tmpObj).each(function(tdPos,tdContent){
		if(tdPos == 1)//POSICION DEL TD CON EL ID DE LA LANDING
		{
			window.open("../?cr="+$(tdContent).html(),"preview","");
			return false;
		}
	});
}

-->
</script>
<?php
		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$idunico=rand(1,10000).time();
		//$query='SELECT lab.*,u.nivel_acceso FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 ';
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		//$querytotal='SELECT COUNT(lab.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 '.$filtroSql;
		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='id_landing'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;

		if($_SESSION['filtros']['visitantes_distintos_busca'])
		{
			$res2=$this->sql->query($querytotal);
			$num_res=mysql_num_rows($res2);
		}
		else
			$num_res=$this->sql->valor($querytotal);

		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $query;

//******************************************************


//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
var idVisualizado=-1;
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->


<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='878' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='12' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td title='Buscar por <b>landing</b> conocida' width="60" align="right" class='tooltip'>Landing:</td>
	<td>
<?php
 		$query='SELECT lsc.id, lsc.landing_name AS landingElement FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_site_config AS lsc INNER JOIN '.$this->sql->db.'.'.$this->nombreTabla.' AS sgp ON lsc.id = sgp.id_landing WHERE 1 ORDER BY lsc.landing_name';
//&& lsc.id=\''.$_SESSION['filtros'][$this->posicionSolapa.'id_landing_buscN'].'\'
//		$query='SELECT lc.id_landing, lc.prefix FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc WHERE lc.is_enabled ORDER BY lc.prefix';
		$res2=$this->sql->query($query);

		$salida='<select id="'.$this->posicionSolapa.'id_landing_buscN" name="'.$this->posicionSolapa.'id_landing_buscN" style=\'width:128px;\'>';

		$salida.='<option value=-1 ';
		if($_SESSION['filtros'][$this->posicionSolapa.'id_landing_buscN'] == -1)
			$salida.='selected=\'selected\'';
		$salida.='>?</option>';

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';

			if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_landing_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_landing_buscN'] == $arra['id'])
				$salida.=' selected="selected"';

			$salida.='>'.$arra['landingElement'].'</option>';
		}

		$salida.='</select>';
		echo $salida;
?>
	</td>

	<td width="70" align="right">Conversion ID:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>conversion_id_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'conversion_id_busca_u']?>' style='width:80px' /></td>

	<td width="40" align="right" title="Buscar por ´conversion language´" class="tooltip">Lang:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>conversion_language_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'conversion_language_busca_u']?>' style='width:20px' maxlength="2" /></td>

	<td title='Es ´remarketing only`?' align="right" class="tooltip">Remarketing:</td>
	<td>
<?php
		$arrSN=array(0=>'NO',1=>'S&iacute;');
		echo '<select id="'.$this->posicionSolapa.'remarketing_only_buscN" name="'.$this->posicionSolapa.'remarketing_only_buscN" style=\'width:50px;\' >';
		echo '<option value=-1 ';
		if($this->remarketing_only==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		for($n=0;$n < 2;$n++)
		{
			echo '<option value='.$n.' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'remarketing_only_buscN']==$n && isset($_SESSION['filtros'][$this->posicionSolapa.'remarketing_only_buscN']))
				echo 'selected=\'selected\'';
			echo ' >'.$arrSN[$n].'</option>';
		}
		echo '</select>';
?>
	</td>

<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->


<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,$this->modalHeight,'',$this->modalWidth,$this->modalLeftPos);

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='ID land.';
		$arraTitulares[]='Nombre Landing';
		$arraTitulares[]='Conv. ID';
		$arraTitulares[]='Conv. label';
		$arraTitulares[]='C. lang.';
		$arraTitulares[]='C. format';
		$arraTitulares[]='C. color';
		$arraTitulares[]='Remark.';
		$arraTitulares[]='Google ga.';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='id_landing';
		$arraCamposOrdenar[]='id_landing';
		$arraCamposOrdenar[]='conversion_id';
		$arraCamposOrdenar[]=$this->output_datos('conversion_label');
		$arraCamposOrdenar[]='conversion_language';
		$arraCamposOrdenar[]='conversion_format';
		$arraCamposOrdenar[]='conversion_color';
		$arraCamposOrdenar[]='conversion_language';
		$arraCamposOrdenar[]='google_ga';

		 // no se incluye el valor para el ID
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=50;
		$arraLongitudes[]=24;
		$arraLongitudes[]=24;
		$arraLongitudes[]=12;
		$arraLongitudes[]=21;
		$arraLongitudes[]=37;
		$arraLongitudes[]=2;
		$arraLongitudes[]=42;
		$arraLongitudes[]=60;
		$arraLongitudes[]=60;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=25;	// id
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=90;
		$arraLongitudesTitulares[]=40;
		$arraLongitudesTitulares[]=120;
		$arraLongitudesTitulares[]=70;
		$arraLongitudesTitulares[]=140;
		$arraLongitudesTitulares[]=40;
		$arraLongitudesTitulares[]=50;
		$arraLongitudesTitulares[]=50;
		$arraLongitudesTitulares[]=40;
		$arraLongitudesTitulares[]=90;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='rop';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;
		$conutRowsDisplayed=0;
		$selectLabelLevelOne=$_SESSION['forcedLangSite'] ? 'mlo.landing_identifier AS nameMenu' : 'CONCAT(mlo.landing_identifier," (",i.prefijo,") AS nameMenu")';

		while($a=$this->sql->fila2($res))
		{
		  $rop=null;
			//	SOLO SE PERMITEN VER LOS ELEMENTOS PROPIOS Y LOS GLOBALES (EXCEPTO PARA GOOD)
		  if($_SESSION['id_usuario'] != $a['id_usuario'] && $a['id_usuario'] != 0 && $this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])
				$rop=1; // fila deshabilitada

			$xc=0; $estaActivo='<center style=color:orange>no</center>';
			$query='SELECT lc.prefix,lsc.landing_name FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_site_config AS lsc INNER JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc ON lc.id_landing=lsc.client_id  WHERE lsc.id='.(int)$a['id_landing'];
			$customerDetails=$this->sql->query($query);
			$dataCustomer=$this->sql->fila2($customerDetails);
//			$query='SELECT lc.prefix FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc WHERE lc.id_landing='.(int)$a['element_type'];
//			$reCustomer=$this->sql->valor($query);

			if($a['remarketing_only']==-1) $arrSN='?';
			if(!$a['remarketing_only']) $arrSN='<span style=color:red>NO</span>';
			if($a['remarketing_only']==1) $arrSN='SI';
			if($a['registrado']==1) $esRegistrado='<span style=color:red>SI</span>';
//			$elIdioma=utf8_encode($this->sql->valor($query));
			$elIdioma=$elIdioma?$elIdioma:'<span style=color:red;><b>Todos</b></span>';

			if($a['remarketing_only']) $estaActivo='<center style=color:lightgreen><b>SI</b></center>';

			$tmpArr[0]=$a['id'];
			array_push($tmpArr,$a['id_landing']);
			array_push($tmpArr,$dataCustomer['landing_name']);
			array_push($tmpArr,utf8_encode($a['conversion_id']));
			array_push($tmpArr,utf8_encode($a['conversion_label']));
			array_push($tmpArr,utf8_encode($a['conversion_language']));
			array_push($tmpArr,utf8_encode($a['conversion_format']));
			array_push($tmpArr,utf8_encode($a['conversion_color']));
			array_push($tmpArr,$arrSN);
			array_push($tmpArr,utf8_encode($a['google_ga']));
		  array_push($tmpArr,$rop);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
			$conutRowsDisplayed++;
		}

		$idRefPops=0;
		$txtRefEliminar=3;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
