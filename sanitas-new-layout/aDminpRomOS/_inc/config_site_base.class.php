<?php
class configSiteBase extends Base
{
	//Datos propios de la clase
	var $id;
	public $correctImagePath;

//******************************************************
//
//******************************************************
	public function __construct($sql)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'system_config';
		$this->posicionSolapa=$posicionSolapa;
		$this->correctImagePath='../';
		return;
	}

//******************************************************
//	CARGAR CONFIGURACION DE SISTEMA (ID=1, FORZADO PARA MANTENER LA COMPATIBILIDAD)
//******************************************************
	function cargaConfig()
	{
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
//		$this->id=$a['id'];
		$this->content_column=$a['content_column'];
		$this->img_logo=$a['img_logo'];
		$this->txt_site_primary=$a['txt_site_primary'];
		$this->txt_site_secondary=$a['txt_site_secondary'];
		$this->vertical_menu=$a['vertical_menu'];
		$this->social_networking=$a['social_networking'];
		$this->min_level_company_edit=$a['min_level_company_edit'];
		$this->word_seeker=$a['word_seeker'];
		$this->htaccess_enabled=$a['htaccess_enabled'];
		$this->block_bottom_mode=$a['block_bottom_mode'];
		$this->forced_lang_site=$a['forced_lang_site'];
		$_SESSION['forcedLangSite']=$this->forced_lang_site;
		return;
	}

//******************************************************
//	SE CARGAN EN EL OBJETO LOS CAMPOS A MANEJAR
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->content_column=$datos['content_column'];
		$this->img_logo=$datos['img_logo'];
		$this->txt_site_primary=$datos['txt_site_primary'];
		$this->txt_site_secondary=$datos['txt_site_secondary'];
		$this->vertical_menu=$datos['vertical_menu'];
		$this->min_level_company_edit=$datos['min_level_company_edit'];
		$this->social_networking=$datos['social_networking'];
		$this->word_seeker=$datos['word_seeker'];
		$this->block_bottom_mode=$datos['block_bottom_mode'];
		$this->forced_lang_site=$datos['forced_lang_site'];
		$this->quitar_archivo=$_REQUEST['quitar_archivo'];
		$this->guarda_datos();
		$txtMsgREcordInsertado=1;
		return;
	}

//******************************************************
//	GUARDAR DATOS DE INSERCIONES Y EDICIONES
//******************************************************
	function guarda_datos()
	{
		$isError=null;
//echo '<br /><pre>';print_r($_REQUEST);print_r($_FILES);print_r($this);exit;

		if((count($_FILES['img_logo']) || count($_FILES['thumbnail'])) && !($this->quitar_archivo || $this->quitar_miniatura))	// SOLO SI ES UNA IMAGEN
		{
			if(!is_dir($this->correctImagePath.$this->path_imagenes))
			{
				if(!@mkdir($this->correctImagePath.$this->path_imagenes))	// SI SE PRODUCIRSE UN ERROR DE CREACIÓN DE CARPETA, SE VISUALIZA UN MENSAJE Y SE ABORTA LA OPERACIÓN
				{
					$this->mensajeSalida='<span class=enc2 style=background-color:red;color:#fff>&nbsp;ERROR GRAVE&nbsp;</span>&nbsp;NO EXISTE LA CARPETA DE IMAGENES';
					$this->mensajeSalida='ERROR GRAVE, EN LA CARPETA DE IMAGENES';
					$isError=1;
				}
			}

			// SOLO SOLO SE PUEDEN SUBIR AL SERVIDOR IMAGENES VALIDAS
			if($_FILES['img_logo']['name'] && !$isError)
				if($_FILES['img_logo']['type']!='image/x-png' && $_FILES['img_logo']['type']!='image/pjpeg' && $_FILES['img_logo']['type']!='image/jpeg' && $_FILES['img_logo']['type']!='image/gif' && $_FILES['img_logo']['type']!='image/png' && $_FILES['img_logo']['type']!='image/x-icon')
				{
					$this->mensajeSalida='ARCHIVO NO SOPORTADO, SOLO IMAGENES';
					$isError=1;
				}
			if($_FILES['thumbnail']['name'] && !$isError)
				if($_FILES['thumbnail']['type']!='image/x-png' && $_FILES['thumbnail']['type']!='image/pjpeg' && $_FILES['thumbnail']['type']!='image/jpeg' && $_FILES['thumbnail']['type']!='image/gif' && $_FILES['thumbnail']['type']!='image/png' && $_FILES['thumbnail']['type']!='image/x-icon')
				{
					$this->mensajeSalida='ARCHIVO NO SOPORTADO PARA LA MINIATURA, SOLO IMAGENES';
					$isError=1;
				}

			$tamanio=$_FILES['img_logo']['size'];
			$tamanioThumb=$_FILES['thumbnail']['size'];

			if($tamanio > $this->tamanioMaximoImagenes && !$isError)	// SE HA SUPERADO EN BYTES EL TAMAÑO MAXIMO PERMITIDO PARA EL OBJETO DE FLASH
			{
				$tamanio=number_format(($tamanio/1000),2,',','.').'Kb';
				$this->mensajeSalida='ERROR: es demasiado grande: '.$tamanio.' (MAX: '.($this->tamanioMaximoImagenes/1000).'Kb)';
				$isError=1;
			}

			if($tamanioThumb > $this->tamanioMaximoThubnail && !$isError)	// SE HA SUPERADO EN BYTES EL TAMAÑO MAXIMO PERMITIDO PARA EL OBJETO DE FLASH
			{
				$tamanioThumb=number_format(($tamanioThumb/1000),2,',','.').'Kb';
				$this->mensajeSalida='ERROR: la miniatura es demasiado grande: '.$tamanioThumb.' (MAX: '.($this->tamanioMaximoThubnail/1000).'Kb)';
				$isError=1;
			}

//			if(is_uploaded_file($_FILES['thumbnail']['tmp_name']) && !$isError)
//			{
//				$tmp_fileThumb='imx'.date('dmyis',time()).$_FILES['thumbnail']['name'];
//				copy($_FILES['thumbnail']['tmp_name'], $this->correctImagePath.$this->path_imagenes.$tmp_fileThumb);
////echo $_FILES['imagen']['tmp_name'].'<br /><pre>';print_r($_REQUEST);print_r($this);exit;
//				$_imgThumb=', "'.$tmp_fileThumb.'" ';
//				$campoThumb=', thumbnail';
//				$campoUpdate.=', thumbnail="'.$tmp_fileThumb.'"';
//				$modif_thumbnail=1;
//			}

			if(is_uploaded_file($_FILES['img_logo']['tmp_name']) && !$isError)
			{
				$tmp_file=$_FILES['img_logo']['name'];
				copy($_FILES['img_logo']['tmp_name'], $this->correctImagePath.$this->path_imagenes.$tmp_file);
//echo $_FILES['imagen']['tmp_name'].'<br /><pre>';print_r($_REQUEST);print_r($this);exit;
				$_img=', "'.$tmp_file.'" ';
				$campoImg=', img_logo';
				$campoUpdate.=', img_logo="'.$tmp_file.'"';
			}
		}

		if($isError)	// ECHO PROCESS ERROR
		{
			$sriptName=substr($_SERVER['SCRIPT_NAME'],strrpos($_SERVER['SCRIPT_NAME'],'/')+1);
			$sriptName=substr($sriptName,0,-4);
			echo '<br /><br /><center><p ><h4 style="color:red">'.$this->mensajeSalida.'</h4></p><br /><br /><br /><a href="#null" style="width:100px" class="btn" onclick="document.location=\''.$sriptName.'.php'.'\';">volver a editar</a></center>';				exit;
		}

		if($this->quitar_archivo)	// SI SE HA SELECCIONADO QUITAR EL ARCHIVO, SE PROCEDE A BORRARLO DE LA TABLA
		{
			$_img=',""'; $campoImg=', img_logo';
			$this->img_logo='';
			$query='SELECT img_logo FROM '.$this->nombreTabla." WHERE id=".($this->id ? $this->id : 1);	// SE OBTIENE LA archivo A BORRAR
			$archivoBorrar=$this->sql->valor($query);
//echo $archivoBorrar.'<br /><pre>';print_r($_REQUEST);print_r($_FILES);print_r($this);exit;

			if($this->correctImagePath.$this->path_imagenes.$archivoBorrar)	// SI EXISTE EL ARCHIVO, SE BORRA FISICAMENTE
			{
				@unlink($this->correctImagePath.$this->path_imagenes.$archivoBorrar);
				$query='UPDATE '.$this->nombreTabla.' SET img_logo="" WHERE id='.($this->id ? $this->id : 1);
				$this->sql->query($query);
			}
		}

//		if($this->quitar_miniatura)
//		{
//			$_imgThumb=',""'; $campoThumb=', thumbnail';
//			$modif_thumbnail=1; $this->thumbnail='';
//			$query='SELECT thumbnail FROM '.$this->nombreTabla.' WHERE id='.$this->id;	// SE OBTIENE LA archivo A BORRAR
//			$archivoBorrar=$this->sql->valor($query);
//
//			if($this->correctImagePath.$this->path_imagenes.$archivoBorrar)	// SI EXISTE EL ARCHIVO, SE BORRA FISICAMENTE
//			{
//				@unlink($this->correctImagePath.$this->path_imagenes.$archivoBorrar);
//				$query='UPDATE '.$this->nombreTabla.' SET thumbnail="" WHERE id='.$this->id;
//				$this->sql->query($query);
//			}
//		}

			$query='UPDATE '.$this->sql->db.'.'.$this->nombreTabla.' SET content_column='.(int)$this->content_column.',vertical_menu='.(int)$this->vertical_menu.',social_networking='.(int)$this->social_networking.', word_seeker='.(int)$this->word_seeker.', block_bottom_mode='.$this->block_bottom_mode.', forced_lang_site='.$this->forced_lang_site.', min_level_company_edit='.$this->min_level_company_edit.', txt_site_primary=\''.$this->txt_site_primary.'\', min_level_company_edit=\''.$this->txt_site_secondary.'\' '.$campoUpdate.' WHERE id='.($this->id ? $this->id : 1);
		if(!$this->id)
	  $query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id, content_column, vertical_menu,social_networking,word_seeker, block_bottom_mode,forced_lang_site,min_level_company_edit,txt_site_primary,txt_site_secondary '.$campoImg.$campoThumb.') VALUES (1, '.$this->content_column.', '.$this->vertical_menu.', '.$this->social_networking.', '.$this->word_seeker.', '.$this->block_bottom_mode.', '.$this->forced_lang_site.', '.$this->min_level_company_edit.', \''.$this->txt_site_primary.'\', \''.$this->txt_site_secondary.'\' '.$_img.$_imgThumb.')';


//echo $query.'VERIFICAR ELIMINACION DE IMAGEN<br /><pre>';print_r($_REQUEST);print_r($this);die();
//echo $query.'<br /><pre>';print_r($_REQUEST);print_r($_FILES);exit;
		$this->sql->query($query);
		return;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
//alert(document.f<?=$idunico?>.name);
  document.f<?=$idunico?>.submit();
}


function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','logo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}

$(function(){
});
-->
</script>

<?php
		$permisoSuperAdmin=$this->obtenerNivelMaxAcceso();

		if($_SESSION['usuarioNivel'] != $permisoSuperAdmin)	// SOLO SE PERMITE EL ACCESO A ESTA FICHA A LOS SUPER-ADMINISTRADORES
		{
		  echo '<div class="txt" style="padding:2%;"><center><b>Usted no tiene permiso para acceder a esta ficha.</b></center><br /><br />Si necesita utilizar esta herramienta, por favor, p&oacute;ngase en contacto con el super-administrador del sitio o con el responsable de la empresa.</div>';
		  return false;
		}
		include('_inc/form_std_edit.php');

		return $idunico;
	}


//******************************************************
//	OBTENER EL NIVEL MAXIMO DE PERMISO DE ADMINISTRACION
//******************************************************
	function obtenerNivelMaxAcceso()
	{
	  $query='SELECT MAX(nivel_acceso) FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE activo';
	  $res=$this->sql->valor($query);
	  return $res;
	}

//******************************************************
//	FORMULARIO DE EDICION /INSERCION DE DATOS
//******************************************************
	function formulario_datos($nombreForm=0)
	{
	  $this->cargaConfig();
//echo'<pre>';print_r($this);
?>
<tr>
	<td title="Estado y posici&oacute;n de la columna de contenidos" align="right"><b>Columna contenidos</b>:</td>
	<td>
<?php
		$arraColContent=array(0=>'Oculta',1=>'Izquierda',2=>'Derecha');
		echo '<select id="content_column" name="content_column" style=\'width:90px;\'>';
		for($n=0;$n<3;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->content_column==$n && isset($this->content_column))
				echo 'selected=\'selected\'';
			echo ' >'.$arraColContent[$n].'</option>';
		}
		echo '</select>';
?>
	</td>
	<td title="Buscador integrado" align="right"><b>Buscador</b>:</td>
	<td>
<?php
		$arraColContent=array(0=>'No',1=>'Top banner',2=>'Bottom banner');
		echo '<select id="word_seeker" name="word_seeker" style=\'width:110px;\'>';
		for($n=0;$n<3;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->word_seeker==$n && isset($this->word_seeker))
				echo 'selected=\'selected\'';
			echo ' >'.$arraColContent[$n].'</option>';
		}
		echo '</select>';
?>
	</td>
		<td title="Nivel m&iacute;nimo para editar formularios en la oficina t&eacute;cnica" align="right"><b>Min level edit</b>:</td>
		<td>
<?php
		echo '<select id="min_level_company_edit" name="min_level_company_edit" style=\'width:90px;\'>';
		echo '<option value=-1 ';
		if($this->min_level_company_edit==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=0;$n<16;$n++)
		{
			if($n>$_SESSION['usuarioNivel']) break;	// SE PERMITE MODIFICAR DE NIVEL HASTA EL MAXIMO PERMITIDO AL USUARIO ACTUAL

			if($_SESSION['usuarioNivel']<11 && $n>10) continue;
			echo '<option value='.$n.' title="'.$n.'"';
			if($this->min_level_company_edit==$n)
				echo 'selected=\'selected\'';
			echo ' >'.$n.'</option>';
		}
		echo '</select>';
?>
		</td>
</tr>

<tr>
	<td title="Menú vertical" align="right"><b>Menú vertical</b>:</td>
	<td>
<?php
		$arraMnuVert=array(0=>'Oculta',1=>'Izquierda',2=>'Derecha');
		echo '<select id="vertical_menu" name="vertical_menu" style=\'width:90px;\'>';
		for($n=0;$n<3;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->vertical_menu==$n && isset($this->vertical_menu))
				echo 'selected=\'selected\'';
			echo ' >'.$arraMnuVert[$n].'</option>';
		}
		echo '</select>';
?>
		</td>
		<td title="M&oacute;dulo de redes sociales" align="right"><b>Redes sociales</b>:</td>
		<td>
<?php
		$arraEsActivo=array(0=>'No',1=>'Lateral',2=>'Footer left',3=>'Footer right',4=>'Box bottom');
		echo '<select id="social_networking" name="social_networking" style=\'width:100px;\'>';
		for($n=0;$n<5;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->social_networking==$n && isset($this->social_networking))
				echo 'selected=\'selected\'';
			echo ' >'.$arraEsActivo[$n].'</option>';
		}
		echo '</select>';
?>
		</td>
		<td><b>Texto Logo Web</b></td>
		<td><input type="text" id="txt_site_primary" name="txt_site_primary" value="<?=$this->txt_site_primary?>" maxlength="255" /></td>
  </tr>

	<tr>
	<td align="right" title="Modo del bloque del pi&eacute; p&aacute;gina"><b>Bloque footer</b>:</td>
	<td>
<?php
		$arraMnuVert=array(0=>'Standard',1=>'Box cerrado');
		echo '<select id="block_bottom_mode" name="block_bottom_mode" style=\'width:90px;\'>';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->block_bottom_mode==$n && isset($this->block_bottom_mode))
				echo 'selected=\'selected\'';
			echo ' >'.$arraMnuVert[$n].'</option>';
		}
		echo '</select>';
?>
		</td>
	<td title="Force to <b>ONLY</b> one languaje" align="right"><b>Force 1 lang</b>:</td>
	<td>
<?php
		$arraMnuVert=array(0=>'NO',1=>'SI');
		echo '<select id="forced_lang_site" name="forced_lang_site" style=\'width:46px;\'>';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->forced_lang_site==$n && isset($this->forced_lang_site))
				echo 'selected=\'selected\'';
			echo ' >'.$arraMnuVert[$n].'</option>';
		}
		echo '</select>';
?>
		</td>
		<td><b>Texto Logo sec.</b></td>
		<td><input type="text" id="txt_site_secondary" name="txt_site_secondary" value="<?=$this->txt_site_secondary?>" maxlength="255" /></td>
  </tr>

	<tr>
	<td align='right' valign="top">
<?php
			echo 'Im&aacute;gen:';
?>
  </td>
	<td valign="top" colspan="3"><input type="file" id="img_logo" name="img_logo" style="width:98%" />&nbsp;
<?php
		if($this->img_logo)	// SE VISYALIZA LA CASILLA PARA BORRAR LA IMAGEN SOLO EN EDICIÓN DE REGISTRO
		{
			echo '<br /><span title="Marcar para eliminar la imágen">Eliminar imagen: <input type="checkbox" id="quitar_archivo" name="quitar_archivo" value="1" style="border:0px;height:14px;width:14px;margin-right:20px;"></span>';
			// SE DUBUJA EL BOTÓN PARA ABRIR LA IMAGEN A TANAÑO REAL EN UNA VENTANA POP
			@$dat=getimagesize($this->correctImagePath.$this->path_imagenes.$this->img_logo);
			echo '<a href="#null" class="btn" style="border:1px solid green" onclick="im(\''.$this->correctImagePath.$this->path_imagenes.$this->img_logo.'\','.$dat[0].','.$dat[1].')" title="click para ver la imagen, tamaño máximo permitido: '.(number_format(($this->tamanioMaximoImagenes/1000),0,',','.')).'Kb">Ver la imágen actual</a>';

		}
		if(!$this->img_logo)	// SE VISYALIZA LA CASILLA PARA BORRAR LA IMAGEN SOLO EN EDICIÓN DE REGISTRO
			echo '<br /><span title="Este elemento del carrousel NO tiene la imágen" style="color:#000;background-color:'.$_SESSION['colorFondoWarning'].';padding-left:10px;padding-right:10px;"><b>&nbsp;SIN IMAGEN PARA EL LOGO DE LA WEB!&nbsp;</b></span>';
?>
	</td>
  </tr>

	<tr>
		<td colspan="10" width="400">
			<br />
		</td>
  </tr>
  <tr>
	<td colspan="10" width="400"><div id="mesajes_formulario"></div>
	</td>
  </tr>
<?php
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca='', $destinoedita='', $filtros='',$colFore='')
	{
	  return;
	}

}
?>