<?php
class ContenidosDinamicos extends Base
{
	//Datos propios de la clase
	public $id;
	public $nombreTabla;
	public $posicionSolapa;	// POSICION DE LA VENTANA RESPECTO A LAS DEMAS DE SU MISMA SECCION E IDENTIFICADOR PARA LOS CAMPOS DE LAS BUSQUEDAS
	public $arrTargets;

//******************************************************
//	INICIALIZACION DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'contenidos';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->arrTargets=array(
			'?',
			'Solapas',
			'Footer',
			'Extras'
		);
		$this->getAdminLevel();	// RETRIEVE CONSTANTS LEVELS USER
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->id_niv_one=$a['id_niv_one'];
		$this->id_niv_two=$a['id_niv_two'];
		$this->fecha=$a['fecha'];
		$this->txt_link=$a['txt_link'];
		$this->extra_content=$a['extra_content'];
		$this->id_usuario=$a['id_usuario'];
		$this->tipo=$a['tipo'];
		$this->url_link=$a['url_link'];
		$this->tooltip=utf8_encode($a['tooltip']);
		$this->activo=$a['activo'];
		$this->img_fondo_pagina=$a['img_fondo_pagina'];
		$this->css_class=$a['css_class'];
		$this->orden=$a['orden'];
		$this->group_ids=$a['group_ids'];
		return;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		$modif_respuesta=0;
		$enEdicion=$this->id ? '_editar':'';
		$groupsIds='';
		if($this->group_ids)
		{
		  foreach($this->group_ids as $key=>$value)
			$groupsIds.=$value.',';
		}

		if(trim($_FILES['img_fondo_pagina']['name']))
		{
			$dataContent='';
			if(count($_FILES['img_fondo_pagina']) && !$this->quitar_archivo)	// SOLO SI ES UNA IMAGEN
			{
				if(!is_dir($this->path_imagenes))
				{
					if(!@mkdir($this->path_imagenes))	// SI SE PRODUCIRSE UN ERROR DE CREACION DE CARPETA, SE VISUALIZA UN MENSAJE Y SE ABORTA LA OPERACION
					{
						$this->mensajeSalida='<span class=enc2 style=background-color:red;color:#fff>&nbsp;ERROR GRAVE&nbsp;</span>&nbsp;NO EXISTE LA CARPETA DE IMAGENES';
						$this->mensajeSalida='ERROR GRAVE, EN LA CARPETA DE IMAGENES';
					$isError=1;
					}
				}
				// SOLO SOLO SE PUEDEN SUBIR AL SERVIDOR IMAGENES VALIDAS
				if($_FILES['img_fondo_pagina']['name'])
					if($_FILES['img_fondo_pagina']['type']!='image/x-png' && $_FILES['img_fondo_pagina']['type']!='image/pjpeg' && $_FILES['img_fondo_pagina']['type']!='image/gif' && $_FILES['img_fondo_pagina']['type']!='image/png' && $_FILES['img_fondo_pagina']['type']!='image/x-icon' && $_FILES['img_fondo_pagina']['type']!='image/jpeg')
					{
						$this->mensajeSalida='ARCHIVO NO SOPORTADO, SOLO IMAGENES';
					$isError=1;
					}

				$tamanio=$_FILES['img_fondo_pagina']['size'];
//echo ($tamanio .'---'. $this->tamanioMaximoImagenes).'<br /><pre>';print_r($_FILES['imagen']);print_r($datosImagen[0]);exit;

				if($tamanio > $this->tamanioMaximoImagenes)	// SE HA SUPERADO EN BYTES EL TAMAÑO MAXIMO PERMITIDO PARA EL OBJETO DE FLASH
				{
					$tamanio=number_format(($tamanio/1000),2,',','.').'Kb';
					$this->mensajeSalida='ERROR: es demasiado grande: '.$tamanio.' (MAX: '.($this->tamanioMaximoImagenes/1000).'Kb)';

					if(!$_REQUEST['id'])	// ES UN RECORD NUEVO
					{
	$_REQUEST['logEstado']='editar imagen fondo pagina';
	$_REQUEST['id_grabar']=0;
	new $this->Log($sql, $id_pagina);

						echo '<br /><br /><center><p ><h4 style="color:red">'.$this->mensajeSalida.'</h4></p><br /><br /><br /><a href="#null" style="width:100px" class="btn" onclick="document.location=\''.$this->nombreFilePhpBase.$enEdicion.'.php?id='.$this->id.'\';">volver</a></center>';
						exit;;
					}
					else	// ES UN RECORD EDITADO
					{
	$_REQUEST['logEstado']='editar imagen fondo pagina';
	$_REQUEST['id_grabar']=$_REQUEST['id'];
						echo '<br /><br /><center><p ><h4 style="color:red">'.$this->mensajeSalida.'</h4></p><br /><br /><br /><a href="#null" style="width:100px" class="btn" onclick="document.location=\''.$this->nombreFilePhpBase.$enEdicion.'.php?id='.$this->id.'\';">volver a editar</a></center>';
						exit;;
					}
					$isError=1;
				}

				$datosImagen=@getimagesize($_FILES['img_fondo_pagina']['tmp_name']);

				if($datosImagen[0] > $this->anchoMaximoImagenes)	// SE HA SUPERADO EN ANCHURA EL MAXIMO CONSENTIDO
				{
					$datosImagenAncho=number_format(($datosImagen[0]),0,',','.').'px';
					$this->mensajeSalida='ERROR: la im&aacute;gen es <u>demasiado ancha</u>: '.$datosImagenAncho.' (MAX: '.($this->anchoMaximoImagenes).'px)';

					if(!$_REQUEST['id'])	// ES UN RECORD NUEVO
					{
						echo '<br /><br /><center><p ><h4 style="color:red">'.$this->mensajeSalida.'</h4></p><br /><br /><br /><a href="#null" style="width:100px" class="btn" onclick="document.location=\''.$this->nombreFilePhpBase.$enEdicion.'.php?id='.$this->id.'\';">volver</a></center>';
						$isError=1;
					}
					else	// ES UN RECORD EDITADO
					{
						echo '<br /><br /><center><p ><h4 style="color:red">'.$this->mensajeSalida.'</h4></p><br /><br /><br /><a href="#null" style="width:100px" class="btn" onclick="document.location=\''.$this->nombreFilePhpBase.$enEdicion.'.php?id='.$this->id.'\';">volver a editar</a></center>';
						$isError=1;
					}
					return;
				}

				if(is_uploaded_file($_FILES['img_fondo_pagina']['tmp_name']))
				{
					$tmp_file='imx'.date('dmyis',time()).$_FILES['img_fondo_pagina']['name'];
					copy($_FILES['img_fondo_pagina']['tmp_name'], $this->path_imagenes.$tmp_file);
					$_img=',"'.$tmp_file.'" ';
					$dataContent=$tmp_file;
				}
				$campoImg=', img_fondo_pagina';
			}

		}

		if($isError)
		{
				echo '<br /><br /><center><p ><h4 style="color:red">'.$this->mensajeSalida.'</h4></p><br /><br /><br /><a href="#null" style="width:100px" class="btn" onclick="document.location=\''.$this->nombreFilePhpBase.$enEdicion.'.php?id='.$this->id.'\';">volver a editar</a></center>';
				exit;
		}

		if($this->quitar_archivo)	// SI SE HA SELECCIONADO QUITAR EL ARCHIVO, SE PROCEDE A BORRARLO DE LA TABLA
		{
			$_img=',""'; $campoImg=', img_fondo_pagina';
			$modif_imagenCarrousel=1; $this->img_fondo_pagina='';
			$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla." WHERE id=".$_REQUEST['id'];	// SE OBTIENE LA archivo A BORRAR
			$res=$this->sql->query($query);
			$arra=$this->sql->fila2($res);
			$UltimoId=$arra['id'];
			$archivoBorrar=trim($arra['img_fondo_pagina']);

			if($this->path_imagenes.$archivoBorrar)	// SI EXISTE EL ARCHIVO, SE BORRA FISICAMENTE
			{
				@unlink($this->path_imagenes.$archivoBorrar);
				$query='UPDATE '.$this->sql->db.'.'.$this->nombreTabla.' SET img_fondo_pagina="" WHERE id='.$UltimoId;
			}
		}

		if(!$tmp_file)
		{
			$this->img_fondo_pagina=str_replace('"','\'',$this->img_fondo_pagina);
			$dataContent=$this->img_fondo_pagina;
		}

		if($this->id_niv_two<0) $this->id_niv_two=0;
		$fechaContenido=$_REQUEST['fechaAnio'].'-'.$_REQUEST['fechaMes'].'-'.$_REQUEST['fechaDia'];
		$this->extra_content=str_replace('"','\'',$this->extra_content);
		$this->url_link=str_replace('"','\'',$this->url_link);
		$this->tooltip=str_replace('"','\'',$this->tooltip);
		if(!$this->id) $this->id=0; // NECESARIO PARA AÑADIR NUEVOS REGISTROS

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id, id_niv_one, id_niv_two, txt_link, extra_content, activo, id_usuario, tipo, url_link, tooltip, css_class,orden,fecha, group_ids '.$campoImg.') VALUES ("'.$this->id.'","'.$this->id_niv_one.'", "'.$this->id_niv_two.'", "'.utf8_decode($this->txt_link).'", "'.str_replace("\r\n",'',utf8_decode($this->extra_content)).'", "'.$this->activo.'", "'.$this->id_usuario.'", "'.$this->tipo.'", "'.str_replace("\r\n",'',utf8_decode($this->url_link)).'", "'.str_replace("\r\n",'',utf8_decode($this->tooltip)).'", "'.(int)$this->css_class.'", "'.(int)$this->orden.'","'.$fechaContenido.'", "'.$groupsIds.'" '.$_img.')';

		if(!$_img && $this->id)
		$query='UPDATE '.$this->sql->db.'.'.$this->nombreTabla.' SET id_niv_one="'.$this->id_niv_one.'", id_niv_two="'.$this->id_niv_two.'", txt_link="'.utf8_decode($this->txt_link).'", extra_content="'.str_replace("\r\n",'',utf8_decode($this->extra_content)).'", activo="'.$this->activo.'", id_usuario="'.(int)$this->id_usuario.'", tipo="'.$this->tipo.'", url_link="'.str_replace("\r\n",'',$this->url_link).'", tooltip="'.str_replace("\r\n",'',utf8_decode($this->tooltip)).'", css_class="'.(int)$this->css_class.'", group_ids="'.$groupsIds.'", orden="'.(int)$this->orden.'", fecha="'.$fechaContenido.'" WHERE id='.$this->id;

//echo $query.'<br />[[['.$_img;echo ']]]]<pre>';print_r($this);die();
		$this->sql->query($query);
		return;
	}

//******************************************************
//	FORMULARIO DE EDICION /INSERCION DE DATOS
//******************************************************
	function formulario_datos($nombreForm=0)
	{
//		$this->PhpEditarHTML($this->nombreTabla,$nombreForm,$this->id);
		$dataPopline=str_replace("\r\n",'',($this->extra_content));
		$dataPopline=html_entity_decode($dataPopline);
		$dataPopline=htmlspecialchars_decode($dataPopline);
		$dataPopline=utf8_encode($dataPopline);

		$dataPopline1=str_replace("\r\n",'',($this->tooltip));
//		$dataPopline1=html_entity_decode($dataPopline1);
		$dataPopline1=($dataPopline1);
//		$this->tooltip=utf8_decode($this->tooltip);
?>
<tr height="<?=$this->id?'':''?>">
	<td align="right" title="Solapa de nivel principal">Solapa princ.:</td>
	<td align="left" colspan="3" width="300">
<?php
		if($this->levelAccessMaxLevel != $_SESSION['usuarioNivel'] && !$this->id)	// FILTRO PARA TODOS LOS USUARIOS CON NIVEL INFERIOR A GOOD
		  $filterSql='&& (mlo.id_usuario='.(int)$_SESSION['id_usuario'].' || mlo.id_usuario=0)';

		$query='SELECT mlo.id,mlo.etiqueta, mlo.scope_element FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_one AS mlo WHERE 1 '.$filterSql.' ORDER BY mlo.etiqueta';
		$res=$this->sql->query($query);

		echo '<select id=\'id_niv_one\' name=\'id_niv_one\' style=\'width:100%\' onchange=\'actualizarSubPagina(this)\'><option value="-1" class="tx10">seleccionar</option>';
		while($arra=$this->sql->fila2($res))
		{
			if($this->levelAccessMaxLevel != $_SESSION['usuarioNivel'] && $this->id && $this->id_niv_one != $arra['id'])
				continue;

			$targetMenuOne=strtoupper(substr($this->arrTargets[$arra['scope_element']],0,3));
			echo '<option value=\''.$arra['id'].'\' ';
			if($this->id_niv_one==$arra['id'])
				echo 'selected=\'selected\'';
			echo '>'.$targetMenuOne.': '.$arra['etiqueta'].'</option>';
		}
		echo '</select>';
?>
	</td>
	<td align='right' title="Posici&oacute;n en el segundo nivel (vertical, ya en el box relativo a la solapa de primer nivel seleccionada)">Men&uacute; niv. 2:</td>
	<td align='left' width="200">
<?php
		if(isset($this->id_niv_two))
		{
			$salida='';
			$query='SELECT * FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_two WHERE id_level_one='.$this->id_niv_one;
			$res2=$this->sql->query($query);

			while($arra=$this->sql->fila2($res2))
			{
				$salida.='<option value=\''.$arra['id'].'\'';
				if($this->id_niv_two == $arra['id']) $salida.=' selected="selected"';
				$salida.='>'.($arra['etiqueta']).'</option>';
			}
		}
		echo '<select name="id_niv_two" id="id_niv_two" style=\'width:100%\'>';
		echo '<option value=\'0\' style="color:#888">No tiene</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
	<td title="Texto del link a mostrar" align="right">Texto link:</td>
	<td colspan="4"><input id="txt_link" name="txt_link" type="text" value="<?=utf8_encode($this->txt_link)?>" style="width:100%" /></td>
</tr>
<tr height="">
	<td width="74" align="right" title="NOTA para las modales internas:<br>1.- se debe escribir en este campo como identificativo &uacute;nico el `abridor`, iniciando con <b><i>#_father_</i></b>.<br>2.- se le debe asignar obligatoriamente la clase CSS `<b><i>lightboxTrigger</i></b>`.<br>3.- A cada elemento de la modal, como inicio del link deber&aacute; incluir `<b><i>#_inBox_</i></b>` + `<b>referencia Del Padre</b>` + `<b>_</b>` (un gui&oacute;n bajo, obligatorio) para que este elemento se incluya autom&aacute;ticamente en el recuadro esperado.<br />Ej.: padre = `#_father_MiRefPadre`<br />Hijo = `#_inBox_MiRefPadre_http:miweb.com`">&nbsp;<a href="<?=((substr($this->url_link,0,5) == 'http:') ? $this->url_link : '../'.$this->url_link)?>" class="btn" target="_blank">Url link:</a> </td>
	<td width="150">
		<input id="url_link" name="url_link" type="text" value="<?=$this->url_link?>" style="width:100%;" />
	</td>
  <td width="30" align="right" title="Posici&oacute;n del elemento respecto a los dem&aacute;s">&nbsp;Orden:</td>
	<td width="30"><input id="orden" name="orden" type="text" value="<?=$this->orden?>" style="text-align:right;width:100%;" /></td>

	<td align="right" title="Usuario propietario de este determinado elemento o accesible a todos">Usuario:</td>
	<td>
<?php
		$salida='';
		$query='SELECT id, CONCAT(nombre," ",apellidos) AS theName,nivel_acceso FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE activo=1 ORDER BY nombre';
		$res2=$this->sql->query($query);
		$salida.='<option value="0"';

		if(isset($this->id_usuario) && $this->id_usuario == 0) $salida.=' selected="selected" ';
		$salida.='>Global</option>';

		while($arra=$this->sql->fila2($res2))
		{
			//SE OMITEN OTROS USUARIOS CON NIVEL SUPERIOR DE ACCESO AL DEL USUARIO ACTUAL
			if($_SESSION['usuarioNivel'] < $arra['nivel_acceso'])	continue;

			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->id_usuario == $arra['id']) $salida.=' selected="selected" ';
			$salida.='>'.utf8_encode($arra['theName']).'</option>';
		}

		echo '<select id="id_usuario" name="id_usuario" style=\'width:100%\' ><option value="-1" class="tx10" style="color:#666">?</option>';
		echo $salida;
		echo '</select>';
?>
	</td>
	<td title='Target del link' align="right">Target:</td>
	<td align='left'>
<?php
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'contenidos_tipos WHERE activo ORDER BY texto_tipo';
		$res=$this->sql->query($query);

		echo '<select id=\'tipo\' name=\'tipo\' style=\'width1:100px\'><option value="-1" class="tx10">?</option>';
		while($arra=$this->sql->fila2($res))
		{
			echo '<option value=\''.$arra['id'].'\' ';
			if($this->tipo==$arra['id'])
				echo 'selected=\'selected\'';
			echo '>'.$arra['texto_tipo'].'</option>';
		}
		echo '</select>';
?>
	</td>
	<td title='Indica si el elemento est&aacute; habilitado' align="right">Activo:</td>
	<td>
<?php
		$arraEsActivo=array(0=>'No',1=>'S&iacute;');
		echo '<select id="activo" name="activo" style=\'width:100%;\'>';
		echo '<option value=-1 ';
		if($this->activo==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->activo==$n)
				echo 'selected=\'selected\'';
			echo ' >'.$arraEsActivo[$n].'</option>';
		}
		echo '</select>';
?>
	</td>
</tr>
<tr>
	<td align="right">Fecha:</td>
	<td colspan="3">
<?php
		$fechaMostrar=$this->fecha;
		if(!$fechaMostrar) $fechaMostrar=date('Y-m-d');
		$this->combos_fecha('fechaDia','fechaMes','fechaAnio',date('Y-m-d',strtotime($fechaMostrar)),'2000#2021'); //
?>
	</td>
	<td align="right" title="Editor HTML para los contenidos extra"><a class='btn ajax editAjax' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?><?='&innerTextHeight='.$this->innerTextHeight?>&nombreCampo=data_extra_content" title="">HTML extra:</a></td>
	<td>
		<div id="data_extra_content" class='editorPopline' contenteditable='true' style="display:block;margin-top:10px;border:1px solid #999;width:<?=$this->id ? '250px' : '200px';?>;height:<?=$this->id ? '50px' : '30px';?>;overflow:auto;"><?=$dataPopline?></div>
		<textarea id='extra_content' name='extra_content' style='display:none;' ><?=$dataPopline?></textarea>
	</td>

	<td align="right" title="Editor HTML para el texto del tooltip"><a class='btn ajax editAjax' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?><?='&innerTextHeight='.$this->innerTextHeight?>&nombreCampo=data_tooltip">HTML tooltip:</a></td>
    <td colspan="4">
		<div id="data_tooltip" class='editorPopline' contenteditable='true' style="display:block;margin-top:10px;border:1px solid #999;width:<?=$this->id ? '250px' : '200px';?>;height:<?=$this->id ? '50px' : '30px';?>;overflow:auto;"><?=$dataPopline1?></div>
		<textarea id='tooltip' name='tooltip' style='display:none;' ><?=$dataPopline1?></textarea>
			<!--<textarea id='tooltip' name='tooltip' rows="1" style="width:100%;"><?=str_replace("\r\n",'',utf8_encode($this->tooltip))?></textarea>-->
		</td>
</tr>
<tr>
	<td align="right" title="Clase CSS a utilizar en el botón del link, necesario para las ventanas modales si utilizadas">CSS:</td>
	<td>
<?php
		$query='SELECT id,css_class FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'css_classes WHERE active ORDER BY css_class';
		$res=$this->sql->query($query);

		echo '<select id=\'css_class\' name=\'css_class\' style=\'width1:100px\'><option value="-1" class="tx10">?</option>';
		while($arra=$this->sql->fila2($res))
		{
			echo '<option value=\''.$arra['id'].'\' ';
			if($this->css_class == $arra['id'])
				echo 'selected=\'selected\'';
			echo '>'.$arra['css_class'].'</option>';
		}
		echo '</select>';
?>
	</td><td>
	</td><td>
	</td><td>
	</td><td>
	</td>
	<td align="right" title="Grupos a los que puede pertenecer">Grupos:</td>
	<td rowspan="2" colspan="3" valign="top">
<?php
		if($_SESSION['usuarioNivel'] >= $this->levelAccessModules['SuperAdmin'])
		{
		  $arrUsers=explode(',',$this->group_ids);
		  $salida='';
		  $query='SELECT id, group_name FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'user_groups WHERE is_enabled=1 ORDER BY group_name';
		  $res2=$this->sql->query($query);

		  while($arra=$this->sql->fila2($res2))
		  {
			  $salida.='<option value=\''.$arra['id'].'\'';
			  if(in_array($arra['id'],$arrUsers)) $salida.=' selected="selected" ';
			  $salida.='>'.($arra['group_name']).'</option>';
		  }

		  echo '<select id="group_ids" name="group_ids[]" style="width:100%;height:40px;" multiple="multiple" size=2 >';
		  echo $salida;
		  echo '</select>';
		}
		else
		  echo'<span style="display:block;padding-top:3px;color:'.$_SESSION['colorErrors'].'">n./d.</span>';
?>
	</td>
</tr>

<?php
	}

//******************************************************
//	CONTROLES ESPECIFICOS PARA EL MODULO + CARGA DEL FORMULARIO
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
var txPagina = new Array();
var idPagina = new Array();
//var tipoRespuestas=0;
//var esArchivo=0;

$(document).ready(function(){
	//editor jQuery color box
	$(".editAjax").colorbox({
		opacity: 0.4,
		initialHeight:"10px",
		initialWidth:"10%",
		close:true,
		overlayClose:true,
		width:"<?=$this->id ? 74 : 74 ?>%",
		height:"100%",
		iframe:true,
		html:true,
		innerHeight:true
	});

	$('#orden').mask('099');
});

function removeOptionSelected(nombreSelect)	// ELIMINA TODOS LOS ELEMENTOS DEL SELECT 'nombreSelect'
{
	txPagina = new Array();
	idPagina = new Array();
	var elSel = document.getElementById(nombreSelect);
	for(i=elSel.length;i>=0;i--)	// CAMBIAR 'i>0' A 'i>=0' SI HAY QUE ELIMINAR TODAS LAS OPCIONES DEL SELECT O 'i>=0' SI HAY QUE QUITAR TAMBIÉN 'seleccionar'
		elSel.remove(i);
	return;
}

function actualizarSubPagina(elemento)	// ACTUALIZAR EL SELECT DE LA 'SECCION' DESDE EL SELECT DEL 'AREA'
{
	if(document.f<?=$idunico?>.id_niv_one.value>-1 && elemento.name=='id_niv_one')
		document.f<?=$idunico?>.id_niv_two.options[0].selected=true;

	if(document.f<?=$idunico?>.id_niv_one.value==-1 && elemento.name=='id_niv_one')
	{
		document.f<?=$idunico?>.id_niv_one.style.backgroundColor='red';
		alert('Atencion, esta seleccion no es valida!');
		document.f<?=$idunico?>.id_niv_one.focus();
		document.f<?=$idunico?>.id_niv_one.style.backgroundColor='';
		return;
	}

	if(document.f<?=$idunico?>.tipo.value==-1 && elemento.name=='tipo')
	{
		document.f<?=$idunico?>.tipo.style.backgroundColor='red';
		alert('Atencion, esta seleccion no es valida!');
		document.f<?=$idunico?>.tipo.focus();
		document.f<?=$idunico?>.tipo.style.backgroundColor='';
		return;
	}

	document.f<?=$idunico?>.ref_desplegable.value=elemento.name;
	tmpAction=document.f<?=$idunico?>.action;
	document.f<?=$idunico?>.target="ifrAopyoPreg";

	document.f<?=$idunico?>.target='ifrAopyoPreg';
	document.f<?=$idunico?>.action='edicion_contenidos_publicos_iframe.php';
	document.f<?=$idunico?>.submit();

	document.f<?=$idunico?>.action=tmpAction;
	document.f<?=$idunico?>.target='_self';
	return;
}

function revisa()
{
	if($('#id_niv_one').val() < 0)
	{
		resaltarCampo($("#id_niv_one")[0]);
		$("#id_niv_one").focus();
		alert('Por favor, debe seleccionar una solapa de nivel UNO');
		restauraCampo($("#id_niv_one")[0]);
		return false;
	}
	if($('#id_niv_two').val() < 1)
	{
		resaltarCampo($("#id_niv_two")[0]);
		$("#id_niv_two").focus();
		alert('Por favor, debe seleccionar un elemento de nivel DOS');
		restauraCampo($("#id_niv_two")[0]);
		return;
	}
	if(!trim($('#txt_link').val()))
	{
		resaltarCampo($("#txt_link")[0]);
		alert('Por favor, escriba el texto del link');
		document.f<?=$idunico?>.txt_link.focus();
		restauraCampo($("#txt_link")[0]);
		return;
	}
	if(!trim($('#url_link').val()))
	{
		resaltarCampo($("#url_link")[0]);
		alert('Por favor, escriba la URL del link');
		document.f<?=$idunico?>.url_link.focus();
		restauraCampo($("#url_link")[0]);
		return;
	}
	if($('#id_usuario').val() < 0)
	{
		resaltarCampo($("#id_usuario")[0]);
		$("#id_usuario").focus();
		alert('Por favor, indique un usuario');
		restauraCampo($("#id_usuario")[0]);
		return;
	}
	if(document.f<?=$idunico?>.tipo.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.tipo);
		alert('Por favor, seleccione el tipo de target');
		document.f<?=$idunico?>.tipo.focus();
		restauraCampo(document.f<?=$idunico?>.tipo);
		return;
	}
	if(document.f<?=$idunico?>.activo.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.activo);
		alert('Por favor, indique si ha de estar activo');
		document.f<?=$idunico?>.activo.focus();
		restauraCampo(document.f<?=$idunico?>.activo);
		return;
	}
	popeditorSaveData("f<?=$idunico?>");
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}

function appendOptionLast(id,texto,nombreSelect)	// AÑADE ELEMENTOS (OPTIONS) A LA SELECT 'nombreSelect'
{//alert(texto);return;
  var elOptNew = document.createElement('option');
  elOptNew.text = texto;
  elOptNew.value = id;
  var elSel = document.getElementById(nombreSelect);
  try	//intenta la instruccion siguiente, si lo consigue, ejecuta
	{
    elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
  }
  catch(ex)	// si falla la anterior instruccion, se ejecutara la siguiente
	{
    elSel.add(elOptNew); // IE only
  }
}

-->
</script>
<iframe name='ifrAopyoPreg' id='ifrAopyoPreg' style='display:none;width:100%' src=''></iframe>
<?php
		$camposAdicionales='<input type="hidden" id="ref_desplegable" name="ref_desplegable" value="">
';
		include('_inc/form_std_edit.php');
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->quitar_archivo=$datos['quitar_archivo'];
		$this->id=$datos['id'];
		$this->id_niv_one=$datos['id_niv_one'];
		$this->id_niv_two=$datos['id_niv_two'];
		$this->txt_link=$datos['txt_link'];
		$this->tipo=$datos['tipo'];
		$this->url_link=$datos['url_link'];
		$this->tooltip=$datos['tooltip'];
		$this->id_usuario=$datos['id_usuario'];
		$this->activo=$datos['activo'];
		$this->extra_content=$datos['extra_content'];
		$this->fecha=$datos['fecha'];
		$this->img_fondo_pagina=$datos['img_fondo_pagina'];
		$this->css_class=$datos['css_class'];
		$this->orden=$datos['orden'];
		$this->group_ids=$datos['group_ids'];
		$this->guarda_datos();
	}

//******************************************************
//	ELIMINACION DE REGISTROS CON CONTROL DE DATOS RELACIONADOS DE OTRAS TABLAS AL MISMO
//******************************************************
	function eliminar_registro()
	{
		$query1='SELECT COUNT(id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE (activo) && id='.$_REQUEST['id'];
		$existen_paginas=$this->sql->valor($query1);
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$_REQUEST['id'];
		if(!$existen_paginas && $_REQUEST['id'])	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
//		$idunico=rand(1,10000).time();
		$mens.='</b>';
		if (count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b&uacute;squeda:</span> ";

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID
		$_SESSION['filtros']['offset_u']*=1;
/*
		if($_SESSION['forcedLangSite'])	// SI SE FUERZA EL SITE A UN IDIOMA, SE CARGAN SOLAMENTE LAS ETIQUETAS DEL QUE HAYA ACTIVO
		{
			$filtroSql .= $_SESSION['id_idioma'] ? ' && id_idioma='.$_SESSION['id_idioma'] : '';
		}
*/
		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='orden,fecha'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $query;die();
//echo '<pre>';print_r($_SESSION);
//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<form action='<?=$destinobusca?>.php' method='post' name='formListados'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->

<table align='center' cellpadding='3' cellspacing='0' class='borde' width='990'>
<thead>
<tr>
	<td class='enc2' colspan='4'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='12' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
</tr>
</thead>
<tr>
<?php
		echo '<td width="40" title="Buscar por usuarios">Usuario:</td>
			<td>';
		$salida='';
		$query='SELECT id, CONCAT(nombre," ",apellidos) AS theName, nivel_acceso FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE activo=1 ORDER BY nombre';
		$res2=$this->sql->query($query);
		$salida.='<option value="0"';

		if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN'] == 0) $salida.=' selected="selected" ';
		$salida.='>Global</option>';

		while($arra=$this->sql->fila2($res2))
		{
			//SE OMITEN OTROS USUARIOS CON NIVEL SUPERIOR DE ACCESO AL DEL USUARIO ACTUAL
			if($_SESSION['usuarioNivel'] < $arra['nivel_acceso'])	continue;

			$salida.='<option value=\''.$arra['id'].'\'';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN'] == $arra['id'])
				$salida.=' selected="selected"';
			$salida.='>'.utf8_encode($arra['theName']).'</option>';
		}

		echo '<select id="'.$this->posicionSolapa.'id_usuario_buscN" name="'.$this->posicionSolapa.'id_usuario_buscN" style=\'width:90px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select></td>';
?>
	<td align="right" title="Pesta&ntilde;a o nivel 1">Niv.1:</td>
	<td>
<?php
		if($this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])	// FILTRO PARA TODOS LOS USUARIOS CON NIVEL INFERIOR A GOOD
		  $filterSql='&& (mlo.id_usuario='.(int)$_SESSION['id_usuario'].' || mlo.id_usuario=0)';

		$query='SELECT mlo.* FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_one AS mlo WHERE 1 '.$filterSql.' ORDER BY mlo.etiqueta';
		if($_SESSION['forcedLangSite'])	// SI SE FUERZA EL SITE A UN IDIOMA, SE CARGAN SOLAMENTE LAS ETIQUETAS DEL QUE HAYA ACTIVO
		{
			$query='SELECT mlo.id,mlo.etiqueta, mlo.scope_element FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_one AS mlo WHERE 1 '.$filterSql.' ORDER BY mlo.etiqueta';
		}
		$res2=$this->sql->query($query);

		echo '<select id=\''.$this->posicionSolapa.'id_niv_one_buscN\' name=\''.$this->posicionSolapa.'id_niv_one_buscN\' style=\'width:100px\' ><option value=\'-1\'>?</option>';
		while($arra2=$this->sql->fila2($res2))
		{
			echo '<option value=\''.$arra2['id'].'\' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_niv_one_buscN']==$arra2['id'])
				echo 'selected=\'selected\'';
			echo '>'.strtoupper(substr($this->arrTargets[$arra2['scope_element']],0,3)).': '.$arra2['etiqueta'].'</option>';
		}
		echo '</select>';
?>
	</td>
	<td width="60" align="right" title="Apartado de nivel 2">Niv. 2:</td>
	<td>
<?php
		$filtro2=$_SESSION['filtros'][$this->posicionSolapa.'id_niv_one_buscN']?' WHERE id_level_one='.$_SESSION['filtros'][$this->posicionSolapa.'id_niv_one_buscN']:'';
		$query='SELECT mlt.* FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_two AS mlt '.$filtro2.' ORDER BY mlt.etiqueta';
		$res2=$this->sql->query($query);

		echo '<select id=\''.$this->posicionSolapa.'id_niv_two_buscN\' name=\''.$this->posicionSolapa.'id_niv_two_buscN\' style=\'width:100px\'><option value=\'-1\'>?</option>';
		while($arra2=$this->sql->fila2($res2))
		{
			echo '<option value=\''.$arra2['id'].'\' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_niv_two_buscN']==$arra2['id'])
				echo 'selected=\'selected\'';
			echo '>'.$arra2['etiqueta'].'</option>';
		}
		echo '</select>';
?>
	</td>
	<td width="40" align="right">URL:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>url_link_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'url_link_busca_u']?>' style='width:70px' /></td>
	<td width="68" align="right">Texto link:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>txt_link_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'txt_link_busca_u']?>' style='width:70px' /></td>

	<td width="30">Activo:</td>
	<td>
<?php
		$arraEsActivoBusc=array(0=>'No',1=>'S&iacute;');
		echo '<select name="'.$this->posicionSolapa.'activo_buscN" style=\'width:42px;\' class="tx10">';
		echo '<option value="-1" >?</option>';
		foreach($arraEsActivoBusc as $key=>$value)
		{
			echo '<option value='.$key.' ';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'activo_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'activo_buscN']==$key)
				echo 'selected=\'selected\'';
			echo ' >'.($value).'</option>';
		}
		echo '</select>';
?>
  </td>

	<td width="20">Tipo:</td>
	<td>
<?php
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'contenidos_tipos ORDER BY texto_tipo';
		$res2=$this->sql->query($query);

		echo '<select id=\'tipo\' name="'.$this->posicionSolapa.'tipo_buscN" style=\'width1:80px\' class="tx10"><option value="-1">?</option>';
		while($arra2=$this->sql->fila2($res2))
		{
			echo '<option value=\''.$arra2['id'].'\' ';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'tipo_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'tipo_buscN']==$arra2['id'])
				echo 'selected=\'selected\'';
			echo '>'.($arra2['texto_tipo']).'</option>';
		}
		echo '</select>';
?>
  </td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,$this->modalHeight,'',$this->modalWidth,$this->modalLeftPos);

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='URL link';
		$arraTitulares[]='Texto link';
		$arraTitulares[]='Solapa principal';
		$arraTitulares[]='Del elemento';
		$arraTitulares[]='Orden';
		$arraTitulares[]='Usuario';
		$arraTitulares[]='Activo';
		$arraTitulares[]='Target';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='url_link';
		$arraCamposOrdenar[]='txt_link';
		$arraCamposOrdenar[]='id_niv_one';
		$arraCamposOrdenar[]='id_niv_two';
		$arraCamposOrdenar[]='orden';
		$arraCamposOrdenar[]='id_usuario';
		$arraCamposOrdenar[]='activo';
		$arraCamposOrdenar[]='tipo';

		 // no se incluye el valor para el ID
		$arraLongitudes[]=24;
		$arraLongitudes[]=20;
		$arraLongitudes[]=64;
		$arraLongitudes[]=36;
		$arraLongitudes[]=36;
		$arraLongitudes[]=140;
		$arraLongitudes[]=50;
		$arraLongitudes[]=40;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=30;	// id
		$arraLongitudesTitulares[]=150;
		$arraLongitudesTitulares[]=100;
		$arraLongitudesTitulares[]=160;
		$arraLongitudesTitulares[]=160;
		$arraLongitudesTitulares[]=40;
		$arraLongitudesTitulares[]=120;
		$arraLongitudesTitulares[]=40;
		$arraLongitudesTitulares[]=40;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		$arraTipos[]='url';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='rop';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;
		$conutRowsDisplayed=0;
		$selectLabelLevelOne=$_SESSION['forcedLangSite'] ? 'mlo.etiqueta AS nameMenu' : 'CONCAT(mlo.etiqueta," (",i.prefijo,") AS nameMenu")';

		while($a=$this->sql->fila2($res))
		{
		  $rop=null;
			//	SOLO SE PERMITEN VER LOS ELEMENTOS PROPIOS Y LOS GLOBALES (EXCEPTO PARA GOOD)
		  if($_SESSION['id_usuario'] != $a['id_usuario'] && $a['id_usuario'] != 0 && $this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])
				$rop=1; // fila deshabilitada

		  $esCorecta='&nbsp;';
		  $esActivo=$a['activo']?'SI':'<span style=color:'.$_SESSION['colorTextWarning'].'><b>NO</b></style>';

		  $query='SELECT id,texto_tipo FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'contenidos_tipos WHERE id='.$a['tipo'];
		  $res3=$this->sql->query($query);
		  $b=$this->sql->fila2($res3);

		  $query='SELECT '.$selectLabelLevelOne.', mlo.scope_element FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_one AS mlo WHERE mlo.id=\''.$a['id_niv_one'].'\'';
		  $resTmp=$this->sql->query($query);
		  $arrLevelRoot=$this->sql->fila2($resTmp);
		  $nombreEtiquetaMenuOne=ucwords($arrLevelRoot['nameMenu']);
		  $targetMenuOne=strtoupper(substr($this->arrTargets[$arrLevelRoot['scope_element']],0,3));
		  $nombrePagina=$nombreEtiquetaMenuOne ? '<span style=color:blue;>'.$targetMenuOne.': </span>'.$nombreEtiquetaMenuOne : '<center><b>?</b></center>';

		  $query='SELECT mlo.etiqueta FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_two AS mlo WHERE mlo.id=\''.$a['id_niv_two'].'\'';

		  $nombreSubPagina=$this->sql->valor($query);
		  $nombreSubPagina=$nombreSubPagina;
		  if(!$nombreSubPagina) $nombreSubPagina='<i style=color:'.$_SESSION['colorTextLoose'].'>ninguna</i>';

		  $url_link=strip_tags($a['url_link']);
		  if(strlen($url_link)>55) $url_link=substr($url_link,0,50).' [...]';
		  if($url_link != $a['url_link']) $url_link=$url_link.' **';	// SE AÑADEN ASTERISCOS SI EL TITULAR TIENE ALGUN FORMATO ADICIONAL
		  if(!trim($url_link)) $url_link='<i style=color:#999>no tiene</i>';

		  $txt_link=strip_tags($a['txt_link']);
		  if(strlen($txt_link)>55) $txt_link=substr($txt_link,0,50).' [...]';
		  if($txt_link!=$a['txt_link']) $txt_link=$txt_link.' **';	// SE AÑADEN ASTERISCOS SI EL TITULAR TIENE ALGUN FORMATO ADICIONAL
		  if(!trim($txt_link)) $txt_link='<i style=color:#999>no tiene</i>';

		  $query='SELECT CONCAT(nombre," ",apellidos) AS theName FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE id="'.$a['id_usuario'].'"';
		  $elUsuario=($this->sql->valor($query));
		  $elUsuario=$elUsuario ? utf8_encode($elUsuario) : '<span style=color:red;><b>Todos</b></span>';

		  $query='SELECT texto_tipo FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'contenidos_tipos WHERE id="'.$a['tipo'].'"';
		  $theTarget=($this->sql->valor($query));
		  $theTarget=$theTarget?$theTarget:'<span style=color:red;><b>Todos</b></span>';

		  //$fecha=date('d-m-Y',strtotime($a['fecha']));
//echo'>'.$tmpArr,$a['orden'];
		  $tmpArr[0]=$a['id'];
		  array_push($tmpArr,$url_link);
		  array_push($tmpArr,utf8_encode($txt_link));
		  array_push($tmpArr,$nombrePagina);
		  array_push($tmpArr,$nombreSubPagina);
		  array_push($tmpArr,$a['orden']);
		  array_push($tmpArr,$elUsuario);
		  array_push($tmpArr,$esActivo);
		  array_push($tmpArr,$theTarget);
		  array_push($tmpArr,$rop);
		  $arraDatos[]=$tmpArr;
		  unset($tmpArr);
		  $conutRowsDisplayed++;
		}

		$idRefPops=0;
		$txtRefEliminar=1;
		$posicFondo=0;	// NUMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
