<?php
class MenuIdiomas extends Base
{
	//Datos propios de la clase
	var $id;
	var $linkmenu;

//******************************************************
//	INICIALIZACI�N DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla='idiomas_menu';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las b�squedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset']==1) unset($_SESSION['filtros']);
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$this->carga_datos();
	}

//******************************************************
//
//******************************************************
	function carga_datos()
	{
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE ID="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->texto_alternativo=$this->output_datos($a['texto_alternativo']);
		$this->ref_link=$a['ref_link'];
		$this->id_idioma=$a['id_idioma'];
		$this->texto_idioma=$this->output_datos($a['texto_idioma']);
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		if(!$this->id)
			$this->id=0;

		$query='SELECT texto_idioma FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE ref_link="'.$this->ref_link.'" AND id_idioma='.$this->id_idioma;
		$existeRegistro=$this->sql->valor($query);

		if($existeRegistro && !$this->id)
		{
			echo '<table height="100%" width="100%" border="0" cellspacing="0" cellpadding="0" class="textNormal">
  <tr align="center" height="20%">
    <td style="color:yellow"><b>NO SE PERMITE CONTINUAR CON LA CREACI�N/MODIFICACI�N DE ESTE REGISTRO</b></td>
  </tr>
  <tr align="center" height="40%">
    <td>La entrada de men� para el idioma seleccionado ya existe actualmente con el siguiente texto: "<b><span style="color:orange">'.$existeRegistro.'</span></b>"</td>
  </tr>
  <tr align="center" height="20%">
    <td><b>NOTA:</b> No se permiten duplicados.</td>
  </tr>
  <tr align="center" height="40%">
    <td><a href="#null" class="btn" style="line-height:30px" onclick="volverFormulario();">&nbsp;&nbsp;Volver&nbsp;&nbsp;</a></td>
  </tr>
</table>
<script>
function volverFormulario()
{
';

		if($this->id) echo 'window.history.back();';
		else echo 'document.location="menu_idiomas.php'.$cadena.'";';
	echo '
}
</script>
';
			$this->mensajeSalida='<span class=enc2 style=background-color:red;color:#fff>&nbsp;ERROR GRAVE&nbsp;</span><br />NO EXISTE � NO SE PUEDE ESCRIBIR EN LA CARPETA DE <br />'.$this->path_imagenes;
//			echo $this->mensajeSalida;
//echo'<pre>';print_r($_SERVER);print_r($this);
			exit;
		}

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id,ref_link,texto_idioma,texto_alternativo,id_idioma) VALUES ("'.$this->id.'", "'.$this->ref_link.'", "'.$this->texto_idioma.'", "'.$this->texto_alternativo.'","'.$this->id_idioma.'")';

//echo $query;echo'<pre>';print_r($_REQUEST);print_r($this);exit;

		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
/*
		$query='SELECT COUNT(id) FROM '.$this->sql->db.'.menu_vert_nivel2 WHERE id_nivel1="'.$this->id.'"';
echo $query;echo'<pre>';print_r($_REQUEST);print_r($this);exit;
		$existen_paginas=$this->sql->valor($query);
*/
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function formulario_datos()
	{
?>
<tr>
	<td align="right">Referencia URL:</td>
	<td>
<?php
		// START SE CONTRUYE EL ARRAY CON TODOS LOS ELEMENTOS DE MEN�S 
		$salida='';
		$query='SELECT DISTINCT(link),id FROM '.$this->sql->db.'.menu_vert_nivel1 ';
		$res2=$this->sql->query($query);

		while($arra2=$this->sql->fila2($res2))
			$arraMenus['link'][]=$arra2['link'];
/*
		$query='SELECT DISTINCT(link),id FROM '.$this->sql->db.'.menu_vert_nivel2 ';
		$res2=$this->sql->query($query);

		while($arra2=$this->sql->fila2($res2))
			$arraMenus['link'][]=$arra2['link'];

		$query='SELECT DISTINCT(link),id FROM '.$this->sql->db.'.menu_vert_nivel3 ';
		$res2=$this->sql->query($query);

		while($arra2=$this->sql->fila2($res2))
			$arraMenus['link'][]=$arra2['link'];
*/
		@asort($arraMenus['link']);
		// END SE CONTRUYE EL ARRAY CON TODOS LOS ELEMENTOS DE MEN�S 

		if(count($arraMenus['link']))
			foreach($arraMenus['link'] as $key=>$value)
			{
				$salida.='<option value=\''.$value.'\'';
				if($this->ref_link==$value) $salida.=' selected="selected"';
				$salida.='>'.$value.'</option>';
			}

		echo '<select id="ref_link" name="ref_link" style=\'width:170px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>

  </td>
	<td align="right">Idioma:</td>
	<td>
<?php
		// START SE CONTRUYE EL ARRAY CON TODOS LOS ELEMENTOS DE MEN�S 
		$salida='';
		$query='SELECT * FROM '.$this->sql->db.'.idiomas ORDER BY idioma';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->id_idioma== $arra['id']) $salida.=' selected="selected"';
			$salida.='>'.$arra['idioma'].'</option>';
		}

		echo '<select id="id_idioma" name="id_idioma" style=\'width:170px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
</tr>
<tr>
	<td align="right">Entrada de men�:</td>
	<td ><input type='text' name='texto_idioma' value='<?=$this->texto_idioma?>' style='width:170px'></td>
	<td align="right">Texto alternativo:</td>
	<td ><input type='text' name='texto_alternativo' value='<?=$this->texto_alternativo?>' style='width:170px'></td>
</tr>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
	if(document.f<?=$idunico?>.ref_link.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.ref_link);
		document.f<?=$idunico?>.ref_link.focus();
		alert('Por favor, seleccione el men�');
		restauraCampo(document.f<?=$idunico?>.ref_link);
		return;
	}
	if(document.f<?=$idunico?>.id_idioma.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.id_idioma);
		document.f<?=$idunico?>.id_idioma.focus();
		alert('Por favor, seleccione el idioma');
		restauraCampo(document.f<?=$idunico?>.id_idioma);
		return;
	}
	if(document.f<?=$idunico?>.texto_idioma.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.texto_idioma);
		document.f<?=$idunico?>.texto_idioma.focus();
		alert('Por favor, escriba la traducci�n del men� deseado para idioma seleccionado');
		restauraCampo(document.f<?=$idunico?>.texto_idioma);
		return;
	}

	document.f<?=$idunico?>.submit();
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}
-->
</script>
<form action='<?=$destino?>' method='post' name='f<?=$idunico?>' enctype='multipart/form-data'>
<input type='hidden' name='accion' value='<?=$accion?>'>
<input type='hidden' name='id' value='<?=$this->id?>'>
<?=($_REQUEST['id'])?str_pad('',30,'<br />'):''?>
<table align='center' cellpadding='5' cellspacing='0' class='borde' border='0'>
<tr>
	<td colspan='12' class='enc2'><?=$titulo?></td>
</tr>
<?php
//		echo " <p style=\"color:red\">[<b>$destino</b>] accion= ".$accion.'</p>';
		$this->formulario_datos();
?>
<tr>
	<td colspan='12' align='center'>
  	<a href="#null" class='btn' onclick="revisa()" style="width:160px;" >Guardar datos</a>
<?php
if($_REQUEST['id'])
	echo '&nbsp;&nbsp;&nbsp;<a href="#null" class="btn xcancel" onclick="parent.edicionClose()" style="width:110px;" >Cerrar sin guardar</a>';
?>
  </td>
</tr>
</table>
</form>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->ref_link=$datos['ref_link'];
		$this->texto_idioma=$datos['texto_idioma'];
		$this->texto_alternativo=$datos['texto_alternativo'];
		$this->id_idioma=$datos['id_idioma'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N�mero registros: ')
	{
		$idunico=rand(1,10000).time();
		if (count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b�squeda:</span> ";

		$query='SELECT * FROM '.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(*) FROM '.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='ref_link'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' DESC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//******************************************************


//******************************************************
//Formulario para efectuar b�squedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->


<!-- START BLOQUE PARA GESTIONAR EDICION FLOTANTE -->
<input type="hidden" name="accion" value="<?=$_REQUEST['accion']?>" />
<input type="hidden" id="id" name="id" value="" />

<iframe id="ifrNotas" name="ifrNotas" style="display:none"></iframe>

<div id="divEdicionFlotante" style="position:absolute;display:none;top:0px;left:20px;width:95%;height:90%;z-index:100;border:1px solid #015EAF">
<iframe id="ifrFlotanteEdicion" name="ifrFlotanteEdicion" style="display:inline;width:100%;height:100%;overflow:auto" scrolling="auto" frameborder="0" allowtransparency="0" src="" width="100%"></iframe>
</div>

<div id="divEdicionFlotanteSombra" style="position:absolute;display:none;top:10px;left:20px;background-color:<?=$_SESSION['colorFondoFlotante']?>;border-left:0px solid #000;border-bottom:0px solid #000; text-align:right;opacity:0.6;filter:alpha(opacity=60);"></div>
<iframe id="ifrEdition" name="ifrEdition" style="display:none;width:90%"></iframe>

<div id="divEdicionProducto" style="position:absolute;display:none;top:90px;left:40px;width:90%;text-align:left;z-index:101">
</div>
<!-- END BLOQUE PARA GESTIONAR EDICION FLOTANTE -->

<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='780' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td align="right">Idioma:</td>
	<td>
<?php
		// START SE CONTRUYE EL ARRAY CON TODOS LOS ELEMENTOS DE MEN�S 
		$salida='';
		$query='SELECT * FROM '.$this->sql->db.'.idiomas ORDER BY idioma';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_idioma_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_idioma_buscN']==$arra['id']) $salida.=' selected="selected"';
			$salida.='>'.$arra['idioma'].'</option>';
		}

		echo '<select id="'.$this->posicionSolapa.'id_idioma_buscN" name="'.$this->posicionSolapa.'id_idioma_buscN" style=\'width:110px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
	<td width="20">Texto:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>texto_idioma_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'texto_idioma_busca_u']?>' style='width:110px'></td>
	<td width="20">URL:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>ref_link_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'ref_link_busca_u']?>' style='width:110px'></td>
  <td width="10%"></td>
	<td align="right"><a href="#null" class='btn' onclick="submit()" style="width:90px;" >Buscar</a>&nbsp;&nbsp;<a href="#null" class='btn' onclick="document.location='<?=$destinobusca?>.php?reset=1'" style="width:60px;color:<?=$_SESSION['colorTextWarning']?>">Reset</a></td>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->


<script language="JavaScript" type="text/javascript">
<!--
var xXp=-100; var fMH=0; var qMH=0;
var opacidad=30;
var tmpConsulta;

function aparecerDespacio(idDiv)
{
	if(xXp>0&&fMH==0 && qMH){fMH=1;xXp=-100;return;}
	if(xXp>50&&fMH==1){fMH=0;xXp=-100;return;}
	if(!fMH)qMH=16;
	xXp=xXp+qMH;
	lm=((xXp < 40)?lm=opacidad+xXp:lm=60);
	document.getElementById('divEdicionFlotanteSombra').style.filter='alpha(opacity='+(lm)+')';

	e=document.getElementById(idDiv);
	e.style.top = xXp + 'px';
	t=setTimeout("aparecerDespacio('"+idDiv+"');",1);
}

function editarRegistro(id)
{
	if(id>0)
		idVisualizado=id;

		myDiv = document.getElementById('divEdicionFlotanteSombra');
		sombraL= getDimensions(myDiv).x;
		sombraT= getDimensions(myDiv).y;
		
		document.getElementById('divEdicionFlotanteSombra').style.top='0px';
		document.getElementById('divEdicionFlotanteSombra').style.left='0px';
		document.getElementById('divEdicionFlotanteSombra').style.width='100%';
		document.getElementById('divEdicionFlotanteSombra').style.height='100%';
		document.getElementById('divEdicionFlotanteSombra').style.filter='alpha(opacity=10)';
		document.getElementById('divEdicionFlotanteSombra').style.display='inline';
		document.getElementById('divEdicionFlotanteSombra').innerHTML='<center><span style=color:#000;background-color:#ffffff;font-size:16px><b>&nbsp;CARGANDO...&nbsp;</b></span></center>';
//		document.getElementById('divEdicionFlotante').style.display='none';
		document.formListados<?=$idunico?>.target='ifrEdition';
		laAccion=document.formListados<?=$idunico?>.accion.value;
		document.formListados<?=$idunico?>.accion.value=8;
		document.formListados<?=$idunico?>.id.value=idVisualizado;
		document.formListados<?=$idunico?>.submit();
		document.formListados<?=$idunico?>.accion.value=laAccion;
		document.formListados<?=$idunico?>.target='_self';
		return;
}

function edicionClose()	// CERRAR EDICI�N DE PRODUCTO SIN REALIZAR CAMBIOS
{
	var nSel=document.getElementsByTagName("select");
	var numsel=nSel.length;;
	for(n=0;n<numsel;n++)
	{
		nSel[n].style.visibility="visible";
	}

	document.getElementById('divEdicionFlotante').style.display='none';
	document.getElementById('divEdicionFlotante').style.top='-100px';
	qMH=10; xXp=80; fMH=1;
	quitarSombra("divEdicionFlotanteSombra");
}

function quitarSombra(idDiv)
{
	if(xXp<0&&fMH==0 && qMH){fMH=1;xXp=0;document.getElementById('divEdicionFlotanteSombra').style.display='none';return;}
	if(xXp<0&&fMH==1){fMH=0;xXp=0;document.getElementById('divEdicionFlotanteSombra').style.display='none';return;}
	if(!fMH)qMH=5;
	xXp=xXp-qMH;
	e=document.getElementById(idDiv);
	e.style.filter= 'alpha(opacity='+(xXp)+')';
	t=setTimeout("quitarSombra('"+idDiv+"');",1);
	return;
}

function modifica(id)	// PARA MANTENER COMPATIBILIDAD
{
<?php
		if(!$this->editarFila)
			echo 'return;';
?>
	editarRegistro(id);
	return;
}

function eliminar(id,nombre)
{
	if(confirm('�Desea realmente eliminar el men� "'+nombre+'"?'))
	{
		var a=screen.width;
		var b=screen.height;
		var ancho=300;
		var alto=140;
		var x=(a-ancho)/2;
		var y=(b-alto)/2;
		var t=window.open ('<?=$destinobusca?>_eliminar.php?id='+id+'&accion=1','eliminar','width='+ancho+',height='+alto+',left='+x+',top='+y+'');
		t.focus();
		return;
	}
}

function ordenarPorCampo(campo)	// ORDENACI�N DEL LISTADO DE RESULATDOS
{
	document.getElementById('mesajes_formulario').innerHTML='<span style=color:<?=$_SESSION['colorTextWarning']?>><b>ordenando...</b></span>';
	if(document.formListados<?=$idunico?>.ordenarPor.value==campo)	// SI EL CAMPO POR EL QUE SE EST� ORDENANDO ES EL MISMO, SE INVIERTE EL SENTIDO DEL ORDEN DEL MISMO
	{
		if(document.formListados<?=$idunico?>.sentidoOrdenacion.value==1)
			document.formListados<?=$idunico?>.sentidoOrdenacion.value=0;
		else
			document.formListados<?=$idunico?>.sentidoOrdenacion.value=1;
	}
	// SE RECARGA LA P�GINA CON LOS VALORES CORRIENTES ORDENANDO POR EL NUEVO CAMPO SELECCIONADO
	document.location='<?=$destinobusca?>.php?reset=<?=$_REQUEST['estado.php']?>&offset_u=<?=$_REQUEST['offset_u']?>&ordenarPor='+campo+'&sentidoOrdenacion='+document.formListados<?=$idunico?>.sentidoOrdenacion.value+'';
	return;
}


function recargarPaginaOrdenada(campo)	// ORDENACI�N DEL LISTADO DE RESULATDOS
{
	// SE RECARGA LA P�GINA CON LOS VALORES CORRIENTES ORDENANDO POR EL NUEVO CAMPO SELECCIONADO
	document.location='<?=$destinobusca?>.php?reset=<?=$_REQUEST['estado.php']?>&offset_u=<?=$_REQUEST['offset_u']?>&ordenarPor='+campo+'&sentidoOrdenacion='+document.formListados<?=$idunico?>.sentidoOrdenacion.value;
	return;
}
-->
</script>
<!-- *******************  fin formulario b�squedas  *************** !-->
<?php
		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('Idioma','Texto link','URL','Texto alternativo');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id','id_idioma','texto_idioma','ref_link','texto_alternativo');
		$xc=0;$arraLongitudes=array($xc++=>40,$xc++=>40,$xc++=>29,$xc++=>60); // no se incluye el valor para el ID
			$xc=0;$arraLongitudesTitulares=array($xc++=>25,$xc++=>160,$xc++=>180,$xc++=>130,$xc++=>190);
		$xc=0;$arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'txt',$xc++=>'txt',$xc++=>'txt');
		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$xc=0; $estaActivo='<center style=color:orange>no</center>';
			$query='SELECT idioma FROM '.$this->sql->db.'.idiomas WHERE id='.$a['id_idioma'];
			$elIdioma=$this->sql->valor($query);

			if($a['activo']) $estaActivo='<center style=color:lightgreen><b>SI</b></center>';

			$alternativo=(trim($a['texto_alternativo']))?trim($a['texto_alternativo']):'&nbsp;';
			$arraDatos[]=array($xc++=>$a['id'],$xc++=>$elIdioma,$xc++=>$a['texto_idioma'],$xc++=>$a['ref_link'],$xc++=>nl2br($alternativo));
		}
//($xc++=>$a['texto_alternativo'])?$xc++=>nl2br($a['texto_alternativo'])?:'-')
		$idRefPops=0;
		$txtRefEliminar=1;
		$posicFondo=0;	// N�MERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  FIN DEL LISTADO  ********************/

		if($num_res>mysql_num_rows($res) && !$this->contFilas)
		{
			foreach($_SESSION['filtros'] as $key=>$value)	// SE RECARGA LA PAGINA SI NO HAY FILAS EN PANTALLA Y HAY RESULTADOS EN LA BUSQUEDA
			{
				$pos=strpos($key,'_busca');
				if ($pos>0)
					$url.='&'.$key.'='.urlencode($value);
			}

			$_SESSION['filtros']['offset_u']=0;
			echo "<script language='JavaScript' type='text/JavaScript'>
			document.location='".$destinobusca.".php?$url';
			</script>";
		}
?>
</table>
<table align='center' cellpadding='2'>
<tr><td style='line-height:22px' align='center'><span id='spn_infoPage'></span>
<?php
	$url='';
	foreach($_SESSION['filtros'] as $key=>$value)
	{
		$pos=strpos($key,'_busca');
		if ($pos>0)
			$url.='&'.$key.'='.urlencode($value);
	}
	if(isset($_REQUEST['offset_u']))
		$offset_u = $_REQUEST['offset_u'];
	else
		$offset_u = 0;

// ********************************************************************************************************
// nota importante: EL �LTIMO PAR�METRO ES LA REFERENCIA DEL IFRAME RESPECTO A LOS DEM�S DE LA P�GINA PADRE, es B�SICO
// ********************************************************************************************************
	$hay=$this->paginacion($num_res, $this->filasporpagina, $offset_u, $destinobusca.".php?filtrar=".$_REQUEST['filtrar'].$url.'&sentidoOrdenacion='.$_REQUEST['sentidoOrdenacion'].'&ordenarPor='.$_REQUEST['ordenarPor']."&offset_u=",$this->TotPagMostrar,$this->posicionSolapa);
// ********************************************************************************************************
	if(trim($hay))
		echo '<script>
$("#paginarTop").html(\''.$hay.'\');
$("#paginarTop").css("display","block");
</script>';
echo $hay;
?>
<script>
var arraTxtDescripcion=new Array();
var sombraL; var sombraT;
function mostrarPagina(direccion,posicionSolapa)	// se recarga la p�gina desde los links de la barra inferior de paginaci�n (2011)
{
	$("#mesajes_formulario").html("<i>accediendo...</i>");
	document.location=direccion+"&posicionSolapa="+posicionSolapa;
	return;
}

</script>
</td></tr></table>
</form>
<?php
	}
}
?>