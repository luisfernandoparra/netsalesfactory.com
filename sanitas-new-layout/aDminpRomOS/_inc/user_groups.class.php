<?php
class userGroups extends Base
{
	//Datos propios de la clase
	var $id;
	var $linkmenu;
	var $nombreTabla;
	var $posicionSolapa;	// POSICION DE LA VENTANA RESPECTO A LAS DEMAS DE SU MISMA SECCION E IDENTIFICADOR PARA LOS CAMPOS DE LAS BUSQUEDAS

//******************************************************
//	INICIALIZACION DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'user_groups';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->getAdminLevel();	// RETRIEVE CONSTANTS LEVELS USER
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE ID="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->group_name=$this->output_datos($a['group_name']);
		$this->ids_users=$a['ids_users'];
		$this->is_enabled=$a['is_enabled'];
		return;
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.(int)$this->id.' && is_enabled';
		$existen_paginas=$this->sql->valor($query);
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		if(!$this->id) $this->id=0;
		if($this->ids_users)
		{
		  foreach($this->ids_users as $key=>$value)
			$userList.=$value.',';
		}

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id,group_name,ids_users,is_enabled) VALUES ("'.$this->id.'", "'.trim($this->group_name).'", "'.$userList.'","'.(int)$this->is_enabled.'")';

		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos()
	{
?>
<tr>
	<td align="right">Nombre del grupo:</td>
	<td colspan="3"><input type='text' id="group_name" name='group_name' value='<?=$this->group_name?>' style='width:95%;'></td>

	<td align="right" title="Lista de usuarios">Usuarios:</td>
	<td rowspan="3">

<?php
		$arrUsers=explode(',',$this->ids_users);
		$salida='';
		$query='SELECT id, CONCAT(nombre," ",apellidos) AS theName,nivel_acceso FROM '.$this->sql->db.'.usuarios WHERE activo=1 ORDER BY nombre';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			//SE OMITEN OTROS USUARIOS CON NIVEL SUPERIOR DE ACCESO AL DEL USUARIO ACTUAL
//			if($_SESSION['usuarioNivel'] < $arra['nivel_acceso'])	continue;

			$salida.='<option value=\''.$arra['id'].'\'';
			if(in_array($arra['id'],$arrUsers)) $salida.=' selected="selected" ';
			$salida.='>'.utf8_encode($arra['theName']).'</option>';
		}

		echo '<select id="ids_users" name="ids_users[]" style="width:100%" multiple="multiple" >';
		echo $salida;
		echo '</select>';
?>
	</td>

<tr>
</tr>
	<td align="right" title="El men&uacute; es activo?" valign="top" style="padding-top:4px;">Est&aacute; activo:</td>
	<td valign="top">
<?php
		echo '<select id="is_enabled" name="is_enabled" style="width:50px;">';
		echo '<option value=-1 ';
		if($this->is_enabled==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->is_enabled==0 && isset($this->is_enabled))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->is_enabled==1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>
</tr>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
	if(document.f<?=$idunico?>.group_name.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.group_name);
		document.f<?=$idunico?>.group_name.focus();
		alert('Por favor, escriba el nombre del grupo');
		restauraCampo(document.f<?=$idunico?>.group_name);
		return;
	}
	if($('#ids_users').val() == null)
	{
		$("#ids_users").focus();
		alert('Por favor, debe seleccionar por lo menos un uasuario');
		return;
	}

	document.f<?=$idunico?>.submit();
}

-->
</script>
<?php
		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->group_name=$datos['group_name'];
		$this->is_enabled=$datos['is_enabled'];
		$this->ids_users=$datos['ids_users'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$idunico=rand(1,10000).time();

//		$query='SELECT ug.* FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS ug WHERE 1 ';
		$query='SELECT ug.*,u.nivel_acceso FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS ug LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id IN(ug.ids_users) WHERE u.nivel_acceso <='.(int)$_SESSION['usuarioNivel'];

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa,'ug.');	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		$querytotal='SELECT COUNT(ug.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS ug LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id IN(ug.ids_users) WHERE 1 '.$filtroSql;
//		$querytotal='SELECT COUNT(ug.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS ug WHERE 1 '.$filtroSql;
//die();
		$ordenacion='ug.group_name'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;

		if($_SESSION['filtros']['visitantes_distintos_busca'])
		{
			$res2=$this->sql->query($querytotal);
			$num_res=mysql_num_rows($res2);
		}
		else
			$num_res=$this->sql->valor($querytotal);

		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $this->levelAccessMaxLevel.'<hr>';
//echo $query;

//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->

<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='780' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
<?php
?>
	<td width="90" align="right">Nombre grupo:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>group_name_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'group_name_busca_u']?>' style='width:180px'></td>
	<td width="30">Activo:</td>
	<td>
<?php
		$arraActivo[0]='NO';
		$arraActivo[1]='SÍ';
		echo '<select name="'.$this->posicionSolapa.'is_enabled_buscN" style=\'width:50px;\'>';
		echo '<option value="-1" >?</option>';
		foreach($arraActivo as $key=>$value)
		{
			echo '<option value='.$key.' ';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']==$key)
				echo 'selected=\'selected\'';
			echo ' >'.$value.'</option>';
		}
		echo '</select>';
?>
  </td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->


<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'170px','','700px','18%');

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		if(!$_SESSION['forcedLangSite']) $arraTitulares[]='Idioma';
		$arraTitulares[]='Nombre grupo';
		$arraTitulares[]='usuarios';
		$arraTitulares[]='Activo';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		if(!$_SESSION['forcedLangSite']) $arraCamposOrdenar[]='id_idioma';
		$arraCamposOrdenar[]='group_name';
		$arraCamposOrdenar[]='ids_users';
		$arraCamposOrdenar[]='is_enabled';

		 // no se incluye el valor para el ID
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=50;
		$arraLongitudes[]=44;
		$arraLongitudes[]=84;
		$arraLongitudes[]=70;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=25;	// id
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=90;
		$arraLongitudesTitulares[]=210;
		$arraLongitudesTitulares[]=400;
		$arraLongitudesTitulares[]=50;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='rop';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;
//echo $_SESSION['usuarioNivel'];
		while($a=$this->sql->fila2($res))
		{
echo $a['id_usuario'];
		  $rop=null; $theScope='<span style=color:'.$_SESSION['colorTextWarning'].';>?<span>';
			//	SOLO SE PERMITEN VER LOS ELEMENTOS PROPIOS Y LOS GLOBALES (EXCEPTO PARA GOOD)
		  if($_SESSION['id_usuario'] != $a['id_usuario'] && $a['id_usuario'] != 0 && $this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])
				$rop=1; // fila deshabilitada

		  $xc=0; $estaActivo='<center style=color:orange>no</center>';
		  if($a['is_enabled']==-1) $esActivo='?';
		  if($a['is_enabled']==0) $esActivo='<span style=color:'.$_SESSION['colorTextWarning'].'>NO</span>';
		  if($a['is_enabled']==1) $esActivo='SI';
		  if($a['is_enabled']==1) $esActivo='SI';
		  if($a['scope_element'] == 1) $theScope='Solapas';
		  if($a['scope_element'] == 2) $theScope='Footer';

		  $query='SELECT CONCAT(nombre," ",apellidos) AS theName FROM '.$this->sql->db.'.usuarios WHERE id IN('.rtrim($a['ids_users'],',').')';
		  $resU=$this->sql->query($query);
		  $usersOfGroup='';
		  while($b=$this->sql->fila2($resU))
			$usersOfGroup.=utf8_encode($b['theName']).', ';

		  $usersOfGroup=substr($usersOfGroup,0,-2);
		  $elUsuario=$elUsuario?$elUsuario:'<span style=color:'.$_SESSION['colorTextWarning'].';><b>Todos</b></span>';

		  if($a['is_enabled']) $estaActivo='<center style=color:lightgreen><b>SI</b></center>';

		  $tmpArr[0]=$a['id'];
		  array_push($tmpArr,$a['group_name']);
		  array_push($tmpArr,$usersOfGroup);
		  array_push($tmpArr,$esActivo);
		  array_push($tmpArr,$_SESSION['usuarioNivel']);
		  $arraDatos[]=$tmpArr;
		  unset($tmpArr);
		}

		$idRefPops=0;
		$txtRefEliminar=1;
		$posicFondo=0;	// NUMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
