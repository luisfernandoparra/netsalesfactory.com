<?php
class PlugInsModules extends Base
{
	//Datos propios de la clase
	public $id;
	public $nombre;
	public $nombreTabla;
	public $tamanioMaximoImagenes;
	public $path_imagenes;
	public $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS
	public $anchoMaximoImagenes;
	public $path_plug_ins_admin;

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'back_office_modules';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las búsquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->module_name=$a['module_name'];
		$this->texto=$a['texto'];
		$this->tooltip_home=$a['tooltip_home'];
		$this->module_path=$a['module_path'];
		$this->link_externo=$a['link_externo'];
		$this->is_enabled=$a['is_enabled'];
		$this->posicion=$a['posicion'];
		$this->id_idioma=$a['id_idioma'];
		$this->module_type=$a['module_type'];
		$this->imagen_galeria=$a['imagen_galeria'];
		$this->home=$a['home'];
		return;
	}

//******************************************************
//	ELIMINACIÓN DE REGISTROS CON CONTROL DE DATOS RELACIONADOS DE OTRAS TABLAS U OTRAS SELECCIONES
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE is_enabled && id='.$_REQUEST['id'];
		$existen_paginas=$this->sql->valor($query);
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$_REQUEST['id'];

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
		{
			$isError=1;
			$res=@rmdir(PATH_PLUG_INS_ADMIN.$this->module_path);
			$this->mensajeSalida='Ha fallado el intento de borrar "<b>'.PATH_PLUG_INS_ADMIN.$this->module_path.'</b>".<br />Comprueba que los permisos sean adecuados<br />o que la carpeta a borrar est&eacute; completamente vac&iacute;a.';

			if($res)
				$isError=0;

			if($isError)
			{
				echo $res.'<center><p style="color:red">'.$this->mensajeSalida.'</p><br /><br /><a href="#null" style="width:100px;padding:5px;" class="btn rounded" onclick="cerrarBoxDelete()">cerrar</a></center>
				<script>var oldColor=parent.parent.$("#cboxTitle").css("color");parent.$("#titDeleteProcess").html("Proceso abortado");parent.parent.$("#cboxTitle").css("color","red");parent.parent.$("#cboxTitle").html("REVISAR EL ERROR");function cerrarBoxDelete(){parent.parent.$("#cboxTitle").css("color",oldColor);parent.parent.modalClose();}</script>';
				die();
			}

			$this->sql->query($query);
		}
		return $existen_paginas;
	}

//******************************************************
//	GUARDAR DATOS DE INSERCIONES Y EDICIONES
//******************************************************
	function guarda_datos()
	{
		$enEdicion=$this->id ? '_editar' : '';
		if(!$this->id) $this->id=0; // NECESARIO PARA AÑADIR NUEVOS REGISTROS

		if($_REQUEST['old_module_path'] != $this->module_path && $this->id)	// SE HA CAMBIADO EL NOMBRE DE LA CARPETA
		{
			$isError=1;
			$this->mensajeSalida='Error al intentar renombrar la carpeta en "<b>'.PATH_PLUG_INS_ADMIN.$_REQUEST['old_module_path'].'</b>".<br />Comprueba que hay permisos para efectuar esta operación<br />o que no exista ya una carpeta denominada <b>"'.$this->module_path.'"</b>.';
			$res=@rename(PATH_PLUG_INS_ADMIN.$_REQUEST['old_module_path'], PATH_PLUG_INS_ADMIN.$this->module_path);
			if($res)
				$isError=0;
		}

		if($this->module_path && !$this->id)	// SE CREA UNA NUEVA CARPETA
		{
			$isError=2;
			$this->mensajeSalida='Error al crear la carpeta en "<b>'.PATH_PLUG_INS_ADMIN.$this->module_path.'</b>".<br />Comprueba que hay permisos para efectuar esta operación<br />o que no exista ya una carpeta denominada <b>"'.$this->module_path.'"</b>.';
			$res=@mkdir(PATH_PLUG_INS_ADMIN.$this->module_path);
			if($res)
				$isError=0;
		}

		if($isError)
		{
				echo $res.'<center><p style="color:red">'.$this->mensajeSalida.'</p><br /><br /><a href="#null" style="width:100px" class="btn" onclick="document.location=\''.$this->nombreFilePhpBase.$enEdicion.'.php?id='.$this->id.'\';">volver a editar</a></center>';
				die();
		}

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id, module_type, module_name, is_enabled, module_path) VALUES ("'.$this->id.'", "'.(int)$this->module_type.'", "'.utf8_decode($this->module_name).'", '.(int)$this->is_enabled.', "'.$this->module_path.'")';

		if(!$_img && $this->id)
		$query='UPDATE '.$this->sql->db.'.'.$this->nombreTabla.' SET module_type="'.(int)$this->module_type.'", module_name="'.utf8_decode($this->module_name).'", is_enabled="'.(int)$this->is_enabled.'", module_path="'.$this->module_path.'" WHERE id='.(int)$this->id;

//echo $query.'<br /><pre>';print_r($_REQUEST);print_r($this);exit;
		$this->sql->query($query);
		return;
	}


//******************************************************
//	FORMULARIO DE EDICIÓN /INSERCIÓN DE DATOS
//******************************************************
	function formulario_datos($nombreForm=0)
	{
//		$this->PhpEditarHTML($this->nombreTabla,$nombreForm,$this->id);
		$arraSiNo=array('No','S&iacute;');
?>
<tr>
	<td width="90" title="module name" align="right">module name:</td>
	<td width="200"><input type='text' id='module_name' name='module_name' value='<?=  utf8_encode($this->module_name)?>' maxlength="255" style='width:100%'/></td>
	<td width="60" title="Ruta donde del módulo" align="right">Ruta:</td>
	<td>
		<input type='text' id='module_path' name='module_path' value='<?=$this->module_path?>' maxlength="255" style="width:99%" />
  </td>
<?php
		if(!$_SESSION['forcedLangSite'])	// SOLO SI HAY GESTION DE IDIOMAS
		{
			echo '<td align="right">Idioma:</td><td>';
			$salida='';
			$query='SELECT * FROM '.$this->sql->db.'.idiomas ORDER BY idioma';
			$res2=$this->sql->query($query);

			while($arra=$this->sql->fila2($res2))
			{
				$salida.='<option value=\''.$arra['id'].'\'';
				if($this->id_idioma== $arra['id']) $salida.=' selected="selected"';
				$salida.='>'.$arra['idioma'].'</option>';
			}

			echo '<select id="id_idioma" name="id_idioma" style=\'width:110px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
			echo $salida;
			echo '</select>
		</td>';
		}
		else
		{
			echo '<input type="hidden" id="id_idioma" name="id_idioma" value="'.$_SESSION['forcedLangSite'].'" />';
		}
?>
</tr>

<tr>
	<td title='Indicar el tipo del m&oacute;dulo' align="right">Tipo del m&oacute;dulo:</td>
	<td>
<?php
		$query2='SELECT id,description_type,description_details FROM '.$this->sql->db.'.back_office_module_types WHERE is_enabled';
		$res2=$this->sql->query($query2);

		while($arra2=$this->sql->fila2($res2))
		{
			$salidaFam.='<option value=\''.$arra2['id'].'\'';
			if($this->module_type==$arra2['id']) $salidaFam.=' selected="selected"';
			$salidaFam.='>'.$arra2['description_type'].'</option>';
		}

		echo '<select id="module_type" name="module_type" style=\'width:120px;\'>';
		echo '<option value=-1 ';
		if($this->posicion==-1)
			echo 'selected=\'selected\'';
		echo '>Gen&eacute;rico</option>';
		echo utf8_encode($salidaFam);
		echo '</select>';
?>
	</td>
	<td title='Indica si el idioma está disponible para su utilización' align="right" width="50">Activo:</td>
	<td>
<?php
		echo '<select id="is_enabled" name="is_enabled" style=\'width:50px;\'>';
		echo '<option value=-1 ';
		if($this->is_enabled==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->is_enabled==$n && isset($this->is_enabled))
				echo 'selected=\'selected\'';
			echo ' >'.$arraSiNo[$n].'</option>';
		}
		echo '</select>';
?>
		<input type='hidden' id='old_module_path' name='old_module_path' value='<?=$this->module_path?>' />
	</td>

  </tr>

<?php
		if($this->id) echo '<tr><td height="10"></td></tr>';
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
var txPagina = new Array();
var idPagina = new Array();
var tipoRespuestas=0;
var esArchivo=0;

$(function(){
	//editor jQuery color box
	$(".editAjax").colorbox({
		opacity: 0.4,
		initialHeight:"10px",
		initialWidth:"10%",
		close:true,
		overlayClose:true,
		width:"<?=$this->id ? 88 : 74 ?>%",
		height:"100%",
		iframe:true,
		html:true,
		innerHeight:true
	});
});

function removeOptionSelected(nombreSelect)	// ELIMINA TODOS LOS ELEMENTOS DEL SELECT 'nombreSelect'
{
	txPagina = new Array();
	idPagina = new Array();
	var elSel = document.getElementById(nombreSelect);
	for(i=elSel.length;i>=0;i--)	// CAMBIAR 'i>0' A 'i>=0' SI HAY QUE ELIMINAR TODAS LAS OPCIONES DEL SELECT O 'i>=0' SI HAY QUE QUITAR TAMBIÉN 'seleccionar'
		elSel.remove(i);
	return;
}

function revisa()
{

	if(document.f<?=$idunico?>.id_idioma.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.id_idioma);
		document.f<?=$idunico?>.id_idioma.focus();
		alert('Por favor, seleccine un idioma');
		restauraCampo(document.f<?=$idunico?>.id_idioma);
		return;
	}
	if(trim($('#module_name').val()) == "")
	{
		resaltarCampo($("#module_name")[0]);
		$("#module_name").focus();
		alert('Es obligatorio indicar el nombre del módulo');
		document.f<?=$idunico?>.module_name.focus();
		restauraCampo($("#module_name")[0]);
		return false;
	}
	if(trim(document.f<?=$idunico?>.module_path.value)=="")
	{
		resaltarCampo(document.f<?=$idunico?>.module_path);
		alert('Es obligatorio indicar la ruta del módulo');
		document.f<?=$idunico?>.module_path.focus();
		restauraCampo(document.f<?=$idunico?>.module_path);
		return;
	}
	if($('#is_enabled').val() < 0)
	{
		resaltarCampo($("#is_enabled")[0]);
		$("#is_enabled").focus();
		alert('Por favor, seleccione si está habilitado');
		document.f<?=$idunico?>.is_enabled.focus();
		restauraCampo($("#is_enabled")[0]);
		return;
	}

	document.f<?=$idunico?>.submit();
}

function appendOptionLast(id,texto,nombreSelect)	// AÑADE ELEMENTOS (OPTIONS) A LA SELECT 'nombreSelect'
{
  var elOptNew = document.createElement('option');
  elOptNew.text = texto;
  elOptNew.value = id;
  var elSel = document.getElementById(nombreSelect);
  try	//intenta la instrucción siguiente, si lo consigue, ejecuta
	{
    elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
  }
  catch(ex)	// si falla la anterior instrucción, se ejecutará la siguiente
	{
    elSel.add(elOptNew); // IE only
  }
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}

-->
</script>
<iframe name='ifrAopyoPreg' id='ifrAopyoPreg' style='display:none;width:100%' src=''></iframe>
<?php
		$camposAdicionales='<input type="hidden" id="ref_desplegable" name="ref_desplegable" value="">
';
		include('_inc/form_std_edit.php');
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->module_name=$datos['module_name'];
		$this->module_path=$datos['module_path'];
		$this->module_type=$datos['module_type'];
		$this->is_enabled=$datos['is_enabled'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	public function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='Número registros: ')
	{
//		$mens.='</b>';
		if(count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la búsqueda:</span> ";

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='module_name'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' DESC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';

//******************************************************
//Formulario para efectuar búsquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>
<form action='<?=$destinobusca?>.php' method='post' name='formListados'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->

<table align='center' cellpadding='3' cellspacing='0' class='borde' width='500' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
<?php
		if(!$_SESSION['forcedLangSite'])
		{
			echo '<td width="30">idioma:</td>
				<td title="Idiomas disponibles">';
			$query='SELECT * FROM '.$this->sql->db.'.idiomas ORDER BY idioma';
			$res2=$this->sql->query($query);

			echo '<select id="'.$this->posicionSolapa.'id_idioma_buscN" name="'.$this->posicionSolapa.'id_idioma_buscN" style=\'width:110px;\' title="Idiomas disponibles">';
			echo '<option value=-1 ';
			if($this->posicion==-1)
				echo 'selected=\'selected\'';
			echo '>?</option>';
			while($arra2=$this->sql->fila2($res2))
			{
				echo '<option value='.$arra2['id'].' ';
				if($_SESSION['filtros'][$this->posicionSolapa.'id_idioma_buscN']==$arra2['id'])
					echo 'selected=\'selected\'';
				echo ' >'.$arra2['idioma'].'</option>';
			}
			echo '</select></td>';
		}
?>
	<td width="30">is_enabled:</td>
	<td>
<?php
		$arraEsActivoBusc=array(0=>'No',1=>'S&iacute;');
		echo '<select name="'.$this->posicionSolapa.'is_enabled_buscN" style=\'width:50px;\' title="Está is_enabled?">';
		echo '<option value="-1" >?</option>';
		foreach($arraEsActivoBusc as $key=>$value)
		{
			echo '<option value='.$key.' ';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']==$key)
				echo 'selected=\'selected\'';
			echo ' >'.$value.'</option>';
		}
		echo '</select>';
?>
  </td>

<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->

<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,$this->modalHeight,'',$this->modalWidth,$this->modalLeftPos);
//		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'55%','','94%','3%');	//DATOS PARA LA POP INCRUSTADA

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='Nombre módulo';
		$arraTitulares[]='Ruta';
		if(!$_SESSION['forcedLangSite']) $arraTitulares[]='Idioma';
		$arraTitulares[]='Activo';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='module_name';
		$arraCamposOrdenar[]='module_path';
		if(!$_SESSION['forcedLangSite']) $arraCamposOrdenar[]='id_idioma';
		$arraCamposOrdenar[]='is_enabled';

		 // no se incluye el valor para el ID
		$arraLongitudes[]=40;
		$arraLongitudes[]=60;
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=60;
		$arraLongitudes[]=100;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=25;
		$arraLongitudesTitulares[]=300;
		$arraLongitudesTitulares[]=300;
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=50;
		$arraLongitudesTitulares[]=46;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='img';
		$arraTipos[]='txt';

		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$mostrarImagenVideo='';
			$enPortada=$a['home']?'<span style=color:'.$_SESSION['colorTextHighli'].'><b>SI</b></span>':'NO';
			$query2='SELECT nombre_familia FROM '.$this->sql->db.'.videos_familias WHERE id_idioma='.$a['id_idioma'].' && ref_familia='.$a['module_type'].' ORDER BY nombre_familia';
//			$laFamilia=$this->sql->valor($query2);
			$esActivo=($a['is_enabled']==1)?'<span style=color:'.$_SESSION['colorTextHighli'].'>SI</span>':'<span style=color:'.$_SESSION['colorTextWarning'].'><b>NO</b></style>';
			$query2='SELECT prefijo FROM '.$this->sql->db.'.idiomas WHERE id='.$a['id_idioma'];
//			$prefBanderita=$this->sql->valor($query2);
			$laPosicion=$a['posicion']?$a['posicion']:'<center><span style=color:'.$_SESSION['colorTextWarning'].';><b>?</b></span></center>';

			$archivoImagen='../banderas/flag_'.$prefBanderita.'.jpg';
			$mostrarBanderita=file_exists($this->path_imagenes.$a['imagen_galeria'])?$a['imagen_galeria']:'no_image_ind.jpg';

			if($a['imagen_galeria'])
				$mostrarImagenVideo=$this->path_imagenes.(file_exists($this->path_imagenes.$a['imagen_galeria'])?$a['imagen_galeria']:'no_image_ind.jpg');

			$tmpArr[0]=$a['id'];
			array_push($tmpArr,  utf8_encode($a['module_name']));
			array_push($tmpArr,$a['module_path']);
			if(!$_SESSION['forcedLangSite']) array_push($tmpArr,$archivoImagen);
			array_push($tmpArr,$esActivo);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
		}
//print_r($tmpArr);
		$idRefPops=0; $txtRefEliminar=1; $ocultarEliminar=0;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo,$ocultarEliminar);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
