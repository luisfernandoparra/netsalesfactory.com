<?php
@header('Content-Type: text/html; charset=utf-8');

class landingsSiteContent extends Base
{
	//Datos propios de la clase
	public $id;
	public $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEM�?S DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS
	public $linkmenu;
	private $root_local_path;

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'landings_data_contents';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->getAdminLevel();	// RETRIEVE CONSTANTS LEVELS USER
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->header_top_content=utf8_decode($a['header_top_content']);
//		$this->header_top_content=$a['header_top_content'];
//		$this->box_top_left=$a['box_top_left'];
		$this->box_top_left= utf8_decode($a['box_top_left']);

		$this->box_bottom_form=utf8_decode($a['box_bottom_form']);//utf8_decode
		$this->box_footer_text=utf8_decode($a['box_footer_text']);
		$this->site_landing_id=$a['site_landing_id'];
		$this->script_form_name=$a['script_form_name'];
		$this->mobile_auto_modal=$a['mobile_auto_modal'];
		$this->is_enabled=$a['is_enabled'];
		return;
	}

//******************************************************
//	ASIGNACION CAMPOS RECIBIDOS POR POST
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->mobile_auto_modal=$datos['mobile_auto_modal'];
		$this->is_enabled=$datos['is_enabled'];
		$this->header_top_content=htmlspecialchars_decode($datos['header_top_content']);
		$this->box_top_left=htmlspecialchars_decode($datos['box_top_left']);
		$this->box_bottom_form=htmlspecialchars_decode($datos['box_bottom_form']);
		$this->box_footer_text=htmlspecialchars_decode($datos['box_footer_text']);
		$this->site_landing_id=$datos['site_landing_id'];
		$this->script_form_name=$datos['script_form_name'];
		$this->guarda_datos();
	}

//******************************************************
//	GUARDAR EN BBDD DATOS
//******************************************************
	function guarda_datos()
	{
		$dato_box_top_left=str_replace("€",'&euro;',$this->box_top_left);
		$dato_box_top_left=trim(utf8_decode($dato_box_top_left));
		$dato_box_top_left=utf8_encode($dato_box_top_left);

		$dato_header_top_content=str_replace("€",'&euro;',$this->header_top_content);
		$dato_header_top_content=trim(utf8_decode($dato_header_top_content));
		$dato_header_top_content=utf8_encode($dato_header_top_content);

		$dato_box_bottom_form=str_replace("€",'&euro;',$this->box_bottom_form);
		$dato_box_bottom_form=utf8_decode($dato_box_bottom_form);
//		$dato_box_bottom_form=str_replace('\'','`',$dato_box_bottom_form);;
		$dato_box_bottom_form=utf8_encode($dato_box_bottom_form);

		$dato_box_footer_text=str_replace("€",'&euro;',$this->box_footer_text);
		$dato_box_footer_text=trim(utf8_decode($dato_box_footer_text));
//		$dato_box_footer_text=str_replace('\'','`',$dato_box_footer_text);
		$dato_box_footer_text=utf8_encode($dato_box_footer_text);

		$this->sql->query('SET NAMES utf8');
		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.'(id, header_top_content, box_top_left, box_bottom_form, box_footer_text, script_form_name, mobile_auto_modal, is_enabled, site_landing_id) VALUES ("'.(int)$this->id.'", \''.$dato_header_top_content.'\', \''.$dato_box_top_left.'\', \''.$dato_box_bottom_form.'\', \''.$dato_box_footer_text.'\', \''.trim($this->script_form_name).'\', '.(int)$this->mobile_auto_modal.', '.(int)$this->is_enabled.', "'.(int)$this->site_landing_id.'")';

//echo $query;die();echo'<hr><pre>';print_r($_REQUEST);print_r($this);die();
		$this->sql->query($query);
		return;
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$this->root_local_path='';
		$query='SELECT COUNT(c.id) + IFNULL(mlt.is_enabled,0) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS mlt LEFT JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'contenidos AS c ON c.id_niv_two=mlt.id WHERE mlt.id="'.(int)$this->id.'"';
		$existen_paginas=$this->sql->valor($query);
echo $query;die();
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function formulario_datos($nombreForm=0)
	{
		header('Content-Type: text/html; charset=utf-8');
		$dataPopline=str_replace("\r\n",'',($this->header_top_content));
		$dataPopline=utf8_encode($dataPopline);
		$dataPopline=htmlspecialchars_decode($dataPopline);
		$dataPopline2=str_replace("\r\n",'',($this->box_top_left));
		$dataPopline2=utf8_encode($dataPopline2);
		$dataPopline2=htmlspecialchars_decode($dataPopline2);
		$dataPopline3=str_replace("\r\n",'',($this->box_bottom_form));
		$dataPopline3=utf8_encode($dataPopline3);
		$dataPopline3=htmlspecialchars_decode($dataPopline3);
		$dataPopline4=str_replace("\r\n",'',($this->box_footer_text));
		$dataPopline4=utf8_encode($dataPopline4);
		$dataPopline4=htmlspecialchars_decode($dataPopline4);
//echo $dataPopline;
?>
<tr valign="top">
	<td align="right" class="tooltip" title="Landing a la que pertenecen los actuales contenidos">Nombre promo:</td>
	<td>
<?php
		$salida='';
		$sqlFilter=1;

		if(!$this->id)	// SE LIMITA LA LISTA A LANDING SIN CONTENIDOS YA EXISTENTES
			$sqlFilter.=' && lsc.id NOT IN (SELECT site_landing_id FROM '.$this->sql->db.'.'.$this->nombreTabla.')';

 		$query='SELECT DISTINCT(lsc.id), lsc.landing_name AS landingElement, csc.root_local_path FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_site_config AS lsc INNER JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'customers_site_config AS csc ON csc.client_id=lsc.client_id WHERE '.$sqlFilter.' ORDER BY lsc.landing_name';
		$res2=$this->sql->query($query);
//echo $query;
		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->site_landing_id == $arra['id'])
			{
				$salida.=' selected="selected"';
				$this->root_local_path=$arra['root_local_path'];
			}
			$salida.='>'.$arra['landingElement'].'</option>';
		}

		echo '<select id="site_landing_id" name="site_landing_id" style=\'width:125px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
	<td align="right" style="padding-top:3px!important;">
		<a title="Edición <b>Wysiwyg</b> para el contenido del <b>header superior</b> de la landing" class='btn ajax editAjax tooltip' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?>&site_landing_id=<?=(int)$this->site_landing_id?>&innerTextHeight=<?=$this->innerTextHeight?>&rootLocalPath=<?=$this->root_local_path?>&nombreCampo=data_header_top_content">Header top:</a>
		<span class="<?=$this->id ? '' : 'hide'?>"><br /><a title="Editar directamente el código <b>HTML</b>" class='btn ajax directHTML tooltip' href="<?=basename($_SERVER['PHP_SELF'])?>?id=<?=$this->id?>&accion=directHTML&fieldName=data_header_top_content">Html code:</a></span>
	</td>
	<td>
		<div id="data_header_top_content" class='editorPopline' contenteditable='true' style="display:block;margin-top:1px;border:1px solid #999;width:<?=$this->id ? '250px' : '200px';?>;height:<?=$this->id ? '150px' : '16px';?>;overflow:auto;"><?=$dataPopline?></div>
		<textarea id='header_top_content' name='header_top_content' style='display:none;' ><?=$this->data_header_top_content?></textarea>
	</td>

	<td align="right" style="padding-top:3px!important;">
		<a title="Edición <b>Wysiwyg</b> del contenido de la <b>caja a la izquierda del formulario</b> de la landing" class='btn ajax editAjax tooltip' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?>&site_landing_id=<?=(int)$this->site_landing_id?>&innerTextHeight=<?=$this->innerTextHeight?>&rootLocalPath=<?=$this->root_local_path?>&nombreCampo=data_box_top_left">Left box:</a>
		<span class="<?=$this->id ? '' : 'hide'?>"><br /><a title="Editar directamente el código <b>HTML</b>" class='btn ajax directHTML tooltip' href="<?=basename($_SERVER['PHP_SELF'])?>?id=<?=$this->id?>&accion=directHTML&fieldName=data_box_top_left">Html code:</a></span>
	</td>
	<td>
		<div id="data_box_top_left" class='editorPopline' contenteditable='true' style="display:block;margin-top:1px;border:1px solid #999;width:<?=$this->id ? '250px' : '200px';?>;height:<?=$this->id ? '150px' : '16px';?>;overflow:auto;"><?=$dataPopline2?></div>
		<textarea id='box_top_left' name='box_top_left' style='display:none;' ><?=$this->data_box_top_left?></textarea>
	</td>

	<td align="right" class="tooltip" title="El sitio es activo?">Habilitado:</td>
	<td width="80">
<?php
		echo '<select id="is_enabled" name="is_enabled" style="width:44px;">';
		echo '<option value=-1 ';
		if($this->is_enabled == -1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->is_enabled == 0 && isset($this->is_enabled))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->is_enabled == 1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>
</tr>

<tr valign="top">
	<td align="right" class="tooltip" title="Archivo a utilizar para los campos del formulario,<br />Si se utiliza la opción de `<i>Específico</i>`, se incluirá<br />el script terminado con el ID de la landing actual <br />(ej.: <i>form_data_<b>09</b></i>).">Form script:</td>
	<td>
<?php
		echo '<select id="script_form_name" name="script_form_name" style="width:125px">';

		echo '<option value=-1 ';
		if($this->script_form_name == -1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->script_form_name == 0 && isset($this->script_form_name))
			echo 'selected=\'selected\'';
		echo '>Standard</option>';

		echo '<option value=1 ';
		if($this->script_form_name == 1)
			echo 'selected=\'selected\'';
		echo '>Específico</option>';
		echo '</select>';
?>
</td>

	<td align="right" style="padding-top:3px!important;">
		<a title="Edición <b>Wysiwyg</b> para la <b>caja inferior del formulario</b> de usuario" class='btn ajax editAjax tooltip' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?>&site_landing_id=<?=(int)$this->site_landing_id?>&innerTextHeight=<?=$this->innerTextHeight?>&rootLocalPath=<?=$this->root_local_path?>&nombreCampo=data_box_bottom_form">Bottom form:</a>
		<span class="<?=$this->id ? '' : 'hide'?>"><br /><a title="Editar directamente el código <b>HTML</b>" class='btn ajax directHTML tooltip' href="<?=basename($_SERVER['PHP_SELF'])?>?id=<?=$this->id?>&accion=directHTML&fieldName=data_box_bottom_form">Html code:</a></span>
	</td>
	<td>
		<div id="data_box_bottom_form" class='editorPopline' contenteditable='true' style="display:block;margin-top:1px;border:1px solid #999;width:<?=$this->id ? '250px' : '200px';?>;height:<?=$this->id ? '150px' : '16px';?>;overflow:auto;"><?=$dataPopline3?></div>
		<textarea id='box_bottom_form' name='box_bottom_form' style='display:none;' ><?=$this->data_box_bottom_form?></textarea>
	</td>

	<td align="right" style="padding-top:3px!important;">
		<a title="Edición <b>Wysiwyg</b> para la <b>caja pie de página</b>" class='btn ajax editAjax tooltip' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?>&site_landing_id=<?=(int)$this->site_landing_id?>&innerTextHeight=<?=$this->innerTextHeight?>&rootLocalPath=<?=$this->root_local_path?>&nombreCampo=data_box_footer_text">Text footer:</a>
		<span class="<?=$this->id ? '' : 'hide'?>"><br /><a title="Editar directamente el código <b>HTML</b>" class='btn ajax directHTML tooltip' href="<?=basename($_SERVER['PHP_SELF'])?>?id=<?=$this->id?>&accion=directHTML&fieldName=data_box_footer_text">Html code:</a></span>
	</td>
	<td>
		<div id="data_box_footer_text" class='editorPopline' contenteditable='true' style="display:block;margin-top:1px;border:1px solid #999;width:<?=$this->id ? '250px' : '200px';?>;height:<?=$this->id ? '150px' : '16px';?>;overflow:auto;"><?=$dataPopline4?></div>
		<textarea id='box_footer_text' name='box_footer_text' style='display:none;' ><?=$this->data_box_footer_text?></textarea>
	</td>

	<td align="right" class="tooltip" title="Se debe cargar la modal automáticamente al abrir la landing?">Auto-modal:</td>
	<td width="80">
<?php
		echo '<select id="mobile_auto_modal" name="mobile_auto_modal" style="width:44px;">';
		echo '<option value=-1 ';
		if($this->mobile_auto_modal == -1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->mobile_auto_modal == 0 && isset($this->mobile_auto_modal))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->mobile_auto_modal == 1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>
</tr>

<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--

$(document).ready(function(){
	//editor jQuery color box
	$(".editAjax").colorbox({
		opacity: 0.4,
		initialHeight:"10px",
		initialWidth:"10%",
		close:true,
		overlayClose:true,
		width:"<?=$this->id ? 94 : 74 ?>%",
		height:"100%",
		iframe:true,
		html:true,
		innerHeight:true
	});

	if("<?=$this->id?>")
	{
		var previewData = $("#btnSaveRecord").parent();
		$(previewData).prepend("<button id='btnPreview' class='btn tooltip' onclick='return false;' title='visualiza el contenido actualmente almacenado en la BBDD' >&nbsp;Visualizar contenido <b>en BBDD</b>&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;");
	}

	$("#btnPreview").click(function(){
		window.open("../?cr=<?=$this->site_landing_id?>","test","");
	});
});

function revisa()
{
	if($('#site_landing_id').val() < 0)
	{
		displayFormError("#site_landing_id","Por favor, seleccione una landing");
		return false;
	}
	if($.trim($('#is_enabled').val()) < 0)
	{
		displayFormError("#is_enabled","Por favor, selecciona si está activa");
		$('#is_enabled').val("");
		return;
	}
	if($.trim($('#script_form_name').val()) < 0)
	{
		displayFormError("#script_form_name","Por favor, selecciona el tipo de formulario a incluir");
		$('#script_form_name').val("");
		return;
	}
	if($.trim($('#mobile_auto_modal').val()) < 0)
	{
		displayFormError("#mobile_auto_modal","Por favor, indica si se debe abrir la modal al carcar la landing automaticamente");
		$('#mobile_auto_modal').val("");
		return;
	}
	popeditorSaveData("f<?=$idunico?>");
}

function resaltarCasilla(id,xx,zz)	// ABRIR LANDIN ALMACENADA EN BBDD
{
	var tmpElem=$("#"+id).parent();
	var tmpObj=$(tmpElem[0]).children();

	$(tmpObj).each(function(tdPos,tdContent){
		if(tdPos == 1)//POSICION DEL TD CON EL ID DE LA LANDING
		{
			window.open("../?cr="+$(tdContent).html(),"test","");
			return false;
		}
	});
}

-->
</script>
<?php
		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$idunico=rand(1,10000).time();
		//$query='SELECT lab.*,u.nivel_acceso FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 ';
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		//$querytotal='SELECT COUNT(lab.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 '.$filtroSql;
		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='site_landing_id'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;

		$num_res=$this->sql->valor($querytotal);

		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $query;

//******************************************************


//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
var idVisualizado=-1;
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->


<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='878' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='12' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td title='Buscar por <b>landing</b> conocida' width="60" align="right" class='tooltip'>Landing:</td>
	<td>
<?php
 		$query='SELECT lsc.id, lsc.landing_name AS landingElement FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_site_config AS lsc INNER JOIN '.$this->sql->db.'.'.$this->nombreTabla.' AS ldc ON lsc.id = ldc.site_landing_id WHERE 1 ORDER BY lsc.landing_name';
//&& lsc.id=\''.$_SESSION['filtros'][$this->posicionSolapa.'site_landing_id_buscN'].'\'
//		$query='SELECT lc.id_landing, lc.prefix FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc WHERE lc.is_enabled ORDER BY lc.prefix';
		$res2=$this->sql->query($query);

		$salida='<select id="'.$this->posicionSolapa.'site_landing_id_buscN" name="'.$this->posicionSolapa.'site_landing_id_buscN" style=\'width:128px;\'>';

		$salida.='<option value=-1 ';
		if($_SESSION['filtros'][$this->posicionSolapa.'site_landing_id_buscN'] == -1)
			$salida.='selected=\'selected\'';
		$salida.='>?</option>';

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';

			if(isset($_SESSION['filtros'][$this->posicionSolapa.'site_landing_id_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'site_landing_id_buscN'] == $arra['id'])
				$salida.=' selected="selected"';

			$salida.='>'.$arra['landingElement'].'</option>';
		}

		$salida.='</select>';
		echo $salida;
?>
	</td>

	<td width="58" align="right">Header top:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>header_top_content_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'header_top_content_busca_u']?>' style='width:80px' /></td>

	<td width="78" align="right">Caja izquierda:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>box_top_left_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'box_top_left_busca_u']?>' style='width:80px' /></td>

	<td title='' align="right">Activo:</td>
	<td title=''>
<?php
		$esActivo=array(0=>'NO',1=>'S&iacute;');
		echo '<select id="'.$this->posicionSolapa.'is_enabled_buscN" name="'.$this->posicionSolapa.'is_enabled_buscN" style=\'width:50px;\' title="Nivel de acceso del usuario al Back-office">';
		echo '<option value=-1 ';
		if($this->is_enabled==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		for($n=0;$n < 2;$n++)
		{
			echo '<option value='.$n.' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']==$n && isset($_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']))
				echo 'selected=\'selected\'';
			echo ' >'.$esActivo[$n].'</option>';
		}
		echo '</select>';
?>
	</td>

<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->


<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		//$this->javascripts->javascriptFunctions($idunico,$destinobusca,'80%',$this->modalHeight,'75%','12%');
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,$this->modalHeight,'',$this->modalWidth,$this->modalLeftPos);

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='ID promo';
		$arraTitulares[]='Cliente';
		$arraTitulares[]='Nombre promo';
		$arraTitulares[]='Tipo formulario datos';
		$arraTitulares[]='Auto-modal';
		$arraTitulares[]='Activo';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='site_landing_id';
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='';
		$arraCamposOrdenar[]='site_landing_id';
		$arraCamposOrdenar[]='script_form_name';
		$arraCamposOrdenar[]='mobile_auto_modal';
		$arraCamposOrdenar[]='is_enabled';

		 // no se incluye el valor para el ID
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=50;
		$arraLongitudes[]=14;
		$arraLongitudes[]=50;
		$arraLongitudes[]=50;
		$arraLongitudes[]=62;
		$arraLongitudes[]=60;
		$arraLongitudes[]=60;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=25;	// id
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=90;
		$arraLongitudesTitulares[]=50;
		$arraLongitudesTitulares[]=180;
		$arraLongitudesTitulares[]=250;
		$arraLongitudesTitulares[]=115;
		$arraLongitudesTitulares[]=65;
		$arraLongitudesTitulares[]=40;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='rop';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;
		$conutRowsDisplayed=0;
//		$selectLabelLevelOne=$_SESSION['forcedLangSite'] ? 'mlo.landing_identifier AS nameMenu' : 'CONCAT(mlo.landing_identifier," (",i.prefijo,") AS nameMenu")';

		while($a=$this->sql->fila2($res))
		{
		  $rop=null;
			//	SOLO SE PERMITEN VER LOS ELEMENTOS PROPIOS Y LOS GLOBALES (EXCEPTO PARA GOOD)
		  if($_SESSION['id_usuario'] != $a['id_usuario'] && $a['id_usuario'] != 0 && $this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])
				$rop=1; // fila deshabilitada

			$xc=0; $estaActivo='<center style=color:orange>no</center>';
			$query='SELECT lc.prefix,lsc.landing_name FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_site_config AS lsc INNER JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc ON lc.id_landing=lsc.client_id  WHERE lsc.id='.(int)$a['site_landing_id'];
			$customerDetails=$this->sql->query($query);
			$dataCustomer=$this->sql->fila2($customerDetails);
			$elscript_form_name='?';
			if($a['is_enabled'] == -1) $esActivo='?';
			if(!$a['is_enabled']) $esActivo='<span style=color:red>NO</span>';
			if($a['is_enabled'] == 1) $esActivo='SI';

			if($a['mobile_auto_modal'] == -1) $isAutoLoad='?';
			if(!$a['mobile_auto_modal']) $isAutoLoad='<span style=;>NO</span>';
			if($a['mobile_auto_modal'] == 1) $isAutoLoad='<span style=color:'.$_SESSION['colorTextHighli'].';>SI</span>';

			if($a['script_form_name'] == 0) $elscript_form_name='<span style=color:;>Standard</span>';
			if($a['script_form_name'] == 1) $elscript_form_name='<span style=color:'.$_SESSION['colorTextWarning'].';>Específico</span>';
			$query='SELECT CONCAT(nombre," ",apellidos) AS theName FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE id="'.$a['id_usuario'].'"';
//			$elIdioma=utf8_encode($this->sql->valor($query));
			$elIdioma=$elIdioma?$elIdioma:'<span style=color:red;><b>Todos</b></span>';

			if($a['is_enabled']) $estaActivo='<center style=color:lightgreen><b>SI</b></center>';

			$tmpArr[0]=$a['id'];
			array_push($tmpArr,$a['site_landing_id']);
			array_push($tmpArr,$dataCustomer['prefix']);
			array_push($tmpArr,$dataCustomer['landing_name']);
//			array_push($tmpArr,$a['landing_identifier']);
			array_push($tmpArr,$elscript_form_name);
			array_push($tmpArr,$isAutoLoad);
			array_push($tmpArr,$esActivo);
		  array_push($tmpArr,$rop);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
			$conutRowsDisplayed++;
		}

		$idRefPops=0;
		$txtRefEliminar=1;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
