<?php
class technicalOffice extends Base
{
	//Datos propios de la clase
	var $id;
	var $accion;
	var $nombreTabla;
	var $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'logs_events_'.date('Y');
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las búsquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset']==1) unset($_SESSION['filtros']);
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->accion=$a['accion'];
		$this->fecha=$a['fecha'];
		$this->ip_usuario=$a['ip_usuario'];
		$this->id_usuario=$a['id_usuario'];
		return;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->accion=$datos['accion'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens)
	{
		$mens.='</b>';

		$query='SELECT lab.* FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID
		//$filtroSql.=' && u.nivel_acceso <='.$_SESSION['usuarioNivel'].' ';	// FILTRO NIVEL USUARIOS
		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(lab.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab WHERE 1 '.$filtroSql;

		$ordenacion='lab.fecha, lab.ora'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
//echo $querytotal;

		if($_SESSION['filtros']['visitantes_distintos_busca'])
		{
			$res2=$this->sql->query($querytotal);
			$num_res=mysql_num_rows($res2);
		}
		else
			$num_res=$this->sql->valor($querytotal);

		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $query;
//******************************************************
//Formulario para efectuar búsquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<form action='<?=$destinobusca?>.php' method='post' name='formListados'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='934' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='4'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td>Usuario:</td>
	<td>
<?php
		$query='SELECT id, CONCAT(nombre," ",apellidos) AS nombreU FROM '.$this->sql->db.'.usuarios WHERE 1 ORDER BY nombre';
		$res2=$this->sql->query($query);

		echo '<select id=\''.$this->posicionSolapa.'id_usuario_buscN\' name=\''.$this->posicionSolapa.'id_usuario_buscN\' style=\'width:120px\' ><option value=\'-1\'>seleccionar</option>';
		while ($arra=$this->sql->fila2($res2))
		{
			echo '<option value=\''.$arra['id'].'\' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN']==$arra['id'])
				echo 'selected=\'selected\'';
			echo '>'.utf8_encode($arra['nombreU']).'</option>';
		}
		echo '</select>';
?>
	</td>
	<td>Ip usuario:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>ip_usuario_busca' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'ip_usuario_busca']?>'  style='width:70px' /></td>
	<td align='right' title='Filtro por fechas'>Fecha:</td>
	<td style='' title='Filtro por fechas'>
<input type='text' id="<?=$this->posicionSolapa?>fecha_busca" name="<?=$this->posicionSolapa?>fecha_busca" value='<?=$_SESSION['filtros'][$this->posicionSolapa.'fecha_busca']?>' style='width:80px' class="tcal" />
	</td>
	<td width='30'>Acci&oacute;n:</td>
	<td>
<input type='text' name='<?=$this->posicionSolapa?>accion_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'accion_busca_u']?>' style='width:50px'>
	</td>
	<td width='32'>ID ref.:</td>
	<td>
<input type='text' name='<?=$this->posicionSolapa?>id_elemento_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'id_elemento_busca_u']?>' style='width:50px'>
	</td>
	<td align='right'>Script:</td>
	<td>
<input type='text' name='<?=$this->posicionSolapa?>formulario_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'formulario_busca_u']?>' style='width:50px'>
	</td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>

<script language="JavaScript" type="text/javascript">
<!--
function modifica(){return;}
-->
</script>
<!-- *******************  fin formulario búsquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'75%','','80%','10%',$this->editarFila);

		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('Usuario','IP','Fecha','Hora','Acci&oacute;n','ID Ref.','Script');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id','id_usuario','ip_usuario','fecha','ora','accion','id_elemento','formulario');
		$xc=0; $arraLongitudes=array($xc++=>30,$xc++=>30,$xc++=>25,$xc++=>25,$xc++=>120,$xc++=>30,$xc++=>30); // no se incluye el valor para el ID
		$xc=0; $arraLongitudesTitulares=array($xc++=>45,$xc++=>150,$xc++=>70,$xc++=>60,$xc++=>50,$xc++=>290,$xc++=>50,$xc++=>180);
		$xc=0; $arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'num',$xc++=>'fec',$xc++=>'num',$xc++=>'txt',$xc++=>'num',$xc++=>'txt');
		$verEmergentesaccionsFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			if(!$a['id_usuario']) $usuario='visitante';

			$queryPersonas='SELECT CONCAT(nombre," ",apellidos) FROM '.$this->sql->db.'.usuarios WHERE id='.$a['id_usuario'];
			$usuario=''.$this->sql->valor($queryPersonas);
			$xc=0;
			$laAccion=$a['accion'];
			if(!$laAccion) $laAccion='<span style=color:#999>consultar</span>';
			
			$arraDatos[]=array($xc++=>$a['id'],$xc++=>  utf8_encode($usuario),$xc++=>$a['ip_usuario'],$xc++=>$a['fecha'],$xc++=>$a['ora'],$xc++=>$laAccion,$xc++=>$a['id_elemento'],$xc++=>$a['formulario']);
		}

		$idRefPops=0; $txtRefEliminar=2; $ocultarEliminar=1;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo,$ocultarEliminar);
		unset($laAccion);
		/*******************  FIN DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>