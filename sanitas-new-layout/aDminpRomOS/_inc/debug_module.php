<?php
@session_start();
include_once('_cnf/sesion.php');
include_once('_cnf/colores.php');
include_once('_cnf/sesion.php');
include_once('_cnf/connect2.php');
require_once('_inc/debug_module.class.php');

$objeto=new debugLogica($sql,$_SESSION['id_usuario']);
$objeto->ObtenerNavegador();
?>
<style type="text/css">
<!--
<?php
include('_css/estilos_dinamicos.php');
?>
#divCajaDebug{
padding:8px 5px 0pt 50px;
}

#divDebugIframes{
display:none;
float:right;
width:364px;
height:400px;
background:url(_img/fnd_debug_layer.png) no-repeat !important;
right:-334px;
padding:0;
position:fixed;
top:0px;
z-index:1002;
border-radius:10px;
-moz-border-radius:10px;
-webkit-border-radius:10px;
-moz-box-shadow:50px 4px 4px #333;-webkit-box-shadow:50px 5px 5px #333;
}
div.debugContenidos{
padding-left:2px;
margin-top:2px;
margin-left:-8px;
width:318px;
height:380px;
background-color:#fff;
overflow:auto;
border-radius:10px;
-moz-border-radius:10px;
-webkit-border-radius:10px;
}

#titularDebug{
font-size:12px !important;
display:block;position:absolute;
color:#eee;
top:86px;left:-56px;
width:148px;
writing-mode:tb-rl;
-webkit-transform:rotate(90deg);	
-moz-transform:rotate(90deg);
-ms-transform:rotate(90deg);
-o-transform:rotate(90deg);
transform:rotate(90deg);
}
-->
</style>

<script type="text/javascript">
<!--
<?php
echo 'var elNavegador="'.$objeto->nameNavegador.'";';
?>
/*
var userAgent=navigator.userAgent.toLowerCase();jQuery.browser={version:(userAgent.match( /.+(?:rv|it|ra|ie|me)[\/: ]([\d.]+)/ ) || [])[1],chrome: /chrome/.test( userAgent ),safari: /webkit/.test( userAgent ) && !/chrome/.test( userAgent ),opera: /opera/.test( userAgent ),msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),mozilla: /mozilla/.test( userAgent )&&!/(compatible|webkit)/.test( userAgent)};
*/

jQuery(function()
{

	if(elNavegador == "msie")
//	if($.browser.msie)
	{
		if($.browser.version<7)	// SE DESHABILITA EL DIV FLOTANTE PARA VERSIONES DE EXPLORER INFERIORES A LA 7
		{
			$("#divDebugIframes").hide();return false;
		}
		$("#titularDebug").css("left","-115px");
		$("#titularDebug").css("top","16px");
	}

	jQuery("#divDebugIframes").hover(function(){
		jQuery("#divDebugIframes").stop(true,false).animate({right:"1"},"medium");
	},
		function(){
			jQuery("#divDebugIframes").stop(true,false).animate({right:"-334"},"medium");},200);
		return false;
});

$(document).ready(function(){
	setTimeout("$('#divDebugIframes').show(200);",300);
});

-->
</script> 

<?php
//unset($_SESSION['debugSession']);
if($_SESSION['debugBackOffice'])	// DEBUG FLOATING INNER WINDOW (mf.2011)
{
	$titularDebug=(!$_SESSION['debugSession'] && !$_SESSION['debugRequest'] && !$_SESSION['debugObjetos'])?'<span style=color:red;background-color:#fff;>&nbsp;Debug no definido!&nbsp;</span>':'Debug: ';
	if($_SESSION['debugRequest']) {$titularDebug.='<b>REQU.</b>&nbsp;';$verElemDebug++;}
	if($_SESSION['debugSession']) {$titularDebug.='<b>SESS.</b>&nbsp;';$verElemDebug++;}
	if($_SESSION['debugObjetos']) {$titularDebug.='<b>OBJS.</b>&nbsp;';$verElemDebug++;}

	echo'<div id="divDebugIframes"><div id="titularDebug">'.$titularDebug.'</div>
<div id="divCajaDebug"><div class="debugContenidos">';
	if(!$verElemDebug)
		echo '<div style=padding:10px;><b>NO se definieron elementos para ser visualizados en esta pantalla.</b><br><br><br>Si se desean visualizar variables din&aacute;micamnete, debe cambiar las preferencias de la configuraci&oacute;n del back-office de <i>'.$_SESSION['nombre_site'].'</i>.</div>';
	echo'<pre style="font-family:Verdana, Geneva, sans-serif;font-size:10px;">';
	if($_SESSION['debugRequest']) {echo'<i>request:</i>';print_r($_REQUEST);}
	if($_SESSION['debugSession']) {echo'<i>session:</i>';print_r($_SESSION);}
	if($_SESSION['debugObjetos']) {echo'';print_r($objeto);}
	echo'</pre>';
	echo'</div></div></div>';
}
?>