<?php
/*
 * EJECUCION DE GENERAL DE ALGUNAS FUNCIONES PARA TODOS LOS FORMULARIOS DEL BACK-OFFICE, DETERMINADOS POR "$_REQUEST['accion']"
 * @accion = 1 INSERCION DE UN NUEVO RECORD
 * @accion = 2 BACK EDICION PAGINA PADRE
 * @accion = 8 FORMULARIO PARA LA EDICION DE REGISTRO
 */
$msg=$_REQUEST['msg']?$_REQUEST['msg']:'';

//------------------------------------------------------------------------------------------------------------
//	PLUG-IN BORRADO MASIVO DE REGISTROS (v.11.2011.1A-2/2)
//	START ACCION PARA ELIMINAR TODOS LOS REGISTROS SELECCIONADOS POR LOS FILTROS PARA LA TABLA PRINCIPAL ACTIVA
//	NOTA IMPORTANTE: si el módulo posee filtros de fechas, necesario utilizar el calendario en un unico campo
//echo '<pre>';print_r($_SESSION);die();
if($_REQUEST['borrarFiltrados'])
{
	if($_REQUEST['borrarFiltrados']>1) $plurBorrando='s';
	echo '<center><div id="msgEliminacion" style="display:block;width:300px;margin-top:5px;margin-bottom:5px;padding-left:20px;padding-right:20px;text-align:center;font-size:1.2em;color:red;background-color:#fff"><i>Borrando</i> <b>'.$_REQUEST['borrarFiltrados'].'</b> <i>registro'.$plurBorrando.'</i>...</div></center>';

	require_once('_inc/delete.filters.class.php');
	$objeto->logicaExtendida=new deleteFilters($sql,$objeto->nombreTabla,$id_pagina);
	$resElim=$objeto->logicaExtendida->elimiarRegistrosFiltrados();

	if($resElim)
	{
		echo '<script>$(function(){$("#tblFilas tr:not(:first)").remove().delay(1000,function(){$("#msgEliminacion").hide();});$("#tblFilas tr:last").after("<tr><td colspan=\'10\' style=\'background-color:'.$_SESSION['colorFondoWeb'].';\'><h2>Se eliminaron: <b><span style=\'color:'.$_SESSION['colorOverImportant'].';\'>'.$_REQUEST['borrarFiltrados'].' registro'.$plurBorrando.'</span></b></h2><br /><br />Para saber si a&uacute;n quedan registros en este m&oacute;dulo, haga click en `Reset` o efect&uacute;e una selecci&oacute;n distinta con los filtros presentes en la pantalla.<br /><br /><br /><br /></td></tr>");$("#tblFilas tr:first").css("visibility","hidden")});</script>';
		$_REQUEST['logEstado']=$_REQUEST['borrarFiltrados'].' records deleted<br /><br />';
	}
	else
	{
		echo '<script>$(function(){$("#tblFilas tr").remove();$("#spnBtnElimiFilter").hide();$("#msgEliminacion").html("Ha ocurrido un error al intentar eliminar los registros seleccionados!<br />(<b>'.$_REQUEST['borrarFiltrados'].'</b>)<br /><br /><span style=font-size:.8em;>Si este error se repite reiteradamente, por favor,<br />p&oacute;ngase en contacto con el responsable de la Web.</span>");});</script>';
		$_REQUEST['logEstado']='ERROR on record delete';
	}
	$_REQUEST['id_grabar']='';
}
// END ACCION PARA ELIMINAR TODOS LOS REGISTROS SELECCIONADOS POR LOS FILTROS PARA LA TABLA PRINCIPAL ACTIVA
//----------------------------------------------------------------------------------------------------------

if($_REQUEST['accion'] == 8)	// FORMULARIO PARA LA EDICION DE REGISTRO
{
	require('_inc/common_functions.php');	// FUNCIONES COMUNES DE TODAS LAS CLASES
	$objeto=new commonFunctionsAdmin($sql, $admin);
	$_REQUEST['logEstado']=$txtLogEdit;
	$_REQUEST['id_grabar']=$_REQUEST['id'];
	if($objeto->posicionSolapa)	$_REQUEST['posicionSolapa']=$objeto->posicionSolapa;

	new Log($sql, $id_pagina);
	$objeto->form_editar_flotante($nombreFilePhpBase.'.php',$colPaginacionActivoFondo,$colBorde,$txtTitularEdicion,$varsUrl['query']);
	echo '</body></html>';
	die();
}

if($_REQUEST['accion'] == 1)	// INSERCION DE UN NUEVO RECORD EN BBDD, PROCESO DE LOS DATOS RECIBIDOS DESDE EL FORMULARIO
{
	$_REQUEST['logEstado']=$txtLogInsert;
	$objeto->form_editar_datos_procesa($_REQUEST);
	echo '<script>document.location.href="'.$nombreFilePhpBase.'.php?posicionSolapa='.$_REQUEST['posicionSolapa'].'&operacionEfectuada='.$_REQUEST['accion'].'&msg='.$msg.'";</script>';
	die();
}

if($_REQUEST['accion'] == 2)
{
	$msg=$txtMsgREcordEditado;
}
?>
</head>
<body class="fondoPagina" id="bodyModulo">

<?php
if(!$objeto->omitir_insercion)	// SE OMITE EL FORMULARIO DE INSERCION DE NUEVO REGISTRO SI EXISTE ESTE FLAG
	$objeto->form_editar_datos($nombreFilePhpBase.'.php','1',$txtTitularFormularioNewRecord);

$objeto->listar_datos($nombreFilePhpBase,$nombreFilePhpBase.'_editar.php',$colPaginacionActivoFondo,$colBorde,$txtTitularBuscador,$txtMsgStandard);

if(!$_REQUEST['logEstado']) $_REQUEST['logEstado']=$txtLogList;

include('_inc/help_button.class.php');

echo '
<script language="JavaScript" type="text/JavaScript">var temp=document.getElementById("mesajes_formulario").innerHTML;';
if($msg)
	echo 'document.getElementById("mesajes_formulario").innerHTML="'.$msg.'";';
echo '
setTimeout("document.getElementById(\'mesajes_formulario\').innerHTML=temp;",4000);</script>
';

if($objeto->permiteEliminarGlobal)	// ULTIMO CONTROL/AVISO PARA ELIMINACION DE REGISTROS UTILIZANDO EL FILTRO ACTIVO
{
	echo '<script type="text/javascript">
<!--
function eliminarRegistrosFiltrados(el,idUnico)
{
	var objElem=document.getElementById("divExtraFields");

	if(!el)
	{
		alert("No se encontraron registros para eliminar!\n\nPor favor, revise sus filtros...");
		return;
	}
	nombreFormulario="formListados";

	if(idUnico)
		nombreFormulario="formListados"+idUnico;

	if(confirm("CUIDADO!!!! se van a aliminar "+el+" registros\n\n Han sido seleccionados con los filtros utilizados actualmente...\n desea realmente ELIMINAR estos registros?"))
	{
		document.getElementById("spnBtnElimiFilter").innerHTML="<span style=padding-left:2px;padding-right:2px;color:red;background-color:#fff;>Ejecutando</span>";
		objElem.innerHTML="<input name=\'borrarFiltrados\' type=\'hidden\' value=\'"+el+"\' />";
		document[nombreFormulario].submit();
	}
	return;
}
-->
</script>
';
	unset($objeto->permiteEliminarGlobal);
}

?>
<script>
<!--
$(function(){
	if(typeof repaginarFrame!='undefined')
	{
		numModulos=top.frames.length;

		for(n=0;n<numModulos;n++)
		{
			nombreScript=eval(top.frames[n].location)+"...";
			if(nombreScript.indexOf(repaginarFrame)>0)
				top.posicFramAll=n;
		}
		paginarBloque=1;
		repaginarFrame='undefined';
	}

	top.$("#divNotasBottom").fadeIn(2000);

});

-->
</script>
<script type='text/javascript' src='_js/post.js'></script>
</body>
</html>
<?php
include_once('debug_module.php');
@new Log($sql,$id_pagina);
//echo'<pre>';print_r($_SESSION['usuarioNivel']);
?>