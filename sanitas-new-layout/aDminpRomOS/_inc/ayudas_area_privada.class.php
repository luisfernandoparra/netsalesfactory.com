<?php
class ayudasAreaPrivada extends Base
{
	//Datos propios de la clase
	var $id;
	var $id_area;
	var $nombreTabla;
	var $posicionSolapa;	// POSICI�N DE LA VENTANA RESPECTO A LAS DEM�S DE SU MISMA SECCI�N E IDENTIFICADOR PARA LOS CAMPOS DE LAS B�SQUEDAS

//******************************************************
//	INICIALIZACI�N DE LA CLASE
//******************************************************
	function __construct($sql)
	{
		$this->sql=$sql;
		$this->nombreTabla='ayudas_area_privada';
		$this->crearVariablesBusquedas($this->posicionSolapa);	// se crean las variables para las b�squedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset']==1) unset($_SESSION['filtros']);
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$this->carga_datos();
	}

//******************************************************
//
//******************************************************
	function carga_datos()
	{
		$query='SELECT * FROM '.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->id_area=$a['id_area'];
		$this->texto=$this->output_datos($a['texto']);
		return;
	}

//******************************************************
//	ELIMINACI�N DE REGISTROS CON CONTROL DE DATOS RELACIONADOS DE OTRAS TABLAS AL MISMO
//******************************************************
	function eliminar_registro()
	{
		$query="DELETE FROM ".$this->nombreTabla." WHERE id='".$this->id."'";
		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);
//echo $query.'<pre>'; print_r($_REQUEST);print_r($x);exit;
		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		$this->verificarDuplicados($this->id_area, $this->nombreTabla, 'id_area', 'id_nivel_acceso',1);	// VERIFICACI�N DE DATO DUPLICADO

		if(!$this->id) $this->id=0; // NECESARIO PARA A�ADIR NUEVOS REGISTROS

		$query="REPLACE INTO ".$this->nombreTabla." (id,id_area,texto) VALUES ('".$this->id."', '".$this->id_area."', '".str_replace("\r\n",'',$this->texto)."')";
//echo $query.'<pre>'; print_r($_REQUEST);print_r($x);exit;
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos($nombreForm)
	{
		$this->PhpEditarHTML($this->nombreTabla,$nombreForm,$this->id);
?>
<tr valign="top">
	<td>�rea privada:</td>
	<td>
<?php
		$query='SELECT * FROM documentos_areas WHERE 1 ORDER BY orden';
		$res=$this->sql->query($query);

		echo '<select id=\'id_area\' name=\'id_area\' style=\'width:160px\' ><option value="-1" class="tx10">seleccionar</option>';
		while($arra=$this->sql->fila2($res))
		{
			echo '<option value=\''.$arra['id'].'\' ';
			if($this->id_area==$arra['id'])
				echo 'selected=\'selected\'';
			echo '>'.$arra['nombre_area_docs'].'</option>';
		}
		echo '</select>&nbsp;</td>';

?>
  </td>
	<td align="right">Reglas:<br /><a id="btnEditHtml" href="#null" onclick="editarHTML('texto')" class="btn" style="width:100%" title="Editar con estilos HTML">html</a></td>
	<td><textarea name='texto' style='width:500px'><?=str_replace("\r\n",'',$this->texto)?></textarea></td>
</tr>
<?php
		if($this->id) echo '<tr><td height="25"></td></tr>';
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
//alert(parent.document.frames[0].document.location);
function revisa()
{
	if (document.f<?=$idunico?>.id_area.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.id_area);
		alert('Por favor, seleccione el �rea de la ayuda');
		document.f<?=$idunico?>.id_area.focus();
		restauraCampo(document.f<?=$idunico?>.id_area);
		return;
	}
	document.f<?=$idunico?>.submit();
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}
-->
</script>
<form action='<?=$destino?>' method='post' name='f<?=$idunico?>' enctype='multipart/form-data' onsubmit='alert("Por favor, para validar el formulario, pulse el bot�n [Guardar datos]");return false'>
<input type='hidden' name='accion' value='<?=$accion?>'>
<input type='hidden' name='id' value='<?=$this->id?>'>
<table align='center' cellpadding='3' cellspacing='0' class='borde' border='0'>
<tr>
	<td colspan='12' class='enc2'><?=$titulo?></td>
</tr>
<?php
		if($this->id)
			echo '<script>document.body.style.border="0px";</script><tr><td height="5"></td></tr>';
		$this->formulario_datos('f'.$idunico);
?>
<tr>	<td colspan='12' align='center' height="10" valign="bottom"><input type='button' class='boton' value='  Guardar datos  ' onclick='revisa()'></td>
</tr>
</table>
</form>
<?php
	return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->id_area=$datos['id_area'];
		$this->texto=$datos['texto'];
		$this->guarda_datos();
		return;
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111')
	{
		$mens='Niveles de acceso presentes: </b>';
		if (count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b�squeda:</span> ";

		$query='SELECT * FROM '.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(*) FROM '.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='id_area'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';

//******************************************************
//Formulario para efectuar b�squedas
//******************************************************
?>
<form action='<?=$destinobusca?>' method='post' name='formListados'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='500'>
<thead>
<tr>
	<td class='enc2' colspan='2'>Buscar estados de solicitudes</td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal' id='mesajes_formulario'><?=$mens?></td>
</tr>
</thead>
<tr>
	<td align="right" title="Esta ayuda se mostrar� para el �rea privada seleccionada">�rea:</td>
	<td title="Esta entrada de sub-men� es hija de la p�gina">
<?php

		$query='SELECT id,nombre_area_docs FROM documentos_areas WHERE en_menu>0 AND id<6 ORDER BY nombre_area_docs';
		$res2=$this->sql->query($query);

		echo '<select id=\''.$this->posicionSolapa.'id_area_buscN\' name=\''.$this->posicionSolapa.'id_area_buscN\' style=\'width:150px\' ><option value="-1" class="tx10">seleccionar</option>';

		while($arra2=$this->sql->fila2($res2))
		{
			echo '<option value=\''.$arra2['id'].'\' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_area_buscN']==$arra2['id'])
				echo 'selected=\'selected\'';
			echo '>'.$arra2['nombre_area_docs'].'</option>';
		}
		echo '</select>&nbsp;</td>';

?>
	</td>
	<td width='20'><input type='submit' class='boton' value='   Buscar  '></td>
	<td width='20'><input type='button' class='boton' value=' Reset ' style='color:orange' onClick="document.location='<?=$destinobusca?>?reset=1'"></td>
</tr>
</table>

<script language="JavaScript" type="text/javascript">
function modifica(id)
{
	var a=screen.width;
	var b=screen.height;
	var ancho=870;
	var alto=170;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var t=window.open ('<?=$destinoedita?>?id='+id+'&accion=2','edicion','width='+ancho+',height='+alto+',left='+x+',top='+y+'');
	t.focus();
}

function eliminar(id,nombre)
{
	if(confirm('�Desea realmente la ayuda para el �rea privada "'+nombre+'"?'))
	{
		var a=screen.width;
		var b=screen.height;
		var ancho=300;
		var alto=140;
		var x=(a-ancho)/2;
		var y=(b-alto)/2;
		var t=window.open ('<?=substr($destinobusca,0,strpos($destinobusca,'.php'))?>_eliminar.php?id='+id+'&accion=3','eliminar','width='+ancho+',height='+alto+',left='+x+',top='+y+'');
		t.focus();
		return;
	}
}

function recargarPaginaOrdenada(campo)	// ORDENACI�N DEL LISTADO DE RESULATDOS
{
	// SE RECARGA LA P�GINA CON LOS VALORES CORRIENTES ORDENANDO POR EL NUEVO CAMPO SELECCIONADO
	document.location='<?=$destinobusca?>?reset=<?=$_REQUEST['estado.php']?>&offset_u=<?=$_REQUEST['offset_u']?>&ordenarPor='+campo+'&sentidoOrdenacion='+document.formListados.sentidoOrdenacion.value;
	return;
}
</script>
<!-- *******************  fin formulario b�squedas  *************** !-->
<?php
		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('Nombre p�gina','texto');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id','id_area');
		$xc=0; $arraLongitudes=array($xc++=>30,$xc++=>90); // no se incluye el valor para el ID
		$xc=0; $arraLongitudesTitulares=array($xc++=>30,$xc++=>220,$xc++=>520);
		$xc=0; $arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'txt');
		$this->verIdRegistros=0;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$xc=0;
			$query='SELECT nombre_area_docs FROM documentos_areas WHERE id='.$a['id_area'];
			$id_areaListado=$this->sql->valor($query);
			$contenido=str_replace('"','`',nl2br($a['texto']));
			$arraDatos[]=array($xc++=>$a['id'],$xc++=>$id_areaListado,$xc++=>$contenido);
		}

		$idRefPops=0;
		$txtRefEliminar=1;
		$posicFondo=0;	// N�MERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  FIN DEL LISTADO  ********************/

		if($num_res>mysql_num_rows($res) && !$this->contFilas)
		{
			foreach($_SESSION['filtros'] as $key=>$value)	// SE RECARGA LA PAGINA SI NO HAY FILAS EN PANTALLA Y HAY RESULTADOS EN LA BUSQUEDA
			{
				$pos=strpos($key,'_busca');
				if ($pos>0)
					$url.='&'.$key.'='.$value;
			}
			$_SESSION['filtros']['offset_u']=0;
			echo "<script language='JavaScript' type='text/JavaScript'>
			document.location='$destinobusca?$url';
			</script>";
		}
?>
</table>
<table align='center' cellpadding='2'>
<tr><td style='line-height:22px' align='center'><span id='spn_infoPage'></span>
<?php
	$url='';
	foreach($_SESSION['filtros'] as $key=>$value)
	{
		$pos=strpos($key,'_busca');
		if ($pos>0)
			$url.='&'.$key.'='.$value;
	}
	if(isset($_REQUEST['offset_u']))
		$offset_u = $_REQUEST['offset_u'];
	else
		$offset_u = 0;

// ********************************************************************************************************
// nota importante: EL �LTIMO PAR�METRO ES LA REFERENCIA DEL IFRAME RESPECTO A LOS DEM�S DE LA P�GINA PADRE, es B�SICO
// ********************************************************************************************************
	$hay=$this->paginacion($num_res, $this->filasporpagina, $offset_u, $destinobusca."?&filtrar=".$_REQUEST['filtrar'].$url.'&sentidoOrdenacion='.$_REQUEST['sentidoOrdenacion'].'&ordenarPor='.$_REQUEST['ordenarPor']."&offset_u=",$this->TotPagMostrar,$this->posicionSolapa);
// ********************************************************************************************************
	if(trim($hay))
		echo '<script>
$("#paginarTop").html(\''.$hay.'\');
$("#paginarTop").css("display","block");
</script>';
echo $hay;
?>
</td></tr></table>
</form>
<script>
function mostrarPagina(direccion,posicionSolapa)	// se recarga la p�gina desde los links de la barra inferior de paginaci�n (2011)
{
	$("#mesajes_formulario").html("<i>accediendo...</i>");
	document.location=direccion+"&posicionSolapa="+posicionSolapa;
	return;
}
</script>
<?php
	}
}
?>