<?php
class MenuNivel2 extends Base
{
	//Datos propios de la clase
	public $id;
	public $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEM�?S DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS
	public $arrTargets;
	public $linkmenu;

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'mnu_level_two';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->arrTargets=array(
			'?',
			'Solapas',
			'Footer',
			'Extras'
		);
		$this->getAdminLevel();	// RETRIEVE CONSTANTS LEVELS USER
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE ID="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->id_level_one=$a['id_level_one'];
		$this->linkMenu=$this->output_datos($a['linkMenu']);
		$this->etiqueta=$this->output_datos($a['etiqueta']);
		$this->alias=$a['alias'];
		$this->orden=$a['orden'];
		$this->activo=$a['activo'];
		$this->tip=$a['tip'];
		$this->id_usuario=$a['id_usuario'];
		$this->group_ids=$a['group_ids'];
		return;
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(c.id) + IFNULL(mlt.activo,0) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS mlt LEFT JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'contenidos AS c ON c.id_niv_two=mlt.id WHERE mlt.id="'.(int)$this->id.'"';
		$existen_paginas=$this->sql->valor($query);

		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		if(!$this->id) $this->id=0;
		$groupsIds='';
		if($this->group_ids)
		{
		  foreach($this->group_ids as $key=>$value)
			$groupsIds.=$value.',';
		}

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id,id_level_one,linkMenu,etiqueta,alias,orden,activo,registrado,tip,id_usuario,group_ids) VALUES ("'.$this->id.'","'.$this->id_level_one.'", "'.trim($this->linkMenu).'", "'.trim($this->etiqueta).'", "'.trim($this->alias).'","'.$this->orden.'","'.$this->activo.'","'.(int)$this->registrado.'","'.trim($this->tip).'", "'.$this->id_usuario.'", "'.$groupsIds.'")';
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos()
	{
?>
<tr>
	<td align="right">Texto Men&uacute;:</td>
	<td><input type='text' id='etiqueta' name='etiqueta' value='<?=$this->etiqueta?>' style='width:120px'></td>
	<td align="right">Alias:</td>
	<td><input type='text' id='alias' name='alias' value='<?=$this->alias?>' style='width:120px'></td>
	<td align="right">Tip:</td>
	<td colspan="3"><input type='text' id='tip' name='tip' value='<?=$this->tip?>' style='width:144px'></td>
	<td align="right">Link:</td>
	<td><input type='text' id='linkMenu' name='linkMenu' value='<?=$this->linkMenu?>' style='width:99%;'></td>
</tr>
<tr>
	<td align="right" title="Pertenece al men&uacute; de nivel 1">Pertenece a:</td>
	<td>
<?php
		// START SE CONSTRUYE EL CONTROL CON TODOS LOS ELEMENTOS DE MENU DISPONIBLES PARA EL USARIUO ACTUAL
		$salida='';

		if($this->levelAccessMaxLevel != $_SESSION['usuarioNivel'] && !$this->id)	// FILTRO PARA TODOS LOS USUARIOS CON NIVEL INFERIOR A GOOD
		  $filterSql='&& (mlo.id_usuario='.(int)$_SESSION['id_usuario'].' || mlo.id_usuario=0)';

		$query='SELECT mlo.id,mlo.etiqueta, mlo.scope_element FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_one AS mlo WHERE 1 '.$filterSql.' ORDER BY mlo.etiqueta';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			if($this->levelAccessMaxLevel != $_SESSION['usuarioNivel'] && $this->id && $this->id_level_one != $arra['id'])
				continue;

			$targetMenuOne=strtoupper(substr($this->arrTargets[$arra['scope_element']],0,3));
			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->id_level_one == $arra['id']) $salida.=' selected="selected"';
			$salida.='>'.$targetMenuOne.': '.$arra['etiqueta'].($_SESSION['forcedLangSite']?'' :'::'.$arra['prefijo']).'</option>';
		}

		echo '<select id="id_level_one" name="id_level_one" style=\'width:125px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>

	<td align="right" title="Usuario propietario de este determinado elemento o accesible a todos">Usuario:</td>
	<td>
<?php
		$salida='';
		$query='SELECT id, CONCAT(nombre," ",apellidos) AS theName,nivel_acceso FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE activo=1 ORDER BY nombre';
		$res2=$this->sql->query($query);
		$salida.='<option value="0"';
//		if(!$arra['activo']) $salida.=' style="text-decoration:line-through;"';
		if(isset($this->id_usuario) && $this->id_usuario == 0) $salida.=' selected="selected" ';
		$salida.='>Global</option>';

		while($arra=$this->sql->fila2($res2))
		{
			//SE OMITEN OTROS USUARIOS CON NIVEL SUPERIOR DE ACCESO AL DEL USUARIO ACTUAL
			if($_SESSION['usuarioNivel'] < $arra['nivel_acceso'])	continue;

			$salida.='<option value=\''.$arra['id'].'\'';
//			if(!$arra['activo']) $salida.=' style="text-decoration:line-through;"';
			if($this->id_usuario == $arra['id']) $salida.=' selected="selected" ';
			$salida.='>'.utf8_encode($arra['theName']).'</option>';
		}

		echo '<select id="id_usuario" name="id_usuario" style=\'width:165px\' ><option value="-1" class="tx10" style="color:#666">?</option>';
		echo $salida;
		echo '</select>';
?>
	</td>
	<td align="right">Posici&oacute;n:</td>
	<td><input type='text' id='orden' name='orden' value='<?=$this->orden?>' style='width:24px;text-align:right' maxlength="2"></td>
	<td align="right" title="El men&uacute; es activo?">Habilitado:</td>
	<td>
<?php
		echo '<select id="activo" name="activo" style="width:44px;"';
		echo '<option value=-1 ';
		if($this->activo==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->activo==0 && isset($this->activo))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->activo==1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>
	<td align="right" title="Grupos a los que puede pertenecer">Grupos:</td>
	<td rowspan="2" valign="top">
<?php
		if($_SESSION['usuarioNivel'] >= $this->levelAccessModules['SuperAdmin'])
		{
		  $arrUsers=explode(',',$this->group_ids);
		  $salida='';
		  $query='SELECT id, group_name FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'user_groups WHERE is_enabled=1 ORDER BY group_name';
		  $res2=$this->sql->query($query);

		  while($arra=$this->sql->fila2($res2))
		  {
			  $salida.='<option value=\''.$arra['id'].'\'';
			  if(in_array($arra['id'],$arrUsers)) $salida.=' selected="selected" ';
			  $salida.='>'.($arra['group_name']).'</option>';
		  }

		  echo '<select id="group_ids" name="group_ids[]" style="width:100%;height:40px;" multiple="multiple" size=2 >';
		  echo $salida;
		  echo '</select>';
		}
		else
		  echo'<span style="display:block;padding-top:3px;color:'.$_SESSION['colorErrors'].'">n./d.</span>';
?>
	</td>
</tr>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
	if(document.f<?=$idunico?>.etiqueta.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.etiqueta);
		document.f<?=$idunico?>.etiqueta.focus();
		alert('Por favor, escriba la etiqueta que debe aparecer');
		restauraCampo(document.f<?=$idunico?>.etiqueta);
		return;
	}
	if(document.f<?=$idunico?>.alias.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.alias);
		document.f<?=$idunico?>.alias.focus();
		alert('Por favor, IMPRESCINDIBLE el alias (solo letras alfabeto inglés)');
		restauraCampo(document.f<?=$idunico?>.alias);
		return;
	}

	err=controlNumerico(document.f<?=$idunico?>.orden.value,'orden','f<?=$idunico?>',1);
	if(err)
	{
		document.f<?=$idunico?>.orden.value='';
		return false;
	}
	if(document.f<?=$idunico?>.orden.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.orden);
		document.f<?=$idunico?>.orden.focus();
		alert('Por favor, indique la posicion del elemento del menu');
		restauraCampo(document.f<?=$idunico?>.orden);
		return;
	}

	document.f<?=$idunico?>.submit();
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}
-->
</script>
<?php
		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->id_level_one=$datos['id_level_one'];
		$this->linkMenu=$datos['linkMenu'];
		$this->etiqueta=$datos['etiqueta'];
		$this->alias=$datos['alias'];
		$this->orden=$datos['orden'];
		$this->activo=$datos['activo'];
		$this->tip=$datos['tip'];
		$this->id_usuario=$datos['id_usuario'];
		$this->group_ids=$datos['group_ids'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$idunico=rand(1,10000).time();
		$query='SELECT lab.*,u.nivel_acceso FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios AS u ON u.id=lab.id_usuario WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		$querytotal='SELECT COUNT(lab.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios AS u ON u.id=lab.id_usuario WHERE 1 '.$filtroSql;

		$ordenacion='orden'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;

		if($_SESSION['filtros']['visitantes_distintos_busca'])
		{
			$res2=$this->sql->query($querytotal);
			$num_res=mysql_num_rows($res2);
		}
		else
			$num_res=$this->sql->valor($querytotal);

		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $this->levelAccessMaxLevel.'<hr>';
//echo $query;

//******************************************************


//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
var idVisualizado=-1;
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->


<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='878' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='12' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
<?php
		if(!$_SESSION['forcedLangSite'])
		{
			echo '<td width="32" align="right">idioma:</td>
				<td title="Idiomas disponibles">';
			$salida='';
			$query='SELECT * FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'idiomas ORDER BY idioma';
			$res2=$this->sql->query($query);

			while($arra=$this->sql->fila2($res2))
			{
				$salida.='<option value=\''.$arra['id'].'\'';
				if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN']==$arra['id']) $salida.=' selected="selected"';
				$salida.='>'.utf8_encode($arra['idioma']).'</option>';
			}

			echo '<select id="'.$this->posicionSolapa.'id_usuario_buscN" name="'.$this->posicionSolapa.'id_usuario_buscN" style=\'width:90px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
			echo $salida;
			echo '</select></td>';
		}
?>
	<td title='Pertenece al men&uacute; principal' width="60" align="right">Pertenece:</td>
	<td>
<?php
		if($this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])	// FILTRO PARA TODOS LOS USUARIOS CON NIVEL INFERIOR A GOOD
		  $filterSql='&& (mlo.id_usuario='.(int)$_SESSION['id_usuario'].' || mlo.id_usuario=0)';

		$query='SELECT mlo.id,mlo.etiqueta, mlo.scope_element FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_one AS mlo WHERE 1 '.$filterSql.' ORDER BY mlo.etiqueta';

		if($_SESSION['forcedLangSite'])	// SI SE FUERZA EL SITE A UN IDIOMA, SE CARGAN SOLAMENTE LAS ETIQUETAS DEL QUE HAYA ACTIVO
		{
			$query='SELECT mlo.id,mlo.etiqueta, mlo.scope_element FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_one AS mlo  WHERE 1 '.$filterSql.' ORDER BY mlo.etiqueta';
		}

		$res2=$this->sql->query($query);

		$salida='<select id="'.$this->posicionSolapa.'id_level_one_buscN" name="'.$this->posicionSolapa.'id_level_one_buscN" style=\'width:88px;\'>';

		$salida.='<option value=-1 ';
		if($_SESSION['filtros'][$this->posicionSolapa.'id_level_one_buscN']==-1)
			$salida.='selected=\'selected\'';
		$salida.='>?</option>';

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_level_one_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_level_one_buscN']==$arra['id']) $salida.=' selected="selected"';
			$salida.='>'.strtoupper(substr($this->arrTargets[$arra['scope_element']],0,3)).': '.$arra['etiqueta'].($_SESSION['forcedLangSite'] ? '' : '::'.$arra['prefijo']).'</option>';
		}

		$salida.='</select>';
		echo $salida;
?>
	</td>

	<td width="48" align="right">Etiqueta:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>etiqueta_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'etiqueta_busca_u']?>' style='width:70px' /></td>
	<td width="20">Link:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>linkMenu_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'linkMenu_busca_u']?>' style='width:70px' /></td>
<?php
		echo '<td width="60" title="Buscar por usuarios">Del usuario:</td>
			<td>';
		$salida='';
		$query='SELECT id, CONCAT(nombre," ",apellidos) AS theName, nivel_acceso FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE activo=1 ORDER BY nombre';
		$res2=$this->sql->query($query);
		$salida.='<option value="0"';

		if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN'] == 0) $salida.=' selected="selected" ';
		$salida.='>Global</option>';

		while($arra=$this->sql->fila2($res2))
		{
			//SE OMITEN OTROS USUARIOS CON NIVEL SUPERIOR DE ACCESO AL DEL USUARIO ACTUAL
			if($_SESSION['usuarioNivel'] < $arra['nivel_acceso'])	continue;

			$salida.='<option value=\''.$arra['id'].'\'';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN'] == $arra['id'])
				$salida.=' selected="selected"';
			$salida.='>'.utf8_encode($arra['theName']).'</option>';
		}

		echo '<select id="'.$this->posicionSolapa.'id_usuario_buscN" name="'.$this->posicionSolapa.'id_usuario_buscN" style=\'width:90px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select></td>';
?>

<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->


<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'34%','','75%','12%');

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='Elemento de nivel principal';
		$arraTitulares[]='Etiqueta';
		$arraTitulares[]='Alias';
		$arraTitulares[]='Posici&oacute;n';
		$arraTitulares[]='Ususario';
		$arraTitulares[]='Activo';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='id_level_one';
		$arraCamposOrdenar[]='etiqueta';
		$arraCamposOrdenar[]='alias';
		$arraCamposOrdenar[]='orden';
		$arraCamposOrdenar[]='id_usuario';
		$arraCamposOrdenar[]='activo';

		 // no se incluye el valor para el ID
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=50;
		$arraLongitudes[]=64;
		$arraLongitudes[]=29;
		$arraLongitudes[]=29;
		$arraLongitudes[]=29;
		$arraLongitudes[]=42;
		$arraLongitudes[]=60;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=25;	// id
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=90;
		$arraLongitudesTitulares[]=170;
		$arraLongitudesTitulares[]=170;
		$arraLongitudesTitulares[]=170;
		$arraLongitudesTitulares[]=50;
		$arraLongitudesTitulares[]=140;
		$arraLongitudesTitulares[]=40;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='rop';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;
		$conutRowsDisplayed=0;
		$selectLabelLevelOne=$_SESSION['forcedLangSite'] ? 'mlo.etiqueta AS nameMenu' : 'CONCAT(mlo.etiqueta," (",i.prefijo,") AS nameMenu")';

		while($a=$this->sql->fila2($res))
		{
		  $rop=null;
			//	SOLO SE PERMITEN VER LOS ELEMENTOS PROPIOS Y LOS GLOBALES (EXCEPTO PARA GOOD)
		  if($_SESSION['id_usuario'] != $a['id_usuario'] && $a['id_usuario'] != 0 && $this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])
				$rop=1; // fila deshabilitada

			$xc=0; $estaActivo='<center style=color:orange>no</center>';
			$query='SELECT '.$selectLabelLevelOne.', mlo.scope_element FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_one AS mlo WHERE mlo.id=\''.$a['id_level_one'].'\'';
			$resTmp=$this->sql->query($query);
			$arrLevelRoot=$this->sql->fila2($resTmp);
			$nombreEtiquetaMenuOne=ucwords($arrLevelRoot['nameMenu']);
			$targetMenuOne=strtoupper(substr($this->arrTargets[$arrLevelRoot['scope_element']],0,3));
			$nombreEtiquetaMenuOne=$nombreEtiquetaMenuOne ? '<span style=color:blue;>'.$targetMenuOne.':</span> '.$nombreEtiquetaMenuOne : '<center><b>?</b></center>';

			if($a['activo']==-1) $esActivo='?';
			if(!$a['activo']) $esActivo='<span style=color:red>NO</span>';
			if($a['activo']==1) $esActivo='SI';
			if($a['registrado']==1) $esRegistrado='<span style=color:red>SI</span>';
			if($a['registrado']<0) $esRegistrado='<span style=color:orange>?</span>';
			$query='SELECT CONCAT(nombre," ",apellidos) AS theName FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE id="'.$a['id_usuario'].'"';
			$elIdioma=utf8_encode($this->sql->valor($query));
			$elIdioma=$elIdioma?$elIdioma:'<span style=color:red;><b>Todos</b></span>';

			if($a['activo']) $estaActivo='<center style=color:lightgreen><b>SI</b></center>';

			$tmpArr[0]=$a['id'];
			array_push($tmpArr,$nombreEtiquetaMenuOne);
			array_push($tmpArr,$a['etiqueta']);
			array_push($tmpArr,$a['alias']);
			array_push($tmpArr,$a['orden']);
			array_push($tmpArr,$elIdioma);
			array_push($tmpArr,$esActivo);
		  array_push($tmpArr,$rop);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
			$conutRowsDisplayed++;
		}

		$idRefPops=0;
		$txtRefEliminar=3;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
