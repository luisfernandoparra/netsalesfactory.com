<?php
class paramsSeoGoogle extends Base
{
	//Datos propios de la clase
	public $id;
	public $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEM�?S DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS
	public $linkmenu;

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'seo_site_params';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->getAdminLevel();	// RETRIEVE CONSTANTS LEVELS USER
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->id_landing=$a['id_landing'];
		$this->meta_canonical=$a['meta_canonical'];
		$this->is_enabled=$a['is_enabled'];
		return;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->id_landing=$datos['id_landing'];
		$this->meta_canonical=$datos['meta_canonical'];
		$this->is_enabled=$datos['is_enabled'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
//		$query='SELECT COUNT(c.id) + IFNULL(mlt.enabled,0) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS mlt LEFT JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'contenidos AS c ON c.id_niv_two=mlt.id WHERE mlt.id="'.(int)$this->id.'"';
//		$existen_paginas=$this->sql->valor($query);
		$existen_paginas=0;
//echo $query;die();
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.'(id, id_landing, meta_canonical, is_enabled) VALUES ('.(int)$this->id.', '.(int)$this->id_landing.', \''.utf8_decode($this->meta_canonical).'\', '.(int)$this->is_enabled.')';
//echo $query.'<pre>'.$query;print_r($_REQUEST);print_r($this);die();
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos($nombreForm=0)
	{
?>
<tr>
	<td align="right" class="tooltip" title="Landing a la que pertenecen los parámetros para<br />el Google <b>SEM</b> (Search Engine Marketing).<br /><span style=color:#bbb;>Recuerda que al insertar un <b>nuevo registro</b>, aparecen<br />disponibles únicamente las promociones que no tienen<br />todavía parámetros SEM guardados.</span>">Landing:</td>
	<td>
<?php
		$salida='';
		$sqlFilter=1;

		if(!$this->id)	// SE LIMITA LA LISTA A LANDING SIN CONTENIDOS YA EXISTENTES
			$sqlFilter.=' && lsc.id NOT IN (SELECT id_landing FROM '.$this->sql->db.'.'.$this->nombreTabla.')';

 		$query='SELECT DISTINCT(lsc.id), lsc.landing_name AS landingElement FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_site_config AS lsc WHERE '.$sqlFilter.' ORDER BY lsc.landing_name';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->id_landing == $arra['id']) $salida.=' selected="selected"';
			$salida.='>'.($arra['landingElement'] ? $arra['landingElement'] : '???').'</option>';
		}

		echo '<select id="id_landing" name="id_landing" style=\'width:150px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>

	<td align="right" class="tooltip" title="Parámetro ´<b>Meta canonical</b>´.<br />Ej.: http://wwww.milanding.com">Meta canonical:</td>
	<td><input type='text' id='meta_canonical' name='meta_canonical' value='<?=$this->meta_canonical?>' style='width:90%;max-width:400px;' /></td>

	<td align="right" class="tooltip" title="Registro activo.<br />Valores: <i>S&Iacute; / NO</i>">Activo?:</td>
	<td width="80">
<?php
		echo '<select id="is_enabled" name="is_enabled" style="width:44px;">';
		echo '<option value=-1 ';
		if($this->is_enabled == -1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->is_enabled == 0 && isset($this->is_enabled))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->is_enabled == 1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>

</tr>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--

function revisa()
{
	if($('#id_landing').val() < 0)
	{
		displayFormError("#id_landing","Por favor, selecciona una landing");
		return false;
	}
	document.f<?=$idunico?>.submit();
}

$(document).ready(function(){
});

function resaltarCasilla(id,xx,zz)	// ABRIR LANDIN ALMACENADA EN BBDD
{
	var tmpElem=$("#"+id).parent();
	var tmpObj=$(tmpElem[0]).children();

	$(tmpObj).each(function(tdPos,tdContent){
		if(tdPos == 1)//POSICION DEL TD CON EL ID DE LA LANDING
		{
			window.open("../?cr="+$(tdContent).html(),"preview","");
			return false;
		}
	});
}

-->
</script>
<?php
		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$idunico=rand(1,10000).time();
		//$query='SELECT lab.*,u.nivel_acceso FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 ';
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		//$querytotal='SELECT COUNT(lab.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 '.$filtroSql;
		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='id_landing'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;

		if($_SESSION['filtros']['visitantes_distintos_busca'])
		{
			$res2=$this->sql->query($querytotal);
			$num_res=mysql_num_rows($res2);
		}
		else
			$num_res=$this->sql->valor($querytotal);

		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $query;

//******************************************************


//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
var idVisualizado=-1;
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->


<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='878' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='12' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td title='Buscar por <b>landing</b> conocida' width="70" align="right" class='tooltip'>En la Landing:</td>
	<td>
<?php
 		$query='SELECT lsc.id, lsc.landing_name AS landingElement FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_site_config AS lsc INNER JOIN '.$this->sql->db.'.'.$this->nombreTabla.' AS sgp ON lsc.id = sgp.id_landing WHERE 1 ORDER BY lsc.landing_name';
//&& lsc.id=\''.$_SESSION['filtros'][$this->posicionSolapa.'id_landing_buscN'].'\'
//		$query='SELECT lc.id_landing, lc.prefix FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc WHERE lc.is_enabled ORDER BY lc.prefix';
		$res2=$this->sql->query($query);

		$salida='<select id="'.$this->posicionSolapa.'id_landing_buscN" name="'.$this->posicionSolapa.'id_landing_buscN" style=\'width:128px;\'>';

		$salida.='<option value=-1 ';
		if($_SESSION['filtros'][$this->posicionSolapa.'id_landing_buscN'] == -1)
			$salida.='selected=\'selected\'';
		$salida.='>?</option>';

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';

			if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_landing_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_landing_buscN'] == $arra['id'])
				$salida.=' selected="selected"';

			$salida.='>'.$arra['landingElement'].'</option>';
		}

		$salida.='</select>';
		echo $salida;
?>
	</td>

	<td width="110" align="right">Meta canonical:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>meta_canonical_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'meta_canonical_busca_u']?>' style='width:80px' /></td>

	<td title='Est&aacute; activo?' align="right" class="tooltip">Activo:</td>
	<td>
<?php
		$arrSN=array(0=>'NO',1=>'S&iacute;');
		echo '<select id="'.$this->posicionSolapa.'is_enabled_buscN" name="'.$this->posicionSolapa.'is_enabled_buscN" style=\'width:50px;\' >';
		echo '<option value=-1 ';
		if($this->is_enabled==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		for($n=0;$n < 2;$n++)
		{
			echo '<option value='.$n.' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']==$n && isset($_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']))
				echo 'selected=\'selected\'';
			echo ' >'.$arrSN[$n].'</option>';
		}
		echo '</select>';
?>
	</td>

<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->


<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,$this->modalHeight,'',$this->modalWidth,$this->modalLeftPos);

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='ID land.';
		$arraTitulares[]='Nombre Landing';
		$arraTitulares[]='Meta canonical';
		$arraTitulares[]='Activo';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='id_landing';
		$arraCamposOrdenar[]='id_landing';
		$arraCamposOrdenar[]='meta_canonical';
		$arraCamposOrdenar[]='is_enabled';

		 // no se incluye el valor para el ID
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=50;
		$arraLongitudes[]=24;
		$arraLongitudes[]=24;
		$arraLongitudes[]=61;
		$arraLongitudes[]=60;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=25;	// id
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=90;
		$arraLongitudesTitulares[]=40;
		$arraLongitudesTitulares[]=120;
		$arraLongitudesTitulares[]=350;
		$arraLongitudesTitulares[]=90;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='rop';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;
		$conutRowsDisplayed=0;
		$selectLabelLevelOne=$_SESSION['forcedLangSite'] ? 'mlo.landing_identifier AS nameMenu' : 'CONCAT(mlo.landing_identifier," (",i.prefijo,") AS nameMenu")';

		while($a=$this->sql->fila2($res))
		{
		  $rop=null;
			//	SOLO SE PERMITEN VER LOS ELEMENTOS PROPIOS Y LOS GLOBALES (EXCEPTO PARA GOOD)
		  if($_SESSION['id_usuario'] != $a['id_usuario'] && $a['id_usuario'] != 0 && $this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])
				$rop=1; // fila deshabilitada

			$xc=0; $estaActivo='<center style=color:orange>no</center>';
			$query='SELECT lc.prefix,lsc.landing_name FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_site_config AS lsc INNER JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc ON lc.id_landing=lsc.client_id  WHERE lsc.id='.(int)$a['id_landing'];
			$customerDetails=$this->sql->query($query);
			$dataCustomer=$this->sql->fila2($customerDetails);

			if($a['is_enabled']==-1) $arrSN='?';
			if(!$a['is_enabled']) $arrSN='<span style=color:red>NO</span>';
			if($a['is_enabled']==1) $arrSN='SI';
			if($a['registrado']==1) $esRegistrado='<span style=color:red>SI</span>';
//			$elIdioma=utf8_encode($this->sql->valor($query));
			$elIdioma=$elIdioma?$elIdioma:'<span style=color:red;><b>Todos</b></span>';

			if($a['is_enabled']) $estaActivo='<center style=color:lightgreen><b>SI</b></center>';

			$tmpArr[0]=$a['id'];
			array_push($tmpArr,$a['id_landing']);
			array_push($tmpArr,$dataCustomer['landing_name']);
			array_push($tmpArr,utf8_encode($a['meta_canonical']));
			array_push($tmpArr,$arrSN);
		  array_push($tmpArr,$rop);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
			$conutRowsDisplayed++;
		}

		$idRefPops=0;
		$txtRefEliminar=3;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
