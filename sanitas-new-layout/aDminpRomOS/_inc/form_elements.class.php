<?php
class formElements extends Base
{
	//Datos propios de la clase
	public $id;
	public $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEM�?S DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS
	public $linkmenu;

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'form_elements';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->getAdminLevel();	// RETRIEVE CONSTANTS LEVELS USER
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->element_name=$this->output_datos($a['element_name']);
//		$this->is_array=utf8_encode($a['is_array']);
		$this->is_array=utf8_encode($a['is_array']);
		$this->enabled=$a['enabled'];
		$this->element_type=$a['element_type'];
		$this->element_value=$this->output_datos($a['element_value']);
		$this->element_label=$this->output_datos($a['element_label']);
		$this->error_required=$this->output_datos($a['error_required']);
		$this->error_data_type=$this->output_datos($a['error_data_type']);
		$this->order_build_in=$a['order_build_in'];
		return;
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(c.id) + IFNULL(mlt.enabled,0) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS mlt LEFT JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'contenidos AS c ON c.id_niv_two=mlt.id WHERE mlt.id="'.(int)$this->id.'"';
		$existen_paginas=$this->sql->valor($query);
echo $query;die();
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.'(id, element_name,is_array,enabled,element_type, element_value, element_label, error_required, error_data_type,order_build_in) VALUES ('.(int)$this->id.', \''.trim(utf8_decode($this->element_name)).'\', '.(int)$this->is_array.', '.(int)$this->enabled.', \''.$this->element_type.'\',\''.utf8_decode($this->element_value).'\', \''.utf8_decode($this->element_label).'\', \''.utf8_decode($this->error_required).'\', \''.utf8_decode($this->error_data_type).'\','.(int)$this->order_build_in.')';
//echo '<pre>'.$query;print_r($_REQUEST);print_r($this);die();
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos($nombreForm=0)
	{
		$dataPopline=str_replace("\r\n",'',($this->element_label));
		$dataPopline=utf8_encode($dataPopline);
		$dataPopline=htmlspecialchars_decode($dataPopline);
		$dataPopline1=str_replace("\r\n",'',($this->error_required));
		$dataPopline1=utf8_encode($dataPopline1);
		$dataPopline1=htmlspecialchars_decode($dataPopline1);
		$dataPopline2=str_replace("\r\n",'',($this->error_data_type));
		$dataPopline2=utf8_encode($dataPopline2);
		$dataPopline2=htmlspecialchars_decode($dataPopline2);
?>
<tr>
	<td align="right" class="tooltip" title="Texto identificador del elemento para el formulario.<br />Para que el elemeto sea parte de un array, finalizar el nombre con ´<b>[]</b>´">Nombre:</td>
	<td><input type='text' id='element_name' name='element_name' value='<?=$this->element_name?>' style='width:120px' /></td>

	<td align="right" class="tooltip" title="Tipo de elemeto para el formulario">Tipo:</td>
	<td>
<?php
		$salida='';

 		$query='SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME =\''.$this->nombreTabla.'\' AND COLUMN_NAME = \'element_type\'';
		$resQuery=$this->sql->valor($query);
		$arra=explode(',', str_replace('\'', '', substr($resQuery, 5, (strlen($resQuery)-6))));

		foreach($arra as $key=>$value)
		{
			$salida.='<option value=\''.$value.'\'';
			if(isset($this->element_type) && $this->element_type === $value)
				$salida.=' selected="selected"';
			$salida.='>'.$value.'</option>';
		}

		echo '<select id="element_type" name="element_type" style=\'width:125px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
	<td align="right" class="tooltip" title="Valor a mostrar en caso que sea un elemento de array">Valor:</td>
	<td><input type='text' id='element_value' name='element_value' value='<?=utf8_encode($this->element_value)?>' style='width:120px'></td>
	<td align="right" class="tooltip" title="El elemento pertenece a un array?">Array:</td>
	<td width="80">
<?php
		echo '<select id="is_array" name="is_array" style="width:46px;" >';
		echo '<option value=-1 ';
		if($this->is_array==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->is_array==0 && isset($this->is_array))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->is_array==1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>

</tr>
<tr <?=$this->id ? 'valign="top"' : '' ?>>
	<td align="right">
		<a title="Edición <b>Wysiwyg</b> Texto de la `label` que puede acompañar al elento del formulario." class='btn ajax editAjax tooltip' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?>&innerTextHeight=<?=$this->innerTextHeight?>&nombreCampo=data_element_label&customer_landing_id=<?=(int)$this->element_type?>">Label:</a>
		<span class="<?=$this->id ? '' : 'hide'?>"><br /><a title="Editar directamente el código <b>HTML</b>" class='btn ajax directHTML tooltip' href="<?=basename($_SERVER['PHP_SELF'])?>?id=<?=$this->id?>&accion=directHTML&fieldName=data_element_label">Html:</a></span>
	</td>
	<td>
		<div id="data_element_label" class='editorPopline' contenteditable='true' style="display:block;margin-top:1px;border:1px solid #999;<?=$this->id ? 'width:100%;max-width:240px;' : 'width:auto';?>;height:<?=$this->id ? '50px' : '16px';?>;overflow:auto;"><?=$dataPopline?></div>
		<textarea id='element_label' name='element_label' style='display:none;'><?=$this->data_element_label?></textarea>
	</td>
	<td align="right">
		<a title="Edición <b>Wysiwyg</b> del texto para el error del elemento vacío (que es obligatorio)." class='btn ajax editAjax tooltip' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?>&innerTextHeight=<?=$this->innerTextHeight?>&nombreCampo=data_error_required&customer_landing_id=<?=(int)$this->element_type?>">Error vacío:</a>
		<span class="<?=$this->id ? '' : 'hide'?>"><br /><a title="Editar directamente el código <b>HTML</b>" class='btn ajax directHTML tooltip' href="<?=basename($_SERVER['PHP_SELF'])?>?id=<?=$this->id?>&accion=directHTML&fieldName=data_error_required">Html:</a></span>
	</td>
	<td>
		<div id="data_error_required" class='editorPopline' contenteditable='true' style="display:block;margin-top:1px;border:1px solid #999;<?=$this->id ? 'width:100%;max-width:240px;' : 'width:auto';?>;height:<?=$this->id ? '50px' : '16px';?>;overflow:auto;"><?=$dataPopline1?></div>
		<textarea id='error_required' name='error_required' style='display:none;'><?=$this->data_error_required?></textarea>
	</td>
	<td align="right">
		<a title="Texto para el error cuando el contenido no es válido." class='btn ajax editAjax tooltip' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?>&innerTextHeight=<?=$this->innerTextHeight?>&nombreCampo=data_error_data_type&customer_landing_id=<?=(int)$this->element_type?>">Txt inválido:</a>
		<span class="<?=$this->id ? '' : 'hide'?>"><br /><a title="Editar directamente el código <b>HTML</b>" class='btn ajax directHTML tooltip' href="<?=basename($_SERVER['PHP_SELF'])?>?id=<?=$this->id?>&accion=directHTML&fieldName=data_error_data_type">Html:</a></span>
	</td>
	<td>
		<div id="data_error_data_type" class='editorPopline' contenteditable='true' style="display:block;margin-top:1px;border:1px solid #999;<?=$this->id ? 'width:100%;max-width:240px;' : 'width:auto';?>;height:<?=$this->id ? '50px' : '16px';?>;overflow:auto;"><?=$dataPopline2?></div>
		<textarea id='error_data_type' name='error_data_type' style='display:none;'><?=$this->data_error_data_type?></textarea>
	</td>
	<td class="tooltip" title='Este registro está habilitado?' align="right">Activo:</td>
	<td>
<?php
		echo '<select id="enabled" name="enabled" style="width:46px;" >';
		echo '<option value=-1 ';
		if($this->enabled==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->enabled==0 && isset($this->enabled))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->enabled==1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>
</tr>
<tr>
	<td align="right" class="tooltip" title="Orden (interno) correspondiente si este elemento hace parte de un array">Orden:</td>
	<td>
		<input type='text' id='order_build_in' name='order_build_in' value='<?=$this->order_build_in?>' style='width:50px;' />
	</td>
</tr>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--

$(document).ready(function(){
	//editor jQuery color box
	$(".editAjax").colorbox({
		opacity: 0.4,
		initialHeight:"10px",
		initialWidth:"10%",
		close:true,
		overlayClose:true,
		width:"<?=$this->id ? 94 : 64 ?>%",
		height:"100%",
		iframe:true,
		html:true,
		innerHeight:true
	});
});

function revisa()
{
	if($.trim($('#element_name').val()) == '')
	{
		displayFormError("#element_name","Por favor, introduce un texto identificador para este elemento");
		$('#element_name').val("");
		return;
	}
	if($('#element_type').val() == "")
	{
		displayFormError("#element_type","Por favor, seleccione el tipo de elemento");
		return false;
	}
	if($.trim($('#is_array').val()) < 0)
	{
		displayFormError("#is_array","Por favor, indicar si es un array");
		$('#txt_title_site').val("");
		return;
	}
	popeditorSaveData("f<?=$idunico?>");
}

-->
</script>
<?php
		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->element_name=$datos['element_name'];
		$this->is_array=$datos['is_array'];
		$this->enabled=$datos['enabled'];
		$this->element_type=$datos['element_type'];
		$this->element_value=htmlspecialchars_decode($datos['element_value']);
		$this->order_build_in=$datos['order_build_in'];
		$this->element_label=htmlspecialchars_decode($datos['element_label']);
		$this->error_required=htmlspecialchars_decode($datos['error_required']);
		$this->error_data_type=htmlspecialchars_decode($datos['error_data_type']);
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$idunico=rand(1,10000).time();
		//$query='SELECT lab.*,u.nivel_acceso FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 ';
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		//$querytotal='SELECT COUNT(lab.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 '.$filtroSql;
		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='element_name'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;

		if($_SESSION['filtros']['visitantes_distintos_busca'])
		{
			$res2=$this->sql->query($querytotal);
			$num_res=mysql_num_rows($res2);
		}
		else
			$num_res=$this->sql->valor($querytotal);

		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $query;

//******************************************************


//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
var idVisualizado=-1;
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->


<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='878' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='12' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td title='Buscar por tipo de elemento' width="25" align="right" class='tooltip'>Tipo:</td>
	<td>
<?php
		$salida='';
 		$query='SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME =\''.$this->nombreTabla.'\' AND COLUMN_NAME = \'element_type\'';
		$resQuery=$this->sql->valor($query);
		$arra=explode(',', str_replace('\'', '', substr($resQuery, 5, (strlen($resQuery)-6))));

		foreach($arra as $key=>$value)
		{
			$salida.='<option value=\''.$value.'\'';

			if($_SESSION['filtros'][$this->posicionSolapa.'element_type_busca'] == $value)
				$salida.=' selected="selected"';

			$salida.='>'.$value.'</option>';
		}

		echo '<select id="'.$this->posicionSolapa.'element_type_busca" name="'.$this->posicionSolapa.'element_type_busca" style=\'width:108px\' >
			<option value="" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
	</td>

	<td width="30" align="right">Nombre:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>element_name_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'element_name_busca_u']?>' style='width:80px' /></td>

	<td width="30" align="right">Label:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>element_label_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'element_label_busca_u']?>' style='width:80px' /></td>

	<td width="30" align="right">Valor:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>is_array_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'is_array_busca_u']?>' style='width:80px' /></td>
	<td title='' align="right">Array:</td>
	<td>
<?php
		$esArr=array(0=>'NO',1=>'S&iacute;');
		echo '<select id="'.$this->posicionSolapa.'is_array_buscN" name="'.$this->posicionSolapa.'is_array_buscN" style=\'width:50px;\' title="Nivel de acceso del usuario al Back-office">';
		echo '<option value=-1 ';
		if($this->is_array==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		for($n=0;$n < 2;$n++)
		{
			echo '<option value='.$n.' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'is_array_buscN']==$n && isset($_SESSION['filtros'][$this->posicionSolapa.'is_array_buscN']))
				echo 'selected=\'selected\'';
			echo ' >'.$esArr[$n].'</option>';
		}
		echo '</select>';
?>
	</td>

	<td title='' align="right">Activo:</td>
	<td>
<?php
		$esActivo=array(0=>'NO',1=>'S&iacute;');
		echo '<select id="'.$this->posicionSolapa.'enabled_buscN" name="'.$this->posicionSolapa.'enabled_buscN" style=\'width:50px;\' title="Nivel de acceso del usuario al Back-office">';
		echo '<option value=-1 ';
		if($this->enabled==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		for($n=0;$n < 2;$n++)
		{
			echo '<option value='.$n.' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'enabled_buscN']==$n && isset($_SESSION['filtros'][$this->posicionSolapa.'enabled_buscN']))
				echo 'selected=\'selected\'';
			echo ' >'.$esActivo[$n].'</option>';
		}
		echo '</select>';
?>
	</td>

<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->


<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,$this->modalHeight,'',$this->modalWidth,$this->modalLeftPos);

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='Tipo de elemento';
		$arraTitulares[]='Nombre';
		$arraTitulares[]='label';
		$arraTitulares[]='Valor';
		$arraTitulares[]='Es array';
		$arraTitulares[]='Activo';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='element_type';
		$arraCamposOrdenar[]='element_name';
		$arraCamposOrdenar[]='element_label';
		$arraCamposOrdenar[]='element_value';
		$arraCamposOrdenar[]='is_array';
		$arraCamposOrdenar[]='enabled';

		 // no se incluye el valor para el ID
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=50;
		$arraLongitudes[]=64;
		$arraLongitudes[]=32;
		$arraLongitudes[]=38;
		$arraLongitudes[]=37;
		$arraLongitudes[]=42;
		$arraLongitudes[]=60;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=25;	// id
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=90;
		$arraLongitudesTitulares[]=90;
		$arraLongitudesTitulares[]=120;
		$arraLongitudesTitulares[]=210;
		$arraLongitudesTitulares[]=200;
		$arraLongitudesTitulares[]=50;
		$arraLongitudesTitulares[]=40;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='rop';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;
		$conutRowsDisplayed=0;
		$selectLabelLevelOne=$_SESSION['forcedLangSite'] ? 'mlo.landing_identifier AS nameMenu' : 'CONCAT(mlo.landing_identifier," (",i.prefijo,") AS nameMenu")';

		while($a=$this->sql->fila2($res))
		{
		  $rop=null;
			//	SOLO SE PERMITEN VER LOS ELEMENTOS PROPIOS Y LOS GLOBALES (EXCEPTO PARA GOOD)
		  if($_SESSION['id_usuario'] != $a['id_usuario'] && $a['id_usuario'] != 0 && $this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])
				$rop=1; // fila deshabilitada

			$xc=0; $estaActivo='<center style=color:orange>no</center>';
//			$query='SELECT lc.prefix FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc WHERE lc.id_landing='.(int)$a['element_type'];
//			$reCustomer=$this->sql->valor($query);

			if($a['enabled']==-1) $esActivo='?';
			if(!$a['enabled']) $esActivo='<span style=color:red>NO</span>';
			if($a['enabled']==1) $esActivo='SI';
			$isArray=$a['is_array'] > 0 ? '<span style=color:green>SI</span>' : '<span style=color:red>NO</span>';
			if($a['registrado']==1) $esRegistrado='<span style=color:red>SI</span>';
			if($a['registrado']<0) $esRegistrado='<span style=color:orange>?</span>';
			$query='SELECT CONCAT(nombre," ",apellidos) AS theName FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE id="'.$a['id_usuario'].'"';
//			$elIdioma=utf8_encode($this->sql->valor($query));
			$elIdioma=$elIdioma?$elIdioma:'<span style=color:red;><b>Todos</b></span>';

			if($a['enabled']) $estaActivo='<center style=color:lightgreen><b>SI</b></center>';

			$tmpArr[0]=$a['id'];
			array_push($tmpArr,$a['element_type']);
			array_push($tmpArr,utf8_encode($a['element_name']));
			array_push($tmpArr,utf8_encode($a['element_label']));
			array_push($tmpArr,utf8_encode($a['element_value']));
			array_push($tmpArr,$isArray);
			array_push($tmpArr,$esActivo);
		  array_push($tmpArr,$rop);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
			$conutRowsDisplayed++;
		}

		$idRefPops=0;
		$txtRefEliminar=3;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
