<?php
class filesBackoffice extends Base
{
	//Datos propios de la clase
	var $id;

	function __construct($sql)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'back_office_tabs';
		return;
	}

//******************************************************
//	SE EXTRAEN LOS REGISTROS CON NOMBRES DE ARCHIVOS PHP EN "tab_content"
//******************************************************
	function obtenerScriptsUtilizados()
	{
// (bot.tab_type=1 || bot.tab_type =2) &&
		$query='SELECT bot.id,bot.tab_content,bot.tab_title,bot.tab_child,(SELECT sec.tab_title FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS sec WHERE sec.id_user='.$_SESSION['idUsuarioTabActivo'].' && sec.id=bot.tab_child) AS rootTab FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS bot WHERE  bot.tab_content !=  "" && bot.tab_content LIKE "%.php%" && INSTR(bot.tab_content,".php") != 0 && bot.id_user='.$_SESSION['idUsuarioTabActivo'];
//echo $query;
		$res=$this->sql->query($query);
		while($arra=$this->sql->fila2($res))
		{
			$this->tituloRootTab[$arra['id']]=$arra['rootTab'];
			$this->tituloTab[$arra['id']]=$arra['tab_title'];
			$this->scriptExsitente[$arra['id']]=strip_tags($arra['tab_content']);
		}
		return;
	}
}
?>