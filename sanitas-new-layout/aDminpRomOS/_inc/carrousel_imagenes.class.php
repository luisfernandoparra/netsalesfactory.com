<?php
class CarrouselImagenes extends Base
{
	//Datos propios de la clase
	var $id;
	var $nombreTabla;
	var $posicionSolapa;	// POSICION DE LA VENTANA RESPECTO A LAS DEMAS DE SU MISMA SECCION E IDENTIFICADOR PARA LOS CAMPOS DE LAS BUSQUEDAS

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'contenidos_slides';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las búsquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->id_niv_one=$a['id_niv_one'];
		$this->id_niv_two=$a['id_niv_two'];
		$this->title_banner=$a['title_banner'];
		$this->texto_opcional=$this->output_datos($a['texto_opcional']);
		$this->alto=$a['alto'];
		$this->ancho=$a['ancho'];
		$this->auto_play=$a['auto_play'];
		$this->posicion=$a['posicion'];
		$this->thumbnail=$a['thumbnail'];
		$this->imagen=$a['imagen'];
		return;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		$modif_imagenCarrousel=0; $isError=null;
		$enEdicion=$this->id ? '_editar':'';
//echo '<br /><pre>';print_r($_REQUEST);print_r($_FILES);print_r($this);exit;

		if((count($_FILES['imagen']) || count($_FILES['thumbnail'])) && !($this->quitar_archivo || $this->quitar_miniatura))	// SOLO SI ES UNA IMAGEN
		{
			if(!is_dir($this->path_imagenes))
			{
				if(!@mkdir($this->path_imagenes))	// SI SE PRODUCIRSE UN ERROR DE CREACIÓN DE CARPETA, SE VISUALIZA UN MENSAJE Y SE ABORTA LA OPERACIÓN
				{
					$this->mensajeSalida='<span class=enc2 style=background-color:red;color:#fff>&nbsp;ERROR GRAVE&nbsp;</span>&nbsp;NO EXISTE LA CARPETA DE IMAGENES';
					$this->mensajeSalida='ERROR GRAVE, EN LA CARPETA DE IMAGENES';
					$isError=1;
				}
			}
		
			// SOLO SOLO SE PUEDEN SUBIR AL SERVIDOR IMAGENES VALIDAS
			if($_FILES['imagen']['name'] && !$isError)
				if($_FILES['imagen']['type']!='image/x-png' && $_FILES['imagen']['type']!='image/pjpeg' && $_FILES['imagen']['type']!='image/jpeg' && $_FILES['imagen']['type']!='image/gif' && $_FILES['imagen']['type']!='image/png' && $_FILES['imagen']['type']!='image/x-icon')
				{
					$this->mensajeSalida='ARCHIVO NO SOPORTADO, SOLO IMAGENES';
					$isError=1;
				}
			if($_FILES['thumbnail']['name'] && !$isError)
				if($_FILES['thumbnail']['type']!='image/x-png' && $_FILES['thumbnail']['type']!='image/pjpeg' && $_FILES['thumbnail']['type']!='image/jpeg' && $_FILES['thumbnail']['type']!='image/gif' && $_FILES['thumbnail']['type']!='image/png' && $_FILES['thumbnail']['type']!='image/x-icon')
				{
					$this->mensajeSalida='ARCHIVO NO SOPORTADO PARA LA MINIATURA, SOLO IMAGENES';
					$isError=1;
				}
	
			$tamanio=$_FILES['imagen']['size'];
			$tamanioThumb=$_FILES['thumbnail']['size'];

			if($tamanio > $this->tamanioMaximoImagenes && !$isError)	// SE HA SUPERADO EN BYTES EL TAMAÑO MAXIMO PERMITIDO PARA EL OBJETO DE FLASH
			{
				$tamanio=number_format(($tamanio/1000),2,',','.').'Kb';
				$this->mensajeSalida='ERROR: es demasiado grande: '.$tamanio.' (MAX: '.($this->tamanioMaximoImagenes/1000).'Kb)';
				$isError=1;
			}

			if($tamanioThumb > $this->tamanioMaximoThubnail && !$isError)	// SE HA SUPERADO EN BYTES EL TAMAÑO MAXIMO PERMITIDO PARA EL OBJETO DE FLASH
			{
				$tamanioThumb=number_format(($tamanioThumb/1000),2,',','.').'Kb';
				$this->mensajeSalida='ERROR: la miniatura es demasiado grande: '.$tamanioThumb.' (MAX: '.($this->tamanioMaximoThubnail/1000).'Kb)';
				$isError=1;
			}

			if(is_uploaded_file($_FILES['thumbnail']['tmp_name']) && !$isError)
			{
				$tmp_fileThumb='imx'.date('dmyis',time()).$_FILES['thumbnail']['name'];
				copy($_FILES['thumbnail']['tmp_name'], $this->path_imagenes.$tmp_fileThumb);
//echo $_FILES['imagen']['tmp_name'].'<br /><pre>';print_r($_REQUEST);print_r($this);exit;
				$_imgThumb=', "'.$tmp_fileThumb.'" ';
				$campoThumb=', thumbnail';
				$campoUpdate.=', thumbnail="'.$tmp_fileThumb.'"';
				$modif_thumbnail=1;
			}

			if(is_uploaded_file($_FILES['imagen']['tmp_name']) && !$isError)
			{
				$tmp_file='imx'.date('dmyis',time()).$_FILES['imagen']['name'];
				copy($_FILES['imagen']['tmp_name'], $this->path_imagenes.$tmp_file);
//echo $_FILES['imagen']['tmp_name'].'<br /><pre>';print_r($_REQUEST);print_r($this);exit;
				$_img=', "'.$tmp_file.'" ';
				$campoImg=', imagen';
				$campoUpdate.=', imagen="'.$tmp_file.'"';
				$modif_imagenCarrousel=1;
			}
		}

		if($isError)
		{
				echo '<br /><br /><center><p ><h4 style="color:red">'.$this->mensajeSalida.'</h4></p><br /><br /><br /><a href="#null" style="width:100px" class="btn" onclick="document.location=\''.$this->nombreFilePhpBase.$enEdicion.'.php?id='.$this->id.'\';">volver a editar</a></center>';				exit;
		}

		if($this->quitar_archivo)	// SI SE HA SELECCIONADO QUITAR EL ARCHIVO, SE PROCEDE A BORRARLO DE LA TABLA
		{
			$_img=',""'; $campoImg=', imagen';
			$modif_imagenCarrousel=1; $this->imagen='';
			$query='SELECT imagen FROM '.$this->nombreTabla." WHERE id=".$this->id;	// SE OBTIENE LA archivo A BORRAR
			$archivoBorrar=$this->sql->valor($query);

			if($this->path_imagenes.$archivoBorrar)	// SI EXISTE EL ARCHIVO, SE BORRA FISICAMENTE
			{
				@unlink($this->path_imagenes.$archivoBorrar);
				$query='UPDATE '.$this->nombreTabla.' SET imagen="" WHERE id='.$this->id;
				$this->sql->query($query);
			}
		}

		if($this->quitar_miniatura)
		{
			$_imgThumb=',""'; $campoThumb=', thumbnail';
			$modif_thumbnail=1; $this->thumbnail='';
			$query='SELECT thumbnail FROM '.$this->nombreTabla.' WHERE id='.$this->id;	// SE OBTIENE LA archivo A BORRAR
			$archivoBorrar=$this->sql->valor($query);
//			$arra=$this->sql->fila2($res);
//			$UltimoId=$arra['id'];
//			$archivoBorrar=$arra['thumbnail'];

			if($this->path_imagenes.$archivoBorrar)	// SI EXISTE EL ARCHIVO, SE BORRA FISICAMENTE
			{
				@unlink($this->path_imagenes.$archivoBorrar);
				$query='UPDATE '.$this->nombreTabla.' SET thumbnail="" WHERE id='.$this->id;
				$this->sql->query($query);
			}
		}

		if($this->id_niv_two<0) $this->id_niv_two=0;
		if(!$this->id) $this->id=0; // NECESARIO PARA AÑADIR NUEVOS REGISTROS
		$this->texto_opcional=str_replace('"',"'",$this->texto_opcional);

//		if(!($modif_imagenCarrousel || $modif_thumbnail) && $_REQUEST['id'])	// ACTULIZAR UN REGISTRO
//			$query='UPDATE '.$this->nombreTabla.' SET texto_opcional="'.utf8_decode($this->texto_opcional).'", id_niv_one='.$this->id_niv_one.',id_niv_two='.$this->id_niv_two.',title_banner="'.$this->title_banner.'", alto="'.$this->alto.'", ancho="'.$this->ancho.'", auto_play="'.$this->auto_play.'", posicion="'.$this->posicion.'" WHERE id='.$this->id;
//		else
//			$query='REPLACE INTO '.$this->nombreTabla.' (id,texto_opcional,id_niv_one,id_niv_two,title_banner,alto,ancho,auto_play,posicion '.$campoImg.$campoThumb.') VALUES ('.$this->id.', "'.utf8_decode($this->texto_opcional).'",'.$this->id_niv_one.','.$this->id_niv_two.',"'.$this->title_banner.'","'.$this->alto.'","'.$this->ancho.'",'.$this->auto_play.',"'.$this->posicion.'" '.$_img.$_imgThumb.')';

			$query='UPDATE '.$this->nombreTabla.' SET texto_opcional="'.utf8_decode($this->texto_opcional).'", id_niv_one='.$this->id_niv_one.',id_niv_two='.$this->id_niv_two.',title_banner="'.$this->title_banner.'", alto="'.$this->alto.'", ancho="'.$this->ancho.'", auto_play="'.$this->auto_play.'", posicion="'.$this->posicion.'" '.$campoUpdate.' WHERE id='.$this->id;
		if(!$this->id)
			$query='REPLACE INTO '.$this->nombreTabla.' (id,texto_opcional,id_niv_one,id_niv_two,title_banner,alto,ancho,auto_play,posicion '.$campoImg.$campoThumb.') VALUES ('.$this->id.', "'.utf8_decode($this->texto_opcional).'",'.$this->id_niv_one.','.$this->id_niv_two.',"'.$this->title_banner.'","'.$this->alto.'","'.$this->ancho.'",'.$this->auto_play.',"'.$this->posicion.'" '.$_img.$_imgThumb.')';

		
//echo $query.'VERIFICAR ELIMINACION DE IMAGEN<br /><pre>';print_r($_REQUEST);print_r($this);exit;
//echo $query.'<br /><pre>';print_r($_REQUEST);print_r($_FILES);exit;
		$this->sql->query($query);
		return;
	}

//******************************************************
//	FORMULARIO DE EDICIÓN /INSERCIÓN DE DATOS
//******************************************************
	function formulario_datos($nombreForm=0)
	{
		if($this->id) echo '<tr><td colspan="20" height="10">&nbsp;</td></tr>';
?>
<tr>
	<td align="right" title="Posición del nivel principal">En p&aacute;gina:&nbsp;</td>
	<td colspan="3" align='left' title="Posición del nivel principal">
<?php
		$query='SELECT mlo.id,mlo.etiqueta,i.prefijo FROM '.$this->sql->db.'.mnu_level_one AS mlo INNER JOIN '.$this->sql->db.'.idiomas AS i ON i.id=mlo.id_idioma ORDER BY mlo.etiqueta';
		$res=$this->sql->query($query);

		echo '<select id=\'id_niv_one\' name=\'id_niv_one\' style=\'width:190px\' onchange=\'actualizarSubPagina(this)\'><option value="-1" class="tx10">seleccionar</option>';
		while($arra=$this->sql->fila2($res))
		{
			echo '<option value=\''.$arra['id'].'\' ';
			if($this->id_niv_one==$arra['id'])
				echo 'selected=\'selected\'';
			echo '>'.$arra['etiqueta'].($_SESSION['forcedLangSite']? '' :' - '.$arra['prefijo']).'</option>';
		}
		echo '</select>';
?>
	</td>

	<td align="right">Titular:</td>
	<td colspan="3"><input type='text' id='title_banner' name='title_banner' value='<?=$this->title_banner?>' style='width:94%'></td>

	<td align="right" valign="top">Texto Opcional:<br /><a class='btn ajax editAjax' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?><?='&innerTextHeight='.$this->innerTextHeight?>&nombreCampo=texto_opcional" title="">HTML</a><br /></td>
	<td valign="top" rowspan="2"><textarea i='texto_opcional' name='texto_opcional' style='width:95%' rows="4" ><?=$this->texto_opcional?></textarea></td>
</tr>

<tr>
	<td align='right' title="Posición en el segundo nivel">Página nivel 2:</td>
	<td colspan="3" align='left' title="Posición en el segundo nivel">
<?php
		if(isset($this->id_niv_two))
		{
			$salida='';
			$query='SELECT * FROM '.$this->sql->db.'.mnu_level_two WHERE id_level_one='.$this->id_niv_one;
			$res2=$this->sql->query($query);

			while($arra=$this->sql->fila2($res2))
			{
				$salida.='<option value=\''.$arra['id'].'\'';
				if($this->id_niv_two == $arra['id']) $salida.=' selected="selected"';
				$salida.='>'.$arra['etiqueta'].'</option>';
			}
		}
		echo '<select name="id_niv_two" id="id_niv_two" style=\'width:190px\'>';
		echo '<option value=\'0\' style="color:#888">No tiene</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
	<td align="right" <?=$this->id?'valign="top"':''?>>Alto:</td>
	<td <?=$this->id?'valign="top"':''?>><input type='text' name='alto' value='<?=$this->alto?>' style='width:70px'></td>
	<td align="right" <?=$this->id?'valign="top"':''?>>Ancho:</td>
	<td <?=$this->id?'valign="top"':''?>><input type='ancho' name='ancho' value='<?=$this->ancho?>' style='width:60px'></td>
</tr>

<tr>
	<td align="right" <?=$this->id?'valign="top"':''?>>Auto-play:</td>
	<td <?=$this->id?'valign="top"':''?>>
<?php
		echo '<select id="auto_play" name="auto_play" style=\'width:50px;\' title="Se debe activar automaticamente?">';
		echo '<option value=-1 ';
		if($this->auto_play==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->auto_play==0 && isset($this->auto_play))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->auto_play==1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>
	<td align="right" title="Posición respecto a las demás imagenes del carrousel" <?=$this->id?'valign="top"':''?>>Posición:</td>
	<td <?=$this->id?'valign="top"':''?>><input type='text' id='posicion' name='posicion' value='<?=$this->posicion?>' style='width:40px;text-align:right' title="Posición respecto a las demás imagenes del carrousel"></td>
	<td align='right' valign="top" title="Tamaño máximo permitido: <?=(number_format(($this->tamanioMaximoImagenes/1000),0,',','.'))?>Kb">
<?php
		if($this->id && $this->imagen)	// SE DUBUJA EL BOTÓN PARA ABRIR LA IMAGEN A TANAÑO REAL EN UNA VENTANA POP
		{
			$dat=getimagesize($this->path_imagenes.$this->imagen);
			echo '<a href="#null" class="btn" style="border:2px solid green" onclick="im(\''.$this->path_imagenes.$this->imagen.'\','.$dat[0].','.$dat[1].')" title="click para ver la imagen">&nbsp;Imagen </a>';
		}
		else
			echo 'Im&aacute;gen:';
?>
  </td>
	<td valign="top" colspan="3"><input type="file" id="imagen" name="imagen" style="width:98%" />&nbsp;
<?php
		if($this->id && $this->imagen)	// SE VISYALIZA LA CASILLA PARA BORRAR LA IMAGEN SOLO EN EDICIÓN DE REGISTRO
			echo '<br /><span title="Marcar para eliminar la imágen">Eliminar imagen: <input type="checkbox" id="quitar_archivo" name="quitar_archivo" value="1" style="border:0px;height:14px;width:14px"></span>';
		if($this->id && !$this->imagen)	// SE VISYALIZA LA CASILLA PARA BORRAR LA IMAGEN SOLO EN EDICIÓN DE REGISTRO
			echo '<br /><span title="Este elemento del carrousel NO tiene la imágen" style="color:#000;background-color:orange"><b>&nbsp;SIN IMAGEN!&nbsp;</b></span>';
?>
	</td>

	<td align='right' valign="top" title="Tamaño máximo permitido: <?=(number_format(($this->XXXX/1000),0,',','.'))?>Kb">
<?php
		if($this->id && $this->thumbnail)
		{
			$dat=getimagesize($this->path_imagenes.$this->thumbnail);
			echo '<a href="#null" class="btn" style="border:2px solid green" onclick="im(\''.$this->path_imagenes.$this->thumbnail.'\','.$dat[0].','.$dat[1].')" title="click para ver la miniatura">&nbsp;Miniatura </a>';
		}
		else
			echo 'Miniatura:';
?>
  </td>
	<td valign="top" colspan="3"><input type="file" id="thumbnail" name="thumbnail" style="width:98%" />&nbsp;
<?php
		if($this->id && $this->thumbnail)
			echo '<br /><span title="Marcar para eliminar la imágen">Eliminar Mniatura: <input type="checkbox" id="quitar_miniatura" name="quitar_miniatura" value="1" style="border:0px;height:14px;width:14px"></span>';
		if($this->id && !$this->thumbnail)
			echo '<br /><span title="Este elemento del carrousel NO tiene la imágen" style="color:#000;background-color:orange"><b>&nbsp;SIN MINIATURA!&nbsp;</b></span>';
?>
	</td>
</tr>
<?php

	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
var txPagina = new Array();
var idPagina = new Array();

$(function(){
	//editor jQuery color box
	$(".editAjax").colorbox({
		opacity: 0.4,
		initialHeight:"10px",
		initialWidth:"10%",
		close:true,
		overlayClose:true,
		close: "<div class=btn style=position:absolute;width:32px;right:16px;text-align:right;top:1px; title=\"cerrar este recuadro\">cerrar</div>",
		width:"<?=$this->id ? 90 : 74 ?>%",
		height:"100%",
		iframe:true,
		html:true,
		innerHeight:true
	});
});

function removeOptionSelected(nombreSelect)	// ELIMINA TODOS LOS ELEMENTOS DEL SELECT 'nombreSelect'
{
	txPagina = new Array();
	idPagina = new Array();
	var elSel = document.getElementById(nombreSelect);
	for(i=elSel.length;i>=0;i--)	// CAMBIAR 'i>0' A 'i>=0' SI HAY QUE ELIMINAR TODAS LAS OPCIONES DEL SELECT O 'i>=0' SI HAY QUE QUITAR TAMBIÉN 'seleccionar'
		elSel.remove(i);
	return;
}

function actualizarSubPagina(elemento)	// ACTUALIZAR EL SELECT DE LA 'SECCIÓN' DESDE EL SELECT DEL 'AREA'
{
	if(document.f<?=$idunico?>.id_niv_one.value>-1 && elemento.name=='id_niv_one')
		document.f<?=$idunico?>.id_niv_two.options[0].selected=true;

	if(document.f<?=$idunico?>.id_niv_one.value==-1 && elemento.name=='id_niv_one')
	{
		document.f<?=$idunico?>.id_niv_one.style.backgroundColor='red';
		alert('Atención, esta selección no es válida!');
		document.f<?=$idunico?>.id_niv_one.focus();
		document.f<?=$idunico?>.id_niv_one.style.backgroundColor='';
		return;
	}

	document.f<?=$idunico?>.ref_desplegable.value=elemento.name;
	tmpAction=document.f<?=$idunico?>.action;
	document.f<?=$idunico?>.target="ifrAopyoPreg";

	document.f<?=$idunico?>.target='ifrAopyoPreg';
	document.f<?=$idunico?>.action='edicion_contenidos_publicos_iframe.php';
	document.f<?=$idunico?>.submit();

	document.f<?=$idunico?>.action=tmpAction;
	document.f<?=$idunico?>.target='_self';
	return;
}

function appendOptionLast(id,texto,nombreSelect)	// AÑADE ELEMENTOS (OPTIONS) A LA SELECT 'nombreSelect'
{//alert(texto);return;
  var elOptNew = document.createElement('option');
  elOptNew.text = texto;
  elOptNew.value = id;
  var elSel = document.getElementById(nombreSelect);
  try	//intenta la instrucción siguiente, si lo consigue, ejecuta
	{
    elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
  }
  catch(ex)	// si falla la anterior instrucción, se ejecutará la siguiente
	{
    elSel.add(elOptNew); // IE only
  }
}

function revisa()
{
<?php
		$salidaControl=(isset($this->id))? 'modificacion="'.$this->id.'";':'modificacion=0;';
		echo $salidaControl;
?>
	if(document.f<?=$idunico?>.id_niv_one.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.id_niv_one);
		document.f<?=$idunico?>.id_niv_one.focus();
		alert('Por favor, seleccione un contenido para este elemento del carrousel');
		restauraCampo(document.f<?=$idunico?>.id_niv_one);
		return;
	}
	if(document.f<?=$idunico?>.imagen.value=='' && !modificacion)
	{
		resaltarCampo(document.f<?=$idunico?>.imagen);
		document.f<?=$idunico?>.imagen.focus();
		alert('Por favor, incluya la imágen para este elemento del carrousel');
		restauraCampo(document.f<?=$idunico?>.imagen);
		return;
	}
//alert(document.f<?=$idunico?>.imagen.value)
/*
	if(document.f<?=$idunico?>.alto.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.alto);
		document.f<?=$idunico?>.alto.focus();
		alert('Por favor, escriba el alto del recuadro para el carrousel');
		restauraCampo(document.f<?=$idunico?>.alto);
		return;
	}
	
	if(document.f<?=$idunico?>.ancho.value=="")
	{
		resaltarCampo(document.f<?=$idunico?>.ancho);
		document.f<?=$idunico?>.ancho.focus();
		alert('Por favor, escriba el ancho del recuadro para el carrousel');
		restauraCampo(document.f<?=$idunico?>.ancho);
		return;
	}
*/
	err=controlNumerico(document.f<?=$idunico?>.posicion.value,'posicion','f<?=$idunico?>',1);

	if(err)
	{
		document.f<?=$idunico?>.posicion.value='';
		return false;
	}

	document.f<?=$idunico?>.submit();
}


function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}
-->
</script>
<iframe name='ifrAopyoPreg' id='ifrAopyoPreg' style='display:none;width:100%' src=''></iframe>

<?php
		$camposAdicionales='<input type="hidden" id="ref_desplegable" name="ref_desplegable" value="">
<input type="hidden" name="en_portada" value="'.$this->en_portada.'">
';
		include('_inc/form_std_edit.php');
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->id_niv_one=$datos['id_niv_one'];
		$this->id_niv_two=$datos['id_niv_two'];
		$this->title_banner=$datos['title_banner'];
		$this->alto=$datos['alto'];
		$this->ancho=$datos['ancho'];
		$this->auto_play=$datos['auto_play'];
		$this->posicion=$datos['posicion'];
		$this->imagen=$datos['imagen'];
		$this->texto_opcional=utf8_encode($datos['texto_opcional']);
		$this->quitar_archivo=$datos['quitar_archivo'];
		$this->quitar_miniatura=$datos['quitar_miniatura'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT imagen,thumbnail FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$_REQUEST['id'].'';
		$res=$this->sql->query($query);
		while($arra=$this->sql->fila2($res))
		{
			@unlink($this->path_imagenes.$arra['imagen']);
			@unlink($this->path_imagenes.$arra['thumbnail']);
		}

		$query='DELETE FROM '.$this->nombreTabla." WHERE id='".$this->id."'";
//echo $query.'<pre>';print_r($_REQUEST);print_r($this);exit;
		$this->sql->query($query);
		return;
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='Número registros: ')
	{
		$idunico=rand(1,10000).time();
		if (count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la búsqueda:</span> ";

		$query='SELECT * FROM '.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$querytotal='SELECT COUNT(*) FROM '.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='imagen'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' DESC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//******************************************************


//******************************************************
//Formulario para efectuar búsquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->

<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='780' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='5'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td align="right" title="Buscar en el Nivel 1" valign="top">Nivel 1:</td>
	<td colspan="2" title="Buscar en el Nivel 1" valign="top">
<?php
		$salida='';
		$query='SELECT c.titular,c.id,i.prefijo FROM '.$this->sql->db.'.contenidos AS c INNER JOIN idiomas AS i ON c.id_idioma=i.id ORDER BY c.titular';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_niv_one_buscN'] == $arra['id']) $salida.=' selected="selected"';
			$salida.='>'.(strlen($arra['titular'])>1?$arra['titular']:'!!p&aacute;g. sin idioma !!').($_SESSION['forcedLangSite']?'' :' - '.$arra['prefijo'].'').'</option>';
		}

		echo '<select id="'.$this->posicionSolapa.'id_niv_one_buscN" name="'.$this->posicionSolapa.'id_niv_one_buscN" style=\'width:190px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
	<td align="right" title="Apartado de nivel 2">Nivel 2:</td>
	<td>
<?php
		$filtro2=$_SESSION['filtros'][$this->posicionSolapa.'id_niv_one_buscN']?' WHERE id_level_one='.$_SESSION['filtros'][$this->posicionSolapa.'id_niv_one_buscN']:'';
		$query='SELECT mlt.*,i.prefijo FROM '.$this->sql->db.'.mnu_level_two AS mlt INNER JOIN '.$this->sql->db.'.idiomas AS i ON i.id=mlt.id_idioma '.$filtro2.' ORDER BY mlt.etiqueta';
		$res2=$this->sql->query($query);

		echo '<select id=\''.$this->posicionSolapa.'id_niv_two_buscN\' name=\''.$this->posicionSolapa.'id_niv_two_buscN\' style=\'width:120px\'><option value=\'-1\'>?</option>';
		while($arra2=$this->sql->fila2($res2))
		{
			echo '<option value=\''.$arra2['id'].'\' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_niv_two_buscN']==$arra2['id'])
				echo 'selected=\'selected\'';
			echo '>'.$arra2['etiqueta'].($_SESSION['forcedLangSite']?'' :' - '.$arra2['prefijo']).'</option>';
		}
		echo '</select>';
?>
	</td>
	<td>Texto:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>texto_opcional_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'texto_opcional_busca_u']?>' style='width:110px'></td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->

<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'65%','','81%','9%');

		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('Titular','P&aacute;g. niv 1','Sub-p&aacute;gina','Texto (opcional)','Auto-play','Imágen','Posic.');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id','title_banner','id_niv_one','id_niv_two','texto_opcional','auto_play','imagen','posicion');
		$xc=0; $arraLongitudes=array($xc++=>48,$xc++=>42,$xc++=>42,$xc++=>34,$xc++=>60,$xc++=>60,$xc++=>60); // no se incluye el valor para el ID
		$xc=0; $arraLongitudesTitulares=array($xc++=>25,$xc++=>140,$xc++=>150,$xc++=>130,$xc++=>160,$xc++=>62,$xc++=>110,$xc++=>42);
		$xc=0; $arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'txt',$xc++=>'txt',$xc++=>'txt',$xc++=>'txt',$xc++=>'img',$xc++=>'num');
		$this->verIdRegistros=0;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$titleBannerShow=$a['title_banner'] ? $a['title_banner'] : '<center>-</center>';
			$xc=0; $estaActivo='<center style=color:orange>no</center>'; $esAutoplay='<center style=color:orange>no</center>';
			$query='SELECT CONCAT(mlo.etiqueta," (<i>",i.prefijo,"</i>)") FROM '.$this->sql->db.'.mnu_level_one AS mlo INNER JOIN '.$this->sql->db.'.idiomas AS i ON i.id=mlo.id_idioma WHERE mlo.id='.$a['id_niv_one'];
			$nombrePagina=$this->sql->valor($query);

			$query='SELECT CONCAT(mlo.etiqueta," (<i>",i.prefijo,"</i>)") FROM '.$this->sql->db.'.mnu_level_two AS mlo INNER JOIN '.$this->sql->db.'.idiomas AS i ON i.id=mlo.id_idioma WHERE mlo.id=\''.$a['id_niv_two'].'\'';

			$nombreSubPagina=$this->sql->valor($query);
			if(!$nombreSubPagina) $nombreSubPagina='<i style=color:'.$_SESSION['colorTextLoose'].'>ninguna</i>';

			if(!$titularContenido) $titularContenido='<span style=color:'.$_SESSION['colorTextWarning'].'>--</span>';
			$laImagen=$a['imagen']?$this->path_imagenes.'/'.$a['imagen']:'';

			if($a['activo']) $estaActivo='<center style=color:'.$_SESSION['colorTextWarning'].'><b>SI</b></center>';
			if($a['auto_play']>0) $esAutoplay='<center style=color:'.$_SESSION['colorTextWarning'].'><b>SI</b></center>';
			$textoOpcionalReducido=str_replace('<p>',' ',$a['texto_opcional']);
			$textoOpcionalReducido=str_replace('<br />',' ',$textoOpcionalReducido);
			$textoOpcionalReducido=str_replace('<br>',' ',$textoOpcionalReducido);
			$textoOpcionalReducido=strip_tags($textoOpcionalReducido,'<b><i><u>');

			$arraDatos[]=array($xc++=>$a['id'],$xc++=>$titleBannerShow,$xc++=>$nombrePagina,$xc++=>$nombreSubPagina,$xc++=>$textoOpcionalReducido,$xc++=>$esAutoplay,$xc++=>$laImagen,$xc++=>$a['posicion']);
		}

		$idRefPops=0;
		$txtRefEliminar=1;
		$posicFondo=0;	// NUMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  FIN DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>