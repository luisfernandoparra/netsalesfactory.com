<?php
header('Content-Type: text/html; charset=utf-8');
@session_start();
include('_cnf/sesion.php');
include('_cnf/colores.php');
$altoCentral=580;
$anchoCentral=900;
setlocale(LC_ALL,'es_ES');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content='IE=EmulateIE7' http-equiv='X-UA-Compatible'/>
<title><?=$_SESSION['nombre_site']?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv='imagetoolbar' content='no'/>
<link rel='stylesheet' href='_css/estilos.css' type='text/css' />
<link rel='stylesheet' href='_css/colorbox2013.css' type='text/css' />
<link rel='stylesheet' href='_css/backoffice.css' type='text/css' />
<!--<link rel='stylesheet' href='_css/jquery.qtip.css' type='text/css' />-->

<!-- START POPLINE -->
<link rel='stylesheet' href='_css/popline_themes/default.css' type='text/css' />
<link rel='stylesheet' href='_css/normalize.css' type='text/css' />
<link rel='stylesheet' href='_css/toggle-switch.css' type='text/css' />
<link rel='stylesheet' href='_css/tooltipster.css' type='text/css' />
<link rel='stylesheet' href='font-awesome/css/font-awesome.min.css' type='text/css' />
<!-- END POPLINE -->

<?php
if(count($objeto->cssScriptPrivados))
{
	foreach($objeto->cssScriptPrivados as $cssModulo=>$tipo)
	{
		if($tipo=='css')
		{
			echo '
<link rel="stylesheet" href="'.$cssModulo.'" type="text/css" />
';
		}
		else
		{
			echo'<style type="text/css">
<!--
';
			include_once($cssModulo);
			echo'
-->
</style>
';
		}
	}
}
?>
<script type='text/javascript' src='_js/standar.js'></script>
<!--<script type="text/javascript" src="_js/jquery-1.9.1.min.js"></script>-->
<script type="text/javascript" src="_js/jquery-1-8-2.min.js"></script>
<script type="text/javascript" src="_js/modernize_custom.js"></script>
<script type='text/javascript' src='_js/jquery.colorbox-min.js'></script>
<!--<script type='text/javascript' src='_js/jquery.qtip-1.0.0-rc3.min.js'></script>-->
<script type='text/javascript' src='_js/jquery.tooltipster.min.js'></script>
<script language="javascript" type="text/javascript" src="_js/jquery.mask.min.js"></script>

<!-- START POPLINE -->
<script type='text/javascript' src='_js/jquery.popline.min.js'></script>
<!-- END POPLINE -->

<style type="text/css">
<!--
<?php
include('_css/estilos_generales.php');
include('_css/estilos_dinamicos.php');
?>

-->
</style>
<div id="cboxOverlay" style="position:absolute;display:none;height:100%;width:100%;opacity:0.1;cursor:auto;z-index:1"></div>

<!-- START TCAL-->
<link rel="stylesheet" type="text/css" href="_css/tcal.css" />
<script type="text/javascript" src="_js/tcal.js"></script>
<!-- END TCAL-->

<script type="text/javascript">

// VISUALIZAR CUALQUIER MENSAJE DE ERROR PARA LOS FORMULARIOS DEL ADMIN
function displayFormError(refElement,message)
{
	resaltarCampo($(refElement)[0]);
	$(refElement).focus();
	alert(message);
	restauraCampo($(refElement)[0]);
	return true;
}

$(document).ready(function(){
	$('.tooltip').tooltipster({
		contentAsHTML:true
	});
/*
//	CUSTOM STANDARD TOOLTIPS
	$("td,a,.icoDocument,.blockIcons[title],.qtipForced").qtip({
		position:{
			adjust:{resize:true},
			corner:{
				target:'centerMiddle',
				tooltip:'centerMiddle'
			},
			adjust:{x:-60,y:10}
			},
			show:{effect:function(){$(this).animate({marginLeft:5},100).animate({marginLeft:0,opacity:"show" });}}
	});

	$(".qtipForcedLeft").qtip({
		position:{
				target:'mouse',adjust:{mouse:false},
				adjust:{x:-100,y:-220}
			},
			style:{
      border:{
				 color:'#000',
				 padding:0,
				 margin:0
				}
			},
			show:{effect:function(){$(this).fadeIn("dast").animate({marginLeft:-0,opacity:"show" });}}
	});

//	CUSTOM TEXTAREA TOOLTIPS
	$("textarea[title]").qtip({
		position:{adjust:{x:-100,y:-100}}
		,show:{effect:function(){$(this).animate({marginLeft:5},100).animate({marginLeft:0,opacity:"show"});}}
		,hide:{event:false,inactive:3000}
	})

//	CUSTOM CLASS ".blockIcons" TOOLTIPS
	$(".icoDocument[title]").qtip({
		style:{width:240}
	})

//	CUSTOM BUTTON "btnEndTransaction" TOOLTIPS
	$("#btnEndTransaction[title]").qtip({
		position:{at:'left',adjust:{y:-60}}
	})
*/
	$(".editorPopline").popline();

  $(".directHTML").colorbox({	// MODAL WINDOW TO EDIT HTML DIRECT CODE
	  opacity:0.7,
	  initialHeight:"60px",
	  initialWidth:"60%",
	  close:true,
	  overlayClose:true,
	  width:"<?=$_REQUEST['id'] ? '98%' : '80%'?>%",
	  height:"<?=$_REQUEST['id'] ? '98%' : '90%'?>%",
	  title:"Edición de código HTML",
	  iframe:true,
	  innerHeight:true,
	  onOpen:function(){$("#cboxBottomCenter").html(" <i>cargando código HTML...</i>")},
	  onClosed:function(){$("#cboxBottomCenter").html("")},
	  onComplete:function(){$("#cboxBottomCenter").html("").fadeIn()}
  });

//	var bodyH=parent.parent.$("body").innerHeight();
	var bodyH=parent.innerHeight;
	parent.$(".iframeSolapa").css("height",(bodyH-120)+"px");
})
</script>

<?php
/*
 * MODAL DE EDICION DE UN CAMPO EN
 * FORMATO HTML PURO, GESTIONADO ENTERAMENTE
 * CON JAVASCRIPT, UNICO CON PHP ID CAMPO A TRATAR
 * (2014.05.30)
 */
if($_REQUEST['accion'] == 'directHTML')
{
	echo'
<style>
.directEditField{
display:inline-block;
width:99%;
min-height:100px;
height:98%;
}
</style>
';
?>
<textarea name="tmpDataEdit" id="tmpDataEdit" class="directEditField">recuperando datos...</textarea>
<br /><center><br /><br /><a href="#" class="btn updateHtmlField">Actualizar</a>&nbsp;&nbsp;&nbsp;<a href="javascript:parent.jQuery.colorbox.close();" class="btn">cancelar</a></center>
<script type="text/javascript">
$(document).ready(function(){
	var pHeight=window.parent.$("body").innerHeight();
	pHeight=pHeight/100*<?=$_REQUEST['fieldName'] ? '72' : '95'?>;
	$("#tmpDataEdit").css("height",pHeight);
	$("#tmpDataEdit").val(window.parent.$("#<?=$_REQUEST['fieldName']?>").html());

	$(".updateHtmlField").click(function(){
		window.parent.$("#<?=$_REQUEST['fieldName']?>").html($("#tmpDataEdit").val());
		parent.jQuery.colorbox.close();
	});
});
</script>

<?php
	die();
}
?>
