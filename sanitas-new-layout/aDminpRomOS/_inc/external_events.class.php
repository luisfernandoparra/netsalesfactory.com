<?php
class externalEvents extends Base
{
	//Datos propios de la clase
	var $id;
	var $activo;
	var $link;
	var $ajuteRSS;

//******************************************************
//	INICIALIZACI�N DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla='external_events';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las b�squedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset']==1) unset($_SESSION['filtros']);
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE ID="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->link=$this->output_datos($a['link']);
		$this->activo=$a['activo'];
		$this->id_idioma=$a['id_idioma'];
		$this->cad_start_tit=$a['cad_start_tit'];
		$this->cad_end_tit=$a['cad_end_tit'];
		$this->cad_start_des=$a['cad_start_des'];
		$this->cad_end_des=$a['cad_end_des'];
		$this->cad_start_pub=$a['cad_start_pub'];
		$this->cad_end_pub=$a['cad_end_pub'];
		$this->cad_start_link=$a['cad_start_link'];
		$this->cad_end_link=$a['cad_end_link'];
		$this->ajuteRSS=$a['ajuteRSS'];
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		if($this->password)
		{
			$this->password=md5($this->password);
			$laPass=",'".$this->password."'";
			$campoPass=', password';
			$campoPassUpdate=',password="'.$this->password.'"';
		}

		if(!$_REQUEST['id']) $_REQUEST['id']=0;
		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id,link,cad_start_tit,cad_end_tit,cad_start_des,cad_end_des,cad_start_pub,cad_end_pub,cad_start_link,cad_end_link,id_idioma,activo,ajuteRSS'.$campoPass.") VALUES ('".$this->id."', '".$this->link."','".$this->cad_start_tit."','".$this->cad_end_tit."','".$this->cad_start_des."','".$this->cad_end_des."','".$this->cad_start_pub."','".$this->cad_end_pub."','".$this->cad_start_link."','".$this->cad_end_link."','".$this->id_idioma."','".$this->activo."','".$this->ajuteRSS."'".$laPass.")";
/*
		else
			$query='UPDATE '.$this->sql->db.'.'.$this->nombreTabla.' SET link="'.$this->link.'",usuario="'.$this->usuario.'",apellidos="'.$this->apellidos.'",nivel_acceso="'.$this->nivel_acceso.'",activo="'.$this->activo.'",ajuteRSS="'.$this->ajuteRSS.'" '.$campoPassUpdate.' WHERE id='.$_POST['id'];
*/
//echo $query;echo '<pre>'; print_r($_REQUEST);exit;
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos()
	{
?>
<tr>
	<td align="right">Link:</td>
	<td colspan="4"><input type='text' id='link' name='link' value='<?=$this->link?>' style='width:200px'></td>
	<td align="right">Idioma:</td>
	<td colspan="2">
<?php
		// START SE CONTRUYE EL ARRAY CON TODOS LOS ELEMENTOS DE MEN�S 
		$salida='';
		$query='SELECT * FROM '.$this->sql->db.'.idiomas ORDER BY idioma';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->id_idioma== $arra['id']) $salida.=' selected="selected"';
			$salida.='>'.$arra['idioma'].'</option>';
		}

		echo '<select id="id_idioma" name="id_idioma" onchange="actualizarSubPagina(this)" style=\'width:110px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
	<td title='' align="right">Activo:</td>
	<td title=''>
<?php
		$esActivo=array(0=>'NO',1=>'S�');
		echo '<select id="activo" name="activo" style=\'width:50px;\' title="Nivel de acceso del usuario al Back-office">';
		echo '<option value=-1 ';
		if($this->activo==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->activo==$n)
				echo 'selected=\'selected\'';
			echo ' >'.$esActivo[$n].'</option>';
		}
		echo '</select>';
?>
	</td>
	<td align="right">ajuteRSS:</td>
	<td title=''>
<?php
		$tieneAjuteRSS=array(0=>'NO',1=>'S�');
		echo '<select id="ajuteRSS" name="ajuteRSS" style=\'width:50px;\' title="Nivel de acceso del usuario al Back-office">';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->ajuteRSS==$n)
				echo 'selected=\'selected\'';
			echo ' >'.$tieneAjuteRSS[$n].'</option>';
		}
		echo '</select>';
?>
	</td>
</tr>
<tr>
	<td align="right">start_title:</td>
	<td><input type='text' id='cad_start_tit' name='cad_start_tit' value='<?=$this->cad_start_tit?>' style='width:50px'></td>
	<td align="right">end_title:</td>
	<td><input type='text' id='cad_end_tit' name='cad_end_tit' value='<?=$this->cad_end_tit?>' style='width:50px'></td>
	<td align="right">start_desc:</td>
	<td><input type='text' id='cad_start_des' name='cad_start_des' value='<?=$this->cad_start_des?>' style='width:50px'></td>
	<td align="right">end_desc:</td>
	<td><input type='text' id='cad_end_des' name='cad_end_des' value='<?=$this->cad_end_des?>' style='width:50px'></td>
	<td align="right">start_pub:</td>
	<td><input type='text' id='cad_start_pub' name='cad_start_pub' value='<?=$this->cad_start_pub?>' style='width:50px'></td>
	<td align="right">end_pub:</td>
	<td><input type='text' id='cad_end_pub' name='cad_end_pub' value='<?=$this->cad_end_pub?>' style='width:50px'></td>
</tr>
<tr>
	<td align="right">start_link:</td>
	<td><input type='text' id='cad_start_link' name='cad_start_link' value='<?=$this->cad_start_link?>' style='width:50px'></td>
	<td align="right">end_link:</td>
	<td><input type='text' id='cad_end_link' name='cad_end_link' value='<?=$this->cad_end_link?>' style='width:50px'></td>
</tr>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
	if(document.f<?=$idunico?>.link.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.link);
		document.f<?=$idunico?>.link.focus();
		alert('Por favor, escriba la URL de la Web');
		restauraCampo(document.f<?=$idunico?>.link);
		return;
	}
	if(document.f<?=$idunico?>.id_idioma.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.id_idioma);
		document.f<?=$idunico?>.id_idioma.focus();
		alert('Por favor, seleccione el idioma');
		restauraCampo(document.f<?=$idunico?>.id_idioma);
		return;
	}
	if(document.f<?=$idunico?>.activo.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.activo);
		document.f<?=$idunico?>.activo.focus();
		alert('Por favor, indique si est� activo');
		restauraCampo(document.f<?=$idunico?>.activo);
		return;
	}
	if(document.f<?=$idunico?>.cad_start_tit.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.cad_start_tit);
		alert('Por favor, escriba el TAG');
		document.f<?=$idunico?>.cad_start_tit.focus();
		restauraCampo(document.f<?=$idunico?>.cad_start_tit);
		return;
	}

	document.f<?=$idunico?>.submit();
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}
-->
</script>
<?php
		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->link=$datos['link'];
		$this->activo=$datos['activo'];
		$this->id_idioma=$datos['id_idioma'];
		$this->ajuteRSS=$datos['ajuteRSS'];
		$this->cad_start_tit=$datos['cad_start_tit'];
		$this->cad_end_tit=$datos['cad_end_tit'];
		$this->cad_start_des=$datos['cad_start_des'];
		$this->cad_end_des=$datos['cad_end_des'];
		$this->cad_start_pub=$datos['cad_start_pub'];
		$this->cad_end_pub=$datos['cad_end_pub'];
		$this->cad_start_link=$datos['cad_start_link'];
		$this->cad_end_link=$datos['cad_end_link'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='DELETE FROM '.$this->nombreTabla." WHERE id='".$this->id."'";
		$this->sql->query($query);
		return;
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N�mero registros: ')
	{
		$idunico=rand(1,10000).time();
		if(count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b�squeda:</span> ";

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='link'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' DESC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $query;
//******************************************************


//******************************************************
//Formulario para efectuar b�squedas
//******************************************************
	$colorTextoMsg='#CCD2D6';
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>
<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->

<table align='center' cellpadding='3' cellspacing='0' class='borde' width='780' border='0'>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>

<tr>
	<td>link:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>link_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'link_busca_u']?>' style='width:210px'></td>
	<td width='30'>idioma:</td>
	<td title='Idiomas disponibles'>
<?php
		$query='SELECT * FROM '.$this->sql->db.'.idiomas ORDER BY idioma';
		$res2=$this->sql->query($query);

		echo '<select id="'.$this->posicionSolapa.'id_idioma_buscN" name="'.$this->posicionSolapa.'id_idioma_buscN" style=\'width:110px;\' title="Idiomas disponibles">';
		echo '<option value=-1 ';
		if($this->posicion==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		while($arra2=$this->sql->fila2($res2))
		{
			echo '<option value='.$arra2['id'].' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_idioma_buscN']==$arra2['id'])
				echo 'selected=\'selected\'';
			echo ' >'.$arra2['idioma'].'</option>';
		}
		echo '</select>';
?>
	</td>
	<td title='' align="right">Activo:</td>
	<td title=''>
<?php
		$esActivo=array(0=>'NO',1=>'S�');
		echo '<select id="'.$this->posicionSolapa.'activo_buscN" name="'.$this->posicionSolapa.'activo_buscN" style=\'width:50px;\' title="Nivel de acceso del usuario al Back-office">';
		echo '<option value=-1 ';
		if($this->activo==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'activo_buscN']==$n && isset($_SESSION['filtros'][$this->posicionSolapa.'activo_buscN']))
				echo 'selected=\'selected\'';
			echo ' >'.$esActivo[$n].'</option>';
		}
		echo '</select>';
?>
	</td>

	<td align="right">
    <table width="98" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="48%"><a href="#null" class='btn' onclick="document.formListados<?=$idunico?>.submit()" style="width:90px;" >Buscar</a></td>
        <td>&nbsp;</td>
        <td width="48%"><a href="#null" class='btn' onclick="document.location='<?=$destinobusca?>.php?reset=1'" style="width:60px;color:orange">Reset</a></td>
      </tr>
    </table>
  </td>
</tr>
</table>
<!-- *******************  fin formulario b�squedas  *************** !-->

<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'36%','','75%','14%');

		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('Idioma','Link URL','Activo','ajuteRSS');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id','id_idioma','link','activo','ajuteRSS');
		$xc=0; $arraLongitudes=array($xc++=>40,$xc++=>50,$xc++=>60,$xc++=>50); // no se incluye el valor para el ID
		$xc=0; $arraLongitudesTitulares=array($xc++=>25,$xc++=>60,$xc++=>380,$xc++=>100,$xc++=>60);
		$xc=0; $arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'txt',$xc++=>'txt',$xc++=>'txt');

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$xc=0;
			$esActivo=($a['activo']==1)?'<span style=color:#fff>SI</span>':'<span style=color:#C38000><b>NO</b></style>';
			$conAjuteRSS=($a['ajuteRSS']==1)?'<span style=color:#fff>SI</span>':'<span style=color:#C38000><b>NO</b></style>';
			$query2='SELECT prefijo FROM '.$this->sql->db.'.idiomas WHERE id='.$a['id_idioma'];
			$prefBanderita=strtoupper($this->sql->valor($query2));

			$arraDatos[]=array($xc++=>$a['id'],$xc++=>$prefBanderita,$xc++=>$a['link'],$xc++=>$esActivo,$xc++=>$conAjuteRSS);
		}

		$idRefPops=0;
		$txtRefEliminar=2;
		$posicFondo=0;	// N�MERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo,0,0,0);
		/*******************  FIN DEL LISTADO  ********************/

		if($num_res>mysql_num_rows($res) && !$this->contFilas)
		{
			foreach($_SESSION['filtros'] as $key=>$value)	// SE RECARGA LA PAGINA SI NO HAY FILAS EN PANTALLA Y HAY RESULTADOS EN LA BUSQUEDA
			{
				$pos=strpos($key,'_busca');
				if ($pos>0)
					$url.='&'.$key.'='.urlencode($value);
			}

			$_SESSION['filtros']['offset_u']=0;
			echo "<script language='JavaScript' type='text/JavaScript'>
			document.location='$destinobusca?$url';
			</script>";
		}
?>
</table>
<table align='center' cellpadding='2'>
<tr><td style='line-height:22px' align='center'><span id='spn_infoPage'></span>
<?php
	$url='';
	foreach($_SESSION['filtros'] as $key=>$value)
	{
		$pos=strpos($key,'_busca');
		if ($pos>0)
			$url.='&'.$key.'='.urlencode($value);
	}
	if(isset($_REQUEST['offset_u']))
		$offset_u=$_REQUEST['offset_u'];
	else
		$offset_u=0;

// ********************************************************************************************************
// nota importante: EL �LTIMO PAR�METRO ES LA REFERENCIA DEL IFRAME RESPECTO A LOS DEM�S DE LA P�GINA PADRE, es B�SICO
// ********************************************************************************************************
	$hay=$this->paginacion($num_res, $this->filasporpagina, $offset_u, $destinobusca.".php?filtrar=".$_REQUEST['filtrar'].$url.'&sentidoOrdenacion='.$_REQUEST['sentidoOrdenacion'].'&ordenarPor='.$_REQUEST['ordenarPor']."&offset_u=",$this->TotPagMostrar,$this->posicionSolapa);
// ********************************************************************************************************
	if(trim($hay))
		echo '<script>
		paginarBloque=1;
		function repagina(){top.document.getElementById("divPaginacion").style.display=\'block\';top.document.getElementById("divPaginacion").innerHTML=\' '.$hay.' \';}
		repagina();
</script>';
?>
<script>
var arraTxtDescripcion=new Array();
var sombraL; var sombraT;
</script>
</td></tr></table>
</form>
<?php
	}
}
?>