<?php
class Configuracion extends Base
{
	//Datos propios de la clase
	var $id;
	var $path_imagenes_concursos;
	var $arraSiNo;

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql)
	{
		$this->sql=$sql;
		$this->arraSiNo=array(0=>'No',1=>'S&iacute;');
		$this->id=$id;
		$this->nombreTabla=$this->sql->prefixTbl.'user_config';
		$this->path_imagenes_concursos='../imag_concursos/';	// RUTA PARA LAS IMAGENES
	}

//******************************************************
//
//******************************************************
	function Configuracion($sql, $id='')
	{
		$this->sql=$sql;
		$this->id=$id;
		unset($_SESSION['filtros']);
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$this->carga_datos();
	}

//******************************************************
//
//******************************************************
	function carga_datos()
	{
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$_SESSION['id_usuario'].'" ';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->id=$a['id'];
		$this->colorFondoWeb=$this->output_datos($a['colorFondoWeb']);
		$this->max_filas_noticias=$this->output_datos($a['max_filas_noticias']);
		$this->colorTextoStd=$this->output_datos($a['colorTextoStd']);
		$this->colorFondoBotones=$this->output_datos($a['colorFondoBotones']);
		$this->colorFondoOverBotones=$this->output_datos($a['colorFondoOverBotones']);
		$this->colorForeBotones=$this->output_datos($a['colorForeBotones']);
		$this->colorForeOverBotones=$this->output_datos($a['colorForeOverBotones']);
		$this->colorForeTitular=$this->output_datos($a['colorForeTitular']);
		$this->colorFondoTitular=$this->output_datos($a['colorFondoTitular']);
		$this->colorTableBordeLight=$this->output_datos($a['colorTableBordeLight']);
		$this->colorTextWarning=$this->output_datos($a['colorTextWarning']);
		$this->colorFondoWarning=$this->output_datos($a['colorFondoWarning']);
		$this->colorTextHighli=$this->output_datos($a['colorTextHighli']);
		$this->colorTextLoose=$this->output_datos($a['colorTextLoose']);
		$this->colorFondoCampos=$this->output_datos($a['colorFondoCampos']);

		$this->debugRequest=$a['debugRequest'];
		$this->debugSession=$a['debugSession'];
		$this->debugObjetos=$a['debugObjetos'];
		$this->debugBackOffice=$a['debugBackOffice'];
		$_SESSION['colorTextoStd']=$this->colorTextoStd;
		$_SESSION['colorFondoWeb']=$this->colorFondoWeb;
		$_SESSION['colorForeOverBotones']=$this->colorForeOverBotones;
		$_SESSION['colorFondoOverBotones']=$this->colorFondoOverBotones;
		$_SESSION['colorForeBotones']=$this->colorForeBotones;
		$_SESSION['colorFondoBotones']=$this->colorFondoBotones;
		$_SESSION['debugBackOffice']=$this->debugBackOffice;
		$_SESSION['colorForeTitular']=$this->colorForeTitular;
		$_SESSION['colorFondoTitular']=$this->colorFondoTitular;
		$_SESSION['colorTableBordeLight']=$this->colorTableBordeLight;
		$_SESSION['colorTextWarning']=$this->colorTextWarning;
		$_SESSION['colorFondoWarning']=$this->colorFondoWarning;
		$_SESSION['colorTextHighli']=$this->colorTextHighli;
		$_SESSION['colorTextLoose']=$this->colorTextLoose;
		$_SESSION['colorFondoCampos']=$this->colorFondoCampos;

		$this->img_checks_restore=$this->output_datos($a['img_checks_restore']);
		$this->id_usuario_email_test=$a['id_usuario_email_test'];

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'system_config WHERE id=1';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$_SESSION['forcedLangSite']=$a['forced_lang_site'];
		$_SESSION['min_level_company_edit']=$a['min_level_company_edit'];
/*
		if($this->debugBackOffice==1) echo '<script>$("divDebugIframes").show(1000);</script>';
		else echo '<script>$("divDebugIframes").hide();</script>';
*/
		return;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		$modif_imagen=0; $campoImg='';

		if (is_uploaded_file($_FILES['img_checks_restore']['tmp_name']))
		{
			$tmp_file=date('dmyis',time()).$_FILES['img_checks_restore']['name'];

//echo $this->2.$tmp_file.' ---> '.$_FILES['img_checks_restore']['tmp_name'];

			copy($_FILES['img_checks_restore']['tmp_name'], $this->path_imagenes_concursos.$tmp_file);
//{echo $this->path_imagenes_concursos.$tmp_file.' ---> '.$_FILES['img_checks_restore']['tmp_name'];exit;}
			$_img=', "'.$tmp_file.' "';
			$modif_imagen=1;
			$campoImg=',img_checks_restore';
		}

		if($this->quitar_imagen)	// SI SE HA SELECCIONADO QUITAR LA IMAGEN, SE PROCEDE A BORRARLA DE LA TABLA
		{
			$modif_imagen=1; $_img=',""'; $campoImg=',img_checks_restore';
			$query='SELECT img_checks_restore FROM '.$this->nombreTabla.' WHERE id='.$_REQUEST['id'];	// SE OBTIENE LA img_checks_restore A BORRAR
			$imagenBorrar=$this->sql->valor($query);

			if($this->path_imagenes_concursos.$imagenBorrar)	// SI EXISTE EL ARCHIVO, SE BORRA FISICAMENTE
				@unlink($this->path_imagenes_concursos.$imagenBorrar);
		}
//	colorTableBordeLight colorTextWarning
		if($this->id_usuario_email_test)
		{
			$upSqlMailTest=',id_usuario_email_test='.$this->id_usuario_email_test;
			$ReMailTestFiel=',id_usuario_email_test';
			$ReMailTestData=',\''.$this->id_usuario_email_test.'\'';
		}

		$this->colorTextoStd=($this->colorTextoStd != '' && substr($this->colorTextoStd,0,1) != '#') ? '#'.$this->colorTextoStd : $this->colorTextoStd;
		$this->colorFondoWeb=($this->colorFondoWeb != '' && substr($this->colorFondoWeb,0,1) != '#') ? '#'.$this->colorFondoWeb : $this->colorFondoWeb;
		$this->colorForeOverBotones=($this->colorForeOverBotones != '' && substr($this->colorForeOverBotones,0,1) != '#') ? '#'.$this->colorForeOverBotones : $this->colorForeOverBotones;
		$this->colorForeBotones=($this->colorForeBotones != '' && substr($this->colorForeBotones,0,1) != '#') ? '#'.$this->colorForeBotones : $this->colorForeBotones;
		$this->colorForeTitular=($this->colorForeTitular != '' && substr($this->colorForeTitular,0,1) != '#') ? '#'.$this->colorForeTitular : $this->colorForeTitular;
		$this->colorFondoTitular=($this->colorFondoTitular != '' && substr($this->colorFondoTitular,0,1) != '#') ? '#'.$this->colorFondoTitular : $this->colorFondoTitular;
		$this->colorFondoOverBotones=($this->colorFondoOverBotones != '' && substr($this->colorFondoOverBotones,0,1) != '#') ? '#'.$this->colorFondoOverBotones : $this->colorFondoOverBotones;
		$this->colorTableBordeLight=($this->colorTableBordeLight != '' && substr($this->colorTableBordeLight,0,1) != '#') ? '#'.$this->colorTableBordeLight : $this->colorTableBordeLight;
		$this->colorTextWarning=($this->colorTextWarning != '' && substr($this->colorTextWarning,0,1) != '#') ? '#'.$this->colorTextWarning : $this->colorTextWarning;
		$this->colorFondoWarning=($this->colorFondoWarning != '' && substr($this->colorFondoWarning,0,1) != '#') ? '#'.$this->colorFondoWarning : $this->colorFondoWarning;
		$this->colorTextHighli=($this->colorTextHighli != '' && substr($this->colorTextHighli,0,1) != '#') ? '#'.$this->colorTextHighli : $this->colorTextHighli;
		$this->colorTextLoose=($this->colorTextLoose != '' && substr($this->colorTextLoose,0,1) != '#') ? '#'.$this->colorTextLoose : $this->colorTextLoose;
		$this->colorFondoCampos=($this->colorFondoCampos != '' && substr($this->colorFondoCampos,0,1) != '#') ? '#'.$this->colorFondoCampos : $this->colorFondoCampos;
		$this->colorFondoBotones=($this->colorFondoBotones != '' && substr($this->colorFondoBotones,0,1) != '#') ? '#'.$this->colorFondoBotones : $this->colorFondoBotones;
		$query='REPLACE INTO '.$this->nombreTabla.' (id, colorFondoWeb, max_filas_noticias,colorTextoStd,colorForeBotones,colorFondoBotones,colorFondoOverBotones,colorForeOverBotones,colorForeTitular,colorFondoTitular,colorTableBordeLight,colorTextWarning, colorFondoWarning, colorTextHighli, colorTextLoose, colorFondoCampos, debugRequest, debugSession,debugObjetos,debugBackOffice '.$ReMailTestFiel.$campoImg.') VALUES ('.(int)$_SESSION['id_usuario'].",'".(substr($this->colorFondoWeb,0,1)=='#'?$this->colorFondoWeb:'#'.$this->colorFondoWeb)."','".$this->max_filas_noticias."', '".$this->colorTextoStd."', '".$this->colorForeBotones."', '".$this->colorFondoBotones."', '".$this->colorFondoOverBotones."', '".$this->colorForeOverBotones."', '".$this->colorForeTitular."', '".$this->colorFondoTitular."', '".$this->colorTableBordeLight."', '".$this->colorTextWarning."', '".$this->colorFondoWarning."', '".$this->colorTextHighli."', '".$this->colorTextLoose.'\',"'.$this->colorFondoCampos.'","'.$this->debugRequest.'","'.$this->debugSession.'","'.$this->debugObjetos.'", "'.$this->debugBackOffice.'" '.$ReMailTestData.$_img.')';
//echo $query.'<hr /><pre>';print_r($_REQUEST);exit;

		$_SESSION['configUser']=$_SESSION['id_usuario'];

		$this->sql->query($query);
		$this->carga_datos();
		return;
	}

//******************************************************
//
//******************************************************
	function formulario_datos()
	{
//echo $query.'<hr /><pre>';print_r($_SESSION);exit;
?>
<tr>
	<td align="right">Color fondo back-office:</td>
	<td>
  <input type='text' id='colorFondoWeb' name='colorFondoWeb' value='<?=($this->colorFondoWeb!='#'?$this->colorFondoWeb:'')?>' style='width:56px;background-color:<?=$this->colorFondoWeb?>'></td>
	<td align="right"><span id="spnTest">Color fondo botones:</span></td>
	<td><input type='text' id='colorFondoBotones' name='colorFondoBotones' value='<?=($this->colorFondoBotones!='#'?$this->colorFondoBotones:'')?>' style='width:56px;background-color:<?=$this->colorFondoBotones?>'></td>
	<td align="right">Color fondo Hover botones:</td>
	<td><input type='text' id='colorFondoOverBotones' name='colorFondoOverBotones' value='<?=($this->colorFondoOverBotones!='#'?$this->colorFondoOverBotones:'')?>' style='width:56px;background-color:<?=$this->colorFondoOverBotones?>'></td>
	<td align="right">Color texto botones:</td>
	<td><input type='text' id='colorForeBotones' name='colorForeBotones' value='<?=($this->colorForeBotones!='#'?$this->colorForeBotones:'')?>' style='width:56px;background-color:<?=$this->colorForeBotones?>'></td>
	<td align="right">Col. texto Hover bot.:</td>
	<td><input type='text' id='colorForeOverBotones' name='colorForeOverBotones' value='<?=($this->colorForeOverBotones!='#'?$this->colorForeOverBotones:'')?>' style='width:56px;background-color:<?=$this->colorForeOverBotones?>'></td>
</tr>
<tr>
	<td align="right">Color texto standard:</td>
	<td><input type='text' id='colorTextoStd' name='colorTextoStd' value='<?=$this->colorTextoStd?>' style='width:56px;background-color:<?=$this->colorTextoStd?>'></td>
	<td align="right">Color texto titulares:</td>
	<td><input type='text' id='colorForeTitular' name='colorForeTitular' value='<?=$this->colorForeTitular?>' style='width:56px;background-color:<?=$this->colorForeTitular?>'></td>
	<td align="right">Color fondo titulares</td>
	<td><input type='text' id='colorFondoTitular' name='colorFondoTitular' value='<?=$this->colorFondoTitular?>' style='width:56px;background-color:<?=$this->colorFondoTitular?>'></td>
	<td align="right">Color del borde tablas</td>
	<td><input type='text' id='colorTableBordeLight' name='colorTableBordeLight' value='<?=$this->colorTableBordeLight?>' style='width:56px;background-color:<?=$this->colorTableBordeLight?>'></td>
	<td align="right">Textos indicaciones:</td>
	<td><input type='text' id='colorTextWarning' name='colorTextWarning' value='<?=$this->colorTextWarning?>' style='width:56px;background-color:<?=$this->colorTextWarning?>'></td>
</tr>

<tr>
	<td align="right" title="Textos Resaltados positivamente">Textos Resaltados</td>
	<td title="Textos Resaltados positivamente"><input type='text' id='colorTextHighli' name='colorTextHighli' value='<?=$this->colorTextHighli?>' style='width:56px;background-color:<?=$this->colorTextHighli?>'></td>
	<td align="right" title="Textos deshabilitados">Textos deshabilit.</td>
	<td title="Textos deshabilitados"><input type='text' id='colorTextLoose' name='colorTextLoose' value='<?=$this->colorTextLoose?>' style='width:56px;background-color:<?=$this->colorTextLoose?>'></td>

	<td align="right">M&aacute;x n&uacute;m. filas:</td>
	<td><input type='text' id='max_filas_noticias' name='max_filas_noticias' value='<?=$this->max_filas_noticias?>' style='width:40px; text-align:right'></td>
	<td align="right">Fondo Warning:</td>
	<td><input type='text' id='colorFondoWarning' name='colorFondoWarning' value='<?=$this->colorFondoWarning?>' style='width:56px;background-color:<?=$this->colorFondoWarning?>'></td>
	<td align="right">Fondo Campos:</td>
	<td><input type='text' id='colorFondoCampos' name='colorFondoCampos' value='<?=$this->colorFondoCampos?>' style='width:56px;'></td>
</tr>

<?php
		if($_SESSION['usuarioNivel']>14)	// SOLO PARA USUARIOS DEL MAXIMO NIVEL
		{
?>
<tr>
	<td align="right">Tests x newsletter:</td>
	<td colspan='1'>
<?php
			$query='SELECT id, nombre, apellidos, e_mail FROM '.$this->sql->prefixTbl.'usuarios WHERE 1';
			$res=$this->sql->query($query);
			$textoOpcionCero='seleccionar';
			echo '<select id=\'id_usuario_email_test\' name=\'id_usuario_email_test\' style=\'width:100px\'>';
			echo '<option value=\'-1\'>seleccionar</option>';

			while ($arra=$this->sql->fila2($res))
			{
				echo '<option value='.$arra['id'].' ';
				if($this->id_usuario_email_test==$arra['id'])
					echo 'selected=\'selected\'';
				echo '>'.($arra['nombre'].' '.$arra['apellidos']).'</option>';
			}
			echo '</select>';
?>
  </td>
	<td align="right">Debug back-office:</td>
	<td>
<?php
			echo '<select id="debugBackOffice" name="debugBackOffice" style=\'width:60px;\' title="Indica si debe aparecer en portada">';
			echo '<option value=-1 ';
			if($this->debugBackOffice==-1)
				echo 'selected=\'selected\'';
			echo '>?</option>';
			for($n=0;$n<2;$n++)
			{
				echo '<option value='.$n.' ';
				if($this->debugBackOffice==$n && isset($this->debugBackOffice))
					echo 'selected=\'selected\'';
				echo ' >'.$this->arraSiNo[$n].'</option>';
			}
			echo '</select>';
?>
  </td>
	<td align="right">Request:</td>
	<td>
<?php
			echo '<select id="debugRequest" name="debugRequest" style=\'width:60px;\' title="Indica si debe aparecer en portada">';
			echo '<option value=-1 ';
			if($this->debugRequest==-1)
				echo 'selected=\'selected\'';
			echo '>?</option>';
			for($n=0;$n<2;$n++)
			{
				echo '<option value='.$n.' ';
				if($this->debugRequest==$n && isset($this->debugRequest))
					echo 'selected=\'selected\'';
				echo ' >'.$this->arraSiNo[$n].'</option>';
			}
			echo '</select>';
?>
  </td>
	<td align="right">Session:</td>
	<td>
<?php
			echo '<select id="debugSession" name="debugSession" style=\'width:60px;\' title="Indica si debe aparecer en portada">';
			echo '<option value=-1 ';
			if($this->debugSession==-1)
				echo 'selected=\'selected\'';
			echo '>?</option>';
			for($n=0;$n<2;$n++)
			{
				echo '<option value='.$n.' ';
				if($this->debugSession==$n && isset($this->debugSession))
					echo 'selected=\'selected\'';
				echo ' >'.$this->arraSiNo[$n].'</option>';
			}
			echo '</select>';
?>
  </td>
	<td align="right">Objetos:</td>
	<td>
<?php
			echo '<select id="debugObjetos" name="debugObjetos" style=\'width:60px;\' title="Indica si debe aparecer en portada">';
			echo '<option value=-1 ';
			if($this->debugObjetos==-1)
				echo 'selected=\'selected\'';
			echo '>?</option>';
			for($n=0;$n<2;$n++)
			{
				echo '<option value='.$n.' ';
				if($this->debugObjetos==$n && isset($this->debugObjetos))
					echo 'selected=\'selected\'';
				echo ' >'.$this->arraSiNo[$n].'</option>';
			}
			echo '</select>';
?>
  </td>
</tr>
<?php
		}

		if($_REQUEST['accion']==1)	// CUANDO SE SALVA LA CONFIGURACION SE DIBUJA EL BOTON PARA ACTUALIZAR EL ENTERO BACK-OFFICE
		{
?>
<tr>
	<td colspan="12" align="center"><br /><a href="#null" class='btn' onclick="parent.document.location='index.php';" style='padding-right:100px;padding-left:100px;text-align:center'>Click aqu&iacute; si desea actualizar todo el back-office AHORA</a>
  </td>
</tr>
<?php
		}
		else
			echo '<tr><td height="5" colspan="12"></td></tr>';
?>
</table>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo,$id=1)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
$(document).ready(function(){
	$("#idForm").val(<?=(int)$_SESSION['id_usuario']?>);
	$('#colorTextoStd,#colorFondoWeb,#colorForeBotones,#colorFondoBotones,#colorFondoOverBotones,#colorForeOverBotones,#colorForeTitular,#colorFondoTitular,#colorTableBordeLight,#colorTextWarning,#colorTextHighli,#colorTextLoose,#colorFondoWarning,#colorFondoCampos').ColorPicker(
	{
		onSubmit:function(hsb, hex, rgb, el)
		{
			$(el).val(hex);
			$(el).ColorPickerHide();
			$(el).css("background-color","#"+el.value);
		},
		onBeforeShow:function()
		{
			$(this).ColorPickerSetColor(this.value);
		}
	})
});

function revisa()
{
	if(document.f<?=$idunico?>.colorFondoWeb.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.colorFondoWeb);
		alert('Por favor, introduzca un color');
		document.f<?=$idunico?>.colorFondoWeb.focus();
		restauraCampo(document.f<?=$idunico?>.colorFondoWeb);
		return;
	}

	if($("#id_usuario_email_test").id)
		if(document.f<?=$idunico?>.id_usuario_email_test.value<0)
		{
			resaltarCampo(document.f<?=$idunico?>.id_usuario_email_test);
			alert('Por favor, seleccione una dirección de correo para los mails de pruebas');
			document.f<?=$idunico?>.id_usuario_email_test.focus();
			restauraCampo(document.f<?=$idunico?>.id_usuario_email_test);
			return;
		}
//alert("<?=$idunico?>, en pruebas!");return false;
	document.f<?=$idunico?>.submit();
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}
var tmpTxt='<b>zona para mensajes</b> en la <i>barra del t&iacute;tulo</i> de algunos formularios';
setTimeout("$('#mesajes_formulario').html('"+tmpTxt+"');",4000);
-->
</script>
<!-- IMPORTANTE: los estilos aqui abajo han de ser idénticos al los contenidos en 'estilos.php', solamente cambian los identificadores de las variables PHP --->
<style type="text/css">
<!--
a.botones,a.botones:link, a.botones:visited, a.botones:activeMenuHoriz{font-size:21px;font-family:Arial, Helvetica, sans-serif;width:100%;
background-color:<?=$this->colorFondoBotones?>;line-height:13px;color:#40330A;border:1px solid #666;height:15px;text-decoration:none;}
a.botones:hover{padding-left:1px;background-color:<?=$this->colorFondoOverBotones?>;color:<?=$this->colorForeBotones?>;cursor:pointer;text-align:center;}

.mnuizq,mnuizq:visited{text-decoration:none;text-align:left;padding-left:3px;font-size:12px;font-family:Arial, Helvetica, sans-serif;width:100%;background-color:<?=$this->colorFondoBotones?>;
line-height:17px;color:#000;border-bottom:1px solid #777;border-left:2px solid #aaa;border-right:2px solid #888;}

.mnuizq:hover{background-color:<?=$this->colorFondoOverBotones?>;color:#FFFFFF;}

.mnuizqActivo{text-align:left;text-decoration:none;padding-left:6px;border-left:1px solid #888;border-top:1px solid #888;font-size:12px;font-family:Arial, Helvetica, sans-serif;width:100%;line-height:19px;color:#000;}

.arch,.arch:visited,.arch:active{text-decoration:none; text-align:justify; width:100%; padding:2px}
.arch:hover{background-color:<?=$this->foreColAreas[$_SESSION['area']-1]?>}
.mnuSumario,mnuSumario:visited{text-decoration:none;text-decoration:none;text-align:left;padding-left:5px;font-size:12px;font-family:Arial, Helvetica, sans-serif;
width:100%;background-color:<?=$this->debugSession?>;line-height:17px;color:#000;border-bottom:1px solid #777;border-left:2px solid #aaa;border-right:2px solid #888;}
.mnuSumario:hover{
background-color:<?=$this->colorFondoOverBotones?>;color:yellow;
}

-->
</style>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
noListar=1;
-->
</script>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>

<table width="99%" border="0" cellspacing="0" cellpadding="0" align="left">
  <tr>
    <td>
<?php
		include('_inc/form_std_edit.php');
?>
    </td>
  </tr>
</table>
    </td>
  </tr>
  <tr>
    <td>

<table width="99%" cellpadding="4" style="border:<?=($this->id?0:1)?>px solid <?=$_SESSION['colorTableBorde']?>;" border="<?=($_SESSION['debugBackOffice']?1:0)?>" align="center">
<thead>
  <tr>
    <td colspan="2" align="center" class='enc2' height="24">Ejemplo para comprobar las configuraciones</td>
    <td colspan="2" align="right" class='enc2' id='mesajes_formulario' style="padding-right:9px;text-align:right;font-weight:normal;color:<?=$_SESSION['colorTextWarning']?>">listo</td>
  </tr>
</thead>
  <tr>
    <td width="124" align="center" height="20"><i>Elementos</i></td>
    <td width="158" style="color:#000; background-color:<?=$this->colorFondoWeb?>" align="center"><i>ejemplos</i></td>
    <td width="" rowspan="12" style="color:#000; background-color:<?=$this->AreaBackColor[0]?>" valign="top">
		<span style='color:<?=$_SESSION['colorTextWarning']?>'><b>LEER PRIMERO</b>&nbsp;</span><br />
		Este es un texto de ejemplo (<span style="color:<?=$_SESSION['colorTextLoose']?>">este texto est&aacute; atenuado</span>) (<span style="color:<?=$_SESSION['colorTextHighli']?>">este texto est&aacute; <b>resaltado</b> <i>positivamente</i></span>), para visualizar m&aacute;s ver&iacute;dicamente el resultado obtenido con la combinación de los colores definidos.<br />
		<br />
		T&eacute;ngase en cuenta, <u>que TODOS los cambios efectuados</u> en este panel de control <b>afectar&aacute;n </b> la todo el back-office.<br />
		<br />
		Aconsejamos que tomen nota en un papel del valor o valores que hayan de mofificar, para as&iacute; poder volver a configurarlo como estaba originalmente en caso de error o cambio de idea y efect&uacute;en ANTES una copia de seguridad de la base de datos..<br /><br />IMPORTANTE: recuerde que cuando se indique un color, &eacute;ste debe estar precedido siempre por el car&aacute;cter <b>"#"</b>, en caso contrario se pueden ocasionar fallos.
		</td>
  </tr>
  <tr height="26">
    <td align="right">Botones:</td>
    <td style="color:#000; background-color:<?=$this->colorFondoWeb?>"><a href="#null" class='btn' onclick="alert('es solo un ejemplo!')" style=''>Bott&oacute;n <i>TIPO</i> de ejemplo</a></td>
  </tr>
  <tr height="26">
    <td align="right">Bott&oacute;n Reset:</td>
    <td style="color:#000; background-color:<?=$this->colorFondoWeb?>"><a href="#null" class='btn xcancel' onclick="alert('es solo un ejemplo!')" style='width:60px;color:<?=$_SESSION['colorTextWarning']?>;background-color:<?=$_SESSION['colorFondoWarning']?>'><b>BOT&Oacute;N DE RESET</b></a></td>
  </tr>
  <tr height="26">
    <td align="right">Bott&oacute;n (resaltado);</td>
    <td style="color:#000; background-color:<?=$this->colorFondoWeb?>"><a href="#null" class="btn" onclick="alert('es solo un ejemplo!')" style="color:<?=$_SESSION['colorTextHighli']?>;font-weight:bold;" title="Bott&oacute;n resaltado positivamente">Resaltado positivamente</a></td>
  </tr>
  <tr height="26">
    <td align="right">Texto atenuado:</td>
    <td style="color:#000; background-color:<?=$this->colorFondoWeb?>"><span style="color:<?=$_SESSION['colorTextLoose']?>">este texto est&aacute; atenuado</span></td>
  </tr>
  <tr height="26">
    <td align="right">Texto resaltado:</td>
    <td style="color:#000; background-color:<?=$this->colorFondoWeb?>"><span style="color:<?=$_SESSION['colorTextHighli']?>">texto <b>resaltado</b> <i>positivamente</i></span></td>
  </tr>
  <tr height="26">
    <td align="right">Texto avisos:</td>
    <td style="color:#000; background-color:<?=$this->colorFondoWeb?>"><span style="color:<?=$_SESSION['colorTextWarning']?>">texto <b>aviso / alerta</b></span></td>
  </tr>
  <tr height="26">
    <td align="right">Campos formularios:</td>
    <td style="color:#000; background-color:<?=$this->colorFondoWeb?>"><input type="text" id="demoField" name="demoField" value="<?=($_REQUEST['demoField']?$_REQUEST['demoField']:'Texto de ejemplo, 123')?>" /></td>
  </tr>
  <thead>
  <tr height="26">
    <td style="background-color:<?=$this->colorFondoWeb?>;color:<?=$this->colorTextoStd?>" align="right">Titular listados:</td>
    <td class="botones2 ordEnable enc" title="es solo un ejemplo"><span onclick="alert('es solo un ejemplo!')">Campo del formulario</span></td>
  </tr>
	</thead>
<?php
	// !!!! NECESARIO INCLUIR LA GESTIÓN DE PAGINACIÓN PARA OCULTAR EL DIV DE LA BOTONERA DE ACCESOS PROVINIENDO DE OTRAS VENTANAS !!!!!
	$hay=$this->paginacion($num_res, $this->filasporpagina, $offset_u, $destinobusca."?&filtrar=".$_REQUEST['filtrar'].$url."&offset_u=",$this->TotPagMostrar,0);
	if(trim($hay))
		echo '<script>
		paginarBloque=1;
</script>';
	// ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !
?>
			</table>
		</td>
	</tr>
</table>

<?php

	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->quitar_imagen=$datos['quitar_imagen'];
		$this->colorFondoWeb=$this->output_datos($datos['colorFondoWeb']);
		$this->max_filas_noticias=$this->output_datos($datos['max_filas_noticias']);
		$this->colorTextoStd=$this->output_datos($datos['colorTextoStd']);
		$this->colorFondoBotones=$this->output_datos($datos['colorFondoBotones']);
		$this->colorFondoOverBotones=$this->output_datos($datos['colorFondoOverBotones']);
		$this->colorForeBotones=$this->output_datos($datos['colorForeBotones']);
		$this->colorForeOverBotones=$this->output_datos($datos['colorForeOverBotones']);
		$this->colorForeTitular=$this->output_datos($datos['colorForeTitular']);
		$this->colorFondoTitular=$this->output_datos($datos['colorFondoTitular']);
		$this->colorTableBordeLight=$this->output_datos($datos['colorTableBordeLight']);
		$this->colorTextWarning=$this->output_datos($datos['colorTextWarning']);
		$this->colorFondoWarning=$this->output_datos($datos['colorFondoWarning']);
		$this->colorTextHighli=$this->output_datos($datos['colorTextHighli']);
		$this->colorTextLoose=$this->output_datos($datos['colorTextLoose']);
		$this->colorFondoCampos=$this->output_datos($datos['colorFondoCampos']);

		$this->debugRequest=$datos['debugRequest'];
		$this->debugSession=$datos['debugSession'];
		$this->debugObjetos=$datos['debugObjetos'];
		$this->id_usuario_email_test=$datos['id_usuario_email_test'];
		$this->debugBackOffice=$datos['debugBackOffice'];
//		$this->fnd_col_concurso=$this->output_datos($datos['fnd_col_concurso']);
		$this->img_checks_restore=$this->output_datos($datos['img_checks_restore']);
		$this->guarda_datos();
	}
}

?>