<?php
class idiomas extends Base
{
	//Datos propios de la clase
	var $id;
	var $nombre;
	var $nombreTabla;
	var $tamanioMaximoImagenes;
	var $path_imagenes;
	public $path_autores;
	var $posicionSolapa;	// POSICION DE LA VENTANA RESPECTO A LAS DEMAS DE SU MISMA SECCION E IDENTIFICADOR PARA LOS CAMPOS DE LAS BUSQUEDAS

//******************************************************
//	INICIALIZACION DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'idiomas';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset']==1) unset($_SESSION['filtros']);
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->activo=$a['activo'];
		$this->precedencia=$a['precedencia'];
		$this->idioma=$a['idioma'];
		$this->prefijo=$a['prefijo'];
		return;
	}

//******************************************************
//	ELIMINACION DE REGISTROS CON CONTROL DE DATOS RELACIONADOS DE OTRAS TABLAS AL MISMO
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(id) FROM '.$this->sql->db.'.companies WHERE id_pais='.$_REQUEST['id'];
//		$existen_paginas=$this->sql->valor($query);
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$_REQUEST['id'];
//echo $query.'<br /><pre>';print_r($_REQUEST);print_r($this);exit;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//	GUARDAR DATOS DE INSERCIONES Y EDICIONES
//******************************************************
	function guarda_datos()
	{
		$modif_bandera=0;
//echo '<pre>'.$this->path_autores.$this->prefijo;print_r($_FILES['bandera_imagen']['name']);exit;
		if(count($_FILES['bandera_imagen']['name']) && !$this->quitar_archivo)	// SOLO SI ES UNA IMAGEN
		{
			if(!is_dir($this->path_imagenes))
			{
				if(!@mkdir($this->path_imagenes))	// SI SE PRODUCIRSE UN ERROR DE CREACION DE CARPETA, SE VISUALIZA UN MENSAJE Y SE ABORTA LA OPERACION
				{
					$this->mensajeSalida='<span class=enc2 style=background-color:red;color:#fff>&nbsp;ERROR GRAVE&nbsp;</span><br />NO EXISTE O NO SE PUEDE ESCRIBIR EN LA CARPETA DE <br />'.$this->path_imagenes;
					echo $this->mensajeSalida;
					exit;
				}
			}

			// SOLO SOLO SE PUEDEN SUBIR AL SERVIDOR IMAGENES VALIDAS
			if($_FILES['bandera_imagen']['name'])
				if($_FILES['bandera_imagen']['type']!='image/x-png' && $_FILES['bandera_imagen']['type']!='image/pjpeg' && $_FILES['bandera_imagen']['type']!='image/gif' && $_FILES['bandera_imagen']['type']!='image/png' && $_FILES['bandera_imagen']['type']!='image/x-icon' && $_FILES['bandera_imagen']['type']!='image/jpeg')
				{
					$this->mensajeSalida='<b>ERROR:</b> ARCHIVO NO SOPORTADO (<i>'.$_FILES['bandera_imagen']['name'].'</i>), SOLO IMAGENES';
					$_REQUEST['errorControl']=1;
					return;
				}

			$tamanio=$_FILES['bandera_imagen']['size'];

			if($tamanio > $this->tamanioMaximoImagenes)	// SE HA SUPERADO EN BYTES EL TAMANIO MAXIMO PERMITIDO PARA EL OBJETO DE FLASH
			{
				$tamanio=number_format(($tamanio/1000),2,',','.').'Kb';
				$this->mensajeSalida='ERROR: es demasiado grande: '.$tamanio.' (MAX: '.($this->tamanioMaximoImagenes/1000).'Kb)';

				if(!$_REQUEST['id'])	// ES UN RECORD NUEVO
				{
					echo '<br /><br /><center><p ><h4 style="color:red">'.$this->mensajeSalida.'</h4></p><br /><br /><br /><a href="#null" style="width:100px" class="btn" onclick="history.go(-1)">volver</a></center>';
					exit;;
				}
				return;
			}

			if(is_uploaded_file($_FILES['bandera_imagen']['tmp_name']))
			{
				$extension='.jpg';

				if($_FILES['bandera_imagen']['type']=='image/x-png') $extension='.png';
				if($_FILES['bandera_imagen']['type']=='image/gif') $extension='.png';

				$tmp_file='flag_'.strtolower($this->prefijo).$extension;
				copy($_FILES['bandera_imagen']['tmp_name'], $this->path_imagenes.$tmp_file);
			}
		}

		if($this->quitar_archivo && $this->prefijo)	// SI SE HA SELECCIONADO QUITAR EL ARCHIVO, SE PROCEDE A BORRARLO DE LA TABLA
		{
			$arrayExtensiones= array('.jpg','.gif','.	png');

			foreach($arrayExtensiones as $key)
			{
				if(file_exists($this->path_imagenes.'flag_'.$this->prefijo.$key))
					@unlink($this->path_imagenes.'flag_'.$this->prefijo.$key);
			}
		}

		if(!$this->id) $this->id=0; // NECESARIO PARA ANIADIR NUEVOS REGISTROS
		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla." (id, prefijo, activo, precedencia, idioma) VALUES ('".$this->id."', '".$this->prefijo."', '".$this->activo."','".$this->precedencia."', '".$this->idioma."')";
//echo $query.'<br /><pre>';print_r($_REQUEST);print_r($this);exit;
		$this->sql->query($query);
		return;
	}

//******************************************************
//	FORMULARIO DE EDICION /INSERCION DE DATOS
//******************************************************
	function formulario_datos($nombreForm=0)
	{
?>
<tr valign="top">
	<td valign="top">Idioma:</td>
	<td valign="top"><input type='text' id='idioma' name='idioma' value='<?=$this->idioma?>' style='width:122px'></td>
	<td title='Indica si el idioma est&aacute; activo' align="right" width="50">Activo:</td>
	<td title='Indica si el idioma est&aacute; activo'>
<?php
		$arraEsActivo=array(0=>'No',1=>'S&iacute;');
		echo '<select id="activo" name="activo" style=\'width:60px;\' title="Indica si el idioma est&aacute; activo">';
		echo '<option value=-1 ';
		if($this->activo==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->activo==$n && isset($this->activo))
				echo 'selected=\'selected\'';
			echo ' >'.$arraEsActivo[$n].'</option>';
		}
		echo '</select>';
?>
	</td>
	<td title='Orden de aparici&oacute;n en caso de &uacute;nico idioma disponible' align="right" width="50">Precedencia:</td>
	<td title='Orden de aparici&oacute;n en caso de &uacute;nico idioma disponible'>
<?php
		$query='SELECT COUNT(id) FROM '.$this->sql->db.'.'.$this->nombreTabla;
		$numIdiomas=$this->sql->valor($query)+1;

		echo '<select id="precedencia" name="precedencia" style=\'width:60px;\' title="Orden de aparici&oacute;n en caso de &uacute;nico idioma disponible">';
		echo '<option value=-1 ';
		if($this->precedencia==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=1;$n<$numIdiomas+1;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->precedencia==$n && isset($this->precedencia))
				echo 'selected=\'selected\'';
			echo ' >'.$n.'</option>';
		}
		echo '</select>';
?>
	</td>
  </tr>
  <tr>
	<td valign="top" title="Prefijo internacional en min&uacute;sculas (solo caracteres ingleses)">Prefijo:</td>
	<td valign="top" title="Prefijo internacional en min&uacute;sculas (solo caracteres ingleses)"><input type='text' id='prefijo' name='prefijo' value='<?=$this->prefijo?>' maxlength="3" style='width:24px' title="Prefijo internacional en min&uacute;sculas (solo caracteres ingleses)" /></td>

	<td align='right' valign="top" width="50">
<?php
		if($this->id && $this->bandera_imagen)	// SE DUBUJA EL BOTON PARA ABRIR LA IMAGEN A TANANIO REAL EN UNA VENTANA POP
		{
			$dat=@getimagesize($this->path_imagenes.$this->bandera_imagen);
			if($dat)
				echo '<a href="#null" class="btn" style="border:2px solid green" onclick="im(\''.$this->path_imagenes.$this->bandera_imagen.'\','.$dat[0].','.$dat[1].')" title="click para ver la imagen">&nbsp;Imagen </a>';
		}
		else
			echo 'bandera:';
?>
  </td>
	<td valign="top" colspan="3"><input type="file" id="bandera_imagen" name="bandera_imagen" style="width:134px" />&nbsp;&nbsp;
<?php
		if($this->id)	// SE VISYALIZA LA CASILLA PARA BORRAR LA IMAGEN SOLO EN EDICION DE REGISTRO
		{
			$archivoLaBanderita=$this->path_imagenes.'flag_'.$this->prefijo.'.jpg';

			if(file_exists($archivoLaBanderita))	// SE VISYALIZA LA CASILLA PARA BORRAR LA IMAGEN SOLO EN EDICION DE REGISTRO
			echo '<br /><span title="Marcar para eliminar el bandera">Eliminar imagen: <input type="checkbox" name="quitar_archivo" value="1" style="border:0px;height:14px;width:14px"></span>';
		}
?>
	</td>
</tr>
<?php
		if($this->id) echo '<tr><td height="25"></td></tr>';
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
	if(document.f<?=$idunico?>.idioma.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.idioma);
		document.f<?=$idunico?>.idioma.focus();
		alert('Por favor, escriba el nombre del idioma');
		restauraCampo(document.f<?=$idunico?>.idioma);
		return;
	}
	if(document.f<?=$idunico?>.activo.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.activo);
		document.f<?=$idunico?>.activo.focus();
		alert('Por favor, indique si es activo');
		restauraCampo(document.f<?=$idunico?>.activo);
		return;
	}
	if(document.f<?=$idunico?>.precedencia.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.precedencia);
		document.f<?=$idunico?>.precedencia.focus();
		alert('Por favor, establezca la precedencia del idioma');
		restauraCampo(document.f<?=$idunico?>.precedencia);
		return;
	}
	if(document.f<?=$idunico?>.prefijo.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.prefijo);
		alert('Por favor, escriba el prefijo del idioma');
		document.f<?=$idunico?>.prefijo.focus();
		restauraCampo(document.f<?=$idunico?>.prefijo);
		return;
	}
	document.f<?=$idunico?>.submit();
}
-->
</script>
<?php
		include('_inc/form_std_edit.php');
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->quitar_archivo=$datos['quitar_archivo'];
		$this->idioma=$datos['idioma'];
		$this->activo=$datos['activo'];
		$this->precedencia=$datos['precedencia'];
		$this->bandera_imagen=$datos['bandera_imagen'];
		$this->prefijo=$datos['prefijo'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
//		$mens.='</b>';
		if(count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b&uacute;squeda:</span> ";

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='idioma'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' DESC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';

//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>
<form action='<?=$destinobusca?>.php' method='post' name='formListados'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->

<table align='center' cellpadding='3' cellspacing='0' class='borde' width='500' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td width='30'>idioma:</td>
	<td title='Idiomas disponibles'>
<?php
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' ORDER BY idioma';
		$res2=$this->sql->query($query);

		echo '<select name="'.$this->posicionSolapa.'id_buscN" style=\'width:110px;\' title="Idiomas disponibles">';
		echo '<option value=-1 ';
		if($this->precedencia==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		while($arra2=$this->sql->fila2($res2))
		{
			echo '<option value='.$arra2['id'].' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_buscN']==$arra2['id'])
				echo 'selected=\'selected\'';
			echo ' >'.utf8_encode($arra2['idioma']).'</option>';
		}
		echo '</select>';
?>
	</td>
	<td width="30">Activo:</td>
	<td>
<?php
		$arraEsActivoBusc=array(0=>'No',1=>'S&iacute;');
		echo '<select name="'.$this->posicionSolapa.'activo_buscN" style=\'width:50px;\' title="Est&aacute; activo?">';
		echo '<option value="-1" >?</option>';
		foreach($arraEsActivoBusc as $key=>$value)
		{
			echo '<option value='.$key.' ';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'activo_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'activo_buscN']==$key)
				echo 'selected=\'selected\'';
			echo ' >'.utf8_encode($value).'</option>';
		}
		echo '</select>';
?>
  </td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'45%','','50%','25%');

		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('Idioma','Activo','Precedencia','Bandera');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id','idioma','activo','precedencia','prefijo');
		$xc=0; $arraLongitudes=array($xc++=>40,$xc++=>60,$xc++=>30,$xc++=>100); // no se incluye el valor para el ID
		$xc=0; $arraLongitudesTitulares=array($xc++=>25,$xc++=>150,$xc++=>60,$xc++=>70,$xc++=>100);
		$xc=0; $arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'txt',$xc++=>'num',$xc++=>'img');
		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$xc=0;
			$esActivo=($a['activo']==1)?'<span style=color:'.$_SESSION['colorTextHighli'].'>SI</span>':'<span style=color:'.$_SESSION['colorTextLoose'].'><b>NO</b></style>';
			$archivoLaBanderita=$this->path_imagenes.'flag_'.$a['prefijo'].'.jpg';
			$mostrarBanderita=file_exists($archivoLaBanderita)?$archivoLaBanderita:$this->path_imagenes.'flag_no_bandera.jpg';

			$arraDatos[]=array($xc++=>$a['id'],$xc++=>utf8_encode($a['idioma']),$xc++=>$esActivo,$xc++=>$a['precedencia'],$xc++=>$mostrarBanderita);
		}

		$idRefPops=0; $txtRefEliminar=1; $ocultarEliminar=0;
		$posicFondo=0;	// NUMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo,$ocultarEliminar);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>