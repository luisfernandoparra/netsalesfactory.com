<?php
class logsWeb extends Base
{
	//Datos propios de la clase
	var $id;
	var $accion;
	var $nombreTabla;
	var $posicionSolapa;	// POSICI�N DE LA VENTANA RESPECTO A LAS DEM�S DE SU MISMA SECCI�N E IDENTIFICADOR PARA LOS CAMPOS DE LAS B�SQUEDAS

//******************************************************
//	INICIALIZACI�N DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'log_web_actions'.date('Y');
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las b�squedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->accion=$a['accion'];
		$this->fecha=$a['fecha'];
		$this->session_id=$a['session_id'];
		return;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->accion=$datos['accion'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens)
	{
		$mens.='</b>'; $tmpCheckIp=null;
		$sqlFilterExceptionCount='*';
		$sqlFilterExceptionList='';
//echo '<pre>';print_r($_REQUEST);
//echo (isset($_REQUEST[$this->posicionSolapa.'ip_single_busca']) && $_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca']);
//echo '</pre>';
		if($_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca'] == 'on')
//		if(isset($_REQUEST[$this->posicionSolapa.'ip_single_busca']) && $_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca'])
		{
			$tmpCheckIp=$_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca'];
			unset($_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca']);
			$sqlFilterExceptionCount=' DISTINCT(ip_usuario)';
				$sqlFilterExceptionList=' GROUP BY (ip_usuario)';
		}

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		$querytotal='SELECT COUNT('.$sqlFilterExceptionCount.') FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='fecha'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.$sqlFilterExceptionList.'
			ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;

		if($_SESSION['filtros']['visitantes_distintos_busca'])
		{
			$res2=$this->sql->query($querytotal);
			$num_res=mysql_num_rows($res2);
		}
		else
			$num_res=$this->sql->valor($querytotal);
//echo $query;die();
		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';

		if($tmpCheckIp)
			$_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca']=$tmpCheckIp;
//echo $query;
//******************************************************
//Formulario para efectuar b�squedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];

// START NUMERO VISITANTES DISTINTOS **********
	$query='SELECT COUNT(id) AS cuenta FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql.' GROUP BY `ip_usuario` ';
	$res3=$this->sql->query($query);
	$numVisitantesDistintos=0;

	while($a=$this->sql->fila2($res3))
		$numVisitantesDistintos++;

	$txtVisit=$numVisitantesDistintos==1?'visitante &uacute;nicamnete':'visitantes distintos';
	$losVisitantes='&nbsp;&nbsp;(<b>'.number_format($numVisitantesDistintos,0,',','.').'</b> '.$txtVisit.')';
// END NUMERO VISITANTES DISTINTOS **********
?>
<form action='<?=$destinobusca?>.php' method='post' name='formListados'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='1030' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='7'><?=$textoTitularBuscador?><span id="spnVisitas" style="color:#999"><?=$losVisitantes?></span></td>
	<td class='enc2' colspan='8' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td align="right" title="IP únicas">Single IP:</td>
	<td align="left"><input name="<?=$this->posicionSolapa?>ip_single_busca" id="<?=$this->posicionSolapa?>ip_single_busca" type="checkbox" <?=$_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca'] ? 'checked="checked"' : ''?> /></td>
	<td align="right">Ip usuario:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>ip_usuario_busca' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'ip_usuario_busca']?>'  style='width:65px' /></td>
	<td align="right">Sesión:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>session_id_busca' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'session_id_busca']?>'  style='width:65px' /></td>
	<td class='tx11' style='' title='Filtro por fechas (en notaci&oacute;n americana)'>
	Fecha:&nbsp;
<input type='text' id="<?=$this->posicionSolapa?>fecha_busca" name="<?=$this->posicionSolapa?>fecha_busca" value='<?=$_SESSION['filtros'][$this->posicionSolapa.'fecha_busca']?>' class="tcal" style='width:84px' />
	</td>
	<td width='30'>Acci&oacute;n:</td>
	<td>
		<input type='text' name='<?=$this->posicionSolapa?>accion_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'accion_busca_u']?>' style='width:90px'>
	</td>
	<td align="right">Referer / Errores:</td>
	<td>
		<input type='text' name='<?=$this->posicionSolapa?>http_referer_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'http_referer_busca_u']?>' style='width:90px'>
  </td>
	<td align="right">Promo:</td>
	<td>
<?php
		$query='SELECT id FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_site_config';
		$res2=$this->sql->query($query);

		$output='<select id=\''.$this->posicionSolapa.'id_crea_buscN\' name=\''.$this->posicionSolapa.'id_crea_buscN\' style=\'width:54px;text-align:right;\' ><option value=\'-1\'>?</option>';
		while ($arra=$this->sql->fila2($res2))
		{
			$output.='<option value=\''.$arra['id'].'\' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_crea_buscN']==$arra['id'])
				$output.='selected=\'selected\'';
			$output.='>'.$arra['id'].'</option>';
		}
		$output.='</select>';
		echo $output;
?>
		<!--<input type='text' name='<?=$this->posicionSolapa?>id_crea_buscN' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'id_crea_buscN']?>' style='width:20px'>-->
  </td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>

<script language="JavaScript" type="text/javascript">
<!--
function modifica(){return;}
-->
</script>
<!-- *******************  fin formulario b�squedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'75%','','80%','10%',$this->editarFila);
/*
		$arraTitulares=array('Sesion','IP','Fecha','Hora','Acci&oacute;n','Promo','Formulario');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id','session_id','ip_usuario','fecha','','accion','id_crea','script');
		$xc=0; $arraLongitudes=array($xc++=>30,$xc++=>30,$xc++=>25,$xc++=>25,$xc++=>80,$xc++=>30,$xc++=>30); // no se incluye el valor para el ID
		$xc=0; $arraLongitudesTitulares=array($xc++=>45,$xc++=>130,$xc++=>70,$xc++=>56,$xc++=>50,$xc++=>240,$xc++=>40,$xc++=>180);
		$xc=0; $arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'num',$xc++=>'fec',$xc++=>'num',$xc++=>'txt',$xc++=>'txt',$xc++=>'txt');
		$this->verIdRegistros=1;
		$verEmergentesaccionsFragmentados=1;
		$decimales=0;
*/
		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		if(!$_SESSION['forcedLangSite']) $arraTitulares[]='Idioma';
		$arraTitulares[]='Sesion';
		$arraTitulares[]='IP';
		$arraTitulares[]='Fecha';
		$arraTitulares[]='Hora';
		$arraTitulares[]='Acciones';
		$arraTitulares[]='HTTP Referer / Errores';
		$arraTitulares[]='Promo';
		$arraTitulares[]='Formulario';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		if(!$_SESSION['forcedLangSite']) $arraCamposOrdenar[]='id_idioma';
		$arraCamposOrdenar[]='session_id';
		$arraCamposOrdenar[]='ip_usuario';
		$arraCamposOrdenar[]='fecha';
		$arraCamposOrdenar[]='';
		$arraCamposOrdenar[]='accion';
		$arraCamposOrdenar[]='http_referer';
		$arraCamposOrdenar[]='id_crea';
		$arraCamposOrdenar[]='script';

		 // no se incluye el valor para el ID
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=50;
		$arraLongitudes[]=30;
		$arraLongitudes[]=25;
		$arraLongitudes[]=25;
		$arraLongitudes[]=80;
		$arraLongitudes[]=50;
		$arraLongitudes[]=44;
		$arraLongitudes[]=30;
		$arraLongitudes[]=30;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=45;	// id
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=90;
		$arraLongitudesTitulares[]=120;
		$arraLongitudesTitulares[]=75;
		$arraLongitudesTitulares[]=56;
		$arraLongitudesTitulares[]=50;
		$arraLongitudesTitulares[]=245;
		$arraLongitudesTitulares[]=220;
		$arraLongitudesTitulares[]=36;
		$arraLongitudesTitulares[]=160;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='fec';
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='txt';

		$this->verIdRegistros=0;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;
		$tmpArr=array();
	
		while($a=$this->sql->fila2($res))
		{
			$xc=0;
			$laAccion=$a['accion'];
			if(!$laAccion) $laAccion='<span style=color:#999>consultar</span>';
			$ora=date('H:i:s',strtotime($a['fecha']));

		  $tmpArr[0]=$a['id'];
		  array_push($tmpArr,$a['session_id']);
		  array_push($tmpArr,$a['ip_usuario']);
		  array_push($tmpArr,$a['fecha']);
		  array_push($tmpArr,$ora);
		  array_push($tmpArr,$laAccion);
		  array_push($tmpArr,$a['http_referer']);
		  array_push($tmpArr,$a['id_crea']);
		  array_push($tmpArr,$a['script']);
		  $arraDatos[]=$tmpArr;
		  unset($tmpArr);
		}

		$idRefPops=0; $txtRefEliminar=2; $ocultarEliminar=1;
		$posicFondo=0;	// N�MERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo,$ocultarEliminar);
		unset($laAccion);
		/*******************  FIN DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>