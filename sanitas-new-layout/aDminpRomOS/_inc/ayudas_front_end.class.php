<?php
class ayudasFrontEnd extends Base
{
	//Datos propios de la clase
	var $id;
	var $id_area;
	var $nombreTabla;
	var $posicionSolapa;	// POSICI�N DE LA VENTANA RESPECTO A LAS DEM�S DE SU MISMA SECCI�N E IDENTIFICADOR PARA LOS CAMPOS DE LAS B�SQUEDAS

//******************************************************
//	INICIALIZACI�N DE LA CLASE
//******************************************************
	function __construct($sql)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'fvr_ayudas_web';
		$this->crearVariablesBusquedas($this->posicionSolapa);	// se crean las variables para las b�squedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset']==1) unset($_SESSION['filtros']);
	}

//******************************************************
//	CARAGAR UN DETERMINADO REGISTRO ($id)
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->id_pagina=$a['id_pagina'];
		$this->id_idioma=$a['id_idioma'];
		$this->texto=$a['texto'];
		return;
	}

//******************************************************
//	ELIMINACI�N DE REGISTROS CON CONTROL DE DATOS RELACIONADOS DE OTRAS TABLAS AL MISMO
//******************************************************
	function eliminar_registro()
	{
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		$this->verificarDuplicados($this->id_pagina, $this->nombreTabla, 'id_pagina', 'id_pagina',1);	// VERIFICACI�N DE DATO DUPLICADO

		if(!$this->id) $this->id=0; // NECESARIO PARA A�ADIR NUEVOS REGISTROS

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla." (id,id_pagina,id_idioma,texto) VALUES ('".$this->id."','".$this->id_pagina."','".$this->id_idioma."','".str_replace("\r\n",'',$this->texto)."')";
//echo $query;exit;
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos($nombreForm)
	{
		$this->PhpEditarHTML($this->nombreTabla,$nombreForm,$this->id);
?>
<input type='hidden' id='id_idioma' name='id_idioma' value='1'>

<tr valign="top">
	<td>De la solapa:</td>
	<td>
<?php
		$query2='SELECT fsm.id_pagina, fic.texto_mostrar FROM '.$this->sql->db.'.fvr_solapas_menu AS fsm INNER JOIN '.$this->sql->db.'.fvr_idiomas_common AS fic ON (fic.etiqueta=fsm.label && fic.id_idioma=fic.id_idioma) GROUP BY fsm.id_pagina ORDER BY fsm.id_pagina';

		$res2=$this->sql->query($query2);

		$salida='<select id="id_pagina" name="id_pagina" style=\'width:180px;\' title="Nivel de acceso del usuario al Back-office">';
		$salida.='<option value=-1 ';
		if($this->id_pagina==-1)
			$salida.='selected=\'selected\'';
		$salida.='>seleccionar</option>';
		while($arra2=$this->sql->fila2($res2))
		{
			$salida.='<option value='.$arra2['id_pagina'].' ';
			if($this->id_pagina==$arra2['id_pagina'])
				$salida.='selected=\'selected\'';
			$salida.=' >'.$arra2['texto_mostrar'].'</option>';
		}
		$salida.='</select>';
		echo $salida;
?>
  </td>
	<td align="right">Texto:<br /><a id="btnEditHtml" href="#null" onclick="editarHTML('texto')" class="btn" style="width:100%" title="Editar con estilos HTML">html</a></td>
	<td><textarea id='texto' name='texto' style='width:500px' rows="<?=($this->id)?'18':'4'?>"><?=str_replace("\r\n",'',$this->texto)?></textarea></td>
</tr>
<?php
		if($this->id) echo '<tr><td height="25"></td></tr>';
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
	if (document.f<?=$idunico?>.id_pagina.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.id_pagina);
		alert('Por favor, seleccione la p�gina que debe abrir la ayuda');
		document.f<?=$idunico?>.id_pagina.focus();
		restauraCampo(document.f<?=$idunico?>.id_pagina);
		return;
	}
	document.f<?=$idunico?>.submit();
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}
-->
</script>

<?php
	include('_inc/form_std_edit.php');
	return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->id_pagina=$datos['id_pagina'];
		$this->id_idioma=$datos['id_idioma'];
		$this->texto=$datos['texto'];
		$this->guarda_datos();
		return;
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca,$destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$idunico=rand(1,10000).time();
		if (count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b&uacute;squeda:</span> ";

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

//		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='id_pagina'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';

//******************************************************
//Formulario para efectuar b�squedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->

<table align='center' cellpadding='3' cellspacing='0' class='borde' width='780'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
</tr>
</thead>
<tr>
	<td width="64">En la solapa:</td>
	<td>
<?php
		$query2='SELECT fsm.id_pagina, fic.texto_mostrar FROM '.$this->sql->db.'.fvr_solapas_menu AS fsm INNER JOIN '.$this->sql->db.'.fvr_idiomas_common AS fic ON (fic.etiqueta=fsm.label && fic.id_idioma=fic.id_idioma) GROUP BY fsm.id_pagina ORDER BY fsm.id_pagina';

		$res2=$this->sql->query($query2);

		$salida='<select id="'.$this->posicionSolapa.'id_pagina_buscN" name="'.$this->posicionSolapa.'id_pagina_buscN" style=\'width:180px;\' title="Nivel de acceso del usuario al Back-office">';
		$salida.='<option value=-1 ';
		if($_SESSION['filtros'][$this->posicionSolapa.'id_pagina_buscN']==-1)
			$salida.='selected=\'selected\'';
		$salida.='>seleccionar</option>';
		while($arra2=$this->sql->fila2($res2))
		{
			$salida.='<option value='.$arra2['id_pagina'].' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_pagina_buscN']==$arra2['id_pagina'])
				$salida.='selected=\'selected\'';
			$salida.=' >'.$arra2['texto_mostrar'].'</option>';
		}
		$salida.='</select>';
		echo $salida;
?>
  </td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- *******************  fin formulario b�squedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'75%','','80%','10%');

		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('Nombre solapa','texto');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id','id_pagina','texto');
		$xc=0; $arraLongitudes=array($xc++=>30,$xc++=>120); // no se incluye el valor para el ID
		$xc=0; $arraLongitudesTitulares=array($xc++=>30,$xc++=>220,$xc++=>454);
		$xc=0; $arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'txt');
		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$xc=0;
			$query2='SELECT fic.texto_mostrar FROM '.$this->sql->db.'.fvr_solapas_menu AS fsm INNER JOIN '.$this->sql->db.'.fvr_idiomas_common AS fic ON (fic.etiqueta=fsm.label && fic.id_idioma=1) WHERE fsm.id_pagina='.$a['id_pagina'].'';
			$laSolapa=$this->sql->valor($query2);
			$laSolapa=$laSolapa?$laSolapa:'???';

			$contenido=str_replace('"','`',nl2br(strip_tags($a['texto'])));
			$arraDatos[]=array($xc++=>$a['id'],$xc++=>$laSolapa,$xc++=>$contenido);
		}

		$idRefPops=0;
		$txtRefEliminar=1;
		$posicFondo=0;	// N�MERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  FIN DEL LISTADO  ********************/

		if($num_res>mysql_num_rows($res) && !$this->contFilas)
		{
			foreach($_SESSION['filtros'] as $key=>$value)	// SE RECARGA LA PAGINA SI NO HAY FILAS EN PANTALLA Y HAY RESULTADOS EN LA BUSQUEDA
			{
				$pos=strpos($key,'_busca');
				if ($pos>0)
					$url.='&'.$key.'='.$value;
			}
//			$_SESSION['filtros']['offset_u']=0;
			echo "<script language='JavaScript' type='text/JavaScript'>
			document.location='".$destinobusca.".php?$url';
			</script>";
		}
?>
</table>
<table align='center' cellpadding='2'>
<tr><td style='line-height:22px' align='center'><span id='spn_infoPage'></span>
<?php
	$url='';
	foreach($_SESSION['filtros'] as $key=>$value)
	{
		$pos=strpos($key,'_busca');
		if ($pos>0)
			$url.='&'.$key.'='.$value;
	}
	if(isset($_REQUEST['offset_u']))
		$offset_u = $_REQUEST['offset_u'];
	else
		$offset_u = 0;

// ********************************************************************************************************
// nota importante: EL �LTIMO PAR�METRO ES LA REFERENCIA DEL IFRAME RESPECTO A LOS DEM�S DE LA P�GINA PADRE, es B�SICO
// ********************************************************************************************************
	$hay=$this->paginacion($num_res, $this->filasporpagina, $offset_u, $destinobusca.".php?&filtrar=".$_REQUEST['filtrar'].$url.'&sentidoOrdenacion='.$_REQUEST['sentidoOrdenacion'].'&ordenarPor='.$_REQUEST['ordenarPor']."&offset_u=",$this->TotPagMostrar,$this->posicionSolapa);
// ********************************************************************************************************
	if(trim($hay))
		echo '<script>
$("#paginarTop").html(\''.$hay.'\');
$("#paginarTop").css("display","block");
</script>';
echo $hay;
?>
<script>
var arraTxtDescripcion=new Array();
var sombraL; var sombraT;

function mostrarPagina(direccion,posicionSolapa)	// se recarga la p�gina desde los links de la barra inferior de paginaci�n (2011)
{
	$("#mesajes_formulario").html("<i>accediendo...</i>");
	document.location=direccion+"&posicionSolapa="+posicionSolapa;
	return;
}

</script>
</td></tr></table>
</form>
<?php
	}

//******************************************************
//	DEVUELVE EL TEXTO DE LA AYUDA SOLICITADA
//******************************************************
	function opbtenerTextoAyuda($datos)
	{
		$pg=strpos($_REQUEST['nombre_pagina'],'?');
		$pagina=substr($_REQUEST['nombre_pagina'],0,$pg);

		$titulo=str_replace('_',' ',$_REQUEST['titulo']);
		$query='SELECT texto FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id_pagina="'.$pagina.'"';
		$contenido=$this->sql->valor($query);

		if(!$contenido) $contenido='<br /><br /><br /><center><span style="color:#000;background-color:orange">&nbsp;&nbsp;ESTA P�GINA NO TIENE NINGUNA AYUDA DEFINIDA&nbsp;&nbsp;</span><br /><br />Si necesita que esta ficha disponga de alguna explicaci�n o ayuda,<br />por favor, p�ngase en contacto con el administrador de la Web.</center>';

		$contenido=nl2br($contenido);?>
<table height="90%" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr height="25">
		<td align="center">P�gina de ayuda para la ficha <b>"<?=$titulo?>"</b><hr style="color:#aaa; height:1px" /></td>
	</tr>
	<tr>
		<td valign="top"><?=$contenido?></td>
	</tr>
	<tr>
		<td align="center" height="35"><a href="#null" onclick="window.close()" class="btn" style="width:100px;border:1px solid #000" title="Click para cerrar">Cerrar</a><br /><br /></td>
	</tr>
</table>
<?php
	}

}
?>