﻿<?php
class backup extends Base
{
	//Datos propios de la clase
	var $nombreTabla;
	var $tamanioMaximoImagenes;
	var $path_imagenes;
	var $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
	}

//******************************************************
//
//******************************************************
  function tep_not_null($value)
	{
    if (is_array($value))
		{
      if (sizeof($value) > 0)
        return true;
			else
        return false;
    }
		else
		{
      if((is_string($value) || is_int($value)) && ($value != '') && ($value != 'NULL') && (strlen(trim($value)) > 0))
        return true;
			else
        return false;
    }
  }

//******************************************************
//	EJECUTAR EL BACKUP DE LA BBDD
//******************************************************
	function ejecutarBackUp()
	{
		$tablesBBDD=array_values($_REQUEST['tablaBackUp']);
		set_time_limit(0);	// SE REDIMENSIONA EL TIEMPO LÍMITE PARA ESTA FUNCIÓN, PARA EVITAR ERRORES EN CASO DE BBDD MUY GRANDES
		$this->nuevaBBDD=1;
		$prefijoNombreTabla='db_';

		if(count($_REQUEST['tablaBackUp']) == 1)
		{
			$prefijoNombreTabla=str_replace('_',' ',$tablesBBDD[0]);
			$prefijoNombreTabla=ucwords($prefijoNombreTabla);
			$prefijoNombreTabla=str_replace(' ','',$prefijoNombreTabla);
			$prefijoNombreTabla=$prefijoNombreTabla.'_DB_';
		}
		
		$this->backup_file=$prefijoNombreTabla.strtolower($_REQUEST['nombre_bbdd']).'-'.date('Y.m.d-Hi').'.txt';

		$fp=@fopen($this->path_archivos.$this->backup_file,'w');

		if(empty($fp))	// NO EXISTE O NO SE ACCEDE A LA CARPETA PARA EL BACK-UP
		{
			$this->mensajeSalida='<br /><br /><br /><center><span class=enc2 style=display:block;width:300px;padding:20px;background-color:red;color:#fff>&nbsp;--->&nbsp;ERROR AL EJECUTAR LA TAREA&nbsp;<---&nbsp;</span><br /><br />NO SE PUEDE ESCRIBIR EN LA CARPETA DE <br /><br /><h3>'.(substr($this->path_archivos,strpos($this->path_archivos,'/')+1,strrpos($this->path_archivos,'/')-3)).'</h3><br /><br /><b>(no existe o no tiene los permisos adecuados para crear el archivo "<i>'.$this->backup_file.'</i>")</b>.<br /><br />Para poder efectuar el back-up deseado, primero ha de solucionar este problema.<script>$("#sndMensajeFinalizando").slideUp(500);</script></center>';
			echo $this->mensajeSalida;
			$_REQUEST['accion']=-1;
			$this->sql->nuevaBBDD=0;
			unset($_SESSION['nuevaBBDD']);
			return;
		}

//echo $this->sql->prefixTbl.' ]<br /><pre>';print_r($tablesBBDD);print_r($_REQUEST['tablaBackUp']);die();
		$schema='# '.$_SESSION['nombre_site']."\n" .
'#' . "\n" .
'# Database Backup para: '.$_REQUEST['nombre_bbdd']."\n" .
'# Usuario de esta copia: '.$_SESSION['NOMBRE_ADMIN']."\n" .
'#' . "\n".
'#' . "\n".
'# Fecha de ejecucion: '.date('D-m-y H:i')."\n\n";
		fputs($fp,$schema);

		foreach($tablesBBDD as $key=>$table)
		{
			$schema= 'drop table if exists `'.$table.'`;'."\n" .
			'create table `'.$table.'` ('."\n";

			$table_list = array();
			$query='SHOW FIELDS FROM `'.$table.'`';
			$res2=$this->sql->query($query);

			while($fields=$this->sql->fila2($res2))
			{
				$table_list[]=$fields['Field'];
				$schema.='  '.$fields['Field'].' '.$fields['Type'];

				$brakets="'";if($fields['Default']=='CURRENT_TIMESTAMP') $brakets='';	// <====== RATTOPPO!!!!

				if(strlen($fields['Default']) > 0) $schema.=' default '.$brakets.$fields['Default'].$brakets.'';
				if($fields['Null'] != 'YES') $schema.=' not null';
				if(isset($fields['Extra'])) $schema.=' '.$fields['Extra'];
				$schema.=','."\n";
			}

			$schema=@ereg_replace(",\n$",'',$schema);
			$index=@array();
			$query='show keys from `'.$table.'`';
			$res3=$this->sql->query($query);

			while($keys=$this->sql->fila2($res3))
			{
				$kname=$keys['Key_name'];
				
				if(!isset($index[$kname]))
					$index[$kname]=@array('unique' => !$keys['Non_unique'], 'fulltext' => ($keys['Index_type'] == 'FULLTEXT' ? '1' : '0'), 'columns' => array());
				
				$index[$kname]['columns'][]=$keys['Column_name'];
			}

			while(list($kname, $info) = each($index))
			{
				$schema.=','."\n";
				$columns=implode($info['columns'],', ');

				if($kname == 'PRIMARY')
					$schema.='  PRIMARY KEY ('.$columns.')';
				elseif($info['fulltext'] == '1')
					$schema.='  FULLTEXT '.$kname.' ('.$columns.')';
				elseif($info['unique'])
						$schema.='  UNIQUE '.$kname.' ('.$columns.')';
					else
						$schema.='  KEY '.$kname.' ('.$columns.')';
				}
			$schema.="\n".');'."\n\n";
			@fputs($fp,$schema);

			// dump the data
			if(in_array($table,$_REQUEST['tablaBackUp']))	// SE EFECTUA EL BACK-UP DE DATOS PARA TODAS LAS TABLAS SELECCIONADAS PREVIAMENTE
			{
				$query="SELECT ".implode(',',$table_list)." FROM `".$table.'`';
				$res4=$this->sql->query($query);

				while($rows=$this->sql->fila2($res4))
				{
					$schema='INSERT INTO `'.$table.'` ('.implode(', ',$table_list).') VALUES (';
			
					reset($table_list);
					while(list(,$i) = each($table_list))
					{
						if (!isset($rows[$i]))
							$schema.='NULL, ';
						elseif($this->tep_not_null($rows[$i]))
						{
							$row=addslashes($rows[$i]);
							$row=@ereg_replace("\n#", "\n".'\#', $row);
							$schema.='\''.$row.'\', ';
						}
						else
							$schema.='\'\', ';
					}
			
					$schema=@ereg_replace(', $','',$schema).');'."\n";
					@fputs($fp,$schema);
				}
			}
		}

		fclose($fp);

		$this->sql->nuevaBBDD=0;
		unset($_SESSION['nuevaBBDD']);
		set_time_limit(30);

		if($_REQUEST['comprimido'])
		{
			define('LOCAL_EXE_ZIP', '/usr/local/bin/zip');
			exec(LOCAL_EXE_ZIP.' -j '.$this->path_archivos.$backup_file.'.zip '.$this->path_archivos.$backup_file);
			unlink($this->path_archivos.$backup_file);
			$backup_file.='.zip';

			header('Content-type: application/x-octet-stream');
			header('Content-disposition: attachment; filename='.$backup_file);
	
			readfile($this->path_archivos.$this->backup_file);
			unlink($this->path_archivos.$this->backup_file);
			set_time_limit(30);
			exit;
		}

		if($_REQUEST['abrirEnVentana'])	// ABRIR VENTANA CON BBDD
		{
			set_time_limit(30);
?>
<script language="JavaScript" type="text/javascript">
<!--
ancho=800;
alto=400;	
var x=(sc_an-ancho)/2;
var y=(sc_al-alto)/2;
t=window.open('<?=$this->path_archivos.$this->backup_file?>','sql','width='+ancho+',height='+alto+',left='+x+',top='+y+',menubar=yes,resizable=yes,scrollbars=yes');
t.focus();
-->
</script>
<?php
		}
		set_time_limit(30);
		return;
	}

//******************************************************
//	FORMULARIO
//******************************************************
	function formularioEjecutarBackUp($destino,$numXXX=0,$fraseTitularFormulario)
	{
		$botonEjecutar='hacerBackUp()';
		if($_SESSION['usuarioNivel']<15)
			$botonEjecutar='alert(\'Lamentamos informar que usted no tiene los permisos adecuados para efectuar esta operación.\n\nPóngase en contacto con el administrador para solucionar este inconveniente\');';
		$elMomento=date('Hi');

		if($elMomento == $_REQUEST['momento'])	// HA DE TRANSCURRIR UN MINUTO PARA PODER EFECTUAR NUEVAMENTE LA COPIA DE SEGURIDAD
		{
			$botonEjecutar='alert(\'debe aún esperar unos instantes. Para actualizar el contador, haga click en el botón \\\'Recargar esta página\\\'\')';
			$trRecargar='<tr><td colspan="12" align="center" height="10" valign="bottom">Debe esperar unos instantes para volver a efectuar<br />la copia de seguridad, transcurrido un minuto,<br />haga click en el botón que aparece aquí abajo,<br /><span style=color:'.$_SESSION['colorTextWarning'].'>entonces desaparecerá este mensaje...</span><br /><br /><a href="#null" class="btn" onclick="document.location=\'back_up.php?momento='.$_REQUEST['momento'].'\'" style="width:200px;" >Recargar esta página</a><br /><br /></td></tr>';
		}

		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
noListar=1;

function hacerBackUp()
{
	document.getElementById("tblFormulario").style.display='none';
	document.getElementById("divMesaje").style.display='inline';
	document.getElementById("spnBase").innerHTML="<center>para '"+document.f<?=$idunico?>.nombre_bbdd.value+"'</center>";
	document.f<?=$idunico?>.momento.value="<?=date('Hi')?>";
	document.f<?=$idunico?>.<?=($_SESSION['usuarioNivel']>14?'accion':'backUpOperation')?>.value=1;
	document.getElementById("tblnombrestablas").style.display="none";
	document.f<?=$idunico?>.submit();
}
-->
</script>

<script language="JavaScript" type="text/javascript">
<!--
$(function(){$('input').customInput();});

jQuery.fn.customInput=function()
{
	$(this).each(function(i){	
		if($(this).is('[type=checkbox]')){
			var input = $(this);
			// get the associated label using the input's id
			var label = $('label[for='+input.attr('id')+']');

			//get type, for classname suffix 
			var inputType = 'checkbox';

			// wrap the input + label in a div 
			$('<div class="custom-'+ inputType +'"></div>').insertBefore(input).append(input, label);
			
			// find all inputs in this set using the shared name attribute
			var allInputs = $('input[name='+input.attr('id')+']');
			
			// necessary for browsers that don't support the :hover pseudo class on labels
			label.hover(
				function(){ 
					$(this).addClass('hover'); 
					if(inputType == 'checkbox' && input.is(':checked')){ 
						$(this).addClass('checkedHover'); 
					} 
				},
				function(){ $(this).removeClass('hover checkedHover'); }
			);
			
			//bind custom event, trigger it, bind click,focus,blur events					
			input.bind('updateState', function(){	
				if(input.is(':checked')){
					label.addClass('checked');
					$(this).blur();
				}
				else{label.removeClass('checked checkedHover checkedFocus');}
										
			})
			.trigger('updateState')
			.click(function(){ 
				$(this).trigger('updateState'); 
			})
			.focus(function(){ 
				label.addClass('focus'); 
				if(inputType == 'checkbox' && input.is(':checked')){ 
					$(this).addClass('checkedFocus');
					$(this).blur();
				} 
			})
			.blur(function(){ label.removeClass('focus checkedFocus'); });
		}
	});
};

var estadoMarcados=1;

function marcarDesmarcarCheckBox(formulario,el)
{
	marcar=false;
	if(el) marcar="true";
	var n=0;

	for(i=0;i<document.forms[formulario].elements.length;i++)
	{
		if(document.forms[formulario][i].type!='checkbox') continue;

		var input = $(document.forms[formulario]["check-"+n]);
		var label = $('label[for='+input.attr('id')+']');

		if(marcar)
		{
			document.forms[formulario]["check-"+n].checked=true;
			label.addClass('checked');
		}
		else
		{
			document.forms[formulario]["check-"+n].checked=false;
			label.removeClass('checked checkedHover checkedFocus');
		}
		n++;
	}

	estadoMarcados=1;
	if(el) estadoMarcados=0;
	return false;
}

noListar=1;
-->
</script>

<style>
<!--
/*	wrapper divs */
.custom-checkbox{position:relative;color:<?=($_SESSION['colorTextoStd']?$_SESSION['colorTextoStd']:'#888;')?>;}

/* input, label positioning */
.custom-checkbox input{position:absolute;left:2px;top:3px;margin:0;z-index:0;}

.custom-checkbox label{
display:block;
position:relative;
z-index:1;
font-size:1.06em;
padding-right:1em;
line-height:1em;
margin:0 0 .1em;
cursor:pointer;
max-height:12px;
vertical-align:top !important;
padding-top:0;
}

/* states */
.custom-checkbox label{background:url(_img/checkbox.gif) no-repeat;padding:.5em 0 .5em 30px;}
.custom-checkbox label{background-position:-10px -14px;}
.custom-checkbox label.hover,.custom-checkbox label.focus{background-position:-10px -114px;}
.custom-checkbox label.checked{background-position:-10px -214px;color:<?=$_SESSION['colorOverImportant']?>;}
.custom-checkbox label.checkedHover,.custom-checkbox label.checkedFocus{background-position:-10px -314px;}
.custom-checkbox label.focus{outline:1px dotted #ccc;}
-->
</style>

<form action='<?=$destino?>' method='post' name='f<?=$idunico?>' enctype='multipart/form-data' onsubmit='alert("Por favor, para validar el formulario, pulse el botón [Guardar datos]");return false'>
<input type='hidden' name='<?=($_SESSION['usuarioNivel']>14?'accion':'backUpOperation')?>' value='<?=$accion?>'>
<input type='hidden' name='id' value='<?=$this->id?>'>
<input type='hidden' name='momento' value='<?=$_REQUEST['momento']?>'>
<div id='divMesaje' style='display:none;text-align:center;width:100%' class=''><center><br /><br /><br /><i><h3 style="color:#FF3B00">INICIANDO LA EJECUCI&Oacute;N DEL PROCESO...</h3></i></center><br /><br /><span id="spnBase" style="font-size:20px;font-weight:bold"></span><br /><br /><center>Por favor, no interrumpa este proceso.</center></div>

<?php
//	INCLUDE PARA PERMITIR DIBUJAR SOLAPAS INCRUSTADAS
include('plug_in_modules.php');
?>

<table align='center' cellpadding='3' cellspacing='0' class='borde' border='0' width='480' id='tblFormulario'>
<thead>
<tr>
	<td colspan='12' class='enc2'><?=$fraseTitularFormulario?></td>
</tr>
</thead>
<?php
		if($_SESSION['usuarioNivel']>14)
		{
			echo '<tr>
	<td align="right">Abrir el archivo automáticamente en ventana:</td>
	<td>
';
			echo '<select name=\'abrirEnVentana\' style=\'width:50px\' title=\'Abrir inmediatamente en nueva ventana\'>';
			echo '<option value=0 ';
			if($this->activa==0)
				echo 'selected=\'selected\'';
			echo '>NO</option>';
			echo '<option value=1 ';
			if($this->activa==1)
				echo 'selected=\'selected\'';
			echo '>SÍ</option>';
			echo '</select>';
			echo '	</td>
</tr>';
		}
		else
		{
			echo '<input name=\'abrirEnVentana\' type=\'hidden\' value=\'0\'>';
		}
?>
<tr>
		<td align="right">Nombre de la BBDD:</td>
		<td>
<?php
		if($_SESSION['usuarioNivel']>14)
		{
			echo '<input type="text" name="nombre_bbdd" ';
			echo ' value="';
			if(!$_REQUEST['nombre_bbdd']) echo $this->sql->db;
			echo '" />';
		}
			else echo '<b>'.$this->sql->db.'</b>';
?>
		</td>
</tr>
<!--
<tr>
	<td colspan='' align='right' height="20" valign="middle">Comprimir el archivo generado</td>
	<td colspan='' align='left' height="20" valign="middle"><input type="checkbox" name="comprimido" id="comprimido" value="1" /></td>
</tr>
-->
<tr>
	<td colspan='12' align='center' height="50" valign="middle">Pulse el botón "<b>Realizar el back-up</b>" para realizar la copia de seguridad</td>
</tr>
<tr height="22">
	<td colspan='12' align='center' valign="bottom"><a href="#null" class='btn' onclick="<?=$botonEjecutar?>" style="width:100px;" >Realizar el back-up</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#null" class='btn xcancel' onclick="document.location='<?=$destino?>?reset=1'" style="color:<?=$_SESSION['colorTextWarning']?>;background-color:<?=$_SESSION['colorFondoWarning']?>">Reset</a></td>
</tr>
<tr>
	<td colspan='12' align='center' height="20" valign="bottom">&nbsp;</td>
</tr>
<?=$trRecargar?>

<?php
		if($this->id) echo '<tr><td height="20"></td></tr>';
?>
</table>

<table id="tblnombrestablas" align="center">
	<tr>
  	<td>
<?php
// SE GENERA UN ARRAY CON LAS TABLAS PRESENTES EN EL SITIO
//		$query='SHOW FULL TABLES FROM '.$this->sql->db.'';
		$query='SELECT table_name,(data_length + index_length) FROM information_schema.TABLES WHERE table_schema ="'.$this->sql->db.'" ORDER BY table_name';
		$res=$this->sql->query($query);

		while($arra=$this->sql->fila($res))
		{
			$this->nombreTablasBBDD[]=$arra[0];
			$this->dimensionesBBDD[]=number_format(($arra[1]/1000),1,'.','.').'Kb';
			$this->totalArchivo+=$arra[1];
		}

		$numColumnas=6;
		$salidatablas='<br /><table border="0" cellspacing="0" cellpadding="6" align="center" style="" class="borde">';
		$salidatablas.='<thead><tr><td colspan="'.$numColumnas.'" align="left" style="">Tablas presentes en la base de datos:&nbsp;<span style="color:'.$_SESSION['colorTextWarning'].'">MARCAR LAS TABLAS QUE SE DEBAN <b>RESPALDAR</b></span>&nbsp;(las demás tablas serán ignoradas y no tendrán copia de seguridad).</td></tr></thead>';
		$salidatablas.='<tr>';

		foreach($this->nombreTablasBBDD as $key=>$nombreTabla)
		{
			$marcado='checked="true"';
			if(in_array($nombreTabla,$this->tablasOmitidas)) $marcado='';

			$salidatablas.='<td>
				<input id="check-'.$key.'" type="checkbox" name="tablaBackUp['.$key.']" '.$marcado.' value="'.$nombreTabla.'" /><label for="check-'.$key.'" title="Marcar/desmarcar">'.strtolower(str_replace('_',' ',$nombreTabla)).'<br /><i>'.$this->dimensionesBBDD[$key].'</i></label></td>';

			if(($key%ceil($numColumnas)==($numColumnas-1)))
				$salidatablas.='</tr><tr>';
		}

		$salidatablas.='</tr>';
		$salidatablas.='<tr><td colspan="2" style="color:'.$_SESSION['colorTableBorde'].'">Dimensión de toda la BBDD: '.number_format(($this->totalArchivo/1000),1,'.','.').' Kb</td><td colspan="'.($numColumnas-2).'" align="right"><a href="#null" class="btn" onclick="marcarDesmarcarCheckBox(\'f'.$idunico.'\',estadoMarcados)" style="padding-left:20px;padding-right:20px;" >Marcar / Desmarcar todas las tablas</a></td></tr>';
		$salidatablas.='</table>';

		echo $salidatablas;

//echo'<pre>';print_r($this->nombreTablasBBDD);
?>
		</td>
   </tr>
</table>
</form>
<?php
		return $idunico;
	}
}
?>