<?php
@header('Content-Type: text/html; charset=utf-8');
/*
'OperadorSite'=>5,
'AdminSite'=>13,
'SuperAdmin'=>14,
'MegaAdmin'=>18,
'Good'=>20
*/
class Base
{
	public $filasporpagina=10;
	public $verIdRegistros;
	public $verEmergentesTextosFragmentados;
	public $decimalesEnListado;
	public $sql;
	public $debug=0;
	public $magic_quotes_on=0;
	public $rutaImgFondo;
	public $hashDirCode;
	public $permiteEliminarGlobal;
	public $levelAccessModules;
	public $levelAccessMaxLevel;

//******************************************************
//	INICIALIZACION DE LA CLASE
//******************************************************
	public function __construct()
	{
		$this->rutaImgFondo='img/';
		$this->permiteEliminarGlobal=null;
	}

//******************************************************
//
//******************************************************
	function generarCsvExcel($params='',$sqlPersonalizado='')
	{
		$nombreTabla=$_REQUEST['tablaBBDD']?$_REQUEST['tablaBBDD']:$this->nombreTabla;
		$queryLocal='SELECT * FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.$nombreTabla.' WHERE 1'.$params;
		$query=($sqlPersonalizado)?$sqlPersonalizado:$queryLocal;
//echo $query;exit;
		$res=$this->sql->query($query);
		$resStru=$this->sql->query($query);
		$strucTb=array_keys($this->sql->fila2($resStru));
		$salidaSql='';

		foreach($strucTb as $key=>$campo)
		{
			if($this->campoStart>=$key) continue;	// SE OMITEN CAMPOS AL INICIO DE LA TABLA
			if($this->campoEnd<=$key) continue;	// SE OMITEN CAMPOS AL FINAL DE LA TABLA
			$salidaSql.='"'.$campo.'";';
		}

		$salidaSql=substr($salidaSql,0,-1)."\r\n";
		while($arra=$this->sql->fila2($res))
		{
			foreach($strucTb as $key=>$campo)
			{
			if($this->campoStart>=$key) continue;	// SE OMITEN CAMPOS AL INICIO DE LA TABLA
			if($this->campoEnd<=$key) continue;	// SE OMITEN CAMPOS AL FINAL DE LA TABLA
				$salidaSql.='"'.($arra[$campo]).'";';	// CON ID
			}
			$salidaSql=substr($salidaSql,0,-1);
			$salidaSql.="\r\n";
		}
		$salidaSql=substr($salidaSql,0,-2).';';
		return $salidaSql;
	}

//******************************************************
// EDITAR CON MARCAS 'html'
//******************************************************
	function PhpEditarHTML($nombreTabla,$nombreFormulario,$idRegistro=0)
	{
		echo '<script>
function editarHTML(campo,anchoPantalla)
{
	var ancho=755;
	var alto=500;
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	if(anchoPantalla) ancho=anchoPantalla;
	window.open("editor/editor.php?tbl='.$nombreTabla.'&nombreFormulario='.$nombreFormulario.'&idRegistro='.$idRegistro.'&nombreCampo="+campo,"editH","left="+x+"º,top="+y+",width="+ancho+",height="+alto+",resizable=no,scrollbars=yes");
	return;
}
</script>';
		return;
	}

	//******************************************************
	//	EXTARE CADENAS CON LAS PALABRAS COMPLETAS
	//******************************************************
	function extraerPalabrasCompletas($frase,$numPalabreas=30)
	{
			$palabras=@ereg_replace('[\., :;]',' ',$frase);
			$auxiliar=@split(' ',$palabras);
			$frase='';
			for($n=0;$n<$numPalabreas;$n++) $frase.=$auxiliar[$n].' ';
			if(count($auxiliar)>$n) $frase.='[...]';
			return $frase;
	}

	//******************************************************
	//	EXTARE CADENAS CON LAS PALABRAS COMPLETAS (2)
	//******************************************************
	function extraerPalabrasCompletasDos($string, $limit=300, $break=".", $pad="&nbsp;[...]")
	{
		// return with no change if string is shorter than $limit
		if(strlen($string) <= $limit)
		return $string;

		// is $break present between $limit and the end of the string?
		if(false !== ($breakpoint = strpos($string, $break, $limit)))
		{
			if($breakpoint < strlen($string) - 1)
			{
				$string = substr($string, 0, $breakpoint) . $pad;
			}
		}
		return $string;
	}

//******************************************************
// limpiar textos
//******************************************************
	function quitar_acentos($cadena)
	{
		return strtr($cadena, "???????��������������������������������������������������������������", "SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy");
	}

//******************************************************
// pone en mayusculas el primer caracter de todas las palabras de la variable $text
//******************************************************
	function FixMayusculasTexto($text)
	{
		$text = strtolower($text);
		$sentences = explode(' ',$text);
		$num_palab=count($sentences);
		foreach($sentences as $x)
		{
				if($num_palab-1==$n) $outtext .= ucfirst(trim($x));
				else $outtext .= ucfirst(trim($x)).' ';
				$n++;
		}
		return $outtext;
	}

//******************************************************
// pone en mayusculas el primer caracter de oracion de $TEXT hasta que encuentra un punto o una interrogacion y repite
//******************************************************
	function FixMayusculasTextoOracion($text)
	{
		$text = strtolower($text);
		$sentences = preg_split("/(\.(\s)?|\?(\s)?)/",$text,-1,PREG_SPLIT_DELIM_CAPTURE);
		foreach($sentences as $x)
		{
			if((trim($x) == ".") || (trim($x) == "?"))
				$outtext .= trim($x)." ";
			else
				$outtext .= ucfirst(trim($x));
		}
		return $outtext;
	}

//******************************************************
// corrige las incompatibilidades de las comillas (')
//******************************************************
	function limpiar_quotes($datos)
	{
		$this->magic_quotes_on=get_magic_quotes_gpc();
		foreach($datos as $key=>$value)
		{
			if($this->magic_quotes_on)
			$datos[$key]=addslashes($value);
		}
	}

/*
 * Funcion paginar
 * Devuelve un texto que ejecuta la paginacion
 */
	function paginacion($totalReg,$pp,$offset_u,$url,$TotPagMostrar,$framex=0)
	{
		if(!$framex) $framex=rand(0,10000);
		$MitadTotPagMostrar=$TotPagMostrar/2;
		if($totalReg>$pp)
		{
			$resto=$totalReg%$pp;
			if($resto==0)
				$pages=$totalReg/$pp;
			else
				$pages=(($totalReg-$resto)/$pp)+1;

			if($pages>$TotPagMostrar)
			{
				$current_page=($offset_u/$pp)+1;
				if($offset_u==0)
				{
					$first_page=0;
					$last_page=$TotPagMostrar;
				}
				else if(($current_page>=$MitadTotPagMostrar) && ($current_page<=($pages-$MitadTotPagMostrar)))
				{
					$first_page=$current_page-$MitadTotPagMostrar;
					$last_page=$current_page+$MitadTotPagMostrar;
				}
				else if($current_page<$MitadTotPagMostrar)
				{
					$first_page=0;
					$last_page=$current_page+$MitadTotPagMostrar+($MitadTotPagMostrar-$current_page);
				}
				else
				{
					$first_page=$current_page-$MitadTotPagMostrar-(($current_page+$MitadTotPagMostrar)-$pages);
					$last_page=$pages;
				}
			}
			else
			{
				$first_page=0;
				$last_page=$pages;
			}

			if($pages<$last_page)	// se ajusta el numero maximo de paginas a la ultima a mostrar
				$last_page=$pages;

			if($first_page<0)	// se ajusta el limite inferior de paginas evitando los negativos
				$first_page=0;

			for($i=$first_page;$i< $last_page;$i++)
			{
				$pge=str_pad(floor($i+1),2,'0',STR_PAD_LEFT);
				$nextst=$i*$pp;
				$padNum=4;
				if(strlen($pge)>1)
					$padNum=3;
//echo '<pre>';print_r($this);
				if($offset_u==$nextst)
					$page_nav .= '<td><span style="padding-left:'.$padNum.'px;padding-right:3px;color:'.$_SESSION['paginaActivaFore'].';cursor:default;font-size:11px;background-color:'.$_SESSION['listadoFondoClaro'].';border:1px solid '.$_SESSION['colorTextoStd'].';text-align:center" title="p&aacute;g. '.$pge.'">'.$pge.'</span></td><td>&nbsp;</td>';
				else
					$page_nav .= '<td><a class="lk" href=javascript:mostrarPagina("'.$url.$nextst.'",'.$framex.') style="width:22px;text-align:center">'.$pge.'</a></td><td>&nbsp;</td>';	// TD PAGINA NUMERO
			}

			if($offset_u==0)
				$current_page = 1;
			else
				$current_page = ($offset_u/$pp)+1;
			if($current_page < $pages)
			{
				$page_last = '<td>&nbsp;</td><td><b><a class="lk" href=javascript:mostrarPagina("'.$url.($pages-1)*$pp.'",'.$framex.') title="a la &uacute;ltima p&aacute;gina">&raquo;</a></td>';
				$page_next = '<td><a class="lk" href=javascript:mostrarPagina("'.$url.($current_page*$pp).'",'.$framex.') style="width:20px;text-align:center" title="a la p&aacute;gina siguiente">&#8250;</a></td>';
			}
			else
			{
				$page_last = '<td class="lkOff" style="padding-right:3px;padding-left:1px;" title="Esta es la &uacute;ltima p&aacute;gina, no hay m&aacute;s p&aacute;ginas"><b>&raquo;</b></td>';
				$page_next = '<td class="lkOff" style="padding-right:8px;padding-left:1px;width:15px;text-align:center" title="Esta es la &uacute;ltima p&aacute;gina, no se puede avanzar m&aacute;s">&#8250;</td><td></td>';
			}
			if($offset_u > 0)
			{
				$page_first = '<td><b><a class="lk" href=javascript:mostrarPagina("'.$url.'0",'.$framex.') title="a la prinera p&aacute;gina") >&laquo;</a></b></td><td>&nbsp;</td>';
				$page_previous = '<td><a class="lk" href=javascript:mostrarPagina("'.$url.(($current_page-2)*$pp).'",'.$framex.') style="width:20px;text-align:center" title="a la p&aacute;gina anterior">&#8249;</a></td><td>&nbsp;</td>';
			}
			else
			{
				$page_first = '<td class="lkOff" style="padding-right:4px;padding-left:1px;" title="Es la primera p&aacute;gina, no hay otras antes"><b>&laquo;</b></td><td>&nbsp;</td>';
				$page_previous = '<td class="lkOff" style="padding-right:3px;padding-left:1px;width:16px;" title="Es la primera p&aacute;gina, no se puede retroceder">&#8249;</td><td>&nbsp;</td>';
			}
		}
		return '<center><table border=0 cellspacing=0 cellpadding=0 class=txt ><tr>'.$page_first.' '.$pageBlFirst.' '.$page_previous.' '.$page_nav.' '.$page_next.' '.$pageBlLast.' '.$page_last.'</tr></table></center>';
	}

//******************************************************
// permite la educion de caracteres especiales en los campos
//******************************************************
	function output_datos($dato)
	{
//echo'<hr>1.'.$dato;
		$res=htmlspecialchars($dato, ENT_QUOTES);
//		$res=htmlspecialchars($dato, ENT_COMPAT,'utf8');
		return $res;
	}
//******************************************************
// Elimina caracteres problematicos en nombres de fichero
//******************************************************
	function limpianombre($nombre)
	{
		$t=strlen($nombre);
		for ($x=0;$x<$t;$x++) if((ord($nombre{$x})>47 && ord($nombre{$x})<91) || (ord($nombre{$x})>96 && ord($nombre{$x})<123) || ($nombre{$x}=='.')) $salida.=$nombre{$x};
		return $salida;
	}

//******************************************************
// Genera tres combos para seleccionar una fecha
//******************************************************
	function combos_fecha($n1,$n2,$n3,$valor,$param,$hoy='')
	{
		$valores=explode ("#",$param);

		if($hoy) $valor=date('Y-m-d');

		$arra=explode(" ",$valor);
		$fecha=$arra[0];
		$arra=explode("-",$fecha);
		$v1=$arra[2];
		$v2=$arra[1];
		$v3=$arra[0];

		echo "<select name='$n1' class='tx9'><option value='' ";
		if(!$valor) echo 'selected';echo ">-</option>";
		for ($x=1;$x<32;$x++)
			if($x==$v1) echo "<option value='$x' selected>$x</option>";
				else echo "<option value='$x'>$x</option>";
		echo "</select> - ";
		echo "<select name='$n2' class='tx9'><option value='' ";
		if(!$valor) echo 'selected';echo ">-</option>";
		for ($x=1;$x<13;$x++)
			if($x==$v2) echo "<option value='$x' selected>".$this->nombre_mes($x)."</option>";
				else echo "<option value='$x'>".$this->nombre_mes($x)."</option>";
		echo "</select> - ";
		echo "<select name='$n3' class='tx9'><option value='' ";
		if(!$valor) echo 'selected';echo ">-</option>";

		for ($x=$valores[0];$x<$valores[1];$x++)
			if($x==$v3) echo "<option value='$x' selected>$x</option>";
				else echo "<option value='$x'>$x</option>";
		echo "</select>";
	}

//******************************************************
// Genera tres combos para seleccionar una fecha sin el dia
//******************************************************
	function combos_fecha_corto($n2,$n3,$valor,$param,$hoy='')
	{
		$valores=explode ("#",$param);

		if($hoy)
			$valor=date('Y-m-d');
		$arra=explode(" ",$valor);
		$fecha=$arra[0];
		$arra=explode("-",$fecha);
		$v2=$arra[1];
		$v3=$arra[0];

		echo "<select name='$n2' class='tx9'><option value='' ";if(!$valor) echo 'selected';echo ">-</option>";
		for ($x=1;$x<13;$x++)
			if($x==$v2) echo "<option value='$x' selected>".$this->nombre_mes($x)."</option>";
				else echo "<option value='$x'>".$this->nombre_mes($x)."</option>";
		echo "</select> - ";
		echo "<select name='$n3' class='tx9'><option value='' ";if(!$valor) echo 'selected';echo ">-</option>";
		for ($x=$valores[0];$x<$valores[1];$x++)
			if($x==$v3) echo "<option value='$x' selected>$x</option>";
				else echo "<option value='$x'>$x</option>";
		echo "</select>";
	}

//******************************************************
// Devuelve el nombre del mes a partir de su numero
//******************************************************
	function nombre_mes($num)
	{
		if($num==0) $num=12; if($num==13) $num=1;
		if($num==1) $t='enero'; if($num==2) $t='febrero'; if($num==3) $t='marzo';
		if($num==4) $t='abril'; if($num==5) $t='mayo'; if($num==6) $t='junio';
		if($num==7) $t='julio'; if($num==8) $t='agosto';if($num==9) $t='septiembre';
		if($num==10) $t='octubre'; if($num==11) $t='noviembre'; if($num==12) $t='diciembre';
		return $t;
	}

//******************************************************
// Formatea una fecha de formato MySQL SIN la hora
//******************************************************
	function convierte_fecha($fecha,$tipo=0)
	{
		$arra=explode(" ",$fecha);
		$fecha=$arra[0];
		$hora=$arra[1];
		$arra=explode("-",$fecha);
		$dia=$arra[2];
		$mes=$arra[1];
		$ano=$arra[0];
		$fecha=$dia . "-" . $mes . "-" . $ano;
		if($tipo) $fecha=$dia . ' de ' . nombre_mes($mes) . ' de ' . $ano;
		return $fecha;
	}

//******************************************************
// Formatea una fecha de formato MySQL incluyendo la hora
//******************************************************
	function convierte_fecha_hora($fecha,$tipo=0)
	{
		$arra=explode(" ",$fecha);
		$fecha=$arra[0];
		$hora=$arra[1];
		$arra=explode("-",$fecha);
		$dia=$arra[2];
		$mes=$arra[1];
		$ano=$arra[0];
		$fecha=$dia . "-" . $mes . "-" . $ano." ".$hora;
		if($tipo) $fecha=$dia . ' de ' . nombre_mes($mes) . ' de ' . $ano.' a las '.$hora;
		return $fecha;
	}

//******************************************************
//	DEVUELVE LA LISTA DE DIRECTORIOS Y SUB-DIRECTORIOS
//******************************************************
	function recuperar_directorios($rootdirpath)
	{
	 if($dir = @opendir($rootdirpath))
	 {
		 $array[] = $rootdirpath;
		 while (($file = readdir($dir)) !== false)
			 if(is_dir($rootdirpath."/".$file) && $file != "." && $file != "..")
				 $array = array_merge($array, $this->recuperar_directorios($rootdirpath.$file.'/'));
		 closedir($dir);
	 }
	 return $array;
	}

//******************************************************
//	FORMATEA LA VARIABLE '$dato', UTILIZADA PARA NOMBRE DE CARPETAS
//******************************************************
	function formatear_nombre_carpeta($dato,$mayusculas=0)
	{
		$dato=trim($this->quitar_acentos($dato));
		$dato=str_replace(',','',trim($dato));
		$caracteres_invalidos=array(',',"\\'","'",'"','´','`',' ','&nbsp;');
		$dato=str_replace($caracteres_invalidos,'_',$dato);
		$dato=$mayusculas?strtoupper($dato):strtolower($dato);
		return $dato;
	}

//******************************************************
//	files
//******************************************************
	function recuperar_files($dirname,$extensiones=FALSE,$reverso=FALSE)
	{
		//EXTENSIONES POR DEFECTO QUE SE QUIEREN TESTEAR
		if(!$extensiones) $extensiones=array('jpg','png','jpeg','gif','swf');
		$files=array(); $dir=opendir($dirname);

		while(false!==($file=@readdir($dir)))	//RECUPERA LOS FILES DE ACUERDO CON LAS EXTENSIONES PRESENTES EN EL ARRAY
		{
			for($i=0; $i < count($extensiones);$i++)
			{
				if(preg_match('/.'.$extensiones[$i]."$/",$file)) //Notese los espacios
					if(preg_match('/.'.$extensiones[$i]."$/",$file)) //Notese los espacios
					{
						$files[]=$file;
					}
			}
		}
		@closedir($dir);
		if($reverso)	rsort($files);	// ORDER OF THE ARRAY
			else sort($files);
		return $files;
	}

//******************************************************
//	DEVUELVE LA LISTA DE DIRECTORIOS Y SUB-DIRECTORIOS
//******************************************************
	function retrieveDirs($rootdirpath)
	{
	 if($dir = @opendir($rootdirpath))
	 {
		 $array[] = $rootdirpath;

		 while (($file = readdir($dir)) !== false)
			 if(is_dir($rootdirpath."/".$file) && $file != "." && $file != "..")
				 $array = array_merge($array, $this->retrieveDirs($rootdirpath."/".$file));

		 closedir($dir);
	 }
	 return $array;
	}

//******************************************************
//	DEVUELVE EL FILTRO SQL DEPENDIENDO DE LOS CONTROLES PRESENTES EN EL FORMULARIO, IDENTIFICANDO EL FORMULARIO DE ORIGEN PARA NO MEZCLAR DATOS
//
//	NOTA IMPORTANTE: utilizar el POUND (#) como identificador de otras tablas para discriminar los filtros de tablas distintas a la principal
//******************************************************
	function crearFiltrosBusquedas($posicionSolapa=0,$prefijoTabla='')
	{
		if(count($_SESSION['filtros']))	// SI EXISTEN FILTROS DE BUSQUEDA, SE CONSTRUYEN LOS FILTROS APROPIADOS
			foreach($_SESSION['filtros'] as $key=>$value)	// SE RECORREN TODAS LAS VARIABLES DEL ARRAY 'filtros'
			{
				$pos=strpos($key,'_busca');
				$longitCampoClave=strlen($posicionSolapa);
				$prefijoCampo=substr($key,0,$longitCampoClave);	// NECESARIO PARA DISCRIMINAR EL FORMULARIO DONDE SE EFECTUA LA BUSQUEDA

				// SI LA VARIABLE ES CORRECTA, SE CONSTRUYE EL FILTRO CON EL NOMBRE DE LA MISMA PARA EL NOMBRE DEL CAMPO (strings)
				if($pos>0 && $prefijoCampo == $posicionSolapa)
					if($_SESSION['filtros'][$key] && ($_SESSION['filtros'][$key]!=-1))
					{
						$campoX=substr($key,$longitCampoClave);
						if($limpiar=strpos($campoX,'#'))	// SI HAY UN CAMPO QUE PERTENECE A UN TABLA DISTINTA A LA PRINCIPAL, SE COMPONE EL FILTRO
						{
							$prefijoTabla='';
							$campoX=str_replace('#','.',$campoX);
						}
						$campoX=substr($key,$longitCampoClave);
						$filtroSql.=' AND '.$prefijoTabla.substr($campoX,0,$pos-$longitCampoClave).' LIKE \'%'.$_SESSION['filtros'][$key].'%\' ';
					}

				$posN=strpos($key,'_buscN');
				// SI LA VARIABLE ES CORRECTA, SE CONSTRUYE EL FILTRO CON EL NOMBRE DE LA MISMA PARA EL NOMBRE DEL CAMPO (con id's numericos)
				if($posN > 0 && $prefijoCampo == $posicionSolapa)
					if($value > -1 && $_SESSION['filtros'][$key]!='')
					{
						$campoX=substr($key,$longitCampoClave);
						if($limpiar=strpos($campoX,'#'))	// SI HAY UN CAMPO QUE PERTENECE A UN TABLA DISTINTA A LA PRINCIPAL, SE COMPONE EL FILTRO ADECUADO
						{
							$prefijoTabla='';
							$campoX=str_replace('#','.',$campoX);
						}
						$filtroSql.=' AND '.$prefijoTabla.substr($campoX,0,$posN-$longitCampoClave).'='.$_SESSION['filtros'][$key].' ';
					}

				$posX=strpos($key,'_buscX');

				if($posX > 0 && $prefijoCampo == $posicionSolapa)	// CONSTRUCTORES PERSONALIZADOS
					if($value > -1 && $_SESSION['filtros'][$key] != '')
					{
						$campoX=substr($key,$longitCampoClave);
						if($limpiar=strpos($campoX,'#'))	// SI HAY UN CAMPO QUE PERTENECE A UN TABLA DISTINTA A LA PRINCIPAL, SE COMPONE EL FILTRO ADECUADO
						{
							$prefijoTabla='';
							$campoX=str_replace('#','.',$campoX);
						}
						$campoX=substr($key,$longitCampoClave);
						$filtroSql.=' AND '.$prefijoTabla.substr($campoX,0,$posX-$longitCampoClave).' = '.$_SESSION['filtros'][$key].' ';
					}
				$posF=strrpos($key,'_buscF');	// !!! NOTA: para discriminar correctamente los campos, han de contener en el nombre unos identificativos (Dia o Mes o Anio)

				// FILTRO PARA FECHAS
				//*******************************************************************************************************************	(02.12.2008)
				// *******  formato OBLIGATORIO PARA LAS FECHAS: "dtxxx_" + nombre_campo + "=" + (Dia_buscF o Mes_buscF o Anio_buscF)
				//*******************************************************************************************************************

				if($posF > 0 && $prefijoCampo == $this->posicionSolapa)
					if($value > -1 && $_SESSION['filtros'][$key] != '')
					{
						if(strpos($key,'='))	// CARACTER UTILIZADO EXCLUSIVAMENTE PARA FILTROS CON FECHAS
						{
							$nombreCampoFiltro1=substr($key,strpos($key,'dtxxx_')+6);
							$longCadena=stripos($nombreCampoFiltro1,'=');
							$nombreCampoFiltro=substr($key,strpos($key,'dtxxx_')+6,$longCadena);	// SE EXTRAE EL NOMBRE DEL CAMPO PARA EFECTUAR CORRECTAMENTE LA QUERY

							if(strpos($key,'Mes') == true)
								$filtroSql.=' AND MONTH('.$prefijoTabla.$nombreCampoFiltro.')='.$_SESSION['filtros'][$key].' ';

							if(strpos($key,'Anio') == true)
								$filtroSql.=' AND YEAR('.$prefijoTabla.$nombreCampoFiltro.')='.$_SESSION['filtros'][$key].' ';

							if(strpos($key,'Dia') == true)
								$filtroSql.=' AND day('.$prefijoTabla.$nombreCampoFiltro.')='.$_SESSION['filtros'][$key].' ';
						}
					}
			}

		return ($filtroSql . ' '. utf8_encode($filtroSql) . ' ');
//		return ($filtroSql . ' OR (1 '. utf8_encode($filtroSql) . ')');
	}

//******************************************************
//	SE CONSTRUYUEN LAS VARIABLES PARA LAS BUSQUEDAS DEPENDIENDO DE LOS CONTROLES PRESENTES EN EL FORMULARIO
//******************************************************
	function crearVariablesBusquedas($posicionSolapa='')
	{
		foreach($_REQUEST as $key=>$value)	// SE RECORREN TODAS LAS VEIABLES DEL ARRAY REQUEST
		{
			$pos=strpos($key,'_busca');

			if($pos>0)	// SE CONSTRUYE LA VARIABLE DE SESION CON EL NOMBRE DEL MISMO "REQUEST" + EL VALOR DEL REQUEST (strings)
				if($_REQUEST[$key])
					$_SESSION['filtros'][$key]=trim(str_replace('\'','%',$_REQUEST[$key]));

			$posN=strpos($key,'_buscN');

			if($posN>0)	// SE CONSTRUYE LA VARIABLE DE SESION CON EL NOMBRE DEL MISMO "REQUEST" + EL VALOR DEL REQUEST (id's numericos)
				if($_REQUEST[$key]>-1)
					$_SESSION['filtros'][$key]=trim($_REQUEST[$key]);

			$posF=strpos($key,'_buscF');

			if($posF>0)	// SE CONSTRUYE LA VARIABLE DE SESION CON EL NOMBRE DEL MISMO "REQUEST" + EL VALOR DEL REQUEST (fechas)
				if($_REQUEST[$key]>-1)
					$_SESSION['filtros'][$key]=trim($_REQUEST[$key]);

			$posX=strpos($key,'_buscX');

			if($posX>0)	// SE CONSTRUYE LA VARIABLE DE SESION CON EL NOMBRE DEL MISMO "REQUEST" + EL VALOR DEL REQUEST (especiales)
				if($_REQUEST[$key]>-1)
					$_SESSION['filtros'][$key]=trim($_REQUEST[$key]);

			$_SESSION['filtros'][$key]=$_SESSION['filtros'][$key];

			if($key=='posicionSolapa')
				$_SESSION['filtros'][$key]=TRUE;
		}
		return;
	}

//******************************************************
//	DEVUELVE LA RAZON SOCIAL DE LA EMPRESA
//******************************************************
	function recuperarNombreEmpresa()
	{
		$query='SELECT e.razon_social, e.id FROM '.$this->sql->prefixTbl.'companies e, '.$this->sql->prefixTbl.'usuarios u WHERE u.id_empresa=e.id && u.id_empresa && u.id='.$_SESSION['id_usuario'];
		$res=$this->sql->query($query);
		$arra=$this->sql->fila2($res);

		$this->empresa=$arra['razon_social'];
		$this->id_empresa=$arra['id'];
		return $this->empresa;
	}

//******************************************************
//	COMPARACION ENTRE FECHAS
//******************************************************
	function compara_fechas($fecha1,$fecha2)
	{
		list($year1, $mes1, $day1) = explode("-", $fecha1);
		list($year2, $mes2, $day2) = explode("-", $fecha2);

		$s1 = mktime(0, 0, 0, $mes1, $day1, $year1);
		$s2 = mktime(0, 0, 0, $mes2, $day2, $year2);

		$diff = $s1 - $s2;

		if($diff < 0)
			return -1;

		if($diff > 0)
			return 1;
		return 0;
	}


//******************************************************
/*
	FUNCION PARA INSERTAR EL BOTON COMUN DE TODOS LOS MODULOS
	PARA PERMITIR LA ELIMINACION DE REGISTROS CON FILTROS DE LISTADOS (v.11.2011.1A-1/2)
*/
//******************************************************
	public function botonEliminarConFiltros($num_res,$id_unico_form='')
	{
		if(count($_SESSION['filtros'])>1 && $_SESSION['filtros']['posicionSolapa'] && $num_res)
		{
			$mens='';
			if($this->permiteEliminarGlobal && $_SESSION['usuarioNivel'] > 14)	// SOLO PARA USUARIOS DE UN DETERMINADO NIVEL
			{
				$mens.='<div id="divExtraFields"></div><span id="spnBtnElimiFilter"><a href="#null" onclick="eliminarRegistrosFiltrados('.$num_res.',\''.$id_unico_form.'\');" class="btn" style="">Borrar '.number_format($num_res,0,',','.').' registros</a></span>&nbsp;&nbsp;';
			}
			$mens.="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b&uacute;squeda:</span> ";
		}
		return $mens;
	}

//******************************************************
// CREACION DEL LISTADO DE RESULTADOS OBTENIDOS EN EL FORMULARIO ABIERTO (FUNCION GENERICA UTILIZADA EN MULTIPLES FORMULARIOS)
//******************************************************
	function crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $datos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $verIdRegistros,$verEmergentesTextosFragmentados=0, $decimales=0, $posicFondo=0, $ocultarEliminar=0,$campoNoPop=0,$verBotonSustituir=0,$noEditar=0)
	{
		$this->rutaImgFondo='img/';
		// OPCIONES EXTRAORDINARIAS POR FILA
		$this->rOptions=array(
			0=>'normal',
			1=>'disabled',
		);
/*----------------------------------------------------------------------------------------
	POSIBLES TIPOS DE DATOS, NECESARIO PARA CXREAR SALIDAS APROPIADAS:
	num = numurico
	mon = moneda (la variable "$this->moneda" contiene el simbolo de la moneda a mostrar, tiene que estar definido antes de la llamada a esta funcion)
	med = unidad de medida (la variable "$this->unidadMedida" contiene el simbolo de la unidad a mostrar, tiene que estar definido antes de la llamada a esta funcion)
	txt = texto
	opc = opciones extras
	fec = fecha
	mai = email
	url = referencia URL
	img = imagen
	rop = opciones extra por filas (2013)

	DETALLE DE LAS VARIABLES NECESARIAS PARA CREAR EL LISTADO DE RESULTADOS
	(MUY IMPORTANTE: el id debe de ser siempre el primer campo)

	$arraTitulares = ARRAY CON LAS ETIQUETAS A MOSTRAR EN EL ENCABEZADO DE LA LISTA
	$this->filasporpagina = NUMERO MAXIMO DE FILAS POR PAGINA
	$datos = ARRAY CON TODOS LOS DATOS DE LOS CONTENIDOS
	$xc = CONTADOR UTILIZADO PARA CREAR EL INDICE PARA EL ARRAY DE LOS DATOS "$datos"
	$verIdRegistros = (1 = SI, 0 = NO)
	$txtRefEliminar = TEXTO A MOSTRAR PARA LA ELIMINACION DEL REGISTRO ( es el indice de "$arraTitulares", empezando por UNO)
	$decimales = NUMERO DE DECIMALES A VISUALIUZAR EN LOS DATOS DE TIPO NUMERICO
	$campoNoPop = campo que no ha de abrir la pop para editar
----------------------------------------------------------------------------------------*/
		$tabla='';$contOrdenacion=0;

		foreach($arraCamposOrdenar as $key=>$value)
		{
			if($value)
			$linkOrdenar[$contOrdenacion]=$value;
			$contOrdenacion++;
		}

		$tabla='<center><div id="paginarTop" style="display:none;padding-top:3px;"></div></center>';
		$tabla.='<table id="tblFilas" align="center" class="borde" cellpadding="5" cellspacing="0" id="tbl_lista" style="margin-top:8px" border="0"><thead>';
		$numCampostxt=count($arraTitulares);
		$numCamposLongi=count($arraLongitudes);

		if($numCampostxt != $numCamposLongi)
			echo '<span style=color:red;font-size:14px;BACKGROUND-COLOR:#FFF><b>ATENCION: ERROR DE DIMENSION ENTRE ARRAY "arraTitulares" Y "arraLongitudes"</b></span><br />';
		$claseTitular='enc';

		if($verIdRegistros)
		{
			$claseOrdenar=$claseTitular;
			if($linkOrdenar[$key]==0)	// el ID usualmente
			{
				if($linkOrdenar[0] == substr($_REQUEST['ordenarPor'],8))
				{
					$modoOrd=' (ascendente)';
					if($_REQUEST['sentidoOrdenacion']==0)
					{
						$modoOrd=' (descendente)';
						$claseOrdenar=' ordDesc ordAct';
					}
					else
						$claseOrdenar=' ordAsc ordAct';
				}
			}
			$tabla.='<tr><td class="'.(($linkOrdenar[$key]) ? 'botones2 ordEnable ':'botones2 ordEnable ').$claseOrdenar.'" width="'.$arraLongitudesTitulares[0].'" title="Click para agrupar u ordenar por '.$linkOrdenar[0].$modoOrd.'" onclick="ordenarPorCampo(\'ordenar_'.$linkOrdenar[0].'\')">';
			$tabla.='ID';
			$tabla.='</td>';
		}

		foreach($arraTitulares as $key=>$value)	// SE CREAN LOS TITULARES DE LA FILA DEL LISTADO
		{
			$celdaActiva=''; $modoOrd='';
			$claseOrdenar=$claseTitular;
			if($linkOrdenar[$key+1]==0)
			{
				if($linkOrdenar[$key+1] == substr($_REQUEST['ordenarPor'],8) && isset($_REQUEST['ordenarPor']))
				{
					if($_REQUEST['sentidoOrdenacion']==0)
						$claseOrdenar=' ordDesc ordAct';
					else
						$claseOrdenar=' ordAsc ordAct';
				}
			}
			if($linkOrdenar[$key+1])
			{
				$celdaActiva=' onclick="ordenarPorCampo(\'ordenar_'.$linkOrdenar[$key+1].'\')" title="Click para agrupar u ordenar por '.$value.$modoOrd.'"';
				$modoOrd='';
				if($linkOrdenar[$key+1] == substr($_REQUEST['ordenarPor'],8))	// SE DIFERENCIA EL BOTON CON EL TITULO DEL CAMPO CON LA ORDENACION ACTIVA
				{
					$modoOrd=' (ascendente)';

					if($_REQUEST['sentidoOrdenacion']==0)
						$modoOrd=' (descendente)';
				}
			}

			$tabla.='<td class="'.(($linkOrdenar[$key+1])?'botones2 ordEnable '.$claseOrdenar:'botones2 listaSimple ').'" width="'.$arraLongitudesTitulares[$key+1].'"'.$celdaActiva.' >'.$value.'</td>';
		}

		if($verBotonSustituir)
			$tabla.='<td width="30" style="border-bottom:1px solid #111" align="center" title="Sustituir la im&aacute;gen">Sustituir</td>';

		$functionDeleteRecord='';

		if(!$ocultarEliminar)
		{
			$tabla.='<td width="30" style="border-bottom:1px solid #111;border-right:0" align="center" title="Borrar el registro">borrar</td>';
		}

		$tabla.="</tr></thead>\n";

		if(count($datos))	// SOLAMENTE SI HAY DATOS, SE DIBUJAN LAS FILAS
		{
			$filaX=0;
			$r=$_SESSION['listadoFondoClaro'];
			$deleteRecordDenied=array();

			foreach($datos as $posRegistro=>$registro)
			{
				$txtEmergente=' title="" ';
				$refeTd=' id="'.$registro[0].'" ';

				if($this->editarFila) $txtEmergente=' title="click para editar" ';

				$scriptEditar=($registro[0] > 0 && !$noEditar) ? 'modifica(' : 'accesoNoPermitido(';

				foreach($registro as $key=>$value)
				{
					if($arraTipos[$key] == 'rop' && $value == 1)	// NO SE PERMITE EDITAR ESTA FILA
					{
					  $txtEmergente=' title="No puede editar esta fila"';
					  $scriptEditar='alert(\'No se puede editar esta fila\');(';
					  $deleteRecordDenied[$key]=1;
					}
					else
					{
					  $deleteRecordDenied[$key]=0;
					}

					$fondo='background:red;';
					$functionDeleteRecord='alert(';
				}

				if($deleteRecordDenied[$key])
				{
					$functionDeleteRecord='alert(\'No se puede eliminar este registro\');(';
					$displayLinkUrl=0;
					$mouseOverLimited=true;
				}
				else
				{
					if($_SESSION['usuarioNivel'] >= $this->levelAccessModules['SuperAdmin'])	// SOLO SE PERMITE ELIMINAR A PARTIR DE SuperAdmin
						$functionDeleteRecord='eliminar(';
					else
						$functionDeleteRecord='alert(\'No puede eliminar este registro\');(';

					$displayLinkUrl=1;
					$mouseOverLimited=false;
				}

				if($posRegistro %2)
					$r=$mouseOverLimited ? $_SESSION['listadoFondoClaroDisabled'] : $_SESSION['listadoFondoClaro'];
				else
					$r=$mouseOverLimited ? $_SESSION['listadoFondoOscuroDisabled'] : $r=$_SESSION['listadoFondoOscuro'];;

				$fondo='';

				if($posicFondo)	// HAY UNA IMAGEN PARA DIBUJAR AL FONDO DE LA CELDA "$posicfondo (que corresponde a $key)" DEL REGISTRO ACTUAL
					$fondo='background-position:right;background-repeat:no-repeat;background-image:url('.$this->rutaImgFondo.$arraImgFondo[$posRegistro].');';
				$tabla.='<tr valign="top" style="background:'.$r.'" title="'.$nombrePoblacionMostrar."\" onMouseOver='over_pro(this,\"".$_SESSION['colorOverListadoFondo']."\",\"".$_SESSION['colorOverListado']."\")' onMouseOut='out_pro(this)'>\n";


				if($verIdRegistros)
				{
					if($this->editarFila)
					{
						$tabla.=' <td '.$refeTd.'class="cont" '.$txtEmergente.' onclick="resaltarCasilla('.$registro[0].',1,1)" align="right"';
						if($this->colorFondoResaltado[$posRegistro] && $_SESSION['nivel_acceso']<8) $tabla.=' style="border-bottom: 1px solid red;';
						if($this->colorFondoResaltado[$posRegistro] && $_SESSION['nivel_acceso']>7) $tabla.=' style="border-bottom: 1px solid red;background-color:'.$this->colorFondoResaltado[$posRegistro];
						if($this->imagenEstado[$posRegistro]) $tabla.=';background-image:url(../estados/'.$this->imagenEstado[$posRegistro].');background-position:left;background-repeat:no-repeat';
						$tabla.='"'.$this->textoEstado[$posRegistro].'><span  title="'.$this->textoEstado[$posRegistro].'">'.$registro[0].'</span></td>';
					}
					else
					{
						$tabla.=' <td '.$refeTd.'class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" align="right"';

						if($this->colorFondoResaltado)	// PONER EL COLOR DE FONDO "colorFondoResaltado[]"
							$tabla.=' style="border-bottom:1px solid '.$this->colorFondoResaltado[$filaX++].'"';

						$tabla.='>'.$registro[0].'</td>';
					}
				}

				foreach($registro as $key=>$value)
				{
					$datoIntegro=$value;

					if(!$key) continue;	// SE OMITE EL ID

					if(strlen($value) > $arraLongitudes[$key-1] && $arraTipos[$key] != 'rop')
					{
						if($verEmergentesTextosFragmentados)
							$txtEmergente=' title="'.strip_tags($value).'" ';

						$value=substr($value,0,($arraLongitudes[$key-1]-6)).' <i>[...]</i>';
					}

					if($arraTipos[$key]=='img')	// CREA LINK PARA LA URL DEL REGISTRO
					{
						$dat=@getimagesize($value);
						if($value)
						{
							$txtEmergente='';
							$value='<center><a href="javascript:admin_abre_imagen2(\''.$value.'\','.$dat[0].','.$dat[1].',\''.$value.'\')" style="border:0px;"><img src="miniatura.php?img='.$value.'&w=100&h=30" style="border:1px solid #000;" title="Abrir la imagen (ancho: '.$dat[0].'px, alto: '.$dat[1].'px)" ></a></center>';
							$tabla.=' <td class="cont" '.$txtEmergente.' style="';
							if($posicFondo == $key) $tabla.=' '.$fondo.'';
							$tabla.='cursor:auto" >';
						}
						else
						{
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
							if($posicFondo==$key) $tabla.=' style=\''.$fondo.'\'';
							$tabla.='>&nbsp;';
						}
					}

					if($arraTipos[$key]=='url')	// CREA LINK PARA LA URL DEL REGISTRO
					{
						if($datoIntegro)
						{
							$txtEmergente='';

							if($displayLinkUrl)
								$value='<a href=\''.$datoIntegro.'\' class="lk" target="_blank" title="click aqu&iacute; para abrir: '.$datoIntegro.'" >'.$value.'</a>';
							else
								$value='<s>'.substr($value,0,18).'</s>';
							$tabla.=' <td class="cont" '.$txtEmergente.' style="';
							if($posicFondo==$key) $tabla.=' '.$fondo.'';
							$tabla.='cursor:auto" >';
						}
						else
						{
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
							if($posicFondo==$key) $tabla.=' style=\''.$fondo.'\'';
							$tabla.='>&nbsp;';
						}
					}

					if($arraTipos[$key]=='opc')	// PARA PERMITIR RECIBIR CODIGO MUY PERSONALIZADO, INCLUSO BOTONES (SUS FUNCIONES HAN DE ESTAR EN LA CLASE ESPECIFICA)
					{
						if($datoIntegro)
						{
							$txtEmergente='';
							$tabla.=' <td class="cont" '.$txtEmergente.' style="';
							if($posicFondo==$key) $tabla.=' '.$fondo.'';
							$tabla.='cursor:auto" >';
						}
						else
						{
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
							if($posicFondo==$key) $tabla.=' style=\''.$fondo.'\'';
							$tabla.='>&nbsp;';
						}
					}

					if($arraTipos[$key]=='mai')	// CREA LINK PARA EMAIL
					{
						if($datoIntegro)
						{
							$txtEmergente='';
							$value='<a href=\'mailto:'.$datoIntegro.'\' class="lk" title="click aqu&iacute; para enviar un e-mail">'.$value.'</a>';
							$tabla.=' <td class="cont" '.$txtEmergente.' style="';
							if($posicFondo==$key) $tabla.=' '.$fondo.'';
							$tabla.='cursor:auto" >';
						}
						else
						{
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
							if($posicFondo==$key) $tabla.=' style=\''.$fondo.'\'';
							$tabla.='>&nbsp;';
						}
					}

					if($arraTipos[$key] == 'num')	// FORMATO NUMERICO
					{
						if(is_numeric($datoIntegro))
						{
							$value=number_format($datoIntegro,$decimales,',','.');
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
							if($posicFondo==$key) $tabla.=' '.$fondo.'';
							$tabla.=' align="right">';
						}
						else
						{
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
							if($posicFondo==$key) $tabla.=' style=\''.$fondo.'\'';
							$tabla.='>&nbsp;';
						}
					}

					if($arraTipos[$key]=='med')	// FORMATO MONEDA
					{
						if(is_numeric($datoIntegro))
						{
							$txtEmergente='';
							$value=number_format($datoIntegro,$decimales,',','.').' '.$this->unidadMedida;
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
							if($posicFondo==$key) $tabla.=' '.$fondo.'';
							$tabla.=' align="right">';
						}
						else
						{
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
							if($posicFondo==$key) $tabla.=' style=\''.$fondo.'\'';
							$tabla.='>&nbsp;';
						}
					}

					if($arraTipos[$key]=='mon')	// FORMATO MONEDA
					{
						if(is_numeric($datoIntegro))
						{
							$txtEmergente='';
							$value=number_format($datoIntegro,$decimales,',','.').' '.$this->moneda;
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
							if($posicFondo==$key) $tabla.=' '.$fondo.'';
							$tabla.=' align="right">';
						}
						else
						{
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
							if($posicFondo==$key) $tabla.=' style=\''.$fondo.'\'';
							$tabla.='>&nbsp;';
						}
					}

					if($arraTipos[$key]=='fec')	// DATO DE TIPO "FECHA"
					{
						if($datoIntegro)
						{
							$txtEmergente='';
							$value=date('d-m-Y',strtotime($datoIntegro));
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" align="center" ';
							if($posicFondo==$key) $tabla.=' '.$fondo.'';
							$tabla.='cursor:poniter">';
						}
						else
						{
							if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
							else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
							if($posicFondo==$key) $tabla.=' style=\''.$fondo.'\'';
							$tabla.='>&nbsp;';
						}
					}

					if($arraTipos[$key] == 'txt')
					{
						if($key==$campoNoPop) $tabla.=' <td class="cont" '.$txtEmergente.' ';
						else $tabla.=' <td class="cont" '.$txtEmergente.' onclick="'.$scriptEditar.$registro[0].')" ';
						if($posicFondo==$key) $tabla.=' style=\''.$fondo.'\'';
						$tabla.='>';
					}

					//*********************  E X C E P C I O N E S para visualizar columnas que han de tratarse en modo especial
					//*********************  FIN  E X C E P C I O N E S
//					if(!$value || strlen($value) == 0)
					if(!trim($value))
						$value='';
//						$value='&nbsp;';

					if($arraTipos[$key]!='rop')	// SE OMITE PINTAR rop
					  $tabla.=$value;

					$tabla.="</td>\n";
				}

				if($verBotonSustituir)
					$tabla.='<td class="cont" align="center" style="cursor:auto">
				<a href="#null" class="btn" onclick="this.blur();sustituirImagen('.$registro[0].',\''.$this->output_datos(str_replace("'",'`',strip_tags($registro[$txtRefEliminar]))).'\',\''.$registro[1].'\')" style="width:25px;color:'.$_SESSION['colorTextWarning'].';font-weight:bold;tetxt-align:center" title="click aqu&iacute; para sustituir la im&aacute;gem">SuS</a>
				</td>';

				if(!$ocultarEliminar)
					$tabla.='<td class="cont2" align="center" style="cursor:auto">
				<a id="x_'.$registro[0].'" href="#null" class="btn" onclick="this.blur();'.$functionDeleteRecord.''.$registro[0].',\''.$this->output_datos(str_replace("'",'`',strip_tags($registro[$txtRefEliminar]))).'\')" style="width:30px;color:'.$_SESSION['colorTextWarning'].';background-color:'.$_SESSION['colorFondoWarning'].';padding-left:35%;padding-right:35%;" title="click aqu&iacute; para eliminar este registro"><b>X</b></a>
				</td>';

				$tabla.="</tr>\n\n";
			}
			if(!$posRegistro) $posRegistro=1;	// AL HABER SOLO UN REGISTRO, SU INDICE ES EL CERO, Y EN ESTE CASO SE PONE A 1 "$posRegistro" PARA PODER VISUALIZARLO
		}
		else
		{
			if(count($_SESSION['filtros']))
				$tabla.='<tr><td colspan=100 style=font-size:18px;><center><br>NO SE HALLARON COINCIDENCIAS<br /><br />CON LOS REQUISITOS DE B&Uacute;SQUEDA ACTUALES,<br /><br />O NO HAY REGISTROS EN ESTA FICHA.</center><br><br></td></tr>';
			else
				$tabla.='<tr><td colspan=100 style=font-size:14px;height:130px;color:red><center>cargando...</center></td></tr>';
			$posRegistro=0;
		}
		$tabla.='</table>';
		$this->contFilas=$posRegistro;
		return $tabla;
	}

//----------------------------------------------------------------------------------------
// FILTRO PARA LIMITAR LAS OPCIONES DISPONIBLES DEL SELECT A LOS CONTENIDOS DEL CAMPO DE ORIGEN
//----------------------------------------------------------------------------------------
	function filtrarElementosSelect()
	{
		$query='SELECT a.'.$_REQUEST['PhpIdSelect'].', a.'.$_REQUEST['PhpCampoMostar'].' FROM '.$this->sql->prefixTbl.$_REQUEST['nombreTablaSelect'].' a, '.$this->sql->prefixTbl.$_REQUEST['nombreTablaFuenteDatos'].' b WHERE a.'.$_REQUEST['PhpIdSelect'].'=b.'.$_REQUEST['refIndiceId'].' AND b.'.$_REQUEST['nomreCampoBuscar'].' LIKE \'%'.trim($_REQUEST['textoEncontrar']).'%\' GROUP BY b.'.$_REQUEST['refIndiceId'].' ORDER BY a.'.$_REQUEST['PhpCampoMostar'];

		$res=$this->sql->query($query);

		if(!mysql_num_rows($res))	// NO SE HALLARON COINCIDENCIAS, SE FINALIZA EL SCRIPT SIN MODIFICAR EL SELECT ESPECIFICADO
		{
			$cadena='<script>parent.removeOptionSelected(\''.$_REQUEST['id_select'].'\');	// SE ELIMINAN TODAS LAS OPCIONES Y SE AGREGA UN TEXTO DE AVISO
			parent.appendOptionLast(0,\'Sin datos para "'.$_REQUEST['PhpCampoMostar'].'"\',\'-1\',\''.$_REQUEST['id_select'].'\');
			parent.mostrarMensaje("mesajes_formulario","No se encontraron coincidencias (en "+parent.document.getElementById(\''.$_REQUEST['spanCadenaEnunciadoId'].'\').innerHTML+")");
			</script>';
			echo $cadena;
			return;
		}

		$cadena='<script>parent.removeOptionSelected(\''.$_REQUEST['id_select'].'\');'; $cont=0;
		$colorResalte='red';$colTextoResalte='#fff';

		if($_REQUEST['colorResalte'])
			$colorResalte=$_REQUEST['colorResalte'];
		if($_REQUEST['colTextoResalte'])
			$colTextoResalte=$_REQUEST['colTextoResalte'];

		if($_REQUEST['textoEncontrar'])
		$cadena.='
		function restablecer()
		{
			parent.document.getElementById(\''.$_REQUEST['spanCadenaEnunciadoId'].'\').style.backgroundColor=colB;
			parent.document.getElementById(\''.$_REQUEST['spanCadenaEnunciadoId'].'\').style.color=colF;
		}
		colB=parent.document.getElementById(\''.$_REQUEST['spanCadenaEnunciadoId'].'\').style.backgroundColor;
		colF=parent.document.getElementById(\''.$_REQUEST['spanCadenaEnunciadoId'].'\').style.color;
		parent.document.getElementById(\''.$_REQUEST['spanCadenaEnunciadoId'].'\').style.backgroundColor=\''.$colorResalte.'\';
		parent.document.getElementById(\''.$_REQUEST['spanCadenaEnunciadoId'].'\').style.color=\''.$colTextoResalte.'\';
		parent.document.getElementById(\''.$_REQUEST['spanCadenaEnunciadoId'].'\').style.color=\''.$colTextoResalte.'\';
		';
		$key=0;

		while($arra=$this->sql->fila2($res))
		{
			$cadena.='parent.appendOptionLast(0,\''.$arra[$_REQUEST['PhpCampoMostar']].'\','.$arra['id'].',\''.$_REQUEST['id_select'].'\');';

			if($_SESSION['filtros'][$_REQUEST['id_select']]==$arra['id'])
				$cadena.='
numSub_capitulo=parent.document.forms[\'frmBuscar\'][\''.$_REQUEST['id_select'].'\'].length;
for(n=0;n<numSub_capitulo;n++)
if(parent.document.forms[\'frmBuscar\'][\''.$_REQUEST['id_select'].'\'].options[n].value=="'.$arra['id'].'")
	parent.document.forms[\'frmBuscar\'][\''.$_REQUEST['id_select'].'\'].options[n].selected=true;
			';
			$key++;
		}
		$key++;

		if($_REQUEST['textoEncontrar'])
			$cadena.='
		setTimeout(\'restablecer();\',2000);
		';

		$cadena.='</script>';
		echo $cadena;
		return;
	}

//----------------------------------------------------------------------------------------
// VERIFICA SI EL CAMPO ESTA DUPLICADO AL INSERTAR UN NUEVO REGISTRO ($nombreSelectFormulario=nombre de select para resetear options y poder repetir la operacion)
//----------------------------------------------------------------------------------------
	function verificarDuplicados($datoVerificar, $laTabla, $nombreCampoVerificar, $nombreSelectFormulario='')
	{
		if($this->id) return;	// SALE AL ESTAR EN MODO "MODIFICACION DATOS"
		if(!$nombreSelectFormulario)
		{
			echo '<span style=color:red><br /><br /><center><b><h3>FALTA INCLUIR EL NOMBRE DE LA SELECT QUE INVOCA ESTA FUNCION!</h3></b></center></span>';
			exit;
		}
		$query='SELECT '.$nombreCampoVerificar.' FROM '.prefixTbl.$laTabla.' WHERE '.$nombreCampoVerificar.' LIKE UPPER(\'%'.trim(strtoupper($datoVerificar)).'%\')';
//echo $query.'<hr />'.$salidaSql.'<pre>';print_r($this);print_r($_REQUEST);print_r($_SESSION);exit;
		$existe_duplicado=$this->sql->valor($query);
		if($existe_duplicado)
		{
			echo '
			<table height="100%" width="100%" class="borde">
			<tr><td align="center"><h2 style="color:'.$_SESSION['colorTextWarning'].';"><b>ERROR ('.$datoVerificar.')</b>, ya existe un registro para la posici&oacute;n que intentaba agregar,</h2><br /><br />no es posible efectuar una inserci&oacute;n como nuevo registro.<br /><br /><br /><br />Identificador del campo examinado: "<i>'.trim($nombreCampoVerificar).'</i>"<br /><br /><br /></td></tr>';

// SE ESTA INTENTANDO AÑADIR UN REGISTRO EXISTENTE DESDE UN FORMULARIO DISTINTO AL QUE CONTIENE LA TABLA "$this->nombreTabla"
			if($_REQUEST['nombreSelect'])
				echo '<tr><td align="center"><input onclick="parent.document.forms[0].'.$nombreSelectFormulario.'.options[0].selected=true;parent.document.getElementById(\'ifrApoyo\').style.display=\'none\';" style="width:100px;border:1px solid '.$_SESSION['colorOverListado'].';text-align:center;" value="CERRAR" />';
			else
				echo '<tr><td align="center"><a id="inpVolAnt" href="'.$_REQUEST['destino'].'" class="btn rounded" style="display:block;width:200px;margin-right:0px" title="Volver a la p&aacute;gina anterior">Volver</a><br /><br /></td></tr>';
//			echo '<tr><td align="center"><input type="button" id="inpVolAnt" onclick="history.go(-1)" style="width:100px;border:1px solid '.$_SESSION['colorOverListado'].';text-align:center;" value="VOLVER" /></td></tr>';

			echo '
			</table>
			<script>document.getElementById(\'inpVolAnt\').focus();</script>
			';
			exit;
		}
		return;
	}

//----------------------------------------------------------------------------------------
// Compara dos fechas
// Devuelve 0 (Iguales) , -1(fecha1<fecha2) y 1(fecha1>fehca2)
//----------------------------------------------------------------------------------------
	function dias_entre($fecha1,$fecha2)
	{
		list($year1, $mes1, $day1) = explode("-", $fecha1);
		list($year2, $mes2, $day2) = explode("-", $fecha2);

		$s1 = mktime(0, 0, 0, $mes1, $day1, $year1);
		$s2 = mktime(0, 0, 0, $mes2, $day2, $year2);
		return ceil(($s1-$s2)/86400);
	}

//----------------------------------------------------------------------------------------
// DEVUELVE EL NAVEGADOR CLIENTE UTILIZADO 	enviar:ObtenerNavegador($_SERVER['HTTP_USER_AGENT']);
//----------------------------------------------------------------------------------------
	function ObtenerNavegador($user_agent=0)
	{
		$browsers='mozilla msie gecko firefox konqueror safari netscape navigator opera mosaic lynx amaya omniweb';
		$browsers=@split(' ',$browsers);
		$nua=strToLower($_SERVER['HTTP_USER_AGENT']);
		$l=strlen($nua);
		for($i=0;$i<count($browsers);$i++)
		{
			$browser=$browsers[$i];
			$n=stristr($nua,$browser);
			if(strlen($n)>0)
			{
				$verNav = "";
				$navX = $browser;
				$j=strpos($nua, $navX)+$n+strlen($navX)+1;
				for(; $j<=$l; $j++)
				{
					$s=substr($nua,$j,1);
					if(is_numeric($verNav.$s))
					$verNav.= $s;
					else
					break;
				}
			}
		}
		if(!$navX) $navX=$_SERVER['HTTP_USER_AGENT'];
		$naveg=$navX.' '.$verNav;
		$this->versionNavegador=$verNav;
		$this->nameNavegador=$navX;
//		$this->getTabsJqueryData();	// 2013 added plug-in call
		return $naveg;
	}

//******************************************************
//	SUSTITUIR PATH RELATIVA POR ABSOLUTA PARA LAS IMAGENES DEL SITE (july-2012)
//******************************************************
	function pathRelativeToAbsolute($textoBuscar=null,$rutaSustituir=null)
	{
	  if(!$textoBuscar) return false;
	  if(!$rutaSustituir) return false;
	  $posTexto=stripos($textoBuscar,$rutaSustituir);

	  if($posTexto)	// existe la ruta a sustituir, se procede a cambiar por la ruta absoluta del site
	  {
		$logitudTexto=strlen($rutaSustituir);
		if(strstr($rutaSustituir,'src="/'))	// existe SRC, que tiene ruta de la imagen
		{
echo'<hr/>'.		  $textoAntes=substr($textoBuscar,0,$posTexto);
echo'<hr/>'.		  $textoDespues=substr($textoBuscar,strlen($textoAntes)+$logitudTexto);
		  preg_match_all('/src="(.+)"/', $textoBuscar, $textOut); //Notese los espacios
echo'<hr/>'.$textOut[1][0];exit;
		  $outAbsoluta=str_ireplace('%'.$textOut[1][0].'%','src="'.$this->rutaAbsolutaImagenes,$textoAntes.'%'.$textOut[1][0].'%'.$textoDespues);
		  return $outAbsoluta;
		}
	  }
	  return false;
	}

//******************************************************
//	ENCRIPTACION PRA PASSWORD DE HTACCESS (27.07.2012)
//******************************************************
	function crypt_apr_md5($password, $salt = null)
	{
	  if(empty($salt))
	  {
			$salt_string='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
			$salt='';

			for($i=0;$i < 8;$i++)
			{
				$salt.=$salt_string[rand(0,61)];
			}
	  }

	  $len=strlen($password);

	  $result=$password.'$apr1$'.$salt;
	  $bin=pack('H32',md5($password .$salt.$password));

	  for($i=$len; $i>0; $i-=16)
		{
			$result.=substr($bin,0,min(16,$i));
	  }

	  for($i=$len;$i>0;$i>>=1)
		{
			$result.=($i & 1) ? chr(0) : $password[0];
	  }

	  $bin=pack('H32',md5($result));

	  for($i=0; $i<1000; $i++) {
		$new=($i & 1) ? $password : $bin;

		if($i % 3)
		{
		  $new.=$salt;
		}

		if($i % 7)
		{
		  $new.=$password;
		}

		$new.=($i & 1) ? $bin : $password;
			$bin=pack('H32',md5($new));
	  }

	  for($i=0; $i<5; $i++)
		{
			$k=$i+6;
			$j=$i+12;

			if($j == 16)
			{
				$j=5;
			}

			$tmp=$bin[$i].$bin[$k].$bin[$j].$tmp;
	  }

	  $tmp=chr(0).chr(0).$bin[11].$tmp;
	  $tmp=strtr(strrev(substr(base64_encode($tmp),2)),'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/','./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz');
	  return '$apr1$'.$salt.'$'.$tmp;
	}

//----------------------------------------------------------------------------------------
// ENVIO DE E-MAIL GENERICO
//----------------------------------------------------------------------------------------
	function EnviarMailParametizable($asunto='TEST',$mensaje='TEST',$emailTo,$toNombre='',$from='')
	{
//		require_once('_inc/class.phpmailer.php');
//		$privateDataEmail=(count($this->dataEmail) ? $this->dataEmail : $emailDatos);

		$nombreRemitente=''; $this->attachedFiles=0;
//		$toOculto='mfrancescutto@gmail.com';
		$toOculto='mfrancescutto@gmail.com';

//		$this->mail = new phpmailer();
		$this->mail->IsSMTP();
		$this->mail->SMTPDebug = false;
		$this->mail->SMTPAuth = true;     // turn on SMTP authentication
		$this->mail->Mailer = 'smtp';
		$this->mail->Timeout=10;
		$elRemitente='non-replay@sumatealpellet.es';

//		$elRemitente='mailUsuario@dominio.com';//////$arra['remitente'];
			$this->mail->Username = 'non-replay@sumatealpellet.es';  // SMTP username
			$this->mail->Password = 'noreplay0000'; // SMTP password
//		$this->mail->Host='smtp.SMTP.XXX'; // default: localhost

		if($_SERVER['HTTP_HOST']=="192.168.1.31" ||$_SERVER['HTTP_HOST']=="127.0.0.1" || $_SERVER['HTTP_HOST']=="192.168.1.3" || $_SERVER['HTTP_HOST']=="localhost")
		{
			$elRemitente='montajesPublicados@mfrancescutto.com';
			$this->mail->Username = 'mfrancescutto@mfrancescutto.com';  // SMTP username
			$this->mail->Password = '77776666'; // SMTP password
			$this->mail->Host='hl98.dinaserver.com';
		}

		if($_SERVER['HTTP_HOST'] == 'www.mfrancescutto.com')
		{
			$elRemitente='montajesPublicados@mfrancescutto.com';
			$this->mail->Username = 'mfrancescutto@mfrancescutto.com';  // SMTP username
			$this->mail->Password = '77776666'; // SMTP password
			$this->mail->Host='localhost';
		}

//		if(count($privateDataEmail['fileAttach']))
//		{
//			foreach($privateDataEmail['fileAttach'] as $key => $value)
//			{
//				if($value)
//				{
//					$this->attachedFiles ++;
//					$this->mail->AddAttachment($this->rutaNews.$value,substr($value,9));
//				}
//			}
//		}

// START ****************************************SOLO EMAIL ADICIONAL PARA PRUEBAS ***************************************
//		$this->mail->AddAddress('mfrancescutto@gmail.com', 'pruebaSRDD');
// END ****************************************SOLO EMAIL ADICIONAL PARA PRUEBAS ***************************************

		$nombreRemitente=$nombreRemitente ? $nombreRemitente : 'Notificaciones Apropellets';
		$this->mail->From = $from;
		$this->mail->FromName = $nombreRemitente;

		$body = $mensaje;
		$text_body .= strip_tags($body);
		$this->mail->Body = $body;
		$this->mail->Subject = utf8_decode($asunto);
		$this->mail->AltBody = $text_body;
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//$this->mail->AddAddress('zzzz@XXXX.com',utf8_decode($toNombre));	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!11pruebas
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		$this->mail->AddAddress($emailTo,utf8_decode($toNombre));
		$this->mail->AddBCC($toOculto);
//echo '<pre>';print_r($this);die();

		$errorMail=$this->mail->Send();
		$this->mail->ClearAddresses();
		$this->mail->ClearAttachments();
		return $errorMail;//$errorMail;//$this->mail->ErrorInfo;
	}

	/*
	 * Return jQuery tabs data
	 *
	 * m.f. 2013
	 */
	function getTabsJqueryData()
	{
		$sqlFilter=$_REQUEST['refMain'] ? ' botj.id_tab_level='.(int)$_REQUEST['refMain'].' && ' : ' botj.id_tab_child='.(int)$_REQUEST['refSub'].' && ';

		$query='SELECT botj.id AS idTab, botj.*, bom.* FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'back_office_tabs_jquery AS botj LEFT JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'back_office_modules AS bom ON bom.id=botj.id_module WHERE botj.enabled &&'.$sqlFilter.' botj.user_id='.(int)$_SESSION['id_usuario'];
		$res=$this->sql->query($query);
		$numElementTabsJquery=0;
		while($arra=$this->sql->fila2($res))
		{
			$this->arrTabsjQuery[$arra['idTab']]['title']=$arra['tab_title'];
			$this->arrTabsjQuery[$arra['idTab']]['tab_header']=$arra['tab_header'];
			$this->arrTabsjQuery[$arra['idTab']]['content']=$arra['tab_content'];
			$this->arrTabsjQuery[$arra['idTab']]['plugInPath']=$arra['module_path'];
			$this->arrTabsjQuery[$arra['idTab']]['plugInModuleName']=$arra['module_name'];
			$numElementTabsJquery++;
		}
		return $numElementTabsJquery;
	}


	/*
	 * Return jQuery modules plug-ins
	 *
	 * m.f. 2013
	 */
//	function getModulePlugIn()
//	{
//		$sqlFilter=$_REQUEST['refMain'] ? ' id_tab_level='.(int)$_REQUEST['refMain'].' && ' : ' id_tab_child='.(int)$_REQUEST['refSub'].' && ';
//
//		$query='SELECT id,module_name FROM back_office_modules WHERE is_enabled=1 ORDER BY module_name';
//		$res=$this->sql->query($query);
//
//		while($arra=$this->sql->fila2($res))
//		{
//			$this->arrModulePlugIn[$arra['id']]['module_name']=  utf8_encode($arra['module_name']);
//			$this->arrModulePlugIn[$arra['id']]['module_path']=$arra['module_path'];
//		}
//		return;
//	}

	/*
	 * Set global back-office predefined access levels
	 *
	 * m.f. 2013
	 */
	function getAdminLevel()
	{
		$this->levelAccessMaxLevel=20;
		$this->levelAccessModules=array(
				'OperadorSite'=>5,
				'AdminSite'=>13,
				'SuperAdmin'=>14,
				'MegaAdmin'=>18,
				'Good'=>$this->levelAccessMaxLevel
		);
		return;
	}

// ********************************************************************************************************
//
// ********************************************************************************************************
  public function __destruct(){}
}
