<?php
/**************************************************************************/   
/* you must supply to this script 3 parameters :  title,width and data.   */   
/* - title is the title of the graph                                      */   
/* - width is the width of the generated graph                            */   
/* - data is used for the values to be displayed                          */   
/*     to split the data you have to use separators "^" and "^^"          */   
/*     -> item_title^value^^                                              */    
/* ---------------------------------------------------------------------- */   
/* call this script directly in the browser or in your html page with the */   
/* img tag.                                                               */   
/* example 1 :                                                            */   
/* graph.php3?title=foo&width=500&data=a^15^^b^20^^                       */   
/* example 2 :                                                            */   
/* <html>                                                                 */   
/*  ...                                                                   */   
/*  <img src="graph.php3?title=foo&width=500&data=a^15^^b^20^^">          */   
/*  ...                                                                   */   
/* </html>                                                                */   
/* ---------------------------------------------------------------------- */   
/* you can use as many data items as you want                             */   
/* you need the GD library in your php module                             */   
/* don't forget to encode you parameters (unicode)                        */   

$title = urldecode($_REQUEST['title']);  

$data = urldecode($_REQUEST['data']);  
$altura=$_REQUEST['altura'];
if(!$altura)
	$altura=20;

//start positions of graph  
$x = 40;  
$y = 30;  

//right margin  
$right_margin= 20;  

$bar_width = 10;  
$total = 0;  
$max = 0;  

$unit = (($width-$x)-$right_margin) / 100;  
$items= explode( "^^",$data);  

//calculate total  
while (list($key,$item) = each($items))  
{  
	if($item)  
	{  
		$pos = strpos($item, "^");  
		$value = substr($item,$pos+1,strlen($item));  
//		$total = $total + $value;
		$total=$_REQUEST['total'];
		$totalBan=$_REQUEST['totBanners'];
	}  
}  
reset($items);  

//calculate height of graph  
$height = sizeof($items) * ($bar_width + $altura);  

Header( "Content-type:  image/png");  
$im = imagecreate($width,$height);  
    
// Allocate colors  
$white=ImageColorAllocate($im,240,240,240);  
$yellow=ImageColorAllocate($im,0,0,0);  
$blue=ImageColorAllocate($im,218,233,235);  
$bar=ImageColorAllocate($im,90,154,163);  
$colFecha=ImageColorAllocate($im,90,154,163);
//background  
ImageFilledRectangle($im,0,0,$width,$height,$yellow);  

//title  
$title_x = (imagesx($im)-7.5*strlen($title))/2;  
ImageString($im,3,$title_x,4,$title,$white);  

//line
if($_REQUEST['linea'])	// solo si existe el parametro 'linea' se dibuja
	Imageline($im,$x,$y-25,$x,$height-15,$bar);

//draw data
$elem=0;
while(list($key,$item) = each($items))  
{  
	if ($item)  
	{  
		$pos = strpos($item, "^");  
		$item_title = substr($item,0,$pos);  
		$value = substr($item,$pos+1,strlen($item));
		$elTotal=$total;
		$divisor=$elTotal;

//		if($elem==1)
		{
			$elTotal=$totalBan;
			$divisor=100;
		}
		
			ImageString($im,3,$x-30,$y-2,intval(round(($value/$elTotal)*$divisor)). "%",$bar);
			$px = $x + ( intval(round(($value/$elTotal)*$divisor)) * $unit);  

/*
		if($elem==0)
		{
		}
		else
		{
			ImageString($im,3,$x-30,$y-2,intval(round($elTotal/$value)). "%",$bar);
			$px = $x + ( intval(round($elTotal/$value)) * $unit);  
		}
*/		
		//value right side rectangle
		
		//draw rectangle value
		ImageFilledRectangle($im,$x,$y-2,$px,$y+$bar_width,$bar);  
		
		//draw item title
		ImageString($im,2,$x+5,$y+9,$item_title,$white);  
		
		//draw empty rectangle  
		ImageRectangle($im,$px+1,$y-2,($x+(100*$unit)),$y+$bar_width,$bar);  
		
		//display numbers
		ImageString($im,2,($x+(100*$unit))-40,$y+12,$value. "/".$elTotal,$white);
	}  
	$y=$y+($bar_width+$altura);
	$elem++;
}  

//date
//ImageString($im,1,$width-109,15,date("d/m/Y h:iA"),$colFecha);  

// Display modified image  
ImagePng($im);  

// Release allocated ressources  
ImageDestroy($im);  

?>
