<?php
//@header('Content-Type: text/html; charset=utf-8');

class landingsSiteConfig extends Base
{
	//Datos propios de la clase
	public $id;
	public $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEM�?S DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS
	public $linkmenu;

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'landings_site_config';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->getAdminLevel();	// RETRIEVE CONSTANTS LEVELS USER
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->landing_identifier=$this->output_datos($a['landing_identifier']);
		$this->txt_title_site=utf8_encode($a['txt_title_site']);
		$this->is_enabled=$a['is_enabled'];
		$this->client_id=$a['client_id'];
		$this->script_body=$a['script_body'];
		$this->url_local=$a['url_local'];
		$this->landing_name=utf8_encode($a['landing_name']);
		$this->privacy_policy_file=($a['privacy_policy_file']);
		$this->landing_terms_file=($a['landing_terms_file']);
		$this->footer_cookies_css=$a['footer_cookies_css'];
		$this->root_local_includes_path=$a['root_local_includes_path'];
		$this->skip_check_phone_number=$a['skip_check_phone_number'];
		return;
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(c.id) + IFNULL(mlt.is_enabled,0) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS mlt LEFT JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'contenidos AS c ON c.id_niv_two=mlt.id WHERE mlt.id="'.(int)$this->id.'"';
		$existen_paginas=$this->sql->valor($query);
echo $query;die();
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function formulario_datos($nombreForm=0)
	{
		header('Content-Type: text/html; charset=utf-8');
		$dataPopline=str_replace("\r\n",'',($this->landing_terms_file));
		$dataPopline=utf8_encode($dataPopline);
		$dataPopline=htmlspecialchars_decode($dataPopline);
		$dataPopline1=str_replace("\r\n",'',($this->privacy_policy_file));
		$dataPopline1=utf8_encode($dataPopline1);
		$dataPopline1=htmlspecialchars_decode($dataPopline1);
?>
<tr>
	<td align="right" title="El cliente para esta landing">Cliente:</td>
	<td>
<?php
		$salida='';

 		$query='SELECT lc.id_landing, lc.prefix FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc WHERE lc.is_enabled ORDER BY lc.prefix';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id_landing'].'\'';
			if($this->client_id == $arra['id_landing']) $salida.=' selected="selected"';
			$salida.='>'.$arra['prefix'].'</option>';
		}

		echo '<select id="client_id" name="client_id" style=\'width:125px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
	<td align="right" class="tooltip" title="Texto identificador de la promoción <i>(landing)</i>">Identificador:</td>
	<td><input type='text' id='landing_identifier' name='landing_identifier' value='<?=$this->landing_identifier?>' style='width:120px'></td>
	<td class="tooltip" title="Titular del site" align="right">Título del site:</td>
	<td><input type='text' id='txt_title_site' name='txt_title_site' value='<?=$this->txt_title_site?>' style='width:99%' class=""></td>
	<td align="right" class="tooltip" title="El sitio es activo?<br />(contestando NO, si la promoción está configurada<br />y tiene todos los archivos para la gráfica de 2013,<br />funcionará de esta manera).">Habilitado:</td>
	<td width="80">
<?php
		echo '<select id="is_enabled" name="is_enabled" style="width:44px;"';
		echo '<option value=-1 ';
		if($this->is_enabled==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->is_enabled==0 && isset($this->is_enabled))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->is_enabled==1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>
</tr>
<tr>
	<td align="right" class="tooltip" title="Momentaneamente igual que el <i>IDENTIFICADOR</i>">Nombre:</td>
	<td><input type='text' id='landing_name' name='landing_name' value='<?=$this->landing_name?>' style='width:99%;'></td>
	<td align="right" class="tooltip" title="Archivo PHP contenedor para los elementos específicos de la landing.<br /><i>Ejemplo: <b>body01.php</b></i>.<br />Si no especificado, se utilizará <b>body_common.php</b>.">Body file:</td>
	<td><input type='text' id='script_body' name='script_body' value='<?=$this->script_body?>' style='width:99%'></td>
	<td align="right" class="tooltip" title="Url <i>local</i> utilizada en la landing.">Url local:</td>
	<td class="tooltip" title="campo deshabilitado (no se utiliza)">
		<input type='text' id='url_local' name='url_local' value='<?=$this->url_local?>' style='width:99%' class="useless">
	</td>
	<td align="right" class="tooltip" title="Archivo footer CSS.<br /><i>Ejemplo: <b>mi_cookies.css</b></i>.<br />Si no especificado, se utilizará <b>footer_cookies.css</b>.">Cookies CSS:</td>
	<td><input type='text' id='footer_cookies_css' name='footer_cookies_css' value='<?=$this->footer_cookies_css?>' style='width:99%'></td>
</tr>
<tr <?=$this->id ? 'valign="top"' : '' ?>>
	<td align="right">
		<a title="Edición <b>Wysiwyg</b> para el contenido de las <b>condiciones</b> de la landing.<br />También se puede indicar el <b>nombre de un archivo</b>, es importante<br />que la extensión sea un `.php`, ej.: <i>condiciones.php</i>." class='btn ajax editAjax tooltip' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?>&innerTextHeight=<?=$this->innerTextHeight?>&nombreCampo=data_landing_terms_file&customer_landing_id=<?=(int)$this->client_id?>">Condiciones:</a>
		<span class="<?=$this->id ? '' : 'hide'?>"><br /><a title="Editar directamente el código <b>HTML</b>" class='btn ajax directHTML tooltip' href="<?=basename($_SERVER['PHP_SELF'])?>?id=<?=$this->id?>&accion=directHTML&fieldName=data_landing_terms_file">Html code:</a></span>

	</td>
	<td>
		<div id="data_landing_terms_file" class='editorPopline' contenteditable='true' style="display:block;margin-top:1px;border:1px solid #999;<?=$this->id ? 'width:100%;max-width:240px;' : 'width:auto';?>;height:<?=$this->id ? '50px' : '16px';?>;overflow:auto;"><?=$dataPopline?></div>
		<textarea id='landing_terms_file' name='landing_terms_file' style='display:none;'><?=$this->data_landing_terms_file?></textarea>
	</td>

	<td align="right">
		<a title="Edición <b>Wysiwyg</b> para el contenido de las <b>`Política de privacidad`</b> de la landing.<br />También se puede indicar el <b>nombre de un archivo</b>, es importante<br />que la extensión sea un `.php`, ej.: <i>politicas.php</i><br />Si dejas vacío este campo, se utilizará automáticamente <b>privacy_policy.php</b>." class='btn ajax editAjax tooltip' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?>&innerTextHeight=<?=$this->innerTextHeight?>&nombreCampo=data_privacy_policy_file&customer_landing_id=<?=(int)$this->client_id?>">Política priv.:</a>
		<span class="<?=$this->id ? '' : 'hide'?>"><br /><a title="Editar directamente el código <b>HTML</b>" class='btn ajax directHTML tooltip' href="<?=basename($_SERVER['PHP_SELF'])?>?id=<?=$this->id?>&accion=directHTML&fieldName=data_privacy_policy_file">Html code:</a></span>
	</td>
	<td width="80">
		<div id="data_privacy_policy_file" class='editorPopline' contenteditable='true' style="display:block;margin-top:1px;border:1px solid #999;<?=$this->id ? 'width:100%;max-width:240px;' : 'width:auto';?>;height:<?=$this->id ? '50px' : '16px';?>;overflow:auto;"><?=$dataPopline1?></div>
		<textarea id='privacy_policy_file' name='privacy_policy_file' style='display:none;'><?=$this->data_privacy_policy_file?></textarea>
	</td>

	<td align="right" class="tooltip" title="Omitir el control del número de teléfono por CURL?<br />Dejando <i>SI</i>, se omitirá el control por CURL de la<br />existencia del número de telefono y será válido cualquiera.">Skip Tlf:</td>
	<td>
<?php
		echo '<select id="skip_check_phone_number" name="skip_check_phone_number" style="width:44px;"';
		echo '<option value=-1 ';
		if($this->skip_check_phone_number==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->skip_check_phone_number==0 && isset($this->skip_check_phone_number))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->skip_check_phone_number==1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>

		<input type='text' id='root_local_includes_path' name='root_local_includes_path' value='<?=$this->root_local_includes_path?>' style='width:auto;visibility:hidden;'>
	</td>
</tr>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--

$(document).ready(function(){
	//editor jQuery color box
	$(".editAjax").colorbox({
		opacity: 0.4,
		initialHeight:"10px",
		initialWidth:"10%",
		close:true,
		overlayClose:true,
		width:"<?=$this->id ? 94 : 64 ?>%",
		height:"100%",
		iframe:true,
		html:true,
		innerHeight:true
	});

	if("<?=$this->id?>")
	{
		var previewData = $("#btnSaveRecord").parent();
		$(previewData).prepend("<button id='btnPreview' class='btn tooltip' onclick='return false;' title='visualiza el contenido actualmente almacenado en la BBDD' >&nbsp;Visualizar contenido <b>en BBDD</b>&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;");
	}

	$("#btnPreview").click(function(){
		window.open("../?cr=<?=$this->id?>","test","");
	});
});

function revisa()
{
	if($('#client_id').val() < 0)
	{
		displayFormError("#client_id","Por favor, indique el cliente");
		return false;
	}
	if($.trim($('#landing_identifier').val()) == '')
	{
		displayFormError("#landing_identifier","Por favor, introduce un texto identificador para esta landing");
		$('#landing_identifier').val("");
		return;
	}
	if($.trim($('#txt_title_site').val()) == '')
	{
		displayFormError("#txt_title_site","Por favor, indicar el título del sitio");
		$('#txt_title_site').val("");
		return;
	}
	///document.f<?=$idunico?>.submit();
	popeditorSaveData("f<?=$idunico?>");
}

function resaltarCasilla(id,xx,zz)
{
	window.open("../?cr="+id,"test","");
}

-->
</script>
<?php
		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->landing_identifier=$datos['landing_identifier'];
		$this->txt_title_site=$datos['txt_title_site'];
		$this->is_enabled=$datos['is_enabled'];
		$this->client_id=$datos['client_id'];
		$this->script_body=$datos['script_body'];
		$this->url_local=$datos['url_local'];
		$this->landing_name=$datos['landing_name'];
//		$this->privacy_policy_file=($datos['privacy_policy_file']);
//		$this->landing_terms_file=($datos['landing_terms_file']);
		$this->privacy_policy_file=htmlspecialchars_decode($datos['privacy_policy_file']);
		$this->landing_terms_file=htmlspecialchars_decode($datos['landing_terms_file']);
		$this->footer_cookies_css=$datos['footer_cookies_css'];
		$this->root_local_includes_path=$datos['root_local_includes_path'];
		$this->skip_check_phone_number=$datos['skip_check_phone_number'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		$dato_privacy_policy_file=str_replace("€",'&euro;',$this->privacy_policy_file);
		$dato_privacy_policy_file=trim(utf8_decode($dato_privacy_policy_file));
		$dato_privacy_policy_file=utf8_encode($dato_privacy_policy_file);

		$dato_landing_terms_file=str_replace("€",'&euro;',$this->landing_terms_file);
		$dato_landing_terms_file=trim(utf8_decode($dato_landing_terms_file));
		$dato_landing_terms_file=utf8_encode($dato_landing_terms_file);

		$this->sql->query('SET NAMES utf8');
		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.'(id, landing_identifier,txt_title_site,is_enabled,client_id, script_body, url_local, landing_name, privacy_policy_file, landing_terms_file, footer_cookies_css, root_local_includes_path, skip_check_phone_number) VALUES ('.(int)$this->id.', \''.trim($this->landing_identifier).'\', \''.trim(utf8_decode($this->txt_title_site)).'\',\''.$this->is_enabled.'\', '.(int)$this->client_id.',\''.$this->script_body.'\', \''.$this->url_local.'\', \''.utf8_decode($this->landing_name).'\', \''.$dato_privacy_policy_file.'\', \''.$dato_landing_terms_file.'\', \''.$this->footer_cookies_css.'\', \''.$this->root_local_includes_path.'\', \''.(int)$this->skip_check_phone_number.'\')';
//echo $query;die();
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$idunico=rand(1,10000).time();
		//$query='SELECT lab.*,u.nivel_acceso FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 ';
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		//$querytotal='SELECT COUNT(lab.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 '.$filtroSql;
		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='landing_identifier'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;

		if($_SESSION['filtros']['visitantes_distintos_busca'])
		{
			$res2=$this->sql->query($querytotal);
			$num_res=mysql_num_rows($res2);
		}
		else
			$num_res=$this->sql->valor($querytotal);

		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $query;

//******************************************************


//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
var idVisualizado=-1;
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->


<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='878' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='12' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td title='Buscar por el <b>nombre</b> del cliente' width="60" align="right" class='tooltip'>Cliente:</td>
	<td>
<?php

		$query='SELECT lc.id_landing, lc.prefix FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc WHERE lc.is_enabled ORDER BY lc.prefix';
		$res2=$this->sql->query($query);

		$salida='<select id="'.$this->posicionSolapa.'client_id_buscN" name="'.$this->posicionSolapa.'client_id_buscN" style=\'width:108px;\'>';

		$salida.='<option value=-1 ';
		if($_SESSION['filtros'][$this->posicionSolapa.'client_id_buscN']==-1)
			$salida.='selected=\'selected\'';
		$salida.='>?</option>';

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id_landing'].'\'';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'client_id_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'client_id_buscN']==$arra['id_landing']) $salida.=' selected="selected"';
			$salida.='>'.$arra['prefix'].'</option>';
		}

		$salida.='</select>';
		echo $salida;
?>
	</td>

	<td width="48" align="right">Identificador:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>landing_identifier_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'landing_identifier_busca_u']?>' style='width:100px' /></td>

	<td width="68" align="right">T&iacute;tulo site:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>txt_title_site_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'txt_title_site_busca_u']?>' style='width:100px' /></td>
	<td title='' align="right">Activo:</td>
	<td title=''>
<?php
		$esActivo=array(0=>'NO',1=>'S&iacute;');
		echo '<select id="'.$this->posicionSolapa.'is_enabled_buscN" name="'.$this->posicionSolapa.'is_enabled_buscN" style=\'width:50px;\' title="Nivel de acceso del usuario al Back-office">';
		echo '<option value=-1 ';
		if($this->is_enabled==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		for($n=0;$n < 2;$n++)
		{
			echo '<option value='.$n.' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']==$n && isset($_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']))
				echo 'selected=\'selected\'';
			echo ' >'.$esActivo[$n].'</option>';
		}
		echo '</select>';
?>
	</td>

<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->


<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,$this->modalHeight,'',$this->modalWidth,$this->modalLeftPos);

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='Cliente';
		$arraTitulares[]='Identificador';
		$arraTitulares[]='T&iacute;tulo site';
		$arraTitulares[]='Script body';
		$arraTitulares[]='Skip Tlf';
		$arraTitulares[]='Activo';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='client_id';
		$arraCamposOrdenar[]='landing_identifier';
		$arraCamposOrdenar[]='txt_title_site';
		$arraCamposOrdenar[]='script_body';
		$arraCamposOrdenar[]='skip_check_phone_number';
		$arraCamposOrdenar[]='is_enabled';

		 // no se incluye el valor para el ID
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=50;
		$arraLongitudes[]=64;
		$arraLongitudes[]=29;
		$arraLongitudes[]=32;
		$arraLongitudes[]=29;
		$arraLongitudes[]=60;
		$arraLongitudes[]=60;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=25;	// id
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=90;
		$arraLongitudesTitulares[]=140;
		$arraLongitudesTitulares[]=120;
		$arraLongitudesTitulares[]=165;
		$arraLongitudesTitulares[]=230;
		$arraLongitudesTitulares[]=45;
		$arraLongitudesTitulares[]=38;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='rop';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;
		$conutRowsDisplayed=0;
		$selectLabelLevelOne=$_SESSION['forcedLangSite'] ? 'mlo.landing_identifier AS nameMenu' : 'CONCAT(mlo.landing_identifier," (",i.prefijo,") AS nameMenu")';

		while($a=$this->sql->fila2($res))
		{
		  $rop=null;
			//	SOLO SE PERMITEN VER LOS ELEMENTOS PROPIOS Y LOS GLOBALES (EXCEPTO PARA GOOD)
		  if($_SESSION['id_usuario'] != $a['id_usuario'] && $a['id_usuario'] != 0 && $this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])
				$rop=1; // fila deshabilitada

			$xc=0; $estaActivo='<center style=color:orange>no</center>';
			$query='SELECT lc.prefix FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc WHERE lc.id_landing='.(int)$a['client_id'];
			$reCustomer=$this->sql->valor($query);

			if($a['skip_check_phone_number']==-1) $SkipPhoneCheck='?';
			if(!$a['skip_check_phone_number']) $SkipPhoneCheck='<span style=color:blue>NO</span>';
			if($a['skip_check_phone_number']==1) $SkipPhoneCheck='SI';
			if($a['is_enabled']==-1) $esActivo='?';
			if(!$a['is_enabled']) $esActivo='<span style=color:red>NO</span>';
			if($a['is_enabled']==1) $esActivo='SI';

			if($a['registrado']==1) $esRegistrado='<span style=color:red>SI</span>';
			if($a['registrado']<0) $esRegistrado='<span style=color:orange>?</span>';
			$query='SELECT CONCAT(nombre," ",apellidos) AS theName FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE id="'.$a['id_usuario'].'"';
//			$elIdioma=utf8_encode($this->sql->valor($query));
			$elIdioma=$elIdioma?$elIdioma:'<span style=color:red;><b>Todos</b></span>';

			if($a['is_enabled']) $estaActivo='<center style=color:lightgreen><b>SI</b></center>';

			$tmpArr[0]=$a['id'];
			array_push($tmpArr,$reCustomer);
			array_push($tmpArr,$a['landing_identifier']);
			array_push($tmpArr,utf8_encode($a['txt_title_site']));
			array_push($tmpArr,$a['script_body']);
			array_push($tmpArr,$SkipPhoneCheck);
			array_push($tmpArr,$esActivo);
		  array_push($tmpArr,$rop);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
			$conutRowsDisplayed++;
		}

		$idRefPops=0;
		$txtRefEliminar=3;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
