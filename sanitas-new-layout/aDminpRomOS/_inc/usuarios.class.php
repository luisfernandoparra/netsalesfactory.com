<?php
/*
'OperadorSite'=>5,
'AdminSite'=>13,
'SuperAdmin'=>14,
'MegaAdmin'=>18,
'Good'=>20
*/

class Usuarios extends Base
{
	//Datos propios de la clase
	var $id;
	var $id_pais;
	var $nombre;
	var $usuario;
	var $password;
	var $apellidos;

//******************************************************
//	INICIALIZACION DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'usuarios';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->getAdminLevel();
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->nombre=$this->output_datos(utf8_encode($a['nombre']));
		$this->usuario=$this->output_datos($a['usuario']);
		$this->password=$this->output_datos($a['password']);
		$this->apellidos=$this->output_datos($a['apellidos']);
		$this->activo=$a['activo'];
		$this->nivel_acceso=$a['nivel_acceso'];
		$this->e_mail=$a['e_mail'];
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		if($this->password)
		{
			$this->password=md5($this->password);
			$laPass=",'".$this->password."'";
			$campoPass=', password';
			$campoPassUpdate=',password="'.$this->password.'"';
		}

		if(!$_REQUEST['id'])
			$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id,nombre,usuario,apellidos,nivel_acceso,activo,e_mail'.$campoPass.") VALUES ('".$this->id."', '".utf8_encode($this->nombre)."', '".$this->usuario."', '".utf8_decode($this->apellidos)."','".$this->nivel_acceso."','".$this->activo."','".$this->e_mail."'".$laPass.")";
		else
			$query='UPDATE '.$this->sql->db.'.'.$this->nombreTabla.' SET nombre="'.utf8_decode($this->nombre).'",usuario="'.$this->usuario.'",apellidos="'.utf8_decode($this->apellidos).'",nivel_acceso="'.$this->nivel_acceso.'",activo="'.$this->activo.'",e_mail="'.$this->e_mail.'" '.$campoPassUpdate.' WHERE id='.$_POST['id'];
//echo $query;echo '<pre>'; print_r($_REQUEST);exit;
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos()
	{
?>
<tr>
	<td align="right">Nombre:</td>
	<td><input type='text' id='nombre' name='nombre' value='<?=$this->nombre?>' style='width:100px'></td>
	<td align="right">Apellidos:</td>
	<td><input type='text' id='apellidos' name='apellidos' value='<?=$this->apellidos?>' style='width:120px'></td>
	<td align="right">usuario:</td>
	<td><input type='text' id='usuario' name='usuario' value='<?=$this->usuario?>' style='width:70px'></td>
	<td align="right">password:</td>
	<td><input type='password' id='password' name='password' value='' style='width:60px'></td>
	<td title='' align="right">Activo:</td>
	<td title=''>
<?php
		$esActivo=array(0=>'NO',1=>'S&Iacute;');
		echo '<select id="activo" name="activo" style=\'width:50px;\'>';
		echo '<option value=-1 ';
		if($this->activo==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->activo==$n)
				echo 'selected=\'selected\'';
			echo ' >'.$esActivo[$n].'</option>';
		}
		echo '</select>';
?>
	</td>
</tr>
<tr>
	<td title='Nivel de acceso del usuario al Back-office' align="right">Nivel de acceso:</td>
	<td title='Nivel de acceso del usuario al Back-office'>
<?php
		echo '<select name="nivel_acceso" style=\'width:50px;\'>';
		echo '<option value=-1 ';
		if($this->nivel_acceso == -1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=0;$n < $this->levelAccessMaxLevel+1;$n++)
		{
			if($n > $_SESSION['usuarioNivel']) break;	// SE PERMITE MODIFICAR DE NIVEL HASTA EL M�XIMO PERMITIDO AL USUARIO ACTUAL

			if($_SESSION['usuarioNivel'] < 11 && $n > 10) continue;

			echo '<option value='.$n.' title="'.$n.'"';
			if($this->nivel_acceso==$n)
				echo 'selected=\'selected\'';
			echo ' >'.$n.'</option>';
		}
		echo '</select>';
?>
	</td>
	<td align="right">E-mail:</td>
	<td colspan="5"><input type='text' id='e_mail' name='e_mail' value='<?=$this->e_mail?>' style='width:99%'></td>
</tr>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
	if(document.f<?=$idunico?>.nombre.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.nombre);
		document.f<?=$idunico?>.nombre.focus();
		alert('Por favor, escriba el nombre del usuario');
		restauraCampo(document.f<?=$idunico?>.nombre);
		return;
	}
	if(document.f<?=$idunico?>.apellidos.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.apellidos);
		alert('Por favor, escriba el apellido');
		document.f<?=$idunico?>.apellidos.focus();
		restauraCampo(document.f<?=$idunico?>.apellidos);
		return;
	}
	if(document.f<?=$idunico?>.usuario.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.usuario);
		document.f<?=$idunico?>.usuario.focus();
		alert('Por favor, escriba el usuario de acceso');
		restauraCampo(document.f<?=$idunico?>.usuario);
		return;
	}

	if(document.f<?=$idunico?>.password.value && document.f<?=$idunico?>.password.value.length<4)
	{
		resaltarCampo(document.f<?=$idunico?>.password);
		document.f<?=$idunico?>.password.focus();
		alert('Por favor, escriba password con almenos 4 caracteres');
		restauraCampo(document.f<?=$idunico?>.password);
		return;
	}
	if(document.f<?=$idunico?>.activo.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.activo);
		document.f<?=$idunico?>.activo.focus();
		alert('Por favor, indique si est� activo');
		restauraCampo(document.f<?=$idunico?>.activo);
		return;
	}

	datoMail=trim(document.f<?=$idunico?>.e_mail.value);
	if(datoMail.length>0)
		if(!verificarMail(datoMail,document.f<?=$idunico?>.e_mail))
		{
			document.f<?=$idunico?>.e_mail.focus();
			restauraCampo(document.f<?=$idunico?>.e_mail);
			return;
		}
	document.f<?=$idunico?>.submit();
}

function killTabs()	// ELIMINAR EL PERFIL DEL USUARIOO SELECCIONADO
{
	//alert("<?=$this->id?>");return;
	if(confirm("Realmente desea BORRAR PERMANENTEMENTE todos los modulos del usuario "+$("#nombre").val()+" "+$("#apellidos").val()))
	{
	  $.ajax({
			url:"<?=basename($_SERVER['PHP_SELF'])?>",
			method:"post",
			dataType:"json",
			cache:false,
			data:{
				idUser:"<?=$this->id?>",
				accion:94
			},
			success:function(response)
			{
				$("#btnKillTabs").html("&nbsp;&nbsp;<b>PERFIL ELIMINADO!</b>&nbsp;");
				$("#btnKillTabs").css("color","<?=$_SESSION['colorTextHighli']?>");
				$("#btnKillTabs").css("border","2px solid <?=$_SESSION['colorTextHighli']?>");
			},
			error:function(response){
				$("#btnKillTabs").html("<b>Operaci&oacute;n cancelada</b>, no se ha podido proceder con el borrado");
				$("#btnKillTabs").css("color","<?=$_SESSION['colorTextWarning']?>");
			}
	  })
	}

	return false;
}

$(document).ready(function(){
//	$(".ktabs[title]").qtip({
//		position:{at:"left",adjust:{y:-64}}
//	})
	$(".ktabs").css("background-color","<?=$_SESSION['colorTextWarning']?>");
	$(".ktabs").css("color","<?=$_SESSION['colorFondoBotones']?>");
//style="color:#fff;background-color:'.$_SESSION['colorTextWarning'].';"
})

-->
</script>
<?php
		if($this->id && $_SESSION['usuarioNivel']>14)
		{
			$this->extraButton='<span id="btnKillTabs" style="margin-right:10px;padding:1px;"><a href="#null" style="padding-left:10px;padding-right:10px;border:1px solid '.$_SESSION['colorTableBorde'].'" class="btn rounded ktabs" onclick="killTabs()" title="Se borran todos los m&oacute;dulos del actual usuario; al acceder &eacute;ste nuevamente, se volver&aacute;n a generar autom&aacute;ticamente">Eliminar el perfil del usuario</a></span>';
		}

		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->nombre=$datos['nombre'];
		$this->usuario=$datos['usuario'];
		$this->apellidos=$datos['apellidos'];
		$this->password=$datos['password'];
		$this->nivel_acceso=$datos['nivel_acceso'];
		$this->activo=$datos['activo'];
		$this->e_mail=$datos['e_mail'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='DELETE FROM '.$this->nombreTabla." WHERE id='".$this->id."'";
		$this->sql->query($query);
		return;
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$idunico=rand(1,10000).time();
		$filtroPermisos=' AND nivel_acceso<='.$_SESSION['usuarioNivel'];

		if(count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b&uacute;squeda:</span> ";

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID
		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql.$filtroPermisos;

		$ordenacion='nombre'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' DESC';

		$query.=$filtroSql.$filtroPermisos.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $query;
//******************************************************


//******************************************************
//Formulario para efectuar b�squedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>
<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='780' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td>Nombre:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>nombre_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'nombre_busca_u']?>' style='width:110px'></td>
	<td>Apellidos:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>apellidos_busca' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'apellidos_busca']?>' style='width:110px'></td>
	<td>usuario:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>usuario_busca' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'usuario_busca']?>' style='width:70px'></td>
	<td title='' align="right">Activo:</td>
	<td title=''>
<?php
		$esActivo=array(0=>'NO',1=>'S�');
		echo '<select id="'.$this->posicionSolapa.'activo_buscN" name="'.$this->posicionSolapa.'activo_buscN" style=\'width:50px;\' title="Nivel de acceso del usuario al Back-office">';
		echo '<option value=-1 ';
		if($this->activo==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'activo_buscN']==$n && isset($_SESSION['filtros'][$this->posicionSolapa.'activo_buscN']))
				echo 'selected=\'selected\'';
			echo ' >'.$esActivo[$n].'</option>';
		}
		echo '</select>';
?>
	</td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- *******************  fin formulario busquedas  *************** !-->

<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'32%','','75%','14%');

		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('Nombre','Apellidos','Nombre Usuario','E-mail','Activo','Nivel');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id','nombre','apellidos','usuario','e_mail','activo','nivel_acceso');
		$xc=0; $arraLongitudes=array($xc++=>40,$xc++=>40,$xc++=>29,$xc++=>40,$xc++=>60,$xc++=>60); // no se incluye el valor para el ID
		$xc=0; $arraLongitudesTitulares=array($xc++=>25,$xc++=>110,$xc++=>130,$xc++=>100,$xc++=>250,$xc++=>50,$xc++=>50);
		$xc=0; $arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'txt',$xc++=>'txt',$xc++=>'mai',$xc++=>'txt',$xc++=>'num');

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$xc=0;
			// PARA EL NIVEL AdminSite O INFERIORES, SE PERMITE EDITAR UNICAMENTE EL PROPIO USUARIO
			if($_SESSION['usuarioNivel'] <= $this->levelAccessModules['AdminSite'] && ($_SESSION['id_usuario'] !=$a['id']))
				continue;
			// SE OMITEN LOS ELEMENTO CON NIVEL SUPERIOR RESPECTO AL USUARIO ACTIVO

			if($a['activo']==0) $estaActivo='<center style=color:'.$_SESSION['colorTextLoose'].'>no</center>';
			if($a['activo']) $estaActivo='<center style=color:'.$_SESSION['colorTextHighli'].'><b>SI</b></center>';
			if($a['activo']<0) $estaActivo='<center style=color:'.$_SESSION['colorTextWarning'].'>?</center>';

			if($_SESSION['usuarioNivel'] > 10)
				$arraDatos[]=array($xc++=>$a['id'],$xc++=>utf8_encode($a['nombre']),$xc++=>$a['apellidos'],$xc++=>$a['usuario'],$xc++=>nl2br($a['e_mail']),$xc++=>$estaActivo,$xc++=>$a['nivel_acceso']);
			else
			{
				if($a['nivel_acceso'] > 10)
				{
					$estaActivo='<center>--</center>';
					$a['id']='-99';
					$a['nombre']='n.d.';
					$a['usuario']='n.d.';
					$a['apellidos']='n.d.';
					$a['e_mail']='n.d.';
				}
				$arraDatos[]=array($xc++=>$a['id'],$xc++=>utf8_encode($a['nombre']),$xc++=>$a['apellidos'],$xc++=>$a['usuario'],$xc++=>nl2br($a['e_mail']),$xc++=>$estaActivo);
			}
		}

		$idRefPops=0;
		$txtRefEliminar=2;
		$posicFondo=0;	// NUMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo,0,0,0);
		/*******************  FIN DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
