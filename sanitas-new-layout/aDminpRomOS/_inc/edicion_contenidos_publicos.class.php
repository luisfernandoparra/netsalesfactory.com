<?php
class edicion_contenidos_publicos extends Base
{
	//Datos propios de la clase
	var $id;
	var $nombre;
	var $nombreTabla;
	var $tamanioMaximoImagenes;
	var $path_imagenes_paginas;
	var $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'contenidos';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las búsquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset']==1) unset($_SESSION['filtros']);
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$this->carga_datos();
	}

//******************************************************
//	OBTENER ELEMENTOS DE LOS DESPLEGABLES
//******************************************************
	function seleccionarSubPagina()
	{
		$elementoActualizar='';
		switch($_REQUEST['ref_desplegable'])
		{
			case 'id_niv_one':
				echo'<script>parent.document.getElementById("id_niv_two").options.length=1;</script>';
				$query='SELECT * FROM mnu_level_two WHERE id_level_one='.$_REQUEST['id_niv_one'].' ORDER BY etiqueta';
				$elementoActualizar='';
				$nombreSelectActualiza='id_niv_two';
				break;
		}

		$salida='<script language="JavaScript" type="text/javascript">
		<!--
		';

		$res=$this->sql->query($query);

		if(!mysql_num_rows($res))
		{
			$salida.='parent.removeOptionSelected("'.$nombreSelectActualiza.'");';
			if($nombreSelectActualiza=='id_niv_two')
				$salida.='parent.appendOptionLast("0","--","'.$nombreSelectActualiza.'");';
			else
				$salida.='parent.appendOptionLast("0","--","'.$nombreSelectActualiza.'");';
		}
		else
		{
			$salida.='parent.removeOptionSelected("'.$nombreSelectActualiza.'");';
			$salida.='parent.appendOptionLast("-1","Seleccionar","'.$nombreSelectActualiza.'");';
			$cont=0;

			while($arra=$this->sql->fila2($res))
			{
				if($nombreSelectActualiza=='id_niv_two')
				{
					$salida.='parent.appendOptionLast('.$arra['id'].',"'.strip_tags($arra['etiqueta']).'","'.$nombreSelectActualiza.'");';
					$salida.='parent.document.getElementById(\'id_niv_two\').focus();';
				}
				$cont++;
			}
		}

		$salida.='
		-->
		</script>';
		echo $salida;
		return;
	}
}
?>