<?php

class img2php{
    /**
     * Constructor
     * @access protected
     */
    function img2php(){

    }

    /**
     *
     * @access private
     * @var string
     **/
    var $_imgdir = '';

    /**
     *
     * @access public
     * @return string
     **/
    function Getimgdir(){
        return $this->_imgdir;
    }

    /**
     *
     * @access public
     * @return void
     **/
    function Setimgdir($newValue){
        $this->_imgdir = $newValue;
    }

    /**
     *
     * @access private
     * @var string
     **/
    var $_linelength = 60;

    /**
     *
     * @access public
     * @return string
     **/
    function Getlinelength(){
        return $this->_linelength;
    }

    /**
     *
     * @access public
     * @return void
     **/
    function Setlinelength($newValue){
        $this->_linelength = $newValue;
    }

    /**
     * img2php::generate()    Read directory and Generate the php class
     *
     * @param string $_imgdir        Images directory to use
     * @param string $_classname    Class name you want to create (saved in the same dir)
     * @return
     **/
    function generate($_imgdir = "",$_classname = ""){
        if ($_imgdir!="") {
            $this->Setimgdir($_imgdir);
        }
        $d = dir($this->Getimgdir());

        while(false !== ($entry = $d->read())){
            if($entry!="." && $entry!='..' && preg_match("/(gif|jpg|jpeg|png)$/",$entry)) {
                $fd=fopen($this->Getimgdir().'/'.$entry, 'rb');
                $size=filesize($this->Getimgdir().'/'.$entry);
                $ext=substr("$entry",strrpos($entry,'.')+1);
                $content=fread($fd,$size);
                fclose($fd);
                $base64img=base64_encode($content);
                $imgcode=chunk_split($base64img,$this->Getlinelength(),"'.
'");

                $fname=str_replace('.','_',$entry);
                $fname=str_replace('-','_',$fname);
                $xcase.="
            case \"$fname\":
                gonximage::$fname();
            break;
";
                $xfunx.="    function $fname()
    {
        header(\"Content-type:image/$ext\");
        header(\"Content-length:$size\");
        echo base64_decode(
'$imgcode');
    }\n\n";
            }
        }
        $d->close();

        $data = "<?php

/**
* gonximage class : Generated based on directory (".$this->Getimgdir().")
*
**/
class gonximage{
    /**
     * Constructor
     * @access protected
     */
    function gonximage(){

    }

    /**
     * Return image based on it name
     * @access public
     * @return void
     **/
    function getimage(\$img){
        switch(\$img){
$xcase
        } // switch
    }
$xfunx
}
?>";

        if ($_classname!="") {
            $fp = fopen($_classname.".class.php", 'a');
            fwrite($fp, $data);
            fclose($fp);
        }
        highlight_string($data);
    }

}

$t = new img2php;
$t->generate("images/");

?>