<?php
class logsIpVotations extends Base
{
	//Datos propios de la clase
	var $id_participant;
	var $accion;
	var $nombreTabla;
	var $posicionSolapa;	// POSICI�N DE LA VENTANA RESPECTO A LAS DEM�S DE SU MISMA SECCI�N E IDENTIFICADOR PARA LOS CAMPOS DE LAS B�SQUEDAS

//******************************************************
//	INICIALIZACI�N DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'participant_ip_control';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las b�squedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id_participant=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id_participant="'.(int)$this->id_participant.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->accion=$a['accion'];
		$this->fecha=$a['fecha'];
		$this->id_participant=$a['id_participant'];
		return;
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(DISTINCT(id)) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id = "'.$this->id.'" && status != 3';
//echo $query;exit;
//		$existen_paginas=$this->sql->valor($query);
//echo $existen_paginas.'<pre>';print_r($_REQUEST);die();

		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id_participant='.(int)$this->id_participant;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id_participant=$datos['id_participant'];
		$this->accion=$datos['accion'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens)
	{
		$mens.='</b>'; $tmpCheckIp=null;
		$sqlFilterExceptionCount='*';
		$sqlFilterExceptionList='';
//echo '<pre>';print_r($_REQUEST);
//echo (isset($_REQUEST[$this->posicionSolapa.'ip_single_busca']) && $_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca']);
//echo '</pre>';
		if($_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca'] == 'on')
//		if(isset($_REQUEST[$this->posicionSolapa.'ip_single_busca']) && $_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca'])
		{
			$tmpCheckIp=$_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca'];
			unset($_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca']);
			$sqlFilterExceptionCount=' DISTINCT(ip_usuario)';
				$sqlFilterExceptionList=' GROUP BY (ip_usuario)';
		}

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		$querytotal='SELECT COUNT('.$sqlFilterExceptionCount.') FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='ip_control'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.$sqlFilterExceptionList.'
			ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;

		if($_SESSION['filtros']['visitantes_distintos_busca'])
		{
			$res2=$this->sql->query($querytotal);
			$num_res=mysql_num_rows($res2);
		}
		else
			$num_res=$this->sql->valor($querytotal);
//echo $query;die();
		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';

		if($tmpCheckIp)
			$_SESSION['filtros'][$this->posicionSolapa.'ip_single_busca']=$tmpCheckIp;
//echo $query;
//******************************************************
//Formulario para efectuar b�squedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];

// START NUMERO VISITANTES DISTINTOS **********
	$query='SELECT COUNT(id) AS cuenta FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql.' GROUP BY `ip_usuario` ';
//	$res3=$this->sql->query($query);
	$numVisitantesDistintos=0;

	while($a=$this->sql->fila2($res3))
		$numVisitantesDistintos++;

	$txtVisit=$numVisitantesDistintos==1?'visitante &uacute;nicamnete':'visitantes distintos';
	$losVisitantes='&nbsp;&nbsp;(<b>'.number_format($numVisitantesDistintos,0,',','.').'</b> '.$txtVisit.')';
// END NUMERO VISITANTES DISTINTOS **********
?>
<form action='<?=$destinobusca?>.php' method='post' name='formListados'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACI�N DE LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='500' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='5'><?=$textoTitularBuscador?><span id="spnVisitas" style="color:#999"><?=$losVisitantes?></span></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td align="right">Por Ip:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>ip_control_busca' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'ip_control_busca']?>'  style='width:70px' /></td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>

<script language="JavaScript" type="text/javascript">
<!--
function modifica(){return;}
-->
</script>
<!-- *******************  fin formulario b�squedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'75%','','80%','10%',$this->editarFila);

		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('IP visitante','Participante votado');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('ip_control','id_participant');
		$xc=0; $arraLongitudes=array($xc++=>60,$xc++=>130); // no se incluye el valor para el ID
		$xc=0; $arraLongitudesTitulares=array($xc++=>45,$xc++=>150,$xc++=>320);
		$xc=0; $arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'txt');
		$this->verIdRegistros=0;
		$verEmergentesaccionsFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			if(!$a['id_participant']) $usuario='visitante';

			$queryPersonas='SELECT partecipant_name FROM '.$this->sql->db.'.participants_movies WHERE id='.$a['id_participant'];
			$elUsuario=$this->sql->valor($queryPersonas);
			$usuario=$elUsuario?$elUsuario:'<b style=color:#999>-</b>';
			$xc=0;
			$laAccion=$a['accion'];
			if(!$laAccion) $laAccion='<span style=color:#999>consultar</span>';
			$ora=date('H:i:s',strtotime($a['fecha']));

			$arraDatos[]=array($xc++=>$a['id_participant'],$xc++=>$a['ip_control'],$xc++=>$usuario);
		}

		$idRefPops=0; $txtRefEliminar=2; $ocultarEliminar=1;
		$posicFondo=0;	// N�MERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo,$this->ocultarEliminar);
		unset($laAccion);
		/*******************  FIN DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>