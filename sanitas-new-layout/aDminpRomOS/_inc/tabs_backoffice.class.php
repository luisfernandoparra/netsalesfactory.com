<?php
/*
'OperadorSite'=>5,
'AdminSite'=>13,
'SuperAdmin'=>14,
'MegaAdmin'=>18,
'Good'=>20
*/
class TabsBackoffice extends Base
{
	//Datos propios de la clase
	public $id;
	public $posicionSolapa;	// POSICION DE LA VENTANA RESPECTO A LAS DEMAS DE SU MISMA SECCION E IDENTIFICADOR PARA LOS CAMPOS DE LAS BUSQUEDAS
	public $esTab_level;
	public $resTypes=array();
//	public $levelAccessModules;

//******************************************************
//	INICIALIZACION DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'back_office_tabs';
		$this->posicionSolapa=$posicionSolapa;
		$this->esTab_level=array(1=>'Root level (1)',2=>'Sub-tab (2)');
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'back_office_tabs_types ORDER BY descripcion';
		$this->resTypes=$this->sql->query($query);
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->getAdminLevel();
		return;
	}

//******************************************************
//	SE CARLA EL REGISTRO CON EL ID SELECCIONADO
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->id_user=$a['id_user'];
		$this->tab_level=$a['tab_level'];
		$this->tab_order=$a['tab_order'];
		$this->tab_title=$a['tab_title'];
		$this->tab_type=$a['tab_type'];
		$this->tab_child=$a['tab_child'];
		$this->tab_content=$a['tab_content'];
		$this->tab_access_level=$a['tab_access_level'];
		return;
	}

//******************************************************
//	SUB-TABS
//******************************************************
	function tabs_level_two($id_sub_menu=0)
	{
		$query='SELECT bot.* FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS bot  WHERE bot.tab_level=2 && bot.tab_child='.$id_sub_menu.' && bot.tab_access_level<='.($_SESSION['usuarioNivel']?$_SESSION['usuarioNivel']:0).' && id_user="'.$_SESSION['id_usuario'].'" ORDER BY bot.tab_order';

		$res=$this->sql->query($query);
		while($arra=$this->sql->fila2($res))
		{
			$this->elemMenuTwo[$id_sub_menu]['tabId'][]=$arra['id'];
			$this->elemMenuTwo[$id_sub_menu]['tabTit'][$arra['id']]=($arra['tab_title']);
			$this->elemMenuTwo[$id_sub_menu]['tabCont'][$arra['id']]=$arra['tab_content'];
			$this->elemMenuTwo[$id_sub_menu]['tabTip'][$arra['id']]=$arra['tab_type'];
			$this->elemMenuTwo[$id_sub_menu]['levelAccess'][$arra['id']]=$arra['tab_access_level'];
		}

		return;
	}

//******************************************************
//	TABS DEL ROOT LEVEL, QUE EL MENU PRINCIPAL
//******************************************************
	function tabs_root_level()
	{
		$query='SELECT bot.*,ott.id AS tipoId, ott.descripcion FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS bot INNER JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'back_office_tabs_types AS ott ON ott.id=bot.tab_type WHERE bot.tab_level=1 && !bot.tab_child && bot.tab_access_level<='.($_SESSION['usuarioNivel']?$_SESSION['usuarioNivel']:0).' AND id_user="'.$_SESSION['id_usuario'].'" ORDER BY bot.tab_order';
		$res=$this->sql->query($query);

		while($arra=$this->sql->fila2($res))
		{
			$this->elemMenuBase['tabId'][]=$arra['id'];
			$this->elemMenuBase['tabTit'][$arra['id']]=utf8_encode($arra['tab_title']);
			$this->elemMenuBase['tab_content'][$arra['id']]=$arra['tab_content'];
			$this->elemMenuBase['tabIdType'][$arra['id']]=$arra['tipoId'];
		}
		return;
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE tab_child='.$this->id;
		$existen_paginas=$this->sql->valor($query);
		$query='SELECT tab_content FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE (tab_type=1 || tab_type=2) && id='.$this->id;
		$scriptComprobar=$this->sql->valor($query);

		if($scriptComprobar != 'vacia.php')
		{
			if($scriptComprobar=file_exists($scriptComprobar))	// SE VERIFICA LA EXISTENCIA DEL UN SCRIPT ASOCIADO AL TAB A ELIMINAR
			{
				$existen_paginas++;
				echo '<span style=font-size:.8em;color:'.$_SESSION['colorTextWarning'].'>El Tab tiene un script relacionado: debe borrar ANTES el nombre del script de este registro para poder eliminar el TAB</span>';
			}
		}

		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
		{
			$this->sql->query($query);
			$_REQUEST['actualizarWeb']=2;
		}

		return $existen_paginas;
	}

/**
 * SE AGREGAN REGISTROS CON LOS TABS BASICOS PARA NUEVOS "company_users"
 *
 * @internal record structure:
 * id, id_user, tab_level, tab_child, tab_order, tab_title, tab_type, tab_content, tab_access_level
 *
 * @return void
 */
	function generarTabsBasicos()
	{
		$query='SELECT id+1 FROM '.$this->sql->db.'.'.$this->nombreTabla.' ORDER BY id DESC LIMIT 1';
		$nuevoId=$this->sql->valor($query);
		$posblocklevelone=10;

		//========= LEVEL 0:
		// INTRO PAGE
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\''.($nuevoId++)."', ".$_SESSION['id_usuario'].", 1, 0, 2, 'Inicio', '1', 'avisos.php|miVar=', 1);";
//echo $query.'<h3>('.($this->levelAccessModules['SuperAdmin']).')</h3><pre>';print_r($this);die();
		$this->sql->query($query);


		// BLOCK SYSTEM-CONFIGS
		$query='SELECT id+1 FROM `'.$this->sql->db.'`.`'.$this->nombreTabla.'` ORDER BY id DESC LIMIT 1';
		$nuevoId=$this->sql->valor($query);
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES("'.$nuevoId.'", '.$_SESSION['id_usuario'].', 1, 0, '.$posblocklevelone.', "Personalizar la herramienta", 2, "", '.$this->levelAccessModules['OperadorSite'].')';
		$this->sql->query($query);

		// LEVEL 2: HELP SYSTEM
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Ayudas para los m&oacute;dulos", 2, "ayudas_backoffice.php", '.$this->levelAccessModules['SuperAdmin'].');';
		$this->sql->query($query);

		// LEVEL 2: CONFIGURATION COLORES TECHOFFICE (Y MAS PARA SUPER ADMIN)
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Personalizaci&oacute;n de los colores", 2, "config_back_office.php", '.$this->levelAccessModules['OperadorSite'].');';
		$this->sql->query($query);

		// LEVEL 2: TABS CONTROL TECHOFFICE
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Tabs Site", 2, "tabs_backoffice.php", '.$this->levelAccessModules['MegaAdmin'].');';
		$this->sql->query($query);

		// LEVEL 2: TABS INNER-JQUERY
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Solapas jQuery", 2, "backoffice_tabs_jquery.php", '.$this->levelAccessModules['Good'].');';
		$this->sql->query($query);


		if($_SESSION['usuarioNivel'] >= $this->levelAccessModules['AdminSite'])	// BLOCK SISTEMA
		{
			//========= LEVEL 1:
			// BLOCK SISTEMA
			$query='SELECT id+1 FROM `'.$this->sql->db.'`.`'.$this->nombreTabla.'` ORDER BY id DESC LIMIT 1';
			$nuevoId=$this->sql->valor($query);
			$posSecondLevel=1;
			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES("'.$nuevoId.'", '.$_SESSION['id_usuario'].', 1, 0, '.$posblocklevelone.', "Sistema", 2, "", '.$this->levelAccessModules['OperadorSite'].')';
			$this->sql->query($query);

			// LEVEL 2: ADMIN USERS
			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Usuarios", 2, "usuarios.php", '.$this->levelAccessModules['AdminSite'].');';
			$this->sql->query($query);
//
//			// LEVEL 2: LANGUAJES
//			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Idiomas", 2, "idiomas.php", '.$this->levelAccessModules['Good'].');';
//			$this->sql->query($query);

			// LEVEL 2: BACK-UP BBDD
			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Back-up BBDD", 2, "back_up.php", '.$this->levelAccessModules['Good'].');';
			$this->sql->query($query);

			// LEVEL 2: RESTORE BBDD
			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Restore BBDD", 2, "restore_bbdd.php", '.$this->levelAccessModules['Good'].');';
			$this->sql->query($query);

			// LEVEL 2: HTACCESS AUTHENTICATION
			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Autenticaci&oacute;n por htaccess", 2, "htaccess_aut.php", '.$this->levelAccessModules['Good'].');';
			$this->sql->query($query);

			// LEVEL 2: BASIC CONFIG SITE
			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Config. b&aacute;sica site", 2, "config_site_base.php", '.$this->levelAccessModules['Good'].');';
			$this->sql->query($query);

//			// LEVEL 2: IP-CACHE BY USER
//			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Ip-cache user control", 2, "users_ips.php", '.$this->levelAccessModules['AdminSite'].');';
//			$this->sql->query($query);
//
//			// LEVEL 2: CSS CLASSES
//			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Clases CSS", 2, "css_classes.php", '.$this->levelAccessModules['Good'].');';
//			$this->sql->query($query);
//
//			// LEVEL 2: TARGET LINK TYPES
//			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Tipos de target", 2, "target_type.php", '.$this->levelAccessModules['Good'].');';
//			$this->sql->query($query);
//
//			// LEVEL 2: USER GROUPS
//			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Grupos de usuarios", 2, "user_groups.php", '.$this->levelAccessModules['AdminSite'].');';
//			$this->sql->query($query);
		}


		//========= LEVEL 1:
		// BLOCK LANDIGS ROOT CONFIGS
		$query='SELECT id+1 FROM `'.$this->sql->db.'`.`'.$this->nombreTabla.'` ORDER BY id DESC LIMIT 1';
		$nuevoId=$this->sql->valor($query);
		$posSecondLevel=0;
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES("'.$nuevoId.'", '.$_SESSION['id_usuario'].', 1, 0, '.$posblocklevelone.', "Landings configuration", 2, "", '.(int)$this->levelAccessModules['AdminSite'].')';
		$this->sql->query($query);

		// LEVEL 2:
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Landings local SITE config", 2, "landings_site_config.php", '.$this->levelAccessModules['AdminSite'].');';
		$this->sql->query($query);

		// LEVEL 2:
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Clientes site config", 2, "customers_site_config.php", '.$this->levelAccessModules['MegaAdmin'].');';
		$this->sql->query($query);

		// LEVEL 2:
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "par&aacute;metros Google SEM", 2, "params_sem_google.php", '.$this->levelAccessModules['AdminSite'].');';
		$this->sql->query($query);

		// LEVEL 2:
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "par&aacute;metros Google SEO", 2, "params_seo_google.php", '.$this->levelAccessModules['AdminSite'].');';
		$this->sql->query($query);



		if($_SESSION['usuarioNivel'] >= $this->levelAccessModules['AdminSite'])	// BLOCK STANDARD WEB CONTENTS
		{
			//========= LEVEL 1:
			// BLOCK STANDARD WEB CONTENTS
			$query='SELECT id+1 FROM `'.$this->sql->db.'`.`'.$this->nombreTabla.'` ORDER BY id DESC LIMIT 1';
			$nuevoId=$this->sql->valor($query);
			$posSecondLevel=0;

			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES("'.$nuevoId.'", '.$_SESSION['id_usuario'].', 1, 0, '.$posblocklevelone.', "Contenidos de las landings", 2, "", '.$this->levelAccessModules['AdminSite'].')';
			$this->sql->query($query);

		// LEVEL 2:
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Landings content data", 2, "landings_site_content.php", '.$this->levelAccessModules['AdminSite'].');';
		$this->sql->query($query);


		// LEVEL 2:
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Elementos del formulario", 2, "form_elements.php", '.$this->levelAccessModules['MegaAdmin'].');';
		$this->sql->query($query);

		// LEVEL 2:
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Tel&eacute;fonos por ID fuente", 2, "phones_per_id_sources.php", '.$this->levelAccessModules['AdminSite'].');';
		$this->sql->query($query);

/*
			// LEVEL 2: MODULO PARA GESTION CONTENIDOS VERTICALES
			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Columnas Verticales", 2, "contenidos_columna_vertical.php", '.$this->levelAccessModules['Good'].');';
			$this->sql->query($query);
*/
			// LEVEL 2: WEB MENUS LEVEL ONE
//			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Nivel principal (1)", 2, "menu_nivel1.php", '.$this->levelAccessModules['AdminSite'].');';
//			$this->sql->query($query);

			// LEVEL 2: ELEMENTS LEVEL TWO
//			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Elementos de nivel 2", 2, "menu_nivel2.php", '.$this->levelAccessModules['AdminSite'].');';
//			$this->sql->query($query);

			// LEVEL 2: WEB DINAMIC LINKS/CONTENTS
//			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Links/contenidos para los elementos de nivel 3", 2, "contenidos_dinamicos.php", '.$this->levelAccessModules['AdminSite'].');';
//			$this->sql->query($query);


			// LEVEL 2: MODULO PARA SUBIR IMAGENES
			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Gesti&oacute;n (directa) de im&aacute;genes", 2, "gestion_imagenes.php", '.$this->levelAccessModules['AdminSite'].');';
			$this->sql->query($query);

//			// LEVEL 2: MODULO PARA GESTION CARROUSEL DINAMICO
//			$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Gestor carrousel", 2, "carrousel_imagenes.php", '.$this->levelAccessModules['OperadorSite'].');';
//			$this->sql->query($query);

		}

		//========= LEVEL 1:
		// BLOCK LOGS SITE
		$query='SELECT id+1 FROM `'.$this->sql->db.'`.`'.$this->nombreTabla.'` ORDER BY id DESC LIMIT 1';
		$nuevoId=$this->sql->valor($query);
		$posSecondLevel=0;
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES("'.$nuevoId.'", '.$_SESSION['id_usuario'].', 1, 0, '.$posblocklevelone.', "Logs", 2, "", '.(int)$this->levelAccessModules['MegaAdmin'].')';
		$this->sql->query($query);

		// LEVEL 2: WEB FRONTLOGS
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Logs de la aplicaci&oacute;n", 2, "logs_aplicacion.php", '.$this->levelAccessModules['MegaAdmin'].');';
		$this->sql->query($query);

		// LEVEL 2: BACKOFFICE LOGS
		$query='INSERT INTO `'.$this->nombreTabla.'` VALUES(\'\', '.$_SESSION['id_usuario'].', 2, '.$nuevoId.', '.($posSecondLevel+=10).', "Logs de del Back-office", 2, "logs_backoffice.php", '.$this->levelAccessModules['Good'].');';
		$this->sql->query($query);


//========= LEVEL 1:
		// LOGOUT MODULE
//		$query='INSERT INTO `back_office_tabs` VALUES("", '.$_SESSION['id_usuario'].', 1, 0, 70, "Cerrar", 3, "<span style=\'display: block; height: 100px; padding: 50px; vertical-align: middle;background-color:#D8DEBC;\'><center><br /><i>Usuario accedido: <span style=\'text-decoration: underline;\'>[[NOMBRE_ADMIN]]</span></i><br /><br /><br /><br /><a href=\'index.php?salirAplicacion=1\' class=\'rounded btn\' style=\'letter-spacing:.16em;padding:10px;padding-top:10px !important;background-color:#F7F8F2;\'>CERRAR ESTA SESI&Oacute;N DE TRABAJO</a><br /><br /><br /><br /><br /><br /><div class=\'rounded\' style=\'display:block;width:780px !important;height:48px !important;vertical-align:middle;background-color:#D8DEBC;\'><br />Cerrando la aplicaci&oacute;n desde esta pantalla, usted puede volver a acceder inmediatamente con una nueva sesi&oacute;n de trabajo.</div></center></span>", 1);';
		$query='INSERT INTO `'.$this->sql->prefixTbl.'back_office_tabs` VALUES("", '.$_SESSION['id_usuario'].', 1, 0, 70, "Cerrar", 3, "<span style=\'display: block; height: 100px; padding: 50px; vertical-align: middle;\'><center><br /><i>Usuario accedido: <span style=\'text-decoration: underline;\'>[[NOMBRE_ADMIN]]</span></i><br /><br /><br /><br /><a href=\'index.php?salirAplicacion=1\' class=\'btn rounded\' style=\'letter-spacing: .16em; padding: 10px;\'>CERRAR ESTA SESI&Oacute;N DE TRABAJO</a><br /><br /><br /><br /><br /><br />Cerrando la aplicaci&oacute;n desde esta pantalla, usted puede volver a acceder inmediatamente con un a nueva sesi&oacute;n de trabajo.</center></span>", 1);';

		$this->sql->query($query);

		return;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		if(!$this->id) $this->id=0;
		if($this->tab_type == 2 && !trim($this->tab_content))
			$this->tab_content='vacia.php';

		$this->tab_content=str_replace('"','\'',$this->tab_content);
		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id, id_user, tab_title, tab_level, tab_type, tab_access_level, tab_order,tab_child,tab_content) VALUES ("'.$this->id.'","'.$_SESSION['idUsuarioTabActivo'].'","'.utf8_decode(trim($this->tab_title)).'", "'.$this->tab_level.'", "'.$this->tab_type.'", "'.trim($this->tab_access_level).'","'.$this->tab_order.'","'.$this->tab_child.'","'.utf8_decode(trim($this->tab_content)).'")';

//echo $query.'<h3>('.($this->tab_level . $this->tab_type . trim($this->tab_content)).')</h3><pre>';print_r($_REQUEST);exit;
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos($nombreForm=0)
	{
		$this->PhpEditarHTML($this->nombreTabla,$nombreForm,$this->id);
		$dataPopline=str_replace("\r\n",'',($this->tab_content));
		$dataPopline=html_entity_decode($dataPopline);
		$dataPopline=htmlspecialchars_decode($dataPopline);
//		$this->tab_content=utf8_decode($this->tab_content);
//		$dataPopline=utf8_decode($dataPopline);
?>
<tr>
	<td align="right" title="Texto del men&uacute; para el tab">Texto Tab:</td>
	<td title="Texto del men&uacute; para el tab"><input type='text' id='tab_title' name='tab_title' value='<?=utf8_encode($this->tab_title)?>' style='width:120px'></td>
	<td align="right" title="Nivel de posicionamiento para el tab">Nivel tab:</td>
	<td title="Nivel de posicionamiento para el tab">
<?php
//		$esTab_level=array(1=>'Nivel principal (1)',2=>'Sub-tab (2)',3=>'Sub-sub-tab (3)');	!!!!!!!!! A PROBAR CON TERCER NIVEL
		$numElem=count($this->esTab_level);
		$salida='<select id="tab_level" name="tab_level" style=\'width:104px;\' title="Nivel de posicionamiento para el tab">';
		$salida.='<option value=-1 ';
		if($this->tab_level==-1)
			$salida.='selected=\'selected\'';
		$salida.='>?</option>';
		for($n=1;$n<$numElem+1;$n++)
		{
			$salida.='<option value='.$n.' ';
			if($this->tab_level==$n)
				$salida.='selected=\'selected\'';
			$salida.=' >'.utf8_decode($this->esTab_level[$n]).'</option>';
		}
		$salida.='</select>';
		echo $salida;
?>
  </select>
  </td>
	<td align="right" width="10">Tipo Tab:</td>
	<td>
<?php
		// START SE CONTRUYE EL ARRAY CON TODOS LOS ELEMENTOS DE MENUS
		$salida='<select id="tab_type" name="tab_type" style=\'width:110px\' ><option value="-1" class="tx10" style="color:#999">?</option>';
		while($arra=$this->sql->fila2($this->resTypes))
		{
			$this->arraOTT[$arra['id']]=$arra['descripcion'];
			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->tab_type== $arra['id']) $salida.=' selected="selected"';
			$salida.='>'.utf8_encode($arra['descripcion']).'</option>';
		}
		$salida.='</select>';
		echo $salida;
?>
  </td>
	<td title='Nivel de acceso m&iacute;nimo para este Tab' align="right">Nivel de acceso:</td>
	<td title='Nivel de acceso m&iacute;nimo para este Tab'>
<?php
		$salida='<select id="tab_access_level" name="tab_access_level" style=\'width:50px;\' title="Nivel de acceso m&iacute;nimo para este Tab">';
		$salida.='<option value=-1 ';
		if($this->tab_access_level==-1)
			$salida.='selected=\'selected\'';
		$salida.='>?</option>';

		for($n=0;$n < $this->levelAccessMaxLevel+1;$n++)
		{
			if($_SESSION['usuarioNivel'] < $n) break;	// SE PERMITE MODIFICAR DE NIVEL HASTA EL MAXIMO PERMITIDO AL USUARIO ACTUAL

			$salida.='<option value='.$n.' title="'.$n.'"';
			if($this->tab_access_level==$n && $this->tab_access_level)
				$salida.='selected=\'selected\'';
			$salida.=' >'.$n.'</option>';
		}
		$salida.='</select>';
		echo $salida;
?>
	</td>
</tr>
<tr <?=$this->id ? 'valign="top"' : ''?>>
	<td align="right"><span title="Pertenece al men&uacute; de nivel 1">Pertenece a:</td>
	<td title="Pertenece al men&uacute; de nivel 1">
<?php
		$salida='<select id="tab_child" name="tab_child" style=\'width:145px\' >
			<option value="0" class="tx10">Nivel principal</option>';
		$query='SELECT id, tab_title FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE tab_level=1 && tab_child=0 && tab_type=2 && ID_USER='.$_SESSION['idUsuarioTabActivo'].' ORDER BY tab_title';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->tab_child==$arra['id']) $salida.=' selected="selected"';
			$salida.='>'.utf8_encode($arra['tab_title']).'</option>';
		}

		$salida.='</select>';
		echo $salida;
?>
  </td>
	<td align="right" title="Orden de apariciÃ³n">Posici&oacute;n:</td>
	<td><input type='text' id='tab_order' name='tab_order' value='<?=$this->tab_order?>' style='width:30px;text-align:right' maxlength="2"></td>
	<td align="right" width="100">
		<?=$_SESSION['usuarioNivel']>14?'<a href="javascript:verScriptDisponibles(\'data_tab_content\',\'Contenido\');" class="btn tooltip" title="Abrir ventana con los Links de los scripts disponibles scripts">Contenido?</a>':'Contenido?'?>
		<a class='btn ajax editAjax' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?><?='&innerTextHeight='.$this->innerTextHeight?>&nombreCampo=data_tab_content"  title="">HTML</a>
		<span class="<?=$this->id ? '' : 'hide'?>"><br /><a title="Editar directamente el código <b>HTML</b>" class='btn ajax directHTML tooltip' href="<?=basename($_SERVER['PHP_SELF'])?>?id=<?=$this->id?>&accion=directHTML&fieldName=data_tab_content">code:</a></span>
	</td>
	<td colspan="3">
		<div id="data_tab_content" class='editorPopline' contenteditable='true' style="display:block;margin-top:2px;border:1px solid #999;width:<?=$this->id ? '300px' : '300px';?>;height:<?=$this->id ? '100px' : '40px';?>;overflow:auto;"><?=$dataPopline?></div>
		<textarea id='tab_content' name='tab_content' style='display:none;' ><?=$dataPopline?></textarea>
	</td>
</tr>
<?php
		if($_REQUEST['accion']==2 || $_REQUEST['operacionEfectuada'])	// CUANDO SE ACTUALIZA UN REGISTRO SE PREGUNTA SI SE DESEA ACTUALIZAR EL ENTERO BACK-OFFICE
		{
			$_REQUEST['accion']=0;
			$_REQUEST['operacionEfectuada']=0;
?>
<tr>
	<td colspan="12" align="center"><br /><a href="#null" class='btn' onclick="top.document.location='index.php';" style='padding-right:100px;padding-left:100px;text-align:center;color:<?=$_SESSION['colorTextWarning']?>'>Click aqu&iacute; si desea actualizar todo el back-office AHORA y aplicar los cambios efectuados</a>
  </td>
</tr>
<?php
		}
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--

$(function(){
	//editor jQuery color box
	$(".editAjax").colorbox({
		opacity: 0.4,
		initialHeight:"10px",
		initialWidth:"10%",
		close:true,
		overlayClose:true,
		width:"95%",
		height:"100%",
		iframe:true,
		html:true,
		innerHeight:true
	});
});

function verScriptDisponibles(el,refField)
{
	ancho=450; alto=500;
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/4;
	var y=(b-alto)/2;
	var win=window.open('tabs_backoffice_disponibles_precarga.php?campoActualizar='+el+'&refField='+refField,'popsDisponibles','scrollbars=yes,left='+x+',top='+y+',width='+ancho+',height='+alto);

}
function revisa()
{
	if(document.f<?=$idunico?>.tab_level.value==2 && document.f<?=$idunico?>.tab_child.value<1)
	{
		resaltarCampo(document.f<?=$idunico?>.tab_child);
		document.f<?=$idunico?>.tab_child.focus();
		alert('ERROR: itentan mover al nivel principal una sub-Tab!');
		restauraCampo(document.f<?=$idunico?>.tab_child);
		return;
	}

	if(document.f<?=$idunico?>.tab_title.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.tab_title);
		document.f<?=$idunico?>.tab_title.focus();
		alert('Por favor, escriba el texto del tab de Menu');
		restauraCampo(document.f<?=$idunico?>.tab_title);
		return;
	}
	if(document.f<?=$idunico?>.tab_level.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.tab_level);
		document.f<?=$idunico?>.tab_level.focus();
		alert('Por favor, debe seleccionar el nivel del tab');
		restauraCampo(document.f<?=$idunico?>.tab_level);
		return;
	}
	if(document.f<?=$idunico?>.tab_type.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.tab_type);
		document.f<?=$idunico?>.tab_type.focus();
		alert('Por favor, debe seleccionar el tipo del tab');
		restauraCampo(document.f<?=$idunico?>.tab_type);
		return;
	}
	esNuevo=document.f<?=$idunico?>.tab_type.options[document.f<?=$idunico?>.tab_type.selectedIndex].text;
<?php
		if($this->id && $this->tab_level==1 && $this->tab_type==2)	//CONTROL DE ADVERTENCIA
		{
			$query='SELECT descripcion FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'back_office_tabs_types WHERE id='.$this->tab_type;
			$elTipo=$this->sql->valor($query);
			echo '
			if('.$this->tab_type.' != document.f'.$idunico.'.tab_type.value)
			{
				if(!confirm("AtenciÃ³n: ha cambiando el tipo de TAB de un elemento de tipo \''.$elTipo.'\'\nal ahora seleccionado \'"+esNuevo+"\', esto puede provocar la desconfiguraciÃ³n del back-office, desea continuar?"))
				{
					return false;
				}
			}
';
		}
?>

	if(document.f<?=$idunico?>.tab_access_level.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.tab_access_level);
		document.f<?=$idunico?>.tab_access_level.focus();
		alert('Por favor, debe indicar el nivel de acceso minimo para el Tab');
		restauraCampo(document.f<?=$idunico?>.tab_access_level);
		return;
	}

	err=controlNumerico(document.f<?=$idunico?>.tab_order.value,'tab_order','f<?=$idunico?>',1);
	if(err)
	{
		document.f<?=$idunico?>.tab_order.value='';
		return false;
	}

	if(document.f<?=$idunico?>.tab_order.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.tab_order);
		document.f<?=$idunico?>.tab_order.focus();
		alert('Por favor, indique la posiciÃ³n del tab respecto a las otras');
		restauraCampo(document.f<?=$idunico?>.tab_order);
		return;
	}

	popeditorSaveData("f<?=$idunico?>");
	//document.f<?=$idunico?>.submit();
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}

function recargarUsuarioTab(el)
{
	$("#mesajes_formulario").html("cambiando usuario TABS...");
//	alert("<?=(substr($_SERVER['REQUEST_URI'],-4)=='.php')?>");
//retun;
//	alert("<?=substr($_SERVER['REQUEST_URI'],strrpos($_SERVER['REQUEST_URI'],'/')+1).(substr($_SERVER['REQUEST_URI'],-4)?'&':'?')?>usuarioTabActivar="+el.value);

	document.location="<?=substr($_SERVER['REQUEST_URI'],strrpos($_SERVER['REQUEST_URI'],'/')+1).(substr($_SERVER['REQUEST_URI'],-4)=='.php'?'?':'&')?>usuarioTabActivar="+el.value;
//	document.location=document.location+"&usuarioTabActivar="+el.value<?=$_REQUEST['posicionSolapa']?'+"&posicionSolapa='.$_REQUEST['posicionSolapa'].'"':''?>;

}
-->
</script>
<?php
		if($_SESSION['usuarioNivel']>14)
		{
			if($this->id)
				echo '<input name="id_user" type="hidden" value="'.$_SESSION['idUsuarioTabActivo'].'" />';
			else
			{
				$salida='<div id="divSceltaUser" class="bordeHeader borde" style="display:block;position:absolute;right:'.($this->id?40:110).'px;"><b>&nbsp;Tabs del usuario:</b>&nbsp;';
				$salida.='<select id="id_user" name="id_user" style=\'width:145px\' onchange="recargarUsuarioTab(this)" >';
				if($_SESSION['usuarioNivel']<11) $filtro=' AND id='.$_SESSION['id_usuario'];
				$query='SELECT id,CONCAT(nombre," ",apellidos) AS nombreUser FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE nivel_acceso<='.$_SESSION['usuarioNivel'].$filtro.' ORDER BY nombre';
				$res2=$this->sql->query($query);
				while($arra=$this->sql->fila2($res2))
				{
					$salida.='<option value=\''.$arra['id'].'\'';
					if($arra['id']==$_SESSION['idUsuarioTabActivo'])
						$salida.=' selected="selected"';
					$salida.='>'.utf8_encode($arra['nombreUser']).'</option>';
				}
				$salida.='</select>
	</div>';
				echo $salida;
			}
		}
		else
			echo '<input name="id_user" type="hidden" value="'.$_SESSION['id_usuario'].'" />';
?>


<?php
		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->id_user=$datos['id_user'];
		$this->tab_child=$datos['tab_child'];
		$this->tab_level=$datos['tab_level'];
		$this->tab_order=$datos['tab_order'];
		$this->tab_title=$datos['tab_title'];
		$this->tab_type=$datos['tab_type'];
		$this->tab_content=$datos['tab_content'];
		$this->tab_access_level=$datos['tab_access_level'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$idunico=rand(1,10000).time();

		$filtroAdicional=($_SESSION['usuarioNivel']<11)?' AND id_user='.$_SESSION['id_usuario'].'':' AND id_user='.$_SESSION['idUsuarioTabActivo'];	// SE LIMITA A SI MISMO PARA USUARIOS CON NIVEL INFERIOR A 11

		$filtroPermisos=' AND tab_access_level<='.$_SESSION['usuarioNivel'].$filtroAdicional;

		if (count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b&uacute;squeda:</span> ";

		$query='SELECT * FROM '.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		$querytotal='SELECT COUNT(*) FROM '.$this->nombreTabla.' WHERE 1 '.$filtroSql.$filtroPermisos;

		$ordenacion='tab_level, tab_order'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' DESC';

		$query.=$filtroSql.$filtroPermisos.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res,$idunico);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $querytotal.'<hr>';echo $query;

//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->


<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='100%' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td align="right">Etiqueta:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>tab_title_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'tab_title_busca_u']?>' style='width:70px'></td>
	<td align="right">Nivel Tab:</td>
	<td>
<?php
		$salida='';
		$numElem=count($this->esTab_level);
		$salida.='<select id="'.$this->posicionSolapa.'tab_level_buscN" name="'.$this->posicionSolapa.'tab_level_buscN" style=\'width:104px;\' title="Nivel de posicionamiento para el tab">';
		$salida.='<option value=-1 ';
		if(isset($_SESSION['filtros'][$this->posicionSolapa.'tab_level_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'tab_level_buscN']==-1)
			$salida.='selected=\'selected\'';
		$salida.='>?</option>';
		for($n=1;$n<$numElem+1;$n++)
		{
			$salida.='<option value='.$n.' ';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'tab_level_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'tab_level_buscN']==$n)
				$salida.='selected=\'selected\'';
			$salida.=' >'.$this->esTab_level[$n].'</option>';
		}
		$salida.='</select>';
		echo $salida;
?>
  </td>
	<td align="right" title="Tabs de segundo nivel pertenecientes a... ">Sub-tab:</td>
	<td title="Tabs de segundo nivel pertenecientes a... ">
<?php
		$salida='<select id="'.$this->posicionSolapa.'tab_child_buscN" name="'.$this->posicionSolapa.'tab_child_buscN" style=\'width:140px\' >
		<option value="-1" class="tx10" style="color:#999">?</option>';
		$query='SELECT id,tab_title FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE tab_level=1 AND !tab_child AND id_user='.$_SESSION['idUsuarioTabActivo'].' ORDER BY tab_order';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'tab_child_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'tab_child_buscN']==$arra['id']) $salida.=' selected="selected"';
			$salida.='>'.$arra['tab_title'].'</option>';
		}

		$salida.='</select>';
		echo $salida;
?>
  </td>
	<td align="right">Tipo destino tab:</td>
	<td>
<?php
		$salida='<select id="'.$this->posicionSolapa.'tab_type_buscN" name="'.$this->posicionSolapa.'tab_type_buscN" style=\'width:110px\' ><option value="-1" class="tx10" style="color:#999">?</option>';
		foreach($this->arraOTT as $key=>$value)
		{
			$salida.='<option value=\''.$key.'\'';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'tab_type_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'tab_type_buscN']==$key) $salida.=' selected="selected"';
			$salida.='>'.utf8_encode($value).'</option>';
		}

		$salida.='</select>';
		echo $salida;
?>
  </td>
	<td align="right">Script/Contenido:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>tab_content_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'tab_content_busca_u']?>' style='width:70px'></td>

<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->


<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'61%','','75%','12%');

		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('Usuario','Etiqueta Tab','Nivel Tab','Pertenece a','Orden','Tipo','Script / Contenido','Niv. acc.');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id','id_user','tab_title','tab_child','tab_level','tab_order','tab_type','tab_content','tab_access_level');
		$xc=0;$arraLongitudes=array($xc++=>50,$xc++=>52,$xc++=>52,$xc++=>34,$xc++=>64,$xc++=>29,$xc++=>33,$xc++=>60); // no se incluye el valor para el ID
		$xc=0;$arraLongitudesTitulares=array($xc++=>25,$xc++=>100,$xc++=>160,$xc++=>70,$xc++=>106,$xc++=>36,$xc++=>80,$xc++=>170,$xc++=>50);
		$xc=0;$arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'txt',$xc++=>'txt',$xc++=>'txt',$xc++=>'num',$xc++=>'txt',$xc++=>'txt',$xc++=>'num');
		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$xc=0;
			$query='SELECT descripcion FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'back_office_tabs_types WHERE id='.$a['tab_type'];
			$elTipo=utf8_encode($this->sql->valor($query));
			$query='SELECT CONCAT(nombre," ",apellidos) AS nombreUser FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE id='.$a['id_user'];
			$elUsuarioDelTab=utf8_encode($this->sql->valor($query));

			$query='SELECT tab_title FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$a['tab_child'];
			$perteneceA=($a['tab_child']==0)?'<i style=color:red;>Root level</i>':($this->sql->valor($query));

			$arraDatos[]=array($xc++=>strip_tags($a['id']),$xc++=>$elUsuarioDelTab,$xc++=>utf8_encode($a['tab_title']),$xc++=>$this->esTab_level[$a['tab_level']],$xc++=>utf8_encode($perteneceA),$xc++=>$a['tab_order'],$xc++=>$elTipo,$xc++=>strip_tags(utf8_encode($a['tab_content']),'<b><i>'),$xc++=>$a['tab_access_level']);
		}

		$idRefPops=0;
		$txtRefEliminar=3;
		$posicFondo=0;	// NÃšMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
//echo $destinobusca;return;

		if($num_res>mysql_num_rows($res) && !$this->contFilas)
		{
			foreach($_SESSION['filtros'] as $key=>$value)	// SE RECARGA LA PAGINA SI NO HAY FILAS EN PANTALLA Y HAY RESULTADOS EN LA BUSQUEDA
			{
				$pos=strpos($key,'_busca');
				if ($pos>0)
					$url.='&'.$key.'='.urlencode($value);
			}

			$_SESSION['filtros']['offset_u']=0;
			echo "<script language='JavaScript' type='text/JavaScript'>
//			document.location='$destinobusca.php?$url';
//	alert('$destinobusca?$url');
			</script>";
		}
?>
</table>
<table align='center' cellpadding='2'>
<tr><td style='line-height:22px' align='center'><span id='spn_infoPage'></span>
<?php
		$url='';
		foreach($_SESSION['filtros'] as $key=>$value)
		{
			$pos=strpos($key,'_busca');
			if ($pos>0)
				$url.='&'.$key.'='.urlencode($value);
		}
		if(isset($_REQUEST['offset_u']))
			$offset_u=$_REQUEST['offset_u'];
		else
			$offset_u=0;

// ********************************************************************************************************
// nota importante: EL ÃšLTIMO PARAMETRO ES LA REFERENCIA DEL IFRAME RESPECTO A LOS DEMAS DE LA PAGINA PADRE, es BASICO
// ********************************************************************************************************
		$hay=$this->paginacion($num_res, $this->filasporpagina, $offset_u, $destinobusca.".php?filtrar=".$_REQUEST['filtrar'].$url.'&sentidoOrdenacion='.$_REQUEST['sentidoOrdenacion'].'&ordenarPor='.$_REQUEST['ordenarPor']."&offset_u=",$this->TotPagMostrar,$this->posicionSolapa);
// ********************************************************************************************************
		if(trim($hay))
			echo '<script>
$("#paginarTop").html(\''.$hay.'\');
$("#paginarTop").css("display","block");
</script>';
		echo $hay;

		$query='SELECT tab_title FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id_user='.$_SESSION['idUsuarioTabActivo'].' && tab_level=1 && tab_child!=0';
		$res=$this->sql->query($query); $numErrTabs=0;
		while($arraTab=$this->sql->fila2($res))
		{
			$numErrTabs++;
			$errTabs2.=$arraTab['tab_title'].', ';
		}
		$errTabs2=substr($errTabs2,0,-2);
//echo '<hr />'.$query;
		if($errTabs2)
			echo '<br /><span style="display:block;color:'.$_SESSION['colorTextWarning'].';font-size:1.2em;text-align:left;border:1px solid red;padding:6px;background-color:'.$_SESSION['colorFondoWarning'].';moz-border-radius:6px;-webkit-border-radius:6px;width:90%;"><b>ERROR</b><br />Hay TAB`s (<b><i>'.$errTabs2.'</i></b>) de nivel ROOT de tipo "<u>'.$this->arraOTT[2].'</u>" que est&aacute; configurada como perteneciente a otro elemento del nivel principal (<i>'.$this->esTab_level[1].'</i>): esto es contradictorio, al pertenecer Ã©ste mismo tab al nivel 1 (<i>'.$this->esTab_level[1].'</i>). La soluciÃ³n es configurar el "<u>Nivel de Tab</u>" como <i>'.$this->esTab_level[2].'</i>.</span>';

		$query='SELECT COUNT(DISTINCT(tab_child)) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id_user='.$_SESSION['idUsuarioTabActivo'].' && tab_level=2';
		$numChilds=$this->sql->valor($query);
//echo '<hr />'.$query;

		$query='SELECT COUNT(id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id_user='.$_SESSION['idUsuarioTabActivo'].' && tab_level=1 && (tab_type=2)';
		$numErrTabs=$this->sql->valor($query)-$numChilds;
//echo '<hr />'.$query;

		if($numErrTabs && !$errTabs2)
			echo '<span style="display:block;color:'.$_SESSION['colorTextWarning'].';font-size:1.2em;text-align:left;border:1px solid red;padding:6px;background-color:'.$_SESSION['colorFondoWarning'].';moz-border-radius:6px;-webkit-border-radius:6px;width:90%;"><b>ATENCI&Oacute;N</b><br />Existe por lo menos una TAB de nivel ROOT de tipo "<u>'.$this->arraOTT[2].'</u>" sin tabs de segundo nivel asignadas. Es muy importante que solucione este problema ANTES de actualizar el back-office, pena con <b>NO PODER</b> m&aacute;s subsanar por usted mismo este problema: deber&aacute; avisar al administrador del site.</span>';

?>
<script>
var arraTxtDescripcion=new Array();
var sombraL; var sombraT;

function mostrarPagina(direccion,posicionSolapa)	// se recarga la pagina desde los links de la barra inferior de paginaciÃ³n (2011)
{
	$("#mesajes_formulario").html("<i>accediendo...</i>");
	document.location=direccion+"&posicionSolapa="+posicionSolapa;
	return;
}
</script>
</td></tr></table>
</form>
<?php
	}
}
?>
