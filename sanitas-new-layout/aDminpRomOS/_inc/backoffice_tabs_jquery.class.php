<?php
class backofficeTabsJquery extends Base
{
	//Datos propios de la clase
	var $id;
	var $nombreTabla;
	var $posicionSolapa;	// POSICION DE LA VENTANA RESPECTO A LAS DEMÁS DE SU MISMA SECCION E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS

//******************************************************
//	INICIALIZACION DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'back_office_tabs_jquery';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset']==1) unset($_SESSION['filtros']);
		$this->getAdminLevel();
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->id_tab_level=$a['id_tab_level'];
		$this->id_tab_child=$a['id_tab_child'];
		$this->tab_title=$a['tab_title'];
		$this->tab_content=$a['tab_content'];
		$this->user_id=$a['user_id'];
		$this->enabled=$a['enabled'];
		$this->tab_header=$a['tab_header'];
		return;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		$enEdicion=$this->id ? '_editar':'';

		if($this->id_tab_child<0) $this->id_tab_child=0;
		$this->tab_content=str_replace('"','\'',$this->tab_content);
		if(!$this->id) $this->id=0; // NECESARIO PARA AÑADIR NUEVOS REGISTROS

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id, id_tab_level, id_tab_child, tab_title, tab_content, enabled, user_id, tab_header) VALUES ("'.$this->id.'","'.$this->id_tab_level.'", "'.$this->id_tab_child.'", "'.$this->tab_title.'", "'.str_replace("\r\n",'',$this->tab_content).'", "'.$this->enabled.'", "'.$this->user_id.'", "'.$this->tab_header.'")';

		if(!$_img && $this->id)
		$query='UPDATE '.$this->sql->db.'.'.$this->nombreTabla.' SET id_tab_level="'.$this->id_tab_level.'", id_tab_child="'.$this->id_tab_child.'", tab_title="'.$this->tab_title.'", tab_content="'.str_replace("\r\n",'',$this->tab_content).'", enabled="'.$this->enabled.'", user_id="'.$this->user_id.'", tab_header="'.$this->tab_header.'" WHERE id='.$this->id;

		$this->sql->query($query);
		return;
	}

//******************************************************
//	FORMULARIO DE EDICION /INSERCION DE DATOS
//******************************************************
	function formulario_datos($nombreForm=0)
	{
		$dataPopline=str_replace("\r\n",'',($this->tab_content));
		$dataPopline=html_entity_decode($dataPopline);
		$dataPopline=htmlspecialchars_decode($dataPopline);
?>
<tr valign="top" height="<?=$this->id?50:''?>">
	<td width="90" title='Usuario al que se le asignan las solapas' align="right">Usuario:</td>
	<td width="180">
<?php
		$query='SELECT id, CONCAT(nombre," ",apellidos) AS nombreU, nivel_acceso FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE nivel_acceso <= '.$_SESSION['usuarioNivel'].'';
		$res2=$this->sql->query($query);

		while($arra2=$this->sql->fila2($res2))
		{
			$arrUsers[$arra2['id']]['nivelAcceso']=$arra2['nivel_acceso'];
			$arrUsers[$arra2['id']]['userName']=$arra2['nombreU'];
		}

		$salida='<select id="user_id" name="user_id" style=\'width:99%;\'>';
		$salida.='<option value=-1 ';

		$salida.='>Seleccionar</option>';
		foreach($arrUsers as $idU => $dataU)
		{
			// SE PERMITE USUARIOS CON EL MISMO O INFERIOR NIVEL DEL ADMINISTRADOR ACTUAL
			if($_SESSION['usuarioNivel'] < $dataU['nivelAcceso']) continue;

			$salida.='<option value='.$idU.' title="'.$dataU['userName'].'"';
			$textoMostrar=$dataU['userName'];
			if($_SESSION['id_usuario'] == $idU)
			{
				$textoMostrar='T&uacute; mismo';
				$salida.=' style=color:'.$_SESSION['colorTextWarning'].'; ';
//				if($_SESSION['id_usuario'] == $idU && !$this->id)	// SE SELECCIONA EL USUARIO ACTUAL
//					$salida.='selected=\'selected\'';
			}
			if($this->user_id == $idU && $this->id)
				$salida.='selected=\'selected\'';

			$salida.=' >'.$textoMostrar.'</option>';
		}

		$salida.='</select>';
		echo $salida;
?>
	</td>
	<td align="right" width="55" title="Solapa del nivel principal (del back-office)">Secci&oacute;n:&nbsp;</td>
	<td align='left'>
<?php
		$salida='<select id="id_tab_level" name="id_tab_level" style=\'width:100%;\' >
			<option value="0" class="tx10">Seleccionar</option>';
		$query='SELECT id, tab_title FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'back_office_tabs WHERE tab_level=1 && !tab_child && id_user='.(int)$this->user_id.' ORDER BY tab_order';

		if(isset($this->user_id))
			$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->id_tab_level == $arra['id']) $salida.=' selected="selected"';
			$salida.='>'.utf8_encode($arra['tab_title']).'</option>';
		}

		$salida.='</select>';
		echo $salida; $salida=null;
?>
	</td>
	<td width="70" align='right' title="Solapa de segundo nivel (del back-office)">Sub secci&oacute;n:</td>
	<td align='left' style='width:250px'>
<?php
		if(isset($this->id_tab_child))
		{
			$salida=''; $numTabs=0;
			$query='SELECT id,tab_title FROM '.$this->sql->prefixTbl.'back_office_tabs WHERE tab_child='.$this->id_tab_level.' && tab_level=2 && id_user='.$_SESSION['id_usuario'].' ORDER BY tab_order';
			$res2=$this->sql->query($query);

			while($arra2=$this->sql->fila2($res2))
			{
				$salida.='<option value=\''.$arra2['id'].'\'';
				if($this->id_tab_child == $arra2['id']) $salida.=' selected="selected"';
				$salida.='>'.$arra2['tab_title'].'</option>';
				$numTabs++;
			}
		}
		echo '<select name="id_tab_child" id="id_tab_child" style=\'width:100%;\'>';
		if(!$numTabs && !$this->id)
			echo '<option value="-1" style="color:#888">Escoger primero una sección</option>';
		if(!$numTabs && $this->id)
			echo '<option value="-1" style="color:#888">No tiene</option>';
		echo $salida;
		echo '</select>';
?>
	</td>
</tr>

<tr height="<?=$this->id?1:0?>">
	<td title='T&iacute;tulo cabecera de la solapa' align="right">T&iacute;tulo cabecera:</td>
	<td align='left'>
		<input id="tab_header" name="tab_header" type="text" value="<?=$this->tab_header?>" style="width:100%" />
	</td>

	<td title='Indica si la solapa est&aacute; habilitada' align="right">Activa:</td>
	<td>
<?php
		$arraEsActivo=array(0=>'No',1=>'S&iacute;');
		echo '<select id="enabled" name="enabled" style=\'width:40px;\' title="Nivel de acceso del usuario al Back-office">';
		echo '<option value=-1 ';
		if($this->enabled==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->enabled==$n)
				echo 'selected=\'selected\'';
			echo ' >'.$arraEsActivo[$n].'</option>';
		}
		echo '</select>';
?>
	</td>
	<td title="T&iacute;tulo del contenido" align="right">T&iacute;tulo solapa:</td>
	<td><input id="tab_title" name="tab_title" type="text" value="<?=$this->tab_title?>" /></td>
</tr>

<tr>
	<td align="right" valign="top">Contenido del tab: <a class='btn ajax editAjax' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?><?='&innerTextHeight='.$this->innerTextHeight?>&nombreCampo=data_tab_content" title="">HTML</a>
	</td>
	<td colspan="5" valign="top">
		<div id="data_tab_content" class='editorPopline' contenteditable='true' style="display:block;border:1px solid #999;width:<?=$this->id ? '98%' : '100%';?>;height:<?=$this->id ? '180px' : '40px';?>;overflow:auto;"><?=$dataPopline?></div>
		<textarea id='tab_content' name='tab_content' style='display:none;' ><?=$dataPopline?></textarea>

<!--		<textarea id='tab_content' name='tab_content' rows="<?=($this->id?3:1);?>" style="<?=($this->id?'margin-top:4px;':'');?>;width:96%;"><?=str_replace("\r\n",'',$this->tab_content)?></textarea>-->
  </td>
</tr>

<?php
	}

//******************************************************
//	CONTROLES ESPECIFICOS PARA EL MODULO + CARGA DEL FORMULARIO
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
var txPagina = new Array();
var idPagina = new Array();
var tipoRespuestas=0;
var esArchivo=0;

$(function(){
	//editor jQuery color box
	$(".editAjax").colorbox({
		opacity: 0.4,
		initialHeight:"10px",
		initialWidth:"10%",
		close:true,
		overlayClose:true,
		width:"<?=$this->id ? 88 : 74 ?>%",
		height:"100%",
		iframe:true,
		html:true,
		innerHeight:true
	});

	$("#user_id").change(function(){
		if(this.value < 1)
		{
			resaltarCampo(document.f<?=$idunico?>.user_id);
			alert("Por favor, debe seleccionar un usuario");
			restauraCampo(document.f<?=$idunico?>.user_id);
			return;
		}

		var existElems=0;
		$.ajax({
			url:"ajax_backoffice_tabs_jquery.php",
			method:"post",
			dataType:"json",
			data:{
				idUserManage:this.value,
				thisScript:"<?=basename($_SERVER["SCRIPT_NAME"])?>"
			},
			cache:false,
			success:function(response)
			{
				if(response.numTabs)
				{
					$("#id_tab_child option").remove();
					$("#id_tab_level option").remove();
					$("#id_tab_level").attr("style","color:#000");
					$("#id_tab_child").append($("<option></option>").attr("value",0).text("Escoger primero una sección"));
					$("#id_tab_level").append($("<option></option>").attr("value",0).text("Seleccionar"));

					$.each(response.formData,function(idSubtab,txtTab){
						$("#id_tab_level").append($("<option></option>").attr("value",idSubtab).text(txtTab['tab_title']));
						existElems++;
					})
				}
				else
				{
					$("#id_tab_child option").remove();
					$("#id_tab_level option").remove();
//					$("#id_tab_child").append($("<option></option>").attr("value",0).text("No tiene"));
					$("#id_tab_level").append($("<option></option>").attr("value",0).text("No tiene"));
					$("#id_tab_level").attr("style","color:#888");
				}
				$("#id_tab_level").css("width","100%");
				return;
			},
			error: function(response) {
				alert("ERROR :: REF.0x10998<br>Ha ocurrido un error inesperado");
				return;
			}
		});
	})

	$("#id_tab_level").change(function(){
		if(this.value < 1)
		{
			resaltarCampo(document.f<?=$idunico?>.id_tab_level);
			alert("Por favor, debe seleccionar una solapa principal para continuar");
			restauraCampo(document.f<?=$idunico?>.id_tab_level);
			return;
		}

		var existElems=0;
		$.ajax({
			url:"ajax_backoffice_tabs_jquery.php",
			method:"post",
			dataType:"json",
			data:{
				idLevelOne:this.value,
				thisScript:"<?=basename($_SERVER["SCRIPT_NAME"])?>"
			},
			cache:false,
			success:function(response)
			{
				if(response.numTabs)
				{
					$("#id_tab_child option").remove();
					$("#id_tab_child").attr("style","color:#000");
					$("#id_tab_child").append($("<option></option>").attr("value",0).text("Seleccionar"));

					$.each(response.formData,function(idSubtab,txtTab){
						$("#id_tab_child").append($("<option></option>").attr("value",idSubtab).text(txtTab['tab_title']));
						existElems++;
					})
				}
				else
				{
					$("#id_tab_child option").remove();
					$("#id_tab_child").append($("<option></option>").attr("value",0).text("No tiene"));
					$("#id_tab_child").attr("style","color:#888");
				}
				$("#id_tab_child").css("width","100%");
				return;
			},
			error: function(response) {
				alert("ERROR :: REF.0x10998<br>Ha ocurrido un error inesperado");
				return;
			}
		});
	})
});

function revisa()
{
	if(document.f<?=$idunico?>.user_id.value < 1)
	{
		resaltarCampo(document.f<?=$idunico?>.user_id);
		alert('Por favor, seleccione un usuario de la lista');
		document.f<?=$idunico?>.user_id.focus();
		restauraCampo(document.f<?=$idunico?>.user_id);
		return;
	}
	if(document.f<?=$idunico?>.id_tab_level.value < 1)
	{
		resaltarCampo(document.f<?=$idunico?>.id_tab_level);
		alert('Por favor, seleccione una sección principal para esta solapa jQuery');
		document.f<?=$idunico?>.id_tab_level.focus();
		restauraCampo(document.f<?=$idunico?>.id_tab_level);
		return;
	}
	if(document.f<?=$idunico?>.id_tab_level.value > 0 && document.f<?=$idunico?>.id_tab_child.length > 1 && document.f<?=$idunico?>.id_tab_child.value < 1)
	{
		resaltarCampo(document.f<?=$idunico?>.id_tab_child);
		alert('Por favor, seleccione una sub-sección para poder proseguir');
		document.f<?=$idunico?>.id_tab_child.focus();
		restauraCampo(document.f<?=$idunico?>.id_tab_child);
		return;
	}
	if(!trim(document.f<?=$idunico?>.tab_title.value))
	{
		resaltarCampo(document.f<?=$idunico?>.tab_title);
		alert('Por favor, escriba el título de la solapa');
		document.f<?=$idunico?>.tab_title.focus();
		restauraCampo(document.f<?=$idunico?>.tab_title);
		return;
	}
	if(!trim(document.f<?=$idunico?>.tab_header.value))
	{
		resaltarCampo(document.f<?=$idunico?>.tab_header);
		alert('Por favor, escriba la cabecera para esta solapa');
		document.f<?=$idunico?>.tab_header.focus();
		restauraCampo(document.f<?=$idunico?>.tab_header);
		return;
	}
	if(document.f<?=$idunico?>.enabled.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.enabled);
		alert('Por favor, indique si ha de estar habilitado');
		document.f<?=$idunico?>.enabled.focus();
		restauraCampo(document.f<?=$idunico?>.enabled);
		return;
	}
	popeditorSaveData("f<?=$idunico?>");
//	document.f<?=$idunico?>.submit();
}

-->
</script>
<?php
		$camposAdicionales='';
		include('_inc/form_std_edit.php');
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->quitar_archivo=$datos['quitar_archivo'];
		$this->id=$datos['id'];
		$this->id_tab_level=$datos['id_tab_level'];
		$this->id_tab_child=$datos['id_tab_child'];
		$this->tab_title=$datos['tab_title'];
		$this->tab_header=$datos['tab_header'];
		$this->user_id=$datos['user_id'];
		$this->enabled=$datos['enabled'];
		$this->tab_content=$datos['tab_content'];
		$this->guarda_datos();
	}

//******************************************************
//	ELIMINACION DE REGISTROS CON CONTROL DE DATOS RELACIONADOS DE OTRAS TABLAS AL MISMO
//******************************************************
	function eliminar_registro()
	{
		$query1='SELECT COUNT(id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE enabled && id='.(int)$_REQUEST['id'];
//echo $query1.'<br /><br />existen_paginas='.$existen_paginas.'<br />'.$query.'<br /><pre>';print_r($_REQUEST);print_r($this);exit;
		$existen_paginas=$this->sql->valor($query1);
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$_REQUEST['id'];
		if(!$existen_paginas && $_REQUEST['id'])	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens)
	{
		$mens.='</b>';
		if (count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b&uacute;squeda:</span> ";

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID
		$_SESSION['filtros']['offset_u']*=1;

		$queryTmp='SELECT id FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE nivel_acceso <= '.$_SESSION['usuarioNivel'].'';
		$resTmp=$this->sql->query($queryTmp);
		$listValidUserId='';
		while($arraTmp=$this->sql->fila2($resTmp))
		{
			$listValidUserId.=$arraTmp['id'].',';
		}
		$listValidUserId=substr($listValidUserId,0,-1);
		$filtroSql.='&& user_id IN('.$listValidUserId.') ';
//echo $listValidUserId.'<hr>';


		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='tab_title'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $query;die();
//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<form action='<?=$destinobusca?>.php' method='post' name='formListados'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->

<table align='center' cellpadding='3' cellspacing='0' class='borde' width='920'>
<thead>
<tr>
	<td class='enc2' colspan='4'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
</tr>
</thead>
<tr>
	<td align="right">Sección:</td>
	<td>
<?php
		$salida='<select id="'.$this->posicionSolapa.'id_tab_level_buscN" name="'.$this->posicionSolapa.'id_tab_level_buscN" style=\'width:165px\' >
			<option value="-1" class="tx10">Seleccionar</option>';
		$query='SELECT id, tab_title FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'back_office_tabs WHERE tab_level=1 && !tab_child && id_user='.(int)$_SESSION['id_usuario'].' ORDER BY tab_order';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_tab_level_buscN'] == $arra['id']) $salida.=' selected="selected"';
			$salida.='>'.utf8_encode($arra['tab_title']).'</option>';
		}

		$salida.='</select>';
		echo $salida;
?>
	</td>
	<td width="60" align="right" title="Apartado de nivel 2">Nivel 2:</td>
	<td>
<?php
		$filtro2=$_SESSION['filtros'][$this->posicionSolapa.'id_tab_level_buscN']?' WHERE id_level_one='.$_SESSION['filtros'][$this->posicionSolapa.'id_tab_level_buscN']:'';
		$query='SELECT mlt.*,i.iso3 FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'mnu_level_two AS mlt INNER JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'idiomas AS i ON i.id=mlt.id_idioma '.$filtro2.' ORDER BY mlt.etiqueta';
		$res2=$this->sql->query($query);

		echo '<select id=\''.$this->posicionSolapa.'id_tab_child_buscN\' name=\''.$this->posicionSolapa.'id_tab_child_buscN\' style=\'width:100px\'><option value=\'-1\'>?</option>';
		while($arra2=$this->sql->fila2($res2))
		{
			echo '<option value=\''.$arra2['id'].'\' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'id_tab_child_buscN'] == $arra2['id'])
				echo 'selected=\'selected\'';
			echo '>'.$arra2['etiqueta'].($_SESSION['forcedLangSite']?'' :'::'.$arra2['iso3']).'</option>';
		}
		echo '</select>';
?>
	</td>
	<td width="68" align="right">T&iacute;tulo:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>tab_title_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'tab_title_busca_u']?>' style='width:70px' /></td>

	<td width="20" title="Titular del Header">Header:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>tab_header_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'tab_header_busca_u']?>' style='width:70px' /></td>

	<td width="30">Activo:</td>
	<td>
<?php
		$arraEsActivoBusc=array(0=>'No',1=>'S&iacute;');
		echo '<select name="'.$this->posicionSolapa.'enabled_buscN" style=\'width:42px;\' title="Est&aacute; activo?" class="tx10">';
		echo '<option value="-1" >?</option>';
		foreach($arraEsActivoBusc as $key=>$value)
		{
			echo '<option value='.$key.' ';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'enabled_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'enabled_buscN']==$key)
				echo 'selected=\'selected\'';
			echo ' >'.utf8_encode($value).'</option>';
		}
		echo '</select>';
?>
  </td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,$this->modalHeight,'',$this->modalWidth,$this->modalLeftPos);

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		if(!$_SESSION['forcedLangSite']) $arraTitulares[]='Idioma';
		$arraTitulares[]='T&iacute;tulo jQuery';
		$arraTitulares[]='Cabecera';
		$arraTitulares[]=$_SESSION['forcedLangSite'] ? 'Solapa principal' : 'Solapa principal / idioma';
		$arraTitulares[]='Sub-solapa';
		$arraTitulares[]='Usuario';
		$arraTitulares[]='Activo';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		if(!$_SESSION['forcedLangSite']) $arraCamposOrdenar[]='id_idioma';
		$arraCamposOrdenar[]='tab_title';
		$arraCamposOrdenar[]='tab_header';
		$arraCamposOrdenar[]='id_tab_level';
		$arraCamposOrdenar[]='id_tab_child';
		$arraCamposOrdenar[]='user_id';
		$arraCamposOrdenar[]='enabled';

		 // no se incluye el valor para el ID
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=38;
		$arraLongitudes[]=32;
		$arraLongitudes[]=80;
		$arraLongitudes[]=36;
		$arraLongitudes[]=140;
		$arraLongitudes[]=50;
		$arraLongitudes[]=50;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=30;	// id
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=60;
		$arraLongitudesTitulares[]=160;
		$arraLongitudesTitulares[]=150;
		$arraLongitudesTitulares[]=120;
		$arraLongitudesTitulares[]=100;
		$arraLongitudesTitulares[]=195;
		$arraLongitudesTitulares[]=42;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='txt';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$esCorecta='&nbsp;';
			$esActivo=$a['enabled']?'SI':'<span style=color:'.$_SESSION['colorTextWarning'].'><b>NO</b></style>';
//			$niveDeAcceso=$a['user_level'] < 1 ? '<span style=color:'.$_SESSION['colorTextWarning']."><b>Todos</b></style>":'<span style=color:'.$_SESSION['colorTextoStd'].'><b>'.$a['user_level'].'</b></style>';
			$query2='SELECT CONCAT(nombre," ",apellidos) AS nombreU FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE id= '.$a['user_id'];
			$userNameDDBB=$this->sql->valor($query2);

			$tipoContenido=$b['texto_tipo'];
			$tipoId=$b['id'];
			$tipoContenido=($tipoId==1?'<span style=color:'.$_SESSION['colorTextoStd'].'>'.$tipoContenido.'</span>':$tipoContenido);

			$query2='SELECT tab_title FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'back_office_tabs WHERE id='.$a['id_tab_level'];
//			$query='SELECT CONCAT(mlo.etiqueta'.($_SESSION['forcedLangSite']?'':'," (<i>",i.iso3,"</i>)"').') FROM '.$this->sql->db.'.mnu_level_one AS mlo INNER JOIN '.$this->sql->db.'.idiomas AS i ON i.id=mlo.id_idioma WHERE mlo.id='.$a['id_tab_level'];
			$nombrePagina=$this->sql->valor($query2);

			if(!$_SESSION['forcedLangSite'])
			{
				$query2='SELECT idioma FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'idiomas WHERE id='.$a['id_idioma'];
				$elIdioma=  utf8_encode($this->sql->valor($query2));
				$elIdioma=$elIdioma?$elIdioma:'?';
			}

//			$query2='SELECT CONCAT(mlo.etiqueta'.($_SESSION['forcedLangSite']?'':'," (<i>",i.iso3,"</i>)"').') FROM '.$this->sql->db.'.mnu_level_two AS mlo INNER JOIN '.$this->sql->db.'.idiomas AS i ON i.id=mlo.id_idioma WHERE mlo.id=\''.$a['id_tab_child'].'\'';
			$query2='SELECT tab_title,tab_content FROM '.$this->sql->prefixTbl.'back_office_tabs WHERE id='.(int)$a['id_tab_child'];
			$nombreSubPagina=$this->sql->valor($query2);
			if(!$nombreSubPagina) $nombreSubPagina='<i style=color:'.$_SESSION['colorTextLoose'].'>ninguna</i>';

			$txtTitular=strip_tags($a['tab_title']);
			if(strlen($txtTitular)>55) $txtTitular=substr($txtTitular,0,50).' [...]';
			if($txtTitular!=$a['tab_title']) $txtTitular=$txtTitular.' **';	// SE AÑADEN ASTERISCOS SI EL TITULAR TIENE ALGÚN FORMATO ADICIONAL
			if(!trim($txtTitular)) $txtTitular='<i style=color:#999>no tiene</i>';

			$fecha=date('d-m-Y',strtotime($a['fecha']));

			$tmpArr[0]=$a['id'];
			if(!$_SESSION['forcedLangSite']) array_push($tmpArr,$elIdioma);
			array_push($tmpArr,$txtTitular);
			array_push($tmpArr,$a['tab_header']);
			array_push($tmpArr,$nombrePagina);
			array_push($tmpArr,$nombreSubPagina);
			array_push($tmpArr,$userNameDDBB);
			array_push($tmpArr,$esActivo);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
		}

		$idRefPops=0;
		$txtRefEliminar=1;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
