<?php
class cssClasses extends Base
{
	//Datos propios de la clase
	public $id;
	public $nombre;
	public $nombreTabla;
	public $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'css_classes';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las búsquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->css_class=$a['css_class'];
		$this->active=$a['active'];
		return;
	}

//******************************************************
//	ELIMINACIÓN DE REGISTROS CON CONTROL DE DATOS RELACIONADOS DE OTRAS TABLAS U OTRAS SELECCIONES
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(cc.id)+COUNT(c.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS cc LEFT JOIN '.$this->sql->db.'.contenidos AS c ON c.css_class=cc.id WHERE cc.active && cc.id='.$_REQUEST['id'];
		$existen_paginas=$this->sql->valor($query);

		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//	GUARDAR DATOS DE INSERCIONES Y EDICIONES
//******************************************************
	function guarda_datos()
	{
		$enEdicion=$this->id ? '_editar' : '';
		if(!$this->id) $this->id=0; // NECESARIO PARA AÑADIR NUEVOS REGISTROS

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id, active, css_class) VALUES ("'.$this->id.'", "'.(int)$this->active.'", "'.utf8_decode($this->css_class).'")';

		if(!$_img && $this->id)
		$query='UPDATE '.$this->sql->db.'.'.$this->nombreTabla.' SET active="'.(int)$this->active.'", css_class="'.utf8_decode($this->css_class).'" WHERE id='.(int)$this->id;

//echo $query.'<br /><pre>';print_r($_REQUEST);print_r($this);exit;
		$this->sql->query($query);
		return;
	}


//******************************************************
//	FORMULARIO DE EDICIÓN /INSERCIÓN DE DATOS
//******************************************************
	function formulario_datos($nombreForm=0)
	{
//		$this->PhpEditarHTML($this->nombreTabla,$nombreForm,$this->id);
		$arraSiNo=array('No','S&iacute;');
?>
<tr>
		<td width="90" title="Nombre de la clase CSS" align="right">Clase CSS:</td>
		<td><input type='text' id='css_class' name='css_class' value='<?=$this->css_class?>' maxlength="255" style='width:98%'/></td>
	<td width="70" align="right" title="El men&uacute; es activo?">Est&aacute; activo:</td>
	<td>
<?php
		echo '<select id="active" name="active" style="width:50px;"';
		echo '<option value=-1 ';
		if($this->active==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->active==0 && isset($this->active))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->active==1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>
  </tr>

<?php
		if($this->id) echo '<tr><td height="10"></td></tr>';
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
$(document).ready(function(){
});

function revisa()
{
	if(!trim($('#css_class').val()))
	{
		resaltarCampo($("#css_class")[0]);
		$("#css_class").focus();
		alert('Es obligatorio indicar el nombre de la clase');
		$("#css_class").focus();
		restauraCampo($("#css_class")[0]);
		return false;
	}
	if($('#active').val() < 0)
	{
		resaltarCampo($("#active")[0]);
		$("#active").focus();
		alert('Por favor, seleccione un usuario');
		$("#active").focus();
		restauraCampo($("#active")[0]);
		return;
	}

	document.f<?=$idunico?>.submit();
}

-->
</script>
<?php
		$camposAdicionales='<input type="hidden" id="ref_desplegable" name="ref_desplegable" value="">
';
		include('_inc/form_std_edit.php');
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->css_class=$datos['css_class'];
		$this->active=$datos['active'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	public function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='Número registros: ')
	{
//		$mens.='</b>';
		if(count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la búsqueda:</span> ";

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='css_class'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' DESC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';

//******************************************************
//Formulario para efectuar búsquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>

<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>
<form action='<?=$destinobusca?>.php' method='post' name='formListados'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->

<table align='center' cellpadding='3' cellspacing='0' class='borde' width='500' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td width="70" title="Buscar por el nombre de la clase CSS">Combre clase:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>css_class_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'css_class_busca_u']?>' style='width:80px'></td>
	<td width="30">Activo:</td>
	<td>
<?php
		$arraEsActivoBusc=array(0=>'No',1=>'S&iacute;');
		echo '<select name="'.$this->posicionSolapa.'active_buscN" style=\'width:50px;\' title="Est&aacute; activo?">';
		echo '<option value="-1" >?</option>';
		foreach($arraEsActivoBusc as $key=>$value)
		{
			echo '<option value='.$key.' ';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'active_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'active_buscN']==$key)
				echo 'selected=\'selected\'';
			echo ' >'.utf8_encode($value).'</option>';
		}
		echo '</select>';
?>
  </td>

<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->

<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,$this->modalHeight,'',$this->modalWidth,$this->modalLeftPos);
//		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'55%','','94%','3%');	//DATOS PARA LA POP INCRUSTADA

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='Nombre de la clase CSS';
		$arraTitulares[]='Activo';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='css_class';
		$arraCamposOrdenar[]='active';

		 // no se incluye el valor para el ID
		$arraLongitudes[]=40;
		$arraLongitudes[]=45;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=40;
		$arraLongitudesTitulares[]=300;
		$arraLongitudesTitulares[]=60;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';

		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$esActivo=($a['active']==1)?'<span style=color:'.$_SESSION['colorTextHighli'].'>SI</span>':'<span style=color:'.$_SESSION['colorOverImportant'].'><b>NO</b></style>';

			$tmpArr[0]=$a['id'];
			array_push($tmpArr,$a['css_class']);
			array_push($tmpArr,$esActivo);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
		}

		$idRefPops=0; $txtRefEliminar=1; $ocultarEliminar=0;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo,$ocultarEliminar);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
