<?php
class commonListed extends Base
{

//******************************************************
//	FUNCIONES JAVASCRIPT COMUNES
//******************************************************
	function javascriptFunctions($idunico,$destinobusca,$altoPopInterna='40%',$txtEliminar='',$anchoPopInterna='75%',$leftPosicion='50px',$esEditable=1)
	{
		$this->ObtenerNavegador();
?>
<!-- START BLOQUE PARA GESTIONAR EDICION FLOTANTE -->
<input type="hidden" name="accion" value="<?=$_REQUEST['accion']?>" />
<input type="hidden" id="id" name="id" value="" />

<!-- <iframe id="ifrNotas" name="ifrNotas" style="display:none"></iframe> -->
<div id="divEdicionFlotante" style="position:absolute;display:none;top:0px;width:<?=$anchoPopInterna?>;height:<?=$altoPopInterna?>;">
<iframe id="ifrFlotanteEdicion" name="ifrFlotanteEdicion" style="display:inline;width:99.9%;height:100%;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="0" src="" width="100%"></iframe>
</div>

<div id="divEdicionFlotanteSombra" style="filter:alpha(opacity=20);top:0px;left:0px;height:100%;width:100%;background-color:<?=$_SESSION['colorFondoFlotante']?>;"></div>
<iframe id="ifrEdition" name="ifrEdition" style="display:none;width:90%"></iframe>

<div id="divEdicionProducto" style="position:absolute;display:block;top:90px;left:40px;width:90%;text-align:left;z-index:101">
</div>
<!-- END BLOQUE PARA GESTIONAR EDICION FLOTANTE -->

<script language="JavaScript" type="text/javascript">
<!--
var xXp=-100; var fMH=0; var qMH=0;
var opacidad=30;
var tmpConsulta;
<?php
echo 'var elNavegador="'.$this->nameNavegador.'";';
?>

function aparecerDespacio(idDiv)
{
//	if($.browser.msie && ($.browser.version<7))
	if(elNavegador == "msie")
	{
		alert("Le recordamos que la versión del navegador que está utilizando no es adecuada,\n\ndebe actualizar o cambiar el navegador que está utilizando.");
		$("#divEdicionFlotanteSombra").hide();
		return;
	}

	$("#divEdicionFlotante").css("left","<?=$leftPosicion?>");
	$("#divEdicionFlotante").slideDown("slow",function(){
		$("#divEdicionFlotanteSombra").html("&nbsp;");
		formListo("auto");
	});
	return;
}

function formListo(como)
{
	$("#ifrFlotanteEdicion").css("overflow",como);
}

function editarRegistro(id)
{
	if(id>0)
		idVisualizado=id;
		document.getElementById('divEdicionFlotanteSombra').innerHTML='<center><span style=color:#000;background-color:#ffffff;font-size:16px><b>&nbsp;CARGANDO...&nbsp;</b></span></center>';
	$("#divEdicionFlotanteSombra").fadeIn("fast");

	document.formListados<?=$idunico?>.target='ifrEdition';
	laAccion=document.formListados<?=$idunico?>.accion.value;
	document.formListados<?=$idunico?>.accion.value=8;
	document.formListados<?=$idunico?>.id.value=idVisualizado;
	document.formListados<?=$idunico?>.submit();
	document.formListados<?=$idunico?>.accion.value=laAccion;
	document.formListados<?=$idunico?>.target='_self';
	return;
}

function edicionClose()	// CERRAR EDICIÓN DE PRODUCTO SIN REALIZAR CAMBIOS
{
	formListo("hidden");
	$("#divEdicionFlotante").slideToggle("slow",function(){
		$("#divEdicionFlotanteSombra").fadeOut("fast",function()
		{
			$("#ifrFlotanteEdicion").attr("src","recargar_modulo.php");
		});
	});
	return;
}

function modifica(id)	// PARA MANTENER COMPATIBILIDAD
{
<?php
	if(!$esEditable)	// SE OMITEN FORMULARIOS QUE NO SE DEBEN EDITAR
		echo 'return;';
?>
	editarRegistro(id);
	return;
}

function accesoNoPermitido(id)
{
	alert("INFORMACIÓN: no puede o no tiene permisos para acceder a este elemento desde esta pantalla!");
	return false;
}

// PERMITE CERRAR VENTANAS MODALES DE BORRADO
var modalClose=$.colorbox.close;

function eliminar(id,nombre)
{
//alert($("#x_"+id).html());return;
	if(!confirm('Desea realmente eliminar <?=$txtEliminar?> "'+nombre+'"?'))
		return false;
	deleteFormPop=$("#x_"+id).colorbox({
		opacity: 0.4,
		title:"<b>Proceso iniciado</b>",
		initialHeight:"0px",
		initialWidth:"0%",
		close:false,
		overlayClose:false,
		width:"550px",
		href:"ajax_admin.php?id="+id+"&accion=29&scriptExec=<?=$destinobusca?>&posicionSolapa=<?=$_REQUEST['posicionSolapa']?>&nombreElemento="+nombre,
		height:"350",
		iframe:true,
		html:false,
		innerHeight:true
	});
		return false;
}


function ordenarPorCampo(campo)	// ORDENACIÓN DEL LISTADO DE RESULATDOS
{
	document.getElementById('mesajes_formulario').innerHTML='<span style=color:<?=$_SESSION['colorTextWarning']?>><b>ordenando...</b></span>';
	if(document.formListados<?=$idunico?>.ordenarPor.value==campo)	// SI EL CAMPO POR EL QUE SE ESTÁ ORDENANDO ES EL MISMO, SE INVIERTE EL SENTIDO DEL ORDEN DEL MISMO
	{
		if(document.formListados<?=$idunico?>.sentidoOrdenacion.value==1)
			document.formListados<?=$idunico?>.sentidoOrdenacion.value=0;
		else
			document.formListados<?=$idunico?>.sentidoOrdenacion.value=1;
	}
	// SE RECARGA LA PÁGINA CON LOS VALORES CORRIENTES ORDENANDO POR EL NUEVO CAMPO SELECCIONADO
	document.location='<?=$destinobusca?>.php?reset=<?=$_REQUEST['reset']?>&offset_u=<?=$_REQUEST['offset_u']?>&ordenarPor='+campo+'&sentidoOrdenacion='+document.formListados<?=$idunico?>.sentidoOrdenacion.value+'&posicionSolapa=<?=$_REQUEST['posicionSolapa']?>';
	return;
}

function admin_abre_imagen2(dir,ancho,alto,nombre_foto)	// POP DE LAS IMAGENES DE ADMINISTRACION
{
	var x=(sc_an-ancho)/2;
	var y=(sc_al-alto)/2;
	dir=dir.replace('#','%23');
	dir=dir.replace('#','%23');
	dir=dir.replace('#','%23');
	dir=dir.replace('#','%23');
	cont=0;
	while(nombre_foto.search('/')>0){nombre_foto=nombre_foto.substr(nombre_foto.search('/')+1);cont++;if(cont>10)break;}

	t=window.open (dir,'img','width='+ancho+',height='+(alto+40)+',left='+x+',top='+y);
	t.document.write("<html><title>"+nombre_foto+"</title><style>body{font-family:arial;font-size:.7em;}a{display:block;margin-top:2px;text-align:center;background-color:#DCE6F4;width:90%;color:#000;text-decoration:none;border:1px solid #000;}</style><head><title>"+nombre_foto+"</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><center><img src='"+dir+"'><a href='javascript:window.close();' class=''><i>Cerrar</i> <b>"+nombre_foto+"</b></a></center></body></html>");
	t.focus();
}

-->
</script>

<?php
	}
}
?>