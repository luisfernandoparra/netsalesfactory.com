﻿<?php
class deleteFilters extends Base
{
	//Datos propios de la clase

//******************************************************
//	INICIALIZACION DE LA CLASE
//******************************************************
	function __construct($sql,$tablaPrincipal='',$posicionSolapa)
	{
		$this->nombreTabla=$tablaPrincipal;
		$this->posicionSolapa=$posicionSolapa;
		$this->sql=$sql;
	}

//******************************************************
//
//******************************************************
	function elimiarRegistrosFiltrados()
	{
		$res=0;
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';
//echo $query;die();
		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID
		$_SESSION['filtros']['offset_u']*=1;
		$query.=$filtroSql;
		$res=$this->sql->query($query);
//echo $query;
//echo '<pre>REQUEST:';	print_r($_REQUEST);
//echo '<pre>session:';	print_r($_SESSION);
//echo '<pre>objeto:';	print_r($this);
		return $res;
	}
}
?>