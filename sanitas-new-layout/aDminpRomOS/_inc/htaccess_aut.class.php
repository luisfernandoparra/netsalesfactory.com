<?php
class HtaccessAut extends Base
{
	//Datos propios de la clase
	var $id;

//******************************************************
//
//******************************************************
	function __construct($sql)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'usuarios';
		$this->posicionSolapa=$posicionSolapa;
		return;
	}

//******************************************************
//	CARGAR CONFIGURACION DE SISTEMA (ID=1, FORZADO PARA MANTENER LA COMPATIBILIDAD)
//******************************************************
	function cargaConfig()
	{
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'system_config WHERE 1';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->htaccess_enabled=$a['htaccess_enabled'];
		return;
	}

//******************************************************
//	SE CARGAN EN EL OBJETO LOS CAMPOS A MANEJAR
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->htaccess_enabled=$datos['htaccess_enabled'];
		$this->guarda_datos();
		$txtMsgREcordInsertado=1;
		return;
	}

//******************************************************
//	GUARDAR DATOS DE INSERCIONES Y EDICIONES
//******************************************************
	function guarda_datos()
	{
	  $is_iis=stripos($HTTP_SERVER_VARS['SERVER_SOFTWARE'],'iis');
	  $_REQUEST['errorControl']=0;
	  $htaccess_array = null;
	  $htpasswd_array = null;

	  if($this->htaccess_enabled)
	  {
		$controlFiles=0;
		if($posibleEscribirFiles=$this->verificarEscribible('.htaccess'))// SE COMPRUEBA EL ACCESO DE ESCRITURA DE LA CARPETA DE ADMIN
		{
		  $controlFiles+=file_exists(DIR_FS_ADMIN.'.htpasswd');
		  $controlFiles+=file_exists(DIR_FS_ADMIN.'.htaccess');
//		  $controlFiles+=$this->verificarEscribible(DIR_FS_ADMIN.'.htaccess');

		  if(!$controlFiles)	// SE CREAN LOS ARCHIVOS, NO EXISTEN AUN
		  {
			$htaccess_array = array();
			$htpasswd_array = array();
			$query='SELECT * FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE activo ORDER BY id';
			$res=$this->sql->query($query);

			while($arra=$this->sql->fila2($res))
			  $htpasswd_array[]=$arra['usuario'].':'.$arra['ht_pass'];

			$authuserfile_array = array(
'AuthType Basic',
'AuthName "Debe identificarse para acceder al Back-office"',
'AuthUserFile ' . DIR_FS_ADMIN . '.htpasswd',
'IndexIgnore .htaccess',
'IndexIgnore .htpasswd',
'Require valid-user');

//			$authuserfile_array = array(
//'AuthType Basic',
//'AuthName "Debe identificarse para acceder al Back-office"',
//'AuthUserFile ' . DIR_FS_ADMIN . '.htpasswd',
//'<files ".htaccess">',
//'order allow,deny',
//'deny from all',
//'</files>',
//'IndexIgnore .htaccess',
//'IndexIgnore .htpasswd',
//'Require valid-user');
//
			array_splice($htaccess_array, sizeof($htaccess_array), 0, $authuserfile_array);
//echo'[<pre>';print_r(implode("\n", $htpasswd_array));echo hash_algos();exit;
			$fp=fopen(DIR_FS_ADMIN.'.htpasswd','w');
			chmod('.htpasswd', 0755);
			fwrite($fp,implode("\n", $htpasswd_array));
			fclose($fp);

			$fp=fopen(DIR_FS_ADMIN.'.htaccess','w');
			chmod('.htaccess', 0755);
			fwrite($fp,implode("\n", $htaccess_array));
			fclose($fp);
		  }
		}
		else
		{
		  $_REQUEST['accion']=6; // ERROR, NO SE PUEDE ESCRIBIR
		  return false;
		}
		$query='REPLACE INTO '.$this->sql->db.'.'.$this->sql->prefixTbl.'system_config (id, htaccess_enabled) VALUES (1, '.$this->htaccess_enabled.')';
	  }
	  else
	  {
		$query='REPLACE INTO '.$this->sql->db.'.'.$this->sql->prefixTbl.'system_config (id, htaccess_enabled) VALUES (1, 0)';
		@unlink('.htaccess');
		@unlink('.htpasswd');
	  }

//echo '<hr/>'.$query.'<br /><pre>';print_r($_REQUEST);print_r($this);exit;
	  $this->sql->query($query);
	  return;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
  //alert(document.f<?=$idunico?>.name);
  document.f<?=$idunico?>.submit();
}
$(function(){
});
-->
</script>

<?php
		$permisoSuperAdmin=$this->obtenerNivelMaxAcceso();

		if($_SESSION['usuarioNivel'] != $permisoSuperAdmin)	// SOLO SE PERMITE EL ACCESO A ESTA FICHA A LOS SUPER-ADMINISTRADORES
		{
		  echo '<div class="txt" style="padding:2%;"><center><b>Usted no tiene permiso para acceder a esta ficha.</b></center><br /><br />Si necesita utilizar esta herramienta, por favor, póngase en contacto con el super-administrador del sitio o con el responsable de la empresa.</div>';
		  return false;
		}
		include('_inc/form_std_edit.php');

		return $idunico;
	}

//******************************************************
//	FORMULARIO DE EDICION /INSERCION DE DATOS
//******************************************************
	function formulario_datos($nombreForm=0)
	{
	  $this->cargaConfig();
//echo'<pre>';print_r($this);
?>
  <tr>
	<td valign="top" title="Control de acceso por htaccess" align="right"><b>htaccess habilitado</b>:</td>
	<td valign="top" title="Control de acceso por htaccess">
<?php
		$arraEsActivo=array(0=>'No',1=>'S&Iacute;');
		echo '<select id="htaccess_enabled" name="htaccess_enabled" style=\'width:90px;\' title="Control de acceso por htaccess">';
		for($n=0;$n<2;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->htaccess_enabled==$n && isset($this->htaccess_enabled))
				echo 'selected=\'selected\'';
			echo ' >'.$arraEsActivo[$n].'</option>';
		}
		echo '</select>';
?>

	</td>
  </tr>
  <tr>
	<td colspan="10" width="400">
	  <br />
<?php
	  if($this->htaccess_enabled)
	  {
		echo '<hr />Actualmente el back-office está protegido por HTACCESS, la seguridad es óptima.<hr />';
	  }
	  else
	  {
		if($_REQUEST['operacionEfectuada'] == 6)
		  echo '<hr /><span style="color:'.$_SESSION['colorOverImportant'].'">NO se han podido crear/modificar los archivos de seguridad, esta función no está habilitada.<br /><br />Para solucionar el problema póngase en contacto con el administrador o responsable del sitio.</span>';
		else
		  echo '<span style="color:'.$_SESSION['colorOverImportant'].'">Actualmente el back-office no está protegido por HTACCESS.</span> Si se desea que la herramienta de administración tenga una protección más elevada, debe activar esta opción si tiene los permisos adecuados para escribir en el hosting donde está alojada esta misma herramienta.';
	  }

?>
	  <br /><br />
	</td>
  </tr>
  <tr>
	<td colspan="10" width="400"><div id="mesajes_formulario"></div>
	</td>
  </tr>

<?php
	}

//******************************************************
//	OBTENER EL NIVEL MAXIMO DE PERMISO DE ADMINISTRACION
//******************************************************
	function obtenerNivelMaxAcceso()
	{
	  $query='SELECT MAX(nivel_acceso) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE activo';
	  $res=$this->sql->valor($query);
	  return $res;
	}

//******************************************************
//	OBJETOS PARA CONSTRUIR LOS RECUADROS CON LOS DATOS DINÁMICOS DE LA WEB
//******************************************************
	function xxxxxxxxxxx()
	{
echo $query;
		return;
	}

//******************************************************
//	COMPRUBA SI ES POSIBLE ESCRIBIR EN EL SITE
//******************************************************
  function verificarEscribible($file)
  {
	if(strtolower(substr(PHP_OS, 0, 3)) === 'win')
	{
	  if(file_exists($file))
	  {
		$file = realpath($file);
		if(is_dir($file))
		{
		  $result = @tempnam($file, 'mft');
		  if (is_string($result) && file_exists($result))
		  {
			unlink($result);
			return (strpos($result, $file) === 0) ? true : false;
		  }
		}
		else
		{
		  $handle = @fopen($file, 'r+');
		  if (is_resource($handle))
		  {
			fclose($handle);
			return true;
		  }
		}
	  }
	  else
	  {
		$dir = dirname($file);
		if (file_exists($dir) && is_dir($dir) && $this->verificarEscribible($dir))
		{
		  return true;
		}
	  }
	  return false;
	}
	else
	{
	  return is_writable($file);
	}
  }

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca='', $destinoedita='', $filtros='',$colFore='')
	{
	  return;
	}

}
?>