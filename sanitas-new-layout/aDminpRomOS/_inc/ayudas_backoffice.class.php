<?php
class ayudas_backoffice extends Base
{
	//Datos propios de la clase
	public $id;
	public $referencia_pagina;
	public $nombreTabla;
	public $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'ayudas_backoffice';
		$this->crearVariablesBusquedas($this->posicionSolapa);	// se crean las variables para las búsquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
	}

//******************************************************
//	CARAGAR UN DETERMINADO REGISTRO ($id)
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->referencia_pagina=$this->output_datos($a['referencia_pagina']);
		$this->texto=$this->output_datos(utf8_encode($a['texto']));
		return;
	}

//******************************************************
//	ACTUALIZAR LISTA DE SCRIPTS CON AYUDAS
//******************************************************
	function actualizarListadoScriptsAyudas()
	{
		unset($_SESSION['help_script_name']);
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.'';
		$res=$this->sql->query($query);

		while($arra=$this->sql->fila2($res))
			$_SESSION['help_script_name'][]=$arra['referencia_pagina'];

		$_SESSION['help_script_name']=array_unique($_SESSION['help_script_name']);
		return;
	}

//******************************************************
//	ELIMINACIÓN DE REGISTROS CON CONTROL DE DATOS RELACIONADOS DE OTRAS TABLAS AL MISMO
//******************************************************
	function eliminar_registro()
	{
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);
		$this->actualizarListadoScriptsAyudas();
		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
//		$this->verificarDuplicados($this->referencia_pagina, $this->nombreTabla, 'referencia_pagina', 'id_nivel_acceso',1);	// VERIFICACIÓN DE DATO DUPLICADO

		if(!$this->id) $this->id=0; // NECESARIO PARA AÑADIR NUEVOS REGISTROS

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla." (id,referencia_pagina,texto) VALUES ('".$this->id."', '".$this->referencia_pagina."', '".str_replace("\r\n",'',utf8_decode($this->texto))."')";
//echo'<pre>';print_r($_REQUEST);die();
		$this->sql->query($query);
		$this->actualizarListadoScriptsAyudas();
	}

//******************************************************
//	EDICION EN MODAL DE UN CAMPO ESPECÍFICO
//******************************************************
	function modal_directHTML($recordId,$fieldNameBBDD='*',$fieldName='')
	{
		$query='SELECT '.$fieldNameBBDD.' FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.(int)$recordId.'"';
		$dataToEdit=$this->sql->valor($query);
?>
<textarea name="tmpDataEdit" id="tmpDataEdit" class="directEditField"><?=utf8_encode($dataToEdit)?></textarea>
<?php
	}

//******************************************************
//
//******************************************************
	function formulario_datos($nombreForm)
	{
		$dataPopline=str_replace("\r\n",'',($this->texto));
		$dataPopline=html_entity_decode($dataPopline);
		$dataPopline=htmlspecialchars_decode($dataPopline);
//		$this->texto=utf8_decode($this->texto);
?>
<tr valign="top">
	<td title="Por ejemplo my_script.php" width="80">Nombre página:</td>
	<td><input type='text' id='referencia_pagina' name='referencia_pagina' value='<?=$this->referencia_pagina?>' style='width:160px'></td>
	<td align="right" width="40">Texto:<br />
		<a id="innerEdit" class='btn ajax' href="editor/editorColorBox.php?tbl=<?=$this->nombreTabla?>&nombreFormulario=<?=$nombreForm?>&idRegistro=<?=$this->id?><?='&innerTextHeight='.$this->innerTextHeight?>&nombreCampo=data_texto" title="Editor completo">HTML</a>
		<span class="<?=$this->id ? '' : ''?>"><br /><a title="Editar directamente el código <b>HTML</b>" class='btn ajax directHTML tooltip' href="<?=basename($_SERVER['PHP_SELF'])?>?id=<?=$this->id?>&accion=directHTML&fieldName=data_texto">Code:</a></span>
	</td>
	<td>
		<div id="data_texto" class='editorPopline' contenteditable='true' style="display:block;border:1px solid #999;width:<?=$this->id ? '90%' : '440px';?>;height:<?=$this->id ? '280px' : '100px';?>;overflow:auto;padding:4px;"><?=$dataPopline?></div>
		<textarea id='texto' name='texto' style='display:none;' ><?=$dataPopline?></textarea>
	</td>
</tr>
<?php
		if($this->id) echo '<tr><td height="5"></td></tr>';
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
$(function(){
	//editor jQuery color box
	$(".editAjax").colorbox({
		opacity: 0.4,
		initialHeight:"10px",
		initialWidth:"10%",
		close:true,
		overlayClose:true,
		width:"<?=$this->id ? 74 : 74 ?>%",
		height:"100%",
		iframe:true,
		html:true,
		innerHeight:true
	});
	$("#innerEdit").colorbox({
		opacity: 0.4,
		initialHeight:"10px",
		initialWidth:"10%",
		close:true,
		overlayClose:true,
		width:"95%",
		height:"100%",
		iframe:true,
		html:true,
		innerHeight:true
	});
});

function revisa()
{
	if (document.f<?=$idunico?>.referencia_pagina.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.referencia_pagina);
		alert('Por favor, escriba el nombre de la página');
		document.f<?=$idunico?>.referencia_pagina.focus();
		restauraCampo(document.f<?=$idunico?>.referencia_pagina);
		return;
	}

	popeditorSaveData("f<?=$idunico?>");
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}
-->
</script>
<?php
	include('_inc/form_std_edit.php');
	return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->referencia_pagina=$datos['referencia_pagina'];
		$this->texto=$datos['texto'];
		$this->guarda_datos();
		return;
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca,$destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='Número registros: ')
	{
		$mens.='</b>';
		if (count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la b&uacute;squeda:</span> ";

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID
//		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='referencia_pagina'; $ordenarHacia=' DESC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';

//******************************************************
//Formulario para efectuar búsquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<form action='<?=$destinobusca?>.php' method='post' name='formListados'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->

<table align='center' cellpadding='3' cellspacing='0' class='borde' width='780'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
</tr>
</thead>
<tr>
	<td width='80'>P&aacute;gina PHP:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>referencia_pagina_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'referencia_pagina_busca_u']?>' style='width:150px'></td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- *******************  fin formulario búsquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'85%','','88%','5%');

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='Nombre p&aacute;gina';
		$arraTitulares[]='Texto';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='referencia_pagina';
		$arraCamposOrdenar[]='texto';

		 // no se incluye el valor para el ID
		$arraLongitudes[]=30;
		$arraLongitudes[]=120;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=30;	// id
		$arraLongitudesTitulares[]=220;
		$arraLongitudesTitulares[]=454;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$xc=0;
			$contenido=str_replace('"','`',nl2br(strip_tags(utf8_encode($a['texto']))));
			$tmpArr[0]=$a['id'];
			array_push($tmpArr,$a['referencia_pagina']);
			array_push($tmpArr,$contenido);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
		}

		$idRefPops=0;
		$txtRefEliminar=1;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  FIN DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}

//******************************************************
//	DEVUELVE EL TEXTO CON LA AYUDA PARA EL MODULO SOLICITADO
//******************************************************
	function opbtenerTextoAyuda($datos='')
	{
		$pagina=$_REQUEST['nombre_pagina'];
		$titulo=str_replace('_',' ',$_REQUEST['titulo']);
		$query='SELECT texto FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE referencia_pagina="'.$datos['refHelp'].'.php"';
		$contenido=$this->sql->valor($query);

		if(!$contenido) $contenido='<br /><br /><br /><center><span style="color:#000;background-color:orange">&nbsp;&nbsp;ESTA PÁGINA NO TIENE NINGUNA AYUDA DEFINIDA&nbsp;&nbsp;</span><br /><br />Si necesita que esta ficha disponga de alguna explicación o ayuda,<br />por favor, póngase en contacto con el administrador de la Web.</center>';

		$contenido=nl2br($contenido);
		$outputHelp.='<table id="tblHlp" height="100%" width="100%" border="0" cellspacing="0" cellpadding="5" style="text-align:left;border:0px;background-color:">
	<tr height="25">
		<td style="text-align:left;border:0px;">Ayuda para el módulo de <b>"';
		$outputHelp.=$datos['helpTitle'];
		$outputHelp.='"</b><hr style="color:#aaa;height:1px" height="1" /></td>
	</tr>
	<tr>
		<td valign="top" style="border:0px;text-align:left;padding:4px;vertical-align:top;">';
		$outputHelp.=utf8_encode($contenido);
		$outputHelp.='</td>
	</tr>';
		$outputHelp.='</table>';
		echo $outputHelp;
/*
	$outputHelp.='<tr>
		<td align="center" height="35" style="border:0px;"><a id="closeHelp" href="#null" class="btn rounded" style="z-index:1000;">&nbsp;&nbsp;...Cerrar&nbsp;&nbsp;</a><br /><br /></td>
	</tr>';

*/
	}
}
?>
