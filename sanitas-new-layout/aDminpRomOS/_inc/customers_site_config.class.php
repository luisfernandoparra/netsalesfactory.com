<?php
class customersSiteConfig extends Base
{
	//Datos propios de la clase
	public $id;
	public $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEM�?S DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS
	public $linkmenu;

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'customers_site_config';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->getAdminLevel();	// RETRIEVE CONSTANTS LEVELS USER
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE ID="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->landing_identifier=$this->output_datos($a['landing_identifier']);
		$this->txt_title_site=$a['txt_title_site'];
		$this->is_enabled=$a['is_enabled'];
		$this->client_id=$a['client_id'];
		$this->url_base=$a['url_base'];
		$this->url_local=$a['url_local'];
		$this->url_root_app=$a['url_root_app'];
		$this->url_admin=$a['url_admin'];
		$this->root_path=$a['root_path'];
		$this->root_local_path=$a['root_local_path'];
		$this->root_local_includes_path=$a['root_local_includes_path'];
		return;
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(c.id) + IFNULL(mlt.is_enabled,0) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS mlt LEFT JOIN '.$this->sql->db.'.'.$this->sql->prefixTbl.'contenidos AS c ON c.id_niv_two=mlt.id WHERE mlt.id="'.(int)$this->id.'"';
		$existen_paginas=$this->sql->valor($query);
echo $query;die();
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.'(id, landing_identifier,txt_title_site,is_enabled,client_id, url_base, url_local, url_root_app, url_admin, root_path, root_local_path, root_local_includes_path) VALUES ("'.(int)$this->id.'", "'.trim($this->landing_identifier).'", "'.trim($this->txt_title_site).'","'.$this->is_enabled.'", "'.(int)$this->client_id.'","'.$this->url_base.'", "'.$this->url_local.'", "'.$this->url_root_app.'", "'.$this->url_admin.'", "'.$this->root_path.'", "'.$this->root_local_path.'", "'.$this->root_local_includes_path.'")';
//echo $query;die();
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos()
	{
?>
<tr>
	<td align="right" title="El cliente para esta landing">Cliente:</td>
	<td>
<?php
		$salida='';

 		$query='SELECT lc.id_landing, lc.prefix FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc WHERE lc.is_enabled ORDER BY lc.prefix';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id_landing'].'\'';
			if($this->client_id == $arra['id_landing']) $salida.=' selected="selected"';
			$salida.='>'.$arra['prefix'].'</option>';
		}

		echo '<select id="client_id" name="client_id" style=\'width:125px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
	<td align="right" class="tooltip" title="Texto identificador de la promoción <i>(landing)</i>">Identificador:</td>
	<td><input type='text' id='landing_identifier' name='landing_identifier' value='<?=$this->landing_identifier?>' style='width:120px'></td>
	<td align="right">Título del site:</td>
	<td><input type='text' id='txt_title_site' name='txt_title_site' value='<?=$this->txt_title_site?>' style='width:120px' class="useless" /></td>
	<td align="right" title="El sitio es activo?">Habilitado:</td>
	<td width="80">
<?php
		echo '<select id="is_enabled" name="is_enabled" style="width:44px;"';
		echo '<option value=-1 ';
		if($this->is_enabled==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		echo '<option value=0 ';
		if($this->is_enabled==0 && isset($this->is_enabled))
			echo 'selected=\'selected\'';
		echo '>NO</option>';

		echo '<option value=1 ';
		if($this->is_enabled==1)
			echo 'selected=\'selected\'';
		echo '>SI</option>';
		echo '</select>';
?>
	</td>
</tr>
<tr>
	<td align="right" class="tooltip" title="Url principal de la landing">Url base:</td>
	<td><input type='text' id='url_base' name='url_base' value='<?=$this->url_base?>' style='width:99%' class="useless" /></td>
	<td align="right" class="tooltip" title="Url <i>local</i> utilizada en la landing">Url local:</td>
	<td><input type='text' id='url_local' name='url_local' value='<?=$this->url_local?>' style='width:99%' class="useless" /></td>
	<td align="right" class="tooltip" title="???">Url root app:</td>
	<td><input type='text' id='url_root_app' name='url_root_app' value='<?=$this->url_root_app?>' style='width:99%;' class="useless" /></td>
	<td align="right" class="tooltip" title="???">Url admin:</td>
	<td width="80"><input type='text' id='url_admin' name='url_admin' value='<?=$this->url_admin?>' style='width:99%;' class="useless" /></td>
</tr>
<tr>
	<td align="right" class="tooltip" title="Root path">Root path:</td>
	<td><input type='text' id='root_path' name='root_path' value='<?=$this->root_path?>' style='width:99%' class="useless" /></td>
	<td align="right" class="tooltip" title="Root local path, sin backslah final <b>(no finalizar con ´/´)</b>.">root local path:</td>
	<td><input type='text' id='root_local_path' name='root_local_path' value='<?=$this->root_local_path?>' style='width:99%'></td>
	<td align="right" class="tooltip" title="Root local includes path, sin backslah final <b>(no finalizar con ´/´)</b>.">Root local includes:</td>
	<td><input type='text' id='root_local_includes_path' name='root_local_includes_path' value='<?=$this->root_local_includes_path?>' style='width:99%'></td>
</tr>
<?php
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
	if($('#client_id').val() < 0)
	{
		displayFormError("#client_id","Por favor, seleccione el cliente");
		return false;
	}
	if($.trim($('#landing_identifier').val()) == '')
	{
		displayFormError("#landing_identifier","Por favor, introduce un texto identificador para esta landing");
		$('#landing_identifier').val("");
		return;
	}
	if($.trim($('#txt_title_site').val()) == '')
	{
		displayFormError("#txt_title_site","Por favor, indicar el título del sitio");
		$('#txt_title_site').val("");
		return;
	}
/*
	err=controlNumerico(document.f<?=$idunico?>.orden.value,'orden','f<?=$idunico?>',1);
	if(err)
	{
		document.f<?=$idunico?>.orden.value='';
		return false;
	}
	if(document.f<?=$idunico?>.orden.value=='')
	{
		resaltarCampo(document.f<?=$idunico?>.orden);
		document.f<?=$idunico?>.orden.focus();
		alert('Por favor, indique la posicion del elemento del menu');
		restauraCampo(document.f<?=$idunico?>.orden);
		return;
	}
*/
	document.f<?=$idunico?>.submit();
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}
-->
</script>
<?php
		include('_inc/form_std_edit.php');
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->landing_identifier=$datos['landing_identifier'];
		$this->txt_title_site=$datos['txt_title_site'];
		$this->is_enabled=$datos['is_enabled'];
		$this->client_id=$datos['client_id'];
		$this->url_base=$datos['url_base'];
		$this->url_local=$datos['url_local'];
		$this->url_root_app=$datos['url_root_app'];
		$this->url_admin=$datos['url_admin'];
		$this->root_path=$datos['root_path'];
		$this->root_local_path=$datos['root_local_path'];
		$this->root_local_includes_path=$datos['root_local_includes_path'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='N&uacute;mero registros: ')
	{
		$idunico=rand(1,10000).time();
		//$query='SELECT lab.*,u.nivel_acceso FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 ';
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;
		//$querytotal='SELECT COUNT(lab.id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' AS lab LEFT JOIN '.$this->sql->db.'.usuarios AS u ON u.id=lab.id_usuario WHERE 1 '.$filtroSql;
		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='landing_identifier'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' ASC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;

		if($_SESSION['filtros']['visitantes_distintos_busca'])
		{
			$res2=$this->sql->query($querytotal);
			$num_res=mysql_num_rows($res2);
		}
		else
			$num_res=$this->sql->valor($querytotal);

		$res=$this->sql->query($query);
		$extra=$this->botonEliminarConFiltros($num_res);
		$mens=$extra?$extra:$mens;
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
//echo $query;

//******************************************************


//******************************************************
//Formulario para efectuar busquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
var idVisualizado=-1;
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->


<!-- START CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->
<table align='center' cellpadding='3' cellspacing='0' class='borde' width='878' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='12' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td title='Buscar por el <b>nombre</b> del cliente' width="60" align="right" class='tooltip'>Cliente:</td>
	<td>
<?php

		$query='SELECT lc.id_landing, lc.prefix FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc WHERE lc.is_enabled ORDER BY lc.prefix';
		$res2=$this->sql->query($query);

		$salida='<select id="'.$this->posicionSolapa.'client_id_buscN" name="'.$this->posicionSolapa.'client_id_buscN" style=\'width:108px;\'>';

		$salida.='<option value=-1 ';
		if($_SESSION['filtros'][$this->posicionSolapa.'client_id_buscN']==-1)
			$salida.='selected=\'selected\'';
		$salida.='>?</option>';

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id_landing'].'\'';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'client_id_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'client_id_buscN']==$arra['id_landing']) $salida.=' selected="selected"';
			$salida.='>'.$arra['prefix'].'</option>';
		}

		$salida.='</select>';
		echo $salida;
?>
	</td>

	<td width="48" align="right">Identificador:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>landing_identifier_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'landing_identifier_busca_u']?>' style='width:100px' /></td>

	<td width="68" align="right">T&iacute;tulo site:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>txt_title_site_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'txt_title_site_busca_u']?>' style='width:100px' /></td>
	<td title='' align="right">Activo:</td>
	<td>
<?php
		$esActivo=array(0=>'NO',1=>'S&iacute;');
		echo '<select id="'.$this->posicionSolapa.'is_enabled_buscN" name="'.$this->posicionSolapa.'is_enabled_buscN" style=\'width:50px;\' title="Nivel de acceso del usuario al Back-office">';
		echo '<option value=-1 ';
		if($this->is_enabled==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';

		for($n=0;$n < 2;$n++)
		{
			echo '<option value='.$n.' ';
			if($_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']==$n && isset($_SESSION['filtros'][$this->posicionSolapa.'is_enabled_buscN']))
				echo 'selected=\'selected\'';
			echo ' >'.$esActivo[$n].'</option>';
		}
		echo '</select>';
?>
	</td>

<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->


<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'34%','','75%','12%');

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='Cliente';
		$arraTitulares[]='Identificador';
		$arraTitulares[]='T&iacute;tulo site';
		$arraTitulares[]='Root local path';
		$arraTitulares[]='Activo';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='client_id';
		$arraCamposOrdenar[]='landing_identifier';
		$arraCamposOrdenar[]='txt_title_site';
		$arraCamposOrdenar[]='root_local_path';
		$arraCamposOrdenar[]='is_enabled';

		 // no se incluye el valor para el ID
		if(!$_SESSION['forcedLangSite']) $arraLongitudes[]=50;
		$arraLongitudes[]=64;
		$arraLongitudes[]=29;
		$arraLongitudes[]=29;
		$arraLongitudes[]=29;
		$arraLongitudes[]=60;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=25;	// id
		if(!$_SESSION['forcedLangSite']) $arraLongitudesTitulares[]=90;
		$arraLongitudesTitulares[]=140;
		$arraLongitudesTitulares[]=120;
		$arraLongitudesTitulares[]=170;
		$arraLongitudesTitulares[]=250;
		$arraLongitudesTitulares[]=40;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		if(!$_SESSION['forcedLangSite']) $arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='rop';

		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;
		$conutRowsDisplayed=0;
		$selectLabelLevelOne=$_SESSION['forcedLangSite'] ? 'mlo.landing_identifier AS nameMenu' : 'CONCAT(mlo.landing_identifier," (",i.prefijo,") AS nameMenu")';

		while($a=$this->sql->fila2($res))
		{
		  $rop=null;
			//	SOLO SE PERMITEN VER LOS ELEMENTOS PROPIOS Y LOS GLOBALES (EXCEPTO PARA GOOD)
		  if($_SESSION['id_usuario'] != $a['id_usuario'] && $a['id_usuario'] != 0 && $this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])
				$rop=1; // fila deshabilitada

			$xc=0; $estaActivo='<center style=color:orange>no</center>';
			$query='SELECT lc.prefix FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'landings_customers AS lc WHERE lc.id_landing='.(int)$a['client_id'];
			$reCustomer=$this->sql->valor($query);
			$reCustomer=$reCustomer ? $reCustomer : '<span style=color:red;>NO ASIGNADO</span>';

			if($a['is_enabled']==-1) $esActivo='?';
			if(!$a['is_enabled']) $esActivo='<span style=color:red>NO</span>';
			if($a['is_enabled']==1) $esActivo='SI';
			if($a['registrado']==1) $esRegistrado='<span style=color:red>SI</span>';
			if($a['registrado']<0) $esRegistrado='<span style=color:orange>?</span>';
			$query='SELECT CONCAT(nombre," ",apellidos) AS theName FROM '.$this->sql->db.'.'.$this->sql->prefixTbl.'usuarios WHERE id="'.$a['id_usuario'].'"';
//			$elIdioma=utf8_encode($this->sql->valor($query));
			$elIdioma=$elIdioma?$elIdioma:'<span style=color:red;><b>Todos</b></span>';

			if($a['is_enabled']) $estaActivo='<center style=color:lightgreen><b>SI</b></center>';

			$tmpArr[0]=$a['id'];
			array_push($tmpArr,$reCustomer);
			array_push($tmpArr,$a['landing_identifier']);
			array_push($tmpArr,$a['txt_title_site']);
			array_push($tmpArr,$a['root_local_path']);
			array_push($tmpArr,$esActivo);
		  array_push($tmpArr,$rop);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
			$conutRowsDisplayed++;
		}

		$idRefPops=0;
		$txtRefEliminar=3;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
