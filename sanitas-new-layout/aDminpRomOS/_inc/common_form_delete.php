<?php
@session_start();
if(!isset($_SERVER['HTTP_REFERER']))
{
	$hackerError=0;
	include_once('strange_errors.php');
	die();
}

if($_REQUEST['accion'] == 1)	// COMMON DELETE PROCESS
{
//	****** HABILITAR EN REAL ******
	$existen_registros=$objeto->eliminar_registro();
	$modeFinishModal=$modeFinishModal ? $modeFinishModal : 1;
	if($existen_registros)	// HAY DATOS RELACIONADOS PARA EL REGISTRO QUE SE INTENTA ELIMINAR
	{
		// CUSTOM MESSAGE FOR ERRORS
		if($objeto->additionalMessage)
		{
			echo '<br /><div style="padding:4px;color:'.$_SESSION['colorTextWarning'].';font-size:12px;text-align:center;">'.$objeto->additionalMessage.'<div>';
			$_REQUEST['logEstado']=$objeto->additionalTxtLog;
			echo '<script language="JavaScript" type="text/JavaScript">;';
			echo '$("#cboxTitle",window.parent.parent.document).html("<i>Proceso</i> <b>anulado</b>");';
			echo '</script>';
			$errProcess=1;
		}
		else	// STANDARD ERROR FOR DELETE MESSAGE
		{
			$_REQUEST['logEstado']=$txtLogDeleteAbort.' ('.$existen_registros.' related records)';
			echo '<center style="color:'.$_SESSION['colorTextWarning'].';font-size:12px;"><br /><b>No se puede borrar este dato</b><br /><br />tiene restricciones o registros asociados '.$txtErrorAdicional.'(<span style=color:'.$_SESSION['forePeligro'].'>'.$existen_registros.'</span>)<br /><br /><span style=font-style:italic;color:'.$_SESSION['colorTableBorde'].'>NOTA: el registro que se desea eliminar no puede estar activo.</span></center>';
			echo '<script language="JavaScript" type="text/JavaScript">;';
			echo '$("#cboxTitle",window.parent.parent.document).html("<span style=color:red;>&#8212;&#8212; Proceso <b>INTERRUMPIDO</b> &#8212;&#8212;</span>");';
			echo '</script>';
			$errProcess=2;
		}
		new Log($sql, $id_pagina);
	}
	else
	{
		$etraOptionDelete=$_REQUEST['actualizarWeb'] ? '&accion='.$_REQUEST['actualizarWeb'] : '';
		$etraOptionDelete.=$objeto->mapRefreshData ? '&mapRefreshData='.$objeto->mapRefreshData : '';	// IFRAMES UPDATE LIST
		$etraOptionDelete.=$objeto->id_archived ? '' : '&skipUpdateFrameReviews=1';	// IS A TRANSACTION FOR NEW LICENSE UNFINISHED, NOT ISSUED
		$etraOptionDelete.=$transactionDeleted ? '&updateTransactionsList=1' : '';
		echo $txtSuccessDisplay ? $txtSuccessDisplay : '<br /><center>finalizando...</center>';
		echo $txtSuccessJs;

		if($_REQUEST['isModal'])	// FLAG TO DELETE/MARK ACTUAL RECORD
			$preScript='parent.';
//echo $_SERVER['HTTP_REFERER'].'<h3>ELIM</h3><pre>';print_r($_REQUEST);print_r($objeto);die();

		echo '<script language="JavaScript" type="text/JavaScript">;';
		echo '
function endProcessDelete(modeOut){
	if(modeOut == 1)
	{
		var originalClose=$.colorbox.close=function(el){
			$.colorbox.close;
			if(el)
				setTimeout("parent.parent.location.href=\'"+el+"\';",1000);
			delete originalClose;
			return true;
		}
		originalClose("'.$_REQUEST['scriptExec'].'.php?posicionSolapa='.$_REQUEST['posicionSolapa'].$etraOptionDelete.'&msg='.$txtMsgEndProcess.'");
	}
	if(modeOut == 2)
	{
		parent.parent.'.$preScript.'location.href="'.$_REQUEST['scriptExec'].'.php?posicionSolapa='.$_REQUEST['posicionSolapa'].$etraOptionDelete.'&msg='.$txtMsgEndProcess.'";
	}
	$("#cboxTitle",window.parent.parent.document).html("<i>Proceso finalizado con &eacute;xito</b>");
}
';
	echo 'setTimeout("endProcessDelete('.$modeFinishModal.');",1000);';
	echo '</script>';
//window.opener.'.$preScript.'location.href="'.$nombreFilePhpBase.'.php?posicionSolapa='.$_REQUEST['posicionSolapa'].$etraOptionDelete.'&msg='.$txtMsgEndProcess.'";

		$_REQUEST['logEstado']=$txtLogDeleteOK;
		$_REQUEST['id_grabar']=$_REQUEST['id'];
		new Log($sql,$id_pagina);
		die();
	}
}

if($errProcess)
{
	$objeto->ObtenerNavegador();
?>
<script language="JavaScript" type="text/JavaScript">
window.parent.$("#titDeleteProcess").html("<?=$titDeleteProcess ? $titDeleteProcess : 'proceso anulado'?>");
window.parent.$("#titDeleteProcess").css("color","orange");
window.parent.$("#titDeleteProcess").css("font-weight","normal");
<?php
if($objeto->nameNavegador == 'msie')
	echo 'alert("AVISO: no ha sido posible efectuar esta operación");'.$preScript.'parent.parent.modalClose();';
?>
//
</script>
<?php
}
?>
</head>
<body class='fondoPagina'>
<center><br/><?=$msg?><br/>
<a href="#null" class="btn rounded" onClick="<?=$preScript?>parent.parent.modalClose();return true;//parent.parent.$.fn.colorbox.close();" style="padding-left:10px;padding-right:10px;">Cerrar la ventana</a>
</center>
</body>
</html>