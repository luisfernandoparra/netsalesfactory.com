<?php
class usersIps extends Base
{
	//Datos propios de la clase
	public $id;
	public $nombre;
	public $nombreTabla;
	public $tamanioMaximoImagenes;
	public $path_imagenes;
	public $posicionSolapa;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS DE SU MISMA SECCIÓN E IDENTIFICADOR PARA LOS CAMPOS DE LAS BÚSQUEDAS
	public $anchoMaximoImagenes;
	public $path_plug_ins_admin;

//******************************************************
//	INICIALIZACIÓN DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'users_ips';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las búsquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset'] == 1)
		{
			unset($_SESSION['filtros']['offset_u']);
			unset($_SESSION['filtros']);
		}
		$this->getAdminLevel();	// RETRIEVE CONSTANTS LEVELS USER
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->ip_cache=$a['ip_cache'];
		$this->id_usuario=$a['id_usuario'];
		return;
	}

//******************************************************
//	ELIMINACIÓN DE REGISTROS CON CONTROL DE DATOS RELACIONADOS DE OTRAS TABLAS U OTRAS SELECCIONES
//******************************************************
	function eliminar_registro()
	{
		$query='SELECT COUNT(id) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE is_enabled && id='.$_REQUEST['id'];
//		$existen_paginas=$this->sql->valor($query);

		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id='.$this->id;

		if(!$existen_paginas)	// SE ELIMINA EL REGISTRO SOLO SI NO HAY OTROS REGISTROS RELACIONADOS
			$this->sql->query($query);

		return $existen_paginas;
	}

//******************************************************
//	GUARDAR DATOS DE INSERCIONES Y EDICIONES
//******************************************************
	function guarda_datos()
	{
		$enEdicion=$this->id ? '_editar' : '';
		if(!$this->id) $this->id=0; // NECESARIO PARA AÑADIR NUEVOS REGISTROS

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id, id_usuario, ip_cache) VALUES ("'.$this->id.'", "'.(int)$this->id_usuario.'", "'.utf8_decode($this->ip_cache).'")';

		if(!$_img && $this->id)
		$query='UPDATE '.$this->sql->db.'.'.$this->nombreTabla.' SET id_usuario="'.(int)$this->id_usuario.'", ip_cache="'.utf8_decode($this->ip_cache).'" WHERE id='.(int)$this->id;

//echo $query.'<br /><pre>';print_r($_REQUEST);print_r($this);exit;
		$this->sql->query($query);
		return;
	}


//******************************************************
//	FORMULARIO DE EDICIÓN /INSERCIÓN DE DATOS
//******************************************************
	function formulario_datos($nombreForm=0)
	{
//		$this->PhpEditarHTML($this->nombreTabla,$nombreForm,$this->id);
		$arraSiNo=array('No','S&iacute;');
?>
<tr>
	<td title='Indicar el tipo del m&oacute;dulo' align="right">Tipo del m&oacute;dulo:</td>
	<td>
<?php
		$salida='';
		$query='SELECT id, CONCAT(nombre," ",apellidos) AS theName,nivel_acceso FROM '.$this->sql->db.'.usuarios WHERE activo=1 ORDER BY nombre';
		$res2=$this->sql->query($query);
		$salida.='<option value="0"';

		if(isset($this->id_usuario) && $this->id_usuario == 0) $salida.=' selected="selected" ';
		$salida.='>Global</option>';

		while($arra=$this->sql->fila2($res2))
		{
			//SE OMITEN OTROS USUARIOS CON NIVEL SUPERIOR DE ACCESO AL DEL USUARIO ACTUAL
			if($_SESSION['usuarioNivel'] < $arra['nivel_acceso'])	continue;

			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->id_usuario == $arra['id']) $salida.=' selected="selected" ';
			$salida.='>'.utf8_encode($arra['theName']).'</option>';
		}

		echo '<select id="id_usuario" name="id_usuario" style=\'width:100%\' ><option value="-1" class="tx10" style="color:#666">?</option>';
		echo $salida;
		echo '</select>';
?>
		</td>
		<td width="80" title="Ip del usuario a cachear" align="right">Ip a cachear:</td>
		<td><input type='text' id='ip_cache' name='ip_cache' value='<?=$this->ip_cache?>' maxlength="255" style='width:98px'/></td>
  </tr>

<?php
		if($this->id) echo '<tr><td height="10"></td></tr>';
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
$(document).ready(function(){
	$('#ip_cache').mask('0ZZ.0ZZ.0ZZ.0ZZ', {translation: {'Z': {pattern: /[0-9]/, optional: true}}});
  $('#ip_cache').mask('099.099.099.099');
});
function revisa()
{
	if(!trim($('#ip_cache').val()))
	{
		resaltarCampo($("#ip_cache")[0]);
		$("#ip_cache").focus();
		alert('Es obligatorio indicar el nombre del módulo');
		$("#ip_cache").focus();
		restauraCampo($("#ip_cache")[0]);
		return false;
	}
	if($('#id_usuario').val() < 0)
	{
		resaltarCampo($("#id_usuario")[0]);
		$("#id_usuario").focus();
		alert('Por favor, seleccione un usuario');
		$("#id_usuario").focus();
		restauraCampo($("#id_usuario")[0]);
		return;
	}

	document.f<?=$idunico?>.submit();
}

-->
</script>
<?php
		$camposAdicionales='<input type="hidden" id="ref_desplegable" name="ref_desplegable" value="">
';
		include('_inc/form_std_edit.php');
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->ip_cache=$datos['ip_cache'];
		$this->id_usuario=$datos['id_usuario'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	public function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='Número registros: ')
	{
//		$mens.='</b>';
		if(count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la búsqueda:</span> ";

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='id_usuario'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' DESC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.(int)$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';

//******************************************************
//Formulario para efectuar búsquedas
//******************************************************
	$colorTextoMsg=$_SESSION['colorTextWarning'];
	if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>

<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>
<form action='<?=$destinobusca?>.php' method='post' name='formListados'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACIÓN DE LA LISTA DE RESULTADOS -->

<table align='center' cellpadding='3' cellspacing='0' class='borde' width='500' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
<?php
		echo '<td width="60" title="Buscar por usuarios">Del usuario:</td>
			<td>';
		$salida='';
		$query='SELECT id, CONCAT(nombre," ",apellidos) AS theName, nivel_acceso FROM '.$this->sql->db.'.usuarios WHERE activo=1 ORDER BY nombre';
		$res2=$this->sql->query($query);
		$salida.='<option value="0"';

		if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN'] == 0) $salida.=' selected="selected" ';
		$salida.='>Global</option>';

		while($arra=$this->sql->fila2($res2))
		{
			//SE OMITEN OTROS USUARIOS CON NIVEL SUPERIOR DE ACCESO AL DEL USUARIO ACTUAL
			if($_SESSION['usuarioNivel'] < $arra['nivel_acceso'])	continue;

			$salida.='<option value=\''.$arra['id'].'\'';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_usuario_buscN'] == $arra['id'])
				$salida.=' selected="selected"';
			$salida.='>'.utf8_encode($arra['theName']).'</option>';
		}

		echo '<select id="'.$this->posicionSolapa.'id_usuario_buscN" name="'.$this->posicionSolapa.'id_usuario_buscN" style=\'width:90px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select></td>';
?>
	<td width="20">IP:</td>
	<td><input type='text' name='<?=$this->posicionSolapa?>ip_cache_busca_u' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'ip_cache_busca_u']?>' style='width:80px'></td>

<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- END CAMPOS PARA BUSQUEDAS EN LA LISTA DE RESULTADOS -->

<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,$this->modalHeight,'',$this->modalWidth,$this->modalLeftPos);
//		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'55%','','94%','3%');	//DATOS PARA LA POP INCRUSTADA

		/*******************    EL LISTADO    ********************/
		// TITULARES DEL LISTADO A MOSTRAR (se omite el id)
		$arraTitulares[]='Nombre usuario';
		$arraTitulares[]='IP-caheada';

		// NOMBRES DE LOS CAMPOS
		$arraCamposOrdenar[]='id';
		$arraCamposOrdenar[]='is_usario';
		$arraCamposOrdenar[]='ip_cache';

		 // no se incluye el valor para el ID
		$arraLongitudes[]=40;
		$arraLongitudes[]=15;

		// longitudes de las cajas a dibujar
		$arraLongitudesTitulares[]=40;
		$arraLongitudesTitulares[]=300;
		$arraLongitudesTitulares[]=60;

		// tipo de datos a mostrar
		$arraTipos[]='num';
		$arraTipos[]='txt';
		$arraTipos[]='txt';
		$arraTipos[]='rop';

		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$rop=null;
			//	SOLO SE PERMITEN VER LOS ELEMENTOS PROPIOS Y LOS GLOBALES (EXCEPTO PARA GOOD)
			if($_SESSION['id_usuario'] != $a['id_usuario'] && $a['id_usuario'] != 0 && $this->levelAccessMaxLevel != $_SESSION['usuarioNivel'])
				$rop=1; // fila deshabilitada

			$query='SELECT CONCAT(nombre," ",apellidos) AS theName FROM '.$this->sql->db.'.usuarios WHERE id="'.$a['id_usuario'].'"';
			$elUsuario=utf8_encode($this->sql->valor($query));
			$elUsuario=$elUsuario?$elUsuario:'<span style=color:red;><b>Todos</b></span>';

			$tmpArr[0]=$a['id'];
			array_push($tmpArr,$elUsuario);
			array_push($tmpArr,($a['ip_cache']));
			array_push($tmpArr,$rop);
			$arraDatos[]=$tmpArr;
			unset($tmpArr);
		}

		$idRefPops=0; $txtRefEliminar=1; $ocultarEliminar=0;
		$posicFondo=0;	// NÚMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo,$ocultarEliminar);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>
