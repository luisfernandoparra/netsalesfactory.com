<?php
class IdiomasMeses extends Base
{
	//Datos propios de la clase
	var $id;
	var $id_pais;
	var $nombre;
	var $usuario;
	var $nombre_mes;

//******************************************************
//	INICIALIZACION DE LA CLASE
//******************************************************
	function __construct($sql,$posicionSolapa)
	{
		$this->sql=$sql;
		$this->nombreTabla=$this->sql->prefixTbl.'idiomas_meses';
		$this->posicionSolapa=$posicionSolapa;
		$this->crearVariablesBusquedas($posicionSolapa);	// se crean las variables para las busquedas
		if($_REQUEST['offset_u']!='') $_SESSION['filtros']['offset_u']=$_REQUEST['offset_u'];
		if($_REQUEST['reset']==1) unset($_SESSION['filtros']);
	}

//******************************************************
//
//******************************************************
	function carga($id)
	{
		$this->id=$id;
		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE ID="'.$this->id.'"';
		$res=$this->sql->query($query);
		$a=$this->sql->fila2($res);
		$this->nombre_mes=$this->output_datos($a['nombre_mes']);
		$this->num_mes=$a['num_mes'];
		$this->id_idioma=$a['id_idioma'];
	}

//******************************************************
//
//******************************************************
	function eliminar_registro()
	{
		$query='DELETE FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id="'.$this->id.'"';
//echo $query;exit;
		$this->sql->query($query);
		return;
	}

//******************************************************
//
//******************************************************
	function guarda_datos()
	{
		if(!$this->id)
			$this->id=0;
		else
			$filtroActualizacion=' AND id!='.$this->id;

		$query='SELECT nombre_mes FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE id_idioma="'.$this->id_idioma.'" AND num_mes="'.$this->num_mes.'"'.$filtroActualizacion.' LIMIT 1';
		$existeRegistro=$this->sql->valor($query);

		if($existeRegistro)
		{
			echo '<table height="100%" width="100%" border="0" cellspacing="0" cellpadding="0" class="textNormal">
  <tr align="center" height="20%">
    <td style="color:yellow"><b>NO SE PERMITE CONTINUAR CON LA CREACI&Oacute;N / MODIFICACI&Oacute;N DE ESTE REGISTRO</b></td>
  </tr>
  <tr align="center" height="40%">
    <td>El mes que deseaba guardar <u>con el nombre <b><span style="color:orange">"'.$this->nombre_mes.'"</span></b></u> ya lo existe como <h2 style="color:orange">"'.$existeRegistro.'"</h2>.</td>
  </tr>
  <tr align="center" height="20%">
    <td><b>NOTA:</b> No se permiten duplicados.</td>
  </tr>
  <tr align="center" height="40%">
    <td><a href="#null" class="btn" style="line-height:30px" onclick="volverFormulario();">&nbsp;&nbsp;Volver&nbsp;&nbsp;</a></td>
  </tr>
</table>
<script>
function volverFormulario()
{
';
		if($this->id) echo 'window.history.back();';
		else echo 'document.location="'.$this->nombreTabla.'.php'.$cadena.'";';
	echo '
}
</script>
';

			exit;
		}

		$query='REPLACE INTO '.$this->sql->db.'.'.$this->nombreTabla.' (id,nombre_mes,num_mes,id_idioma) VALUES ("'.$this->id.'", "'.$this->nombre_mes.'", "'.$this->num_mes.'","'.$this->id_idioma.'")';
//echo $query;exit;
		$this->sql->query($query);
	}

//******************************************************
//
//******************************************************
	function formulario_datos($nombreForm=0)
	{
?>
<tr>
	<td align="right">Nombre del mes:</td>
	<td><input type='nombre_mes' id='nombre_mes' name='nombre_mes' value='<?=$this->nombre_mes?>' style='width:60px'></td>
	<td title='N&uacute;mero del mes correspondiente' align="right">N&uacute;m mes:</td>
	<td title='N&uacute;mero del mes correspondiente'>
<?php
		echo '<select id="num_mes" name="num_mes" style=\'width:50px;\' title="N&uacute;mero del mes correspondiente">';
		echo '<option value=-1 ';
		if($this->num_mes==-1)
			echo 'selected=\'selected\'';
		echo '>?</option>';
		for($n=1;$n<13;$n++)
		{
			echo '<option value='.$n.' ';
			if($this->num_mes==$n)
				echo 'selected=\'selected\'';
			echo ' >'.$n.'</option>';
		}
		echo '</select>';
?>
	</td>
	<td align="right">Idioma:</td>
	<td>
<?php
		// START SE CONTRUYE EL ARRAY CON TODOS LOS ELEMENTOS DE MENUS 
		$salida='';
		$query='SELECT * FROM '.$this->sql->db.'.idiomas ORDER BY idioma';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if($this->id_idioma== $arra['id']) $salida.=' selected="selected"';
			$salida.='>'.$arra['idioma'].'</option>';
		}

		echo '<select id="id_idioma" name="id_idioma" style=\'width:110px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
</tr>
<?php
		if($this->id) echo '<tr><td height="25"></td></tr>';
	}

//******************************************************
//
//******************************************************
	function form_editar_datos($destino,$accion,$titulo)
	{
		$idunico=rand(1,10000).time();
?>
<script language="JavaScript" type="text/javascript">
<!--
function revisa()
{
	if(!document.f<?=$idunico?>.nombre_mes.value)
	{
		resaltarCampo(document.f<?=$idunico?>.nombre_mes);
		document.f<?=$idunico?>.nombre_mes.focus();
		alert('Por favor, escriba el nombre del mes');
		restauraCampo(document.f<?=$idunico?>.nombre_mes);
		return;
	}
	if(document.f<?=$idunico?>.num_mes.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.num_mes);
		document.f<?=$idunico?>.num_mes.focus();
		alert('Por favor, seleccione el numero del mes');
		restauraCampo(document.f<?=$idunico?>.num_mes);
		return;
	}
	if(document.f<?=$idunico?>.id_idioma.value<0)
	{
		resaltarCampo(document.f<?=$idunico?>.id_idioma);
		document.f<?=$idunico?>.id_idioma.focus();
		alert('Por favor, seleccione el idioma');
		restauraCampo(document.f<?=$idunico?>.id_idioma);
		return;
	}

	document.f<?=$idunico?>.submit();
}

function im(nombre,ancho,alto)
{
	var a=screen.width;
	var b=screen.height;
	var x=(a-ancho)/2;
	var y=(b-alto)/2;
	var win=window.open ('','archivo','left='+x+',top='+y+',width='+ancho+',height='+alto);
	win.document.write("<html><head><title>Imagen</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><img src='"+nombre+"'></body></html>");
}
-->
</script>
<?php
		include('_inc/form_std_edit.php');
		return $idunico;
	}

//******************************************************
//
//******************************************************
	function form_editar_datos_procesa($datos)
	{
		$this->id=$datos['id'];
		$this->nombre_mes=$datos['nombre_mes'];
		$this->num_mes=$datos['num_mes'];
		$this->id_idioma=$datos['id_idioma'];
		$this->guarda_datos();
	}

//******************************************************
//
//******************************************************
	function listar_datos($destinobusca, $destinoedita,$colPaginacionActivoFondo='#fff',$colBorde='#111',$textoTitularBuscador='',$mens='Número registros: ')
	{
		$idunico=rand(1,10000).time();
		if (count($_SESSION['filtros'])>1)	$mens="<span style='color:".$_SESSION['forePeligro']."'>Resultados de la búsqueda:</span> ";

		$query='SELECT * FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 ';

		$filtroSql=$this->crearFiltrosBusquedas($this->posicionSolapa);	// los campos han de finalizar con"_busca" para las strings y _buscN para los numeros e ID

		$_SESSION['filtros']['offset_u']*=1;

		$querytotal='SELECT COUNT(*) FROM '.$this->sql->db.'.'.$this->nombreTabla.' WHERE 1 '.$filtroSql;

		$ordenacion='id_idioma,num_mes'; $ordenarHacia=' ASC';
		if($_REQUEST['ordenarPor'])
			$ordenacion=substr($_REQUEST['ordenarPor'],8);

		if($_REQUEST['sentidoOrdenacion'])
			$ordenarHacia=' DESC';

		$query.=$filtroSql.' ORDER BY '.$ordenacion.$ordenarHacia.' LIMIT '.$_SESSION['filtros']['offset_u'].', '.$this->filasporpagina;
		$num_res=$this->sql->valor($querytotal);
		$res=$this->sql->query($query);
		$mens.='<b>'.number_format($num_res,0,',','.').'</b>';
		//******************************************************

		//******************************************************
		//Formulario para efectuar busquedas
		//******************************************************
		$colorTextoMsg=$_SESSION['colorTextWarning'];
		if($_REQUEST['colorError']) $colorTextoMsg=$_REQUEST['colorError'];
?>
<script language="JavaScript" type="text/javascript">
<!--
var idVisualizado=-1;
-->
</script>

<form action='<?=$destinobusca?>.php' method='post' name='formListados<?=$idunico?>'>
<!-- INICIO CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->
<input type="hidden" name="posicionSolapa" value="<?=($this->posicionSolapa)?$this->posicionSolapa:$_REQUEST['posicionSolapa']?>" />
<input type="hidden" name="ordenarPor" value="<?=$_REQUEST['ordenarPor']?>" />
<input type="hidden" name="sentidoOrdenacion" value="<?=$_REQUEST['sentidoOrdenacion']?>" />
<input type="hidden" id="offset_u" name="offset_u" value="<?=$_REQUEST['offset_u']?>" />
<!-- FIN CAMPOS NECESARIOS PARA ORDENACION DE LA LISTA DE RESULTADOS -->

<table align='center' cellpadding='3' cellspacing='0' class='borde' width='550' border='0'>
<thead>
<tr>
	<td class='enc2' colspan='2'><?=$textoTitularBuscador?></td>
	<td class='enc2' colspan='10' style='padding-right:9px;text-align:right;font-weight:normal;color:<?=$colorTextoMsg?>' id='mesajes_formulario'><?=$mens?></td>
<tr>
</thead>
<tr>
	<td>Nombre del mes:</td>
	<td><input type='nombre_mes' name='<?=$this->posicionSolapa?>nombre_mes_busca' value='<?=$_SESSION['filtros'][$this->posicionSolapa.'nombre_mes_busca']?>' style='width:70px'></td>
	<td align="right">Idioma:</td>
	<td>
<?php
		// START SE CONTRUYE EL ARRAY CON TODOS LOS ELEMENTOS DE MENUS 
		$salida='';
		$query='SELECT * FROM '.$this->sql->db.'.idiomas ORDER BY idioma';
		$res2=$this->sql->query($query);

		while($arra=$this->sql->fila2($res2))
		{
			$salida.='<option value=\''.$arra['id'].'\'';
			if(isset($_SESSION['filtros'][$this->posicionSolapa.'id_idioma_buscN']) && $_SESSION['filtros'][$this->posicionSolapa.'id_idioma_buscN']==$arra['id']) $salida.=' selected="selected"';
			$salida.='>'.$arra['idioma'].'</option>';
		}

		echo '<select id="'.$this->posicionSolapa.'id_idioma_buscN" name="'.$this->posicionSolapa.'id_idioma_buscN" style=\'width:90px\' ><option value="-1" class="tx10" style="color:#999">seleccionar</option>';
		echo $salida;
		echo '</select>';
?>
  </td>
	<td title='N&uacute;mero del mes correspondiente' align="right">N&uacute;mero del mes:</td>
	<td title='N&uacute;mero del mes correspondiente'>
<?php
		$salida='<select id="'.$this->posicionSolapa.'num_mes_buscN" name="'.$this->posicionSolapa.'num_mes_buscN" style=\'width:50px;\' title="N&uacute;mero del mes correspondiente">';
		$salida.='<option value=-1 ';
		if($this->num_mes==-1)
			$salida.='selected=\'selected\'';
		$salida.='>?</option>';

		for($n=1;$n<13;$n++)
		{
			$salida.='<option value='.$n.' ';

			if($_SESSION['filtros'][$this->posicionSolapa.'num_mes_buscN']==$n && isset($_SESSION['filtros'][$this->posicionSolapa.'num_mes_buscN']))
				$salida.=' selected="selected"';

			$salida.=' >'.$n.'</option>';
		}
		$salida.='</select>';
		echo $salida;
?>
	</td>
<?php
		include('common_btn_forms.html.php');
?>
</tr>
</table>
<!-- *******************  fin formulario busquedas  *************** !-->
<?php
		include('_inc/common_listed.php');
		$this->javascripts=new commonListed();
		$this->javascripts->javascriptFunctions($idunico,$destinobusca,'30%','','40%','30%');

		/*******************    DEL LISTADO    ********************/
		$arraTitulares=array('Idioma','Mes','N&uacute;mero mes');	// TITULARES DEL LISTADO A MOSTRAR
		$arraCamposOrdenar=array('id','id_idioma','nombre_mes','num_mes');
		$xc=0; $arraLongitudes=array($xc++=>40,$xc++=>40,$xc++=>29); // no se incluye el valor para el ID
		$xc=0; $arraLongitudesTitulares=array($xc++=>25,$xc++=>100,$xc++=>120,$xc++=>70);
		$xc=0; $arraTipos=array($xc++=>'num',$xc++=>'txt',$xc++=>'txt',$xc++=>'num');
		$this->verIdRegistros=1;
		$verEmergentesTextosFragmentados=1;
		$decimales=0;

		while($a=$this->sql->fila2($res))
		{
			$xc=0; $estaActivo='<center style=color:orange>no</center>';
			$query='SELECT idioma FROM '.$this->sql->db.'.idiomas WHERE id=\''.$a['id_idioma'].'\'';
			$nombreIdioma=ucwords($this->sql->valor($query));
			if(!$nombreIdioma) $nombreIdioma='<center><b>?</b></center>';

			if($a['activo']) $estaActivo='<center style=color:lightgreen><b>SI</b></center>';
			
			$arraDatos[]=array($xc++=>$a['id'],$xc++=>$nombreIdioma,$xc++=>$a['nombre_mes'],$xc++=>$a['num_mes']);
		}

		$idRefPops=0;
		$txtRefEliminar=2;
		$posicFondo=0;	// NUMERO DE COLUMNA PARA LA IMAGEN DE FONDO (0 = NINGUNA)
		echo $this->crea_lista_resultados($arraLongitudesTitulares, $arraTitulares, $arraDatos, $arraTipos, $arraCamposOrdenar, $arraLongitudes, $arraImgFondo, $txtRefEliminar, $this->verIdRegistros, $this->verEmergentesTextosFragmentados, $this->decimalesEnListado, $posicFondo);
		/*******************  END DEL LISTADO  ********************/
?>
</table>
<?php
		include('modules_footer.php');
?>
</form>
<?php
	}
}
?>