<?php
@session_start();
require_once('_inc/cabeceraadmin2011.php');
?>
<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/general.css" title="style"  media="screen"/>
<title>Untitled Document</title>
</head>
<body class="fondoPagina" id="bodyModulo">
-->
<?php
echo'<pre style="font-size:.80em;font-family:arial,Verdana,Geneva,sans-serif;line-height:1.4em;">';
?><center><span style="display:block;background-color:#fff;color:blue;width:80%">
DESCRIPCIONES PARA LA CONFIGURACIÓN DE LOS PLUG-INS DEL BACK-OFFICE

</span></center>
&#8226; <b>ELIMINACIÓN MASIVA DE REGISTROS</b>
1.0.1. En la clase base:
<dd>debe existir la siguiente función que es la que dibuja el botón necesario:
<b>function botonEliminarConFiltros</b>($num_res, $id_unico_form = "")<sup>(*1)</sup></dd>
1.0.2. En cada script base en la carpeta principal del ADMINISTRACIÓN:
<dd><b>$objeto->permiteEliminarGlobal = 1;</b><sup>(*2)</sup></dd>
1.0.3. En la query de la clase del listado:<sup>(*3)</sup>
<dd><b>$res = $this->sql->query($query);
$extra = $this->botonEliminarConFiltros($num_res, $idunico);
$mens = $extra?$extra:$mens;</b></dd>
1.0.4 Hay un control que advierte de la eliminación de los registros seleccionados
<dd><b>if($objeto->permiteEliminarGlobal)</b>...<sup>(*4)</sup></dd>
1.1.0 ARCHIVOS PROPIOS DEL PLUG-IN <i>(N)</i> / AFECTADOS <i>(A)</i>
<dd>- _inc/base3000.class.php <i>(A)</i>.<sup>(*1)</sup>
- todos los scripts `padre` <i>(A)</i>.<sup>(*2)</sup>
- _inc/todos los archivos de las clases de los scripts padres <i>(A)</i>.<sup>(*3)</sup>
- _inc/common_admin_script.php <i>(A)<sup>(*4)</sup></i>.</dd>
<?php
echo'</pre>';

include_once('footer.php');
?>
</body>
</html>
