﻿<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/menu_nivel1.class.php');
require_once('_inc/log.class.php');

$objeto=new MenuNivel1($sql, $admin);
$objeto->filasporpagina=10;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=20;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=1;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// NÚMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$objeto->editarFila=1;
$nombreFilePhpBase='menu_nivel1';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR

$txtTitularFormularioNewRecord='Insertar una solapa/link footer';
$txtTitularBuscador='Buscar solapas/links footer';
$txtMsgStandard='Solapas/links footer presentes: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';
$txtTitularEdicion='Editar menú nivel 1';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>
