<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require('_inc/cabeceraadmin.php');
if($_REQUEST['accion']==1) $_SESSION['nuevaBBDD']=1;
require('_cnf/connect2.php');
require('_inc/backup.class.php');
require('_inc/log.class.php');
$objeto=new backup($sql, $admin);

$objeto->path_archivos='../backup/';
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$nombreFilePhpBase='back_up';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$fraseTitularFormulario='Nueva copia de back-up';	// FRASE QUE APARECE EN EL ENCABEZADO DEL FORMULARIO
$txtTitularFormularioNewRecord='Back-up  de la base de datos';
//echo'avisos.php<pre>';print_r($_REQUEST);exit;

// LISTADO DE LAS TABLAS QUE SE DEBE OMITIR RECUPERAR
$objeto->tablasOmitidas=array(
'log_accesos_backoffice',
'log_web_actions2011',
'log_web_actions2012',
'log_web_actions2013',
'technical_log_office2012',
'technical_log_office2013',
'visitas',
'visitantes_activos',
'provincias'
);

if($_REQUEST['accion'] == 1)	// INICIO BACK-UP DE LA ENTERA BBDD
{

	if(!$_REQUEST['tablaBackUp'])	// NO SE SELECIONARION TABLAS
	{
		echo '<br /><br /><br /><center><b><h3 style="COLOR:#FF9900">No se seleccionaron tablas!!!</h3></b><br /><br /><a href="'.$nombreFilePhpBase.'.php?accion=0" class="btn" style="padding-left:40px;padding-right:40px;">volver a la pantalla anterior</a><br /></center>';
		$_REQUEST['logEstado']='ERROR; break back-up for '.$_REQUEST['nombre_bbdd'].' (no tables selected)';
		new Log($sql, $id_pagina);
		unset($_SESSION['nuevaBBDD']);
		return;
	}

	echo '<span id="sndMensajeFinalizando"><br /><br /><br /><center><b><h2 style="COLOR:red">FINALIZANDO LA TAREA DE BACK-UP</h2></b><br /><br /><br />por favor NO interrumpa este proceso...</center></span>';
	$objeto->ejecutarBackUp($_REQUEST);

	if($_REQUEST['accion']==-1)	// ERROR EN CARPETA DE BACK-UP
	{
		echo'<center><br /><br /><br /><a href="#null" class="btn" onclick="document.location=\'back_up.php?momento='.$_REQUEST['momento'].'\'" style="width:200px;" >Volver a la pantalla anterior</a></center>';
	}
	else
	{
		unset($_SESSION['nuevaBBDD']);
		echo '<script>setTimeout("document.location=\''.$nombreFilePhpBase.'.php?accion=2&nombreBase='.$_REQUEST['nombre_bbdd'].'&momento='.$_REQUEST['momento'].'&tablasOK='.(implode(',',$_REQUEST['tablaBackUp'])).'\'",1000);;</script>';
	}
}

if($_REQUEST['accion'] == 2)	// FIN BACK-UP DE LA ENTERA BBDD
{
	$_REQUEST['nombre_bbdd']=$_SESSION['OldBBDD'];
	echo '<br /><br /><br /><center><b><h3 style="color:'.$_SESSION['colorTextWarning'].'">BACK-UP FINALIZADO</h3></b><br /><br />para la BBDD:<h3>'.$_REQUEST['nombreBase'].'</h3>Se han respaldado las siguientes tablas:<br /><br /><div style="display:block;overflow:auto;width:80%;padding:10px;font-weight:bold;height:38px;">'.(str_replace(',',',&nbsp;',$_REQUEST['tablasOK'])).'</div><br /><br /><br /><a href="'.$nombreFilePhpBase.'.php?accion=0&nombre_bbdd=&momento='.$_REQUEST['momento'].'" style="width:240px" class="btn">volver a la pantalla anterior</a><br /><br /><br /><b>NOTA:</b> Recuerde que para realizar otra copia debes esperar más de un minuto de reloj desde ahora.';
	$_REQUEST['logEstado']='BACK-UP de '.$_REQUEST['nombreBase'].', for tables: '.$_REQUEST['tablasOK'];
	new Log($sql, $id_pagina);
	unset($_SESSION['nuevaBBDD']);
//echo'<pre>';print_r($_REQUEST);
}

?>
</head>
<body class='fondoPagina'>
<?php

if(!$_REQUEST['accion'])
{
	$_SESSION['OldBBDD']=$objeto->sql->db;
	$objeto->formularioEjecutarBackUp($nombreFilePhpBase.'.php','1',$fraseTitularFormulario);
}

include_once('_inc/help_button.class.php');
include_once('footer.php');
?>

<script type='text/javascript' src='_js/post.js'></script>
</body>
</html>