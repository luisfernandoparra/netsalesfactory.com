﻿<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/logs_ip_votations.class.php');

$objeto=new logsIpVotations($sql,$admin);
$objeto->filasporpagina=15;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=20;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=0;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// NÚMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$nombreFilePhpBase='logs_ip_votations';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$fraseTitularFormulario='Logs votaciones';	// FRASE QUE APARECE EN EL ENCABEZADO DEL FORMULARIO
$objeto->editarFila=0;	// SE OMITE LA EDICIÓN DE REGISTROS
$objeto->permiteEliminarGlobal=1;

$txtTitularFormularioNewRecord='Logs votaciones';
$txtTitularBuscador='Encontrar votaciones';
$txtMsgStandard='Votaciones presentes: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';
$txtTitularEdicion='';
$objeto->ocultarEliminar=$_SESSION['usuarioNivel'] > 15 ? 0 : 1;

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>