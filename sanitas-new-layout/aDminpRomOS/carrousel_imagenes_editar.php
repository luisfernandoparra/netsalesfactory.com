﻿<?php
include ('_inc/cabeceraadmin.php');
include ('_cnf/connect2.php');
require_once('_inc/carrousel_imagenes.class.php');
require_once('_inc/log.class.php');
$objeto=new CarrouselImagenes($sql, $admin);
$objeto->path_imagenes='../img_diapos/';
$objeto->carga($_REQUEST['id']);
$nombreFilePhpBase='carrousel_imagenes';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$objeto->nombreFilePhpBase=$nombreFilePhpBase;
$objeto->tamanioMaximoImagenes=200000;	// EN BYTES
$objeto->tamanioMaximoThubnail=50000;	// EN BYTES
$objeto->innerTextHeight=230; // altura innerTinyBoxEdit
$txtLogEditFinish='record stored';
$txtTitularEdicion='Editar elemento del carrousel';

include('_inc/common_form_edit.php');
?>
