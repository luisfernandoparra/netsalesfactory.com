<?php
@session_start();
include_once('_inc/cabeceraadmin2011.php');
include_once('_cnf/connect2.php');
include_once('_inc/log.class.php');
include_once('_inc/files_backoffice.class.php');
$objeto=new filesBackoffice($sql);

if($_REQUEST['accionSel']==1)
{
	echo '<h3>FIN!</h3>';
	exit;
}

// ETIQUETAS COMUNES PARA OMITIR EN ARCHIVOS DEL BACK-OFFICE COMO SCRIPTS PERMITIDOS
$objeto->etiquetasOmitir[]='XXX';
$objeto->etiquetasOmitir[]='editar';
$objeto->etiquetasOmitir[]='_eliminar';
$objeto->etiquetasOmitir[]='cabeceraadmin';
$objeto->etiquetasOmitir[]='common_';
$objeto->etiquetasOmitir[]='_mnu';
$objeto->etiquetasOmitir[]='_iframe';

// OMITIR DETERMINADOS ARCHIVOS DEL BACK-OFFICE COMO SCRIPTS PERMITIDOS
$objeto->filesOmitir[]='index.php';
$objeto->filesOmitir[]='index2.php';
$objeto->filesOmitir[]='footer.php';
$objeto->filesOmitir[]='miniatura.php';
$objeto->filesOmitir[]='modules_footer.php';
$objeto->filesOmitir[]='recargar_modulo.php';
$objeto->filesOmitir[]='tabs_backoffice_disponibles.php';
$objeto->filesOmitir[]='tabs_backoffice_disponibles_precarga.php';
$objeto->filesOmitir[]='tabs_level_two.php';
$objeto->filesOmitir[]='vacia.php';
$objeto->filesOmitir[]='veo.php';
$objeto->filesOmitir[]='verifica.php';

$objeto->obtenerScriptsUtilizados();
?>
<style type="text/css">
<!--
<?php
include('_css/estilos_generales.php');
include('_css/estilos_dinamicos.php');
?>
#spnListaScript{padding-bottom:2px;padding-top:2px;line-height:+22px;}
.scUtilizado{border:1px solid <?=$_SESSION['colorTextLoose']?>;color:<?=$_SESSION['colorTextWarning']?>;}
-->
</style>
</head>

<body class="fondoPagina" id="bodyModulo"><center>
<form name="frmSelFiles" action="tabs_backoffice_disponibles.php" method="post" enctype="multipart/form-data">
<input type="hidden" id="accionSel" name="accionSel" value="1" />
<input type="hidden" id="campoActualizar" name="campoActualizar" value="<?=$_REQUEST['campoActualizar']?>" />
<input type="hidden" id="refField" name="refField" value="<?=$_REQUEST['refField']?>" />
<table width="100%" border="0" cellspacing="4" cellpadding="0">
  <tr>
    <td>
<?php
$archivosDirectorio=$objeto->recuperar_files('.','php');
$salida='<table id="tblLista" class="borde" width="95%" border="0" cellspacing="4" cellpadding="0"><tr height="5"><td></td></tr>
';
$cont=1;

foreach($archivosDirectorio as $key=>$nombreArchivo)
{
	$saltarArchivo=0; $saltarTr=0;
	$extension=substr($nombreArchivo,-3);
	if($extension!='php') continue;
	if(in_array($nombreArchivo,$objeto->filesOmitir)) continue;

	foreach($objeto->etiquetasOmitir as $nx=>$etiqueta)
	{
		if(strstr($nombreArchivo,$etiqueta))
		{
			$saltarArchivo=1;
			break;
		}
	}
	if($saltarArchivo) continue;

	if(count($objeto->scriptExsitente))
	{
		foreach($objeto->scriptExsitente as $idTabExist=>$scUtiliz)
		{
			if($nombreArchivo==substr($scUtiliz,0,strpos($scUtiliz,'.php')).'.php')
			{
				$salida.='
	  <tr>
			<td align="left"><span id="spnListaScript" class="scUtilizado">'.str_pad('',18,'&nbsp;').$nombreArchivo.str_pad('',24,'&nbsp;').'</span>&nbsp;<i><b>('.($objeto->tituloRootTab[$idTabExist]?utf8_encode($objeto->tituloRootTab[$idTabExist]).'</b> :: ':'ROOT :: <b>').($objeto->tituloTab[$idTabExist]).')</i></td>
			<td>&nbsp;</td>
		</tr>
';
				$saltarTr=1;
				break;
			}
		}
	}

	if(!$saltarTr)
	{
		$salida.='
	  <tr>
			<td align="left"><a href="#null" class="btn" style="padding-bottom:2px;padding-top:2px;line-height:+22px;" onclick="pegar(\''.$nombreArchivo.'\')" >'.str_pad('',18,'&nbsp;').$nombreArchivo.str_pad('',24,'&nbsp;').'</a></td>
			<td>&nbsp;</td>
		</tr>
';
	}

	$cont++;
}

$salida.='<tr height="5"><td></td></tr></table>';
?>
<span style="color:<?=$_SESSION['colorTextWarning']?>"><b>LEER</b>:</span> el contenido actual del campo "<b><?=$_REQUEST['refField']?></b>" se perder&aacute; al actualizar desde este listado. Al seleccionar uno de los scripts que aparecen aqu&iacute;, &eacute;ste se pegar&aacute; autom&aacute;ticamente.<br /><br />
<?php
echo $salida;
?>
		</td>
  </tr>
</table>
<a id="aCerrar" href="javascript:window.close()" class="btn">&nbsp;&nbsp;cerrar&nbsp;&nbsp;</a><br /><br />
</form>
</center>
<script>
<!--
function pegar(el)
{
	window.opener.$("#<?=$_REQUEST['campoActualizar']?>").html(el);
}
-->
</script>
</body>
</html>
