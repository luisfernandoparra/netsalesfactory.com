﻿<?php
/*
			------------- NOTA IMPORTANTE ----------------
Para configurar las carpetas de las imagenes que han de ser accesibles desde admin, se debe editar el archivo de configuración del plug-in "iBrowser" en:
editor/plugins/ibrowser/config/config.inc.php

Los siguientes array, con las rutas para pruebas y el otro para el site real:
$cfg['ilibs']
$cfg['ilibs']
*/
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/gestion_imagenes_jquery.class.php');
require_once('_inc/log.class.php');
$objeto=new ImagenesJquery($sql, $admin);

$objeto->path_imagenes='';
$objeto->tamanioMaximoImagenes=300000;	// EN BYTES
$nombreFilePhpBase='gestion_imagenes_jquery';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$objeto->nombreFilePhpBase=$nombreFilePhpBase;
$objeto->navegador=$objeto->ObtenerNavegador($_SERVER['HTTP_USER_AGENT']);

$objeto->filasporpagina=6;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=10;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=0;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// NÚMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$objeto->NoeditarFila=0;

$fraseTitularFormulario='--';	// FRASE QUE APARECE EN EL ENCABEZADO DEL FORMULARIO
$objeto->txtTitularDerecho='Imágenes presentes: ';
$varsUrl=parse_url($_SERVER['REQUEST_URI']);

$txtTitularFormularioNewRecord='Gestión de imágenes';
$txtTitularBuscador='Buscar imágenes';
$txtMsgStandard='Imágenes presentes: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';
$txtTitularEdicion='Edición de imágen';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
//echo $_SERVER['HTTP_REFERER'].'<pre>';print_r($_REQUEST);print_r($objeto);
?>