﻿<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/landings_site_content.class.php');
require_once('_inc/log.class.php');
$objeto=new landingsSiteContent($sql, $admin);

$objeto->filasporpagina=20;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=20;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=1;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// NÚMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$objeto->modalHeight='92%';
$objeto->modalWidth='70%';
$objeto->modalLeftPos='9%';
$objeto->innerTextHeight='300';
$objeto->editarFila=1;
$nombreFilePhpBase='landings_site_content';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR

$txtTitularFormularioNewRecord='<b>Insertar / editar</b> los contenidos de la landing';
$txtTitularBuscador='Buscar contenidos de landings';
$txtMsgStandard='Contenidos presentes: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';
//$txtTitularEdicion='Editar men&uacute; nivel 2';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>