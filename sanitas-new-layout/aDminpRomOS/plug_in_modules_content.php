<?php
if(!isset($_SERVER['HTTP_REFERER']))
{
	$hackerError=0;
	include('strange_errors.php');
	die();
}

//	SI EXSITE EL SCRIPT MOULE Y ESTA EN LA CARPETA ESPERADA
if(is_dir(PATH_PLUG_INS_ADMIN.$value['plugInPath']) && is_file((PATH_PLUG_INS_ADMIN.$value['plugInPath'].'/index.php')))
{
	include(PATH_PLUG_INS_ADMIN.$value['plugInPath'].'/index.php');
}
else
{
	echo 'Ha ocurrido un error al cargar el módulo esperado. Es posible que el módulo en cuestión ('.$value['plugInModuleName'].')no esté disponible todavía.';
}

?>
