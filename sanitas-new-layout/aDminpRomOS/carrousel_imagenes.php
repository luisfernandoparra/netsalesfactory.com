﻿<?php
/* ------------- NOTA IMPORTANTE ----------------
Para configurar las carpetas de las imagenes que han de ser accesibles desde admin, se debe editar el archivo de configuración del plug-in "iBrowser" en:
editor/plugins/ibrowser/config/config.inc.php

Los siguientes array, con las rutasw para pruebas y el otro para el site real:
$cfg['ilibs']
$cfg['ilibs']
*/
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require('_inc/cabeceraadmin.php');
require('_cnf/connect2.php');
require('_inc/carrousel_imagenes.class.php');
require('_inc/log.class.php');

$objeto=new CarrouselImagenes($sql, $admin);
$nombreFilePhpBase='carrousel_imagenes';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$objeto->debug=0;
$objeto->navegador=$objeto->ObtenerNavegador($_SERVER['HTTP_USER_AGENT']);

$objeto->nombreFilePhpBase=$nombreFilePhpBase;
$objeto->tamanioMaximoImagenes=200000;	// EN BYTES
$objeto->tamanioMaximoThubnail=50000;	// EN BYTES
$objeto->path_imagenes='../img_diapos/';
$objeto->filasporpagina=10;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=20;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=1;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// NÚMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$objeto->editarFila=1;

$txtTitularFormularioNewRecord='Insertar un Carrousel dinámico';
$txtTitularBuscador='Buscar Carrousels dinámicos';
$txtMsgStandard='Carrousels dinámicos presentes: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';
$txtTitularEdicion='Editar Carrousels dinámicos';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>