﻿<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/tabs_backoffice.class.php');
require_once('_inc/log.class.php');

//	SE SETEA EL ID DEL USUARIO PARA MANIPULAR LOS TABS (SOLO ADMINISTRADORES DE NIVEL SUPERIOR)
if($_SESSION['usuarioNivel']>14)
{
	$_SESSION['idUsuarioTabActivo']=$_REQUEST['usuarioTabActivar']?$_REQUEST['usuarioTabActivar']:($_SESSION['idUsuarioTabActivo']?$_SESSION['idUsuarioTabActivo']:$_SESSION['id_usuario']);
}
else
	$_SESSION['idUsuarioTabActivo']=$_SESSION['id_usuario'];

$objeto=new TabsBackoffice($sql, $admin);
$objeto->filasporpagina=15;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=20;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=1;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// NÚMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$objeto->editarFila=1;
$objeto->permiteEliminarGlobal=1;
$nombreFilePhpBase='tabs_backoffice';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR

//$txtTitularFormularioNewRecord=!$_REQUEST['titularModulo']?utf8_decode($_REQUEST['titularModulo']):'módulo cargado correctamente';
$txtTitularFormularioNewRecord='Tab para el Back-office';
$txtTitularBuscador='Buscar Tabs';
$txtMsgStandard='Tabs presentes: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';
$txtTitularEdicion='Editar Tab';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>