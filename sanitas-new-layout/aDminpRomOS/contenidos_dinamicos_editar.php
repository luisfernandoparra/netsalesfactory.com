<?php
header('Content-Type: text/html; charset=utf-8');
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/log.class.php');

require_once('_inc/contenidos_dinamicos.class.php');
$objeto=new ContenidosDinamicos($sql, $admin);
//$objeto->carga($_REQUEST['id']);

$nombreFilePhpBase='contenidos_dinamicos';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$objeto->nombreFilePhpBase=$nombreFilePhpBase;

$objeto->tamanioMaximoAttach=750000;	// EN bites
$objeto->tamanioMaximoImagenes=200000;	// EN BYTES
$objeto->anchoMaximoImagenes=720;	// EN PIXELS
$objeto->path_imagenes='../images/';
$objeto->innerTextHeight="50"; // altura innerTinyBoxEdit
$txtLogEditFinish='record stored';
$txtTitularEdicion='Edici&oacute;n links/contenidos din&aacute;micos';

include('_inc/common_form_edit.php');
?>
