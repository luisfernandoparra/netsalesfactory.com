<?php
@session_start();
// GESTIÓN DE LAS TABS DE NIVEL 2 DEL BACK-OFFICE
$objeto->tabs_level_two($idTab);
$numSubMenus=count($objeto->elemMenuTwo[$idTab]);
//echo chr(((count($objeto->elemMenuTwo[$idTab]['tabId']))+96)).$contSubTabs;
if($numSubMenus)
{
	$cont=1; $contenidoElemento='';
//	foreach($objeto->elemMenuTwo[$idTab]['tabId'] as $kk=>$idSubTab)
//	{
//		$menu[$idTab][]=array(utf8_encode($objeto->elemMenuTwo[$idTab]['tabTit'][$idSubTab]),$objeto->elemMenuTwo[$idTab]['tabCont'][$idSubTab],$objeto->elemMenuTwo[$idTab]['levelAccess'][$idSubTab]);
//	}
//	$numElemns=count($menu[$idTab]);

	$entradaMenu='<!-- START BLOCK MENU '.$contSubTabs.'-->
<div class="tabset" id="mnu'.$contSubTabs.'">';

	foreach($objeto->elemMenuTwo[$idTab]['tabId'] as $kk=>$idSubTab)
	{
		$scriptName=$objeto->elemMenuTwo[$idTab]['tabCont'][$idSubTab];

		$_SESSION['id0_'.$idTab][$idSubTab]=$idSubTab;//unset($_SESSION['id0_'.$idTab][$idSubTab]);

//		$nombreScript=substr($scriptName,0,strpos($scriptName,'.php')+4);
		if($_SESSION['usuarioNivel'] < $objeto->elemMenuTwo[$idTab]['levelAccess'][$idSubTab]) continue;

		$entradaMenu.='<a name="'.chr(($cont+96)).$contSubTabs.'" id="'.chr(($cont+96)).$contSubTabs.'" class="tab '.$disabledTab.' {content:\'cont_'.$contSubTabs.'_'.$cont.'\'}" >'.utf8_encode($objeto->elemMenuTwo[$idTab]['tabTit'][$idSubTab]).'</a>
';
		$contenidoElemento.='<div id="cont_'.$contSubTabs.'_'.$cont.'" style="padding-right:0;">';

		if(file_exists($scriptName))
		{
			$contenidoElemento.='<iframe name="'.$scriptName.'?posicionSolapa='.(rand(10+$idTab,$idTab*1000)).'&refSub='.$idSubTab.'" class="iframeSolapa" id="ifr_'.chr(($cont+96)).$contSubTabs.'" src="recargar_modulo.php?titularModulo='.strip_tags(utf8_encode($objeto->elemMenuTwo[$idTab]['tabTit'][$idSubTab])).'" frameborder="0" height="'.$altoIframe.'" marginheight="0" marginwidth="0"></iframe>';
		}
		else
		{
			if($objeto->elemMenuTwo[$idTab]['tabTip'][$idSubTab] != 3)
				$contenidoElemento.='<span style="font-size:1.3em;"><br /><br /><center><span id="spnErr" style="width:200px;">ERROR</span><br /><br /><br /><br /><br /><br />El archivo necesario para el m&oacute;dulo <b>"'.utf8_encode($objeto->elemMenuTwo[$idTab]['tabTit'][$idSubTab]).'"</b> no est&aacute; definido o no est&aacute; presente en el servidor en estos momentos.<br /><br />Lamentamos las molestias...<br /><br /><b>NOTA:</b> habitualmente el problema puede deberse a que el archivo no est&aacute; a&uacute;n presente en la Web, puede ser cuesti&oacute;n de tiempos; por favor, int&eacute;ntelo m&aacute;s tarde.</center><br /><br /></span>';
			
			$contenidoElemento.=$scriptName ? $scriptName : '<center><span style="display:block;font-align:center;min-height:200px;"><br /><br /><br />Sin contenidos definidos en estos momentos para este m&oacute;dulo.</span></center>';

		}

		$contenidoElemento.='</div>
';
		$cont++;
	}

	$entradaMenu.='</div>
<!-- END BLOCK MENU '.$contSubTabs.'-->

<!-- START BLOCK CONTENTS '.$contSubTabs.'-->
';
	echo $entradaMenu;
	echo utf8_encode($contenidoElemento);
}
else
{
//<div class="tabset" id="mnu2"><a name="a2" id="a2" class="tab  {content:'cont_2_1'}" >logs_backoffice</a></div>
//echo $cont.'<pre>';print_r($objeto->elemMenuTwo);
	echo '
<center><span style="display:block;width:90%;height:300px;padding:20px;text-align:left;"><center><p style="font-weight:bold;color:'.$_SESSION['colorTextWarning'].'">NO SE DEFINIERON SUB-TABS PARA ESTE NIVEL!</p></center><br /><br /><br /><i>Instrucciones:</i><br />1.- Hay que acceder al menú de creación/modificación de TABS y agregar el/los elemento/s deseados para esta solapa (TAB).<br />2.- El script al que se debe acceder, puede no existir en un primer momento, no es necesario que esté creado a priori; lo que si debe ser exacto, es el nombre del mismo para que éste pueda ser ejecutado.</span></center>';
}
?>
