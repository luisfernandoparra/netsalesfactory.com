<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/ayudas_front_end.class.php');
require_once('_inc/log.class.php');
//new Log($sql, $id_pagina);

$objeto=new ayudasFrontEnd($sql, $admin);
$objeto->filasporpagina=5;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=20;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=0;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// N�MERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICI�N DE LA VENTANA RESPECTO A LAS DEM�S EN SU MISMA SECCI�N O �REA
$nombreFilePhpBase='ayudas_front_end';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR

$txtTitularFormularioNewRecord='Ayudas Front-end';
$txtTitularBuscador='Encontrar ayudas para el front-end';
$txtMsgStandard='Ayudas presentes para el front-end: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>