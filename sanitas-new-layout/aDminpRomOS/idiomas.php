﻿<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/idiomas.class.php');
require_once('_inc/log.class.php');
$objeto=new idiomas($sql, $admin);
$objeto->path_imagenes='../banderas/';
$objeto->path_autores='../autores/';
$nombreFilePhpBase='idiomas';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$objeto->tamanioMaximoImagenes=80000;	// EN BYTES

$objeto->navegador=$objeto->ObtenerNavegador($_SERVER['HTTP_USER_AGENT']);
$objeto->filasporpagina=10;	// NUMERO DE LINEAS QUE SE VISUALIZARAN EN EL LISTADO DE REGISTROS DE LA PAGINA DE RESULTADOS
$objeto->TotPagMostrar=10;	// numero de links para moverse en la paginacion
$objeto->verIdRegistros=1;	// MOSTRAR EL ID DE REGISTRO EN EL LISTADO DE RESULTADOS
$objeto->verEmergentesTextosFragmentados=1; // EN TEXTOS FRAGMENTADOS DEL LISTADO
$objeto->decimalesEnListado=0;	// NÚMERO DE DECIMALES A MOSTRAR
$objeto->posicionSolapa=$id_pagina;	// POSICIÓN DE LA VENTANA RESPECTO A LAS DEMÁS EN SU MISMA SECCIÓN O ÁREA
$objeto->editarFila=0;

$txtTitularFormularioNewRecord='Idiomas de la Web';
$txtTitularBuscador='Buscar idiomas';
$txtMsgStandard='Idiomas presentes: ';
$txtMsgREcordInsertado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos insertados correctamente</b></span>';
$txtMsgREcordEditado='<span style=color:'.$_SESSION['colorTextHighli'].'><b>Datos almacenados correctamente&nbsp;</b></span>';
$txtTitularEdicion='Edición de administrador Fvr`s';

$txtLogList='data listing';
$txtLogEdit='data editing';
$txtLogInsert='new record inserted';

include('_inc/common_admin_script.php');
?>