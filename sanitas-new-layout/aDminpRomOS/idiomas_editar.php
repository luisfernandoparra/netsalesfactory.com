<?php
include ('_inc/cabeceraadmin.php');
include ('_cnf/connect2.php');
require_once('_inc/idiomas.class.php');
require_once('_inc/log.class.php');

$objeto=new idiomas($sql, $admin);
$objeto->path_imagenes='../banderas/';
$objeto->path_autores='../autores/';
$objeto->tamanioMaximoImagenes=80000;	// EN BYTES
$objeto->carga($_REQUEST['id']);
$nombreFilePhpBase='idiomas';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$txtLogEditFinish='record stored';
$txtTitularEdicion='Edición idiomas';

include('_inc/common_form_edit.php');
?>