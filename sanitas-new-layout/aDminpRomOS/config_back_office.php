<?php
$id_pagina=$_REQUEST['posicionSolapa']?$_REQUEST['posicionSolapa']:rand(100,999);
require_once('_inc/cabeceraadmin.php');
require_once('_cnf/connect2.php');
require_once('_inc/config_back_office.class.php');
require_once('_inc/log.class.php');

$objeto=new Configuracion($sql, $admin);
$objeto->carga($_SESSION['id_usuario']);
$nombreFilePhpBase='config_back_office';	// NOMBRE PRINCIPAL DEL ARCHIVO A MANEJAR
$objeto->configBackOfficeScript=1;
$txtTitularFormularioNewRecord=$_REQUEST['titularModulo']?utf8_decode($_REQUEST['titularModulo']):'Personalizar los colores de esta herramienta de administraci&oacute;n';

$objeto->dibujarReset=1;

$_REQUEST['logEstado']='data listing';
$_REQUEST['id_grabar']=$objeto->id;

if($_REQUEST['accion']==1)	// ACTUALIZAR DATOS
{
	$objeto->form_editar_datos_procesa($_REQUEST);
	$_REQUEST['logEstado']='New config saved';
}
?>

<!-- START COLOR PICKER -->
<link rel="stylesheet" href="_css/colorpicker.min.css" type="text/css" />
<script type="text/javascript" src="_js/colorpicker.min.js"></script>
<!-- END COLOR PICKER -->

</head>
<body class='fondoPagina'>
<?php
//	se generan los formularios
$objeto->form_editar_datos($nombreFilePhpBase.'.php','1',$txtTitularFormularioNewRecord.' <span style=color:'.$_SESSION['colorTextWarning'].'>&nbsp;&nbsp;(modificar con atenci&oacute;n)</span>');

if ($_REQUEST['accion']==1)
{
	$msg='<b>Datos salvados correctamente</b>&nbsp;';
	echo "<script>document.getElementById('mesajes_formulario').innerHTML='$msg';</script>";
}

new Log($sql,$_SESSION['id_usuario']);
include_once('_inc/help_button.class.php');
include_once('footer.php');
//echo utf8_decode($_REQUEST['titularModulo']);//'<pre>';print_r($_REQUEST['titularModulo']);
?>
</body>
</html>