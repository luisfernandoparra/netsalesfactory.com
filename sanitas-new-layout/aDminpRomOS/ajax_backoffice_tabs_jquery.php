﻿<?php
if(!isset($_SERVER['HTTP_REFERER']))
{
	$hackerError=0;
	include('strange_errors.php');
	die();
}

include('cabeceraadmin_logica.php');
include('_cnf/connect2.php');
include_once('_inc/log.class.php');

if(count($_GET))	// CARGA ARRAY CON VALORES DE ENTRADA
	$arraExtVars=$_GET;
else
	$arraExtVars=$_REQUEST;


if($arraExtVars['idUserManage'])	// OBTENER TABS PRINCIPALES DEL USUARIO idUserManage
{
	$query='SELECT id, tab_title FROM '.$sql->db.'.back_office_tabs WHERE tab_level=1 && !tab_child && id_user='.(int)$arraExtVars['idUserManage'].' ORDER BY tab_order';
	$res=$sql->query($query); $numTabs=0;

	while($arra=$sql->fila2($res))
	{
		$formData[$arra['id']]['tab_title']=  html_entity_decode(utf8_encode($arra['tab_title']));
		$numTabs++;
	}

	$response['success']=$res ? 1 : 0;
	$response['error']=!$res ? 1 : 0;
	$response['numTabs']=$numTabs;
	$response['formData']=$formData;
	$response['query']=$query;
	$res=json_encode($response);
	die($res);
}


if($arraExtVars['idLevelOne'])	// OBTENER SUB-TABS DEL BACK-OFFICE ACTUAL
{
//	$query='SELECT id,tab_title,tab_content FROM back_office_tabs WHERE tab_child='.$arraExtVars['idLevelOne'].' && tab_level=2 && id_user='.$_SESSION['id_usuario'].' ORDER BY tab_order';
	$query='SELECT id,tab_title,tab_content FROM back_office_tabs WHERE tab_child='.$arraExtVars['idLevelOne'].' && tab_level=2 ORDER BY tab_order';
	$res=$sql->query($query); $numTabs=0;

	while($arra=$sql->fila2($res))
	{
		$formData[$arra['id']]['tab_title']=  html_entity_decode(utf8_encode($arra['tab_title']));
		$numTabs++;
	}

	$response['success']=$res ? 1 : 0;
	$response['error']=!$res ? 1 : 0;
	$response['numTabs']=$numTabs;
	$response['formData']=$formData;
	$response['query']=$query;
	$res=json_encode($response);
	die($res);
}

?>
