<?php
@session_start();
include('../conf/config_web.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>POLÍTICA DE PRIVACIDAD Y PROTECCIÓN DE DATOS DE AGENCIA DE MEDIACIÓN DE SEGUROS</title>
	</head>
	<body style="font:Arial, Helvetica, sans-serif, Gadget, sans-serif; color:#000000; background-color:#CCC">
		<div style="width:auto; background-color:#FFF; padding:20px;">
		<h3 style="font-family: Arial, Helvetica, sans-serif;">POLÍTICA DE PRIVACIDAD Y PROTECCIÓN DE DATOS DE AGENCIA DE MEDIACIÓN DE SEGUROS</h3>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">FINTEL MARKETING SLU PARA SANITAS S.A. DE SEGUROS</h3>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">1. PRESENTACIÓN Y DERECHO DE INFORMACIÓN</h3>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		La presente la web, cuyo titular es FINTEL MARKETING SLU, AGENCIA DE SEGUROS de SANITAS S.A. DE SEGUROS (en adelante SANITAS), según contrato entre ambas partes (en adelante, LA AGENCIA), con CIF B86420213, domicilio en Plaza Callao, nº 4, 6º planta, 28013, en MADRID, pone a disposición de los Usuarios (en adelante, los “Usuarios” o el “Usuario”) de Internet interesados en los servicios (en adelante, los “Servicios”) y contenidos (en adelante, los “Contenidos”) alojados en el Sitio Web.
		Se entiende que el Usuario acepta expresamente la presente Política de Privacidad y Protección de datos una vez que cumplimente el formulario de registro del Sitio Web haciendo click en la palabra “CONTINUAR” o “ENVIAR”.
		</p>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">2. RECOMENDACIONES</h3>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		Por favor lea detenidamente y siga las siguientes recomendaciones:
		<ul style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		    <li> Utilice contraseñas con una extensión mínima de 8 caracteres, alfanuméricos y con uso de mayúsculas y minúsculas. </li>
		    <li> El Sitio Web no está dirigido a menores de edad. Por favor active el control parental para prevenir y controlar el acceso de menores de edad a Internet e informar a los menores de edad sobre aspectos relativos a la seguridad. </li>
		    <li> Mantenga en su equipo un software antivirus instalado y debidamente actualizado, que garantice que su equipo se encuentra libre de software maligno, así como de aplicaciones spyware que pongan en riesgo su navegación en Internet, y en peligro la información alojada en el equipo. </li>
		    <li> Revise y lea las condiciones generales de uso y la política de privacidad que la plataforma pone a su disposición en el Sitio Web. </li>
		</ul>
		</p>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">3. PROCESAMIENTO DE DATOS PERSONALES, FINALIDAD DEL TRATAMIENTO Y CONSENTIMIENTO PARA LA CESIÓN DE DATOS</h3>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		Los datos de los Usuarios que se recaban a través de los formularios de registro online disponibles en el Sitio Web, son recabados por LA AGENCIA con la finalidad de poder prestarles los Servicios ofrecidos a través del Sitio Web.
		Asimismo, le informamos de que sus datos personales de contacto (nombre, apellidos, teléfono móvil, dirección, empresa, cargo, sitio web, dirección de correo electrónico, etc.) serán incorporados a un fichero automatizado y utilizados para remitirle Newsletters y comunicaciones comerciales y promocionales relacionadas con los servicios de LA AGENCIA por carta, teléfono, correo electrónico, SMS/MMS, o por otros medios de comunicación electrónica equivalentes y ello al amparo de lo establecido en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (en adelante, LOPD), en la Ley 34/2002 de 11 de Julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico y en la Ley 32/2003 de 3 de Noviembre, General de Telecomunicaciones.
		El usuario, una vez cumplimente el formulario de registro del Sitio Web, consiente expresamente que sus datos (nombre, apellidos, dirección postal y datos de geolocalización) puedan ser utilizados por SANITAS S.A. DE SEGUROS, en adelante, SANITAS, para la promoción de sus productos, así como para LA AGENCIA, este último como Agencia de Mediación de SANITAS (y en particular a las sociedades pertenecientes al grupo FAST TRACK NETWORK SL), otras previstas en la Ley o cedidos (incluso para depurar datos personales) a otras entidades españolas o de la Unión Europea para enviarle, por carta, teléfono, correo electrónico, SMS/MMS, o por otros medios de comunicación electrónica equivalentes, a través nuestro o de otras entidades, publicidad y ofertas comerciales y de servicios relacionados con los sectores de:
		</p>
		<ul style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		    <li> Marketing o pertenecientes a la Federación Española de Comercio Electrónico y Marketing Directo (FECEMD) </li>
		    <li> Telecomunicaciones: Productos y Servicios de telecomunicaciones y tecnología. </li>
		    <li> Financiero: Prestados por entidades financieras, Aseguradoras y de Previsión social. </li>
		    <li> Ocio: Editorial, Turismo, Deportes, Coleccionismo, Fotografía, Pasatiempos, Juguetería, Transporte, Jardinería, Hobbies, Loterías, peñas de loterías, Comunicación y entretenimiento. </li>
		    <li> Gran consumo: Electrónica, Informática, Textil, Imagen y Sonido, Complementos, Hogar, Bazar, Cuidado personal (Cosmética, Perfumería, Parafarmacia, especialidades Farmacéuticas publicitarias) Mobiliario, Inmobiliario, Alimentación y Bebidas, salud y belleza, Material de oficina. Moda y decoración. </li>
		    <li> Automoción: Productos y Servicios relacionados con el Automóvil, Motocicletas y Camiones. </li>
		    <li> Energía y agua: Productos relacionados con la Electricidad, Hidrocarburos, Gas y Agua. </li>
		    <li> ONG: Productos y Servicios relacionados con ONG. </li>
		</ul>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		El usuario acepta y consiente que LA AGENCIA trate sus Datos Personales y consulte ficheros de terceras entidades con la finalidad de determinar su perfil y ofrecerle productos y servicios adecuados, propios o de terceras empresas pertenecientes a los sectores anteriormente relacionados. El Usuario acepta expresamente la presente Política de Privacidad y otorga su consentimiento expreso al tratamiento automatizado de los datos personales facilitados. No obstante, el Usuario podrá revocar el consentimiento, en cada comunicado comercial o publicitario que se le haga llegar, y en cualquier momento, mediante notificación en la siguiente dirección de correo electrónico legales@fasttracknet.com ; o mediante carta dirigida a FINTEL MARKETING SLU domiciliada en el Palacio de la Prensa, Plaza de Callao 4, 6º, 28013 Madrid.
		</p>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">4. CARÁCTER OBLIGATORIO O FACULTATIVO DE LA INFORMACIÓN FACILITADA POR EL USUARIO Y VERACIDAD DE LOS DATOS</h3>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		El Usuario garantiza que los datos personales facilitados son veraces y se hace responsable de comunicar a LA AGENCIA cualquier modificación de los mismos.

		El Usuario responderá, en cualquier caso, de la veracidad de los datos facilitados, LA AGENCIA el derecho a excluir de los servicios registrados a todo Usuario que haya facilitado datos falsos, sin perjuicio de la demás acciones que procedan en Derecho.

		Se recomienda tener la máxima diligencia en materia de Protección de Datos mediante la utilización de herramientas de seguridad, no pudiéndose responsabilizar a LA AGENCIA de sustracciones, modificaciones o pérdidas de datos ilícitas.
		</p>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">5. DERECHOS DE ACCESO, RECTIFICACIÓN, CANCELACIÓN Y OPOSICIÓN</h3>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		Le recordamos que, en el momento que lo desee, pueda ejercitar sus derechos de acceso, rectificación, cancelación y oposición o de oposición al tratamiento general o con fines promocionales de sus datos personales enviándonos una comunicación gratuita a la dirección de correo electrónico legales@fasttracknet.com indicando la referencia:“NETSALES-Ejercicio derechos LOPD/LSSI]) que deberá contener: su nombre y apellidos, fotocopia de su DNI (pasaporte u otro documento válido que lo identifique), petición en que se concreta la solicitud, dirección a efectos de notificaciones, fecha, firma y documentos acreditativos de la petición que formula, en su caso.
		</p>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">6. USO DE COOKIES</h3>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		De conformidad con el Real Decreto-Ley 13/2012, de 30 de marzo, que entró en vigor el 1 de abril de 2012, LA AGENCIA ha contratado un servicio que posibilita al usuario que accede al Sitio Web de LA AGENCIA aceptar o rechazar que LA AGENCIA utilice cookies en su navegador.

		Mediante la aceptación de la presente Política de Privacidad, el Usuario acepta los servicios de retargeting que se podrán llevar a cabo mediante el envío de comunicaciones comerciales. El Usuario consiente que dichas comunicaciones comerciales contengan dispositivos de almacenamiento o cookies de publicidad que se instalarán en su navegador.

		Estos servicios de retargeting tienen como finalidad proporcionar más información sobre productos o servicios que puedan interesar al Usuario mediante:

		-La instalación de dispositivos de almacenamiento y recuperación de datos o cookies en equipos terminales en algunos de los correos electrónicos enviados a los usuarios.

		-Envío de correos electrónicos con comunicaciones comerciales a los que se haya instalado las Cookies mediante la visita de una página web o mediante la instalación a través de correo electrónico. Además, en cada comunicación comercial que contenga cookies, se informará al usuario de qué tipo de cookies contiene y cómo desactivarlas.

		En todo caso, el usuario, mediante la aceptación de la política de protección de datos y de privacidad del Sitio Web, salvo mención expresa en contrario por el usuario, acepta expresamente que LA AGENCIA pueda utilizar las cookies. Aun así, el usuario tiene la posibilidad de configurar su navegador para ser avisado de la recepción de cookies y para impedir su instalación en su equipo. Por favor, consulte las instrucciones y manuales de su navegador para ampliar

		esta información. Puede obtener más información acerca del funcionamiento de las cookies en http://www.youronlinechoices.com/es/

		Las cookies son ficheros enviados al navegador por medio de un servidor web con la finalidad de registrar las actividades del usuario durante su tiempo de navegación. Las cookies utilizadas por el sitio web se asocian únicamente con un usuario anónimo y su ordenador, y no proporcionan por sí mismas los datos personales del usuario. Mediante el uso de las cookies resulta posible que el servidor donde se encuentra la web, reconozca el navegador web utilizado por el usuario con la finalidad de que la navegación sea más sencilla, permitiendo, por ejemplo, el acceso a los usuarios que se hayan registrado previamente, acceder a las áreas, servicios, promociones o concursos reservados exclusivamente a ellos sin tener que registrarse en cada visita. Se utilizan también para medir la audiencia y parámetros del tráfico, controlar el progreso y número de entradas.

		Para utilizar el sitio web, no resulta necesario que el usuario permita la instalación de las cookies enviadas por el sitio web, o el tercero que actúe en su nombre, sin perjuicio de que sea necesario que el usuario inicie una sesión como tal en cada uno de los servicios cuya prestación requiera el previo registro o “login”.
		</p>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">7. MEDIDAS DE SEGURIDAD</h3>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		LA AGENCIA mantiene los niveles de seguridad de protección de datos personales conforme a la LOPD y al Real Decreto 1720/2007, de 21 de diciembre, por el que se aprueba el Reglamento de desarrollo de la LOPD que contengan datos de carácter personal y ha establecido todos los medios técnicos a su alcance para evitar la pérdida, mal uso, alteración, acceso no autorizado y robo de los datos que el usuario facilite a través del Sitio Web, sin perjuicio de informarle de que las medidas de seguridad en Internet no son inexpugnables.

		LA AGENCIA se compromete a cumplir con el deber de secreto y confidencialidad respecto de los datos personales contenidos en el fichero automatizado de acuerdo con la legislación aplicable, así como a conferirles un tratamiento seguro en las cesiones que, en su caso, puedan producirse.
		</p>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">8. LINKS A PÁGINAS WEB</h3>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		El Sitio Web de LA AGENCIA podría contener links a páginas web de compañías y entidades de terceros.

		LA AGENCIA no puede hacerse responsable de la forma en la que estas compañías tratan la protección de la privacidad y de los datos personales, por lo que le aconsejamos que lea detenidamente las declaraciones de política de privacidad de éstas páginas web que no son propiedad de LA AGENCIA con relación al uso, procesamiento y protección de datos personales. Las condiciones que ofrecen éstas páginas web pueden no ser las mismas que las que ofrece LA AGENCIA.
		</p>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">9. PREGUNTAS</h3>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		Si tiene alguna pregunta sobre esta Política de Privacidad, rogamos que se ponga en contacto con nosotros enviando un email a legales@fasttracknet.com .
		</p>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">10. CAMBIOS</h3>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		LA AGENCIA se reserva el derecho de revisar su Política de Privacidad en el momento que lo considere oportuno. Por esta razón, le rogamos que compruebe de forma regular esta declaración de privacidad para leer la versión más reciente de la política de privacidad de LA AGENCIA.
		</p>
		<h3 style="font-family: Arial, Helvetica, sans-serif;">11. CONDICIONES PARTICULARES</h3>
		<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
		El acceso a determinados Contenidos ofrecidos a través del Sitio Web puede encontrarse sometido a ciertas condiciones particulares propias que, según los casos, sustituyen, completan las modifican las condiciones generales. Por tanto, con anterioridad al acceso y/o utilización de dichos Contenidos, el Usuario ha de leer atentamente también las correspondientes condiciones particulares. 
		</p>
	</body>
</html>
