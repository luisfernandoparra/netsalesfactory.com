<?php
@session_start();
include('../conf/config_web.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Condiciones de Contratación</title>
	</head>
	<body style="font:Arial, Helvetica, sans-serif, Gadget, sans-serif; color:#000000; background-color:#CCC">
		<div style="width:auto; background-color:#FFF; padding:20px;">
			<h3 style="font-family: Arial, Helvetica, sans-serif;">
				Condiciones de Contratación
			</h3>
			<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
				La presente la web, cuyo titular es de FINTEL MARKETING SLU, AGENCIA DE SEGUROS de SANITAS S.A. DE SEGUROS (en adelante SANITAS), 
				según contrato entre ambas partes (en adelante, LA AGENCIA), con CIF B86420213, domicilio en Plaza Callao, nº 4, 6º planta, 28013, en MADRID.<br /><br />
			</p>
			<h3 style="font-family: Arial, Helvetica, sans-serif;">
				1. Condiciones de acceso:
			</h3>
			<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
				La finalidad de esta web es ofrecer información sobre promociones y contratación de servicios de Sanitas dental Milenium en modalidad de pago anual. 
				No se realizan contrataciones por vía electrónica, siendo necesaria en todo caso la contratación telefónica a través de LA AGENCIA. 
				Su utilización supone la aceptación plena por el Usuario de estas condiciones, por lo que si no está de acuerdo con el contenido de las mismas deberá 
				abstenerse de hacer uso de la web, así como de los servicios ofrecidos en ella. LA AGENCIA se reserva el derecho a efectuar las modificaciones que estime 
				oportunas, pudiendo modificar, suprimir o incluir, sin previo aviso, nuevos contenidos y/o servicios, así como la forma en que éstos aparezcan presentados 
				y localizados y las condiciones de uso de la web. El Usuario acepta que el acceso y uso de la web y de los contenidos incluidos en la misma tiene lugar libre 
				y conscientemente, bajo su exclusiva responsabilidad. <br /><br />
			</p>
			<p style="font-size:12px;">
			<h3 style="font-family: Arial, Helvetica, sans-serif;">
				2. Condiciones Contratación:
			</h3>
			</p>
			<h3 style="font-family: Arial, Helvetica, sans-serif;">
				Sanitas Dental 21:
			</h3>
			<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;"><br />
				Puedes contratar Sanitas Dental 21 tengas o no otros seguro de Sanitas tan sólo por 6,4 € persona /mes (1). <br />
				Precio garantizado durante el primer año de contratación.
				- Sin límite de edad para su contratación.<br />
				- Los menores de 6 años no pagan prima, siempre que estén en la póliza con uno de los padres (o tutores).<br />
				- La mensualidad de las prima neta gratuita corresponden al 6º a contar desde la fecha de entrada en vigor del seguro siempre que se encuentre al 
				corriente de pago de la prima neta del seguro dental.<br />
				- Pago de franquicias para los servicios que lo precisen se realizan directamente en consulta.<br />
				- Participación en el coste de los servicios (copago): 3 euros en servicios no franquiciados.<br />
				- Forma de pago mensual, trimestral, semestral y anual. No se aplican descuentos.<br />
				- Sin carencias.<br />
				- Se admiten las patologías dentales preexistentes, es decir, el seguro médico cubre las patologías existentes con anterioridad a la fecha de 
				contratación del producto.<br />
				- Garantía de 10 años en implantes y ortodoncia en las Clínicas Milenium Dental<br />
			</p>
			<h3 style="font-family: Arial, Helvetica, sans-serif;">
				Sanitas Milenium:
			</h3>
			<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
				Puedes contratar Sanitas Dental Milenium tengas o no otro seguro de Sanitas tan sólo por 7,4 € persona/mes (1). Precio garantizado 
				durante el primer año de contratación. <br />
				- Sin límite de edad para su contratación. <br />
				- Los menores de 6 años no pagan prima, siempre que estén en la póliza con uno de los padres (o tutores). <br />
				- La mensualidad de la prima neta gratuita corresponde al 2º mes a contar desde la fecha de entrada en vigor del seguro siempre que se encuentre al 
				corriente de pago de la prima neta del seguro dental.<br />
				- Pago de franquicias para los servicios que lo precisen se realizan directamente en consulta.<br />
				- Sin participación en el coste de los servicios (copago).<br />
				- Forma de pago mensual, trimestral, semestral y anual. No se aplican descuentos. <br />
				- Sin carencias.<br />
				- Se admiten las patologías dentales preexistentes, es decir, el seguro médico cubre las patologías existentes con anterioridad a la 
				fecha de contratación del producto.<br />
				- Garantía de 10 años en implantes y ortodoncia en las Clínicas Milenium Dental<br />
				(1) Prima neta mensual por asegurado a la que se le aplicará el recargo del Consorcio de Compensación de Seguros 0,15% sobre la prima neta y todos 
				los gastos e impuestos repercutibles según la normativa aplicable. <br />
				Primas netas válidas para altas hasta el 31/12/2014 y garantizada durante el primer año de contratación.<br />

			</p>
			<h3 style="font-family: Arial, Helvetica, sans-serif;">
				3. Condiciones de la Promoción Viaje a Disneyland
			</h3>
			<p style="font-size:12px; font-family: Arial, Helvetica, sans-serif;">
				Únicamente participarán en el sorteo www.ganaungranviaje.com  los usuarios que contraten una póliza anual del Seguro Dental Sanitas. 
				La selección del usuario ganador se hará mediante los procedimientos de la entidad organizadora del sorteo. 
				Puede consultar las bases del sorteo en http://www.ganaungranviaje.com/bases-legales/<br />
				A los premios del presente sorteo le será de aplicación lo establecido en la Ley 36/2006 del Impuesto de la Renta de las Personas Físicas. 
				SANITAS y LA AGENCIA no se responsabiliza de las posibles pérdidas, deterioros, robos, retrasos o cualquier otra circunstancia imputable a 
				terceros que puedan afectar al desarrollo de la presente promoción. <br />
				SANITAS Y LA AGENCIA excluye cualquier responsabilidad por daños y perjuicios de toda naturaleza que puedan deberse a la falta temporal de 
				disponibilidad o de continuidad del funcionamiento del sitio web, a la defraudación de la utilidad que los usuarios hubieren podido atribuir 
				a los mismos, y en particular, aunque no de modo exclusivo, a los fallos en el acceso a las distintas páginas y envíos de respuestas de participación 
				a través de Internet habilitadas al efecto.<br />
				LA AGENCIA se reserva el derecho a modificar en cualquier momento las condiciones de la presente promoción, incluso su posible anulación antes 
				del plazo prefijado, siempre que concurra causa justa, comprometiéndose a comunicar las nuevas bases, condiciones o la anulación definitiva, 
				en su caso, con la suficiente antelación.
			</p>
		</div>
	</body>
</html>