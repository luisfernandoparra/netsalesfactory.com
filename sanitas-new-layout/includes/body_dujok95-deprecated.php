<?php
@session_start();
$idLandingFormatted = str_pad($objSiteData->landingId, 3, '0', STR_PAD_LEFT);
$rutaImgs = $path_raiz_aplicacion_local . 'img/' . $idLandingFormatted . '/';
$defaultLocalPhoneCall = '';
$inbound_phone = ($inbound_phone) ? $inbound_phone : ''; // SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
?>
<script src="<?=$path_raiz_aplicacion_local?>js/jquery.bvalidator.min.js"></script>
<link href="<?=$path_raiz_aplicacion_local?>css/bvalidator.theme.bootstrap.rt.css" rel="stylesheet" type="text/css" />
<link href="<?=$path_raiz_aplicacion_local?>css/bvalidator.theme.postit.css" rel="stylesheet" type="text/css" />

<!-- START INNER MODAL RESP -->
<link href="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.css" type="text/css" rel="stylesheet" />
<link href="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.min.js"></script>
<style>
#modal-window .modal-box{top:20%!important;}
</style>
<!-- END INNER MODAL RESP -->

<style>
/*
div.error2{ display : none; border:2px solid #D81E05; }
.error2{color:#D81E05; background:#FCF1F0;}
div.error2 ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error2 ul li{margin:0 0 10 0;list-style-type:none;}
div.error2 ul li label{font-weight:normal}
div.error2, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error2{border:2px solid #D81E05;background-color:#FCF1F0;}

.appbutton {
background-color:#009fe4;
background-image:url("img/f-boton-color.png");
background-position:-10px 0;
background-repeat:no-repeat;
border:medium none #00335b;
border-radius:3px;
box-shadow:0 1px 2px #424242;
color:white !important;
cursor:pointer;
display:inline-block;
font:14px Arial,Helvetica,sans-serif;
margin:5px 5px 0 0;
padding:8px 13px;
text-align:center;
text-decoration:none !important;
text-shadow:0 1px #00335b;
width:auto;
}
*/
.innerModal{
box-sizing:border-box;
margin:0;
padding:0;
padding-top:6%;
position:relative;
line-height:22px!important;
font:12px Arial,Helvetica,sans-serif;
}



/* START bvalidator */
/* invalid input */
.bvalidator_invalid{
	background-color:red!important;
}

.bvalidator_bootstraprt_errmsg{
z-index:100000000;
}

/* error message */
.bvalidator_bootstraprt_cont1{
text-align:left!important;	
}
.bvalidator_errmsg{
background-color:#333;
font-size:10px;
border:1px solid #999;
color:#FFF;
display:none;
-moz-border-radius:4px;
-webkit-border-radius:4px;
border-radius:4px;
-moz-border-radius-bottomleft:0;
-moz-border-radius-topleft:0;
-webkit-border-bottom-left-radius:0;
-moz-border-radius-bottomleft:0;
border-bottom-left-radius:0;
-webkit-border-top-left-radius:0;
-moz-border-radius-topleft:0;
border-top-left-radius:0;
-moz-box-shadow:0 0 6px #ddd;
-webkit-box-shadow:0 0 6px #ddd;
box-shadow:0 0 6px #ddd;
white-space:nowrap;
padding-top:2px;
padding-right:10px;
padding-bottom:2px;
padding-left:5px;
font-family:Arial, Helvetica, sans-serif;
-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=90)";
filter:alpha(opacity=90);
opacity:0.9;
filter:alpha(opacity=90)
}
/* close icon */
.bvalidator_errmsg .bvalidator_close_icon {
margin-left:5px;
margin-top:-2px;
font-family:Arial, Helvetica, sans-serif;
font-weight:bold;
color:#F96;
cursor:pointer
}
/* arrow */
.bvalidator_errmsg em {
display:block;
border-color:red transparent transparent;
border-style:solid;
border-width:10px 10px 0;
height:0;
width:0;
position:absolute;
bottom:-9px;
left:5px
}
.privacy div div .bvalidator_bootstraprt_cont1 div div{font-size:1.16em!important;}
/* END bvalidator */

</style>

<script type="text/javascript">
var errCheckTel = 1;
var iconselected = 1;
var css_selected = 'circulo_azul ico_selected';
var css_not_selected = 'circulo_azul';
var isCtcPass=0;

var bValidatorOptions={
	errorMessages:{
		es:{
			'default':    'Por favor, corrige este valor.',
//			'equalto':    'Introduce el mismo valor de nuevo.',
//			'differs':    'Introduce un valor diferente.',
//			'minlength':  'La longitud debe ser de al menos {0} caracteres',
//			'maxlength':  'La longitud debe ser como mucho de {0} caracteres',
//			'rangelength':'La longitud debe ser de entre {0} y {1} caracteres',
//			'min':        'Introduce un numero mayor o igual que {0}.',
//			'max':        'Introduce un numero menor o igual que {0}.',
//			'between':    'Introduce un numero entre {0} y {1}.',
//			'required':   'Este campo es obligatorio.',
//			'alpha':      'Introduce sólo letras.',
//			'alphanum':   'Introduce sólo caracteres alfanuméricos.',
//			'digit':      'Introduce sólo dígitos.',
//			'number':     'Introduce un número válido.',
//			'email':      'Introduce un e-mail válido.',
//			'image':      'Este campo sólo debería contener imágenes',
//			'url':        'Introduza una URL válida.',
//			'ip4':        'Introduce una dirección IPv4 válida',
//			'ip6':        'Introduce una dirección IPv6 válida',
//			'date':       'Necesario introducir una fecha con el formato <b>{0}</b>.'
		}
	}
};

function validatePhone(number){
	var strLength=number.length;
	if(strLength < 9)
		return true;
	number = parseFloat(number);
	var rx = new RegExp(/^[9|6|7][0-9]{8}$/);
	var res=rx.test(number);
	return res;
}


$(document).ready(function(){
	var optionsBootstrap={
		classNamePrefix:'bvalidator_bootstraprt_',
		position:{x:'left', y:'center'},
		offset:{x:4, y:-40},
		lang:"es",
		template:'<div class="{errMsgClass}"><div class="bvalidator_bootstraprt_arrow"></div><div class="bvalidator_bootstraprt_cont1">{message}</div></div>',    
//		templateCloseIcon:'<div style="display:table"><div style="display:table-cell">{message}</div><div style="display:table-cell"><div class="{closeIconClass}">&#215;</div></div></div>'
		templateCloseIcon:'<div style="display:table"><div style="display:table-cell">{message}</div><div style="display:table-cell"></div></div>'
	};

	$('#enviarPorMailSolicitaInfo').bValidator(optionsBootstrap);
	
	$("body").click(function(){
		$('.bvalidator_bootstraprt_errmsg').hide();
	});


	$('input[name="cookie_rejection"]').click(function(){
		setTimeout('$("#pie_galletero_left").css("width","95%").css("text-align","left");',350)
	});


	$("#boton_ctc").click(function(){
			var phoneBumberCtc=$.trim($('#telefono_ctc').val());
			res=validator2.element("#telefono_ctc");

			if(!isCtcPass)
			{
				res=validator2.element("#telefono_ctc");
				setTimeout('$("#boton_ctc").click();',300);
				return false;
			}

			if(!res && !isCtcPass)
				return false;

			if($.isNumeric(phoneBumberCtc) && phoneBumberCtc.length >= 9)
			{
					var valortype = array_typeofctc[0];
					var campana = arr_campanas[0] ? arr_campanas[0] : 0;
					$.facebox.loading();
					$.ajax({
							url: root_path_local + "includes/phone_check.php",
							method: "post",
							dataType: "json",
							data: {cr:<?= (int) $id_crea ?>, telefono_ctc: phoneBumberCtc},
							cache: false,
							async: false,
							success: function(response)
							{
									if (!response.error)
									{
											$.ajax({
													url: root_path_local + "ajaxs/procesar_registro_ctc.php",
													method: "post",
													dataType: "json",
													data: {
															telefono: phoneBumberCtc,
															sourcetype: valortype,
															campaign: campana,
															fuente: id_source,
															idclient: id_client,
															crea: nomb_promo
													},
													cache: false,
													async: false,
													success: function(response)
													{
															if(!response.error)
															{
																ComprobarInsercion(response, 0);
															}
															else
															{
																ComprobarInsercion(response, 0);
															}
													},
													error:function(response){
															console.log("err2");
															return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
													}
											});
									}
									else
									{
											console.log("err code 2C");
									}
							},
							error: function(response) {
									console.log("err2");
									return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
							}
					});
			}
			return false;
	});


	/**
	 * Fiunción para cambiar el texto según se haga click en el icono
	 */

	$('[id^="ico-"]').click(function(e) {
			var id = $(this).attr('id');
			var tmp = id.split('-');
			var tmp_iconselected = tmp[1];
			if (iconselected != tmp_iconselected) {
					$('#text-' + iconselected).hide();
					$('#text-' + tmp_iconselected).fadeIn('slow');
					$('#selected-' + iconselected).removeClass(css_selected).addClass(css_not_selected);
					$('#selected-' + tmp_iconselected).removeClass(css_not_selected).addClass(css_selected);
					iconselected = tmp_iconselected;

			}
	})

	$(window).scroll(function() {
		$('#animatedElement').each(function(){
		var imagePos = $(this).offset().top;

		var topOfWindow = $(window).scrollTop();
			if (imagePos < topOfWindow+400) {
				$(this).addClass("slideDown");
			}
		});
	});

}); //document.ready

</script>

<header>
	<section class="cabecera">
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
	echo $objSiteData->headerTopContent;
    
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
	</section>
</header>

<section class="basic_content">
	<section class="small_content">
    <section class="skeleton">
<?php
if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div class="marco">
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!
			</div>
';
}

?>
    </section>


    <section class="formulario slideDown">
        <form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
            <input type="hidden" name="destino" id="destino" value="" />
<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
	$formScriptName='form_script_'.$idLandingFormatted.'.php';
}
$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
	echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
	include($formScriptName);
?>
        </form>
    </section> <!--CIERRE FORM -->

    <section class="contenido_pestañas">

    </section> <!--CIERRE CONTENIDO TEXTO -->
	</section>
</section>
<!--CIERRE CONTENIDO --> 


<section class="text_content">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
</section>

<footer>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
</footer>
