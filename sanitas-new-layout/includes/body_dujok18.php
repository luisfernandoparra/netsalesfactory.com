<?php
$idLandingFormatted=str_pad($objSiteData->landingId,3,'0',STR_PAD_LEFT);
?>
<style>
.appbutton {
background-color:#009fe4;
background-image:url("img/f-boton-color.png");
background-position:-10px 0;
background-repeat:no-repeat;
border:medium none #00335b;
border-radius:3px;
box-shadow:0 1px 2px #424242;
color:white !important;
cursor:pointer;
display:inline-block;
font:14px Arial,Helvetica,sans-serif;
margin:5px 5px 0 0;
padding:8px 13px;
text-align:center;
text-decoration:none !important;
text-shadow:0 1px #00335b;
width:auto;
}
.innerModal{
box-sizing:border-box;
margin:0;
padding:0;
padding-top:6%;
position:relative;
line-height:22px!important;
font:12px Arial,Helvetica,sans-serif;
}

.blockCall{
color:#fff;
text-decoration:none;
border:none;
}
</style>
<?php
/*
 * START TAG DE GOOGLE IMPLEMENTADO EL 16.07.2014 (M.F.)
 * SOLO PARA LANDINGS SEM, SALTA AL 
 * HACER CLICK EN `LlAmanos GRATIS`
 */
?>
<script type="text/javascript">

/* <![CDATA[ */
		goog_snippet_vars = function() {
			var w = window;
			w.google_conversion_id = 971711280;
			w.google_conversion_label = "oVfUCJiBlAkQsMaszwM";
			w.google_remarketing_only = false;
		}
		// DO NOT CHANGE THE CODE BELOW.
		goog_report_conversion = function(url) {
			goog_snippet_vars();
			window.google_conversion_format = "3";
			window.google_is_call = true;
			var opt = new Object();
			opt.onload_callback = function() {
				if (typeof(url) != 'undefined') {
					window.location = url;
				}
			}
			var conv_handler = window['google_trackConversion'];
			if (typeof(conv_handler) == 'function') {
				conv_handler(opt);
			}
		}
/* ]]> */

$(document).ready(function(){
	includeScript("//www.googleadservices.com/pagead/conversion_async.js","js");

	$(".blockCall").click(function(){
		goog_report_conversion();
	})

});
</script>
<?php
/*
 * END TAG DE GOOGLE
 */
?>

<header>
	<section class="cabecera">
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
	echo $objSiteData->headerTopContent;
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
///echo $path_raiz_includes_local.$localLandingCuerpoImage;
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
	</section>
</header>

<section class="comolavidamisma">
	<img src="<?=$path_raiz_aplicacion_local?>/img/top_header.png"/>
	<p>Promoci&oacute;n exclusiva de <strong>"Como la vida misma"</strong></p>
</section>

<section class="contenido ">

	<section class="foto <?=$layoutType ? '' : 'hide'?>">

<?php

if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div class="marco">
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!
			</div>
			<span class="bolo"></span>
';
}

$defaultLocalPhoneCall='900806449';
///$inbound_phone=($inbound_phone != $inbound_phone_default) ? $inbound_phone : $defaultLocalPhoneCall;	// SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$inbound_phone=$defaultLocalPhoneCall;	//POR EL MOMENTO SOLO SE DIBUJA EL TELEFONO ESPECIFICO
$drawInbound_phone=preg_replace('/\d{3}/','$0.',str_replace('.',null,trim($inbound_phone)),2);
?>
	</section>

	<section class="formulario">
		<div class="llamanos">
			<a class="blockCall" href="tel:<?=$inbound_phone?>">
				<p>Ll&aacute;manos GRATIS</p>
				<img src="<?=$path_raiz_aplicacion_local?>img/ico-phone.png"/><span><?=$drawInbound_phone?></span>
			</a>
		</div>
		<div class="te_llamamos">
				<p>O te llamamos sin compromiso</p>
		</div>

		<form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
				<input type="hidden" name="destino" id="destino" value="">

<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
	$formScriptName='form_script_'.$idLandingFormatted.'.php';
}

$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
	echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
	include($formScriptName);
?>

				</form>
		</section>

	<div class="clearfix"></div>
</section>

	<section class="contenido_texto">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
	</section>



<footer>
	<div>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
	</div>
</footer>
