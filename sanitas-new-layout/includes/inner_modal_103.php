<?php
/**
 * CONTENIDO DE LA VENTANA MODAL PARA LA LANDING
 * 
 * cr = PARAMETRO RECIBIDO
 */
$content='
<style>.modal-text img{max-width:100px!important;}
.main-pop h1,.main-pop h2{text-aling:left;color:#FFF}.logo-pop,.main-pop h2,.phone-pop,section.cabecera-pop h1{display:inline-block;vertical-align:middle}*{padding:0;margin:0;box-sizing:border-box}img{width:100%;height:auto}@font-face{font-family:Gotham-Light;src:url(fonts/Gotham-Light.otf),url(fonts/Gotham-Bold.otf)}body{font-family:Gotham-Light;background-color:#BBB}.container{width:99%;max-width:600px;margin:auto;background-color:#0079c8;border:4px solid #FFF}.cabecera-pop{background-color:#FFF}.logo-pop{max-width:83px;padding:.6em}section.cabecera-pop h1{color:#636364;font-size:1.8em;text-align:center;margin-bottom:.3em}.main-pop{padding-top:1em;width:99%;text-align:center;margin:auto}.main-pop h1{font-size:3.5em}.main-pop h2{font-size:3.9em;font-weight:700;font-family:Gotham-Bold}.phone-pop{max-width:80px}.main-pop h3{width:56%;margin:auto;text-align:left;font-size:1.6em;color:#FFF;padding-top:.3em;line-height:35px}.modalBform{width:85%;padding-top:1em}section.modalBform input:last-child{font-size:2.5em;cursor:pointer;width:49%;color:#fff;border:0;background-color:#f80;font-family:Gotham-Bold,Arial;padding:.1em 0;margin-bottom:.6em}.modalBform .modalBcelda{padding-left:.3em;background-color:#FFF;font-size:.9em;display:block;width:49%;:35px;margin:.6em auto;border:0;color:#868585;text-align:left;font-family:Gotham,Arial;font-weight:200;height:30px;}#telefono_ctc{padding-left:1em}@media (max-width:580px){section.cabecera-pop h1{font-size:1.3em}.main-pop h1{font-size:3em}.main-pop h2{font-size:3.2em}.main-pop h3{width:66%;font-size:1.2em}.modalBform .modalBcelda,section.modalBform input:last-child{width:60%}}@media (max-width:440px){section.cabecera-pop h1{font-size:1em}}@media (max-width:360px){section.cabecera-pop h1{font-size:.8em}}@media (max-width:260px){section.cabecera-pop h1{font-size:.6em}section.modalBform input:last-child{font-size:1.5em;width:80%}.modalBform .modalBcelda{width:80%}}
</style>
<section class="container">

<section class="cabecera-pop">
<img class="logo-pop" src="img/103/im_logo.png" width="83" height="105" alt=""/> 
  <h1>Tu Seguro M&eacute;dico m&aacute;s completo</h1>
  </section>

    <section class="main-pop">

    <h1>Te llamamos</h1>
        <h2>GRATIS 
        <figure class="phone-pop"><img src="img/103/im_phone.png" width="80" height="70" alt=""/></figure>
        </h2>
        <h3>Danos tu tel&eacute;fono y contactaremos contigo</h3>
        
        <section class="modalBform">
<form method="post" action="" name="ctcForm" id="ctcForm" novalidate>
            <div class="error2"><ul></ul></div>

            <div class="modalBfile">
                
                    <input type="tel" class="modalBcelda" id="telefono_ctc" name="telefono_ctc" maxlength="9" value="" placeholder="Nº de teléfono" required="" data-msg-required="Si deseas que te llamemos es necesario que introduzcas tu número de &lt;strong&gt;teléfono&lt;/strong&gt;" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-msg-minlength="El campo de &lt;strong&gt;teléfono&lt;/strong&gt; para llamarte debe contener al menos 9 dígitos" aria-required="true">
                    <input name="llamar" type="button" value="Llamar" id="boton_ctc" class="modalBbuttonCtc">
                
            </div>
        </form>
</section>

    </section>

</section>  
<br />
';

$success=true;
$response['content']=$content;
$response['success']=$success;
$response=json_encode($response);
die($response);