<?php
@session_start();
$idLandingFormatted = str_pad($objSiteData->landingId, 3, '0', STR_PAD_LEFT);
$rutaImgs = $path_raiz_aplicacion_local . 'img/' . $idLandingFormatted . '/';
$defaultLocalPhoneCall = '';
$inbound_phone = ($inbound_phone) ? $inbound_phone : ''; // SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
?>
<link href="<?=$path_raiz_aplicacion_local?>css/animations_<?=$id_crea?>.css" type="text/css" rel="stylesheet" />

<!-- START INNER MODAL RESP -->
<link href="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.css" type="text/css" rel="stylesheet" />
<link href="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.min.js"></script>
<style>
#modal-window .modal-box{top:10%!important;}
.modal-box{height:auto;}
div .modal-text .container{height:600px;}
</style>
<!-- END INNER MODAL RESP -->

<style>
div.error2{ display : none; border:2px solid #D81E05;margin-left:3em!important;}
.error2{color:#D81E05; background:#FCF1F0;}
div.error2 ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error2 ul li{margin:0 0 10 0;list-style-type:none;}
div.error2 ul li label{font-weight:normal}
div.error2, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error2{border:2px solid #D81E05;background-color:#FCF1F0;}

.appbutton {
background-color:#009fe4;
background-image:url("img/f-boton-color.png");
background-position:-10px 0;
background-repeat:no-repeat;
border:medium none #00335b;
border-radius:3px;
box-shadow:0 1px 2px #424242;
color:white !important;
cursor:pointer;
display:inline-block;
font:14px Arial,Helvetica,sans-serif;
margin:5px 5px 0 0;
padding:8px 13px;
text-align:center;
text-decoration:none !important;
text-shadow:0 1px #00335b;
width:auto;
}
.innerModal{
box-sizing:border-box;
margin:0;
padding:0;
padding-top:6%;
position:relative;
line-height:22px!important;
font:12px Arial,Helvetica,sans-serif;
}
</style>

<script type="text/javascript">
var errCheckTel = 1;
var iconselected = 1;
var css_selected = 'circulo_azul ico_selected';
var css_not_selected = 'circulo_azul';
var isCtcPass=0;

function openCallMe()
{
	modal({size:'normal',buttons:[{text:'cerrar',val:'ok',eKey:true}],center:false});
	$.ajax({
			url:root_path_local + "includes/inner_modal_<?=(int)$id_crea?>.php",
			method:"post", dataType:"json",	data:{cr:<?=(int)$id_crea?>},	cache:false, async:false,
			success:function(response)
			{
				if(!response.error)
				{
					$(".loading").fadeOut();
					$(".modal-text").fadeIn().append(response.content);
					return true;
				}
				else
				{
					$(".loading").fadeOut();
					$(".modal-text").fadeIn().append("<br /><br /><center><span style=color:red;font-weight:bld;>ERROR INESPERADO EN LA CARGA</span><br><br><br><br>Perdonad las molestias</center><br><br><br><br>",function(){

					});
					return true;
				}
			},
			error:function(response) {
				$(".modal-text").fadeIn().append("<br /><br /><center><span style=color:red;font-weight:bld;>ERROR INESPERADO EN LA CARGA</span><br><br><br><br>Perdonad las molestias</center><br><br><br><br>");
				return true;
			}
	});	
}

$(document).ready(function() {

	$("#ctcForm").validate({
			rules:{
				telefono_ctc:{
					required:true
					,remote:{
						url:root_path_local +"includes/phone_check.php",
						contentType:"application/json; charset=utf-8",  
						data:{cr:<?=(int)$id_crea?>},
						dataFilter:function(response){
							var jsonResponse="";
							isCtcPass=1;
							jsonResponse = JSON.parse(response);
							if(jsonResponse.success == false)
							{
								isCtcPass=0;
								return "\"" + jsonResponse.errorMessage + "\"";
							}else{
								return true;
							}
							return false;
						}
					}
				}
			},
			errorLabelContainer: $("#ctcForm div.error2")
	});

	$('input[name="cookie_rejection"]').click(function(){
		setTimeout('$("#pie_galletero_left").css("width","95%").css("text-align","left");',350)
	});

		$(document).on("click",".modalBbuttonCtc",function(){
			validator2 = $("#ctcForm").validate({
					errorContainer: $('div.error2'),
					errorLabelContainer: $('div.error2 ul'),
					wrapper: 'li'
			});

			var phoneBumberCtc=$.trim($('#telefono_ctc').val());
			res=validator2.element("#telefono_ctc");

			if(!isCtcPass)
			{
				res=validator2.element("#telefono_ctc");
//				setTimeout('$("#boton_ctc").click();',300);
//				return false;
			}

			if(!res && !isCtcPass)
				return false;

			if($.isNumeric(phoneBumberCtc) && phoneBumberCtc.length >= 9)
			{
				$(".modal-btn").click();
					var valortype = array_typeofctc[0];
					var campana = arr_campanas[0] ? arr_campanas[0] : 0;
					$.facebox.loading();
					$.ajax({
							url: root_path_local + "includes/phone_check.php",
							method: "post",
							dataType: "json",
							data: {cr:<?= (int) $id_crea ?>, telefono_ctc: phoneBumberCtc},
							cache: false,
							async: false,
							success: function(response)
							{
									if(!response.error)
									{
										$.ajax({
												url: root_path_local + "ajaxs/procesar_registro_ctc.php",
												method: "post",
												dataType: "json",
												data: {
													telefono: phoneBumberCtc,
													sourcetype: valortype,
													campaign: campana,
													fuente: id_source,
													idclient: id_client,
													crea: nomb_promo
												},
												cache: false,
												async: false,
												success: function(response)
												{
														if(!response.error)
														{
															checkInsertion(response, 0);
														}
														else
														{
															checkInsertion(response, 0);
														}
												},
												error:function(response){
														console.log("err2");
														return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
												}
										});
									}
									else
									{
											console.log("err code 2C");
									}
							},
							error: function(response) {
									console.log("err3");
									return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
							}
					});
			}
			return false;
	});

	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
	if(!isSafari)
	{
		// START BACK PROCESS
		history.pushState(null, null, location.href);
		window.onpopstate=function(event){
			history.go(2);
			openCallMe()
			return true;
		};
		// END BACK PROCESS
	}
	else
	{
		setTimeout("openCallMe();",14000);
	}
	$("#pie_galletero").css("background-color","transparent");
});


// START 103 LOCAL SCRIPT
$(window).scroll(function() {
	$('.container-tablet').each(function(){
	var imagePos = $(this).offset().top;

	var topOfWindow = $(window).scrollTop();
		if (imagePos < topOfWindow+400) {
			$(this).addClass("fadeIn");
		}
	});
});
$(window).scroll(function() {
	$('container-mov2').each(function(){
	var imagePos = $(this).offset().top;

	var topOfWindow = $(window).scrollTop();
		if (imagePos < topOfWindow+400) {
			$(this).addClass("fadeIn");
		}
	});
});
$(window).scroll(function() {
	$('.container-box').each(function(){
	var imagePos = $(this).offset().top;

	var topOfWindow = $(window).scrollTop();
		if (imagePos < topOfWindow+500) {
			$(this).addClass("slideLeft");
		}
	});
});
// END 103 LOCAL SCRIPT
</script>


<header>
	<section class="cabecera">
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
	echo $objSiteData->headerTopContent;
    
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
<!-- START ONLY TESTS -->
<!--
<span style="font-size:1em;"><a href="http://www.sanitas-mas-salud.es/tu-seguro-medico-mas-completo/?cr=<?=(int)$id_crea?>&&idClient=<?=(int)$id_client?>xx=2&momento=<?=date('H:i:s')?>"><?=@$_REQUEST['momento'] ? 'Intenta volver atrás (no desde este link!)' : 'pruebame'?></a></span>

<span style="font-size:1em;"><a href="http://<?=$_SERVER['HTTP_HOST']?>/netsalesfactory.com/promociones/sanitas-new-layout/?cr=<?=(int)$id_crea?>&&idClient=<?=(int)$id_client?>xx=2&momento=<?=date('H:i:s')?>"><?=@$_REQUEST['momento'] ? 'Intenta volver atrás (no desde este link!)' : 'pruebame'?></a></span>
-->
<!-- END ONLY TESTS -->
	</section>
</header>


<section class="basic-content">
	<section class="small-content">
    <section class="skeleton">
<?php
if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div class="marco">
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!
			</div>
';
}

?>
    </section>


    <section class="formulario">
        <form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
            <input type="hidden" name="destino" id="destino" value="" />
<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
	$formScriptName='form_script_'.$idLandingFormatted.'.php';
}
$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
	echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
	include($formScriptName);
?>
        </form>
    </section> <!--CIERRE FORM -->

	</section>
</section>
<!--CIERRE CONTENIDO --> 


<section class="text_content">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
</section>

<footer>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
</footer>
