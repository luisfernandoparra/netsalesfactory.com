<?php
@session_start();
?>
						<div class="error"><ul></ul></div>

						<div class="fila">
							<div class="fleft"><label for="nombre">Nombre</label>
							<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" /></div>
						</div>

						<div class="fila">
							<div class="fleft">
								<label for="telefono">Teléfono</label>
								<input name="telefono" id="telefono" type="tel" class="celda" maxlength="9" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" />
							</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<label for="email">E-mail</label>
								<input name="email" id="email" type="email" maxlength="100" class="celda" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" />
							</div>
						</div>


						<div class="legal" style="padding-top:10px;">
								<p class="mini"><input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;He leído y acepto la 
								<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php';?>" data-ref="" target="_blank">política de privacidad</a></p>

						</div>

						<div class="espacio_btn">
								<button id="btnProcesar" class="green sendData" data-direction="down">SOLICITAR INFORMACIÓN</button> 
								<!--<input class="button green sendData" type="button" id="btnProcesar" name="btnProcesar" value="SOLICITAR INFORMACIÓN">-->
						</div>


