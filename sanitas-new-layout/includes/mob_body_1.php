<?php
//echo $mobileArraDayException[$id_crea];
?>
<div id="trddbl" style="display:none"></div>
<img class="info" src="<?=$path_raiz_aplicacion_local?>img/header_mobile<?=$id_crea.$mobileArraDayException[$id_crea]?>.jpg"  >
  <div class="contenido container"><!-- contenido-->
    <div class="cabecera clearfix">
	  <img src="<?=$path_raiz_aplicacion_local?>img/movil_info_dental.jpg" class="informate">
        <div class="formulario">

            <div class="contenedor_form">
                <form action="" method="get">
                    <div class="fila">
                        <div class="fleft">
						  <input class="cajatxt" id="nombre" name="nombre" type="text" value="<?php echo $nombre ? $nombre : 'Nombre y apellidos'; ?>" onBlur="this.value=(this.value==&#39;&#39;)?(&#39;Nombre y apellidos&#39;):(this.value)" onClick="this.value=(this.value==&#39;Nombre y apellidos&#39;)?(&#39;&#39;):(this.value)">
						</div>
                    </div>
                    <div class="fila">
						<input class="cajatxt" id="telefono" name="telefono" type="tel" value="Móvil" onBlur="this.value=(this.value==&#39;&#39;)?(&#39;Móvil&#39;):(this.value)" onClick="this.value=(this.value==&#39;Móvil&#39;)?(&#39;&#39;):(this.value)" maxlength="9">
                    </div> 
                    <div class="fila">
						<input class="cajatxt" id="email" name="email" type="email" value="Email" onBlur="this.value=(this.value==&#39;&#39;)?(&#39;Email&#39;):(this.value)" onClick="this.value=(this.value==&#39;Email&#39;)?(&#39;&#39;):(this.value)">
                    </div>

                    <div class="fila">
                        <div class="fleft">
						  <select class="cajatxt" style="height:40px;width:100%;" id="sel_prov" name="sel_prov" >
						  <option value="-1" selected="selected">Selecione provincia</option>
<?php 
foreach($arr_prov as $key=>$value)
{
  $selected = "";
  $valor = strtolower(trim($value['name']));
  $valor = str_replace($arr_input,$arr_output,$valor);
  if ($valor==$provincia){
	$selected = " selected ";	
  }
  echo "<option value='".$value['id']."'".$selected.">".$value['name']."</option>";
}
?>
                        </select></div>
                    </div>
                 </form>
                 <div class="legal">
                    <input class="checkLeft" type="checkbox" id="cblegales" value="1" name="cblegales" /> Declaro ser mayor de edad y que he leído y acepto la <a href="<?php echo $url_protecciondatos.'?deviceType='.$layoutType; ?>" target="_blank">política de privacidad</a>
                 </div>	  
                 <input name="btnProcesar" id="btnProcesar" value="&rarr; Solicitar información" class="btn_solicitar" type="button" />
				 <div class="legal" style="text-align:center">
				  <a href="<?php echo $url_condiciones.'?deviceType='.$layoutType; ?>" target="_blank">Ver las condiciones generales de Sanitas</a>
				 </div>
            </div><!-- contenedor_form-->
          </div>
        
        </div><!-- cabecera-->
        <div class="linea_morada"></div>
		<div class="hide">
		  <a class="enlace_condiciones" href="#" target="_blank" onClick="MM_openBrWindow('<?php echo $url_condiciones; ?>','','scrollbars=yes,width=1060,height=720'); return false;">Ver las condiciones generales de Sanitas</a>
		</div>
		<span class="containerCondic" style="display:<?=$id_crea==2 ? 'none' : 'inline-block'?>;">
		  <a class="pdfCondiciones" href="<?=$url_local?>/sanitas-new-web/get_pdf.php" target="_blank"><img src="<?=$url_local?>/sanitas-new-web/img/p.gif" border="0" width="225" height="45" /></a>
		</span>

  <div class="pie hide">
	</br>
	<p>www.sanitas.es | © Todos los derechos reservados</p>
  </div>
</div><!-- //contenido-->
