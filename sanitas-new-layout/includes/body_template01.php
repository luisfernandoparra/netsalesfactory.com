<?php
/*
 * 
 * PRIMER EJEMPLO
 * 
 */
?>
<script type="text/javascript">
  $().ready(function() {

    $("#formDatao").validate({
      errorLabelContainer: $("#formDatao div.error")
    });

    // validate the form when it is submitted
    var validator = $("#formData").validate({
      errorContainer: $('div.error'),
      errorLabelContainer: $('div.error ul'),
      wrapper: 'li'
    });
  });
</script>

<header class="lite">
  <a id="logo" href="#null">XXXXXXXXX</a>
  <div id="super">
	  <p class="hh1">TITULAR<br><strong>sub-titular</strong></p>
  </div>
</header>

<div id="mainBox" class="row" style="background-image:url(img/test_sanitas.jpg); background-position:left top;">
  <section style="">
	<div class="marco goldXX" style="margin-top:1%;">
<span class="floatCircle"></span>
	  <section style="display:inline-block;width:45%;float:left;">
		<ul>
		  <h1>Coberturas</h1>
		  <li>Odontología preventiva.</li>
		  <li>Intervenciones quirúrgicas.</li>
		  <li>Odontología conservadora.</li>
		  <li>Endodoncia.</li>
		  <li>Odontología estética.</li>
		  <li>Odontopediatría.</li>
		  <li>Prótesis.</li>
		  <li>Periodoncia.</li>
		  <li>Ortodoncia.</li>
		  <li>Implantología.</li>
		  <li>Diagnóstico por imagen.</li>
		  <li>Patología Articulación Temporomandibular.</li>
		</ul>

	  </section>

	  <section style="display:inline-block;width:45%;float:left;margin-left:10px;">
		<ul>
			<h1>Servicios incluidos</h1>

			<li>Consulta odontológica: exploración y diagnóstico.</li>
			<li>Limpieza bucal</li>
			<li>Cirugía</li>
			<li>Extracción simple, pieza incluida.</li>
			<li>Estudio Radiológico</li>
			<li>Estudio Implantológico</li>
			<li>Ortodoncia: consulta, estudio completo, extracción simple y protector bucal.</li>
			<li>Diagnóstico por imagen: Cefalometría, Tomografía, Ortopantomografía</li>
			<li>Férula para blanqueamiento de fotoactivación.</li>
	   </ul>
	  </section>


	</div>

  </section>
  

    <div id="accion" class="col4">
    <div style="padding:15px 10px 5px 10px;overflow:hidden;" class="sep1">
   
 
	<form id="idFormulario" method="post" action="">
	  <section class="gold25">
		<div class="error"><ul></ul></div>
                   <div class="row">
                        <div class="fleft">Nombre*<input type="text" class="celda" id="nombre" name="nombre" maxlength="499" value="<?php echo $nombre; ?>" /></div>
                    </div>
                    <div class="row">
                        <div class="fleft">Teléfono* <input type="text" class="celda" id="telefono" name="telefono" maxlength="9"  /></div>
                    </div>
                    <div class="row">
                        <div class="fleft">Provincia* <select class="celda" id="sel_prov" name="sel_prov" >
                                        <option value=""> </option>
<?php 
foreach ($arr_prov as $key=>$value) {
		$selected = "";
		$valor = strtolower(trim($value['name']));
		$valor = str_replace($arr_input,$arr_output,$valor);
		if ($valor==$provincia) {
				$selected = " selected ";	
		}
		echo "<option value='".$value['id']."'".$selected.">".$value['name']."</option>";
}
?>
                        </select></div>
                    </div>
		<div class="row">
			<div class="fleft">E-mail* <input type="text" class="celda" name="email" id="email" value="<?php echo $email; ?>" /></div>
		</div>
	  </section>    
	</form>    
    </div><!--/pad10-->
    </div><!--/8-->

<div class="clearfix"></div>
<div id="informacion" class="col8">

    </div><!--/col8-->

</div>




<footer>
<!--<p class="mini">* Servicio ofrecido por Sanitas S.L. de Diversificación (Sociedad Unipersonal), compañía perteneciente al Grupo Sanitas. Este servicio no está cubierto con carácter general por ninguno de nuestros productos aseguradores con excepción de aquellas pólizas donde se haya acordado expresamente su inclusión dentro de la cobertura asegurada. Consultar precios y disponibilidad del servicio en cada provincia.</p> -->
</footer>

<?php
?>