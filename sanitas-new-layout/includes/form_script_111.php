<?php
@session_start();
?>
						<div class="error"><ul></ul></div>
            <p>Solicita información GRATIS</p>

						<div class="fila">
							<div class="fleft">
								<div class="col4"><label for="nombre">Nombre</label></div>
								<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" placeholder="" />
								</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<div class="col4"><label for="nombre">Tel&eacute;fono</label></div>
								<input name="telefono" id="telefono" type="tel" class="celda" maxlength="9" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="" patternt="[0-9]+" />
							</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<div class="col4"><label for="nombre">E-mail</label></div>
								<input name="email" id="email" type="email" maxlength="200" class="celda" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="" />
							</div>
						</div>
                        
                        <div class="col8">
							<div class="col4"><label for="nombre">Provincia</label></div>
							<select class="celda-servicio" type="text" name="doc_type" id="doc_type" maxlength="100" value="sel_prov" required data-msg-required="El campo &lt;strong&gt;Provincia&lt;/strong&gt; es obligatorio" placeholder="" aria-required="true">
								<option value="">Seleccione Provincia</option>
<?php
foreach($arr_prov as $key=>$value)
{
	$selected = '';
	$valor=strtolower(trim($value['name']));
	$valor=str_replace($arr_input,$arr_output,$valor);

	if($valor === $provincia)
	{
		$selected=' selected ';
	}
	echo '<option value=\''.$value['id'].'\''.$selected.'\'>'.$value['name'].'</option>';
}
?>
							</select>
						</div>

						<div class="espacio_btn">
							<input id="btnProcesar" name="registrate" type="button" value="ENVIAR" class="green sendData" data-direction="down">
						</div>


						<div class="legal" style="padding-top:10px;">
								El env&iacute;o del formulario supone la aceptación de la
								<a class="enlace_condiciones" href="<?=$privacyPolicy ? 'includes/privacy_policies.php?cr='.(int)$id_crea : 'proteccion_datos.php';?>" data-ref="" target="_blank">política de privacidad</a>
						</div>


