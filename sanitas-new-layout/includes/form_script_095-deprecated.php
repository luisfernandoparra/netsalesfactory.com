<?php
@session_start();
?>
	<div style="display:none;"><div class="error"></div></div>
	<!--<div class="error"><ul></ul></div>-->
	<p>¡Solicita informaci&oacute;n!</p>

	<div class="fila">
		<div class="fleft">
		<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda" maxlength="100" required placeholder="  Nombre y Apellidos" data-bvalidator="alpha,minlength[3],required" data-bvalidator-msg-minlength="Por lo menos 3 caracteres"  data-bvalidator-msg="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" /></div>
	</div>

	<div class="fila">
		<div class="fleft">
			<input name="email" id="email" type="email" maxlength="200" class="celda" required value="<?=$email?>" placeholder="  E-mail" data-bvalidator="email,required" data-bvalidator-msg="El campo <b>E-mail</b> es obligatorio" />
		</div>
	</div>

	<div class="fila">
		<div class="fleft">
			<input name="telefono" id="telefono" type="tel" class="celda" maxlength="9" required data-rule-digits="true" data-rule-minlength="9" placeholder="  Tel&eacute;fono" data-bvalidator="required,number,minlength[9],validatePhone[]" data-bvalidator-msg="El campo <b>Teléfono</b> es obligatorio" />
		</div>
	</div>


	<div class="legal privacy" style="padding-top:10px;">
			<input required="" data-rule-digits="true" type="checkbox" name="cblegales" id="cblegales" value="1" data-bvalidator="required" data-bvalidator-msg="Debe leer y aceptar la política de privacidad" />&nbsp;&nbsp;He leído y acepto la 
			<a class="enlace_condiciones" href="<?=$privacyPolicy ? 'includes/privacy_policies.php?cr='.(int)$id_crea : 'proteccion_datos.php';?>" data-ref="" target="_blank">política de privacidad</a>

	</div>

	<div class="espacio_btn">
		<input id="btnProcesar" name="registrate" type="submit" value="¡Quiero informaci&oacute;n!" class="green sendData" data-direction="down">
	</div>


