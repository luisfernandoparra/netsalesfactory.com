<?php
@session_start();
?>
            <p>¡Solicita informaci&oacute;n!</p>
						<div class="error"><ul></ul></div>
						<div class="fila">
							<div class="fleft">
								<input required oninput="checkInput(this)" type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda" maxlength="100" data-msg-required="<?=$appleDevice ? 'El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio' : 'nombre'?>" placeholder="  Nombre y Apellidos" />
							</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<input required oninput="checkInput(this)" name="email" id="email" type="email" maxlength="200" class="celda" data-msg-required="<?=$appleDevice ? 'El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio' : 'Email'?>" value="" placeholder="  Email" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" />
							</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<input required oninput="checkInput(this)" name="telefono" id="telefono" type="tel" class="celda" maxlength="9" data-msg-required="<?=$appleDevice ? 'El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio' : 'Teléfono'?>" data-rule-digits="true" pattern="[0-9]{9}" value="" placeholder="  Teléfono" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos"/>
							</div>
						</div>

						<div class="legal" style="padding-top:10px;">
								<input required oninput="checkInput(this)" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;He leído y acepto la 
								<a class="enlace_condiciones" href="<?=$privacyPolicy ? 'includes/privacy_policies.php?cr='.(int)$id_crea : 'proteccion_datos.php';?>" data-ref="" target="_blank">política de privacidad</a>
						</div>

						<div class="espacio_btn">
							<input name="registrate" id="<?=$appleDevice ? 'btnProcesar' : ''?>" type="<?=$appleDevice ? 'button' : 'submit'?>" value="¡Quiero informaci&oacute;n!" class="green <?=$appleDevice ? 'sendData ' : 'sendData095'?>" data-direction="down">
						</div>


