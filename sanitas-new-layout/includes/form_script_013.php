<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/','$0.',str_replace('.',null,trim($inbound_phone)),2);
?>
					<h3 id="h3_movil" class="color cyan">Inf&oacute;rmate GRATIS&nbsp;<i class="icono-grande icotel"></i><a class="telf" href="tel:<?=$inbound_phone?>"><?=$drawInbound_phone?></a></h3>
					<h3 id="h3_web" class="color cyan">Solicita informaci&oacute;n GRATIS</h3>

					<div id="campos">
						<div class="error"><ul></ul></div>

						<div class="row">
							<div class="col4"><label for="nombre">Nombre</label></div>
							<div class="col8"><input type="text" name="nombre" maxlength="100" id="nombre" value="<?=$nombre?>" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" /></div>
						</div>

						<div class="row">
							<div class="col4">
								<label for="telefono">Tel&eacute;fono</label>
							</div>

							<div class="col8">
								<input type="tel" maxlength="9" name="telefono" id="telefono" required data-msg-required="El campo &lt;strong&gt;Tel&eacute;fono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Tel&eacute;fono&lt;/strong&gt; s&oacute;lo acepta n&uacute;meros" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Tel&eacute;fono&lt;/strong&gt; debe contener al menos 9 d&iacute;gitos" />
							</div>
						</div>

						<div class="row">
							<div class="col4">
								<label for="email">E-mail</label>
							</div>

							<div class="col8">
								<input type="email" name="email" maxlength="100" id="email" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" />
							</div>
						</div>

						<div class="row">
							<div class="col4">
								<label id="txt_sel_prov" for="sel_prov">Provincia</label>
							</div>

							<div class="col8">
								<select id="sel_prov" name="sel_prov" required="" data-msg-required="El campo &lt;strong&gt;Provincia&lt;/strong&gt; es obligatorio" data-msg-sel_prov="El campo &lt;strong&gt;Provincia&lt;/strong&gt; no es válido">
									<option value="">Seleccione Provincia</option>
<?php
foreach($arr_prov as $key=>$value)
{
	$selected = '';
	$valor=strtolower(trim($value['name']));
	$valor=str_replace($arr_input,$arr_output,$valor);

	if($valor === $provincia)
	{
		$selected=' selected ';
	}
	echo '<option value=\''.$value['id'].'\''.$selected.'\'>'.$value['name'].'</option>';
}
?>
								</select>
							</div>
						</div>

						<div class="row">
							<div class="col12">
								<input class="button verde sendData" type="button" id="btnProcesar" value="ENVIAR">
							</div>
						</div>

						<div class="row" style="padding-top:10px;">
							<div class="col12">
								<p class="mini">El env&iacute;o del formulario supone la aceptaci&oacute;n de la 
								<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php';?>" data-ref="" target="_blank">pol&iacute;tica de privacidad</a></p>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
