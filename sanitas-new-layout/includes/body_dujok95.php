<?php
@session_start();
$idLandingFormatted = str_pad($objSiteData->landingId, 3, '0', STR_PAD_LEFT);
$rutaImgs = $path_raiz_aplicacion_local . 'img/' . $idLandingFormatted . '/';
$defaultLocalPhoneCall = '';
$inbound_phone = ($inbound_phone) ? $inbound_phone : ''; // SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
$formularioDatosLanding='form'.$id_crea;
$appleDevice=(strpos($resDevice, 'applewebkit'));
?>

<!-- START INNER MODAL RESP -->
<link href="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.css" type="text/css" rel="stylesheet" />
<link href="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<?=$path_raiz_aplicacion?>js/jquery_modal/jquery.modal.min.js"></script>
<style>
#modal-window .modal-box{top:20%!important;}
</style>
<!-- END INNER MODAL RESP -->

<style>
div.error2{ display : none; border:2px solid #D81E05; }
.error2{color:#D81E05; background:#FCF1F0;}
div.error2 ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error2 ul li{margin:0 0 10 0;list-style-type:none;}
div.error2 ul li label{font-weight:normal}
div.error2, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error2{border:2px solid #D81E05;background-color:#FCF1F0;}

div.error095{display:inline-box;padding:4px;font-family:arial,verdana;}
div.error095 p{font-weight:normal!important;line-height:30px;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
.simplemodal-container{background:#eee;height:50%;}
.appbutton {
background-color:#009fe4;
background-image:url("img/f-boton-color.png");
background-position:-10px 0;
background-repeat:no-repeat;
border:medium none #00335b;
border-radius:3px;
box-shadow:0 1px 2px #424242;
color:white !important;
cursor:pointer;
display:inline-block;
font:14px Arial,Helvetica,sans-serif;
margin:5px 5px 0 0;
padding:8px 13px;
text-align:center;
text-decoration:none !important;
text-shadow:0 1px #00335b;
width:auto;
}
.innerModal{
box-sizing:border-box;
margin:0;
padding:0;
padding-top:6%;
position:relative;
line-height:22px!important;
font:12px Arial,Helvetica,sans-serif;
}
</style>

<script type="text/javascript">
var errCheckTel = 1;
var iconselected = 1;
var css_selected = 'circulo_azul ico_selected';
var css_not_selected = 'circulo_azul';
var isCtcPass=0;
var prevPhoneValue=0;
var prevemailValue="";

function checkInput(input)
{
	var thisValue=input.value.trim();
	var thisRefTag=input.name;

	if(thisValue == "")	// PARA TODOS LOS CAMPOS VACIOS
	{
		input.value="";
		input.setCustomValidity("El campo '"+$(input).attr("data-msg-required")+"' es obligatorio");
		return false;
	}

	if(input.value == "")
	{
		input.setCustomValidity("Debes introducir un "+$(input).attr("data-msg-required"));
		return false;
	}

	if(input.name == "email" && input.validity.typeMismatch)
	{
		input.setCustomValidity("Debes introducir un "+$(input).attr("data-msg-required")+" válido");
		return false;
	}

	input.setCustomValidity("");
	return true;
}

function finalizeForm(){
		$.facebox.loading();
		var ccrea = "";
		if(typeof nomb_promo === 'undefined') {
			ccrea = 'SANITAS_1';
		} else {
			ccrea = nomb_promo;
		}
		var source = "";
		if (typeof id_source === 'undefined') {
			source = "";
		} else {
			source = id_source;
		}

		ind = 1;
		if (is_ctc == 1) {
			ind = 0;
		}

		var valortype = array_typeofctc[ind];
		var campana = arr_campanas[ind];

		$.ajax({
			url: root_path_local + "ajaxs/procesar_registro_ctc.php",
			method: "post",
			dataType: "json",
			data: {
				idclient: id_client,
				sourcetype: valortype,
				campaign: campana,
				fuente: source,
				em: $('#email').val(),
				nombre: $('#nombre').val(),
				apellidos: $('#apellidos').val(),
				telefono: $('#telefono').val(),
				provincia: $('#sel_prov').val(),
				provincia_text: $('#sel_prov').find('option:selected').text(),
				cblegales: 1,
				crea: ccrea
			},
			cache: false,
			async: true,
			success: function (response)
			{
				dataLayer.push({'eAction':'gracias','eLabel':'popup','event':'formulario'});	// M.F. (2015.12.17)
				if(id_client == 9 || window.idCr > 102)	// A LA NUEVA THANK YOU PAGE
					checkInsertion(response, ind);
				else
					ComprobarInsercion(response, ind);
			},
			error:function (response){
				console.log("err code 2");
			}
		});
	return false;
}

function phoneCheck(){	//PHONE CHECK
	var fieldCheck=this;

	if(!this.value.trim() || this.value.length < 9 || prevPhoneValue == this.value)	//	BYPASSES REPEAT THE SAME PHONE NUMBER
		return false;

	$(".btnProcesar").prop("disabled","true").css("opacity","0.8");

	$.ajax({
		url:root_path_local +"includes/phone_check.php",
		method:"post",
		dataType:"json",
		data:{cr:<?=(int)$id_crea?>,telefono:fieldCheck.value},
		cache: false,
		async: true,
		success: function (response)
		{
			if(response.success == false)
			{
				var tmp=response.errorMessage;
				var rex = /(<([^>]+)>)/ig;
				tmp=tmp.replace(rex , "");
				fieldCheck.setCustomValidity(tmp);
				$(".btnProcesar").prop("disabled","").css("opacity","1");
				return false;
			}
			$(".btnProcesar").prop("disabled","").css("opacity","1");
		},
		error:function (response){
			console.log("err code 2");
		}
	});

	prevPhoneValue=this.value;
	return;
}

function emailCheck(){	// EMAIL CHECK
	var fieldCheck=this;
	var thisValue=this.value.trim();

	if(thisValue.indexOf("@") < 1)
	{
		prevemailValue="error";
		return false;
	}

	if(!thisValue || thisValue.length < 9 || prevemailValue == this.value)	//	BYPASSES REPEAT THE SAME PHONE NUMBER
		return false;
		
//console.log(chiocciola);return;
	$(".btnProcesar").prop("disabled","true").css("opacity","0.8");

	$.ajax({
		url:root_path +"includes/api_email_check.php",
		method:"get",
		contentType:"application/json; charset=utf-8",
		dataType:"json",
		data:{cr:<?=(int)$id_crea?>,id_client:id_client,email:this.value},
		cache:false,	async:true,
		success:function(response)
		{
			if(response.success != 1)
			{
				var tmp=response.errorMessage ? response.errorMessage : "Parece que el correo está repetido";
				var rex = /(<([^>]+)>)/ig;
				tmp=tmp.replace(rex , "");
				fieldCheck.setCustomValidity(tmp);
				setTimeout('$(".btnProcesar").prop("disabled","").css("opacity","1");',200);
				return false;
			}
			$(".btnProcesar").prop("disabled","").css("opacity","1");
			return true;
		},
		error:function (response){
			console.log("err code 2");
		}
	});

	prevemailValue=this.value;
	return false;
}

$(document).ready(function(){
//	$('.btnProcesar').click(function(e){
//		e.preventDefault();
////		finalizeForm();
//alert("-->");
////		validateFormOld(0);
//	});
<?php
if(!$appleDevice)
{
?>
	$('#telefono').on('blur',phoneCheck);
	$('#email').on('blur',emailCheck);
	$("body").append("<div id='opaque' style='display: none;'></div>");
	$('#telefono').keypress(function (e) {
			return SoloCompatibleTlf(e);
	});
<?php
}
?>

	//Si existe el teléfono_ctc, lo añadimos
	if ($('#telefono_ctc').length) {
			$("#telefono_ctc").keypress(function (e) {
					return SoloCompatibleTlf(e);
			});
	}

	//Política de cookies
	$(':input').focusin(function () {
			if ($(this).attr('id') == "cookie_rejection" || $(this).attr('id') == "cookie_acept")
					return false;
			checkCookie();
	});
	$(':checkbox').click(function (e) {
			checkCookie();
	});
	if (is_ctc == 1) {
			$("#telefono_ctc").keypress(function (e) {
					return SoloCompatibleTlf(e);
			});
	}

	$('#btnProcesar_ctc').click(function (e) {
			e.preventDefault();
			validateFormOld(0);

	}); //btnProcesar_ctc
	/**
	 * Nombre y teléfono
	 */
	$('#btnProcesar_ctc_movil').click(function (e) {
			e.preventDefault();
			validateFormOld(3);

	});
});

</script>

<header>
<!--<div class="error095"><ul></ul></div>-->

	<section class="cabecera">
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
	echo $objSiteData->headerTopContent;
    
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
	</section>
</header>

<section class="basic_content">
	<section class="small_content">
    <section class="skeleton">
<?php
if($objSiteData->boxTopLeft)	// BLOQUE SUPERIOR IZQUIERDO
{
	echo $objSiteData->boxTopLeft;
}
else
{
	echo '
			<div class="marco">
				!!NO SE HA DEFINIDO EL CONTENIDO DE ESTE RECUADRO!!
			</div>
';
}

?>
    </section>


    <section class="formulario slideDown">
			<form id="<?=$appleDevice ? 'buddy' : 'enviarPorMailSolicitaInfo' ?>"></form>
        <form method="post" action="" name="<?=$appleDevice ? 'enviarPorMailSolicitaInfo' : $formularioDatosLanding?>" id="<?=$appleDevice ? 'enviarPorMailSolicitaInfo' : $formularioDatosLanding?>" class="form095" onsubmit="<?=$appleDevice ? '' : 'finalizeForm();return false;'?>">
            <input type="hidden" name="destino" id="destino" value="" />
<?php
$formScriptName='form_script_common.php';	// SCRIPT CON EL FORMULARIO STANDARD QUE SE CARGA POR DEFECTO SI NO EXISTE UNO ESPECÍFICO

if($objSiteData->scriptFormName == 1)
{
	$formScriptName='form_script_'.$idLandingFormatted.'.php';
}
$formScriptName=$path_raiz_includes_local.'includes/'.$formScriptName;

if(!is_file($formScriptName))
	echo '<span class="error">NO SE HA ENCONTRADO EL ARCHIVO PARA COMPONER ESTE FORMULARIO</span>';
else
	include($formScriptName);
?>
        </form>
    </section> <!--CIERRE FORM -->

    <section class="contenido_pestañas">

    </section> <!--CIERRE CONTENIDO TEXTO -->
	</section>
</section>
<!--CIERRE CONTENIDO --> 


<section class="text_content">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
</section>

<footer>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
</footer>
