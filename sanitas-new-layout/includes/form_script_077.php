<?php
@session_start();
?>
						<div class="error"><ul></ul></div>
            <p>Solicita informaci&oacute;n</p>

						<div class="fila">
							<div class="fleft">
							<input type="text" name="nombre" id="nombre" value="<?=$nombre?>" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" placeholder="Nombre y Apellidos" /></div>
						</div>

						<div class="fila">
							<div class="fleft">
								<input name="email" id="email" type="email" maxlength="200" class="celda" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" placeholder="Email" />
							</div>
						</div>

						<div class="fila">
							<div class="fleft">
								<input name="telefono" id="telefono" type="tel" class="celda" maxlength="9" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Teléfono" />
							</div>
						</div>


						<div class="legal" style="padding-top:10px;">
								<input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;He leído y acepto la 
								<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php';?>" data-ref="" target="_blank">política de privacidad</a>

						</div>

						<div class="espacio_btn">
							<input id="btnProcesar" name="registrate" type="button" value="¡Quiero informaci&oacute;n!" class="green sendData" data-direction="down">
						</div>


