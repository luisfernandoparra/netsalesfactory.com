<?php
@session_start();
//modified by LFP. Añadimos posibilidad de imagen .gif para la imagen de cabecera
$header_img = 'img/header_mobile'.$id_crea.$mobileImagePostName;//.'.jpg';
$header_img = (file_exists($path_raiz_includes_local.$header_img.'.gif')) ? $path_raiz_aplicacion_local.$header_img.'.gif' : $path_raiz_aplicacion_local.$header_img.'.jpg';
//echo $header_img;
?>
<style type="text/css">
/*estilos mensajes error*/
div.errorMobile ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.errorMobile label{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.errorMobile{display : none; border:2px solid #D81E05; }
div.errorMobile{margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
div.errorMobile{ display : none; border:2px solid #D81E05;background-color:#FCF1F0;}
.error{color:#D81E05; background:#FCF1F0;}
div.error ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
div.error ul li{margin:0 0 10 0;list-style-type:none;}
div.error ul li label{font-weight:normal}
div.error, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
.ok{color:#333333; background-color:#EFFFDA;padding:10px;}
input.error, textarea.error{border:2px solid #D81E05;background-color:#FCF1F0;}
input.ok{background-color:#EFFFDA}

.innerModal{
display:inline-block;
background:#fff;
text-align:center;
width:100%;
height:250px;
}

.modal{
height:680px!important;
}

#simplemodal-container a.modalCloseImg{background:url(<?=$path_raiz_aplicacion_local;?>img/close_modal.png)no-repeat 2px 0px rgba(0, 0, 0, 0)!important;right:-8px;top:-14px;}
#facebox img{width:32px!important;height:1px!important;text-align:center;}
</style>

<script>

var validator;
$(document).ready(function(){
	$("#telefono").val("");

	$("#enviarPorMailSolicitaInfo").validate({	// CHECK PHONE NUMBER
		rules:{
			telefono:{
				required:true,
				remote:{
					url:root_path_local + "includes/phone_check.php",
			    contentType:"application/json; charset=utf-8",  
					data:{cr:idCr},
					dataFilter:function(response){
						var jsonResponse="";
						jsonResponse = JSON.parse(response);
							if(jsonResponse.success == false)
							{
								return "\"" + jsonResponse.errorMessage + "<br>\"";
							}else{
								return true;
							}
						return false;
					}
				}
			}
		},
		errorLabelContainer: $("#enviarPorMailSolicitaInfo div.errorMobile")
	});

	validator=$("#enviarPorMailSolicitaInfo").validate({
		errorContainer:$('div.errorMobile'),
		errorLabelContainer:$('div.errorMobile ul'),
		wrapper: 'li'
	});
});
</script>

<div id="trddbl" style="display:none"></div>
<img class="info" src="<?=$header_img?>">
<!---<img class="info" src="<?=$path_raiz_aplicacion_local?>img/header_mobile<?=$id_crea.$mobileImagePostName?>.jpg"> --->
  <div class="contenido container">
    <div class="cabecera clearfix">
	  <img src="<?=$path_raiz_aplicacion_local?>img/movil_info_dental.jpg" class="informate">
        <div class="formulario">

            <div class="contenedor_form">
                <form action="" method="get" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">

									<div class="errorMobile"><ul></ul></div>
<?php
if(!$is_ctc)
{
?>

                    <div class="fila">
                        <div class="fleft">
													<input class="cajatxt" id="nombre" name="nombre" type="text" value="" placeholder="Nombre y apellidos" required data-msg-required="<li>El campo &lt;strong&gt;Nombre y apellidos&lt;/strong&gt; es obligatorio.</li>">
						</div>
                    </div>
<?php
}
?>

                    <div class="fila">
						<input type="tel" maxlength="9" name="telefono" id="telefono" class="cajatxt" placeholder="Móvil" required data-msg-required="<li>El campo &lt;strong&gt;Móvil&lt;/strong&gt; es obligatorio.</li>" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" />
                    </div>
<?php
if(!$is_ctc)
{
?>
                    <div class="fila">
											<input type="email" name="email" maxlength="100" id="email" class="cajatxt" placeholder="E-mail" required data-msg-required="<li>El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio.</li>" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="" />
                    </div>

                    <div class="fila">
<?php
if($is_prov != 0)
{
?>

                      <div class="fleft">
												<select class="cajatxt" id="sel_prov" name="sel_prov" required="" data-msg-required="El campo &lt;strong&gt;Provincia&lt;/strong&gt; es obligatorio" data-msg-sel_prov="El campo &lt;strong&gt;Provincia&lt;/strong&gt; no es válido">
													<option value="-1" selected="selected">Selecione provincia</option>
<?php 
foreach($arr_prov as $key=>$value)
{
  $selected = "";
  $valor = strtolower(trim($value['name']));
  $valor = str_replace($arr_input,$arr_output,$valor);
  if ($valor==$provincia){
	$selected = " selected ";	
  }
  echo "<option value='".$value['id']."'".$selected.">".$value['name']."</option>";
}
?>
                        </select>
											</div>
<?php
}
?>
                    </div>

									<div class="legal">
										<input class="checkLeft" required data-msg-required="<li>Debes leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;&lt;/strong&gt;</li>" data-rule-digits="true" data-msg-cblegales="<br>Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />
										 Declaro ser mayor de edad y que he leído y acepto la <a href="<?php echo $url_protecciondatos.'?deviceType='.$layoutType; ?>" target="_blank">política de privacidad</a>

									</div>
<?php
}
?>
                 </form>
							<input class="btn_solicitar sendData" type="button" id="btnProcesar" value="&rarr; Solicitar información">
				 <div class="legal" style="text-align:center">
				  <a href="<?php echo $url_condiciones.'?deviceType='.$layoutType; ?>" target="_blank">Ver las condiciones generales de Sanitas</a>
				 </div>
            </div><!-- contenedor_form-->
          </div>
        
        </div><!-- cabecera-->
        <div class="linea_morada"></div>
		<div class="hide">
		  <a class="enlace_condiciones" href="#" target="_blank" onClick="MM_openBrWindow('<?php echo $url_condiciones; ?>','','scrollbars=yes,width=1060,height=720'); return false;">Ver las condiciones generales de Sanitas</a>
		</div>
		<span class="containerCondic" style="display:inline-block;width:100%;text-align:center;">
		  <a class="pdfCondiciones" href="<?=$url_local.'/'.$subFolderLanding?>//get_pdf.php" target="_blank"><img src="<?=$url_local.'/'.$subFolderLanding?>/img/p.gif" border="0" width="225" height="45" /></a>
		</span>
  <div class="pie hide">
	</br>
	<p>www.sanitas.es | © Todos los derechos reservados</p>
  </div>
</div><!-- contenido-->
