<?php
@session_start();
$cookiesMinTimeLimitStart = $minTimeCookiesStart; // TIEMPO MAXIMO ANTES DE LANZAR AUTOMATICAMENTE LAS COOKIES
//$_SESSION['cookiesMan']=1;
// START TIME CHECK COOKIES CONTROL INIT VARS
$timeUpdateCheckLanding = 5000;  // INTERVALO DE TIEMPO PARA EJECUTAR AUTOMATICAMENTE AJAX TEMPORIZADO (en milisegundos)
$jsInitStartXCounterLanding = 'var startCheckCookieLanding=0;';
ini_set('display_errors',$paintErrors);

//if((basename($_SERVER["SCRIPT_FILENAME"]) != 'index.php') && @!$_SESSION['sess_cookie_manage']) // ACEPTAR IMPLICITAMENTE AL NAVEGAR POR LA WEB

if (!@isset($_SESSION['startCheckCookieLanding'])) { // START VISITOR TIME COUNTER
    $_SESSION['startCheckCookieLanding'] = ceil(microtime(true));
    $_SESSION['sess_cookie_manage'] = 0;
    $jsInitStartXCounter = 'var startCheckCookieLanding=1;';
}

$elapsedTimeStart = ceil(microtime(true) - $_SESSION['startCheckCookieLanding']);
// END TIME CHECK COOKIES CONTROL INIT VARS
//Modificado por LFP 2013.11.21
//echo '-->>>'.$arr_creas[$id_crea]['cookie_css'];
if(isset($arr_creas) && isset($arr_creas[$id_crea]['cookie_css']) && !empty($arr_creas[$id_crea]['cookie_css']))
{
    $styleSheetFooter = trim($arr_creas[$id_crea]['cookie_css']);
} else {

    // START EXCEPCIONES PARA EL CSS SEGUN id_creatividad
    $styleSheetFooter = 'footer_cookies';
    $styleSheetFooterExtra = '';
    switch ($id_crea['body']) {
        case 4:
        case 5:
        case 6:
            $styleSheetFooterExtra = $id_crea['body'];
            break;
    }
    $styleSheetFooter.=$styleSheetFooterExtra . '.css';
    // END EXCEPCIONES PARA EL CSS SEGUN id_creatividad
}
?>

<div id="startScroll"></div>
<div id="timerDisplay"></div>
<link href="<?=$path_raiz_aplicacion_local.'css/footer_cookies'.str_pad($arr_creas[$id_crea]['new_template'],2,'0',STR_PAD_LEFT).'.css'?>" rel="stylesheet" type="text/css" />
<script type="text/javascript">

function drawDefaultPix()
{
//  $("head").append('<img src="https://secure.adnxs.com/seg?add=1177389&t=2" width="1" height="1" />');
  $("head").append('<img src="https://ad.yieldmanager.com/pixel?id=2441367&t=2" width="1" height="1" />');
  $("head").append('<img src="https://reachandrich.antevenio.com/track/compginc.asp?exptime=90&keyword=SanitasDental=visita" width="1" height="1" />');
//  includeScript("<?=$conditionalPixelStatic?>","js");
}

function includeScript(file_path, type)
{
  document.head = document.head || document.getElementsByTagName('head'[0]);
  if (type == "js")
  {
	var j = document.createElement("script");
	j.type = "text/javascript";
	j.src = file_path;
	document.head.appendChild(j);
  }
  return;
}

// START COOKIES CONTROL
    var checkElapsedTime;
    var cookiesEnabled = 0;
    var cookiesManaged = 0;
//// END COOKIES CONTROL


function manageCookies(start)
{
	cookiesEnabled = start;
	cookiesManaged = 1;
	$("#pie_galletero_right").slideUp("fast", function() {
		$("#pie_galletero_right").css("width", "0px !important");
		$(".pie_galletero_left").css("width","98% !important", function() {
			$("#cookie_info").css("text-align", "left");
			$(this).css("max-width", "99%!important");
		});
		$("#cookie_info").css("text-align", "center");
	});
	if(start == 1){
		drawDefaultPix();
		$.ajaxSetup({async: false});
		$.ajax({
			url:"<?=$path_raiz_aplicacion_local?>ajaxs/cookies_ajax_pixels.php",
			method:"post",
			dataType:"json",
			data:{isCookie: cookiesEnabled, fuente: id_source},
			cache:false
			,success:function(response)
			{
				if(response.pixel != undefined){
				$.each(response.pixel,function(key,val)
				{
					$("#footerWebPage").append(val);
				});
				}
			}
		});
		$.ajaxSetup({async: true});
	}
}

$(document).ready(function() {
		$(".closeCookiesPolicy").click(function() {
				$('#info_cook').slideUp("slow");
				return false;
		});

		$("#cookie_acept").click(function() {
				manageCookies(1);
		});

		$("#cookie_rejection").click(function() {
				manageCookies(0);
		});

		$("#pie_galletero_left").click(function(e) {
				e.preventDefault();
				$('#info_cook').show();
				$('html,body').animate({scrollTop:$("#startScroll").offset().top},500);
		});
});
</script>

<center>
<div id="pie_galletero">
    <div id="pie_galletero_left" class="pie_galletero_left">
        Utilizamos cookies propias, de analítica y de terceros para mejorar tu experiencia de usuario. Si aceptas la instalación de cookies, continúas navegando o permaneces en el sitio web más de 30 segundos, consideramos que aceptas su uso.<br />
        <center>
					<a href="#startScroll" id="cookie_info" class="smooth">Más información aquí</a>
				</center>
    </div>
  <div id="pie_galletero_right">
		<center>
        <input type="button" id="cookie_acept" name="cookie_acept" value="Acepto" class="cookieButton" /> <input type="button" id="cookie_rejection" name="cookie_rejection" value="No Acepto" class="cookieButton" />
		</center>
    </div>
    <div class="clearfix">&nbsp;</div>
</div>
</center>

<div id="info_cook" style='display:none;'>
    <div class="closeCookiesPolicy" style='display:block;float:right;'><a href="#null">X</a></div>
    <?php
    include($path_raiz_includes_local . 'includes/cookies_footer_txt.php');
    ?>
    <div class="closeCookiesPolicy" style='display:block;float:right;font-size:60% !important;'><a href="#null">Cerrar</a></div>
</div>

<div id="footerWebPage" style="display:block;"></div>
