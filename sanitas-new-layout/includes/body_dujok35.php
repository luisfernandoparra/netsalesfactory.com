<?php
@session_start();
$idLandingFormatted = str_pad($objSiteData->landingId, 3, '0', STR_PAD_LEFT);
$rutaImgs = $path_raiz_aplicacion_local . 'img/' . $idLandingFormatted . '/';
$defaultLocalPhoneCall = '';
$inbound_phone = ($inbound_phone) ? $inbound_phone : ''; // SI EL TELF NO ES IGUAL AL NUMERO POR DEFECTO PARA LS OTRAS CREAS, SE DIBUJA EL LOCAL
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
?>
<style>
    div.error2{ display : none; border:2px solid #D81E05; }
    .error2{color:#D81E05; background:#FCF1F0;}
    div.error2 ul{list-style-type:none !important;padding:0;margin:0;font:normal 11px Arial, Helvetica, sans-serif;}
    div.error2 ul li{margin:0 0 10 0;list-style-type:none;}
    div.error2 ul li label{font-weight:normal}
    div.error2, p.ok{ margin:0 0 10px 0;padding:10px;font:normal 11px Arial, Helvetica, sans-serif;}
    .ok{color:#333333; background-color:#EFFFDA;padding:10px;}
    input.error2{border:2px solid #D81E05;background-color:#FCF1F0;}

    .appbutton {
        background-color:#009fe4;
        background-image:url("img/f-boton-color.png");
        background-position:-10px 0;
        background-repeat:no-repeat;
        border:medium none #00335b;
        border-radius:3px;
        box-shadow:0 1px 2px #424242;
        color:white !important;
        cursor:pointer;
        display:inline-block;
        font:14px Arial,Helvetica,sans-serif;
        margin:5px 5px 0 0;
        padding:8px 13px;
        text-align:center;
        text-decoration:none !important;
        text-shadow:0 1px #00335b;
        width:auto;
    }
    .innerModal{
        box-sizing:border-box;
        margin:0;
        padding:0;
        padding-top:6%;
        position:relative;
        line-height:22px!important;
        font:12px Arial,Helvetica,sans-serif;
    }


</style>
<script type="text/javascript">
var errCheckTel = 1;
var iconselected = 1;
var css_selected = 'circulo_azul ico_selected';
var css_not_selected = 'circulo_azul';
var isCtcPass=0;

$(document).ready(function() {

	$("#ctcForm").validate({
			rules:{
				telefono_ctc:{
					required:true
					,remote:{
						url:root_path_local +"includes/phone_check.php",
						contentType:"application/json; charset=utf-8",  
						data:{cr:<?=(int)$id_crea?>},
						dataFilter:function(response){
							var jsonResponse="";
							isCtcPass=1;
							jsonResponse = JSON.parse(response);
							if(jsonResponse.success == false)
							{
								isCtcPass=0;
								return "\"" + jsonResponse.errorMessage + "\"";
							}else{
								return true;
							}
							return false;
						}
					}
				}
			},
			errorLabelContainer: $("#ctcForm div.error2")
	});

	$('input[name="cookie_rejection"]').click(function(){
		setTimeout('$("#pie_galletero_left").css("width","95%").css("text-align","left");',350)
	});


	$("#boton_ctc").click(function(){
			var phoneBumberCtc=$.trim($('#telefono_ctc').val());
			res=validator2.element("#telefono_ctc");

			if(!isCtcPass)
			{
				res=validator2.element("#telefono_ctc");
				setTimeout('$("#boton_ctc").click();',300);
				return false;
			}

			if(!res && !isCtcPass)
				return false;

			if($.isNumeric(phoneBumberCtc) && phoneBumberCtc.length >= 9)
			{
					var valortype = array_typeofctc[0];
					var campana = arr_campanas[0] ? arr_campanas[0] : 0;
					$.facebox.loading();
					$.ajax({
							url: root_path_local + "includes/phone_check.php",
							method: "post",
							dataType: "json",
							data: {cr:<?= (int) $id_crea ?>, telefono_ctc: phoneBumberCtc},
							cache: false,
							async: false,
							success: function(response)
							{
									if (!response.error)
									{
											$.ajax({
													url: root_path_local + "ajaxs/procesar_registro_ctc.php",
													method: "post",
													dataType: "json",
													data: {
															telefono: phoneBumberCtc,
															sourcetype: valortype,
															campaign: campana,
															fuente: id_source,
															idclient: id_client,
															crea: nomb_promo
													},
													cache: false,
													async: false,
													success: function(response)
													{
															if(!response.error)
															{
																ComprobarInsercion(response, 0);
															}
															else
															{
																ComprobarInsercion(response, 0);
															}
													},
													error:function(response){
															console.log("err2");
															return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
													}
											});
									}
									else
									{
											console.log("err code 2C");
									}
							},
							error: function(response) {
									console.log("err2");
									return "<br /><br /><center style=color:red;>ERROR INESPERADO EN LA CARGA DE LOS IDIOMAS, Perdonad las molestias!</center>";
							}
					});
			}
			return false;
	});
	validator2 = $("#ctcForm").validate({
			errorContainer: $('div.error2'),
			errorLabelContainer: $('div.error2 ul'),
			wrapper: 'li'
	});
	/**
	 * Fiunción para cambiar el texto según se haga click en el icono
	 */
	$('[id^="ico-"]').click(function(e) {
			var id = $(this).attr('id');
			var tmp = id.split('-');
			var tmp_iconselected = tmp[1];
			if (iconselected != tmp_iconselected) {
					$('#text-' + iconselected).hide();
					$('#text-' + tmp_iconselected).fadeIn('slow');
					$('#selected-' + iconselected).removeClass(css_selected).addClass(css_not_selected);
					$('#selected-' + tmp_iconselected).removeClass(css_not_selected).addClass(css_selected);
					iconselected = tmp_iconselected;

			}
	})
}); //document.ready
</script>

<header>
	<section class="cabecera">
<?php

if($objSiteData->headerTopContent)	// CARGAR CABECERA DESDE bbdd SI EXISTE
	echo $objSiteData->headerTopContent;
    
else
	echo '!! SIN CABECERA DEFINIDA !!';

$localLandingCuerpoImage='img/background_cuerpo'.$idLandingFormatted.'.jpg';
$localLandingCuerpoImage=(file_exists($path_raiz_includes_local.$localLandingCuerpoImage)) ? $path_raiz_aplicacion_local.$localLandingCuerpoImage : $path_raiz_aplicacion_local.'/img/defaul_no_image_body.jpg';
?>
		<div class="llamanos" >
<?php
if($inbound_phone != '')
{
?>
<a class="blockCall" href="tel:<?=$inbound_phone?>">
 <p>Llámanos GRATIS</p>
	<p><?= $drawInbound_phone ?></p>
</a>
<?php
}
?>
		</div>
	</section>
</header>

<section class="contenido">

    <section class="foto">

        <img class="web" src="<?=$rutaImgs?>photo.png"/>
        <img class="mvl" src="<?= $rutaImgs ?>photo_mvl.png"/>


    </section>




    <section class="formulario">

        <form method="post" action="" name="ctcForm" id="ctcForm">
            <img class="hotel" src="<?=$rutaImgs?>hotel.png"/>
            <div class="error2"><ul></ul></div>
            <p>Te llamamos gratis</p>
            <div class="fila">
                <div class="fleft">
                    <input type="tel" class="celda"  id="telefono_ctc" name="telefono_ctc" maxlength="9" value="" placeholder="Introduce tu teléfono" required data-msg-required="Si deseas que te llamemos es necesario que introduzcas tu n&uacute;mero de &lt;strong&gt;teléfono&lt;/strong&gt;" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo de &lt;strong&gt;teléfono&lt;/strong&gt; para llamarte debe contener al menos 9 dígitos" />
                    <input name="llamar" type="button" value="Llamar" id="boton_ctc">
                </div>
            </div>
        </form>




        <form method="post" action="" name="enviarPorMailSolicitaInfo" id="enviarPorMailSolicitaInfo">
            <input type="hidden" name="destino" id="destino" value="">
            <div class="error"><ul></ul></div>
            <p>Solicita informaci&oacute;n</p>

            <div class="fila">
                <div class="fleft"><input type="text" class="celda" id="nombre" name="nombre" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" value="<?= $nombre ?>" placeholder="Nombre y Apellidos"></div>
            </div>
            <div class="fila">
                <div class="fleft"><input type="email" maxlength="100" class="celda" name="email" id="email" value="<?= $email ?>" placeholder="Email" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido"></div>
            </div>
            <div class="fila">
                <div class="fleft"><input type="tel" class="celda" id="telefono" name="telefono"  value="" placeholder="Teléfono" maxlength="9" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos"></div>
            </div>

            <div class="legal">
                <input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />  He leído y acepto la <a href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php'?>" target="_blank">política de privacidad</a>
            </div>
            <div class="espacio_btn">
                <input name="registrate" type="button" value="Enviar"  class="green sendData" data-direction="down">
            </div>               
        </form>
    </section> <!--CIERRE FORM -->




    <section class="contenido_pestañas">

    </section> <!--CIERRE CONTENIDO TEXTO -->  
</section>
<!--CIERRE CONTENIDO --> 


<section class="contenido_texto">
<?php

if($objSiteData->boxBottomForm)	// BLOQUE INFERIOR AL FORMULARIO PRINCIPAL
{
	echo $objSiteData->boxBottomForm;
}
else
{
	echo '
			<div class="boxBottom"></div>
';
}

?>
</section>

<footer>
<?php

if($objSiteData->boxFooterText)	// BLOQUE PIE DE PAGINA
{
	echo $objSiteData->boxFooterText;
}
else
{
	echo '
			<div class="mini">
				
			</div>
';
}

?>
    <div>
			<img src="<?= $rutaImgs ?>bottom.jpg"/>
		</div>
</footer>
