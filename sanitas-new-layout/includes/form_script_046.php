<?php
@session_start();
?>
				<div class="error"><ul></ul></div>

				<input type="tel" maxlength="9" name="telefono" id="telefono" class="" placeholder="Tel&eacute;fono" required data-msg-required="<li>El campo &lt;strong&gt;Móvil&lt;/strong&gt; es obligatorio.</li>" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" />

				<div class="legal">
					<input class="checkLeft" required data-msg-required="<li>Debes leer y marcar la casilla de &lt;strong&gt;La política de privacidad&lt;/strong&gt;&lt;/strong&gt;</li>" data-rule-digits="true" data-msg-cblegales="<br>Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />
					 Acepto la <a href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php';?>" target="_blank">política de privacidad</a>
				</div>

				<input class="sendData" id="btnProcesar" name="btnProcesar" type="button" value="Infórmate">
