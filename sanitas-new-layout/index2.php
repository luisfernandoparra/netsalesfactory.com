<?php
/*
 * NOTAS IMPORTANTES:
 * PARA TODAS LAS CREATIVIDADES "responsive" Y "mobile"
 * EL SISTEMA JS DE CONTROL DE CAMPOS SE EFECTUA POR VALIDATE DE JQUERY
 * EXCEPCIONES: TODAS LAS PROMOS WEB VIEJO MODELO Y FORMULARIOS CON IFRAMES
 * 
 * PARAMETROS CONDICIONANTES
 * 
 * forzarmovil = 1 PARA FORZAR LA PAGINA VISTA DESDE UN MOVIL EN UN DESKTOP PC ($layoutType)
 * inbound = 1 (+ forzarmovil = 1 DESDE PC) FORZAR PROMO INBOUND
 * ctc = 1 FORZAR CLICK TO CALL
 */
@session_start();
if(empty($_SESSION['namePromo']))
{
	$tmpName=session_name('sanitas'.date(Yimis));
	$_SESSION['namePromo']=session_name();
	$_SESSION['sessionCountPromo']=0;
}

include('../conf/config_web.php');
ini_set('display_errors',$paintErrors);
include('conf/config_web.php');
include($path_raiz_includes.'includes/initiate.php');
include($path_raiz_includes_local.'includes/local_vars.php');
$layoutType=null;
$id_crea= $id_crea ? $id_crea : $objSiteData->landingId;

// START INBOUND PHONE SELECT
// (necesario que esté en este punto puesto que ´$id_source´ se recupera desde `initiate.php`)
$inbound_phone_default='900834152';
$arr_inbound_phones_sources=array(
	'2407330'=>'900834153',
	'2413718'=>'900806406'
);
$isInbound=(int)@$_REQUEST['inbound'];

$inbound_phone=$arr_inbound_phones_sources[$id_source] ? $arr_inbound_phones_sources[$id_source] : $inbound_phone_default;	// INBOUND PHONE DISPLAY
$inbound_prefix=$isInbound ? 'inb_' : '';
// END INBOUND PHONE SELECT

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
/*
 * SEGUN EL TIPO DE DISPOSITIVO UTILIZADO PARA ABRIR LA PROMOCION
 * $layoutType = PREFIJO DEL DISPOSITIVO UTILIZADO
 */
if((@$arr_creas[$id_crea]['manageDevice']))
{
	include($path_raiz_includes.'includes/index_mobile_manager.php');	// SCRIPT PARA DETECTAR EL DISPOSITIVO ABRE LA WEB

}

/*
 * SE EFECTUA EL LOG (COMPLETO) SOLO PRIMER ACCESO
 */
if($_SESSION['sessionCountPromo'] == 1)
{
	if(isset($_SESSION['deviceMobileData']))
	{
		$query='INSERT INTO %s (session_id,fecha,ip_usuario,script,accion,http_referer,user_agent,id_crea,device_type,device_os,device_browser)VALUES(\'%s\',NOW(),\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',%d,\'%s\',\'%s\',\'%s\')';
		$query=sprintf($query,$table_front_actions,$_SESSION['namePromo'],$_SERVER['REMOTE_ADDR'],$pag,$laAccion,$_SERVER['HTTP_REFERER'],$_SERVER['HTTP_USER_AGENT'],(int)$_REQUEST['cr'],$_SESSION['deviceMobileData'][0],$_SESSION['deviceMobileData'][1],$_SESSION['deviceMobileData'][2]);
	}
	else
	{
		$query='INSERT INTO %s (session_id,fecha,ip_usuario,script,accion,http_referer,user_agent,id_crea)VALUES(\'%s\',NOW(),\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',%d)';
		$query=sprintf($query,$table_front_actions,$_SESSION['namePromo'],$_SERVER['REMOTE_ADDR'],$pag,$laAccion,$_SERVER['HTTP_REFERER'],$_SERVER['HTTP_USER_AGENT'],(int)$_REQUEST['cr']);
	}
	$conexion->ejecuta_query($query);
if($_SERVER['REMOTE_ADDR']=='212.0.115.180')
{
echo '<pre>';print_r($_SESSION['deviceMobileData']);print_r($_SERVER);echo '</pre><hr>';
echo $query.'<pre>';print_r($_SESSION);print_r($_SERVER);echo '</pre>';die();
}
}

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// SOLO TESTS::PARA FORZAR EL TIPO DE DISPOSITIVO A MOBILE
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if(@$_REQUEST['forzarmovil'])
{
	$layoutType=$arr_creas[$id_crea]['new_template'] ? 1 : 'mob_';
	$is_movil=1;
}

/*
 * START NECESARIO PARA FORZAR SOLAMENTE EL VIEJO JS DE VALIDACION PARA LAS PROMOS WEB
 */
if(!$layoutType && empty($arr_creas[$id_crea]['js']))
	$arr_creas[$id_crea]['js']='validacion_pre_layout.min.js';

if(@$objSiteData->landingId)	// PARA PROMOS BASADAS EN BBDD
	$arr_creas[$id_crea]['js']='';
// END NECESARIO PARA FORZAR SOLAMENTE EL VIEJO JS DE VALIDACION PARA LAS PROMOS WEB


//	START EXCEPCION PARA PERMITIR COMPONER LA PROMO CORRECTAMENTE EN EL IFRAME DE LOS SORTEOS, GTP (M.F. 20140226)
$layoutSorteo='';
$arraySorteos=array(
  'http://www.gana25mileuros.com/landings/',
  'http://www.ganatucoche.com/landings/',
  'http://www.ganaunaplay.com/landings/',
  'http://www.ganaundiadecompras.com/landings/',
  'http://www.ganaungalaxy4.com/landings/',
  'http://www.ganaungranviaje.com/landings/',
  'http://www.ganaunhomecinema.com/landings/',
  'http://www.ganauniphone5.com/landings/',
  'http://www.ganaunpartido.com/landings/',
  'http://www.ganaunsegurodental.com/landings/',
  'http://www.sorteoandalucity.es/landings/',
  'http://www.sorteoelprogreso.es/landings/',
  'http://139.162.246.12/ganatuspremios.4.0.com/plantilla/landings/'
);

if(isset($_SERVER['HTTP_REFERER']) && in_array($_SERVER['HTTP_REFERER'],$arraySorteos))
{
  $layoutSorteo='sorteoIframe';
}
//	END EXCEPCION PARA PERMITIR COMPONER LA PROMO CORRECTAMENTE EN EL IFRAME DE LOS SORTEOS

if($layoutType)
{
	$is_movil=1;
}

// START EXCEPCION PARA LA IMAGEN DE LA CABECERA PARA UNA FECHA EN UNAS DETERMINADAS HORAS Y LA MODAL DE "LLAMAR"
$mobileImagePostName='';

$fechaActivacion='20140306';  // DIA DE ACTIVACION PARA LA EXCEPCION

//$mobileArraDayException=array(2=>'',3=>'',4=>'',6=>'',7=>'');	// UTILIZAR ESTA LINEA SOLO EN CASO SE DEBAN DESHABILITAR LAS PROMOS "mobile"
$mobileArraDayException=array(2=>'_callme',3=>'_callme',4=>'_callme',6=>'_callme',7=>'_callme');
$mobileRangeTimeException=(date('H') >= 10 && date('H') <= 13) ? DATE('Ymd') : '';  // parte final del nombre de la clase para estar activada desde las 10 a las 14 horas (SOLO SI NECESARIO)
// ********* SOLO PARA PRUEBAS, DESCOMENTAR LA DE ARRIBA ***************
//$mobileRangeTimeException=(date('H') >= 10 && date('H') <= 14) ? $fechaActivacion : '';

/*
 * VARIABLES PARA CONTROLAR EL ORARIO
 * DE APARICION LA VENTANA MODAL DE "LLAMAR"
 */
$allowDays=(date('w') >= 1 && date('w') <= 5) ? true : false; // SOLO DIAS LABORALES
$timeStartModal=1000; // HORA + MINUTO DE INICIO DE HABILITACION
$timeEndModal=2030;	// HORA + MINUTO FIN DE HABILITACION

$allowHours=(date('Hi') >= $timeStartModal && date('Hi') <= $timeEndModal) ? true : false; // HORARIO RESTRINGIDO, DESDE ... HASTA
$mobileRangeTimeException=($allowDays && $allowHours) ? $fechaActivacion : null;


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// MODAL DE "LLAMAR"
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if($layoutType == 'mob_' && array_key_exists($id_crea,$mobileArraDayException) && $mobileRangeTimeException)
  $mobileImagePostName=$mobileArraDayException[$id_crea];	// PARA CONSTRUIR EL NOMBRE LA IMAGEN DE LA CABECERA

//////if(array_key_exists($id_crea,$mobileArraDayException) && $mobileRangeTimeException)
// END EXCEPCION PARA LA IMAGEN DE LA CABECERA PARA UNA FECHA Y UNAS DETERMINADAS HORAS

include($path_raiz_includes_local . 'includes/header'.($arr_creas[$id_crea]['new_template'] ? '_tmplt' : '').'.php');
?>
<body class="landing">
<?php
if($arr_creas[$id_crea]['new_template'])
{
	include($path_raiz_includes_local . 'includes/'.$arr_creas[$id_crea]['body']);
	include($path_raiz_includes_local . 'includes/cookies_footer.php');
}
else
{
  include($path_raiz_includes_local . 'includes/'.($isInbound && $layoutType ? $inbound_prefix : '').$layoutType.$arr_creas[$id_crea]['body']);
  include($path_raiz_includes_local . 'includes/'.$layoutType.'cookies_footer.php');
}
//echo'--><pre>';print_r($arr_creas);

?>
</body>
</html>
