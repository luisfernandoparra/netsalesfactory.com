<?php

/**
 * Modificación LFP 2015.03.23
 * Añadimos variable para procesar los datos que vienen de acción mobile emailing Gmail
 * Para eso añadimos variable $emailingSource que será true si viene de gmail
 * Si esta variable es true, no hacemos el die del final y tenemos los datos en un array
 * 
 * @date 2016.07.01. 
 * Nos piden que no mandemos nada a LeadSolutions, que no pasaba por el router. Por tanto lo pasamos todo por el router
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');
/**
 * Config loaded viene del fichero bridge. Si está definida y es true, es que ya ha cargado los configs
 */
if (empty($configLoaded)) {
    include('../../conf/config_web.php');
    include('../conf/config_web.php');
}
$resCurl = array();
///$sOutput = array(); // ARRAY PARA LOS DATOS DE SALIDA JSON
$raw = '';
$resCurl['error'] = 1;
//$sOutput['mensaje'] = 'ha habido un error en el proceso';
$sOutput['id'] = -1;
$cli = (empty($_REQUEST['cli'])) ? '' : $_REQUEST['cli'];
$idCrea = (empty($_REQUEST['idcrea'])) ? 0 : $_REQUEST['idcrea'];

$emailingSource = (!empty($_REQUEST['emailingsource']) && strtoupper(trim($_REQUEST['emailingsource'])) == 'GMAIL');

$nombre = (!empty($_REQUEST['nombre'])) ? trim($_REQUEST['nombre']) : '';
$apellidos = (!empty($_REQUEST['apellidos'])) ? trim($_REQUEST['apellidos']) : '';
$email = (!empty($_REQUEST['em'])) ? trim($_REQUEST['em']) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? $_REQUEST['provincia_text'] : $_REQUEST['provincia'];

$legal = (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : '1';
$permanencia = (!empty($_REQUEST['perm'])) ? trim($_REQUEST['perm']) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;
$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';

//$id_source = (!empty($_REQUEST['id_source']) && is_numeric($_REQUEST['id_source'])) ? (int)$_REQUEST['id_source'] : '';
$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? trim($_REQUEST['idclient']) : '';
$sourcetype = (!empty($_REQUEST['sourcetype'])) ? trim($_REQUEST['sourcetype']) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? trim($_REQUEST['campaign']) : '';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? trim($_REQUEST['prioridad']) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? trim($_REQUEST['telefono']) : '';

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;


//echo'------------>'.$subFolderLanding;
/**
 * Capping. Eliminado. 
 *//*
  $id_cliente=$id_client;
  include($path_raiz_includes.'includes/capping_control.php');
  //die('hola');

 */
/**
 * Pequeña treta para el aplicativo GSS.
 * si el id_client es 28, habrá parámetros que cambiarán
 */
/*
  if ($id_client == 28 && $fuente != 2081175) {
  //SOLO PARA SALUD
  $file_ws = $arr_client_campaign[$id_client]['ctcFile'];
  //cambiamos el sourcetype
  if ($sourcetype != 'CTC')
  $sourcetype = 'FORMULARIO';

  }
 */
$campos = array('idclient' => $id_client, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad, 'nombre' => $nombre, 'apellidos' => $apellidos, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI, 'vcc' => 'fintel', 'pwd' => $secret_pwd, 'usr' => $secret_usr);

$campos['cli'] = $cli; // ADDED 2016.04.13 M.F.
//,"serverIp"=>"84.127.240.42"
$qs = http_build_query($campos);
$url = $url_ws . $file_ws . '?' . $qs;
//print_r($campos);die($url);
//print_r($campos);die($url);die();

if ($telefono != '') {
	$conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
	if (!$conexion->connectDB()) {
		$msg_err = 'No se ha podido conectar a la BBDD. Error 102';
	} else {
		$cur_conn_id = $conexion->get_id_conexion();
		/* $sql1="SET AUTOCOMMIT=0;";
			$conexion->ejecuta_query($sql1);

			$sql2="BEGIN";
			$conexion->ejecuta_query($sql2);
		 */
		$nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
		$nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');

		$telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
		$sql = 'INSERT INTO %s (id_cliente,nombre,apellidos,telefono,id_provincia,b_legales,email) VALUES (\'%d\',\'%s\',\'%s\',\'%s\',\'%d\', %d,\'%s\');';
		$sql = sprintf($sql, $table_registros, $id_client, $nombreIns, $crea, $telefonoIns, (int) $_REQUEST['provincia'], $legal, $email);
		$conexion->ejecuta_query($sql);
		$id = mysql_insert_id();
		$conexion->disconnectDB();
		$rnd = rand(1, 100);

		$arrCvsByCrea=array(160);

		if(in_array($idCrea, $arrCvsByCrea))	// EXCEPCION PARA CREA 159, 2016.12.05 M.F.
		{
			$fileCsv = '../sanitasfiles/sanitas_landings-'.date('Ymd').'.csv';

			$headerFile='';

			if(!file_exists($fileCsv))
				$headerFile='nombre; e-mail; telefono; crea; fecha; fuente'."\r\n";;

			$fp=@fopen($fileCsv,'a') or die('Unable to open file!');
			$line = $headerFile.$nombre.';'.$email.';'.$telefono.';'.$crea.';'.date('d-m-Y').';'.$fuente."\r\n";
			$raw=$error;
			$error=fwrite($fp, $line);
			fclose($fp);
			$error=(int)$error > 0 ? 0 : 1;

			$resCurl = new stdClass();
			$resCurl->error=$error;
			$resCurl->result=$error ? 'KO' : 'OK';
			$resCurl->mensaje=$error ? 'KO' : 'CSV generado';
			$resCurl->id=$id;
			$raw='{"error":'.$resCurl->error.',"result":"'.$resCurl->result.'","mensaje":"'.$resCurl->mensaje.'","id":"'.$resCurl->id.'","setHashedEmail":""}';
		}
		else
		{
			$ch = curl_init();
			$timeout = 0; // set to zero for no timeout
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$raw = curl_exec($ch);
			curl_close($ch);

			$resCurl = json_decode($raw);
			$resCurl->setHashedEmail = ($id_client == 28) ? md5($email) : ''; // ONLY X CRITEO TAG 2015.10.22 M.F.
			$raw = json_encode($resCurl);
			$error = $resCurl->error;
//echo$idCrea.' )---> ';print_r($raw);die();
		}
//echo $raw.' )---> ';print_r($_REQUEST);print_r($resCurl);die($url);die();


		if ($error) {
				$msg_err = 'Parece que el problema se debe a lo siguiente: "' . $resCurl->mensaje . '".';
		} else {
				$tmpCrea = $_SESSION['idCreatividad'];
				unset($_SESSION['namePromo']); // SE DESTRUYE LA REFERENCIA DE LA SESION
				$tmpName = session_name('sanitas' . date(Yimis));
				$_SESSION['namePromo'] = session_name(); // SE CONSTRUYE UNA NUEVA SESION PARA PERMITIR REGISTROS NUEVOS DESDE LA MISMA APERTURA
				$_SESSION['idCreatividad'] = $tmpCrea;

				if (!$emailingSource) {
						die($raw);
				} else {
						echo $url;
						$resCurl = json_decode($raw, true);
				}
		}
	}
} else {
	if ($telefono == '') {
		$error = 1;
		$msg_err = 'Falta el campo Teléfono. Error 104';
	}
}

if ($error) { // LOG ACTIONS
    $query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1 ';
    $query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
    $conexion->getResultSelectArray($query);
    $resQuery = $conexion->tResultadoQuery;
    $idUpdate = $resQuery[0]['id'];
    $query = 'UPDATE %s SET http_referer=\'ERROR CTC: %s\' WHERE id=\'%d\'';
    $query = sprintf($query, $table_front_actions, isset($resCurl->internalCode) ? 'InconcertErr=' . $resCurl->internalCode : $resCurl->mensaje, (int) $idUpdate);
//echo $query;die();
    $conexion->ejecuta_query($query);
}

$resCurl->setHashedEmail = ($id_client == 28) ? md5($email) : ''; // ONLY X CRITEO TAG 2015.10.22 M.F.

if (!$emailingSource) {
    $res = json_encode($resCurl);
    die($res);
}
/* $res = json_encode($resCurl);
  die($res);
 * 
 */
?>