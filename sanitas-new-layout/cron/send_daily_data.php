<?php
/**
 * Cron que recupera el archivo diario y envia a la dirección especificada
 * @version 1.0
 * @date 2016.12.05
 * @author MF
 */
header('Content-Type: text/html; charset=utf-8');
@include('../../conf/config_web.php');
@include('../conf/config_web.php');
$isTestSite=null;

$url_ws='https://ws-server.netsales.es/webservices/transactionalEmails/';
$file_ws='index.php';

/**
 * POR DEFECTO SE ENVIAN LAS DIRECCIONES DE "$transactionEmailDoctorSender"
 * EN CASO DE QUE EXISTA "$transactionEmailDoctorSenderSpecivicCrea[$id_crea]" SE UTILIZAN LOS EMAILS AHI CONTENIDOS
 * NOTA IMPORTANT: emails SEPARADOS POR COMAS SIN ESPACIOS
 */
$defaultReceiver='mario.francescutto@netsales.es'.($sitesPruebas ? ',luis.parra@netsales.es' : ',msantamaria@netsales.es,lgonzaleza@sanitas.es');	//
//$defaultReceiver='mario.francescutto@netsales.es'.($sitesPruebas ? ',luis.parra@netsales.es' : ',msantamaria@netsales.es');	//

if(isset($sitesPruebas) && $sitesPruebas)
	$isTestSite=' (desde HOMER)';


//if(!isset($url_local))
//{
//	$fileLog = 'log_errores-'.date('Ymd').'.log';
//	$fp=@fopen($fileLog,'a') or die('Unable to open file!');
//	$error=fwrite($fp, "\r\nError en la carga de".$isTestSite.": ../conf/config_web.php\r\n");
//	fclose($fp);
//	die('Error en la carga de config_web');
//}

//if(!isset($defaultReceiver))

$dateProcess=date('Ymd');
$dateProcessTxt=date('d-m-Y');
//$dateProcess='20161115';
$fileCsv = $path_raiz_includes_local.'sanitasfiles/sanitas_landings-'.$dateProcess.'.csv';


if(file_exists($fileCsv))
{
	$htmlData='
<html>
<head></head>
<body>
<div style=font-family:arial,verdana;font-size:1em;color:#000;display:inline-block;width:99%;float:left;text-align:left;>
Tiene un archivo de datos generado desde <b>la landing de Sanitas</b>, ha sido conseguido el d&iacute;a <b>'.$dateProcessTxt.'</b>.<br /><br />

Identificador interno: R-'.(date('HisYmd')).'-'.rand(0,10000).'000S.<br /><br />

Link para descargar el contenido en formato CSV:
<br /><a href="http://178.79.159.246/promociones/sanitas-new-web/sanitasfiles/sanitas_landings-'.$dateProcess.'.csv" target="_blank">descargar archivo CSV</a>
<br /><br />Fin del mensaje.

</div>
</body>
</html>
';

	$arrData=array('user'=>'sanitas-2016', 'data'=>array(
			array(
			'receiver'=>$defaultReceiver,
			'subject'=>'Datos registro (por CSV) landing Sanitas'.$isTestSite,
			'html'=>$htmlData,
			'text'=>$htmlData
			)
		)
	);

	$fields_string = json_encode($arrData);

	$url = $url_ws . $file_ws;//. '?user='.$crea;
/*
echo $path_raiz_includes.'<hr>url_ws= '.$url_ws;
echo '<hr>$fields_string= '.$fields_string;
echo '<hr>$fileCsv= '.$fileCsv;
echo '<hr>$crea (USR)= '.$crea;
echo '<hr>';
die();
*/
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_POST, 1);
	curl_setopt($ch,CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, $timeout);
	$raw = curl_exec($ch);
	$statusWs = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
//echo '-->'.$raw;echo "\r\n".$statusWs;die();
	$resCurl=json_decode($raw, true);

// !!!!!!!!!!!!!!!!!!! activar SOLO para solo test !!!!!!!!!!!!!!!!!!!!
//$resCurl['correct']=0;

	if(isset($resCurl['correct']) && $resCurl['correct'] < 1 || !isset($resCurl['correct']))
	{
		$subject = 'Error en envio del correo de Sanitas'.$isTestSite;
		$message = '<h1>Error en el cron diario del env&iacute;o de datos de Sanitas</h1><br />El archivo que se ha generado es:<br /><span style=color:blue;font-weight:bold;>'.$fileCsv.'</span>.';
		$message .= '<br /><br />Fecha del intento:<br /><span style=color:red;font-weight:bold;>'.(date('d-m-Y / H:i:s')).'</span>.';
		$message .= '<br /><br />Link a enviar:<br /><a href="http://178.79.159.246/promociones/sanitas-new-web/sanitasfiles/sanitas_landings-'.$dateProcess.'.csv" target="_blank">descargar archivo CSV</a>';
		$message .= '<br />Error devuelto: '.$raw;
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: webservicenetsales@netsales.netsales.es' . "\r\n" .
		'Reply-To: webservicenetsales@netsales.netsales.es' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();
		$emailsSend='mario.francescutto@netsales.es,luisfer.parra@netsales.es';
		@mail($emailsSend, $subject, $message, $headers);

		$message=str_replace('<br />', "\r\n", $message);
		$message=  strip_tags($message);

		$fileLog = 'log_errores-'.$dateProcess.'.log';
		$fp=@fopen($fileLog,'a') or die('Unable to open file!');
		$error=fwrite($fp, $message."\r\n-------------\r\n");
		fclose($fp);
		die('finalizado con error');
	}
	else
	{
		$message='Email con URL de CSV enviado correctamente: '.$dateProcessTxt.' a '.$defaultReceiver."\r\n";
		$fileLog = 'log_envios-'.$dateProcess.'.log';
		$fp=@fopen($fileLog,'a') or die('Unable to open file!');
		$error=fwrite($fp, $message."\r\n");
		fclose($fp);
		echo 'OK';
	}
//echo $resCurl['correct'].') <pre> WS devuelve='.($raw);
	$resCurl['error']=$error;
}
die();