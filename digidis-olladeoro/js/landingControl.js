/*
 * Fichero que controla la acción sobre los botones de + y -
 */

var url = 'https://www.serviapuestas.es/es/registro.html?';
/**
 * Document Ready
 
 */
$(document).ready(function () {

    /**
     * FUNCIón de click del botón de comprar
     */
    $('#btnPurchase').click(function (e) {
        var url_final = url.replace('[%%VALUE%%]', $('#txtValue').val());
        window.location.href = url_final;
        return false;
    });
});