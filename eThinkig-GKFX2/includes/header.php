<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
if($is_movil == 1){
?>
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php
}
//echo date('s').']--->'.$path_raiz_aplicacion;die();

?>
<title><?php echo $arr_creas[$id_crea]['title']; ?></title>

<link href="<?php echo $path_raiz_aplicacion; ?>js/modal/css/modal.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $path_raiz_aplicacion; ?>js/facebox/facebox.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $path_raiz_aplicacion; ?>js/window/css/window.css" rel="stylesheet" type="text/css" />

<link href="<?php echo $path_raiz_aplicacion_local; ?>css/css_<?php echo $id_crea; ?>.css" rel="stylesheet" type="text/css" />


<?php
//echo $path_raiz_aplicacion;
if(!$isOldBrowser)	// NO FUNCIONA CON I-EXPLORER 8 Y ANTERIORES
{
  echo '
<link href="'.$path_raiz_aplicacion.'css/animate.css" rel="stylesheet" type="text/css" />';
}
?>

<script type="text/javascript" src="<?php echo $path_raiz_aplicacion; ?>js/jquery-1.9.1.min.js"></script>

<?php

if(!$isOldBrowser)	// NO FUNCIONA CON I-EXPLORER 8 Y ANTERIORES, SE OMITE LA CARGA
{
  echo '
<script type="text/javascript" src="'.$path_raiz_aplicacion.'js/jquery.textillate.js"></script>
<script type="text/javascript" src="'.$path_raiz_aplicacion.'js/jquery.lettering.js"></script>';
}

?>

<script type="text/javascript">
var root_path = "<?php echo $path_raiz_aplicacion;?>";<?php /*ruta global para las imgs del facebox y del modal*/?>
var root_path_local = "<?php echo $path_raiz_aplicacion_local;?>";<?php /*ruta global para ajax*/?>
var nomb_promo = "<?php echo $arr_creas[$id_crea]['nombpromo']; ?>";
var id_source = "<?php echo trim($id_source); ?>";
var id_client = "<?php echo $id_client; ?>";

var arr_campanas = [<?php  echo $campanas; ?>];
var is_ctc = "<?php echo trim($is_ctc); ?>";
var is_movil = "<?php echo trim($is_movil); ?>";
var root_path_ws = "<?php echo $path_raiz_ws;?>";
var url_ws = "<?php echo $url_ws;?>";
var file_ws = "<?php echo $file_ws;?>";
var arr_pixel_JS = ["<?php echo $arr_pixel_JS[0];?>","<?php echo $arr_pixel_JS[1];?>"];
var tduid = "<?php echo $tduid; ?>";
var trdbl_organization = "<?php echo $organization; ?>"; //Pixel Tradedoubler. Organizacion
var trbdl_event = "<?php echo $event; ?>";
var nombCliente = "<?php echo $nombCliente; ?>";
var is_prov = "<?php echo trim($is_prov); ?>";

//textos por defecto del formulario
var arr_txts = [['nombre','Tu Nombre'],['apellidos','Tus Apellidos'],['telefono','Tu número de móvil'],['email','Tu correo electrónico']];

$(document).ready(function(){

/////////////////////////////////////////
// START PAINT PRE-DEFINED INPUT TEXTS //

  $("#nombre").val(arr_txts[0][1]);
  $("#apellidos").val(arr_txts[1][1]);
  $("#telefono").val(arr_txts[2][1]);
  $("#email").val(arr_txts[3][1]);
  
  $(".clickform").focusout(function(){
	var id = $(this).attr("id");
	var blanquear = false;
	var valor = $(this).val();

	var txt = "";
	if(valor == ''){
	  switch(id){
		case "nombre": txt = arr_txts[0][1];break;
		case "apellidos": txt = arr_txts[1][1];break;						
		case "telefono": txt = arr_txts[2][1];break;						
		case "email": txt = arr_txts[3][1];break;						
	  }
	}
	if(txt != ''){
	  $(this).val(txt);
	}
  });

  $(".clickform").focusin(function(){
	var id = $(this).attr("id");
	var blanquear = false;
	var valor = $(this).val();

	switch(id){
	  case "nombre": blanquear = (valor == arr_txts[0][1]);break;
	  case "apellidos": blanquear = (valor == arr_txts[1][1]);break;						
	  case "telefono": blanquear = (valor == arr_txts[2][1]);break;						
	  case "email": blanquear = (valor == arr_txts[3][1]);break;						
	}
	if(blanquear){
	  $(this).val('');
	}
});
// END PAINT PRE-DEFINED INPUT TEXTS //
/////////////////////////////////////////
  
  $(".enlace_condiciones").click(function(){
		var msg='<div style="display:block;width:900px;height:500px;background:#fff;"><iframe style="width:100%;height:100%;background:#fff;" frameborder="no" src="'+root_path_local+'cash_back.pdf"></iframe></div>';
		$.facebox(msg);
//		$.facebox({div:'#promoConditions'});
		return false;
  });

  $(".enlace_pp").click(function(){
		var msg='<div style="display:block;width:600px;height:500px;background:#fff;"><iframe style="width:100%;height:100%;background:#fff;" frameborder="no" src="'+root_path_local+'privacy_policy.php"></iframe></div>';
		$.facebox(msg);
		return false;
  });

  $(".srt").css("display","none");
  
  // START ANIMATION FLYn
  var pos_fly0=(parseInt(parseInt($("body").css("width"))/2)-parseInt($("#fly0").css("width"))+450);  // retrieve auto central position element fly0
  
  $("#fly0").delay(200).show("fast").animate(
	{
	  left:pos_fly0,
	  top:(parseInt($(".titulo").css("height"))+569)+"px",
	  opacity:'1',
	  height:'auto',
	  width:parseInt($("#fly0").css("width"))
	},1200,function(){
	  // start second animatoin
		$("#fly1").delay(200).show("fast").animate({
		left:pos_fly0,
		top:(parseInt($("#fly0").css("height"))+100)+"px",
		opacity:'1',
		height:'100px',
		width:parseInt($("#fly0").css("width"))
	  }
	);
	$(".boxCondiciones").fadeIn("slow");
  // END ANIMATION FLYn

  });
<?php

if(!$isOldBrowser)	// NO FUNCIONA CON I-EXPLORER 8 Y ANTERIORES
{
  echo '
	$(".tlt2").textillate().show(10,function(){
	  $(".srt").delay(2300).fadeIn("slow");
	  return false;
	});
';
}
else
  echo '$(".titulo").css("background-image","#fff url(../img/top_background.jpg) no-repeat center top 8px");$(".srt").delay(1300).fadeIn("slow");';	// ESTO SOLO PARA I-EXPLORER VIEJOS

?>
});
</script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion; ?>js/modal/js/jquery.simplemodal.1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion; ?>js/facebox/facebox.min.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion; ?>js/window/js/window.js"></script>
<script type="text/javascript" src="<?php echo $path_raiz_aplicacion; ?>js/funciones_comunes.js"></script>
<?php
if(!empty($arr_creas[$id_crea]['js']) && trim($arr_creas[$id_crea]['js'])!='' ){
?>
	<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;  ?>js/<?php echo trim($arr_creas[$id_crea]['js']); ?>"></script>
<?php
} else {
?>
	<script type="text/javascript" src="<?php echo $path_raiz_aplicacion_local;  ?>js/validacion.js"></script>
<?php

}
?>
</head>
