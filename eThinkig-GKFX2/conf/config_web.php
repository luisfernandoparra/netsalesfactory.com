<?php

//***************************************
// START CONFIG DINAMICA (M.F. 19.11.2013)
$sitesPruebas = ($_SERVER['SERVER_ADDR'] == 'localhost' || $_SERVER['SERVER_ADDR'] == '127.0.0.1' || substr($_SERVER['SERVER_ADDR'], 0, 9) == '192.168.2'); // SITIOS PARA PRUEBAS
$paintErrors = false;
$minTimeCookiesStart = 30;  // TIEMPO (EN SEGUNDOS) PARA ACTIVAR AUTOMATICAMENTE LAS COOKIES
$adjustPathPolicies = '';

if ($_SERVER['REMOTE_ADDR'] == '192.168.2.54') {
    ini_set('display_errors', 1);
}

if ($sitesPruebas) {
    $url_local = 'http://139.162.246.12/netsalesfactory.com/promociones/eThinkig-GKFX2/';
    $web_url = 'http://139.162.246.12/netsalesfactory.com/promociones/eThinkig-GKFX2/';
    $dir_raiz_aplicacion_local = $dir_raiz_aplicacion . 'eThinkig-GKFX2/';
    $path_raiz_aplicacion_local = $dir_raiz_aplicacion_local . '';
    $path_raiz_includes_local = $path_raiz_includes . 'eThinkig-GKFX2/';
    $url_condiciones = $url_local . 'condiciones.php';
    $paintErrors = true;
    $minTimeCookiesStart = 30000;
    $miPathClass = 'http://' . $_SERVER['HTTP_HOST'] . '/netsalesfactory.com/promociones/';
} else {

    $url_local = '';

    //$dir_raiz_aplicacion = '';
    //$path_raiz_aplicacion = '/' . $dir_raiz_aplicacion;          //UTILIZADO PARA INCLUIR LOS FICHEROS CLIENTE DEL FRONT (js,css,img)
    //$path_raiz_includes = $_SERVER['DOCUMENT_ROOT'] . trim($path_raiz_aplicacion);  //UTILIZADO PARA INCLUIR LOS FICHEROS SERVIDOR DEL FRONT .php


    $url_local = 'http://www.gkfx-display-devolucion.com/';
   
    $adjustPathPolicies = 'GKFX_devoluciones/';
    $dir_raiz_aplicacion_local = $dir_raiz_aplicacion . $adjustPathPolicies;
    $path_raiz_aplicacion_local = '/' . $dir_raiz_aplicacion_local;
    $path_raiz_includes_local = $path_raiz_includes . $adjustPathPolicies;
    $url_condiciones = $url_local . $adjustPathPolicies . 'condiciones.html';
    $miPathClass = "http://" . $_SERVER['HTTP_HOST'] . '/netsalesfactory.com/promociones/';
    //$path_raiz_aplicacion = '../';
    $dir_raiz_aplicacion = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $adjustPathPolicies; // 03.04.2013 (M.F.)
}
// END CONFIG DINAMICA (M.F. 19.11.2013)
// 
// 
 $path_raiz_includes_admin	= $path_raiz_includes_local."admin/";
// 
//Variable para la tabla REGISTROS, para diferenciar qué cliente es
$id_client = 25;
$prefFichExtraccion = 'dentalCASER';
$arr_campanas = array('TEST', 'CASER_LEAD');
//echo '$url_local==>'.$url_local;
//Array con todas las creas existentes. 
$arr_creas = array(1 =>
    array(
        'title' => 'GKFX',
        'body' => 'body_1.php',
        'nombpromo' => 'GKFX2_devoluciones',
        'is_mobile' => 0,
        'is_compro_prov' => 1,
        'condiciones' => $url_local . $adjustPathPolicies . 'condiciones.php',
        'protecciondatos' => $url_local . $adjustPathPolicies . 'privacy_policy.php',
        'cookie_css' => 'footer_cookies.css'
    )
);
$is_ctc = 1;

//Parámetros del pixel de TradeDoubler
$organization = '1915991';
$event = '309291';
$nombCliente = 'GKFX';
$secret_pwd_conf = 'p3Rd€r_N@da';
$secret_usr = date('Y-m-d H:i:s');
$secret_pwd = md5($secret_usr . $secret_pwd_conf);
//Pixel expecíficos
$isSpecificPixel = 1;

//Array de display
$arr_isSpecificPixel = array();
/* $arr_isSpecificPixel = array(
  '2261683' => '<img src="http://ads.creafi-online-media.com/pixel?id=2357960&t=2" width="1" height="1" />',
  ,'9999999' => '<script language="JavaScript1.1" src="http://wrap.tradedoubler.com/wrap?id=8498"></script>'
  );
 */


//Para este cliente, lanzaremos 2... deber·n ser seguidos.
$arr_isSpecificPixelJS = array();
/* $arr_isSpecificPixelJS = array(
  '2264375' => array(0 => 'https://secure.adnxs.com/px?id=73601&t=1', 1 => 'https://secure.adnxs.com/seg?add=685956&t=1'),
  '2271169' =>array(0 => 'http://pixel.mathtag.com/event/js?mt_id=224630&mt_adid=111634&v1=&v2=&v3=&s1=&s2=&s3=',1 => '')
  );
 */

$table_registros = 'registros';
$table_provincias = 'provincias';
$table_countries = 'countries';


//sql iNJECTION
$arr_sql_inject_original = array('select', 'insert', 'drop', 'create', 'execute', 'union', 'like', 'truncate', 'delete', '%');
$arr_sql_inject_cambio = array('s3l3ct', '1ns4rt', 'dr0p', 'cr34te', '3x3cut3', '4un10n', '1ik3', 't4unc4t3', 'd31et3', ' ');

//Para generación de CSVs
$sep = ';';
$eol = "\r\n";

$path_raiz_aplicacion.='';
//INCLUDES NECESARIOS TANTO PARA DEBUG COMO PARA LA QUERIES
//echo date('s').']--->'.$miPathClass;
//require_once($miPathClass.'class/class.debug.php');
//$debug_pagina = new CDEBUG();
//require_once($miPathClass.'class/class.bbdd.php');
//require_once($miPathClass.'class/class.dumpvar.php');
//////FUNCIONES COMUNES TANTO AL FRONT COMO AL ADMIN
//require_once($miPathClass.'includes/funciones_comunes.php');
?>