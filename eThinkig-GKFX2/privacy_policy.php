<?php
$privacyPolicyName='GKFX';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>politica privacidad</title>
</head>

<body style="font-family:Arial,Helvetica,sans-serif,Gadget,sans-serif; color:#000000;background-color:#fff;font-size:small;color: #3c3c3c;">


<div style="background-color:#FFF;padding:20px;padding-top:0px;">



  <center><h3 style="font-family:Arial,Helvetica,sans-serif;font-size:140%;">Política de privacidad<br>y protección de datos de <?=$privacyPolicyName?></h3></center>


<p align="JUSTIFY"><span color="#9bba44" style="color: #9bba44;font-size:130%;"><span><b>Pol&iacute;tica de Privacidad GKFX</b></span></span></p>
<ol>
<li>
<p align="JUSTIFY"><span color="#3c3c3c" style="color: #3c3c3c;"><span>(a) GKFX Financial Services cumplir&aacute; en todo momento con la la Ley Org&aacute;nica 15/1999, de 13 de diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal ("LOPD") y su normativa de desarrollo.</span></span></p>
</li>
<li>
<p align="JUSTIFY"><span color="#3c3c3c" style="color: #3c3c3c;"><span>(b) Al aceptar estas condiciones de trading, usted da a entender que nos proporcionar&aacute; su informaci&oacute;n personal que utilizaremos para abrir, administrar y mantener su cuenta de trading con nosotros.</span></span></p>
</li>
<li>
<p align="JUSTIFY"><span color="#3c3c3c" style="color: #3c3c3c;"><span>(c) Nos comprometemos a no vender o transferir su informaci&oacute;n personal a terceros, excepto a los que necesitamos en relaci&oacute;n con el funcionamiento normal de nuestro negocio. Estos incluyen los centros de procesamiento y de verificaci&oacute;n de tarjetas de cr&eacute;dito, los organismos de aplicaci&oacute;n de la ley, cualquier regulador financiero o de otro tipo, nuestros auditores y su Oficial de cumplimiento de Normas (si est&aacute; regulado por el Ente Regulador Financiero de Espa&ntilde;a - Comisi&oacute;n Nacional del Mercado de Valores (CNMV).</span></span></p>
</li>
<li>
<p align="JUSTIFY"><span color="#3c3c3c" style="color: #3c3c3c;"><span>(d) Sin embargo, podremos pasar su informaci&oacute;n a otras compa&ntilde;&iacute;as dentro de nuestro grupo que consideremos puedan ser capaces de proporcionarle un servicio.</span></span></p>
</li>
<li>
<p align="JUSTIFY"><span color="#3c3c3c" style="color: #3c3c3c;"><span>(e) Obtendremos la totalidad o la mayor parte de la informaci&oacute;n sobre nuestros clientes directamente de ellos, pero nos reservaremos el derecho a obtener informaci&oacute;n a trav&eacute;s de otras fuentes.</span></span></p>
</li>
<li>
<p align="JUSTIFY"><span color="#3c3c3c" style="color: #3c3c3c;"><span>(f) Podremos utilizar la informaci&oacute;n que obtengamos para ayudarnos a proporcionarle un mejor servicio o nuevos productos a nuestros clientes.</span></span></p>
</li>
<li>
<p align="JUSTIFY"><span color="#3c3c3c" style="color: #3c3c3c;"><span>(g) Todo el personal de GKFX Financial Services Ltd est&aacute; formado y concienciado en el manejo de informaci&oacute;n personal confidencial.</span></span></p>
</li>
<li>
<p align="JUSTIFY"><span color="#3c3c3c" style="color: #3c3c3c;"><span>(h) Todos los datos personales que est&eacute;n en nuestro poder se mantendr&aacute;n en sistemas de almacenamiento inform&aacute;tico seguros en la medida de lo posible. De lo contrario, mantendremos la informaci&oacute;n de manera segura en archivos f&iacute;sicos impresos. Ninguna persona que no sea autorizadas podr&aacute; acceder a estos sistemas de almacenamiento.</span></span></p>
</li>
<li>
<p align="JUSTIFY"><span color="#3c3c3c" style="color: #3c3c3c;"><span>(i) Nuestro sitio web podr&aacute; instalar cookies en su ordenador de esta manera podremos rendirle un mejor servicio en relaci&oacute;n a sus necesidades ya que conoceremos qu&eacute; &aacute;reas de nuestro sitio web ha estado buscando, y podremos acelerar su navegaci&oacute;n por Internet. Usted tendr&aacute; la opci&oacute;n de inutilizar esta funci&oacute;n a trav&eacute;s de las opciones de configuraci&oacute;n en su ordenador si lo desea, aunque esto puede afectar su capacidad de ver otras partes de la p&aacute;gina web.</span></span></p>
</li>
<li>
<p align="JUSTIFY"><span color="#3c3c3c" style="color: #3c3c3c;"><span>(j) Si bien hacemos todo lo posible para asegurar que toda la informaci&oacute;n que tenemos es correcta y est&aacute; actualizada, es su responsabilidad el proporcionarnos cualquier cambio en su informaci&oacute;n personal.</span></span></p>
</li>
<li>
<p align="JUSTIFY"><span color="#3c3c3c" style="color: #3c3c3c;"><span>(k) Usted tiene el derecho, bajo la Ley de Protecci&oacute;n de Datos de 1999, a solicitar una copia de la informaci&oacute;n que tenemos sobre usted, para que pueda informarnos de cualquier error. Para ello debe escribirnos solicitando esta informaci&oacute;n y proporcionarnos la verificaci&oacute;n de su identidad.</span></span></p>
</li>
</ol>
<p><br /></p>
<p align="JUSTIFY"><span color="#9bba44" style="color: #9bba44;font-size:130%;"><span><b>GKFX. Advertencia de Riesgo</b></span></span></p>
<p>Cualquier producto financiero que negocia con m&aacute;rgenes conlleva un alto riesgo para su capital. Estos productos no son apropiados para todos los inversores y podr&iacute;a perder una cantidad considerablemente superior a la invertida inicialmente. Aseg&uacute;rese de que comprende los riesgos asociados y solicite asesoramiento independiente si lo considera necesario. Consulte nuestro Aviso de Riesgo en nuestros T&eacute;rminos Comerciales.</p>
<p>(a) El Mercado de Divisas y los Contratos por Diferencias con GKFX son productos financieros</p>
<p>especulativos, es decir, son productos que negocian con m&aacute;rgenes. En consecuencia, conllevan de forma inherente un alto riesgo para su capital en comparaci&oacute;n con otros tipos de inversiones, y, por tanto, cabe tener en cuenta que podr&iacute;an hacerle perder una cantidad superior a la invertida inicialmente de manera muy r&aacute;pida. Estos productos son legalmente ejecutables en virtud de la Ley brit&aacute;nica de Mercados y Servicios Financieros del a&ntilde;o 2000 (Financial Services and Markets Act 2000).</p>
<p>(b) Las operaciones especulativas, con m&aacute;rgenes, son operaciones que se fundamentan en la fluctuaci&oacute;n o variaci&oacute;n del precio de un producto. Se establecen en base a la diferencia entre el precio de apertura y el precio de cierre de cada operaci&oacute;n.</p>
<p>(c) No debe operar con productos sobre m&aacute;rgenes salvo que comprenda en su totalidad los riesgos que &eacute;stos conllevan y asimismo disponga de los recursos suficientes para, en el caso de que se diese una fluctuaci&oacute;n adversa en el precio del producto -aunque usted lo considere improbable, poder hacer frente a las obligaciones financieras derivadas del pago de m&aacute;rgenes y p&eacute;rdidas.</p>
<p>(d) Las operaciones especulativas con m&aacute;rgenes son operaciones que permiten el apalancamiento financiero o cierta ratio de endeudamiento, es decir, que le permiten realizar operaciones a gran escala mediante peque&ntilde;as aportaciones monetarias como margen. Si el precio fluct&uacute;a a su favor usted puede aumentar sus beneficios de forma exponencial. Sin embargo, basta con una peque&ntilde;a fluctuaci&oacute;n del precio en su contra para acarrearle cuantiosas p&eacute;rdidas, en cuyo caso puede que le solicitemos que deposite un margen adicional de forma inmediata al objeto de mantener sus operaciones o posiciones abiertas. Usted ser&aacute; responsable de esta actividad y de cualquier p&eacute;rdida derivada del cierre de sus posiciones. Las potenciales p&eacute;rdidas o ganancias derivadas de operaciones con m&aacute;rgenes son, o podr&iacute;an ser, ilimitadas y siempre tendr&aacute; que tener esto en cuenta a la hora de tomar decisiones sobre sus operaciones de inversi&oacute;n.</p>
<p>(e) No todas las operaciones podr&aacute;n abrirse o cerrarse las 24 horas. Muchas de ellas est&aacute;n sujetas a estrictos horarios de apertura y cierre que pueden variar. Encontrar&aacute; dichos horarios disponibles online en nuestras Hojas Informativas sobre Mercados (MIS, por sus siglas en ingl&eacute;s, Market Information Sheets), que procuramos mantener actualizadas sin que medie ninguna obligaci&oacute;n o responsabilidad por nuestra parte en este sentido, ni tampoco en cuanto a su exactitud. Por ejemplo, los d&iacute;as festivos o el cambio de hora estacional afectar&aacute;n a los horarios en que usted pueda operar. Asimismo, un mercado podr&iacute;a suspenderse por m&uacute;ltiples razones y es probable que no se le permita operar durante el lapso de tiempo en que dicho mercado se mantenga suspendido.</p>
<p>(f) Usted realizar&aacute; operaciones sobre nuestros precios y no sobre otros precios de cambio. Dependiendo del mercado, habitualmente nuestros precios estar&aacute;n basados en el precio de cambio, pero pueden fluctuar en relaci&oacute;n a los precios subyacentes o reales por diversos motivos. Las operaciones abiertas s&oacute;lo podr&aacute;n cerrarse y establecerse con nosotros.</p>
<p>(g) GKFX mantendr&aacute; los fondos de clientes minoristas no utilizados en una cuenta bancaria separada. Nos reservamos el derecho de utilizar una parte o el total de dichos fondos hasta el tope que el cliente est&eacute; utilizando para mantener posiciones u operaciones abiertas. Sin embargo, es posible que no se le otorgue protecci&oacute;n completa ni a&uacute;n en esa cuenta separada. Todos los fondos de nuestros clientes particulares est&aacute;n sujetos a la Normativa Monetaria del Cliente de la FCA, salvo si se especifica lo contrario, y est&aacute;n garantizados por dichas Normas en virtud del Sistema de Compensaci&oacute;n de Servicios Financieros (Financial Services Compensation Scheme) hasta un m&aacute;ximo de 50.000 libras esterlinas desde el 1 de enero de 2010. (V&eacute;ase Dep&oacute;sitos/Retirada/Saldos) (h) Si tiene alguna duda acerca de los riesgos derivados del Mercado de Divisas, de los CFD, le recomendamos que busque consejo o asesor&iacute;a profesional independiente antes de continuar.</p>
<p>(i) Si est&aacute; considerando operar con acciones de la empresa para la que trabaja, deber&iacute;a buscar de antemano asesor&iacute;a o consejo legal para asegurarse que no est&aacute; infringiendo ninguna normativa.</p>
<p>(j) Las operaciones sobre m&aacute;rgenes no est&aacute;n dise&ntilde;adas necesariamente para sustituir otros m&eacute;todos existentes o tradicionales de inversi&oacute;n y por lo tanto no son id&oacute;neas para todo el mundo.</p>

</div>




</body>
</html>
