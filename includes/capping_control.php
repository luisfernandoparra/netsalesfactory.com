<?php
/**
 * Include que controlará el capping.
 * Si se da el capping se mandará a otro WS 
 * Ahora mismo lo realizaremos muy simple por cliente.
 * Deben existir las variables alternativas para mandarlo a otro ws
 */
if (!empty($id_cliente) && is_numeric($id_cliente) && !empty($url_ws) && !empty($file_ws) && !empty($url_ws_alternative) && !empty($file_ws_alternative)) {
    $db_conn = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
    $db_conn->connectDB();
    //Vemos a ver si existe un capping para este cliente
    $sql = 'select capping,tipo_capping from %s where b_activo=1 and id_cliente=%s';
    $sql = sprintf($sql, $table_registros_capping,$id_cliente);
    $db_conn->getResultSelectArray($sql);
    
   // echo $sql."<br>";
    $arr_capp = $db_conn->tResultadoQuery;
    if (count($arr_capp)==1) {
        $capping_max = $arr_capp[0]['capping'];
        $capping_type = (int)$arr_capp[0]['tipo_capping'];
        $sql = 'select count(id_registro) as cont from %s where ';
        $sql = sprintf($sql,$table_registros);
        switch ($capping_type) {
            case 1: //Diario
                $sql.= ' date_format(fecha_insercion,\'%Y%m%d\')=date_format(now(),\'%Y%m%d\')';

                break;
            case 2: //Mensual
                $sql.= ' date_format(fecha_insercion,\'%Y%m\')=date_format(now(),\'%Y%m\')';
                break;

            default:
                break;
        } //switch
        $db_conn->getResultSelectArray($sql);
       // echo $sql;
        $arr_dat = $db_conn->tResultadoQuery;
        $curr_items = (int)$arr_dat[0]['cont'];
        unset($arr_dat);
        if ($curr_items>$capping_max) {
           // echo "ALTERNATIVE";
            $url_ws = $url_ws_alternative;
            $file_ws = $file_ws_alternative;
        } /*else {
            echo "no alternativo";
        }
         * 
         */
    }
    unset($arr_capp);
    $db_conn->disconnectDB();
    /*echo "<hr>";
    echo "url_ws: $url_ws file_ws $file_ws"
     * ;
     */
}