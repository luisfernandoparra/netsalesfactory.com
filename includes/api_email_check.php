<?php
/*
 * SCRIPT "LOCAL" DEL MODULO PARA EL API EMAIL PING-CHECK
 *
 * ARCHIVOS INCLUIDOS:
 * - class/class.email_check.php
 * - promociones/conf/config_web.php
 * 
 * 2014.11.20 M.F.
 * v.1.0.1
 * 
 * @date 2016.09.26
 * Añadimos el forzado a que la cabecera de respusta sea json utf8
 */
@session_start();
$response['success']=false;

header("Content-type: application/json; charset=utf-8");
//if(!isset($_SERVER['HTTP_REFERER']))	// IMPIDE EL ACCESO DIRECTO
//{
//  $_REQUEST['txtStrangeError']='direct script access ERROR';
//  include('strange_errors.php');
//	$res=$response['success'];
//	$res=json_encode($res);
//  die($res);
//}

if(count($_GET)) $arraExtVars=$_GET;
else $arraExtVars=$_POST;

if(empty($arraExtVars['email']))	// SALE SI FALTA EL EMAIL
{
	$res=$response['success'];
	$res=json_encode($res);
  die($res);
}

ini_set('display_errors',1);
set_time_limit(10);
include('../conf/config_web.php');
$resOutput=array();

$checkEmail=$_REQUEST['email'];
$sanitizedData=filter_var($checkEmail, FILTER_SANITIZE_EMAIL);

if($controlChekAlgorithm != $_REQUEST['hashPipe'])	// SI EL CHECK NO COINCIDE, SE DEVELVE UN JSON FALSO
{
	$userAccessSec=rand(0,10000);
	$idCustomerSec=rand(888,10000);
	$resOutput['success']=0;
	$resOutput['errorDetail']='Error en el e-mail';
	$resOutput['errorMessage']='Parece que el Email es incorrecto';
	$resOutput['statusId']=$idCustomerSec;
	$res=json_encode($resOutput);

	$line='';
	$logFile = $path_raiz_includes.'log/leads_hack-e_'.date('Y-m-d').'.txt';
	if(isset($_SERVER['HTTP_REFERER']))
		$line.=', procedencia: '.$_SERVER['HTTP_REFERER']."\r\n";

	if(isset($_SERVER['REQUEST_URI']))
		$line.='params: '.$_SERVER['REQUEST_URI']."\r\n";

	$line.=date('H:i:s')."\r\n";
	$line.='email        : '.$_REQUEST['email']."\r\n";
	$line.='URL server   : '.$_SERVER['SERVER_NAME']."\r\n";
	$line.='hash por GET : '.$_REQUEST['hashPipe']."\r\n";
	$line.='hash esperado: '.$controlChekAlgorithm."\r\n";
	$line.='IP cliente   : '.$_SERVER['REMOTE_ADDR']."\r\n";
	$line.='--'."\r\n";
	$dataFileLog=$line;
	$handleLog=@fopen($logFile,'a+');
	@fwrite($handleLog, strip_tags($dataFileLog));
	@fclose($handleLog);

	die($res);
}


if(filter_var($sanitizedData, FILTER_VALIDATE_EMAIL) === false)
{
	$res=$response['success'];
	$res=json_encode($res);
  die($res);
}
$idClient = @$arraExtVars['id_client'] ? $arraExtVars['id_client'] : 0;
$idClient = normalizeInputData('INPUT,STRINGS,SQLINJECTION,NUMBER',$idClient);

$email = normalizeInputData('INPUT,STRINGS,SQLINJECTION,EMAIL',strtolower(trim($_REQUEST['email'])));
				
//echo "\r\n".$sitesPruebas,' - ',$currPath.'<hr><pre>';print_r($_REQUEST);

// START SOLO PARA PRUEBAS EN MARAVILLAO
$urlUri=substr($thisUrl['path'],0,-1);
$posLastBackSlash=strrpos($urlUri,'/')+1;
$currPath=substr($thisUrl['path'],0,$posLastBackSlash);
// END SOLO PARA PRUEBAS EN MARAVILLAO


include($path_raiz_includes.'class/class.email_check.php');
$thisObject=new EmailCheck($sitesPruebas, $currPath);	// SE INICIALIZA EL OBJETO CON LOS METODOS NECESARIOS

$thisObject->currEmail=$email;
$thisObject->id_client=$idClient;
$resWs=$thisObject->execBridgeLocalWs($secret_pwd);
$arrResWs=json_decode($resWs);

//print_r($resWs);die();

$resOutput['success']=@$arrResWs->statusId < 3 ? 1 : 0;
$resOutput['errorDetail']=@$arrResWs->errorMessage;
$resOutput['errorMessage']=@$arrResWs->statusId > 2 ? 'Parece que el Email es incorrecto' : '';
$resOutput['statusId']=@$arrResWs->statusId;

//echo'<pre>';print_r($resOutput);
$response=json_encode($resOutput);
die($response);
