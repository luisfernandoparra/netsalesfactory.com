<?php
/**
 * Aquí hemos cargado ya el config y por tanto el funciones_comunes
 */
//Cogemos solamente si es el de CTC o similar. El otro se da por hecho que es de LEAD

$sourcetype = (empty($_REQUEST['sourcetype'])) ? '' : normalizeInputData('STRINGS,SQLINJECTION',trim(strtoupper($_REQUEST['sourcetype'])));

/*
?>
<script type="text/javascript">
var sourcetype = '<?php echo $sourcetype;?>';
console.log("<?=@$_SERVER['SERVER_NAME'] == 'tutarjeta.netsalesfactory.com'?>")
</script>
<?php
*/

/**
 * Página que cogerá TODAS las variables de Pixeles, parámetros etc, y 
 * dejará saltar los pixeles o no.
 */
// Primero Vemos si cuando se lanza la página, es de otra crea distinta a la misma página
$http_referer = @$_SERVER['HTTP_REFERER'];
$http_server_name = @$_SERVER['SERVER_NAME']; //dominio
/**
 * saltarPixeles. Si TRUE: la página viene correctamente y podemos hacer para que salten los pixeles.
 */
$saltarPixeles = !(@$http_referer == '' || (strpos(@$http_referer, @$http_server_name) === false || strpos(@$http_referer, @$http_url) !== false)) || !empty($_REQUEST['debug']);

$leadNumber = (!empty($_REQUEST['leadNumber'])) ? normalizeInputData('STRINGS,SQLINJECTION',trim($_REQUEST['leadNumber'])) : '';
$id_source = (!empty($_REQUEST['id_source'])) ? normalizeInputData('STRINGS,SQLINJECTION,NUMBER,INPUT',trim($_REQUEST['id_source'])) : '';
$id_client = (!empty($_REQUEST['id_client'])) ? normalizeInputData('STRINGS,SQLINJECTION,NUMBER,INPUT',trim($_REQUEST['id_client'])) : '';
//$id_crea = (!empty($_REQUEST['id_crea'])) ? trim($_REQUEST['id_crea']) : $id_client;
$id_crea = (!empty($_REQUEST['id_crea'])) ? normalizeInputData('STRINGS,SQLINJECTION,NUMBER,INPUT',trim($_REQUEST['id_crea'])) : '';


if (!empty($_REQUEST['debug'])):
    

?>
<script type="text/javascript">
console.log("httpReferer: <?=$http_referer?>");
console.log("httpServerName: <?= $http_server_name ?>");
console.log("http_url: <?= $http_url?>");
console.log("satarPixeles: <?= $saltarPixeles?>");
console.log("leadNumber:<?php echo $leadNumber;?>");
</script>
<?php
endif;

/**
 * cookieEnable: if TRUE: usuario acepta políticas de cookies
 */
$cookieEnable = (!empty($_REQUEST['cookieEnable'])) ? (trim($_REQUEST['cookieEnable'])==1) : false;


/**
 * Debido a que si el usuario es Moroso, le llevamos a la thankyoupage, pero no tiene que saltar el pixel
 */
$correctProcess = empty($_REQUEST['result']) || (!empty($_REQUEST['result']) && strtoupper(trim($_REQUEST['result']))!='KO');

/*$trdbl_organization = (!empty($_REQUEST['trdbl_organization'])) ? trim($_REQUEST['trdbl_organization']) : '';
$trbdl_event = (!empty($_REQUEST['trbdl_event'])) ? trim($_REQUEST['trbdl_event']) : '';
$trbdl_program = (!empty($_REQUEST['trbdl_program'])) ? trim($_REQUEST['trbdl_program']) : '';
 * 
 */