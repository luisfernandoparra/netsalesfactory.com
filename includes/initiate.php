<?php
//Fichero que inicia las variables
$id_crea = (empty($_REQUEST['cr']) || !is_numeric($_REQUEST['cr']) || !array_key_exists($_REQUEST['cr'],$arr_creas)) ? 1 : trim($_REQUEST['cr']);


if (file_exists($path_raiz_includes.'conf/config_web_'.$id_crea.'.php')) {
	include($path_raiz_includes.'conf/config_web_'.$id_crea.'.php');
}

$cli = isset($_REQUEST['cli']) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION',trim($_REQUEST['cli'])) : '';

if (isset($arr_creas) && isset($arr_creas[$id_crea]['is_mobile'])) {$is_movil=normalizeInputData('STRINGS,SQLINJECTION',$arr_creas[$id_crea]['is_mobile']);}
$id_source = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? normalizeInputData('INPUT,STRINGS,SQLINJECTION',$_REQUEST['fuente']) : '';
//parametro para un pixel especifico heredado de Sanitas. Seguramente habra que ponerlo aqui.
$tduid = (!empty($_REQUEST['tduid']) && trim($_REQUEST['tduid'])!='') ? normalizeInputData('INPUT,STRINGS,SQLINJECTION',trim($_REQUEST['tduid'])) : '';
//Variable que identifica si es click to call y necesarias para el JS
$is_ctc = (int)!empty($_REQUEST['ctc']);
$is_movil = (!empty($is_movil)) ? normalizeInputData('STRINGS,SQLINJECTION',$is_movil) : 0;
$path_raiz_ws = (!empty($path_raiz_ws)) ? normalizeInputData('STRINGS,SQLINJECTION',$path_raiz_ws) : '';
$url_ws = (!empty($url_ws)) ? normalizeInputData('SQLINJECTION',$url_ws) : '';
$file_ws = (!empty($file_ws)) ? normalizeInputData('STRINGS,SQLINJECTION',$file_ws) : '';


//Informacion sobre Pixeles
$arr_campanas = (isset($arr_campanas) && count($arr_campanas) > 0) ? $arr_campanas :  array();
$campanas = '';

foreach ($arr_campanas as $v) {
	$campanas .= '\''.$v.'\',';	
}

$pix_retargeting = '';
$arr_pixel_JS = array(0=>'',1=>'',2=>''); //se diferenciaran por boton clicado.
if ($campanas != '') {$campanas = normalizeInputData('STRINGS',substr($campanas,0,-1));}

if (isset($isSpecificPixel) && !empty($isSpecificPixel) && $isSpecificPixel===1) {
	if (isset($arr_isSpecificPixel) && is_array($arr_isSpecificPixel) && $id_source!='' && is_numeric($id_source) && array_key_exists($id_source,$arr_isSpecificPixel)) {
		$pix_retargeting = $arr_isSpecificPixel[$id_source];
	} else 	if (isset($arr_isSpecificPixelJS) && is_array($arr_isSpecificPixelJS) && $id_source!='' && is_numeric($id_source) && array_key_exists($id_source,$arr_isSpecificPixelJS)) {
		$arr_pixel_JS[0] = $arr_isSpecificPixelJS[$id_source][0];
		$arr_pixel_JS[1] = $arr_isSpecificPixelJS[$id_source][1];
		$arr_pixel_JS[2] = $arr_isSpecificPixelJS[$id_source]['isImageObject'];
	//echo $id_source;
	//new dBug($arr_isSpecificPixel);
	}
}


//Prerellno. cuidado que habra sites que tendren en el campo texto por defecto.
$nombre = (!empty($_REQUEST['nombre']) && trim($_REQUEST['nombre'])!='') ? normalizeInputData('INPUT,STRINGS,SQLINJECTION',ucwords(strtolower(trim($_REQUEST['nombre'])))) : '';
$apellidos = (!empty($_REQUEST['apellidos1']) && trim($_REQUEST['apellidos1'])!='') ? normalizeInputData('INPUT,STRINGS,SQLINJECTION',ucwords(strtolower(trim($_REQUEST['apellidos1'])))) : '';
$email = (!empty($_REQUEST['email']) && trim($_REQUEST['email'])!='') ? normalizeInputData('INPUT,STRINGS,SQLINJECTION,EMAIL',strtolower(trim($_REQUEST['email']))) : '';
$telf = (!empty($_REQUEST['telf']) && trim($_REQUEST['telf'])!='') ? normalizeInputData('INPUT,STRINGS,SQLINJECTION',strtolower(trim($_REQUEST['telf']))) : '';

$arr_input = array('á','é','í','ó','ú','Á','É','Í','Ó','Ú');
$arr_output = array('a','e','i','o','u','a','e','i','o','u');
$apellidos = (!empty($_REQUEST['apellidos']))? normalizeInputData('INPUT,STRINGS,SQLINJECTION',trim($_REQUEST['apellidos'])):'';
$provincia = (!empty($_REQUEST['provincia']))?normalizeInputData('INPUT,STRINGS,SQLINJECTION',strtolower(trim($_REQUEST['provincia']))):'';
$provincia = str_replace($arr_input,$arr_output,$provincia);
$dbug =  (!empty($_REQUEST['dbug']) && trim($_REQUEST['dbug'])!='') ? true : false;
if ($dbug) {
	new dBug($_REQUEST);
}


$is_prov = 0;
if(isset($arr_creas) && isset($arr_creas[$id_crea]['is_compro_prov']) && $arr_creas[$id_crea]['is_compro_prov'] == 1) {
	$is_prov = $arr_creas[$id_crea]['is_compro_prov'];
	$sql = 'select id_provincia as id, provincia as name from %s order by provincia';
	$sql_provs = sprintf($sql,$table_provincias);
	$conexion_prov  = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name,null,'utf8',$port);
	$conexion_prov ->connectDB();
	$conexion_prov ->getResultSelectArray($sql_provs);
	$arr_prov = $conexion_prov->tResultadoQuery;
	$conexion_prov->disconnectDB();
}

/**
 * Cogemos información sobre ipSearchEngine
 */
/*
 * COMENTADO 2016.02.08 M.F.  ---> CAUSA RETARDOS DE MINUTOS A LA CARGA DE LAS LANDINGS DE BORSENYA
 * 
*/


if (!empty($ws_ip_check) && empty($_SESSION['ws_ip_id'])) {
    $ip = getRealIpAddr();
    $ws_ip_arr_parameters['ip'] = $ip;
    $url = $ws_ip_url.'?'.http_build_query($ws_ip_arr_parameters);
//echo $url;die();
    $res = file_get_contents($url);
    $_SESSION['ws_ip_resp'] = $res;
   
    $res_array = json_decode($res, true);
    if ($res_array['result']=='OK') {
        $_SESSION['ws_ip_id'] = $res_array['id'];
    }
    //echo 'pillado';
   // print_r($_SESSION);
}

?>