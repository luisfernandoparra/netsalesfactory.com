<?

  // primero hay que incluir la clase phpmailer para poder instanciar

  //un objeto de la misma

  //LO COMENTAMOS PQ EN LOS BUCLES NOS PETA PQ NOS IMPIDE rEDECLARAR la clase 

  //POR TANTO LO HEMOS DE LLAMAR EXTERNAMENTE require $phpmailerclass; //"../class/class.phpmailer.php";



  //instanciamos un objeto de la clase phpmailer al que llamamos 

  //por ejemplo mail

  	$mail = new phpmailer();

	$mail->SetLanguage("en", '../../class/');

  //Definimos las propiedades y llamamos a los métodos 

  //correspondientes del objeto mail



  //Con PluginDir le indicamos a la clase phpmailer donde se 

  //encuentra la clase smtp que como he comentado al principio de 

  //este ejemplo va a estar en el subdirectorio "class"

  $mail->PluginDir = $smtpclass;   //"../class/";



  //Con la propiedad Mailer le indicamos que vamos a usar un 

  //servidor smtp

  $mail->Mailer = "smtp";



  //Asignamos a Host el nombre de nuestro servidor smtp

  $mail->Host = $email_smtp_server;

//	$mail->Host = "localhost";



  //Le indicamos que el servidor smtp requiere autenticación

  $mail->SMTPAuth = $email_smtp_authentication;

  

  $mail->CharSet = "utf-8";

  

  //Le decimos cual es nuestro nombre de usuario y password

  $mail->Username = $email_smtp_user; 

  $mail->Password = $email_smtp_password;



  //Indicamos cual es nuestra dirección de correo y el nombre que 

  //queremos que vea el usuario que lee nuestro correo

  $mail->From = $email_from_smtp;

  $mail->FromName = $email_from_name;



  //el valor por defecto 10 de Timeout es un poco escaso dado que voy a usar 

  //una cuenta gratuita, por tanto lo pongo a 30  

  $mail->Timeout=30;

  

  //Indicamos cual es la dirección de destino del correo

  $mail->AddAddress($dir_envio);  

  

  if(isset($dir_envio_cco)){

	  //$mail->AddBCC($dir_envio_cco);

	  array_map(array($mail, 'AddBCC'), explode(';', $dir_envio_cco));

  }

  //Adjuntamos archivos

  if($attach_name!=""){

  	$mail->AddAttachment($attach_path, $attach_name);

  }

  

  //Asignamos asunto y cuerpo del mensaje

  $mail->Subject = $asunto_envio;

  

  $template = $plantilla_envio;			//plantila que se utiliza para el envio

   

  $body = join ("",file($template));   //reemplazamos los textos del template

  foreach ($template_vars as $k=>$v) 

		$body = str_replace ($k,$v,$body);

		

/*	

	echo "<br>----------<br>";

	echo $body;	  

	echo "<br>----------<br>";



	echo "<br>smtp_correo=".$email_smtp_server;

	echo "<br>user_smtp_correo=".$email_smtp_user;

	echo "<br>password_smtp_correo=".$email_smtp_password;

	echo "<br>from_smtp_correo=".$email_from_smtp;

	echo "<br>from_nombre_correo=".$email_from_name;	

	echo "<br>dir_envio=".$dir_envio;

	echo "<br>asunto_envio=".$asunto_envio;

*/

  

  $mail->Body = $body;



  //Definimos AltBody por si el destinatario del correo no admite email con formato html 

  $mail->AltBody = $body;



  //se envia el mensaje, si no ha habido problemas 

  //la variable $exito tendra el valor true  

  $exito = $mail->Send();  

  

 // echo $mail->ErrorInfo; 

  //Si el mensaje no ha podido ser enviado se realizaran 4 intentos mas como mucho 

  //para intentar enviar el mensaje, cada intento se hara 5 segundos despues 

  //del anterior, para ello se usa la funcion sleep	

  $intentos=1; 

  while ((!$exito) && ($intentos < 1)) {

  		

		sleep(5);

     	//echo $mail->ErrorInfo;

     	$exito = $mail->Send();	

     	$intentos=$intentos+1;		

   } 

   //echo $exito."@@".$intentos;		

   //echo "<br/>".$mail->ErrorInfo;

   if(!$exito)

   {

		//echo "Problemas enviando correo electrónico a ".$valor;

		//echo "<br/>".$mail->ErrorInfo;

		$error_envio = 1;	

   }

   else

   {

   		//echo "Mensaje enviado correctamente";

		$error_envio = 0;

   } 

?>

