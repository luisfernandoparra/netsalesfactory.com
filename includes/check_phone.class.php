<?php
class remoteCheckPhone //extends Base
{
	public $error;
	public $flagDebug;
	public $idcheck;
	public $timeout;
	public $landingMainName;
	public $phoneCheckUserAccess;
	public $phoneCheckPasswordAccess;
	public $phoneCheckUrlTarget;
	public $idChekTel;
         
         public $originalResponse;

//******************************************************
//	INICIALIZACION DE LA CLASE
//******************************************************
	public function __construct()
	{
		$this->succesResponse=false;
		$this->flagDebug=false;
		$this->idcheck='';
		$this->timeout=100;
		$this->landingMainName='';
		$this->phoneCheckUserAccess='';
		$this->phoneCheckPasswordAccess='';
		$this->idChekTel=0;
	}

//******************************************************
//	
//******************************************************
	public function phoneCheckRemote($phoneToCheck=0)
	{
		$dataSent=array(
			'usr'=>$this->phoneCheckUserAccess,
			'pwd'=>$this->phoneCheckPasswordAccess,
			'telf'=>$phoneToCheck,
			'source'=>$this->landingMainName
		);

		$fields_string=http_build_query($dataSent);
		$ch=curl_init();
		curl_setopt($ch,CURLOPT_URL,($this->phoneCheckUrlTarget));
		curl_setopt($ch,CURLOPT_POST,count($dataSent));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$this->timeout);
		curl_setopt($ch,CURLOPT_HEADER,0);
		$res=curl_exec($ch);
		curl_close($ch);

		if(substr($res,0,1) == '-')	// NO SE ENCONTRARON TELEFONOS A DEVOLVER
		{
			$this->succesResponse = false;
		}

		$respArray=json_decode($res);
		$this->idChekTel=$respArray->idcheck ? $respArray->idcheck : 0;
		$this->succesResponse=$respArray->resp > 0 ? 1 : 0;
                  if (!empty($respArray->originalresp))
                      $this->originalResponse = $respArray->originalresp;

		return $this->succesResponse;
	}
}

?>