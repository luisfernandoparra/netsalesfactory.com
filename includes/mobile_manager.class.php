<?php
@session_start();
include($path_raiz_includes.'includes/mobile_detect.php');

class divicesManager extends Mobile_Detect
{
//  $layoutType='';
  /*
   * Constructor de la clase
   *
   */
	public $mites;
	
  function __construct()
  {
  }

  public function layoutTypes()
  {
	return array(
	  '',	// STANDARD PC
	  'mob_',
	  'tab_'
	);
  }

  public function initLayoutType()
  {
	if(!class_exists('Mobile_Detect'))
	{
	  echo '<b>ERROR</b> :: NO CLASS MOBILE DETECT FOUND!';
	  return '';
	}

	$detect=new Mobile_Detect;
	$this->isMobile=$detect->isMobile();
	$this->isTablet=$detect->isTablet();
	$layoutTypes=$this->layoutTypes(); // ARRAY LOCAL CON LOS PREFIJOS UTILIZADOS PARA CANALIZAR SEGÚN TIPO DE DISPOSITIVO UTILIZADO

	// Set the layout type.
	if(isset($_GET['layoutType']))
	{
	  $layoutType = $_GET['layoutType'];
	}
	else
	{
	  if(empty($_SESSION['layoutType']))
	  {
		$layoutType=($this->isMobile ? ($this->isTablet ? $layoutTypes[2] : $layoutTypes[1]) : null);
	  }
	  else
	  {
		$layoutType=$_SESSION['layoutType'];
	  }
	}

	// Fallback. If everything fails choose classic layout.
	if(!in_array($layoutType, $layoutTypes))
	{
	  $layoutType='';	// STANDARD PC LAYOUT
	}

	$_SESSION['layoutType'] = $layoutType;
	return $layoutType;
  }
}

//$layoutType=initLayoutType();
//echo $layoutType.'<pre>';print_r($_REQUEST);
?>