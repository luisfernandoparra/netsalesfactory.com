<?php
@session_start();
include($path_raiz_includes.'includes/mobile_detect.php');

$layoutType='';

function layoutTypes($new_template=0)
{
  if($new_template)
	return array(
	  0,	// STANDARD PC
	  1,
	  2
	);

  return array(
	'',	// STANDARD PC
	'mob_',
	'mob_'	// NOTA: CAMBIAR ESTA ETIQUETA EN CASO SE DEBAN GESTIONAR LAS TABLETS DE MANERA DISTINTA A LOS MÓVILES
  );
}

function initLayoutType($new_template=0)
{

  if(!class_exists('Mobile_Detect'))
  {
	echo '<b>ERROR</b> :: NO CLASS MOBILE DETECT FOUND!';
	return '';
  }

  $detect=new Mobile_Detect;
  $isMobile=$detect->isMobile();
  $isTablet=$detect->isTablet();
  $layoutTypes=layoutTypes($new_template); // ARRAY LOCAL CON LOS PREFIJOS UTILIZADOS PARA CANALIZAR SEGÚN TIPO DE DISPOSITIVO UTILIZADO
	$conRes=0;
	
	unset($_SESSION['deviceMobileData']);
	foreach($detect->getRules() as $name => $regex)
	{
		$check = $detect->{'is'.$name}();
		if($check){@$_SESSION['deviceMobileData'][]=$name;}
	}

  if(isset($_GET['layoutType']))
  {
		$layoutType = $_GET['layoutType'];
  }
  else
  {
		if(empty($_SESSION['layoutType']))
		{
			$layoutType=($isMobile ? ($isTablet ? $layoutTypes[2] : $layoutTypes[1]) : null);
		}
		else
		{
	// ACTIVAR LA LINEA AQUI ABAJO EN REAL
	//	  $layoutType=$_SESSION['layoutType'];
		}
  }

  // Fallback. If everything fails choose classic layout.
  if(!in_array(@$layoutType, $layoutTypes))
  {
		$layoutType=null;	// STANDARD PC LAYOUT
  }

// ACTIVAR LA LINEA AQUI ABAJO EN REAL
//  $_SESSION['layoutType'] = $layoutType;
  return @$layoutType;
}
$layoutType=initLayoutType(@$arr_creas[$id_crea]['new_template']);
?>