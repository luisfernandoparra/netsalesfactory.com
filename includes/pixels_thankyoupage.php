
<?php

/**
 * Código PHP que lanzará los pixeles de la thankyoupage
 */
/*
  ALTER TABLE  `pixels_management` ADD  `environment_add_info` VARCHAR( 255 ) NULL COMMENT  'Informaciíon adicional dependiente del entorno. Por jeemplo, CTC en Sanitas-netsalesfac' AFTER  `id_pix_outputformat` ,
  ADD INDEX (  `environment_add_info` )
 */

//Hacemos saltar los pixeles correspondientes.
$debug = ($http_server_name=='tutarjeta.netsalesfactory.com');
if ($saltarPixeles) {
    $cls_pix = new pixel(false);
    $cls_pix->set_environment_add_info('');
    if (!empty($id_crea)) {
        //Sacamos los pixeles que saltan siempre para la página en cuestión
        $arr = $cls_pix->get_pixels($id_crea);
		?>
		
		<?php 
        if ($sourcetype != '') {
            $cls_pix->set_environment_add_info($sourcetype);
            $arr = array_merge($arr, $cls_pix->get_pixels($id_crea));
            $cls_pix->set_environment_add_info('');
        }
        if (!empty($id_source)) {
            //Si el source no es 0 y no vacío, sacamos los datos para ese source.
            //como debemos sacar los de cookiesEnbale si están a 1, el source lo volvemos a poner a ''
            $cls_pix->set_Id_source($id_source);
            $arr = array_merge($arr, $cls_pix->get_pixels($id_crea));
            if ($sourcetype != '') {
                $cls_pix->set_environment_add_info($sourcetype);
                $arr = array_merge($arr, $cls_pix->get_pixels($id_crea));
                $cls_pix->set_environment_add_info('');
            }
            $cls_pix->set_Id_source('');
        }
        if ($cookieEnable) {
            //Sacamos los Pixeles que son de Cookies
            $cls_pix->set_Cookie_policy(1);
            $arr = array_merge($arr, $cls_pix->get_pixels($id_crea));
            if (!empty($id_source)) {
                $cls_pix->set_Id_source($id_source);
                $arr = array_merge($arr, $cls_pix->get_pixels($id_crea));
                $cls_pix->set_Id_source('');
            }
            if ($sourcetype != '') {
                $cls_pix->set_environment_add_info($sourcetype);
                $arr = array_merge($arr, $cls_pix->get_pixels($id_crea));
                if (!empty($id_source)) {
                    $cls_pix->set_Id_source($id_source);
                    $arr = array_merge($arr, $cls_pix->get_pixels($id_crea));
                }
            }
        }
		
        foreach ($arr as $pixel) {
			
            echo $pixel."<br>";
        }
        
    }
    unset($cls_pix);
}