<?php
/*
 * RECEPTOR GENERICO WS PARA PRUEBAS
 */
header('Content-Type: text/html; charset=utf-8');
//echo '<pre>';print_r($_REQUEST);echo'</pre>';exit;

$response=array(
	'success'=>0,
	'message'=>'response default WEBSERVICE DUJOK TEST!!',
	'errorMessage'=>'response default ERROR WEBSERVICE DUJOK TEST!!'
);

/*
 * SEGUN EL ID DEL CLIENTE (refCl), SE SIMULA EL PROCESO DEL WS
 */
switch($_REQUEST['id_client'])
{
	case 1:
		$response=array(
			'success'=>1,
			'otro'=>$_REQUEST['refCli'],
			'txtError'=>''
		);
		break;

	case 44:
		$response['success']=rand(0,1);
//		$response['errorMessage']='response default ERROR WEBSERVICE DUJOK TEST';
		break;
	default:
		$response['success']=false;
		$response['nota_especifica']=('NO SE HAN RECIBIDO PARAMETROS, puede haber sido ejecutado un CURL por S.O y NO por PHP');
		// SPECIFIC WS emailverifyapi.com PROVIDER RESPONSE
		$response['status']='Bad';
		$response['additionalStatus']='MailboxDoesNotExist';
		$response['emailAddressProvided']=$_REQUEST['email'];
		$response['emailAddressChecked']=$_REQUEST['email'];
		$response['emailAddressSuggestion']='';
}

if($_REQUEST['key'] === '1CEB2B97')	// SIMULAR WS emailverifyapi.com
{
	if($_REQUEST['email'] === 'mario.francescutto@dujok.com')	// RESPUESTA CORRECTA
	{
		$response['success']=true;
		$response['message']='Success';

		// SPECIFIC WS emailverifyapi.com PROVIDER RESPONSE
		$response['status']='Ok';
		$response['additionalStatus']='Success';
		$response['emailAddressProvided']=$_REQUEST['email'];
		$response['emailAddressChecked']=$_REQUEST['email'];
		$response['emailAddressSuggestion']='';
	}
}

$res=is_array($response) ? json_encode($response) : $response;
die($res);
