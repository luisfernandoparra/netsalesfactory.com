<?php
/*  FUNCIONES INCLUIDAS
	ejecutaSELECT($pQuery,$show_db,$debug_pagina) : Para ejecucion de consultas SELECT. INCLUYE LA POSIBILIDAD DE MOSTRAR DEBUG CON LA QUERY Y EL RECORDSET
	show_debug($obj,$show_db=0) : Para sacar el debug de las consultas
	function envia_email($b,$from,$to): en caso de error, envio de email para ver que posible error producido{			
	$emailFrom ="manuel.fonseca@eone.es";
	$destino="manuel.fonseca@arnold4d.es"; 
	$copia="luisfernando.parra@arnold4d.es"; 
	$subject ="Aerocity:LOG XML Envio / Respuesta Reserva SINFE";
	$headers = 'From: Aerocity <'.$emailFrom.'>' . "\r\n";
	//$headers .= 'From: $emailFrom <$emailFrom>' . '\r\n';
	$headers .= 'Reply-To: Aerocity <'.$emailFrom.'>' . '\r\n';
	$headers .= 'X-Mailer: PHP/' . phpversion(). '\r\n';
	$headers .= 'Content-Type: text/html;charset=UTF-8' . '\r\n';
	//$header = "From: ". $Name . " <" . $email . ">\r\n"; //optional headerfields  
	mail("$destino,$copia",$subject,$b,$headers);	 		
}
	ejecutasegunversion($tipo,$q) : Ejecucion de codigo segun version del PHP. Ahora mismo operativa para el JSON
	escape($string): Tratamiento de los JSON
	between: funciono que se utiliza para saber cunado ocurrio algo Ej hace 4 semanas y 10 minutos,opcionalmente se pasa el lang para sacarlo en igles
	formatear_fecha: pasada una fecha estilo 2-3-2011 devuelve 2 de Marzo del 2011 y tambien lo hace en ingles March,2 2011
	dia_semana: dada una fecha obtenemos el dia de la semana que le corresponde
	youtube_vimeo_parser: parseo de la url vimeo / you tube ...
	get_filesize($size); dada el peso de un fichero en bytes, lo devuelve convertido en Kb,MB,GB bytes
	show_cssjs_file($filepath,$type): dado el nombre y el tipo de fichero, pintamo <link> o <script>
	obtener_dominio($url): obtiene el dominio asociado a una url completa. de momento SOLO funciona con http://
	ObtenerNavegador($user_agent): a partir de la variable HTTP_USER_AGENT, obtiene que navegdor estas usando,pequeño bug, el chrome lo trata como mozilla..
	FUNCIONES TRATAMIENTO FECHAS: bateria de funciones de tratamiento de fechas en Mysql
*/
//
function ejecutaInsertsUpdate($queryList,$show_db,$debug_pagina,$pattern = "||") {
	$arr_sql = explode($pattern,$queryList);
	$error = 0;
	$sql_error = "";
	$id = '';
	try {
		if (count($arr_sql) > 0) {
			$conexion  = new CBBDD($GLOBALS['db_type'], $GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pass'], $GLOBALS['db_name'],$GLOBALS['debug_pagina']);
			$conexion ->connectDB();
			$cur_conn_id = $conexion->get_id_conexion();
			$sql="SET AUTOCOMMIT=0;";
			$conexion->ejecuta_query($sql);
			$sql="BEGIN";
			$conexion->ejecuta_query($sql);
			for ($i=0;$i<count($arr_sql); $i++) {
					if ($error == 0) {
						$sql_query = trim($arr_sql[$i]);
						if ($sql_query != "") {
							$conexion->ejecuta_query($sql_query);
							show_debug($conexion,$show_db);
							$sql_error .= "ok";
							if ($conexion->mQueryConExito=='N') {
								$error ++;
								$sql_error .= $sql_query;
							} else {
								$id .= mysql_insert_id().'-';	
							}
						}
					}
			
			}
			if ($error>0) {
				$sql ="ROLLBACK";
				$conexion->ejecuta_query($sql);
			} else {
				$sql="SET AUTOCOMMIT=1;";
				$conexion->ejecuta_query($sql);
			}
			$conexion ->disconnectDB();
		}
	} 
	catch (Exception $e) {
		$error=999;
		$sql_error .= "EXCEPTION: ".$e->getMessage();
	}
	return $error."||".$sql_error."||".$id;
}

function ejecutaStoredProc($sQuery) {
	$result = array();
	$mysqli = mysqli_init();
	$mysqli->real_connect($GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pass'], $GLOBALS['db_name']);
	$mysqli->set_charset("utf8");
    //printf("Error loading character set utf8: %s\n", $mysqli->error);

	if (mysqli_connect_errno())
	{
		return $result;
		//echo "1||101||".mysqli_connect_error();
		exit();
	}
	if($mysqli->real_query ($sQuery)){
			//echo "EXITOOOO";
			//sult = $mysqli->store_result();
			if($objResult = $mysqli->store_result())
				{
					while($row = $objResult->fetch_assoc())
					{
						$result[] = $row;
						//print $row["strProductName"]." ".$row["strProductName"]."<br>\r\n";
					}
					$objResult->free_result();
				}
				
	} else {
		echo $mysqli->error;
	}
	$mysqli->close();
	
	return $result;
}

//EJECUCION DE CONSULTAS TIPO "SELECT"
function ejecutaSELECT($pQuery,$show_db,$debug_pagina,$isajaxslistadoadmin=0,$sFiltroWhere="",$array_fields_listado=array()){	
	$result = array(); //query OK , pero VACIA	
	$conexion  = new CBBDD($GLOBALS['db_type'], $GLOBALS['db_host'], $GLOBALS['db_user'], $GLOBALS['db_pass'], $GLOBALS['db_name'],$GLOBALS['debug_pagina']);
	$conexion ->connectDB();
	$cur_conn_id = $conexion->get_id_conexion();//obtenemos id de conexion para el mysql_real_escape_string 
		
	//CONSTRUCCION DE LA QUERY (CASO ESPECIAL, EL ADMIN CUANDO CARGA UN LISTADO DESDE AJAXS,necesito el $cur_conn_id para aplicarlo a mysql_real_escape_string: 
	//	$arrayfiltros solo se usa cuando es un listado ajaxs del admin
	//	$sFilterWhere solo se usa cuando es un listado ajaxs del admin y se aplican mas filtros ademas de los del buscador
	if($isajaxslistadoadmin ==1 && is_array($array_fields_listado) && count($array_fields_listado) > 0){
		$setting="array_fields";
		include($GLOBALS['path_raiz_includes_admin']."includes/listado_dinamic_settings.php"); //de aqui salen, array_filters, array_columns, array_filters_global,array_ordenes					
		$sFilterWhere  = $sFiltroWhere; //FILTROS GENERALES (SIN TENER QUE VER CON EL BUSCADOR) 
		//obtenemos la query de final con la que presentar el listado teniendo en cuenta TODOS los parametros de filtro, ordenacion, etc...
		$setting="json_listado_params";		
		include($GLOBALS['path_raiz_includes_admin']."includes/listado_dinamic_settings.php");
		$pQuery .= $sWhere." ".$sOrder." ".$sLimit;
	}	
	$conexion ->getResultSelectArray($pQuery);
	$conexion ->disconnectDB();
	$n_rows = $conexion ->getNumRows();	
	if($n_rows > 0) $result = $conexion->tResultadoQuery; //query OK	
	elseif($conexion->mQueryConExito=="N"){
		if(QUERY_SEND_EMAIL_TRACKING==1){	
			//new dBug($conexion);		
			//echo "bbb::".$conexion->mError."<br>"; 
			$body ="\r\nFECHA:".date("d/m/Y H:i:s")."\r\n";
			$body.="\r\n=============================== NAVEGADOR =================================================\r\n";
			$body.="\r\n".trim($_SERVER['HTTP_USER_AGENT'])."\r\n";
			$body.="\r\n=============================== QUERY =====================================================\r\n";		
			$body.="\r\n".trim($pQuery)."\r\n";
			$body.="\r\n====================== FICHERO EN EL QUE SE ENCUENTRA DICHA QUERY =========================\r\n";
			$body.="\r\n".trim($_SERVER['SCRIPT_FILENAME'])."\r\n";
			$body.="\r\n====================== ERROR PRODUCIDO ====================================================\r\n";		
			$body.="\r\n".$conexion->mError."\r\n";
			$body.="\r\n================================================================================\r\n";				
			envia_email(trim($GLOBALS['webtitle']),"ERROR EN EL WEB: (".trim($GLOBALS['webtitle']).")",$body,$GLOBALS['email_from_smtp'],QUERY_EMAIL_TRACKING_TO);
		} 
		$result = false; //query ERROR
	}	
	show_debug($conexion,$show_db);
	//return array(0=>$cur_conn_id,1=>$result); //devuelve array con la conexion (para usarlo en el mysql_real_escape_string) y el recordset 
	return ($n_rows > 0) ? $conexion->tResultadoQuery : array();
}
//: en caso de error, envio de email para ver que posible error producido{
function envia_email($project,$subject,$body,$emailFrom,$to){			
	//$emailFrom ="manuel.fonseca@eone.es";
	//$destino="manuel.fonseca@arnold4d.es"; 
	//$copia="luisfernando.parra@arnold4d.es"; 
	//$subject ="Aerocity:LOG XML Envio / Respuesta Reserva SINFE";
	//$headers = 'From: '.trim($project).' <'.$emailFrom.'>' . "\r\n";
	//$headers .= 'Reply-To: '.trim($project).' <'.$emailFrom.'>' . '\r\n';
	$headers .= 'X-Mailer: PHP/' . phpversion(). '\r\n';
	$headers .= 'Content-Type: text/html;charset=UTF-8' . '\r\n';
	//ini_set("sendmail_from",$emailFrom);
	//ini_set("SMTP",trim($GLOBALS['email_smtp_server']));
	//mail("$to",$subject,$body,$headers);	
	//mail("$destino,$copia",$subject,$b,$headers);	 		
}
//FOR DEBUG
function show_debug($obj,$show_db=0){
	if($obj && $show_db ==1){
		if($obj->mQueryConExito=="S"){//consulta ok
			$obj->objdebug->getQueryExito();
			new dBug($obj->tResultadoQuery); //recordset de la query
		}
		else{
			$obj->objdebug->getQueryError();//consulta erronea
		}
		return;
	}
	return false;
} 
//CONTROL DEL CODIGO SEGUN VERSION DE PHP
function ejecutasegunversion($tipo,$q){
	$r='';
	//new dBug($q);
	//1) FILTRAMOS POSIBLES CASOS ESPECIALES EN LA VERSION DE PHP
	$phpv = phpversion(); 
	$pos = strpos($phpv, '-');
	if ($pos !== false) {
		$phpv = substr($phpv,0,$pos); 
	}	
	//echo "PHPVERSION::::".$phpv."<br>";
	//echo "PHP_VERSION_ID::::".PHP_VERSION_ID."<br>";
	//2) FILTRAMOS POR EL TIPO DE CAMBIO Y LA VERSION
	switch($tipo){
		case "JSON": if(strnatcmp($phpv,'5.2.0') >= 0){//&& PHP_VERSION_ID >=50200
						//echo "entroif<br>";
						$r = json_encode($q);
					 }else{
					 	//echo "entroelse<br>";
						$json = new json;
            			$r = $json->encode($q); 
					 }; 
		break;
	}
	return $r;
}
//TRATAMIENTO DEL JSON
function escape($string) {
	$find = array('\\',        '"',    '/',    "\b",    "\f",    "\n",    "\r",    "\t",    "\u");
    $repl = array('\\\\',    '\"',    '\/',    '\b',    '\f',    '\n',    '\r',    '\t',    '\u');
    $string = str_replace($find, $repl, $string);
    return $string;
}

//FUNCION QUE DEVUELVE DESDE CUANDO SE HIZO ALGO
/*
$d = date('2011-01-20');//5 semanas y 6 dias (ok)
$e = date('2011-01-26');//5 semanas (ok)
$f = date('2011-02-14');//2 semanas y 2 dias (ok)
$g = date('2011-02-20');//1 semanas y 3 dias (ok)
$h = date('2011-02-27');//hace 3 dias y 16 horas (ok)
$i = date('2011-03-02');//hace 16 horas y 12 minutos (ok)
$j = date('2011-03-02 10:43:44');//hace 5 horas y 38 minutos (ok)
$k = date('2011-03-02 16:10:44');//hace 12 minutos (ok)
$l = date('2011-03-02 16:23:44');//hace manos de 1 minuto (ok)
$m = date('2011-03-02 16:24:30');//ahora mismo (ok)

echo "Ejemplo1: d:::".$d."||||".between3("es",$d)."<br>"; 
echo "Ejemplo2: e:::".$e."||||".between3("es",$e)."<br>"; 
echo "Ejemplo3: f:::".$f."||||".between3("es",$f)."<br>"; 
echo "Ejemplo4: g:::".$g."||||".between3("es",$g)."<br>"; 
echo "Ejemplo5: h:::".$h."||||".between3("es",$h)."<br>"; 
echo "Ejemplo6: i:::".$i."||||".between3("es",$i)."<br>"; 
echo "Ejemplo7: j:::".$j."||||".between3("es",$j)."<br>"; 
echo "Ejemplo8: k:::".$k."||||".between3("es",$k)."<br>"; 
echo "Ejemplo9: k:::".$l."||||".between3("es",$l)."<br>"; 
echo "Ejemplo10: k:::".$m."||||".between3("es",$m)."<br>"; 

$d = date('2011-01-20');//5 semanas y 6 dias (ok)
$e = date('2011-01-26');//5 semanas (ok)
$f = date('2011-02-14');//2 semanas y 2 dias (ok)
$g = date('2011-02-20');//1 semanas y 3 dias (ok)
$h = date('2011-02-27');//hace 3 dias y 16 horas (ok)
$i = date('2011-03-02');//hace 16 horas y 12 minutos (ok)
$j = date('2011-03-02 10:43:44');//hace 5 horas y 38 minutos (ok)
$k = date('2011-03-02 16:10:44');//hace 12 minutos (ok)
$l = date('2011-03-02 16:23:44');//hace manos de 1 minuto (ok)
$m = date('2011-03-02 16:24:30');//ahora mismo (ok)

echo "Ejemplo1: d:::".$d."||||".between3("en",$d)."<br>"; 
echo "Ejemplo2: e:::".$e."||||".between3("en",$e)."<br>"; 
echo "Ejemplo3: f:::".$f."||||".between3("en",$f)."<br>"; 
echo "Ejemplo4: g:::".$g."||||".between3("en",$g)."<br>"; 
echo "Ejemplo5: h:::".$h."||||".between3("en",$h)."<br>"; 
echo "Ejemplo6: i:::".$i."||||".between3("en",$i)."<br>"; 
echo "Ejemplo7: j:::".$j."||||".between3("en",$j)."<br>"; 
echo "Ejemplo8: k:::".$k."||||".between3("en",$k)."<br>"; 
echo "Ejemplo9: k:::".$l."||||".between3("en",$l)."<br>"; 
echo "Ejemplo10: k:::".$m."||||".between3("en",$m)."<br>"; 
*/
function between($lan="es",$past = '', $now = '')
{	
    $past = is_string($past)? strtotime($past): (int) $past;
    $now = is_string($now)? strtotime($now): (int) $now;
    $now = $now <= 0? time(): $now;// --
   // restamos..
    $diff = $now - $past;
	//echo "||||DIFF::".$diff."||||";
	$fecha="";
	if($diff >= (7 * 24 *60 *60)){ //semanas - dias
		$semanas= (int)($diff / 604800);
		if($semanas > 1){ if(trim($lan)=="es"){ $fecha.="hace ".$semanas. " semanas";}else{$fecha.=$semanas. " weeks";}}	
		else{ if(trim($lan)=="es"){$fecha.="hace 1 semana";}else{$fecha.="1 week";}}
		if(($diff%604800) > 0){
			$dias=(int)(($diff%604800)/(24 * 60 * 60));			
			if($dias > 1){ if(trim($lan) == "es"){ $fecha.= " y ".$dias." días";}else{$fecha.=" ".$dias. " days";}}
			elseif($dias >=1){ if(trim($lan) == "es"){$fecha.= " y 1 día";}else{$fecha.=" 1 day";}}
		}
		if(trim($lan) == "en"){$fecha.=" ago";}		
	}
	elseif($diff >= (24 *60 *60)){//dias - horas
		$dias= (int)($diff / 86400);
		if($dias > 1){ if(trim($lan) == "es"){$fecha.="hace ".$dias. " días";}else{$fecha.=$dias. " days";}}
		else{ if(trim($lan) == "es"){$fecha.="hace 1 día ";}else{$fecha.="1 day";}}
		if(($diff%86400) > 0){
			$horas=(int)(($diff%86400)/(60 * 60));			
			if($horas > 1){ if(trim($lan) == "es"){$fecha.= " y ".$horas." horas";}else{$fecha.=" ".$horas." hours";}}	
			elseif($horas >=1){ if(trim($lan) == "es"){$fecha.= " y 1 hora";}else{$fecha.= " 1 hour";}}
		}
		if(trim($lan) == "en"){$fecha.=" ago";}
	}
	elseif($diff >= (60 *60)){//horas - minutos
		$horas= (int)($diff / 3600);
		if($horas > 1){ if(trim($lan) == "es"){$fecha.="hace ".$horas. " horas";}else{$fecha.=$horas." hours";}}		
		else{if(trim($lan) == "es"){$fecha.="hace 1 hora";}else{$fecha.="1 hour";}}	
		if(($diff%3600) > 0){
			$minutos=(int)(($diff%3600)/60);			
			if($minutos > 1){ if(trim($lan) == "es"){	$fecha.= " y ".$minutos." minutos";}else{$fecha.= " ".$minutos." minutes";}}	
			elseif($minutos >=1){if(trim($lan) == "es"){	$fecha.= " y 1 minuto";}else{$fecha.=" 1 minute";}}	
		}
		if(trim($lan) == "en"){$fecha.=" ago";}
	}
	elseif($diff >= 60 && $diff < (60 *60)){//minutos
		$minutos= (int)($diff / 60);
		if($minutos > 1){ if(trim($lan) == "es"){ $fecha.="hace ".$minutos. " minutos";}else{$fecha.=$minutos." minutes";}}	
		else{ if(trim($lan) == "es"){ $fecha.="hace 1 minuto";}else{ $fecha.="1 minute";}}
		if(trim($lan) == "en"){$fecha.=" ago";}	
	}
	elseif($diff >= 30 && $diff < 60){//minutos
		if(trim($lan) == "es"){ $fecha.="hace menos de 1 minuto";}
		else{$fecha.="hace menos de 1 minuto";}	
	}
	elseif($diff >= 0 && $diff < 30){
		if(trim($lan) == "es"){ $fecha.="ahora mismo";}	
		else{$fecha.="ahora mismo";}	
	}
	return $fecha;	
}

function formatear_fecha($lan="es",$fecha){
	//la fecha viene en formato aaaa-mm-dd
	ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha); 
    $lafecha=$mifecha[3]."/".$mifecha[2]."/".$mifecha[1]; 
	//echo "LA FECHA::".$lafecha."<br>";
	$fecha_formateada="";
	if(trim($lan) == "es"){
		$meses = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
		$current_mes = $meses[date("n", mktime(0, 0, 0, $mifecha[2], $mifecha[3], $mifecha[1]))-1];
		$cur_dia=(int)$mifecha[3];		
		//$fecha_formateada.= $cur_dia." de ".$current_mes." de ".$mifecha[1];
		$fecha_formateada.= $cur_dia." ".$current_mes." ".$mifecha[1];
	}
	else{
		$meses = array('January','February','March','April','May','June','July','August','September','October','November','December');
		$current_mes = $meses[date("n", mktime(0, 0, 0, $mifecha[2], $mifecha[3], $mifecha[1]))-1];
		$cur_dia=(int)$mifecha[3];
		$fecha_formateada.=$current_mes.",".$cur_dia." ".$mifecha[1];
	}
	return $fecha_formateada;	
}

function dia_semana($lan="es",$fecha) {
	//la fecha viene en formato aaaa-mm-dd
	ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha); 
    $lafecha=$mifecha[3]."/".$mifecha[2]."/".$mifecha[1]; 	
	if(trim($lan) == "es"){
    	$dias = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
	}
	else{
		$dias = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
	}	
    return $dias[date("w", mktime(0, 0, 0, $mifecha[2], $mifecha[3], $mifecha[1]))];
}

//PARSEO DE YOU TUBE / VIMEO
function youtube_vimeo_parser($url_videos)
{
	$url_parseada = "".$url_videos;
	$vidparser = parse_url($url_videos);
	parse_str($vidparser[query], $query);
	parse_str($vidparser[host], $servidor);
	parse_str($vidparser[scheme], $protocolo);
	parse_str($vidparser[path], $ruta);

	if(strpos($url_videos,"youtube") !==false)
	{
		$url_parseada = $vidparser[scheme]."://".$vidparser[host].$vidparser[path]."?v=".($query["v"]);
	}
	elseif(strpos($url_videos,"vimeo") !==false)
	{
		$url_parseada = str_replace("/","",$vidparser[path]);
	}
	
	return $url_parseada;
}

//VERSION DE NAVEGADOR
function getBrowser() 
{ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');	
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';

    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

	 
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
		    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
} 

/*FUNCION QUE DEVUELVE EL PESO DEL FICHERO FORMATEADO*/
function get_filesize ($dsize) { 
	if (strlen($dsize) <= 9 && strlen($dsize) >= 7) { 
		$dsize = number_format($dsize / 1048576,1); 
		return "$dsize MB"; 
	} elseif (strlen($dsize) >= 10) { 
		$dsize = number_format($dsize / 1073741824,1); 
		return "$dsize GB"; 
	} else { 
		$dsize = number_format($dsize / 1024,1); 
		return "$dsize KB"; 
	} 
}
/*PINTAR HTML o JS TAGS*/ 
function show_cssjs_file($filepath,$type,$showtime=1){		

/*<link rel="stylesheet" href="<?php echo $path_raiz_admin;?>resources/admin/css/reset.css?"<?php echo filemtime($path_raiz_admin."resources/admin/css/reset.css");?> type="text/css" media="screen" />*/
/*<script type="text/javascript" src="<?php echo $path_raiz_aplicacion;?>js/lib/jquery-1.3.2<?php echo $js_minified_suffix;?>.js?<?php echo filemtime($filename);?>"></script>*/
	$r_tag ="";
	if(trim($filepath)=="") return $r_tag;	
	$cur_url = ($showtime==1) ? trim($filepath)."?".filemtime(trim($_SERVER['DOCUMENT_ROOT'].$filepath))  : trim($filepath); 		
	if(trim(strtoupper($type)=="CSS")){			
		$r_tag .="<link rel=\"stylesheet\" href=\"".trim($cur_url)."\" type=\"text/css\" media=\"screen\" />";
	}
	elseif(trim(strtoupper($type)=="JS")){
		$r_tag .="<script charset=\"utf-8\" type=\"text/javascript\" language=\"javascript\" src=\"".trim($cur_url)."\"></script>";
	}
	return $r_tag;
}
function obtener_dominio($url){	
	preg_match('@^(?:http://)?([^/]+)@i', $url, $matches);											
	return $matches[1];
}
function ObtenerNavegador($user_agent) {
     $navegadores = array(
          'Opera' => 'Opera',
		  'Chrome' => 'Chrome',
		  'Safari' => 'Safari',
          'Mozilla Firefox'=> '(Firebird)|(Firefox)',
          'Galeon' => 'Galeon',
          'Mozilla'=>'Gecko',
          'MyIE'=>'MyIE',
          'Lynx' => 'Lynx',
          'Netscape' => '(Mozilla/4\.75)|(Netscape6)|(Mozilla/4\.08)|(Mozilla/4\.5)|(Mozilla/4\.6)|(Mozilla/4\.79)',
          'Konqueror'=>'Konqueror',
		  'IE8' => '(MSIE 8\.[0-9]+)',
          'IE7' => '(MSIE 7\.[0-9]+)',
          'IE6' => '(MSIE 6\.[0-9]+)',
          'IE5' => '(MSIE 5\.[0-9]+)',
          'IE4' => '(MSIE 4\.[0-9]+)'
	);
	foreach($navegadores as $navegador=>$pattern){
       if (eregi($pattern, $user_agent))
       return $navegador;
    }
	return 'Desconocido';
} 
#########################################################################################################################
# FUNCIONES CON FECHAS
#########################################################################################################################
//fichero con las funciones auxiliares necesarias
function DDMMAAAA_to_AAAAMMDD($fecha) //tratamiento de fechas 
{
		$dia=substr($fecha,0,2);
		$mes=substr($fecha,3,2);
		$anyo=substr($fecha,6,4);
	
		$AAAAMMDD=$anyo."-".$mes."-".$dia;
		return $AAAAMMDD;
}

//tratamiento de fecha
function AAAAMMDD_to_DDMMAAAA($fecha)//tratamiento de fechas
{
	$dia=substr($fecha,8,2);
	$mes=substr($fecha,5,2);
	$anyo=substr($fecha,0,4);
	
	if($dia=="" && $mes=="" && $anyo==""){
		$DDMMAAAA=$dia.$mes.$anyo;
	}
	else{
		$DDMMAAAA=$dia."/".$mes."/".$anyo;
	}
	
	return $DDMMAAAA;
}

//tratamiento de fewcha y hora
function AAAAMMDD_HHMMSS_to_DDMMAAAA_HHMMSS($fecha,$hora)//tratamiento de fechas y hora
{
	$dia=substr($fecha,8,2);
	$mes=substr($fecha,5,2);
	$anyo=substr($fecha,0,4);
	
	if ($hora == "1")
	{
		$horas=substr($fecha,11,2);	
		$minutos=substr($fecha,14,2);
		$segundos=substr($fecha,18,2);	
		$DDMMAAAA_HHMMSS=$dia."/".$mes."/".$anyo." ".$horas.":".$minutos.":".$segundos;	
	}
	if ($hora == "0")
	{$DDMMAAAA_HHMMSS=$dia."/".$mes."/".$anyo;}
	return $DDMMAAAA_HHMMSS;
}

function convertir_fecha_hora_mysql_normal($fecha,$hora,$formato,$divisor,$separador_res) 
//convierte una fecha en formtato dd/mm/yyyy hh:mm:ss en yyyy-mm-dd hh:mm:ss
{
	if ($hora == "1")
	{
		$dt_elements = explode(' ',$fecha);
		$date_elements  = explode($divisor,$dt_elements[0]);
		$time_elements =  explode(':',$dt_elements[1]);
		
		if($formato == "0"){$fecha_hora_res = $date_elements[2].$separador_res.$date_elements[1].$separador_res.$date_elements[0]." ".$time_elements[0].":".$time_elements[1];}
		if($formato == "1"){$fecha_hora_res = $date_elements[0].$separador_res.$date_elements[1].$separador_res.$date_elements[2]." ".$time_elements[0].":".$time_elements[1];}
	}
	if ($hora == "0")
	{
		$date_elements  = explode($divisor,$fecha);
		if($formato == "0"){$fecha_hora_res = $date_elements[2].$separador_res.$date_elements[1].$separador_res.$date_elements[0];}
		if($formato == "1"){$fecha_hora_res = $date_elements[0].$separador_res.$date_elements[1].$separador_res.$date_elements[2];}
	}
	return $fecha_hora_res;
}
/*
function convertir_fecha_hora_normal_mysql_normal($fecha,$hora) 
//convierte una fecha en formtato dd/mm/yyyy hh:mm:ss en yyyy-mm-dd hh:mm:ss
{
	if ($hora == "1")
	{
		$dt_elements = explode(' ',$fecha);
		$date_elements  = explode('/',$dt_elements[0]);
		$time_elements =  explode(':',$dt_elements[1]);
		$fecha_hora_res = $date_elements[2]."-".$date_elements[1]."-".$date_elements[0]." ".$time_elements[0].":".$time_elements[1].":".$time_elements[2];		
	}
	if ($hora == "0")
	{
		$date_elements  = explode('/',$fecha);
		$fecha_hora_res = $date_elements[2]."-".$date_elements[1]."-".$date_elements[0];
	}
	return $fecha_hora_res;
}
*/
//////////////////////////////////////////////////// 
//Convierte fecha de mysql a normal 
//////////////////////////////////////////////////// 
function cambiaf_a_normal($fecha){ 
    ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha); 
    $lafecha=$mifecha[3]."/".$mifecha[2]."/".$mifecha[1]; 
    return $lafecha;
} 

//////////////////////////////////////////////////// 
//Convierte fecha de normal a mysql 
//////////////////////////////////////////////////// 

function cambiaf_a_mysql($fecha){ 
    ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $fecha, $mifecha); 
    $lafecha=$mifecha[3]."-".$mifecha[2]."-".$mifecha[1]; 
    return $lafecha; 
} 

function obtener_anio_normal($fecha,$fraccion){ 
    ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $fecha, $mifecha); 
    $eldato = "";
	if($fraccion != "")
	{$eldato=$mifecha[$fraccion];}	
    return $eldato; 
} 

//////////////////////////////////////////////////// 
//Obtiene fecha actual en dos formatos
//Si el formato pasado es sp nos dara la fecha en formato dd/mm/yyyy
//Si el formato pasado es en nos dara la fecha en formato yyyy/mm/dd
//////////////////////////////////////////////////// 
function obtener_fecha_actual($formato)
{
	$date_time_array = getdate();
	$month = $date_time_array['mon'];
	$day = $date_time_array['mday'];
	$year = $date_time_array['year'];	
	$timestamp = mktime(0,0,0,$month,$day,$year);
	if ($formato == 'sp'){return strftime('%d/%m/%Y',$timestamp);}
	if ($formato == 'en'){return strftime('%Y/%m/%d',$timestamp);}
}	

//////////////////////////////////////////////////// 
//Obtiene fecha actual en dos formatos
//Si el formato pasado es sp nos dara la fecha en formato dd/mm/yyyy
//Si el formato pasado es en nos dara la fecha en formato yyyy/mm/dd
//////////////////////////////////////////////////// 
function obtener_fecha_hora_actual($formato,$hora,$tipo)
{
	$date_time_array = getdate();
	$hours = $date_time_array['hours'];
	$minutes = $date_time_array['minutes'];
	$seconds = $date_time_array['seconds'];
	$month = $date_time_array['mon'];
	$day = $date_time_array['mday'];
	$year = $date_time_array['year'];
	if ($hora == "0")
	{
		$timestamp = mktime(0,0,0,$month,$day,$year);
		if ($formato == 'sp'){return strftime('%d'.$tipo.'%m'.$tipo.'%Y',$timestamp);}
		if ($formato == 'en'){return strftime('%Y'.$tipo.'%m'.$tipo.'%d',$timestamp);}
	}
	if ($hora == "1")
	{
		$timestamp = mktime($hours,$minutes,$seconds,$month,$day,$year);
		if ($formato == 'sp'){return strftime('%d'.$tipo.'%m'.$tipo.'%Y %H:%M:%S',$timestamp);}
		if ($formato == 'en'){return strftime('%Y'.$tipo.'%m'.$tipo.'%d %H:%M:%S',$timestamp);}
	}	
}	
//////////////////////////////////////////////////
//>Funcion que calcula si un año es bisiesto o no
//////////////////////////////////////////////////

function esBisesto ($anyo) {
    if ((($anyo % 4)==0) && (($anyo % 100)!=0) || (($anyo % 400)==0)) {
        return (true);
    }
    else {
        return (false);
    }
}

//////////////////////////////////////////////////// 
//Suma un numero al tipo de dato aportado
//Interval: parte a la que se le sumara ese numero
//number: numero que sumaremos
//date: fecha a la que le añadiremos ese numero, esta fecha tiene que venir en formato americano y UNIX
//yyyy anio 
//q cuarto 
//m mes 
//y dia del anio 
//d dia 
//w dia de la semana
//ww semana del anio
//h hora
//n minuto
//s segundo
//////////////////////////////////////////////////// 

function DateAdd($interval, $number, $date) {
    list( $day, $month, $year ) = split( '[/.-]', $date);
	$date_time_array = getdate($date);
    $month = $date_time_array['mon'];
    $day = $date_time_array['mday'];
    $year = $date_time_array['year'];
    switch ($interval) {
        case 'yyyy':
            $year+=$number;
            break;        
        case 'm':
            $month+=$number;
            break;        
        case 'd':
			$day+=$number;
            break;         
    }
    $timestamp = mktime(0,0,0,$month,$day,$year);
    return $timestamp;
}
function formato_correcto($fecha) {
	
	$arr_fech = split("/",$fecha);
	if (count($arr_fech)!=3) {return false;}
	else {
		if (strlen($arr_fech[0])!=2 || strlen($arr_fech[1])!=2 || strlen($arr_fech[2])!=4) {
			return false;
		} else {return true;}
	}
}

//////////////////////////////////////////////////// 
//Resta un numero al tipo de dato aportado
//Interval: parte a la que se le restara ese numero
//number: numero que restaremos
//date: fecha a la que le restaremos ese numero, esta fecha tiene que venir en formato americano y UNIX
//yyyy anio 
//q cuarto 
//m mes 
//y dia del anio 
//d dia 
//w dia de la semana
//ww semana del anio
//h hora
//n minuto
//s segundo
//////////////////////////////////////////////////// 
/*
function DateResta($interval, $number, $date) {

    list( $day, $month, $year ) = split( '[/.-]', $date);
	$date_time_array = getdate($date);
    $month = $date_time_array['mon'];
    $day = $date_time_array['mday'];
    $year = $date_time_array['year'];
	
    switch ($interval) {
    
        case 'yyyy':
            $year-=$number;
            break;        
        case 'm':
            $month-=$number;
            break;        
        case 'd':
			$day-=$number;
            break;         
    }
	
    $timestamp = mktime(0,0,0,$month,$day,$year);
    return $timestamp;
}
*/

function DateResta($number,$date) {

    list( $day, $month, $year ) = split( '[/.-]', $date);
    $date_time_array = getdate($date);
    echo "MES".$month = $date_time_array['mon']."<br>";
    echo "DIA".$day = $date_time_array['mday']."<br>";
    echo "ANYO".$year = $date_time_array['year']."<br>";
    
    if($day==1){
    	if($month==1){ //CASO ENERO    	
    	        $day=31;
    		$month=12;
    		$year=$year - 1;
    	}    
    	elseif($month==2 ||$month==4 || $month==6 ||$month==8 || $month==9 || $month==11){ //CASO MESES CON 31 DIAS
    	        $day=31;
    		$month=$month - 1;
    	}
    	elseif($month==5 || $month==7 ||$month==10 || $month==12){ //CASO MESES CON 30 DIAS
    	        $day=30;
    		$month=$month - 1;
    	}
    	elseif($month==3){ //CASO FEBRERO
    		if(esBisesto($year)){
    			$day=29;	
    		}
    		else{
    			$day=28;
    		}
    	   $month=$month - 1;    	            		
    	}
    }
    else
    {
    		$day=$day - 1;    
    }
	
    echo "++++++++++++CAMBIO+++++++++++++++++++<br>";	
    echo "MES".$month."<br>";
    echo "DIA".$day."<br>";
    echo "ANYO".$year."<br>";
    		
    $timestamp = mktime(0,0,0,$month,$day,$year);
    return $timestamp;
}

function checkSqlInjection($cadena) {
	return  str_ireplace($GLOBALS['arr_sql_inject_original'],$GLOBALS['arr_sql_inject_cambio'],$cadena);	
}

/**
 * Función que normaliza una serie de datos de entrada entre ellos SQL Injecttion
 * @param string $type_arr valores separados por comma . SQLINJECTINO, STRINGS, BIRTHDATE,EMAIL,NUMBER, BBDD (deprecated)
 * @param string $string string a normalizar
 * @return string cadena normalizada
 */
function normalizeInputData($type_arr, $string) {
        if ($string == '')
            return $string;

        $tmp = explode(',', $type_arr);
        $type = $tmp[0];

        switch ($type) {
            case 'SQLINJECTION':
                $string = str_ireplace($GLOBALS['arr_sql_inject_original'], $GLOBALS['arr_sql_inject_cambio'], strtolower($string));
//echo "uno";
                break;
            case 'STRINGS':
                /**
                 * Elimina caracters no utf-8. Prueba a realizarlo como esto
                 */
               /* No funciona en servidor real
                * setlocale(LC_CTYPE, 'es_ES.utf8');
                $string = iconv('UTF-8', 'us-ascii//TRANSLIT', $string);
                * 
                */
                $string = mb_convert_encoding($string , 'UTF-8', 'UTF-8');
                break;
            case 'BBDD':
                //$string = addcslashes(mysqli_real_escape_string($this->cur_conn_id, $string), '%_');
                
                break;
            case 'NUMBER':
                $string = (!is_numeric($string)) ? '' : $string;
                break;
            case 'BIRTHDATE';
                $birth = explode('-', $string);
                if (count($birth) == 3 && strlen($birth[0]) == 4 && strlen($birth[1]) == 2 && strlen($birth[2]) == 2) {
                    $string = implode('-', array_reverse($birth));
                }
                break;
            case 'EMAIL';
                if (!filter_var($string, FILTER_VALIDATE_EMAIL))
                    $string = '';
                break;
            case 'INPUT':
                //$string = strtolower($string);
                $string = htmlspecialchars_decode($string,ENT_QUOTES);
                $string = ucwords(str_ireplace($GLOBALS['arr_sql_inject_original'], $GLOBALS['arr_sql_inject_cambio'], strtolower($string)));
                $string = strip_tags($string);
                $string = htmlspecialchars($string);
                
                break;

            default:
                break;
        }
        if (count($tmp) > 1) {
            unset($tmp[0]);
            $type_arr = implode(',', $tmp);
            return normalizeInputData($type_arr, $string);
        }
        return $string;
    }

function getRealIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		  $ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}	
function toSioNo($val) {
	if ($val == "1") {return "Si";} else {return "No";}
}	
?>