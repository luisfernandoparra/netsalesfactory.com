<?php
/*
 * NOTA ACERCA DE INBOUND:
 * EL ARRAY CON LOS TELEFONOS INBOUND SE ENCUENTRAN EN EL
 * `index.php` AS SER NECESARIO RECIBIR `$id_source`
 * QUE SE PROCESA EN `includes/initiate.php` Y SE
 * CARGA DESPUES DE ESTE SCRIPT DE CONFIGURACION
 * 
 */
//***************************************
// START CONFIG DINAMICA (M.F. 12.01.2015)
$sitesPruebas=($_SERVER['SERVER_ADDR'] == '139.162.246.12' || $_SERVER['SERVER_ADDR'] == '127.0.0.1' || substr($_SERVER['SERVER_ADDR'], 0, 9) == '192.168.2'); // SITIOS PARA PRUEBAS
$paintErrors=false;
$minTimeCookiesStart=30;  // TIEMPO (EN SEGUNDOS) PARA ACTIVAR AUTOMATICAMENTE LAS COOKIES
$subFolderLanding='acnur';

// START GLOBAL CHECK PHONE VARS (16.06.2014)
$landingMainName='ACNUR';
// END GLOBAL CHECK PHONE VARS

if($sitesPruebas){
	$subFolderLanding='acnur';
  $url_local_condiciones = $fromServer.'/netsalesfactory.com/promociones/';	//ORIGINAL PARA MARAVILLAO / netsalesfactory.com
	$url_local = $fromServer.'/'.$rootMaravillaoFolder.@$developersLocalFolder[$developerIp].'promociones';
	$dir_raiz_aplicacion_local = $rootMaravillaoFolder.@$developersLocalFolder[$developerIp].'promociones/' . $subFolderLanding.'/';
//	$path_raiz_aplicacion_local = $fromServer.'/'.$dir_raiz_aplicacion_local . '';
	$path_raiz_aplicacion_local = '/'.$dir_raiz_aplicacion_local . '';
	$path_raiz_includes_local = $path_raiz_includes . 'acnur/';
	$url_condiciones = $url_local .'/'.$subFolderLanding .'/condiciones.php';
	$paintErrors=true;
	$minTimeCookiesStart=3000;
         $url_thankyouPage = 'thankyoupage.php';
} else {
	$url_local = 'http://www.';
	$url_local_condiciones = 'http://www.';
	$dir_raiz_aplicacion_local = $dir_raiz_aplicacion . $subFolderLanding.'/';
	$path_raiz_aplicacion_local = '/' . $dir_raiz_aplicacion_local;
	$path_raiz_includes_local = $path_raiz_includes . $subFolderLanding.'/';
	$url_condiciones = $url_local .'/'.$subFolderLanding . '/condiciones.html';
         $url_thankyouPage = '../gracias-por-confiar-acnur';
}
// END CONFIG DINAMICA (M.F. 19.11.2013)

//Variable para la tabla REGISTROS, para diferenciar qué cliente es
$id_client = (!empty($_REQUEST['idClient'])&& is_numeric($_REQUEST['idClient'])) ? (int)$_REQUEST['idClient'] : 52;


$arr_client_campaign=array(
	52 => array(
		'prefFichExtraccion' => 'AdeslasSalud',
		'refInconcert' => array('100000032', '100000032')
	)
);

$prefFichExtraccion = $arr_client_campaign[$id_client]['prefFichExtraccion'];
$arr_campanas = $arr_client_campaign[$id_client]['refInconcert'];

//$file_ws = 'ctcAlisys.php';
$file_ws = 'router.php';
//echo'<pre style=text-align:left;>';print_r($arr_client_campaign);
/* $campana_lead = "PruebasClickToCall";
  $campana_ctc = "PruebasClickToCall";
 */

//Array con todas las creas existentes. 
/*
 * Notas 2014:
 * 1. El parámetro `manageDevice` condiciona la detección del dispositivo que carga la promo
 * 2. `new_template` gestiona el tipo de modelo a seguir con la composición de la promo,
 * - 0: modo antiguo con archivos distintos para contenidos según PC o MOBILE
 * - 1: modo nuevo un archivo único para todos los dispositivos
 */

$arrSemGoogle=array();	//PARAMETROS PARA GOOGLE SEM

$arr_creas = array();


//Parámetros del pixel de TradeDoubler
$organization = '1963177';
$event = '325638';
$trbdl_program='228617';
$secret_pwd_conf='p3Rd€r_N@da';
$secret_usr=date('Y-m-d H:i:s');
$secret_pwd = md5($secret_usr.$secret_pwd_conf);

$nombCliente = 'ACNUR';

//Pixel expecíficos
$isSpecificPixel = 1;

//Array de display
$arr_isSpecificPixel = array(
// START PIXEL DE PRUEBAS (M.F. 19.11.2013)
	'9999999' => '<script language="JavaScript1.1" src="http://wrap.tradedoubler.com/wrap?id=8498"></script>'
// END PIXEL DE PRUEBAS (M.F. 19.11.2013)
	);

// START FIXED PIXELS (ADDED 30.01.2014)
$mi_referer=strtolower(@$_SERVER['HTTP_REFERER']);
$arr_email_domain = array();
$pos = false;

if(!empty($mi_referer)){
  foreach($arr_email_domain as $mi_valor)
	{
		$pos = strpos($mi_referer,$mi_valor);
		if ($pos === true)
			break;
	}
}

if($pos === true || empty($_SERVER['HTTP_REFERER'])){
  $conditionalPixelStatic='https://secure.adnxs.com/seg?add=1432957&t=1';
}else {
  $conditionalPixelStatic='https://secure.adnxs.com/seg?add=1432958&t=1';
}

$conditionalPixelStatic='';
// END FIXED PIXELS 


/*
 * Para el cliente 2264375, lanzaremos 2... deber·n ser seguidos.
 * El parámetro `isImageObject` permite crear una imagen si está habilitado, en caso contrario se construirá un JS.
 */
/*$arr_isSpecificPixelJS = array(
	'2271169'=>array(0=> 'http://pixel.mathtag.com/event/js?mt_id=224630&mt_adid=111634&v1=&v2=&v3=&s1=&s2=&s3=',1 => '','isImageObject'=>0),
	'2371425'=>array(0=> 'https://secure.adnxs.com/px?id=151505&seg=1432958&order_id=[XXfeaturedparamXX]&t=1',1 => '','isImageObject'=>0),
	'2083732'=>array(0=> 'http://app.linkemann.com/pixel.gif.php?cid=408&leadid=[XXfeaturedparamXX]&t=1',1 => '','isImageObject'=>1)
);
 * 
 */
$arr_isSpecificPixelJS = array();

/*
 * SE SOBREESCRIBEN EL ARRAY `$arr_creas` CON LOS DATOS ALMACENADOS EN LA BBDD
 * CORRESPONDIENTES A LA PROMOCION ACTUAL SI ESTA EXISTE
 * NECESARIO TAMBIEN PARA MANTENER COMPATIBILIDAD CON LAS PROMOS ANTERIORES
 * 
 * (M.F. mayo 2014)
 * 
 */
if(@$isDataDb && @$objSiteData->landingId)
{
	$arr_creas[$objSiteData->landingId]['title']=$objSiteData->webTitle;
	$arr_creas[$objSiteData->landingId]['body']=$objSiteData->bodyScript;
	$arr_creas[$objSiteData->landingId]['nombpromo']=$objSiteData->landingName;
	$arr_creas[$objSiteData->landingId]['is_mobile']=0;
	$arr_creas[$objSiteData->landingId]['is_compro_prov']=1;
	$arr_creas[$objSiteData->landingId]['condiciones']=$objSiteData->landingTermsFile;
	$arr_creas[$objSiteData->landingId]['protecciondatos']=$objSiteData->landingPrivacyPolicyFile;
	$arr_creas[$objSiteData->landingId]['cookie_css']=$objSiteData->footerCookiesCssFile;
	$arr_creas[$objSiteData->landingId]['mobile_auto_modal_on_open']=$objSiteData->mobileAutoModal;
	$arr_creas[$objSiteData->landingId]['manageDevice']=1;
	$arr_creas[$objSiteData->landingId]['new_template']=1;
	$arr_creas[$objSiteData->landingId]['js']='';
	$arr_creas[$objSiteData->landingId]['skip_check_phone']=$objSiteData->skipCheckPhone;	//	EVITA EL CONTROL DEL NUMERO TELEFONICO (CURL)
}

if($debugModeNew)
{
//	$debugDujok=true;
//echo 'local-conf:<pre>';print_r($objSiteData->semGoogleParams);echo '</pre>';
}
?>