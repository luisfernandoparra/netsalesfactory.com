<input type="hidden" name="destino" id="destino" value="">
	<div id="campos">
			<div class="error"><ul></ul></div>
			<p>Envía ayuda ahora<br>a través de nuestro<br>formulario</p>

			<div class="fila">
					<div class="fleft">
							<input type="text" name="nombre" id="nombre" value="" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre es obligatorio" placeholder="Nombre" aria-required="true">
					</div>
			</div>

			<div class="fila">
					<div class="fleft">
							<input type="tel" maxlength="9" name="telefono" id="telefono" class="celda" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Teléfono" aria-required="true">
					</div>
			</div>

			<div class="fila">
					<div class="fleft">
							<input type="email" name="email" id="email" maxlength="100" class="celda" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="" placeholder="Email" aria-required="true">
					</div>
			</div>

			<div class="legal" >
				<input required="" data-msg-required="Debes leer y marcar la casilla de <strong>Las condiciones</strong>" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" aria-required="true">&nbsp;&nbsp;Acepto 
				<a class="enlace_condiciones" href="http://acnur.es/politica-privacidad" data-ref="" target="_blank">política de privacidad</a>
			</div>

			<div class="fila">
					<div class="espacio_btn">
							<input class="button green sendData" type="button" id="btnProcesar" name="btnProcesar" value="DONA AHORA">
					</div>
			</div>
	</div>
