<?php
@session_start();
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
//$drawInbound_phone='900806449';
//$drawInbound_phone='';

if ($drawInbound_phone) { // SI EXISTE, SE DIBUJA EL TAG PARA PODER LLAMAR DESDE EL DISPOSITIVO MOVIL HACIENDO UNICAMENTE CLICK
    $tagInboundPhoneStart = '<a class="telf" href="tel:' . $inbound_phone . '" style="text-decoration:none;">';
    $tagInboundPhoneEnd = '</a>';
}

// TEXTO A MOSTRAR ENCIMA DE LOS CAMPOS SEGÚN EL id crea
$arrMsgForm=array(
62=>'PLAN DENTAL PLUS MetLife<br />Infórmate sin compromiso',
63=>'PLAN PROTECCIÓN SALUD MetLife<br />Infórmate sin compromiso',
64=>'Infórmate sin compromiso'
);

?>
<style>
.innerModal{
	box-sizing:border-box;
	margin:0;
	padding:0;
	padding-top:6%;
	position:relative;
	line-height:22px!important;
	font:12px Arial,Helvetica,sans-serif;
}

.sendData{
		cursor:pointer;
}
.privacyBottom{

}
</style>
<?=$tagInboundPhoneStart?>
<?=$tagInboundPhoneEnd?>

<div class="error"><ul></ul></div>
<p><?=$arrMsgForm[$id_crea]?></p>

<div class="fila">
    <div class="fleft">
        <input type="text" name="nombre" id="nombre" value="" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" placeholder="Nombre" aria-required="true">
    </div>
</div>

<div class="fila">
    <div class="fleft">
        <input name="telefono" id="telefono" type="tel" class="celda" maxlength="9" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Teléfono" aria-required="true">
    </div>
</div>

<div class="fila boton">
		 <div class="fleft">
		<input id="btnProcesar" name="registrate" type="button" value="Enviar" class="sendData" data-direction="down">
		</div>
</div>

<div class="clearfix"></div>

<div class="legal">
    <input required="" data-msg-required="Debe leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debe leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />

    He leído y acepto la <a href="index.php?cr=<?=$id_crea?>#privacy" class="smooth privacyBottom">política de privacidad<sup>1</sup></a>
</div>

