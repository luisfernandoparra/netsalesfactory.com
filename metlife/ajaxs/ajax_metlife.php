<?php
/**
 * SCRIPT ESPECIFICO METLIFE
 * SE EFECTÚA EL ENVÍO DE DATOS POR WS AL CLIENTE DE LA LANDING
 * 
 * M.F. 2015.06.29
 */


/*
 * FUNCION ESPECIFICA PARA EL CLIENTE metlife
 * INICIALMENTE SE CARGA DESDE webservice/clientes/client_181.php
 * 
 * 2014.10.01 M.F.
 */
function DialerCallBackML($serverIp='http://c2c.unitono.com', $extraData, $vcc='unitono', $state='Corregistros',$debug_flag=0)
{
	date_default_timezone_set('UTC');
	$port='8083';
	$soaUri='/inconcert/apps/soaintegration';
	$appUri='/execute_flow_state/';
	$url=$serverIp.':'.$port.$soaUri.$appUri;

	$header=array (  
		'User-Agent MSIE 7.0',
		'Content-Type: application/json'
	);

	$isVip=0;	$isClient=0; $zip=-1;	$extension=''; $agentId='';
	$data=new SimpleXMLElement('<data/>');
	$data->addChild('vcc', $vcc);
	$data->addChild('state', $state);

	foreach($extraData as $nombre => $valor)
	{
		$item=$data->addChild('extradata');
		$item->addChild('name', $nombre);
		$item->addChild('value', $valor);
	}

	$postXml=$data->asXML();
	$postXml=str_replace('<?xml version="1.0"?>', '', $postXml);
//if(trim($_REQUEST['nombre']) == 'pruebaEnvio')print_r($extraData);echo '<hr>FUNCION METLIFE:<p>URL = '.$url.'<p>sigue el postXml = <hr> '.$postXml.' <hr>';

	$date1= time();
	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postXml);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_POST, 1);
	$response=curl_exec($ch);
//if(trim($_REQUEST['nombre']) == 'pruebaEnvio'){echo'RESPONSE==>[';print_r($response);echo']';}
	curl_close($ch);
	$date2=time();
	$connection_time =$date2 - $date1;

	return $response;
}
// END FUNCION ESPECIFICA PARA ESTE CLIENTE


$serverIp = 'c2c.unitono.com';
$arrADN[63]='NETSALESFACTORY|CPL|EMAIL|1517-0005|INICIAL';	//ADN SALUD
$arrADN[62]='NETSALESFACTORY|CPL|EMAIL|1509-0005|INICIAL';	//ADN DENTAL
$Adn=$arrADN[$campos['crea']];	//ADN SEGÚN LA LANDING CARGADA

$metlifeFields=array(
	'IdLead'=>$id,
	'Adn'=>$Adn,
	'sexo'=>'',
	'Nombre'=>$campos['nombre'],
	'Apellidos'=>'',
	'Email'=>'',
	'TlfFijo'=>$campos['telefono'],
	'Franja'=>0,
	'Movil'=>$campos['telefono'],
	'FNacimiento'=>'',
	'Localidad'=>'',
	'Cp'=>'',
	'Fecha'=>date('d/m/Y'),
	'Fumador'=>'',
	'Url'=>''
);

////IP FUNCIONAL EN GTP4 --> http://85.62.84.136:8083
////$resMetlife=DialerCallBackML('http://195.235.218.189:8083/inconcert/apps/soaintegration/execute_flow_state/', $metlifeFields);

$resMetlife=DialerCallBackML('http://85.62.84.136', $metlifeFields);
$arrResMetlife=json_decode($resMetlife);
$respWs=(int)$arrResMetlife->data ? $arrResMetlife->data : '0';
$query='UPDATE %s SET response_ws=\'%s\', response_extended=\'%s\' WHERE id_registro=%d ';	// SE ACTUALIZA registros CON LA RESPUESTA DEL CLIENTE
$query=sprintf($query, $table_registros, $respWs, $resMetlife ,(int)$id);
$conexion->ejecuta_query($query);
//echo'respuesta=';print_r($arrResMetlife->data);
