<?php
/**
 * Página de gracias
 */
if((empty($_REQUEST['fromindex']) || empty($_SERVER['HTTP_REFERER'])))
{
	//header("location:http://www.cursosinglesoceanoidiomas.com/");
}
//echo "<pre>";
//print_r($_REQUEST);
//print_r($_SERVER);
//die('bye');

include('../conf/config_web.php');
ini_set('display_errors',0);
include('conf/config_web.php');
include($path_raiz_includes . 'includes/initiate_thankyoupage.php');
include($path_raiz_includes . 'class/class.pixels.management.php');
/**
 * Como hay que mandar los datos por ws incluyendo provincia, debemos primero sacar la pronvicia a la que pertenece.
 * Hay excepciones del propio cliente para la provincia. 2 tipos excepciones:
 * 1.- Guipuzcoa es SS
 * 2.- algunas provincias se coge la primera letra
 */
$array_prov_directa = array(1=>'VI',
    2=>'AB',
    3=>'A',
    4=>'AL',
    5=>'AV',
    6=>'BA',
    7=>'PM',
    8=>'B',
    9=>'BU',
    10=>'CC',
    11=>'CA',
    12=>'CS',
    13=>'CR',
    14=>'CO',
    15=>'C',
    16=>'CU',
    17=>'GI',
    18=>'GR',
    19=>'GU',
    20=>'SS',
    21=>'H',
    22=>'HU',
    23=>'J',
    24=>'LE',
    25=>'L',
    26=>'LO',
    27=>'LU',
    28=>'M',
    29=>'MA',
    30=>'MU',
    31=>'NA',
    32=>'OR',
    33=>'O',
    34=>'P',
    35=>'GC',
    36=>'PO',
    37=>'SA',
    38=>'TF',
    39=>'S',
    40=>'SG',
    41=>'SE',
    42=>'SO',
    43=>'T',
    44=>'TE',
    45=>'TO',
    46=>'V',
    47=>'VA',
    48=>'BI',
    49=>'Z',
    50=>'ZA',
    51=>'CE',
    52=>'ML');
/**
 * Recogemos los datos que nos vienen para enviárselos al WS del cliente
 */
$movil = '';
$email = (!empty($_REQUEST['email']) && filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) ? strtolower(trim($_REQUEST['email'])) : '';
$nombre = (!empty($_REQUEST['nombre'])) ? ucwords(strtolower(trim($_REQUEST['nombre']))) : '';
$apellidos = (!empty($_REQUEST['apellidos'])) ? ucwords(strtolower(trim($_REQUEST['apellidos']))) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono']) && strlen($_REQUEST['telefono'])==9) ? strtolower(trim($_REQUEST['telefono'])) : '';
$cp = (!empty($_REQUEST['cp']) && is_numeric($_REQUEST['cp']) && (strlen($_REQUEST['cp'])>=4 && strlen($_REQUEST['cp'])<=5)) ? str_pad(strtolower(trim($_REQUEST['cp'])),5,'0',STR_PAD_LEFT) : '';
$poblacion = (!empty($_REQUEST['población'])) ? ucwords(strtolower(trim($_REQUEST['población']))) : '';
$id = (!empty($_REQUEST['leadNumber']) && is_numeric($_REQUEST['leadNumber'])) ? (int)(trim($_REQUEST['leadNumber'])) : '';


$dia_fech = (!empty($_REQUEST['dia_fech']) && is_numeric($_REQUEST['dia_fech']) && strlen($_REQUEST['dia_fech'])==2) ? strtolower(trim($_REQUEST['dia_fech'])) : '';
$mes_fech = (!empty($_REQUEST['mes_fech']) && is_numeric($_REQUEST['mes_fech']) && strlen($_REQUEST['mes_fech'])==2) ? strtolower(trim($_REQUEST['mes_fech'])) : '';
$ano_fech = (!empty($_REQUEST['ano_fech']) && is_numeric($_REQUEST['ano_fech']) && strlen($_REQUEST['ano_fech'])==4) ? strtolower(trim($_REQUEST['ano_fech'])) : '';

$fecha = $ano_fech.'-'.$mes_fech.'-'.$dia_fech;


$prov = ''; 
$lanzarpixel = $email!='' && $nombre!='' && $apellidos!='' && $telefono!='' && $cp!='' && $ano_fech!='' && $mes_fech!='' && $dia_fech!='';
if ($cp !='') {
    $prov = 'ES-'.$array_prov_directa[(int)substr($cp,0,2)];
}
/*if ($telefono!='' && strlen($telefono)>2 && ($telefono[0]=='6' || $telefono[0]=='7')) {
    $movil = $telefono;
    $telefono = '';
}
 * 
 */
/**
 * URL del WS
 */
$url = "http://www.activolead.com/tracking.php?idsoporte=479&idcurso=4686&idcampana=308&email=$email&nombre=$nombre&idpais=ES&"
        . "idprovincia=$prov&apellidos=$apellidos&telefono=$telefono&fechanacimiento=$fecha&sexo=&localidad=$poblacion&direccion="
        . "&cp=$cp&numero=&dni=&movil=&telefono2=&nestudios=&situacion=&empresa=&nota=&horario_llamada=";

?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>¡Gracias!</title>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width,initial-scale=1"/>
<link href="<?php echo $path_raiz_aplicacion_local; ?>css/estilos_thanks.css" rel="stylesheet" type="text/css">
</head>

<body>
    <input type="hidden" id="t1" name="t1" value="<?php echo $id;?>" />
    <input type="hidden" id="t2" name="t2" value="<?php echo @$_REQUEST['leadNumber'];?>" />
<section class="contenedor">
		<section class="foto">
				<img src="<?php echo $path_raiz_aplicacion_local.'img/'.$objSiteData->prefixFolders.'/thankyou_page.jpg'; ?>"/>
				<img src="<?php echo $path_raiz_aplicacion_local.'img/'.$objSiteData->prefixFolders.'/thankyou_page_mvl.jpg'; ?>"/>
		</section>
</section>
    <?php 
    if ($lanzarpixel) 
    {    ?>
    <img src="<?php echo $url ?>" width="1" height="1" border="0" />
    <?php } ?>
    <?php 
    if ($id !=''){
    ?>
    <img src="http://tbl.tradedoubler.com/report?organization=1973940&event=329091&leadNumber=<?php echo $id;?>" width="1" height="1" border="0" />
    <?php  } ?>
<?php

//include($path_raiz_includes . 'includes/pixels_thankyoupage.php');
?>
</body>
</html>