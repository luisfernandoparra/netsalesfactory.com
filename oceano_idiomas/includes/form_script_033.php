<?php
@session_start();
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
//$drawInbound_phone='900806449';
//$drawInbound_phone='';

if ($drawInbound_phone) { // SI EXISTE, SE DIBUJA EL TAG PARA PODER LLAMAR DESDE EL DISPOSITIVO MOVIL HACIENDO UNICAMENTE CLICK
    $tagInboundPhoneStart = '<a class="telf" href="tel:' . $inbound_phone . '" style="text-decoration:none;">';
    $tagInboundPhoneEnd = '</a>';
}
?>
<style>


    .innerModal{
        box-sizing:border-box;
        margin:0;
        padding:0;
        padding-top:6%;
        position:relative;
        line-height:22px!important;
        font:12px Arial,Helvetica,sans-serif;
    }

    .sendData{cursor:pointer;}
</style>
<?= $tagInboundPhoneStart ?>
<?= $tagInboundPhoneEnd ?>

<div class="error"><ul></ul></div>
<input type="email" name="email" maxlength="200" id="email" value="<?= $em ?>" required data-msg-required="El campo &lt;strong&gt;Email&lt;/strong&gt; es obligatorio" placeholder="Email" />
<input type="tel" name="telefono" minlength="9" maxlength="9" data-rule-minlength="9" data-rule-maxlength="9" id="telefono" value="<?= $telefono ?>" required data-msg-minlength="Debe introducir solamente n&uacute;meros sin espacios" data-msg-number="Ha introducido un n&uacute;mero de tel&eacute;fono incorrecto, <b>solo se admiten n&uacute;meros</b>" data-msg-required="El campo &lt;strong&gt;tel&eacute;fono&lt;/strong&gt; es obligatorio" placeholder="Tel&eacute;fono" />
<div class="clearfix"></div>

<div class="legal">
    <input required="" data-msg-required="Debe leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debe leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />
    Al hacer "click" en el boton PARTICIPAR, del presente formulario, declaro y garantizo ser mayor de 18 años, y acepto tanto <a href="includes/landing_conditions.php?cr=33" target="_blank" class="smooth">Bases del sorteo</a> como la <a href="<?= $privacyPolicy ? $privacyPolicy : 'proteccion_datos.php'; ?>" target="_blank" class="smooth">política de privacidad</a>
</div>

<input class="sendData box fade-in one" type="button" id="btnProcesar" name="btnProcesar" value="Participar" data-direction="down" />
<!--/#campos-->
