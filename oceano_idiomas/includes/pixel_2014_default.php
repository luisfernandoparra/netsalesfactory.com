<?php
/*
 * GET DEFAULT BBDD PIXEL
 * 
 * ONLY $id_crea PIXELS
 * (LFP - MF 2014.07.05)
 */
@session_start();

include($path_raiz_includes.'class/class.pixels.management.php');	// ADDED 2014.07.05
$cls_pix = new pixel(0);
//Sacamos los pixeles que saltan siempre para la página en cuestión
$arrPixelsBBDD = $cls_pix->get_pixels($id_crea);

if (!empty($id_source)) {
		$cls_pix->set_Id_source($id_source);
		$arrPixelsBBDD = array_merge($arrPixelsBBDD, $cls_pix->get_pixels($id_crea));
		$cls_pix->set_Id_source('');
}

foreach ($arrPixelsBBDD as $pixel)
	echo $pixel;

unset($cls_pix);
?>