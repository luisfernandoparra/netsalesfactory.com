<?php
@session_start();
$drawInbound_phone = preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);
//$drawInbound_phone='900806449';
//$drawInbound_phone='';

if ($drawInbound_phone) { // SI EXISTE, SE DIBUJA EL TAG PARA PODER LLAMAR DESDE EL DISPOSITIVO MOVIL HACIENDO UNICAMENTE CLICK
    $tagInboundPhoneStart = '<a class="telf" href="tel:' . $inbound_phone . '" style="text-decoration:none;">';
    $tagInboundPhoneEnd = '</a>';
}
?>
<style>
    .innerModal{
        box-sizing:border-box;
        margin:0;
        padding:0;
        padding-top:6%;
        position:relative;
        line-height:22px!important;
        font:12px Arial,Helvetica,sans-serif;
    }

    .sendData{
        cursor:pointer;
    }
</style>
<?= $tagInboundPhoneStart ?>
<?= $tagInboundPhoneEnd ?>

<div class="error"><ul></ul></div>
<p>Solicita información</p>

<div class="fila">
    <div class="fleft">
        <input type="text" name="nombre" id="nombre" value="" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" placeholder="Nombre" aria-required="true">
    </div>
</div>

<div class="fila">
    <div class="fleft">
        <input type="text" name="apellidos" id="apellidos" value="" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Apellidos&lt;/strong&gt; es obligatorio" placeholder="Apellidos" aria-required="true">
    </div>
</div>

<div class="fila">
    <div class="fleft">
        <input type="email" name="email" class="celda" maxlength="200" id="email" value="<?= $em ?>" required data-msg-required="El campo &lt;strong&gt;Email&lt;/strong&gt; es obligatorio" placeholder="Email" />
    </div>
</div>

<div class="fila">
    <div class="fleft">
        <input name="telefono" id="telefono" type="tel" class="celda" maxlength="9" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" placeholder="Teléfono" aria-required="true">
    </div>
</div>

<div class="fila">
    <div class="fleft">
        <input name="cp" id="cp" type="text" class="celda" maxlength="5" required data-msg-required="El campo &lt;strong&gt;Cp&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="4" data-msg-digits="El campo &lt;strong&gt;Cp&lt;/strong&gt; sólo acepta números" data-msg-minlength="El campo &lt;strong&gt;Cp&lt;/strong&gt; debe contener al menos 4 dígitos" placeholder="Código Postal" aria-required="true">
    </div>
</div>

<div class="fila">
    <div class="fleft">
        <input name="población" id="población" type="text" class="celda" maxlength="100" required data-msg-required="El campo &lt;strong&gt;Población&lt;/strong&gt; es obligatorio" data-rule-minlength="3" data-msg-minlength="El campo &lt;strong&gt;Población&lt;/strong&gt; debe contener al menos 3 letras" placeholder="Población" aria-required="true">
    </div>

</div>

<div class="fila">
    <div class="fecha_nacimiento">
        <p>Fecha de Nacimiento</p>
        <div id="capa_fecha">
            <select name="dia_fech" id="dia_fech" class="f_dia cookiesEnable" required data-msg-required="El campo &lt;strong&gt;D&iacute;a de Nacimiento&lt;/strong&gt; es obligatorio" placeholder="Dia">
                <option value='' selected="selected">Dia</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option>
            </select>
            <select name="mes_fech" id="mes_fech" class="f_mes cookiesEnable" required="required" data-msg-required="El campo &lt;strong&gt;Mes de Nacimiento&lt;/strong&gt; es obligatorio">
                <option value='' selected="selected">Mes</option><option value="01">Enero</option><option value="02">Febrero</option><option value="03">Marzo</option><option value="04">Abril</option><option value="05">Mayo</option><option value="06">Junio</option><option value="07">Julio</option><option value="08">Agosto</option><option value="09">Septiembre</option><option value="10">Octubre</option><option value="11">Noviembre</option><option value="12">Diciembre</option>
            </select>
            <select name="ano_fech" id="ano_fech" class="f_ano cookiesEnable" required="required" data-msg-required="El campo &lt;strong&gt;Año de Nacimiento&lt;/strong&gt; es obligatorio">
                <option value='' selected="selected">Año</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option><option value="1964">1964</option><option value="1963">1963</option><option value="1962">1962</option><option value="1961">1961</option><option value="1960">1960</option><option value="1959">1959</option><option value="1958">1958</option><option value="1957">1957</option><option value="1956">1956</option><option value="1955">1955</option><option value="1954">1954</option><option value="1953">1953</option><option value="1952">1952</option><option value="1951">1951</option><option value="1950">1950</option><option value="1949">1949</option><option value="1948">1948</option><option value="1947">1947</option><option value="1946">1946</option><option value="1945">1945</option><option value="1944">1944</option><option value="1943">1943</option><option value="1942">1942</option><option value="1941">1941</option><option value="1940">1940</option><option value="1939">1939</option><option value="1938">1938</option><option value="1937">1937</option><option value="1936">1936</option><option value="1935">1935</option><option value="1934">1934</option><option value="1933">1933</option><option value="1932">1932</option><option value="1931">1931</option><option value="1930">1930</option><option value="1929">1929</option><option value="1928">1928</option><option value="1927">1927</option><option value="1926">1926</option><option value="1925">1925</option><option value="1924">1924</option><option value="1923">1923</option><option value="1922">1922</option><option value="1921">1921</option><option value="1920">1920</option><option value="1919">1919</option><option value="1918">1918</option><option value="1917">1917</option><option value="1916">1916</option><option value="1915">1915</option><option value="1914">1914</option><option value="1913">1913</option><option value="1912">1912</option><option value="1911">1911</option><option value="1910">1910</option><option value="1909">1909</option><option value="1908">1908</option><option value="1907">1907</option><option value="1906">1906</option><option value="1905">1905</option><option value="1904">1904</option><option value="1903">1903</option><option value="1902">1902</option><option value="1901">1901</option><option value="1900">1900</option>
            </select>
        </div>
    </div>
</div>




<div class="clearfix"></div>

<div class="legal">
    <input required="" data-msg-required="Debe leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debe leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />

    He leído y acepto la <a href="<?= $privacyPolicy ? $privacyPolicy : 'proteccion_datos.php'; ?>" target="_blank" class="smooth">política de privacidad</a>
</div>

<div class="espacio_btn">
    <input id="btnProcesar" name="registrate" type="button" value="Enviar" class="green sendData" data-direction="down">
</div>


<!--/#campos-->
