<?php
//***************************************
// START CONFIG DINAMICA (M.F. 19.11.2013)
$sitesPruebas = ($_SERVER['SERVER_ADDR'] == 'localhost' || $_SERVER['SERVER_ADDR'] == '127.0.0.1' || substr($_SERVER['SERVER_ADDR'], 0, 9) == '192.168.2'); // SITIOS PARA PRUEBAS
$paintErrors=false;
$minTimeCookiesStart=30;  // TIEMPO (EN SEGUNDOS) PARA ACTIVAR AUTOMATICAMENTE LAS COOKIES
$adjustPathPolicies='';


if ($sitesPruebas) {
    $url_local = 'http://139.162.246.12/netsalesfactory.com/promociones/seguros-caser/';
    $dir_raiz_aplicacion_local = $dir_raiz_aplicacion . 'seguros-caser/';
    $path_raiz_aplicacion_local = $dir_raiz_aplicacion_local . '';
    $path_raiz_includes_local = $path_raiz_includes . 'seguros-caser/';
    $url_condiciones = $url_local.'condiciones.php';
	$paintErrors=true;
	$minTimeCookiesStart=3000;
//	echo 'SITE EN PRUEBAS:: testeando. Este texto está en el config web ';
} else {
//Config web de CASER
    $url_local = 'http://www.caserseguro.com/';
	$adjustPathPolicies='seguros-caser/';
    $dir_raiz_aplicacion_local = $dir_raiz_aplicacion . $adjustPathPolicies;
    $path_raiz_aplicacion_local = '/' . $dir_raiz_aplicacion_local;
    $path_raiz_includes_local = $path_raiz_includes . $adjustPathPolicies;
    $url_condiciones = $url_local . $adjustPathPolicies.'condiciones.html';
}
// END CONFIG DINAMICA (M.F. 19.11.2013)

//Variable para la tabla REGISTROS, para diferenciar qué cliente es
$id_client = 19;
$prefFichExtraccion = 'dentalCASER';
$arr_campanas = array('TEST', 'CASER_LEAD');

//Array con todas las creas existentes. 
$arr_creas = array(1 =>
    array(
	  'title' => 'Seguros CASER',
	  'body' => 'body_1.php',
	  'nombpromo' => 'CASER',
	  'is_mobile' => 0,
	  'is_compro_prov' => 1,
	  'condiciones' => $url_local.$adjustPathPolicies.'condiciones.php',
	  'protecciondatos' => $url_local.$adjustPathPolicies.'privacy_policy.php',
	  'cookie_css' => 'footer_cookies.css'
	)
);
$is_ctc=1;

//Parámetros del pixel de TradeDoubler
$organization = '1886220';
$event = '301987';
$nombCliente = 'CASER';
$secret_pwd_conf='p3Rd€r_N@da';
$secret_usr=date('Y-m-d H:i:s');
$secret_pwd = md5($secret_usr.$secret_pwd_conf);
//Pixel expecíoficos
$isSpecificPixel = 1;

//Array de display
$arr_isSpecificPixel=array();
/*$arr_isSpecificPixel = array(
	'2261683' => '<img src="http://ads.creafi-online-media.com/pixel?id=2357960&t=2" width="1" height="1" />',
    '1945955' => '<img src="https://ad.yieldmanager.com/pixel?id=2318907&t=2" width="1" height="1" />',
    '2262090' => '<img src="https://www.wtp101.com/pixel?id=16499" width="1" height="1" border="0" />',
    '2264375' => '<script src="http://ib.adnxs.com/seg?add=685954&t=1" type="text/javascript"></script>',
    '2271169' => '<script language="JavaScript1.1" src="http://pixel.mathtag.com/event/js?mt_id=224629&mt_adid=111634&v1=&v2=&v3=&s1=&s2=&s3="></script>',
    '1985489' => '<img src="https://secure.adnxs.com/seg?add=1046253&t=2" width="1" height="1" />'

    ,'9999999' => '<script language="JavaScript1.1" src="http://wrap.tradedoubler.com/wrap?id=8498"></script>'

	);
 * */
 

//Para este cliente, lanzaremos 2... deber·n ser seguidos.
$arr_isSpecificPixelJS = array();
/*$arr_isSpecificPixelJS = array(
	'2264375' => array(0 => 'https://secure.adnxs.com/px?id=73601&t=1', 1 => 'https://secure.adnxs.com/seg?add=685956&t=1'),
    '2271169' =>array(0 => 'http://pixel.mathtag.com/event/js?mt_id=224630&mt_adid=111634&v1=&v2=&v3=&s1=&s2=&s3=',1 => '')
	);
 * 
 */
?>