<?php

$arr_DATO_2=array(
'-'=>'--- Elige seguro---',
'Asisa'=>'Asisa',
'Mapfre'=>'Mapfre',
'Adeslas'=>'Adeslas',
'DKV'=>'DKV',
'Sanitas'=>'Sanitas',
'Caser'=>'Caser',
'Otro'=>'Otro',
'No tengo'=>'No tengo'
);

$arr_DATO_3=array(
''=>'--- Elige seguro---',
'Acceder a sanidad privada a un coste reducido'=>'Acceder a sanidad privada a un coste reducido',
'Las coberturas y el cuadro médico más completos'=>'Las coberturas y el cuadro médico más completos',
'Poder elegir médico y hospital en todo el mundo'=>'Poder elegir médico y hospital en todo el mundo',
'No quiero sufrir listas de espera'=>'No quiero sufrir listas de espera'
);
?>
 

<div class="mainContent" style="display:block;position:relative;padding-bottom:240px;">
<div id="contenedor_centrado">
<?php
if(!$isOldBrowser)	// NO FUNCIONA CON I-EXPLORER 8 Y ANTERIORES
{
?>
  <div class="entryFly">
	<p>El seguro que mejor se adapta a tí</p>
	<p style="display:none;" class="tlt2"  data-in-effect="rotateInDownRight">PAGA POR LO NECESARIO</p>
  </div>
<?php
}
?>
 
 <div class="titulo"></div>
 
 <div class="form-contenido">
     <div class="formulario">

            <div class="contenedor_form">
                <p><strong>Solicita información GRATIS</strong></p>
                <p><strong>Te llamamos nosotros</strong></p>
                <form action="" method="get" enctype="multipart/form-data">
                    <div class="fila">
                        <div class="fleft">Nombre*<input type="text" class="celda" id="nombre" name="nombre" maxlength="499" value="<?php echo $nombre; ?>" /></div>
                    </div>
                    <div class="fila">
                        <div class="fleft">Teléfono* <input type="text" class="celda" id="telefono" name="telefono" maxlength="9"  /></div>
                    </div>
                    <div class="fila">
                        <div class="fleft">E-mail* <input type="text" class="celda" name="email" id="email" value="<?php echo $email; ?>" /></div>
                    </div>
                     <div class="fila_min">
                        <div class="fleft">En caso de tener seguro médico, indique cual:
						  <select class="celda" id="DATO_2" name="DATO_2">
<?php 
foreach($arr_DATO_2 as $key => $value)
{
  $selected = '';
  $valor = strtolower(trim($value['name']));
//  if($valor == $_REQUEST['DATO_2'])
//	$selected = ' selected="selected"';
  
  echo '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
}
?>
						  </select>
                        </div>
                    </div>
                     <div class="fila_min">
                        <div class="fleft">
						  ¿Qué buscas en tu seguro de salud?
						  <select class="celda" id="DATO_3" name="DATO_3">
<?php 
foreach($arr_DATO_3 as $key => $value)
{
  $selected = '';
  $valor = strtolower(trim($value['name']));
//  if($valor == $_REQUEST['DATO_3'])
//	$selected = ' selected="selected"';
  echo '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
}
?>
						  </select>
						</div>
                    </div>
                    
                    
              </form>   
                 <div class="legal">
                    <input type="checkbox" id="cblegales" value="1" name="cblegales" /> He leído y acepto la <a class="enlace_pp" href="#null">política de privacidad</a>
                 </div>
			  <input name="btnProcesar" id="btnProcesar" value="Solicitar información >" class="btn_solicitar" type="button" />
            </div><!-- contenedor_form-->
          </div>
   </div>  <!-- contenedor_form contenido-->   
    <div class="srtBox"><div class="srt sello"></div></div>

   <div class="coberturas_servicios clearfix" >
        
            <div class="coberturas">
            
                <ul>
                    <h1>Coberturas</h1>
                    <li> Atención Primaria y Pediátrica</li>
                    <li>Todo tipo de Hospitalización</li>
                    <li>Tratamientos especiales.</li>
                    <li>Psicología </li>
                    <li>Prótesis sin límite.</li>
                    <li>Atención 24 horas de urgencia.</li>
                    <li>Especialidades médicas y avanzados Medios de Diagnóstico</li>
               </ul>
      </div> 
            
            <div class="servicios">
                <ul>
                    <h1>Coberturas especiales CASER</h1>
                    <li>Plan de Desarrollo Infantil</li>
                    <li>Cuidados Postparto</li>
                    <li>Planificación Familiar</li>
                    <li>Complemento Bucodental</li>
                    <li>Garantía de niños recién adoptados.</li>
                    <li>Ayuda en gastos de Farmacia y Vacunas infantiles</li>
                    <li>Diagnóstico de Infertilidad y Reproducción Asistida</li>
               </ul>
               <br/>
            </div>
		<div id="promoConditions" style="display:none;">
		  <h3>Condiciones de la promoción</h3>
		<p>Promoción válida para nueva contrataciones  de clientes particulares hasta el 31 de enero de 2014, que no hayan sido asegurados de Salud en ninguna de las Compañías del Grupo Caser en los últimos 6 meses.<br/><br/>Las tres mensualidades sin coste se aplicarán para los productos de Caser Salud Prestigio, Integral, Adapta, Activa y Médica, a cada uno de los beneficiarios de la póliza en los recibos de abril y noviembre de 2014 y si se renueva la póliza, el recibo de abril de 2015, siempre que ésta se encuentre al corriente de pago.<br/><br/>Además, en los productos Caser Salud Prestigio, Integral y Activa se aplicará durante 2014 el reembolso del 50% del importe de las facturas de óptica, hasta un máximo de 100€, así como el reembolso del 50% del importe de la facturas de farmacia hasta un máximo de 100€ por asegurado y año, en las condiciones estipuladas en dichas coberturas.<br/><br/>Coberturas sujetas al condicionado general, especial y particular de la póliza.</p>
		</div>

<!--		<div class="lnkPromo">
		  <p><a class="enlace_condiciones lnkStd" href="#null">Ver condiciones de la promoción</a></p>
        </div>-->
		<div class="pie">
		  <p>Borsenya S.L - Agente Exclusivo Caser - Plaza Callao, 4 6ª planta - Madrid, España - <a class="enlace_condiciones lnkStd" href="#null">Ver condiciones de la promoción</a></p>
        </div>
	 
    </div>
 </div> 
<br /><br />
 </div>
<?php

?>