<?php

/**
 * Modificación LFP 2015.03.23
 * Añadimos variable para procesar los datos que vienen de acción mobile emailing Gmail
 * Para eso añadimos variable $emailingSource que será true si viene de gmail
 * Si esta variable es true, no hacemos el die del final y tenemos los datos en un array
 * 
 * @date 2016.07.01. 
 * Nos piden que no mandemos nada a LeadSolutions, que no pasaba por el router. Por tanto lo pasamos todo por el router
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');
/**
 * Config loaded viene del fichero bridge. Si está definida y es true, es que ya ha cargado los configs
 */
if (empty($configLoaded)) {
    include('../../conf/config_web.php');
    include('../conf/config_web.php');
}
$resCurl = array();
///$sOutput = array(); // ARRAY PARA LOS DATOS DE SALIDA JSON
$raw = '';
$resCurl['error'] = 1;
//$sOutput['mensaje'] = 'ha habido un error en el proceso';
$sOutput['id'] = -1;
$cli = (empty($_REQUEST['cli'])) ? '' : $_REQUEST['cli'];

$emailingSource = (!empty($_REQUEST['emailingsource']) && strtoupper(trim($_REQUEST['emailingsource'])) == 'GMAIL');

$nombre = (!empty($_REQUEST['nombre'])) ? trim($_REQUEST['nombre']) : '';
$apellidos = (!empty($_REQUEST['apellidos'])) ? trim($_REQUEST['apellidos']) : '';
$email = (!empty($_REQUEST['em'])) ? trim($_REQUEST['em']) : '';
$provI = (!empty($_REQUEST['provincia_text'])) ? $_REQUEST['provincia_text'] : $_REQUEST['provincia'];

$legal = (!empty($_REQUEST['cblegales'])) ? trim($_REQUEST['cblegales']) : '';
$legal = ($legal === 1 || $legal === 0) ? $legal : '1';
$permanencia = (!empty($_REQUEST['perm'])) ? trim($_REQUEST['perm']) : '';
$permanencia = ($permanencia == 1 || $permanencia == 0) ? $permanencia : 0;
$crea = (!empty($_REQUEST['crea'])) ? trim($_REQUEST['crea']) : '';

$id_client = (!empty($_REQUEST['idclient']) && is_numeric($_REQUEST['idclient'])) ? trim($_REQUEST['idclient']) : '';
$sourcetype = (!empty($_REQUEST['sourcetype'])) ? trim($_REQUEST['sourcetype']) : '';
//Este dato lo metemos en bbdd???
$campaign = (!empty($_REQUEST['campaign'])) ? trim($_REQUEST['campaign']) : '';
$fuente = (!empty($_REQUEST['fuente']) && is_numeric($_REQUEST['fuente'])) ? trim($_REQUEST['fuente']) : '';

$prioridad = (!empty($_REQUEST['prioridad']) && is_numeric($_REQUEST['prioridad'])) ? trim($_REQUEST['prioridad']) : '';
$telefono = (!empty($_REQUEST['telefono']) && is_numeric($_REQUEST['telefono'])) ? trim($_REQUEST['telefono']) : '';

$error = 1;
$msg_err = 'Faltan Datos. Error 101';
$id = -1;


//echo'------------>'.$subFolderLanding;
/**
 * Capping. Eliminado. 
 *//*
  $id_cliente=$id_client;
  include($path_raiz_includes.'includes/capping_control.php');
  //die('hola');

 */
/**
 * Pequeña treta para el aplicativo GSS.
 * si el id_client es 28, habrá parámetros que cambiarán
 */
/*
  if ($id_client == 28 && $fuente != 2081175) {
  //SOLO PARA SALUD
  $file_ws = $arr_client_campaign[$id_client]['ctcFile'];
  //cambiamos el sourcetype
  if ($sourcetype != 'CTC')
  $sourcetype = 'FORMULARIO';

  }
 */
$campos = array('idclient' => $id_client, 'sourcetype' => $sourcetype, 'campaign' => $campaign, 'fuente' => $fuente, 'prioridad' => $prioridad, 'nombre' => $nombre, 'apellidos' => $apellidos, 'telefono' => $telefono, 'em' => $email, 'legal' => $legal, 'crea' => $crea, 'province' => $provI, 'vcc' => 'fintel', 'pwd' => $secret_pwd, 'usr' => $secret_usr);

$campos['cli'] = $cli; // ADDED 2016.04.13 M.F.
//,"serverIp"=>"84.127.240.42"
$qs = http_build_query($campos);
$url = $url_ws . $file_ws . '?' . $qs;
//print_r($campos);die($url);

if ($telefono != '') {
    $conexion = new CBBDD($db_type, $db_host, $db_user, $db_pass, $db_name, $debug_pagina);
    if (!$conexion->connectDB()) {
        $msg_err = 'No se ha podido conectar a la BBDD. Error 102';
    } else {
        $cur_conn_id = $conexion->get_id_conexion();
        /* $sql1="SET AUTOCOMMIT=0;";
          $conexion->ejecuta_query($sql1);

          $sql2="BEGIN";
          $conexion->ejecuta_query($sql2);
         */
        $nombre = str_replace('  ', ' ', ucwords(strtolower($nombre)));
        $nombreIns = addcslashes(mysql_real_escape_string(checkSqlInjection($nombre), $cur_conn_id), '%_');

        $telefonoIns = addcslashes(mysql_real_escape_string(checkSqlInjection($telefono), $cur_conn_id), '%_');
        $sql = 'INSERT INTO %s (id_cliente,nombre,apellidos,telefono,id_provincia,b_legales,email) VALUES (\'%d\',\'%s\',\'%s\',\'%s\',\'%d\', %d,\'%s\');';
        $sql = sprintf($sql, $table_registros, $id_client, $nombreIns, $crea, $telefonoIns, (int) $_REQUEST['provincia'], $legal, $email);
        $conexion->ejecuta_query($sql);
        $id = mysql_insert_id();
        $conexion->disconnectDB();
        $rnd = rand(1, 100);
        /**
         * PROCESO PARA EFECTUAR EL DE "Sanitas Dental"
         * 2016.06.10. Si es cliente 9 y además tiene una prioridad del 50% lo mandamos como ahora, si no, lo mandamos al router
         * 2016.07.01. Lo mandamos todo al router
         */
//        if ($id_client == 9 && $rnd<50) { // ONLY CLIENT ID = 9
//            include($path_raiz_includes . $subFolderLanding . '/includes/SOA.inconcert.php');
//
//            $campaignId = 'SanitasC2C'; //'NombreCampaña'; //$crea #Poner el nombre de la campaña en Inconcert a la que hay que ingresar el lead para que lo llamen
////				$serverIp = '212.231.5.28'; #Dirección IP del servidor donde se encuentra el servicio WebHandler
//            $serverIp = '212.231.5.129'; #Dirección IP del servidor donde se encuentra el servicio WebHandler (2016.02.01)
//            $vcc = 'Leading'; #VCC en el que está definido el flujo
//            $importId = '';
//            $batchId = 'NetSales'; #Lote del motor de marcación de la campaña en Inconcert
//            $contactId = $id; #se ha de generar un contactID nuevo por contacto
//            $countryId = '';
//            $areaId = '';
//            $scheduleDate = '';
//            $agentId = '';
//            $timeZone = 'CET'; #Zona horaria en la que se interpretara a $scheduleDate, en españa es CET
//            $batchPriority = '1'; #prioridad del lote, por defecto es 1
//            $contactPriority = '100'; #Prioridad del contacto, por defecto es 100
//            $contactName = utf8_encode($nombre . ' ' . $apellidos); #Hago encode a UTF-8 para evitar problemas con los tildes y ñ
//            $phoneType = (substr($telefono, 0, 1) == 6) ? 'CELLULAR' : 'HOME'; #Se ha de definir el tipo de telefono, HOME o CELLULAR
//
//            $extraData = array(
//                'direccion' => '',
//                'ciudad' => ($provI ? $provI : ''),
//                'FechaTest' => date('d-m-Y H:i:s')
//            );
//
//            // LLAMADA A LA FUNCION ESPECIFICA DE SOA INCONCERT (M.F. 2015.12.28)
//            $prePhoneNUmber = '+34';
//            $raw = DialerCallBack($serverIp, $vcc, $campaignId, $importId, $batchId, $contactId, $phoneType, $countryId, $areaId, $prePhoneNUmber . $telefono, $contactName, $contactPriority, $extraData, $scheduleDate, $timeZone, $agentId, $batchPriority);
//
//            $resCurl = json_decode($raw);
//            $resCurl->setHashedEmail = ($id_client == 28) ? md5($email) : ''; // ONLY X CRITEO TAG 2015.10.22 M.F.
//            $raw = json_encode($resCurl);
//            $error = $resCurl->status && $resCurl->internalCode == 1 ? 0 : 1;
//            $resCurl->mensaje = $resCurl->internalCode == -7 ? 'El teléfono ingresado no es válido' : '';
//            $resCurl->error = $error;
//            $resCurl->id = $id;
//            $resCurl->success = !$error;
//            $raw = json_encode($resCurl);
//
//            $query = 'UPDATE %s SET response_ws=%d, response_extended=\'%s\' WHERE id_registro=%d';
//            $query = sprintf($query, $table_registros, $resCurl->success, 'internalCode=' . $resCurl->internalCode . ($resCurl->message ? ', ' . $resCurl->message : ''), (int) $id);
//            $conexion->ejecuta_query($query);
////echo $query.'<hr>error='.$error;print_r($_REQUEST);print_r($resCurl);print_r($raw);echo $id_client;die();
//        } else {
            $ch = curl_init();
            $timeout = 0; // set to zero for no timeout
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $raw = curl_exec($ch);
            curl_close($ch);

            $resCurl = json_decode($raw);
            $resCurl->setHashedEmail = ($id_client == 28) ? md5($email) : ''; // ONLY X CRITEO TAG 2015.10.22 M.F.
            $raw = json_encode($resCurl);
            $error = $resCurl->error;
//        }


        if ($error) {
            $msg_err = 'Parece que el problema se debe a lo siguiente: "' . $resCurl->mensaje . '".';
        } else {
            $tmpCrea = $_SESSION['idCreatividad'];
            unset($_SESSION['namePromo']); // SE DESTRUYE LA REFERENCIA DE LA SESION
            $tmpName = session_name('sanitas-pro' . date(Yimis));
            $_SESSION['namePromo'] = session_name(); // SE CONSTRUYE UNA NUEVA SESION PARA PERMITIR REGISTROS NUEVOS DESDE LA MISMA APERTURA
            $_SESSION['idCreatividad'] = $tmpCrea;

            if (!$emailingSource) {
                die($raw);
            } else {
                echo $url;
                $resCurl = json_decode($raw, true);
            }
        }
    }
} else {
    if ($telefono == '') {
        $error = 1;
        $msg_err = 'Falta el campo Teléfono. Error 104';
    }
}

if ($error) { // LOG ACTIONS
    $query = 'SELECT id FROM %s WHERE accion=\'%s\' && session_id=\'%s\' ORDER BY id DESC LIMIT 1 ';
    $query = sprintf($query, $table_front_actions, $email, $_SESSION['namePromo']);
    $conexion->getResultSelectArray($query);
    $resQuery = $conexion->tResultadoQuery;
    $idUpdate = $resQuery[0]['id'];
    $query = 'UPDATE %s SET http_referer=\'ERROR CTC: %s\' WHERE id=\'%d\'';
    $query = sprintf($query, $table_front_actions, isset($resCurl->internalCode) ? 'InconcertErr=' . $resCurl->internalCode : $resCurl->mensaje, (int) $idUpdate);
//echo $query;die();
    $conexion->ejecuta_query($query);
}

$resCurl->setHashedEmail = ($id_client == 28) ? md5($email) : ''; // ONLY X CRITEO TAG 2015.10.22 M.F.

if (!$emailingSource) {
    $res = json_encode($resCurl);
    die($res);
}
/* $res = json_encode($resCurl);
  die($res);
 * 
 */
?>