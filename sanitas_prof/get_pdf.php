<?php
include('../conf/config_web.php');
$filename='ServiciosSanitasDentalMilenium.pdf';
$fileinfo=pathinfo($filename);
$sendname=$fileinfo['filename'].'.'.strtoupper($fileinfo['extension']);
header('Pragma: public');  // required
header('Expires: 0');  // no cache
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Cache-Control: private', false);
header('Content-Type: application/pdf');
header('Content-disposition: attachment; filename='.$sendname);
header("Content-Transfer-Encoding:  binary");
header('Content-Length: ' . filesize($filename)); // provide file size
header('Connection: close');
readfile($filename);
die();?>
