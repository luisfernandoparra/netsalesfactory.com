<?php
@session_start();
$drawInbound_phone=preg_replace('/\d{3}/', '$0.', str_replace('.', null, trim($inbound_phone)), 2);

?>
					<h3 id="h3_movil" class="color cyan">Infórmate GRATIS&nbsp;<i class="icono-grande icotel"></i><a class="telf" href="tel:902310122">900.834.152</a></h3>
					<h3 id="h3_web" class="color cyan">Solicita información GRATIS</h3>

					<div id="campos">
						<div class="error"><ul></ul></div>

						<div class="row">
							<div class="col4"><label for="nombre">Nombre</label></div>
							<div class="col8"><input type="text" name="nombre" maxlength="100" id="nombre" value="<?=$nombre?>" required data-msg-required="El campo &lt;strong&gt;Nombre&lt;/strong&gt; es obligatorio" /></div>
						</div>

						<div class="row">
							<div class="col4">
								<label for="telefono">Teléfono</label>
							</div>

							<div class="col8">
								<input type="tel" maxlength="9" name="telefono" id="telefono" required data-msg-required="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; es obligatorio" data-rule-digits="true" data-rule-minlength="9" data-msg-digits="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; sólo acepta números" data-rule-minlength="9" data-msg-minlength="El campo &lt;strong&gt;Teléfono&lt;/strong&gt; debe contener al menos 9 dígitos" />
							</div>
						</div>

						<div class="row">
							<div class="col4">
								<label for="email">E-mail</label>
							</div>

							<div class="col8">
								<input type="email" name="email" maxlength="100" id="email" required data-msg-required="El campo &lt;strong&gt;E-mail&lt;/strong&gt; es obligatorio" data-msg-email="El campo &lt;strong&gt;Email&lt;/strong&gt; no es válido" value="<?=$email?>" />
							</div>
						</div>

						<div class="row">
							<div class="col4">
								<label id="txt_sel_prov" for="sel_prov">Provincia</label>
							</div>

							<div class="col8">
								<select id="sel_prov" name="sel_prov" required="" data-msg-required="El campo &lt;strong&gt;Provincia&lt;/strong&gt; es obligatorio" data-msg-sel_prov="El campo &lt;strong&gt;Provincia&lt;/strong&gt; no es válido">
									<option value="">Seleccione Provincia</option>
<?php
foreach($arr_prov as $key=>$value)
{
	$selected = '';
	$valor=strtolower(trim($value['name']));
	$valor=str_replace($arr_input,$arr_output,$valor);

	if($valor === $provincia)
	{
		$selected=' selected ';
	}
	echo '<option value=\''.$value['id'].'\''.$selected.'\'>'.$value['name'].'</option>';
}
?>
								</select>
							</div>
						</div>

						<div class="row">
							<div class="col12">
								<input class="button orange sendData" type="button" id="btnProcesar" name="btnProcesar" value="ENVIAR">
							</div>
						</div>

						<div class="row" style="padding-top:10px;">
							<div class="col12">
								<p class="mini"><input required="" data-msg-required="Debes leer y marcar la casilla de &lt;strong&gt;La política&lt;/strong&gt; y &lt;strong&gt;Las condiciones&lt;/strong&gt;" data-rule-digits="true" data-msg-cblegales="Debes leer y marcar la casilla" type="checkbox" name="cblegales" id="cblegales" value="1" />&nbsp;&nbsp;He leído y acepto la 
								<a class="enlace_condiciones" href="<?=$privacyPolicy ? $privacyPolicy : 'proteccion_datos.php';?>" data-ref="" target="_blank">política de privacidad</a> y 
								<a class="enlace_pp" href="<?=$condicionesPromo;?>" data-ref="" target="_blank">condiciones generales.</a></p>
							</div><!--/12-->
<!--							<div class="col12">
								<p class="mini">El envío del formulario supone la aceptación de la 
								<a class="enlace_condiciones" href="<?=$path_raiz_aplicacion_local;?>proteccion_datos.php" data-ref="" target="_blank">política de privacidad</a> y 
								<a class="enlace_pp" href="<?=$arr_creas['condiciones'];?>" data-ref="" target="_blank">condiciones generales.</a></p>
							</div>/12-->
						</div>
						<div class="clearfix"></div>
					</div>
					<!--/#campos-->
