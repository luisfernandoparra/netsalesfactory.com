<?php
/**
 * FUNCION INCONCERT
 * 
 * 2015.12.28
 */
@session_start();
header('Content-Type: text/html; charset=utf-8');

function DialerCallBack($serverIp, $vcc, $campaignId, $importId, $batchId, $contactId, $phoneType, $countryId, $areaId, $number, $contactName, $contactPriority, $extraData, $scheduleDate, $timeZone, $agentId, $batchPriority)
{
	date_default_timezone_set('UTC');
	$port = '8082';
	$soaUri = '/inconcert/apps/soaintegration';
	$appUri = '/dialer_callback/';
	$url = 'http://'.$serverIp.':'.$port.$soaUri.$appUri;

	$header = array (  
		'User-Agent MSIE 7.0',
		'Content-Type: application/json'
	);

	$isVip = 0;
	$isClient = 0;
	$zip = '-1';
	$extension = '';
	$year = date('Y',strtotime($scheduleDate));
	$month = date('n',strtotime($scheduleDate));
	$day = date('j',strtotime($scheduleDate));
	$hour = date('H',strtotime($scheduleDate));
	$minute = date('i',strtotime($scheduleDate));

	/*$year = '';
			$month = '' ;
			$day = '';
			$hour = '';
			$minute = '';*/

	$data = new SimpleXMLElement('<data/>');
	$data->addChild('vcc', $vcc);
	$data->addChild('campaignid', $campaignId);
	$data->addChild('importid', $importId);
	$data->addChild('batchid', $batchId);
	$data->addChild('batchPriority', $batchPriority);
	$contact = new SimpleXMLElement('<contact/>');

	if ($contactId != '') {
			$contact->addChild('contactId', $contactId);
	}
	$contact->addChild('name', $contactName);
	$contact->addChild('accountOfficer');
	$contact->addChild('accountGroup');
	$contact->addChild('category');
	$contact->addChild('campaign');
	$contact->addChild('VIP', $isVip);
	$contact->addChild('IsClient', $isClient);
	$contact->addChild('lastManagementResult');
	$contact->addChild('agentId', $agentId);
	$contact->addChild('newPriority', $contactPriority);
	$contact->addChild('year', $year);
	$contact->addChild('month', $month);
	$contact->addChild('day', $day);
	$contact->addChild('hour', $hour);
	$contact->addChild('minute', $minute);
	$contact->addChild('timeZone', $timeZone);

	$phones = $contact->addChild('phones');
	$phone = $phones->addChild('phone');

	$phone->addChild('phoneType', $phoneType);
	$phone->addChild('countryId', $countryId);
	$phone->addChild('areaId', $areaId);
	$phone->addChild('ZIP', $zip);
	$phone->addChild('phoneNumber', $number);
	$phone->addChild('phoneExtension', $extension);
	$phone->addChild('importName', 'Telefono');

	$extra = $contact->addChild('data');

	foreach ($extraData as $nombre=>$valor)
	{
		$item = $extra->addChild('item');
		$item->addChild('name', $nombre);
		$item->addChild('value', $valor);
		$item->addChild('index', '1');
	}

	$postXml = $data->asXML().$contact->asXML();
	$postXml = str_replace('<?xml version="1.0"?>', '', $postXml);
//print_r($header);die();echo $url;echo $postXml;die();
	$ch = curl_init();
	#curl_setopt($ch, CURLOPT_VERBOSE, true);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postXml);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
	curl_setopt($ch, CURLOPT_POST, 1); 

	$response = curl_exec($ch);

	return $response;
}
