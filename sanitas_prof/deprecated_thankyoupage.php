<?php
/**
 * SANITAS SALUD
 * 
 * página de thankyuou, no utilizada realmente, solo paralas creas de gmail
 */
include('../conf/config_web.php');
ini_set('display_errors', 0);
include('conf/config_web.php');
include($path_raiz_includes . 'includes/initiate_thankyoupage.php');
include($path_raiz_includes . 'class/class.pixels.management.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
		<title>¡Gracias de <?php echo ucwords($landingMainName); ?>!</title>
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width,initial-scale=1"/> 
		<link href="<?php echo $path_raiz_aplicacion_local; ?>css/estilos_thanks.css" rel="stylesheet" type="text/css">
</head>


<body>

	<section class="contenedor">
		<section class="foto">
				<img src="<?php echo $path_raiz_aplicacion_local; ?>img/thanks_medico.jpg"/>
				<img src="<?php echo $path_raiz_aplicacion_local; ?>img/thanks_medico_mvl.jpg"/>
		</section>

		<section class="pie">
				<img src="<?php echo $path_raiz_aplicacion_local; ?>img/thanks_pie-img.jpg"/>
		</section
	</section>
<?php
if (!empty($_REQUEST['debug'])) {
	new dBug($_REQUEST);
}
/* $id_crea=25;
$leadNumber = 'test-d6t345';
$saltarPixeles = true;
$cookieEnable = true;

echo 'id_crea: ' . $id_crea;
echo ' salta pixel: ' . $saltarPixeles;
new dBug($_REQUEST);
*  */

include($path_raiz_includes . 'includes/pixels_thankyoupage.php');
session_destroy();
?>
</body>
</html>